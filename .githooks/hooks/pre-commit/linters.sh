#!/usr/bin/env bash
#
# Запускаем валидацию ESlint-ом всех файлов которые мы хотим закоммитить

# Подключаем функции-помощники
. "$(dirname $0)/utils.sh"

eslintPath="$(npm bin)/eslint"
stylelintPath="$(npm bin)/stylelint"

if [ ! -e "$eslintPath" ]; then
    print_fail "ESlint не установлен локально в ваших зависимостях, устанавливаю ESlint."
    npm install eslint --save-dev
fi

if [ ! -e "$stylelintPath" ]; then
    print_fail "Stylelint не установлен локально в ваших зависимостях, устанавливаю Stylelint."
    npm install stylelint --save-dev
fi

# файлы которые хотим закоммитить
JSfilesForCheck=$(git diff --cached --name-only --diff-filter=ACM HEAD | egrep '.jsx?$' | xargs echo)



if [ "$JSfilesForCheck" != "" ]; then
  $eslintPath $JSfilesForCheck

  if [ $? == 1 ]; then
    print_fail "Нельзя просто так взять и закоммитить код не по стайл-гайду!";
    exit 1;
   fi
fi

if [ "$LESSfilesForCheck" != "" ]; then
  $stylelintPath $LESSfilesForCheck

  if [ $? == 2 ]; then
    print_fail "Нельзя просто так взять и закоммитить код не по стайл-гайду!";
    exit 1;
   fi
fi

exit 0;
