import Keycloak from 'assets/libs/keycloak';
import serviceUrl from './service-url';

export const KEYCLOAK_INIT_CONFIG = {
  flow: 'implicit',
  onLoad: 'login-required',
  checkLoginIframe: false,
  responseMode: 'fragment'
};

if (window.sessionStorage && window.sessionStorage.getItem('token')) {
  KEYCLOAK_INIT_CONFIG.token = window.sessionStorage.getItem('token');
}


class KeycloakWrapper {
  constructor() {
    this.instance = null;
  }

  init() {
    this.instance = new Keycloak({
      get url() {
        return serviceUrl.AUTH_BASE_URL;
      },
      realm: 'elbrus-ng',
      clientId: 'spa'
    });
    return this.instance;
  }

  auth() {
    return new Promise((resolve, reject) => {
      this.instance.init(KEYCLOAK_INIT_CONFIG).success(() => {
        if (this.instance.authenticated && window.sessionStorage) {
          window.sessionStorage.setItem('token', this.instance.token);
          resolve();
        } else {
          reject('notAuthenticated');
        }
      });
    });
  }
}

export const keycloak = new KeycloakWrapper();
