const urlService = {
  // Корень сервисов бакенда, должен оканчиваться на слеш "/"
  SERVICE_BASE_URL: '/',

  // Корень сервиса авторизации, должен оканчиваться на слеш "/"
  AUTH_BASE_URL: '/'
};

export default urlService;
