## JAVA
  (http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html)[скачать java]

## Download latest selenium standalone server
  curl -O http://selenium-release.storage.googleapis.com/3.0/selenium-server-standalone-3.0.1.jar

## Download the latest version geckodriver for your environment and unpack it in your project directory
  OSX geckodriver
   * curl -L https://github.com/mozilla/geckodriver/releases/download/v0.11.1/geckodriver-v0.11.1-macos.tar.gz | tar xz
  OSX ChromeDriver - WebDriver for Chrome 
   * (https://sites.google.com/a/chromium.org/chromedriver/home)[chromedriver] 

## Start selenium standalone server
  * java -jar -Dwebdriver.gecko.driver=./geckodriver selenium-server-standalone-3.0.1.jar
  * java -jar -Dwebdriver.chrome.driver selenium-server-standalone-3.0.1.jar


##
 * (https://buddy.works/guides/how-write-selenium-tests-in-nodejs-with-webdriver)
 * (http://webdriver.io/guide.html)




