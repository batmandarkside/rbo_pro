/* eslint-disable */
const webdriverio = require('webdriverio');
const config = require('./integration.config');
const qs = require('qs');
const uuid = require('uuid/v4');

// старая авторизация
function authorize(login, password) {
  const responseType = 'response_type=id_token token';
  const responseMode = 'response_mode=fragment';
  const scope = 'scope=openid';
  const clientId = 'client_id=spa';
  const redirectUri = 'redirect_uri=http://localhost:3444';
  const state = uuid();
  const nonce = uuid();
  const urlAyth = `http://auth.dev.rbo.raiffeisen.ru/authorize?${clientId}&${redirectUri}&${responseType}&${responseMode}&${scope}&state=${state}&nonce=${nonce}`;
  webdriverio
    .remote(config)
    .init()
    // переходим на страницу авторизации
    .url(urlAyth)
    // подставляем в форму login и password
    .setValue("input[name='login']", login)
    .setValue("input[name='password']", password)
    // кнопка submit
    .click('button.primary')    
    // ждем пока поменяется url ( произойдет редирект на localhost с параметров access_token )  
    // парсим урл и записываем USER_ACCESS_TOKEN в переменную окружения
    .waitUntil(function () {
      return this.getUrl().then(
        (url) => {
          const search = url.split('#')[1];
          const parsedUrl = qs.parse(search);
          console.log('access_token', parsedUrl.access_token);
          process.env.USER_ACCESS_TOKEN = parsedUrl.access_token;
          return url.includes('localhost') && parsedUrl.access_token;
        },
        (err) => {
          throw err;
        }
      );
    }, 10000, 'Expectation error: Timed out waiting for current url')
    .waitForVisible('.b-application__header')
    .end();
}

exports = module.exports = authorize;
