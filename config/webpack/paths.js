const path = require('path');

const appPath = path.join(process.cwd(), 'src');
const assetsPath = path.join(appPath, 'assets');
const relAssetsPath = path.relative('.', assetsPath);
const processEnv = process.env.NODE_ENV;


const aliases = {
  root: process.cwd(),
  'moment-timezone': 'moment-timezone/moment-timezone',
  styles: path.join(assetsPath, 'styles'),
  services: path.join(appPath, 'services'),
  'ui-components': path.join(appPath, 'components', 'ui-components'),
  'constants': path.join(appPath, 'constants')
};

module.exports = {
  appPath,
  assetsPath,
  relAssetsPath,
  processEnv,
  aliases
};
