import { expect } from 'chai';
import getAxios from './axiosconfig';
import { orgId } from './settings.json';
import { findAllAccountsByQuery, findAllMovementsByQuery } from '../accounts';

const axios = getAxios();

describe('Account API ', () => {
  it('findAllAccountsByQuery', () => {
    const config = findAllAccountsByQuery({
      accNum:'40702810600001489171',
      currIsoCode:'RUB',
      orgId,
      status:'OPEN',
      type:'1'
    });

     return axios(config).then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.have.all.keys('accNum','accStatus','accType','availableBalance','balanceDate','branchBic','branchCorrAcc','branchId','branchName','currCode','currCodeIso','currentBalance','id','intName','lastBalance','lastBalanceByAccCourse','lastBalanceCourse','name','orgId','orgName','rest', 'restDate');
      })
  });

  it('findAllMovementsByQuery', () => {
    const config = findAllMovementsByQuery({
      accId: '080f2994-2dbd-42d0-946c-0160dbe338bf'
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.have.all.keys('account','aviseType','corrAccount','corrBankName','corrName','credit','currCodeIso','debet','inn','isFinal','operDate','operNumber','operType','operationType','orgName','paymentPurpose','recordID','statementRecordID','stmtDate','valDate');
      });
  });
});
