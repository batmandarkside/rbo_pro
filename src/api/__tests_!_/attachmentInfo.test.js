import { expect } from 'chai';
import getAxios from './axiosconfig';
import { findByAttachmentId } from '../attachmentInfo';
import { outgoingMessageId } from './settings.json';

const axios = getAxios();

describe('AttachmentsInfo API ', () => {
  it('findByAttachmentId', () => {
    const config = findByAttachmentId(outgoingMessageId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.have.all.keys('comment', 'date', 'docId', 'id', 'mimeType', 'name', 'size');
      })
  });
});
