import { expect } from 'chai';
import getAxios from './axiosconfig';
import { updatePassword, logout, findRuleChangePassword } from '../authentication';

const axios = getAxios();

describe('Authentication API ', () => {

  // @todo Нужно подумать как лучше тестить смену пароля(updatePassword) и выход(logout), как это корректней делать

  it('findRuleChangePassword', () => {
    const config = findRuleChangePassword();

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data).to.be.length(4);
        expect(res.data[0]).to.be.a('string');
      });
  });
});
