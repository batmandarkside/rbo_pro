import httpAdapter from 'axios/lib/adapters/http';
import { axiosConfig } from 'config/application/axios';
import { token } from './settings.json';

import settings from 'app/app-settings.json';

export default function getAxios() {
  axiosConfig.setAuthToken(`Bearer ${token}`);
  const axios = axiosConfig.getAxiosInstance();
  axios.defaults.baseURL = settings.SERVICE_BASE_URL;
  axios.defaults.adapter = httpAdapter;

  return axios;
}
