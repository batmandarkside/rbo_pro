import { expect } from 'chai';
import getAxios from './axiosconfig';
import logResponse from './debugUtils';
import { findOneById, findAllByQuery, update, deleteByIds } from '../correspondent';
import { corrspondentId, orgId } from './settings.json';

const axios = getAxios();

describe('Correspondent API ', () => {
  it('findOneById', () => {
    const config = findOneById(corrspondentId);

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'id',
        'orgId',
        'account',
        'receiverName',
        'receiverBankBIC',
        'receiverINN',
        'receiverKPP',
        'signed',
        'paymentPurpose',
        'allowedSmActions'
      );
    });
  });

  // @todo: возвращает 400 :(
  it('findAllByQuery', () => {
    const config = findAllByQuery({
      orgId,
      sort: 'receiverName',
      desc: true
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'id',
        'orgId',
        'account',
        'receiverName',
        'receiverBankBIC',
        'receiverINN',
        'receiverKPP',
        'signed',
        'paymentPurpose',
        'allowedSmActions'
      );
    });
  });

  it('update', () => {
    const config = update(corrspondentId);

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'status',
        'recordID',
        'allowedSmActions',
        'errors',
      );
    });
  });

  it('deleteByIds', () => {
    const config = deleteByIds(['']);

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
    });
  });
});
