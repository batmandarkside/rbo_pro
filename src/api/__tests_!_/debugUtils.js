/* eslint no-console: off */
export default function logResponse(response) {
  console.log('URL: ', response.config.url);
  console.log('Method: ', response.config.method);
  console.log('Status: ', response.status);
  console.log('StatusText: ', response.statusText);
  console.log('Path: ', response.request.path);
  console.log('Data: ', response.data);
}
