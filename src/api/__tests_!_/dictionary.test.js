import {expect} from 'chai';
import getAxios from './axiosconfig';
import {
  findCurrencyOperationTypes,
  findDrawerStatuses,
  findCBC,
  findBIC,
  findPaymentKinds,
  findPaymentPriorities,
  findVatCalculationRules,
  findCustomerKpps,
  findPayReasons,
  findAccounts,
  findChargeTypes,
  findTaxPeriods,
  findReceiverTopics,
  findCurrCodes
} from '../dictionary';
import { orgId } from './settings.json';

const axios = getAxios();

describe('Dictionary API ', () => {

  // @Todo: Нужно доделать этот метод, не ясно что посылать, на orgId свой не реагирует может нужен чужой
  // it('findPartners', () => {
  //   const config = findPartners({ orgId });
  //
  //   return axios(config).then((res) => {
  //     logResponse(res)
  //     expect(res.status).to.equal(200);
  //     expect(res.data).to.exist;
  //     expect(res.data).to.be.a('array');
  //     expect(res.data[0]).to.have.all.keys('accNum', 'accStatus', 'accType', 'availableBalance', 'balanceDate', 'branchBic', 'branchCorrAcc', 'branchId', 'branchName', 'currCode', 'currCodeIso', 'currentBalance', 'id', 'intName', 'lastBalance', 'lastBalanceByAccCourse', 'lastBalanceCourse', 'name', 'orgId', 'orgName', 'rest', 'restDate');
  //   })
  // });

  it('findCurrencyOperationTypes', () => {
    const config = findCurrencyOperationTypes({
      code: '01030'
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('code', 'description');
      expect(res.data[0].code).to.be.a('string');
      expect(res.data[0].description).to.be.a('string');
    })
  });

  it('findDrawerStatuses', () => {
    const config = findDrawerStatuses({});

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'status', 'comment');
      expect(res.data[0].id).to.be.a('string');
      expect(res.data[0].status).to.be.a('string');
      expect(res.data[0].comment).to.be.a('string');
    })
  });

  it('findCBC', () => {
    const config = findCBC({
      code: '04810807281011000110'
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'code', 'description');
      expect(res.data[0].id).to.be.a('string');
      expect(res.data[0].code).to.be.a('string');
      expect(res.data[0].description).to.be.a('string');
    })
  });

  it('findBIC', () => {
    const config = findBIC({
      code: '044525700'
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys(
        'address',
        'bankName',
        'bankType',
        'bic',
        'corrAccount',
        'id',
        'locationName',
        'locationType',
        'postcode',
        'typeEPaymentsPart');
    })
  });

  it('findPaymentKinds', () => {
    const config = findPaymentKinds(orgId);

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('code', 'description');
    })
  });

  it('findPaymentPriorities', () => {
    const config = findPaymentPriorities();

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'code', 'description');
      expect(res.data[0].id).to.be.a('string');
      expect(res.data[0].code).to.be.a('string');
      expect(res.data[0].description).to.be.a('string');
    })
  });

  it('findVatCalculationRules', () => {
    const config = findVatCalculationRules();

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('description', 'rule');
    })
  });

  it('findCustomerKpps', () => {
    const config = findCustomerKpps([orgId, orgId]);

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('param', 'value');
    })
  });

  it('findPayReasons', () => {
    const config = findPayReasons();

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'comment', 'param');
    })
  });

  it('findAccounts', () => {
    const config = findAccounts({orgId});

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys(
        'orgId',
        'accNum',
        'payerName',
        'payerBankName',
        'payerBankSettlementType',
        'payerBankCity',
        'payerBankBIC',
        'payerBankCorrAccount',
        'currentBalance',
        'actualBalanceDate'
      );
    })
  });

  it('findChargeTypes', () => {
    const config = findChargeTypes();

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'comment', 'param');
    })
  });

  it('findTaxPeriods', () => {
    const config = findTaxPeriods();

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'comment', 'param');
      expect(res.data[0].id).to.be.a('string');
      expect(res.data[0].comment).to.be.a('string');
      expect(res.data[0].param).to.be.a('string');
    })
  });

  it('findReceiverTopics', () => {
    const config = findReceiverTopics();

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'messageText', 'receiver', 'topic');
      expect(res.data[0].id).to.be.a('string');
      expect(res.data[0].messageText).to.be.a('string');
      expect(res.data[0].receiver).to.be.a('string');
      expect(res.data[0].topic).to.be.a('string');
    })
  });

  it('findCurrCodes', () => {
    const config = findCurrCodes({});

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.have.all.keys('id', 'code', 'isoCode', 'name');
      expect(res.data[0].id).to.be.a('string');
      expect(res.data[0].code).to.be.a('string');
      expect(res.data[0].isoCode).to.be.a('string');
      expect(res.data[0].name).to.be.a('string');
    })
  });

});
