import { expect } from 'chai';
import getAxios from './axiosconfig';
import { docId } from './settings.json';
import { stateHistoryById, signsCheckById, deleteById, toArchiveByIds, deleteArchiveByIds } from '../document';

const axios = getAxios();

describe('Document API ', () => {
  it('stateHistoryById', () => {
    const config = stateHistoryById(docId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.have.all.keys('beginState','endState','user','date');
        expect(res.data[0].beginState).to.be.a('string');
        expect(res.data[0].endState).to.be.a('string');
        expect(res.data[0].user).to.be.a('string');
        expect(res.data[0].date).to.be.a('string');
      })
  });

  it('signsCheckById', () => {
    const config = signsCheckById(docId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.have.all.keys('digest','docType','signs');
        expect(res.data[0].digest).to.be.a('string');
        expect(res.data[0].docType).to.be.a('string');
        expect(res.data[0].signs).to.be.a('array');
        expect(res.data[0].signs[0]).to.be.a('object');
        expect(res.data[0].signs[0]).to.have.all.keys('signId', 'signType', 'signValid', 'signDateTime', 'signHash', 'userName', 'userPosition', 'userLogin');
        expect(res.data[0].signs[0].signId).to.be.a('string');
        expect(res.data[0].signs[0].signType).to.be.a('string');
        expect(res.data[0].signs[0].signValid).to.be.a('boolean');
        expect(res.data[0].signs[0].signDateTime).to.be.a('string');
        expect(res.data[0].signs[0].signHash).to.be.a('string');
        expect(res.data[0].signs[0].userName).to.be.a('string');
        expect(res.data[0].signs[0].userPosition).to.be.a('string');
        expect(res.data[0].signs[0].userLogin).to.be.a('string');
      })
  });

  // @todo - нужно придумать как их тестировать так как они перемещеают документ
  // it('deleteById', () => {
  //   const config = deleteById('f84b6334-df73-4250-aa46-dd72c0ba8bd1');
  //
  //   return axios(config)
  //     .then((res) => {
  //       expect(res.status).to.equal(200);
  //       expect(res.data).to.exist;
  //     })
  // });
  //
  // it('toArchiveByIds', () => {
  //   const config = toArchiveByIds(['f84b6334-df73-4250-aa46-dd72c0ba8bd1']);
  //
  //   return axios(config)
  //     .then((res) => {
  //       expect(res.status).to.equal(200);
  //       expect(res.data).to.exist;
  //     })
  // });
  //
  // it('deleteArchiveByIds', () => {
  //   const config = deleteArchiveByIds(['f84b6334-df73-4250-aa46-dd72c0ba8bd1']);
  //
  //   return axios(config)
  //     .then((res) => {
  //       expect(res.status).to.equal(200);
  //       expect(res.data).to.exist;
  //     })
  // });
});
