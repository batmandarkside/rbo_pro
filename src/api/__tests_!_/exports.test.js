import {expect} from 'chai';
import getAxios from './axiosconfig';
import {
  createIncomeMessage,
  createOutcomeMessage,
  createOutcomeMessageArchived,
  createOutcomeMessageDeleted,
  createAccountOperations,
  createStatementByQuery,
  createStatementsByQuery,
  createOperationById,
  createDocumentById
} from '../exports';
import { orgId, docId } from './settings.json';

const axios = getAxios();

describe('Export API ', () => {
  it('createIncomeMessage', () => {
    const config = createIncomeMessage({
      orgId,
      desc: true,
      sort: 'date',
      visibleColumns: ['date', 'attach', 'read', 'title', 'receiver', 'sender', 'account', 'mustRead']
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createOutcomeMessage', () => {
    const config = createOutcomeMessage({
      orgId,
      desc: true,
      sort: 'date',
      visibleColumns: ['number', 'attach', 'date', 'status', 'receiver', 'topic', 'account']
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createOutcomeMessageArchived', () => {
    const config = createOutcomeMessageArchived({
      orgId,
      desc: true,
      sort: 'date',
      visibleColumns: ['number', 'attach', 'date', 'status', 'receiver', 'topic', 'account']
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createOutcomeMessageDeleted', () => {
    const config = createOutcomeMessageDeleted({
      orgId,
      desc: true,
      sort: 'date',
      visibleColumns: ['number', 'attach', 'date', 'status', 'receiver', 'topic', 'account']
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createAccountOperations', () => {
    const config = createAccountOperations({
      account: '40702810600001489171',
      desc:'true',
      isFinal:'true',
      orgId:'a7468808-5048-4d99-b9ad-0557e06b5410',
      sort:'stmtDate',
      stmtDateFrom:'2014-11-20T00:00:00+03:00',
      stmtDateTo:'2014-11-20T00:00:00+03:00',
      visibleColumns: ['account', 'final', 'operDate', 'debit', 'credit', 'currCodeIso', 'corrName', 'paymentPurpose']
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createStatementByQuery', () => {
    const config = createStatementByQuery({
      docId: '28a4a37a-0721-41e3-ba43-e3f9e2b4899e',
      detail: false,
      format: 'pdf'
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createStatementsByQuery', () => {
    const config = createStatementsByQuery({
      desc: 'false',
      orgId: 'a7468808-5048-4d99-b9ad-0557e06b5410',
      sort: 'account',
      visibleColumns: ['account', 'accountCurrency', 'stateDate', 'stateType', 'opBalance', 'closingBalance', 'turnoverD', 'turnoverC', 'documentDCount', 'documentCCount', 'availBalance', 'org']
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createOperationById', () => {
    const config = createOperationById({
      docId: '18513c23-f754-45de-865a-7e6b24e6fe25',
      format: 'pdf' });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

  it('createDocumentById', () => {
    const config = createDocumentById({
      docId,
      format: 'pdf'
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
    });
  });

});
