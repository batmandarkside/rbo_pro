import { expect } from 'chai';
import getAxios from './axiosconfig';
import logResponse from './debugUtils';
import { findOneById, findAllByQuery, findErrorsImport, createTask } from '../import';
import { orgId, importId } from './settings.json';

const axios = getAxios();

describe('Import API ', () => {
  it('findOneById', () => {
    const config = findOneById(importId);

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'finished',
        'started',
        'state',
        'status',
        'totalDocs',
        'fileName',
        'importType',
        'log',
        'sum',
        'errors',
        'success',
        'warnings',
        'failed',
        'statuses'
      );
    });
  });

// @todo: Очень странно, но back возвращает 400
  it('findAllByQuery', () => {
    const config = findAllByQuery({
      offset: 0,
      offsetStep: 31
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.be.a('object');
      expect(res.data[0]).to.have.all.keys(
        'id',
        'taskType',
        'finished',
        'started',
        'state',
        'status',
        'totalDocs',
        'fileName',
        'importType'
      );
    });
  });

// @todo: Очень странно, но back возвращает 400
  it('findErrorsImport', () => {
    const config = findErrorsImport({
      docId: importId
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('array');
      expect(res.data[0]).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'orderIndex',
        'message'
      );
    });
  });

  it('createTask', () => {
    const config = createTask({
      fileName: 'РПП_1С ЗАО АВИС.txt',
      content:'',
      settings: {
        payerNameFromFile: false,
        paymentKindFromFile: true,
        paymentPurposeIgnoreNewLines: false,
        correctionRules: ''
      }
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'taskId'
      );
    });
  });
});
