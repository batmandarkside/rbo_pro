import { expect } from 'chai';
import getAxios from './axiosconfig';
import { findById, findAllByQuery, findAllImportant, findAttachmentsInfoById, findAttachmentsById } from '../incomingMessage';
import { incomingMessageId, orgId, outgoingAttachId } from './settings.json';

const axios = getAxios();

describe('IncomingMessage API ', () => {
  it('findById', () => {
    const config = findById(incomingMessageId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.have.all.keys(
          'docId',
          'date',
          'topic',
          'attachExists',
          'sender',
          'account',
          'message',
          'receivers',
          'docNumber',
          'messageType'
        );
      });
  });

  it('findAllByQuery', () => {
    const config = findAllByQuery({
      desc:'true',
      offset:'0',
      offsetStep:'31',
      orgId,
      sort:'date'
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'docId',
          'date',
          'topic',
          'attachExists',
          'sender',
          'account',
          'message',
          'receivers',
          'mustRead',
          'read',
          'messageType'
        );
      });
  });

  it('findAllImportant', () => {
    const config = findAllImportant({
      orgId
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'docId',
          'date',
          'topic',
          'attachExists',
          'sender',
          'account',
          'message',
          'receivers',
          'mustRead',
          'read',
          'messageType'
        );
      });
  });

  it('findAttachmentsInfoById', () => {
    const config = findAttachmentsInfoById(
      incomingMessageId
    );

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array')
        expect(res.data[0]).to.be.a('object')
        expect(res.data[0]).to.have.all.keys(
          'id',
          'docId',
          'name',
          'mimeType',
          'comment',
          'size',
          'date'
        );
      });
  });


  describe('findAttachmentsById API ', () => {
    it('findById', () => {
      const config = findAttachmentsById(outgoingAttachId);

      return axios(config)
        .then((res) => {
          expect(res.status).to.equal(200);
          expect(res.data).to.exist;
        })
    });
  });

});
