import { expect } from 'chai';
import getAxios from './axiosconfig';
import { findAllForCurrentUser } from '../organization';

const axios = getAxios();

describe('Organization API ', () => {
  it('findAllForCurrentUser', () => {
    const config = findAllForCurrentUser();

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys('branches','links','orgs');
      expect(res.data.branches).to.be.a('array');
      expect(res.data.branches[0]).to.be.a('object');
      expect(res.data.branches[0]).to.have.all.keys(
        'id',
        'shortName',
        'intName',
        'bic',
        'payName',
        'intPayName',
        'corAccount',
        'placeType',
        'place',
        'intPlace',
        'swift',
        'countryCode',
        'countryIsoCode',
        'address',
        'intAddress',
        'placeShortName',
        'timeZone');
      expect(res.data.links).to.be.a('array');
      expect(res.data.links[0]).to.be.a('object');
      expect(res.data.links[0]).to.have.all.keys('orgId', 'branchId');
      expect(res.data.orgs).to.be.a('array');
      expect(res.data.orgs[0]).to.be.a('object');
      expect(res.data.orgs[0]).to.have.all.keys(
        'area',
        'city',
        'country',
        'id',
        'inn',
        'intAddress',
        'intName',
        'kpp',
        'lawType',
        'name',
        'ogrn',
        'shortName'
      );
    });
  });
});
