import { expect } from 'chai';
import getAxios from './axiosconfig';
import logResponse from './debugUtils';
import { findOneById, findAllByQuery, findAllArchived, findAllDeleted, update, findAllStatuses, countUnreaded, sendMessageWithId, createCopyByMessageId, findDefaultValuesByOrganisationId } from '../outgoingMessage';
import { outgoingMessageId, orgId } from './settings.json';

const axios = getAxios();

describe('OutgoingMessage API ', () => {
  it('findOneById', () => {
    const config = findOneById(outgoingMessageId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.have.all.keys(
          'docId',
          'docNumber',
          'docDate',
          'account',
          'topic',
          'attachExists',
          'branchId',
          'receiver',
          'orgId',
          'allowedSmActions',
          'status',
          'message',
          'signed',
          'linkedDocs'
        );
      });
  });

  it('findAllByQuery', () => {
    const config = findAllByQuery({
      'desc':'true',
      orgId,
      'sort':'date',
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'docId',
          'docNumber',
          'docDate',
          'account',
          'topic',
          'attachExists',
          'receiver',
          'orgName',
          'allowedSmActions',
          'status'
        );
        expect(res.data[0].allowedSmActions).to.be.a('object');
        expect(res.data[0].allowedSmActions).to.have.all.keys(
          'visa',
          'sign',
          'fromArchive',
          'delete',
          'save',
          'archive',
          'send'
        );
      });
  });

  it('findAllArchived', () => {
    const config = findAllArchived({
      'desc':'true',
      orgId,
      'sort':'date',
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'docId',
          'docNumber',
          'docDate',
          'account',
          'topic',
          'attachExists',
          'receiver',
          'orgName',
          'allowedSmActions',
          'status'
        );
        expect(res.data[0].allowedSmActions).to.be.a('object');
        expect(res.data[0].allowedSmActions).to.have.all.keys(
          'visa',
          'sign',
          'fromArchive',
          'delete',
          'save',
          'archive',
          'send'
        );
      });
  });

  it('findAllDeleted', () => {
    const config = findAllDeleted({
      'desc':'true',
      orgId,
      'sort':'date',
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'docId',
          'docNumber',
          'docDate',
          'account',
          'topic',
          'attachExists',
          'receiver',
          'orgName',
          'allowedSmActions',
          'status'
        );
        expect(res.data[0].allowedSmActions).to.be.a('object');
        expect(res.data[0].allowedSmActions).to.have.all.keys(
          'visa',
          'sign',
          'fromArchive',
          'delete',
          'save',
          'archive',
          'send'
        );
      });
  });

  it('update', () => {
    const config = update({
        docNumber: "14",
        docDate: "2017-03-09T10:16:03+0300",
        account: "40702810600001489171",
        topic: "xx3x5x7x9x12x15x18x21x24x27x30x33x36x39x42x45x48x51x54x57x60x63x66x69x72x75x78x81x84x87x90x93x96x100",
        branchId: "3002",
        branchName: null,
        receiver: "Вниманию Отдела открытия счетов",
        orgId,
        orgName: null,
        status: "Новое",
        message: "qweqwe"
      });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
      });
  });

  it('findAllStatuses', () => {
    const config = findAllStatuses();

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
      });
  });

  it('countUnreaded', () => {
    const config = countUnreaded();

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('string');
      });
  });

  // @todo: Нужно подумать как тестировать действия которые перемещают письма используемые для теста
  // it('sendMessageWithId', () => {
  //   const config = sendMessageWithId('61239809-cec8-400a-ad76-b7bfee454812');
  //
  //   return axios(config)
  //     .then((res) => {
  //       expect(res.status).to.equal(400);
  //       expect(res.data).to.exist;
  //     });
  // });

  it('createCopyByMessageId', () => {
    const config = createCopyByMessageId(outgoingMessageId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys(
          'docNumber',
          'docDate',
          'account',
          'topic',
          'attachExists',
          'branchId',
          'receiver',
          'orgId',
          'orgName',
          'allowedSmActions',
          'message',
          'signed',
          'linkedDocs'
        )
      });
      expect(res.data[0].allowedSmActions).to.be.a('object');
      });
  });

  it('findDefaultValuesByOrganisationId', () => {
    const config = findDefaultValuesByOrganisationId(orgId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys(
          'docNumber',
          'docDate',
          'attachExists',
          'orgId',
          'orgName',
          'allowedSmActions',
          'signed',
          'linkedDocs'
        );
  });
});
