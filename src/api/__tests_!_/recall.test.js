import { expect } from 'chai';
import getAxios from './axiosconfig';
import { create, getDocDefaultData } from '../recall';
import { orgId } from './settings.json';

const axios = getAxios();

describe('Recall API ', () => {
  it('create', () => {
    const config = create({
      "recallDocID" : "be22c6d5-8c75-4e63-8730-4fbee36befcb",
      "orgId" : "a7468808-5048-4d99-b9ad-0557e06b5410",
      "docInfo" : "Рублевое платежное поручение:\n    Номер 831 от 19.07.2016 на сумму 85200 руб.\n    со счета № 40702.810.6.00001489171\n    на счет № 40125.810.8.64561024956",
      "orgInn" : "3250503443",
      "orgName" : "ООО Керамика Люкс",
      "docNumber" : "2",
      "docDate" : "2017-03-09T00:00:00+0300",
      "reason" : "asd"
    });

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'status',
        'recordID',
        'allowedSmActions',
        'errors'
      );
    });
  });

  it('getDocDefaultData', () => {
    const config = getDocDefaultData(orgId);

    return axios(config).then((res) => {
      expect(res.status).to.equal(200);
      expect(res.data).to.exist;
      expect(res.data).to.be.a('object');
      expect(res.data).to.have.all.keys(
        'orgId',
        'orgInn',
        'orgName',
        'docNumber',
        'docDate',
        'allowedSmActions'
      );
    });
  });
});
