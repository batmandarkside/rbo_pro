import { expect } from 'chai';
import getAxios from './axiosconfig';
import logResponse from './debugUtils';
import { findOneById, findAllByQuery, countByQuery, findDefaultValuesByOrganisationId } from '../rpay';
import { orgId } from './settings.json';

const axios = getAxios();

describe('Rpay API ', () => {
  it('findOneById', () => {
    const config = findOneById('be22c6d5-8c75-4e63-8730-4fbee36befcb');

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys(
          'docNumber',
          'status',
          'amount',
          'docDate',
          'receiverName',
          'receiverINN',
          'receiverAccount',
          'receiverKPP',
          'receiverBankCorrAccount',
          'receiverBankBIC',
          'receiverBankName',
          'receiverBankCity',
          'receiverBankSettlementType',
          'paymentPurpose',
          'payerAccount',
          'recordID',
          'customerBankRecordID',
          'vatSum',
          'vatRate',
          'operationType',
          'payerKPP',
          'payerName',
          'payerINN',
          'payerBankBIC',
          'payerBankName',
          'payerBankCorrAccount',
          'paymentKind',
          'paymentPriority',
          'paymentCode',
          'bankAcceptDate',
          'vatCalculationRule',
          'allowedSmActions',
          'signed',
          'linkedDocs'
        );
      });
  });

  it('findAllByQuery', () => {
    const config = findAllByQuery({
      "orgIds":[orgId],
      'offsetStep':31,
      'sort':'date',
      'desc':true
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'status',
          'amount',
          'receiverName',
          'receiverAccount',
          'paymentPurpose',
          'payerAccount',
          'recordID',
          'customerBankRecordID',
          'payerName',
          'payerBankBIC',
          'bankMessage',
          'paymentKind',
          'allowedSmActions',
          'signed',
          'linkedDocs',
          'lastChangeStateDate'
        );
      });
  });

  it('countByQuery', () => {
    const config = countByQuery({
      'orgIds': [orgId],
      'statuses': ['Создан', 'Импортирован', 'Частично подписан'],
      'dateFrom': '2017-02-27T00:00:00+03:00'
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
      });
  });

  it('findDefaultValuesByOrganisationId', () => {
    const config = findDefaultValuesByOrganisationId(orgId);

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys(
          'docNumber',
          'amount',
          'docDate',
          'customerBankRecordID',
          'vatRate',
          'operationType',
          'payerKPP',
          'payerName',
          'payerINN',
          'paymentKind',
          'paymentPriority',
          'paymentCode',
          'vatCalculationRule',
          'allowedSmActions',
          'signed',
          'linkedDocs'
        );
      });
  });
});
