import { expect } from 'chai';
import getAxios from './axiosconfig';
import { findOneForCurrentSession, updateForCurrentSession } from '../settings';

const axios = getAxios();

describe('Settings API ', () => {
  it('findOneForCurrentSession', () => {
    const config = findOneForCurrentSession();

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys('settings');
        expect(res.data.settings).to.be.a('object');
      })
  });

  it('updateForCurrentSession', () => {
    const config = updateForCurrentSession({
      settings: {
        test: '123'
      }
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys('settings');
        expect(res.data.settings).to.be.a('object');
      })
  });
});
