import { expect } from 'chai';
import getAxios from './axiosconfig';
import * as SingNormalizr from 'dal/sign/normalizr';
// import logResponse from './debugUtils';
import { 
  getCryptoProfiles, 
  signToFormat,
  getCertList 
} from '../signature';
import { docIdforSign } from './settings.json';

const axios = getAxios();

// @Todo Нужно получать из копии хорошего документа  и далее с копией работать
const docId = docIdforSign;

describe('Signature API ', () => {

  it('getCertList', (done) => {
    const config = getCertList();    
    return axios(config).then((res) => {    
      console.log('process.env.USER_ACCESS_TOKEN', process.env.USER_ACCESS_TOKEN);      
      expect(res.data).to.exist;      
      expect(res.data[0].mainStatus).to.equal('sendToBank');      
      expect(res.data[0].entityType).to.equal('reqNewOnly');            
      done();      
    });  
  });

  it('getCryptoProfiles', () => {
    const config = getCryptoProfiles({
      docId,
      forVisa: false
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'Name',
          'Position',
          'cryptoType',
          'ClientName',
          'cryptoProfileId',
          'Type'
        );
        console.log(res.data[0].cryptoProfileId);
      });
  });

  // @todo: Нужно подумать как тестировать действия которые перемещают письма используемые для теста
  // it('initSign', () => {
  //   const config = initSign({
  //     docId: [docId],
  //     cryptoProfileId: '608ed303-7c59-44cd-9cca-0a1935493284'
  //   });
  //
  //   return axios(config)
  //     .then((res) => {
  //       expect(res.status).to.equal(200);
  //       expect(res.data).to.exist;
  //       expect(res.data).to.be.a('object');
  //       expect(res.data).to.have.all.keys(
  //         'digests',
  //         'cert'
  //       );
  //       expect(res.data.digests).to.be.a('array');
  //     });
  // });
  //
  // it('sendSignToBank', () => {
  //   const config = sendSignToBank({
  //     signs: [{
  //       docId: docId,
  //       sign: '11111111'
  //     }],
  //     onToken: false
  //   });
  //
  //   return axios(config)
  //     .then((res) => {
  //       expect(res.status).to.equal(200);
  //       expect(res.data).to.exist;
  //       expect(res.data).to.be.a('array');
  //       expect(res.data[0]).to.be.a('object');
  //       expect(res.data[0]).to.have.all.keys(
  //         'status',
  //         'docId',
  //         'docType'
  //       );
  //     });
  // });

  it('signToFormat', () => {
    const config = signToFormat({
      docId:'631328ba-fc4e-4edb-a667-0cc3d4fd9c31',
      format:'zip',
      signId:'f2f014f7-6ce3-4070-af28-2cc86e235e6e'
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
      });
  });
});
