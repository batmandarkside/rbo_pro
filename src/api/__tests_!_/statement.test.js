import { expect } from 'chai';
import getAxios from './axiosconfig';
import { orgId } from './settings.json';
import { findOneById, findAllByQuery } from '../statement';

const axios = getAxios();

describe('Statement API ', () => {
  // Этот метод странный всё кроме пустой строки или с пробелами возвращает 200 и пустой ответ. В аурелии не использовался.
  it('findOneById', () => {
    const config = findOneById('_');

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
      });
  });

  it('findAllByQuery', () => {
    const config = findAllByQuery({});

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('array');
        expect(res.data[0]).to.be.a('object');
        expect(res.data[0]).to.have.all.keys(
          'recordID',
          'stateType',
          'docNumber',
          'stateDate',
          'stateUpdateDate',
          'receiptDate',
          'account',
          'accountCurrency',
          'accountName',
          'accountStatus',
          'natCurrency',
          'orgId',
          'branchName',
          'opBalance',
          'documentDCount',
          'documentCCount',
          'closingBalance',
          'availBalance'
        )
      })
  });
});
