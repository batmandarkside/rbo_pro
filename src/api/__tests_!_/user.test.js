import { expect } from 'chai';
import getAxios from './axiosconfig';
import { findOneForCurrentSession } from '../user';

const axios = getAxios();

describe('User API ', () => {
  it('findOneForCurrentSession', () => {
    const config = findOneForCurrentSession();

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.have.all.keys('login', 'fullName');
        expect(res.data.login).to.be.a('string');
        expect(res.data.fullName).to.be.a('string');
      })
  });
});
