import { expect } from 'chai';
import getAxios from './axiosconfig';
import {
  createByOutgoingMessageDetails,
  createByRecallDetails
} from '../validation';
import { orgId } from './settings.json';

const axios = getAxios();

describe('Validation API ', () => {
  it('createByOutgoingMessageDetails', () => {
    const config = createByOutgoingMessageDetails({
        docNumber: "14",
        docDate: "2017-03-09T10:16:03+0300",
        account: "40702810600001489171",
        topic: "xx3x5x7x9x12x15x18x21x24x27x30x33x36x39x42x45x48x51x54x57x60x63x66x69x72x75x78x81x84x87x90x93x96x100",
        branchId: "3002",
        branchName: null,
        receiver: "Вниманию Отдела открытия счетов",
        orgId,
        orgName: null,
        status: "Новое",
        message: "qweqwe"
      });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys(
          'status',
          'recordID',
          'allowedSmActions',
          'errors'
        );
      });
  });

  it('createByRecallDetails', () => {
    const config = createByRecallDetails({
      "recallDocID" : "be22c6d5-8c75-4e63-8730-4fbee36befcb",
      "orgId" : "a7468808-5048-4d99-b9ad-0557e06b5410",
      "docInfo" : "Рублевое платежное поручение:\n    Номер 831 от 19.07.2016 на сумму 85200 руб.\n    со счета № 40702.810.6.00001489171\n    на счет № 40125.810.8.64561024956",
      "orgInn" : "3250503443",
      "orgName" : "ООО Керамика Люкс",
      "docNumber" : "2",
      "docDate" : "2017-03-09T00:00:00+0300",
      "reason" : "asd"
    });

    return axios(config)
      .then((res) => {
        expect(res.status).to.equal(200);
        expect(res.data).to.exist;
        expect(res.data).to.be.a('object');
        expect(res.data).to.have.all.keys(
          'status',
          'recordID',
          'allowedSmActions',
          'errors'
        );
      });
  });
});
