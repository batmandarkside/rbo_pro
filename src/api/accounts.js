/**
 * Поиск счётов удовлетворяющих критериям
 * @param  {Object} config
 * @param  {string} config.orgId       Идентификатор организации
 * @param  {string} config.accNum      Номер счета
 * @param  {string} config.bic         Номер БИК
 * @param  {string} config.currIsoCode ISO код валюты счета
 * @param  {string} config.status      Статус счета (OPEN, CLOSED)
 * @param  {Number} config.type        Тип счета 1 - 'р/с, 2 - транзитный, 4 - депозитный, 5 - овердрафт
 * @param  {Number} [config.offset=0]  Смещение в элементах
 * @return {Object}
 */
export function findAllAccountsByQuery({ orgId, accNum, bic, currIsoCode, status, type, offset = 0 }) {
  const endpoint = {
    url: 'accounts'
  };

  const params = {
    orgId,
    accNum,
    bic,
    currIsoCode,
    status,
    type,
    offset
  };
  return { ...endpoint, params };
}


/**
 * Поиск движений по счёту удовлетворяющих критериям
 * @param  {string}  accId            Id счета
 * @param  {string}  account          Номер счета
 * @param  {Number}  offset=0         Смещение в элементах
 * @param  {Number}  offsetStep=31    Количество записей
 * @param  {string}  sort='stmtDate'  Поле для сортировки.
 * @param  {boolean} desc=false       Флаг сортировки в обратном направлении
 * @param  {string}  from             Начальная дата движений по счету (включительно)
 * @param  {string}  to               Конечная дата движений по счету (включительно)
 * @param  {string}  stmtDateFrom     Начальная дата выписки
 * @param  {string}  stmtDateTo       Конечная дата выписки
 * @param  {string}  operType         Тип операции: 0 – Дебет; 1 – Кредит. При отсутствии в фильтр попадает и дебет, и кредит
 * @param  {string}  operNumber       Номер операции
 * @param  {number}  sumMin           Минимальная сумма для выборки документа (включительно)
 * @param  {number}  sumMax           Максимальная сумма для выборки документа (включительно)
 * @param  {string}  currCodeIso      Валюта счета
 * @param  {string}  corrName         Наименование контрагента
 * @param  {string}  corrAccount      Счет контрагента
 * @param  {string}  purpose          Назначение платежа
 * @param  {string}  inn              ИНН контрагента
 * @param  {boolean} isFinal          Признак того, финальная операция или внутридневная
 * @return {Object}
 */
export function findAllMovementsByQuery(
  {
    accId,
    account,
    offset = 0,
    offsetStep = 31,
    sort = 'stmtDate',
    desc = false,
    from,
    to,
    stmtDateFrom,
    stmtDateTo,
    operType,
    operNumber,
    sumMin,
    sumMax,
    currCodeIso,
    corrName,
    corrAccount,
    purpose,
    inn,
    isFinal
  }) {
  const endpoint = {
    url: 'accounts/movements'
  };
  const params = {
    accId,
    account,
    offset,
    offsetStep,
    sort,
    desc,
    from,
    to,
    stmtDateFrom,
    stmtDateTo,
    operType,
    operNumber,
    sumMin,
    sumMax,
    currCodeIso,
    corrName,
    corrAccount,
    purpose,
    inn,
    isFinal
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}
