/**
 * Справочник "Справочник бенефициаров"
 * @param  {Array} orgId          Id организации
 * @param  {number} offset        смещение
 * @param  {string} offsetStep    колличество элементов
 * @param  {string} sort          сортировка
 * @param  {boolean} desc         направление сортировки
 * @param  {boolean} signed
 * @param  {string} benefName     наименование бенефициара
 * @param  {string} benefAccount  счет бенефициара
 * @param  {string} benefCountryCode  код страны бенефициара
 * @param  {string} benefBankCountryCode  код страны банка бенефициара
 * @param  {string} iMediaBankSWIFT  swift
 * @param  {string} benefBankSWIFT   bank swift
 * @param  {string} paymentDetails   назначение платежа
 * @return {Object}
 */
export function findAllByQuery(
  {
    orgId,
    offset = 0,
    offsetStep = 31,
    sort,
    desc,
    signed,
    benefName,
    benefAccount,
    benefCountryCode,
    benefBankCountryCode,
    iMediaBankSWIFT,
    benefBankSWIFT,
    paymentDetails
  }) {
  const endpoint = {
    url: 'beneficiars'
  };

  const params = {
    query: {
      orgId,
      offset,
      offsetStep,
      sort,
      desc,
      signed,
      benefName,
      benefAccount,
      benefCountryCode,
      benefBankCountryCode,
      iMediaBankSWIFT,
      benefBankSWIFT,
      paymentDetails
    }
  };

  return { ...endpoint, params };
}

export function save(docData) {
  const endpoint = {
    url: 'beneficiars',
    method: 'POST'
  };

  const data = {
    ...docData
  };

  return { ...endpoint, data };
}

export function remove(docIds) {
  const endpoint = {
    url: `beneficiars?docId=${docIds.join('&docId=')}`,
    method: 'DELETE'
  };

  return { ...endpoint };
}

export function getById(docId) {
  const endpoint = {
    url: `beneficiars/${docId}`
  };

  return { ...endpoint };
}

// принимает все теже данные как и при сохранении
export function validate(docData) {
  const endpoint = {
    url: 'beneficiars/validation',
    method: 'POST'
  };

  const data = {
    ...docData
  };

  return { ...endpoint, data };
}
