export const findOneById = docId => ({ url: `curtransfers/${docId}` });

export const fetchCPaymentConstraints = () => ({ url: 'curtransfers/constraints?docId=CurrencyTransfer' });

/**
 * пример запроса
 *  * @param documentData =
 * {
  "docInfo" : {
      "status": "Создан",
      "docNumber": "115",
      "docDate": "2016-05-25T00:00:00+0300",
      "urgent": true,
      "payUntilDate": "2016-05-25T00:00:00+0300",
      "branchShortName": "ЗАО РАЙФФАЙЗЕНБАНК",
      "docNote": "Заметки",
      "paymentDetails": "34534534534535",
      "taxConfirmPayDoc": "Проплаченные налоги!",
      "residentType": "1",
      "serviceTypeCode": "51",
      "nameServiceCode": "Строительные работы за рубежом",
      "currControlInfo": "Доп инфо для валютного контроля",
      "bankAgreementText": "",
      "currDealInquiryNumber": "",
      "currDealInquiryDate": ""
    },
    "payer" : {
      "payerName": "ООО Крокодил Инвест",
      "payerINN": "1234567894",
      "payerOKPO": "1112223334",
      "payerAccount": "40801840444444444444",
      "payerNameInt": "OJSC Krokodil Invest",
      "payerAddress": "",
      "payerBankName": ""
    },
    "officials" : {
      "senderOfficials": "Кирилл",
      "phoneOfficials": "324234234234"
    },
    "moneyInfo" : {
      "currTransCode": "840",
      "amountTrans": 5000,
      "currTransISOCode": "USD",
      "multiCurr": false,
      "currOffCode": "840",
      "amountOff": 5000,
      "currOffISOCode": "USD"
    },
    "beneficiar" : {
      "benefAccount": "66666666666",
      "benefSWIFT": "AAALSARIJED",
      "benefName": "SAUDI HOLLANDI BANK",
      "benefAddress": "",
      "benefPlace": "JEDDAH",
      "benefCountryCode": "682",
      "benefCountryCode02": "SA",
      "benefCountryName": "САУДОВСКАЯ АРАВИЯ"
    },
    "benefBank" : {
      "benefBankSWIFT": "AAIBAEA1XXX",
      "beBankClearCodeShort": "",
      "beBankClearCode02": "",
      "beBankClearCode": "",
      "benefBankName": "ARAB AFRICAN INTERNATIONAL BANK",
      "benefBankFilialName": "",
      "benefBankCorrAccount": "4345345345345",
      "benefBankAddress": "ARAB MONETARY FUND BUILDING",
      "benefBankPlace": "ABU DHABI",
      "benefBankCountryCode": "784",
      "benefBankCountryCode02": "AE",
      "benefBankCountryName": "ОБЪЕДИНЕННЫЕ АРАБСКИЕ ЭМИРАТЫ",
      "beBankClearCountryCode02": ""
    },
    "iMediaBank" : {
      "iMediaBankSWIFT": "AABKDEF1XXX",
      "iMeBankClearCodeShort": "",
      "iMeBankClearCode02": "",
      "iMeBankClearCode": "",
      "iMediaBankName": "A AND A ACTIENBANK GMBH",
      "iMediaBankAddress": "IM TRUTZ FRANKFURT 55",
      "iMediaBankPlace": "FRANKFURT AM MAIN",
      "iMediaBankCountryCode": "276",
      "iMediaBankCountryCode02": "DE",
      "iMediaBankCountryName": "ГЕРМАНИЯ",
      "iMediaInfo": "345345345345",
      "iMeBankClearCountryCode02": ""
    },
    "chargesInfo" : {
      "chargesType": "OUR",
      "chargesTypeInfo": "все комиссии за Ваш счет",
      "chargesAccount": "40702810500001405640",
      "chargesBankBIC": "044525700",
      "chargesBankName": "ЗАО РАЙФФАЙЗЕНБАНК Г. МОСКВА"
    }
}
 */

export const save = (documentData) => {
  const { docInfo, payer, officials, moneyInfo, beneficiar, benefBank, iMediaBank, chargesInfo } = documentData;
  const endpoint = {
    url   : 'curtransfers',
    method: 'POST'
  };

  const data = {
    docInfo,
    payer,
    officials,
    moneyInfo,
    beneficiar,
    benefBank,
    iMediaBank,
    chargesInfo
  };

  return { ...endpoint, data };
};

export const validate = (documentData) => {
  const { docInfo, payer, officials, moneyInfo, beneficiar, benefBank, iMediaBank, chargesInfo } = documentData;
  const endpoint = {
    url   : 'curtransfers/validation',
    method: 'POST'
  };

  const data = {
    docInfo,
    payer,
    officials,
    moneyInfo,
    beneficiar,
    benefBank,
    iMediaBank,
    chargesInfo
  };

  return { ...endpoint, data };
};
