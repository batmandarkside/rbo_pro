/**
 * Поиск корреспондента по ID
 * @param   {String} docId ID
 * @returns {Object}
 */
export const findOneById = docId => ({ url: `correspondents/${docId}` });

/**
 * Поиск корреспондентов по критериям
 * @param   {Array}   orgId           ИД организаций
 * @param   {String}  requisite       поиск
 * @param   {Number}  offset          смещение выдачи (для постраничного вывода)
 * @param   {Number}  offsetStep      кол-во items в выдаче (для постраничного вывода)
 * @param   {String}  sort            Сортировка по полю
 * @param   {Boolean} desc            Направление сортировки
 * @param   {String}  receiverINN     ИНН
 * @param   {String}  receiverName    Наименование
 * @param   {String}  receiverKPP     КПП
 * @param   {String}  receiverAccount Счет
 * @param   {Boolean} signed          Заверен
 * @param   {String}  receiverBankBIC БИК
 * @param   {String}  paymentPurpose  Назначение платежа
 * @param   {String}  comment         Комментарий
 * @returns {Object}
 */
export function findAllByQuery(
  {
    orgId,
    requisite,
    offset = 0,
    offsetStep = 31,
    sort = 'account',
    desc,
    receiverINN,
    receiverName,
    receiverKPP,
    receiverAccount,
    signed,
    receiverBankBIC,
    paymentPurpose,
    comment
  }) {
  const endpoint = {
    url: 'correspondents'
  };
  const params = {
    query: {
      orgId,
      requisite,
      offset,
      offsetStep,
      sort,
      desc,
      receiverINN,
      receiverName,
      receiverKPP,
      receiverAccount,
      signed,
      receiverBankBIC,
      paymentPurpose,
      comment
    }
  };
  return { ...endpoint, params };
}

/**
 * Создание или обновление корреспондента
 * @param   {String} id              ИД документа
 * @param   {String} orgId           ИД организации
 * @param   {String} account         Счёт
 * @param   {String} receiverINN     ИНН
 * @param   {String} receiverKPP     КПП
 * @param   {String} receiverBankBIC БИК
 * @param   {String} receiverName    Наименование получателя
 * @param   {String} paymentPurpose  Назначение платежа
 * @param   {String} comment         Комментарий
 * @param   {String} signed          Подписан
 * @param   {String} orgName         Наименование организации
 * @returns {Object}
 */
export function update(
  {
    id,
    orgId,
    account,
    receiverINN,
    receiverKPP,
    receiverBankBIC,
    receiverName,
    paymentPurpose,
    comment,
    signed,
    orgName
  }) {
  const endpoint = {
    url   : 'correspondents',
    method: 'POST'
  };
  const data = {
    id,
    orgId,
    account,
    receiverINN,
    receiverKPP,
    receiverBankBIC,
    receiverName,
    paymentPurpose,
    comment,
    signed,
    orgName
  };
  return { ...endpoint, data };
}

/**
 * Валидация корреспондента
 * @param  {Object} data
 * @return {Object}
 */
export const validate = data => (
  {
    url: 'correspondents/validation',
    method: 'POST',
    data
  }
);

/**
 * Удаление корреспондентов
 * @param   {Array} docIds Идентификаторы документа
 * @returns {Object}
 */
export function deleteByIds(docIds) {
  const endpoint = {
    url   : `correspondents?${docIds.map(docId => `docId=${docId}`).join('&')}`,
    method: 'DELETE'
  };
  const data = {
    docId: docIds
  };
  return { ...endpoint, data };
}
