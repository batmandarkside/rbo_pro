/**
 * Поиск валютного платежа по ID
 * @param   {String} docId ID платежа
 * @returns {Object}
 */
export const findOneById = docId => ({ url: `curtransfers/${docId}` });

export function findDefaultCurTransferValue({ orgId }) {
  const data = { orgId };

  return {
    url: 'defaultValues/curtransfer',
    method: 'POST',
    data
  };
}

/**
 * Поиск платежей удовлетворяющих критерию с постраничным получением.
 * @param   {String|String[]}  orgId          Идентификатор организации
 * @param   {String}  section                 Секция платежей - Архив, Удаленные
 * @param   {Number}  [offset=0]              default: 0
 * @param   {Number}  [offsetStep=31]         default: 31
 * @param   {String}  sort                    Поле для сортировки. Возможные значения: number, date, sum, account.
 * @param   {Boolean} desc                    Флаг сортировки в обратном направлении
 * @param   {String|String[]}  status         Статусы
 * @param   {String}  currTransISOCode        ISO-код валюты перевода                                                   (не реализовано на момент написания комментария)
 * @param   {String}  account                 Номер счета перевододателя
 * @param   {String}  payerName               Перевододатель                                                            (не реализовано на момент написания комментария)
 * @param   {String}  dateFrom                Дата начала периода выборки документов в формате yyyy-MM-dd
 * @param   {String}  dateTo                  Дата окончания периода выборки документов в формате yyyy-MM-dd
 * @param   {Number}  sumMin                  Минимальная сумма для выборки документа (включительно)
 * @param   {Number}  sumMax                  Максимальная сумма для выборки документа(включительно)
 * @param   {String}  benefName               Наименование бенефициара
 * @param   {String}  benefAccount            Счет бенефициара
 * @param   {String}  paymentPurpose          Назначение платежв
 * @param   {String}  notes                   Заметки
 * @param   {Boolean} withRecall              ВП с отзывом
 * @param   {Boolean} multiCurr               мультивалютные ВП                                                         (не реализовано на момент написания комментария)
 * @param   {Boolean} withCC031               наличие связанных справок
 * @returns {Object}
 */

export const FILTERS = {
};
export function findAllByQuery(
  {
    orgId,
    section,
    offset = 0,
    offsetStep = 10,
    sort,
    desc,
    status,
    currTransISOCode,
    account,
    payerName,
    dateFrom,
    dateTo,
    sumMin,
    sumMax,
    benefName,
    benefAccount,
    paymentPurpose,
    notes,
    withRecall,
    multiCurr
  }) {
  const url = `curtransfers${section ? `/${section}` : ''}`;
  const endpoint = { url };
  const params = {
    query: {
      orgId,
      offset,
      offsetStep,
      sort,
      desc,
      status,
      currTransISOCode,
      account,
      payerName,
      dateFrom,
      dateTo,
      sumMin,
      sumMax,
      benefName,
      benefAccount,
      paymentPurpose,
      notes,
      withRecall,
      multiCurr
    }
  };
  return { ...endpoint, params };
}

export function saveAsTemplate({docId, name}) {
  const endpoint = {
    url   : 'curtransfertemplates/saveByDocument',
    method: 'POST'
  };
  const data = {
    i: docId,
    name
  };

  return { ...endpoint, data };
}
