import { removeEmptyParams } from './utils';

/**
 * Справочник контрагентов
 * @param  {Object}  config
 * @param  {Number}  [config.offset=0] Смещение в элементах
 * @param  {String}  config.name       Часть наименования корреспондента
 * @param  {Boolean} config.isApproved Признак заверенного корреспондента
 * @param  {String}  config.orgId      Идентификатор организации
 * @return {Object}
 */
export function findPartners({ offset = 0, name, isApproved, orgId }) {
  const endpoint = {
    url: 'dicts/partners'
  };
  const params = {
    offset,
    name,
    isApproved,
    orgId
  };

  return { ...endpoint, params };
}

/**
 * Справочник "Виды валютных операций"
 * @param  {Object} config
 * @param  {Number} [config.offset=0] Смещение в элементах
 * @param  {string} config.code       Код вида операции валютного документа
 * @return {Object}
 */
export function findCurrencyOperationTypes({ offset = 0, code }) {
  const endpoint = {
    url: 'dicts/currencyOperationTypes'
  };
  const params = {
    offset,
    code
  };

  return { ...endpoint, params };
}

/**
 * Справочник "Показатели статуса налогоплательщика"
 * @param  {Number} [offset=0] Смещение в элементах
 * @return {Object}
 */
export function findDrawerStatuses(offset = 0) {
  const endpoint = {
    url: 'dicts/drawerStatuses'
  };
  const params = {
    offset
  };

  return { ...endpoint, params };
}

/**
 * Коды бюджетной классификации
 * @param  {Object} config
 * @param  {Number} config.offset Смещение в элементах
 * @param  {string} config.code   Часть кода бюджетной классификации
 * @return {Object}
 */
export function findCBC({ offset = 0, code }) {
  const endpoint = {
    url: 'dicts/CBC'
  };
  const params = {
    offset,
    code
  };

  return { ...endpoint, params };
}

/**
 * Справочник "БИК банков"
 * @param  {Object} config
 * @param  {Number} [config.offset=0] Смещение в элементах
 * @param  {string} config.code       Часть кода БИК
 * @return {Object}
 */
export function findBIC({ offset = 0, code }) {
  const endpoint = {
    url: 'dicts/BIC'
  };
  const params = {
    offset,
    code
  };

  return { ...endpoint, params };
}

/**
 * Справочник "Способы отправки платежа"
 * @param  {string|array} orgId Идентификатор организации
 * @return {Object}
 */
export function findPaymentKinds(orgId) {
  const endpoint = {
    url: 'dicts/paymentKinds'
  };
  const params = {
    orgId
  };

  return { ...endpoint, params };
}

/**
 * Справочник "Типы Срочности платежа"
 * @return {Object}
 */
export const findPaymentPriorities = () => ({
  url: 'dicts/paymentPriorities'
});

/**
 * Типы расчета НДС
 * @return {Object}
 */
export const findVatCalculationRules = () => ({
  url: 'dicts/vatCalculationRules'
});

/**
 * Справочник "КПП организации"
 * @param  {string} orgId Идентификатор организации
 * @return {Object}
 */
export function findCustomerKpps(orgId) {
  const endpoint = {
    url: 'dicts/customerKpps'
  };
  const params = {
    orgId
  };

  return { ...endpoint, params };
}

/**
 * Справочник "Основания платежа"
 * @return {Object}
 */
export const findPayReasons = () => ({
  url: 'dicts/payReasons'
});

/**
 * Справочник "Счета организации"
 * @param  {Object} config
 * @param  {string} config.orgId Идентификатор организации
 * @param  {string} config.field Имя поля
 * @return {Object}
 */
export function findAccounts({ orgId, field }) {
  const endpoint = {
    url: 'dicts/accounts'
  };
  const params = {
    orgId,
    field
  };

  return { ...endpoint, params };
}

/**
 * Справочник "Показатели типа платежа"
 * @return {Object}
 */
export const findChargeTypes = () => ({
  url: 'dicts/chargeTypes'
});

/**
 * Справочник "Показатели налогового периода"
 * @return {Object}
 */
export const findTaxPeriods = () => ({
  url: 'dicts/taxPeriods'
});

/**
 * Справочник "Справочник тем, получателей и шаблонов писем в банк"
 * @return {Object}
 */
export const findReceiverTopics = () => ({
  url: 'dicts/receiverTopics'
});

/**
 * Справочник "Справочник кодов валют"
 * @param  {string} code    Код валюты
 * @param  {string} isoCode ISO код валюты
 * @param  {string} name    Имя кода валюты
 * @param  {string} every   Искать везде
 * @return {Object}
 */
export function findCurrCodes({ code, isoCode, name, every }) {
  const endpoint = {
    url: 'dicts/currCodes'
  };
  const params = {
    code,
    isoCode,
    name,
    every
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}

export const findClearingCodes = params => ({
  url: 'dicts/clearingCodes',
  params: removeEmptyParams(params)
});

export const findOfficials = params => ({ url: 'dicts/officials', params });

export const findPaymentPurposes = ({ offset = 0, orgId, purpose }) => {
  const endpoint = {
    url: 'dicts/paymentPurposes',
  };

  const params = {
    offset,
    orgId,
    purpose
  };

  return {
    ...endpoint,
    params: removeEmptyParams(params)
  };
};

export const findForeignBanks = (params = {}) => ({
  url: 'dicts/foreignBanks',
  params: removeEmptyParams(params)
});

export const findComissionTypes = () => ({ url: 'dicts/comissionTypes' });

export function findCountries(params = {}) {
  Object.keys(params).forEach(key => { if (!params[key]) delete params[key]; });
  return ({ url: 'dicts/countries', params });
}

export function findCurrencyOperationCodes(params = {}) {
  Object.keys(params).forEach(key => { if (!params[key]) delete params[key]; });
  return ({ url: 'dicts/currencyOperationTypes', params });
}

export const findDealPassports = params => ({
  url: 'dicts/dealPassports',
  params: removeEmptyParams(params)
});

export const findContractTypes = () => ({ url: 'dicts/contractTypes' });

export const findResidentAttributes = () => ({ url: 'dicts/residentAttributes' });
