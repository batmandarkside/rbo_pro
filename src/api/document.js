/**
 * История статусов документа
 * @param  {string} docId Идентификатор документа
 * @return {Object}
 */
export function stateHistoryById(docId) {
  const endpoint = {
    url: 'documents/stateHistory'
  };
  const params = {
    docId
  };
  return { ...endpoint, params };
}

/**
 * Проверить подпись документа
 * @param  {string} docId Идентификатор документа
 * @return {Object}
 */
export function signsCheckById(docId) {
  const endpoint = {
    url: 'documents/signsCheck'
  };
  const params = {
    docId
  };
  return { ...endpoint, params };
}

/**
 * Удалить документ
 * @param  {array} docIds Идентификатор документа
 * @return {Object}
 */
export function deleteById(docIds) {
  const query = docIds.map(id => `docId=${id}`).join('&');
  const endpoint = {
    url: `documents?${query}`,
    method: 'DELETE'
  };
  const data = {
    docId: docIds
  };
  return { ...endpoint, data };
}

/**
 * Отправить документ в архив
 * @param  {String[]} docIds Идентификатор документа
 * @return {Object}
 */
export function toArchiveByIds(docIds) {
  const endpoint = {
    url: 'documents/archivation',
    method: 'POST'
  };
  const data = {
    docId: docIds
  };
  return { ...endpoint, data };
}

/**
 * Удаление архивного документа
 * @param  {String[]} docIds Идентификатор документа
 * @return {Object}
 */
export function deleteArchiveByIds(docIds) {
  const query = docIds.map(id => `docId=${id}`).join('&');
  const endpoint = {
    url: `documents/archivation?${query}`,
    method: 'DELETE'
  };
  const data = {
    docId: docIds
  };
  return { ...endpoint, data };
}



/**
 *
 * @param docId
 * @returns {{params: {docId: *}}}
 */
export function sendDocumentToBank({ docId }) {
  const endpoint = {
    url: 'rpays/send321',
  };

  const params = {
    docId
  };

  return {
    ...endpoint,
    params
  };
}
