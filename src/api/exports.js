/**
 * Экспорт входящих писем
 * @param  {[String]} orgId           Идентификатор организации
 * @param  {String}   sort            Поле для сортировки
 * @param  {Boolean}  desc            Флаг сортировки в обратном направлении
 * @param  {String}   from            Дата начала периода выборки в формате yyyy-MM-dd (включительно)
 * @param  {String}   to              Дата окончания периода выборки в формате yyyy-MM-dd (включительно)
 * @param  {String}   topic           Тема письма
 * @param  {String}   receiver        Получатель
 * @param  {String}   sender          Отправитель
 * @param  {String}   text            Текст письма
 * @param  {String}   account         Номер счета
 * @param  {Boolean}  isRead          Статус прочтения
 * @param  {Boolean}  mustRead        Должно быть прочитано
 * @param  {String}   visibleColumns  Столбцы для экспорта
 * @return {Object}
 */
export function createExportIncomingMessagesList(
  {
    orgId,
    sort,
    desc,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    isRead,
    mustRead,
    visibleColumns
  }) {
  const endpoint = {
    url: 'messages/income/exportScroller',
    responseType: 'blob'
  };
  const params = {
    orgId,
    sort,
    desc,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    isRead,
    mustRead,
    visibleColumns
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}

/**
 * Экспорт исходящих писем
 * @param {String}    section       Секция документов:
 *                                    archived  Архивные,
 *                                    deleted   Удаленные
 * @param  {String}  orgId          Идентификатор организации
 * @param  {String}  sort           Поле для сортировки
 * @param  {Boolean} desc           Флаг сортировки в обратном направлении
 * @param  {String}  from           Дата начала периода выборки в формате yyyy-MM-dd (включительно)
 * @param  {String}  to             Дата окончания периода выборки в формате yyyy-MM-dd (включительно)
 * @param  {String}  topic          Тема письма
 * @param  {String}  receiver       Получатель
 * @param  {String}  sender         Отправитель
 * @param  {String}  text           Текст письма
 * @param  {String}  account        Номер счета
 * @param  {String}  status         Статусы
 * @param  {String}  visibleColumns Столбцы для экспорта
 * @return {Object}
 */
export function createExportOutgoingMessagesList(
  {
    section,
    orgId,
    sort,
    desc,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    status,
    visibleColumns
  }
) {
  const url = `messages/outcome/exportScroller${section ? `${section[0].toUpperCase()}${section.slice(1)}` : ''}`;

  const endpoint = {
    url,
    responseType: 'blob'
  };

  const params = {
    orgId,
    sort,
    desc,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    status,
    visibleColumns
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}

/**
 * Экспорт рабочих документов
 * @param {String}    section                Секция документов:
 *                                              archived  Архивные,
 *                                              deleted   Удаленные
 * @param  {String}  orgId                   Идентификатор организации
 * @param  {String}  sort                    Поле для сортировки. Возможные значения: number, date, sum, account.
 * @param  {Boolean} desc                    Флаг сортировки в обратном направлении
 * @param  {String}  status                  Статусы
 * @param  {String}  account                 Номер счета
 * @param  {String}  receiver                Получатель
 * @param  {String}  dateFrom                Дата начала периода выборки документов в формате yyyy-MM-dd
 * @param  {String}  dateTo                  Дата окончания периода выборки документов в формате yyyy-MM-dd
 * @param  {Number}  sumMin                  Минимальная сумма для выборки документа (включительно)
 * @param  {Number}  sumMax                  Максимальная сумма для выборки документа(включительно)
 * @param  {String}  payerName               Плательщик
 * @param  {String}  receiverINN             ИНН получателя
 * @param  {String}  receiverAccount         Счет получателя
 * @param  {String}  paymentPurpose          Назначение платежа
 * @param  {String}  notes                   Заметки
 * @param  {Boolean} withRecall              РПП с отзывом
 * @param  {String}  importId                id импорта
 * @param  {String}  lastStateChangeDateFrom Начало даты последнего изменения статуса
 * @param  {String}  lastStateChangeDateTo   Конец даты последнего изменения статуса
 * @param  {String}  visibleColumns          Столбцы для экспорта
 * @return {Object}
 */
export function createExportRpaysList(
  {
    section,
    orgId,
    sort,
    desc,
    status,
    account,
    receiver,
    dateFrom,
    dateTo,
    sumMin,
    sumMax,
    payerName,
    receiverINN,
    receiverAccount,
    paymentPurpose,
    notes,
    withRecall,
    importId,
    lastStateChangeDateFrom,
    lastStateChangeDateTo,
    visibleColumns
  }
) {
  const url = `rpays/exportScroller${section ? `${section[0].toUpperCase()}${section.slice(1)}` : ''}`;

  const endpoint = {
    url,
    responseType: 'blob'
  };

  const params = {
    query: {
      orgId,
      sort,
      desc,
      status,
      account,
      receiver,
      dateFrom,
      dateTo,
      sumMin,
      sumMax,
      payerName,
      receiverINN,
      receiverAccount,
      paymentPurpose,
      notes,
      withRecall,
      importId,
      lastStateChangeDateFrom,
      lastStateChangeDateTo,
      visibleColumns
    }
  };

  return { ...endpoint, params };
}

/**
 * Экспорт движений по счёту
 * @param  {String}  orgId          Идентификатор организации
 * @param  {String}  accId          Id счета
 * @param  {String}  account        Номер счета
 * @param  {String}  sort           Поле для сортировки
 * @param  {Boolean} desc           Флаг сортировки в обратном направлении
 * @param  {String}  from           Начальная дата движений по счету (включительно)
 * @param  {String}  to             Конечная дата движений по счету (включительно)
 * @param  {String}  stmtDateFrom   Начальная дата выписки
 * @param  {String}  stmtDateTo     Конечная дата выписки
 * @param  {String}  operType       Тип операции: 0 – Дебет; 1 – Кредит. При отсутствии в фильтр попадает и дебет, и кредит
 * @param  {String}  operNumber     Номер операции
 * @param  {Number}  sumMin         Минимальная сумма для выборки документа (включительно)
 * @param  {Number}  sumMax         Максимальная сумма для выборки документа (включительно)
 * @param  {String}  currCodeIso    Валюта счета
 * @param  {String}  corrName       Наименование контрагента
 * @param  {String}  corrAccount    Счет контрагента
 * @param  {String}  purpose        Назначение платежа
 * @param  {String}  inn            ИНН контрагента
 * @param  {Boolean} isFinal        Признак того, финальная операция или внутридневная
 * @param  {String}  visibleColumns Столбцы для экспорта
 * @return {Object}
 */
export function createAccountOperations(
  {
    orgId,
    accId,
    account,
    sort,
    desc,
    from,
    to,
    stmtDateFrom,
    stmtDateTo,
    operType,
    operNumber,
    sumMin,
    sumMax,
    currCodeIso,
    corrName,
    corrAccount,
    purpose,
    inn,
    isFinal,
    visibleColumns
  }) {
  const endpoint = {
    url: 'accounts/exportScrollerOperations',
    responseType: 'blob'
  };
  const params = {
    orgId,
    accId,
    account,
    sort,
    desc,
    from,
    to,
    stmtDateFrom,
    stmtDateTo,
    operType,
    operNumber,
    sumMin,
    sumMax,
    currCodeIso,
    corrName,
    corrAccount,
    purpose,
    inn,
    isFinal,
    visibleColumns
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}

/**
 * Подготовка для печати в определенный формат
 * @param  {Object}  config
 * @param  {String}  config.docId   Идентификатор документа
 * @param  {boolean} config.detail  Флаг выписки c движениями
 * @param  {String}  config.format  Формат документа
 * @param  {String}  config.date    Дата выписки
 * @param  {String}  config.account Счет
 * @return {Object}
 */
export function createStatementByQuery({ docId, detail = false, format, date, account }) {
  const endpoint = {
    url: 'statements/format',
    responseType: 'blob'
  };
  const params = {
    docId,
    detail,
    format,
    date,
    account
  };
  return { ...endpoint, params };
}

/**
 * Подготовка для экспорта в 1С
 * @param  {Object}  config
 * @param  {String}  config.docId   Идентификатор документа
 * @return {Object}
 */
export function exportStatement1C({ docId }) {
  const endpoint = {
    url: 'statements/export1C',
    responseType: 'blob'
  };
  const params = {
    docId
  };
  return { ...endpoint, params };
}

/**
 * Экспорт движений по счёту удовлетворяющих критериям
 * @param  {String}  orgId           Идентификатор организации
 * @param  {Number}  offset=0        Смещение в элементах
 * @param  {Number}  offsetStep=31   Количество записей
 * @param  {String}  sort            Поле для сортировки. Возможные значения: stateDate, recieptDate, account, id, number
 * @param  {Boolean} desc            Флаг сортировки в обратном направлении
 * @param  {String}  account         Список счетов
 * @param  {String}  from            Начало перода запроса
 * @param  {String}  to              Конец периода запроса
 * @param  {Number}  stmtType        Тип выписки. 0 - итоговая. 1 - промежуточная.
 * @param  {Boolean} zero            Включать в выборку нулевые выписки
 * @param  {String}  corrName        Наименование контрагента
 * @param  {String}  corrAccount     Счет контрагента
 * @param  {String}  currCodeIso     ISO Код валюты счета
 * @param  {String}  visibleColumns  Столбцы для экспорта
 * @return {Object}
 */
export function createStatementsByQuery(
  {
    orgId,
    offset = 0,
    offsetStep = 31,
    sort,
    desc,
    account,
    from,
    to,
    stmtType,
    zero,
    corrName,
    corrAccount,
    currCodeIso,
    visibleColumns
  }) {
  const endpoint = {
    url: 'statements/exportScroller',
    responseType: 'blob'
  };
  const params = {
    orgId,
    offset,
    offsetStep,
    sort,
    desc,
    account,
    from,
    to,
    stmtType,
    zero,
    corrName,
    corrAccount,
    currCodeIso,
    visibleColumns
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}

/**
 * Подготовка для печати операции в определенный формат
 * @param  {Object} config
 * @param  {String} config.docId  Идентификатор вложения
 * @param  {String} config.format Формат документа
 * @return {Object}
 */
export function createOperationById({ docId, format }) {
  const endpoint = {
    url: 'statements/operationsFormat',
    responseType: 'blob'
  };
  const params = {
    docId,
    format,
  };
  return { ...endpoint, params };
}

/**
 * Подготовка для печати документа в определенный формат
 * @param  {Object} config
 * @param  {String} config.docId  Идентификатор вложения
 * @param  {String} config.format Формат документа
 * @return {Object}
 */
export function createDocumentById({ docId, format }) {
  const endpoint = {
    url: 'documents/format',
    responseType: 'blob'
  };
  const params = {
    docId,
    format
  };
  return { ...endpoint, params };
}
