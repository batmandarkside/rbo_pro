/**
 * Поиск импорта по ID
 * @param   {String} docId ID платежа
 * @returns {Object}
 */
export const findOneById = docId => ({ url: `rpays/import/${docId}` });

/**
 * Поиск имопрта по критериям
 * @param   {Number} offset     Смещение
 * @param   {Number} offsetStep Количество
 * @param   {String} sort       Сортировка по полю
 * @param   {String} desc       Напраление сортировки
 * @param   {String} dateFrom   Дата от
 * @param   {String} dateTo     Дата до
 * @param   {Array}  status     Статусы
 * @param   {String}  format    Формат импорта
 * @param   {String}  search    Поисковая строка
 * @returns {Object}
 */
export function findAllByQuery({
  offset = 0,
  offsetStep = 31,
  sort = 'started',
  desc = true,
  dateFrom,
  dateTo,
  status,
  format = '1C',
  search
}) {
  const endpoint = {
    url: 'rpays/imports'
  };

  const params = {
    query: {
      offset,
      offsetStep,
      sort,
      desc,
      dateFrom,
      dateTo,
      status,
      format,
      search
    }
  };

  return { ...endpoint, params };
}

/**
 * Поиск не имопртированных документов
 * @param   {Object} config
 * @param   {String} config.docId
 * @param   {Number} config.offset
 * @param   {Number} config.offsetStep
 * @returns {Object}
 */
export function findErrorsImport({ docId, offset = 0, offsetStep = 31 }) {
  const endpoint = {
    url: 'rpays/import/errors'
  };

  const params = {
    query: JSON.stringify({
      docId,
      offset,
      offsetStep
    })
  };

  return { ...endpoint, params };
}

/**
 * Создание задания на импорт
 * @param   {Object} config
 * @param   {String} config.fileName
 * @param   {String} config.content
 * @param   {Object} config.settings
 * @returns {Object}
 */
export function createTask({ fileName, content, settings }) {
  const endpoint = {
    url: 'rpays/import',
    method: 'POST'
  };

  const data = {
    fileName,
    content,
    ...settings
  };

  return { ...endpoint, data };
}
