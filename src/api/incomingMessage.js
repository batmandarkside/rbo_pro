/**
 * Получить входящее письмо по идентификатору
 * @param  {string} id Идентификатор входящего письма
 * @return {Object}
 */
export const findById = id => ({
  url   : `messages/income/${id}`,
});

/**
 * Найти все письма удовлетворяющие условиям
 * @param {[string]}  orgId             Идентификаторы организаций
 * @param {Number}    offset=0          Смещение
 * @param {Number}    offsetStep=10     Количество записей
 * @param {string}    sort='date'       Поле для сортировки
 * @param {Boolean}   desc=true         Флаг сортировки в обратном направлении
 * @param {string}    from              Дата начала периода выборки в формате yyyy-MM-dd (включительно)
 * @param {string}    to                Дата окончания периода выборки в формате yyyy-MM-dd (включительно)
 * @param {string}    topic             Тема письма
 * @param {string}    receiver          Получатель
 * @param {string}    sender            Отправитель
 * @param {string}    text              Текст письма
 * @param {string}    account           Номер счета
 * @param {Boolean}   isRead            Статус прочтения
 * @param {Boolean}   mustRead          Должно быть прочитано
 * @return {Object}
 */
export function findAllByQuery(
  {
    orgId,
    offset = 0,
    offsetStep = 10,
    sort = 'date',
    desc = true,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    isRead,
    mustRead
  }) {
  const url = 'messages/income';
  const endpoint = { url };

  const params = {
    orgId,
    offset,
    offsetStep,
    sort,
    desc,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    isRead,
    mustRead
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}

/**
 * [findAllImportant description]
 * @param  {Object}  config
 * @param  {String}  config.orgId           Идентификатор организации
 * @param  {Number}  [config.offset=0]      Смещение
 * @param  {Number}  [config.offsetStep=31] Количество записей
 * @param  {Boolean} config.desc            Флаг сортировки в обратном направлении
 * @param  {string}  config.sort            Поле для сортировки
 * @return {Object}
 */
export function findAllImportant({ orgId, offset = 0, offsetStep = 10, sort, desc }) {
  const endpoint = {
    url: 'messages/income/important'
  };
  const params = {
    orgId,
    offset,
    offsetStep,
    sort,
    desc
  };
  return { ...endpoint, params };
}

/**
 * Получить информацию по вложению в письме
 * @param  {string} docId Идентификатор вложения
 * @return {Object}
 */
export function findAttachmentsInfoById(docId) {
  const endpoint = {
    url: 'messages/income/attachments/info'
  };
  const params = {
    docId
  };
  return { ...endpoint, params };
}

/**
 *
 * @param id
 * @returns {{params: *}}
 */
export function findAttachmentsById(id) {
  const endpoint = {
    url: `attachments/${id}`,
    responseType: 'blob'
  };
  return { ...endpoint };
}
