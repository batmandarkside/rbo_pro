import { axiosConfig } from 'config/application/axios';
import * as accounts from './accounts';
import * as authentication from './authentication';
import * as correspondent from './correspondent';
import * as dictionary from './dictionary';
import * as document from './document';
import * as exports from './exports';
import * as imports from './import';
import * as incomingMessage from './incomingMessage';
import * as organization from './organization';
import * as outgoingMessage from './outgoingMessage';
import * as recall from './recall';
import * as rpay from './rpay';
import * as settings from './settings';
import * as signature from './signature';
import * as statement from './statement';
import * as user from './user';
import * as validation from './validation';
import * as cPayment from './c-payment';
import * as beneficiaries from './beneficiaries';
import * as curtransfers from './curtransfers';

const packages = {
  accounts,
  authentication,
  correspondent,
  dictionary,
  document,
  exports,
  imports,
  incomingMessage,
  organization,
  outgoingMessage,
  recall,
  rpay,
  settings,
  signature,
  statement,
  user,
  validation,
  cPayment,
  beneficiaries,
  curtransfers
};

function axios() {
  return axiosConfig.getAxiosInstance();
}

function createRequest(request) {
  return function (params) {
    const config = request(params);
    const instanceAxios = axios();
    return instanceAxios(config);
  };
}

function createMethods(reqRespObj) {
  const methods = {};
  Object.keys(reqRespObj).forEach((methodName) => {
    methods[methodName] = createRequest(reqRespObj[methodName]);
  });
  return methods;
}

function wrapAllByCreateMethods(obj){
  const ret = {};
  Object.keys(obj).forEach((packageName) => {
    ret[packageName] = createMethods(obj[packageName]);
  });
  return ret;
}

const methodsForBackendCall = wrapAllByCreateMethods(packages);

export default methodsForBackendCall;
