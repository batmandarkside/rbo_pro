/**
 * Created by drune on 27/02/2017.
 */
import getCorrespondents from './getCorrespondents.json';

const headers = {
  'content-type': 'application/json',
};

function profile(mock) {
  mock.onGet('correspondents').reply(200, getCorrespondents, headers);
}

export default profile;
