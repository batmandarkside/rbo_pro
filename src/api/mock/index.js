/**
 * Created by drune on 27/02/2017.
 */
import MockAdapter from 'axios-mock-adapter';

import profile from './profile';
import organizations from './organizations';
import rpay from './rpay';
import messages from './messages';
import statements from './statements';
import signature from './signature';
import correspondents from './correspondents';

export function configureMocks(axiosInstance) {
  const mock = new MockAdapter(axiosInstance);

  profile(mock);
  organizations(mock);
  rpay(mock);
  messages(mock);
  statements(mock);
  signature(mock);
  correspondents(mock);
}
