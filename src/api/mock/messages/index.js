/**
 * Created by drune on 16/02/2017.
 */
import incomingMessages from './incoming-messages.json';
import outgoingMessages from './outgoing-messages.json';
import attachInfo from './attach-info.json';
import attach from './ticket_attach.json';

const headers = {
  'content-type': 'application/json',
};

function isImportant({ mustRead, read }) {
  return mustRead === '1' && read === '0';
}

function addImportantMessagesEndpoint(mock) {
  const importantMessages = incomingMessages.filter(isImportant);
  mock.onGet('messages/income/important').reply(200, importantMessages, headers);
}

function addIncomingMessagesEndpoint(mock) {
  mock.onGet('messages/income').reply(200, incomingMessages, headers);
}

function addOutgoingMessagesEndpoint(mock) {
  mock.onGet('messages/outcome').reply(200, outgoingMessages, headers);
}

function addOutgoingArchiveMessagesEndpoint(mock) {
  mock.onGet('messages/outcome/archived').reply(200, outgoingMessages, headers);
}

function addOutgoingTrashMessagesEndpoint(mock) {
  mock.onGet('messages/outcome/deleted').reply(200, outgoingMessages, headers);
}

function getAttachInfo(mock) {
  mock.onGet('messages/income/attachments/info').reply(200, attachInfo, headers);
}

function getAttach(mock) {
  mock.onGet('attachments/5070cea1-1956-45dc-97ab-52b8460bb15a').reply(200, attach, headers);
}

export default function addMessagesEndpoints(mock) {
  addImportantMessagesEndpoint(mock);
  addIncomingMessagesEndpoint(mock);
  addOutgoingMessagesEndpoint(mock);
  addOutgoingArchiveMessagesEndpoint(mock);
  addOutgoingTrashMessagesEndpoint(mock);
  getAttachInfo(mock);
  getAttach(mock);
}
