import getUserOrganization from './getUserOrganization.json';

const headers = {
  'content-type': 'application/json'
};

function organizations(mock) {
  mock.onGet('user/organizations').reply(200, getUserOrganization, headers);
}

export default organizations;
