import getUser from './getUser.json';
import getAccounts from './accounts-response.json';
import accountMovements from './account-movements.json';
import getUserSettings from './getUserSettings.json';

const headers = {
  'content-type': 'application/json'
};

function profile(mock) {
  mock.onGet('user').reply(200, getUser, headers);
  mock.onGet('accounts').reply(200, getAccounts, headers);
  mock.onGet('accounts/movements').reply(200, accountMovements, headers);

  mock.onGet('user/settings').reply(200, getUserSettings, headers);
  mock.onPut('user/settings').reply(200, getUserSettings, headers);
}

export default profile;
