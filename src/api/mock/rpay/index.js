/**
 * Created by drune on 27/02/2017.
 */
import getRpay from './getRpay.json';
import getRpayStatuses from './getRpayStatuses.json';

const headers = {
  'content-type': 'application/json',
};

const getRPaymentsCount = (config) => {
  const { params: { query = {} } } = config;
  const result = getRpay.filter(item => !!query.status.find(s => item.status === s)).length;
  return [200, result, headers];
};

function rpay(mock) {
  mock.onGet('rpays').reply(200, getRpay, headers);
  mock.onGet('rpays/statuses').reply(200, getRpayStatuses, headers);
  mock.onGet('rpays/count').reply(getRPaymentsCount);
}

export default rpay;
