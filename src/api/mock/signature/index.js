import certList from './cert-list.json';
import getUlk from './ulk.json';

const headers = {
  'content-type': 'application/json'
};

function addSignatureEndpoint(mock) {
  mock.onGet('certs').reply(200, certList, headers);
  mock.onGet('defaultValues/certReq').reply(200, getUlk, headers);
}

export default function addSignatureMainEndpoints(mock) {
  addSignatureEndpoint(mock);
}
