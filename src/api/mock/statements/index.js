import statements from './statements.json';

const headers = {
  'content-type': 'application/json',
};


function addStatementsEndpoint(mock) {
  mock.onGet('statements').reply(200, statements, headers);
}

export default function addStatementMainEndpoints(mock) {
  addStatementsEndpoint(mock);
}
