/**
 * Найти все организации пользователя
 * @return {Object}
 */
export const findAllForCurrentUser = () => ({
  url   : 'user/organizations',
});
