/**
 * Найти исходящее письмо по идентификатору
 * @param  {String} id Идентификатор вложения
 * @return {Object}
 */
export const findOneById = id => ({
  url   : `/messages/outcome/${id}`,
});

/**
 * Найти все исходящие письма удовлетворяющие условиям
 * @param {String}    section           Секция писем:
 *                                      archived  Архивные,
 *                                      deleted   Удаленные
 * @param {[string]}  orgId             Идентификаторы организаций
 * @param {Number}    offset=0          Смещение
 * @param {Number}    offsetStep=10     Количество записей
 * @param {string}    sort='date'       Поле для сортировки
 * @param {Boolean}   desc=true         Флаг сортировки в обратном направлении
 * @param {string}    from              Дата начала периода выборки в формате yyyy-MM-dd (включительно)
 * @param {string}    to                Дата окончания периода выборки в формате yyyy-MM-dd (включительно)
 * @param {string}    topic             Тема письма
 * @param {string}    receiver          Получатель
 * @param {string}    sender            Отправитель
 * @param {string}    text              Текст письма
 * @param {string}    account           Номер счета
 * @param {Boolean}   status            Статусы
 * @return {Object}
 */
export function findAllByQuery(
  {
    section,
    orgId,
    offset = 0,
    offsetStep = 10,
    sort = 'date',
    desc = true,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    status
  }) {
  const url = `messages/outcome${section && `/${section}`}`;
  const endpoint = { url };

  const params = {
    orgId,
    offset,
    offsetStep,
    sort,
    desc,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    status
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}

/**
 * Сохранение письма
 * @param  {Object}   config
 * @param  {String}   config.docId             Идентификатор документа
 * @param  {Number}   config.docNumber         Номер документа
 * @param  {String}   config.docDate           Например 2016-01-27T00:00:00+0300
 * @param  {String}   config.account           Счет
 * @param  {String}   config.topic             Тема письма
 * @param  {String}   config.branchId          @todo:[description]
 * @param  {String}   config.branchName        @todo:[description]
 * @param  {String}   config.receiver          Получатель
 * @param  {String}   config.orgId             Идентификатор организации
 * @param  {String}   config.message           Сообщение
 * @param  {Object[]} config.attaches
 * @param  {String}   config.attaches.name     Имя файла
 * @param  {String}   config.attaches.mimeType MIME Type файла
 * @param  {String}   config.attaches.content  Содержимое файла в base64
 * @param  {String}   config.attaches.comment  Комментарий к файлу
 * @return {Object}
 */
export function update({
 orgId,
 docId,
 docDate,
 docNumber,
 orgName,
 topic,
 receiver,
 message,
 account,
 branchId,
 branchName,
 attaches
}) {
  const endpoint = {
    url: 'messages/outcome',
    method: 'POST'
  };

  const formattedAttaches = attaches.map(attach => ({
    name: attach.name,
    mimeType: attach.mimeType,
    content: attach.content,
    comment: attach.comment
  }));

  return {
    ...endpoint,
    data: {
      orgId,
      docId,
      docDate,
      docNumber,
      orgName,
      topic,
      receiver,
      message,
      account,
      branchId,
      branchName,
      attaches: formattedAttaches
    }
  };
}

/**
 * Найти все статусы исходящих сообщений
 * @return {Object}
 */
export const findAllStatuses = () => ({
  url   : 'messages/outcome/statuses',
});

/**
 * Получить значение не прочитаных сообщений
 * @return {Object}
 */
export const countUnreaded = () => ({
  url   : 'messages/outcome/unread',
});

/**
 * Отправить исходящее сообщение по идентификатору
 * @param  {String} id Идентификатор документа
 * @return {Object}
 */
export function sendMessageWithId(id) {
  const endpoint = {
    url: 'messages/outcome/send'
  };
  const params = {
    docId: id
  };
  return { ...endpoint, params };
}

/**
 * Создать копию исходящего письма по идентификатору
 * @param  {String} id Идентификатор документа
 * @return {Object}
 */
export function createCopyByMessageId(id) {
  const endpoint = {
    url: 'messages/outcome/copy',
    method: 'POST'
  };
  const data = {
    docId: id
  };
  return { ...endpoint, data };
}

/**
 * Получить значения для нового письма
 * @param  {String} id Идентификатор организации
 * @return {Object}
 */
export function findDefaultValuesByOrganisationId(id) {
  const endpoint = {
    url: 'defaultValues/message',
    method: 'POST'
  };
  const data = {
    orgId: id
  };
  return { ...endpoint, data };
}

/**
 * Валидация исходящего пиьсма
 * @param  {Object} data
 * @return {Object}
 */
export function validateDocument(
  {
    orgId,
    docId,
    docDate,
    docNumber,
    orgName,
    topic,
    receiver,
    message,
    account,
    branchId,
    branchName,
    attaches
  }
) {
  const endpoint = {
    url: 'messages/outcome/validation',
    method: 'POST'
  };
  const formattedAttaches = attaches.map(attach => ({
    name: attach.name,
    mimeType: attach.mimeType,
    content: attach.content,
    comment: attach.comment
  }));
  return {
    ...endpoint,
    data: {
      orgId,
      docId,
      docDate,
      docNumber,
      orgName,
      topic,
      receiver,
      message,
      account,
      branchId,
      branchName,
      attaches: formattedAttaches
    }
  };
}

/**
 * Получение информации по вложению для письма в банк (исходящее)
 * @param  {string} docId - Идентификатор письма в банк
 * @return {Object}
 */
export const findByAttachmentId = docId => ({
  url   : 'attachments/info',
  params: {
    docId
  }
});

/**
 *
 * @param id
 * @returns {{params: *}}
 */
export function getAttachmentById(id) {
  const endpoint = {
    url: `attachments/${id}`,
    responseType: 'blob'
  };
  return { ...endpoint };
}

/**
 *
 * @param id
 * @returns {{params: *}}
 */
export function downloadAttachmentById(id) {
  const endpoint = {
    url: `attachments/${id}`,
    responseType: 'blob'
  };
  return { ...endpoint };
}
