/**
 * Создание отзыва документа
 * @param  {Object} config
 * @param  {String} config.recordID    Идентификатор отзыва
 * @param  {String} config.recallDocID Идентификатор отзываемого документа
 * @param  {Number} config.docNumber   Номер отзыва
 * @param  {String} config.docDate     Дата документа
 * @param  {String} config.docInfo     Информация о платёжном поручении
 * @param  {String} config.reason      Причина отзыва
 * @return {Object}
 */
export function create({ recordID, recallDocID, docNumber, docDate, status, docInfo, reason, orgName, orgInn }) {
  const endpoint = {
    url: 'recalls',
    method: 'POST'
  };
  const data = {
    recordID,
    recallDocID,
    docNumber,
    docDate,
    status,
    orgName,
    orgInn,
    docInfo,
    reason
  };
  return { ...endpoint, data };
}

/**
 * Получение данных по умолчанию для создания документа
 * @param  {String} orgId Индентификатор организации
 * @return {Object}
 */
export const getDocDefaultData = orgId => ({ url: 'recalls/defaultValues', params: { orgId } });

/**
 * Проверка данных документа и получение ошибок
 * @param  {String} recordID    Идентификатор отзыва
 * @param  {String} recallDocID Идентификатор отзываемого документа
 * @param  {Number} docNumber   Номер отзыва
 * @param  {String} docDate     Дата документа
 * @param  {String} docInfo     Информация о платёжном поручении
 * @param  {String} reason      Причина отзыва
 * @return {Object}
 */
export const getDocErrors = ({ recordID, recallDocID, docNumber, docDate, docInfo, reason }) => {
  const endpoint = { url: 'recalls/validation', method: 'POST' };
  const data = { recordID, recallDocID, docNumber, docDate, docInfo, reason };
  return { ...endpoint, data };
};

/**
 * Сохранение документа
 * @param  {String} recordID    Идентификатор отзыва
 * @param  {String} recallDocID Идентификатор отзываемого документа
 * @param  {Number} docNumber   Номер отзыва
 * @param  {String} docDate     Дата документа
 * @param  {String} docInfo     Информация о платёжном поручении
 * @param  {String} reason      Причина отзыва
 * @return {Object}
 */
export const saveDoc = ({ recordID, recallDocID, docNumber, docDate, docInfo, reason }) => {
  const endpoint = { url: 'recalls', method: 'POST' };
  const data = { recordID, recallDocID, docNumber, docDate, docInfo, reason };
  return { ...endpoint, data };
};
