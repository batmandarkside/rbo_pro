import { removeEmptyParams } from './utils';
/**
 * Поиск платежа по ID
 * @param   {String} docId ID платежа
 * @returns {Object}
 */
export const findOneById = docId => ({ url: `rpays/${docId}` });

/**
 * Поиск платежей удовлетворяющих критерию с постраничным получением.
 * @param   {String|String[]}  orgId          Идентификатор организации
 * @param   {String}  section                 Секция платежей - Архив, Удаленные
 * @param   {Number}  [offset=0]              default: 0
 * @param   {Number}  [offsetStep=31]         default: 31
 * @param   {String}  sort                    Поле для сортировки. Возможные значения: number, date, sum, account.
 * @param   {Boolean} desc                    Флаг сортировки в обратном направлении
 * @param   {String|String[]}  status         Статусы
 * @param   {String}  account                 Номер счета
 * @param   {String}  receiver                Получатель
 * @param   {String}  dateFrom                Дата начала периода выборки документов в формате yyyy-MM-dd
 * @param   {String}  dateTo                  Дата окончания периода выборки документов в формате yyyy-MM-dd
 * @param   {Number}  sumMin                  Минимальная сумма для выборки документа (включительно)
 * @param   {Number}  sumMax                  Максимальная сумма для выборки документа(включительно)
 * @param   {String}  payerName               Наименование плательщика
 * @param   {String}  receiverINN             ИНН получателя
 * @param   {String}  receiverAccount         Счет получателя
 * @param   {String}  paymentPurpose          Назначение платежа
 * @param   {String}  notes                   Заметки
 * @param   {Boolean} withRecall              РПП с отзывом
 * @param   {String} importId                 id импорта
 * @param   {String}  lastStateChangeDateFrom Начало даты последнего изменения статуса
 * @param   {String}  lastStateChangeDateTo   Конец даты последнего изменения статуса
 * @returns {Object}
 */
export function findAllByQuery(
  {
    orgId,
    section,
    offset = 0,
    offsetStep = 10,
    sort,
    desc,
    status,
    account,
    receiver,
    dateFrom,
    dateTo,
    sumMin,
    sumMax,
    payerName,
    receiverINN,
    receiverAccount,
    paymentPurpose,
    notes,
    withRecall,
    importId,
    lastStateChangeDateFrom,
    lastStateChangeDateTo
  }) {
  const url = `rpays${section ? `/${section}` : ''}`;
  const endpoint = { url };
  const params = {
    query: {
      orgId,
      offset,
      offsetStep,
      sort,
      desc,
      status,
      account,
      receiver,
      dateFrom,
      dateTo,
      sumMin,
      sumMax,
      payerName,
      receiverINN,
      receiverAccount,
      paymentPurpose,
      notes,
      withRecall,
      importId,
      lastStateChangeDateFrom,
      lastStateChangeDateTo
    }
  };
  return { ...endpoint, params };
}

/**
 * Подсчет платежей удовлетворяющих критерию
 * @param   {Object}   query                         Критерий фильтра платежей
 * @param   {String[]} query.orgIds                  Список IDs организаций. Как минимум одна организация принадлежащая пользователю.
 * @param   {String[]} query.statuses                Список IDs статусов. Пустой список означает все статусы.
 * @param   {String}   query.dateFrom                Дата рабочего дня для которого будет производиться подсчет
 * @param   {String}   query.lastStateChangeDateFrom Дата начиная с которой статус документа не менялся
 * @returns {Object}
 */
export function countByQuery({ orgIds, statuses, dateFrom, lastStateChangeDateFrom }) {
  const endpoint = {
    url: 'rpays/count',
  };

  const params = {
    query: removeEmptyParams({
      orgId : orgIds,
      status: statuses,
      dateFrom,
      lastStateChangeDateFrom
    })
  };

  return { ...endpoint, params };
}

/**
 * Запрос статусов возможных для документа
 * @param   {String} docType Тип документа
 * @returns {Object}
 */
export function findAllStatusesByDocType(docType = 'PaymentOrder') {
  const endpoint = {
    url: 'rpays/statuses',
  };

  const params = {
    docType
  };

  return { ...endpoint, params };
}

/**
 * Запрос ограничений документа
 * @param   {String} docId Идентификатор документа
 * @returns {Object}
 */
export function findAllConstraintsById(docId) {
  const endpoint = {
    url: 'rpays/constraints',
  };

  const params = {
    docId
  };

  return { ...endpoint, params };
}

/**
 * Создание копии документа по id
 * @param   {String} docId Идентификатор документа
 * @returns {Object}
 */
export function createCopyDocumentById(docId) {
  const endpoint = {
    url: 'rpays/copy',
    method: 'POST'
  };

  const data = {
    docId
  };

  return { ...endpoint, data };
}

/**
 * Шаблоны платежных поручений
 * @param   {String} [docType = 'Платежное поручение']     Тип документа для отображения клиенту
 * @param   {String} search      Название шаблона или Название получателя
 * @param   {String} orgId       Список организаций
 * @param   {Number} [offset=0]  Смещение в выборке
 * @param   {Number} [offsetStep=240]  Кол-во запрашиваемых шаблонов
 * @returns {Object}
 */
export function findTemplatesByQuery({ docType = 'Платежное поручение', search, orgId, offset = 0, offsetStep = 240 }) {
  const endpoint = {
    url: 'templates'
  };

  const params ={
    query: {
      docType,
      search,
      orgId,
      offset,
      offsetStep
    }
  };

  return { ...endpoint, params };
}

/**
 * Шаблоны платежных поручений, получение шаблона по id
 * @param {String} id
 * @return {{}}
 */
export function getTemplateById(id) {
  const endpoint = {
    url: `templates/${id}`
  }

  return { ...endpoint }
}

/**
 *  Шаблоны платежных поручений
 *  сохранение шаблона
 *
 * @param options = {
    "recordID" :"e51947af-e51c-437b-a0bf-07d2aa432ec2",
    "customerBankRecordID":"14b70f22-703f-bf04-db60-bd110572f40d",
    "docDate":"2016-11-23T00:00:00+0300",
    "docNumber":"142",
    "amount":"1100.99",
    "cbc":"",
    "docDateDay" :"",
    "docDateMonth" :"",
    "docDateYear" :"",
    "taxDocNumber" :"",
    "taxOrCustoms" :"",
    "vatSum" :"198.18",
    "ocato" :"",
    "operationType" :"01",
    "payerName" :"ФИРМА Web11",
    "payerAccount" :"40802810000000100001",
    "payerBankBIC" :"044525225",
    "payerBankName" :"ОАО &quot;СБЕРБАНК РОССИИ&quot;",
    "payerBankCorrAccount" :"30101810400000000225",
    "payerINN" :"1012345678",
    "payerKPP" :"111111111",
    "payerBankCity" :"МОСКВА",
    "payerBankSettlementType" :"Г",
    "paymentPriority" :"1",
    "receiverName" :"123",
    "receiverAccount" :"11111810611111111111",
    "receiverBankCorrAccount" :"30101810900000000469",
    "receiverBankBIC" :"040037469",
    "receiverBankName" :"ФИЛИАЛ &quot;БАЙКОНУР&quot; ОАО &quot;СОБИНБАНК&quot;",
    "receiverINN" :"111111111111",
    "receiverKPP" :"111111111",
    "receiverBankCity" :"БАЙКОНУР",
    "receiverBankSettlementType" :"Г",
    "payReason" :"",
    "chargeType" :"",
    "paymentKind" :"",
    "paymentCode" :"",
    "drawerStatus" :"",
    "taxPeriodDay1" :"",
    "taxPeriodDay2" :"",
    "taxPeriodMonth" :"",
    "taxPeriodYear" :"",
    "uip" :"12345678901234567890",
    "currencyOperationType" :"",
    "vatCalculationRule" :"Ручной ввод",
    "vatRate" :"18",
    "paymentPurpose" :"123.&#10;В том числе НДС 198.18.",
    "docNote" :"",
    "templateName": "Шаблон233"
    }
 */
export function saveTemplate(options) {
  const endpoint = {
    url   : '/templates',
    method: 'POST'
  };
  const data = {
    ...options
  };
  return { ...endpoint, data };
}

export function removeTemplate(templateId) {
  const endpoint = {
    url   : `/templates/${templateId}`,
    method: 'DELETE'
  };

  return { ...endpoint };
}

/**
 * Шаблоны платежных поручений
 * @param   {Object} config
 * @param   {String} config.docType     Тип документа для отображения клиенту
 * @param   {String} config.search      Название шаблона или Название получателя
 * @param   {String} config.name        Название шаблона
 * @param   {String} config.to          Название получателя
 * @param   {String} config.ids         Список идентификаторов
 * @param   {String} config.orgId       Список организаций
 * @param   {Number} [config.offset=0]  Смещение в выборке
 * @returns {Object}
 */
export function findAllTemplatesByQuery({ docType, search, name, to, ids, orgId, offset = 0 }) {
  const endpoint = {
    url: 'rpays/templatesSummary'
  };

  const params ={
    docType,
    search,
    name,
    to,
    ids,
    orgId,
    offset
  };

  return { ...endpoint, params };
}

/**
 * Создание РПП по шаблону
 * @param   {String} tplId Идентификатор документа
 * @returns {Object}
 */
export function createDocumentByTemplateId(tplId) {
  const endpoint = {
    url: 'rpays/documentByTemplate'
  };

  const params ={
    docId: tplId
  };

  return { ...endpoint, params };
}

/**
 * Отправить РПП в банк
 * @param   {String} docId Идентификатор документа
 * @returns {Object}
 */
export function sendToBankById(docId) {
  const endpoint = {
    url: 'rpays/send321'
  };

  const params ={
    docId
  };

  return { ...endpoint, params };
}

/**
 * Создание платёжного документа
 * @param  {String} config.customerBankRecordID
 * @param  {String} config.docNumber
 * @param  {Number} config.amount
 * @param  {String} config.docDate
 * @param  {String} config.receiverName
 * @param  {String} config.receiverINN
 * @param  {String} config.receiverAccount
 * @param  {String} config.receiverKPP
 * @param  {String} config.receiverBankCorrAccount
 * @param  {String} config.receiverBankBIC
 * @param  {String} config.receiverBankName
 * @param  {String} config.receiverBankCity
 * @param  {String} config.receiverBankSettlementType
 * @param  {String} config.paymentPurpose
 * @param  {String} config.payerAccount
 * @param  {String} config.recordID
 * @param  {Number} config.vatSum
 * @param  {Number} config.vatRate
 * @param  {String} config.operationType
 * @param  {String} config.payerKPP
 * @param  {String} config.payerName
 * @param  {String} config.payerINN
 * @param  {String} config.payerBankBIC
 * @param  {String} config.payerBankName
 * @param  {String} config.payerBankCity
 * @param  {String} config.payerBankCorrAccount
 * @param  {String} config.payerBankSettlementType
 * @param  {String} config.paymentKind
 * @param  {String} config.currencyOperationType
 * @param  {String} config.paymentPriority
 * @param  {String} config.paymentCode
 * @param  {String} config.taxOrCustoms
 * @param  {String} config.vatCalculationRule
 * @return {Object}
 */
export function createByRpay({ 
  taxPeriodDay1,
  taxPeriodDay2,
  taxPeriodMonth,
  taxPeriodYear,
  taxOrCustoms,
  taxDocNumber,
  docDateDay,
  docDateMonth,
  docDateYear,
  payReason,
  amount: amountValue,
  currencyOperationType,
  vatCalculationRule,
  vatRate,
  vatSum,
  customerBankRecordID: selectedOrgId,
  payerAccount,
  payerBankBIC,
  payerBankName,
  payerBankCorrAccount,
  payerINN,
  payerKPP,
  payerName,
  payerBankSettlementType,
  payerBankCity,
  receiverAccount,
  receiverBankBIC,
  receiverBankCity,
  receiverBankCorrAccount,
  receiverBankName,
  receiverBankSettlementType,
  receiverINN,
  receiverKPP,
  uip,
  receiverName,
  docDate,
  docNote,
  docNumber,
  paymentCode,
  paymentKind,
  paymentPriority,
  paymentPurpose,
  drawerStatus,
  notes,
  ocato,
  bankAcceptDate,
  bankMessage,
  bankMessageAuthor,
  cbc,
  chargeType,
  operationType,
  recordID
}) {
  const endpoint = {
    url: 'rpays',
    method: 'POST'
  };
  const data = {
    taxPeriodDay1,
    taxPeriodDay2,
    taxPeriodMonth,
    taxPeriodYear,
    taxDocNumber,
    taxOrCustoms,
    docDateDay,
    docDateMonth,
    docDateYear,
    payReason,
    amount: amountValue,
    currencyOperationType,
    vatCalculationRule,
    vatRate,
    vatSum,
    customerBankRecordID: selectedOrgId,
    payerAccount,
    payerBankBIC,
    payerBankName,
    payerBankCorrAccount,
    payerINN,
    payerKPP,
    payerName,
    payerBankSettlementType,
    payerBankCity,
    receiverAccount,
    receiverBankBIC,
    receiverBankCity,
    receiverBankCorrAccount,
    receiverBankName,
    receiverBankSettlementType,
    receiverINN,
    receiverKPP,
    receiverName,
    docDate,
    docNote,
    docNumber,
    paymentCode,
    uip,
    paymentKind,
    paymentPriority,
    paymentPurpose,
    drawerStatus,
    notes,
    ocato,
    bankAcceptDate,
    bankMessage,
    bankMessageAuthor,
    cbc,
    chargeType,
    operationType,
    recordID
  };
  
  return { ...endpoint, data };
}

/**
 * Получение значений по умолчанию для создания нового платежа
 * @param {array|string} orgId   ID организации со счета которой будет производиться платеж
 * @returns {Config}
 */
export function findDefaultValuesByOrganisationId(orgId) {
  const endpoint = {
    url   : 'defaultValues/rpay',
    method: 'POST'
  };
  const data = {
    orgId
  };
  return { ...endpoint, data };
}

export function saveAsTemplate({docId, name}) {
  const endpoint = {
    url   : 'templates/saveByDocument',
    method: 'POST'
  };
  const data = {
    i: docId,
    name
  };

  return { ...endpoint, data };
}

/**
 * Создание документа из шаблона по id шаблона
 * @param   {String} templateId Идентификатор шаблона
 * @returns {Object}
 */
export function createDocumentFromTemplate(templateId) {
  const endpoint = {
    url: 'templates/create',
    method: 'POST'
  };

  const data = {
    templateId
  };

  return { ...endpoint, data };
}

/**
 * валидация шаблона
 */
export function validateTemplate(data) {
  const endpoint = {
    url: 'templates/validation',
    method: 'POST'
  }

  return { ...endpoint, data };
}

/**
 * Валидация платёжа
 * @param  {Object} data
 * @return {Object}
 */
export function validateDocument(data) {
  const endpoint = {
    url: 'rpays/validation',
    method: 'POST'
  };
  return { ...endpoint, data };
}

