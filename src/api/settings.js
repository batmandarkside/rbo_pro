/**
 * Получить настройки текущего пользователя
 * @return {Object}
 */
export const findOneForCurrentSession = () => ({
  url: 'user/settings'
});

/**
 * Обновить настройки для текущего пользоватлея
 * @param  {Object} settings настройки
 * @return {Object}
 */
export function updateForCurrentSession(settings) {
  const endpoint = {
    url: 'user/settings',
    method: 'PUT'
  };
  const data = settings;
  const transformRequest = toJSON;
  return { ...endpoint, data, transformRequest };
}

export function getUlk() {
  const endpoint = {
    url: 'ulk'
  };

  const params = {};

  return { ...endpoint, params };
}

/**
 * JSON в строковое представление
 * @param  {Object} data
 * @return {String}
 */
function toJSON(data) {
  return JSON.stringify(data);
}
