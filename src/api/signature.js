/**
 * getCryptoProfiles
 * @param  {Object}  config
 * @param  {Array}  config.docId
 * @param  {Boolean} config.forVisa
 * @return {Object}
 */
export function getCryptoProfiles({ docId, forVisa }) {
  const endpoint = {
    url: 'cryptoProfiles_v2'
  };

  const params = {
    docId,
    forVisa
  };

  return {
    ...endpoint,
    params
  };
}

/**
 * initSign
 * @param  {Array}  config.docId список документов на подпись
 * @param  {String}  config.cryptoProfileId
 * @param  {Boolean} config.forVisa
 * @return {Object}
 */
export function initSign({ docId, cryptoProfileId, forVisa }) {
  const endpoint = {
    url: 'signature',
    method: 'POST'
  };

  const data = {
    docId,
    cryptoProfileId,
    forVisa
  };

  return {
    ...endpoint,
    data
  };
}

/**
 * sendSignToBank
 * @param  {Object} config
 * @param  {Array} config.signs
 * @param  {Boolean} config.onToken
 * @return {Object}
 */
export function saveSignStatus({ signs, onToken }) {
  const endpoint = {
    url: 'signature',
    method: 'PUT'
  };

  const data = {
    signs,
    onToken
  };

  return {
    ...endpoint,
    data
  };
}


/**
 * signToFormat
 * @param  {Object} config
 * @param  {String} config.docId
 * @param  {String} config.signId
 * @param  {String} config.format
 * @return {Object}
 */
export function signToFormat({ docId, signId, format }) {
  const endpoint = {
    url: 'signature',
    responseType: 'blob'
  };

  const params = {
    docId,
    signId,
    format
  };

  return {
    ...endpoint,
    params
  };
}

export function getCertList() {
  const endpoint = {
    url: 'certs'
  };

  return {
    ...endpoint
  };
}

/**
 *
 * @param reqDate
 * @param reqNum
 * @param ulk: {
 *  name,
 *  id,
 *  email,
 *  cityName,
 *  orgINN,
 *  branchName,
 * }
 * @param cryptoProfileId
 * @param country
 * @param reqType
 * @param reqData
 * @param containerId
 */
export function createNewCert(params) {
  const endpoint = {
    url: 'certs',
    method: 'POST'
  };

  const data = {
    ...params
  };

  return {
    ...endpoint,
    data
  };
}

/**
 *
 * @param certId
 * @returns {{data: {}}}
 */
export function downloadActiveCert({ certId }) {
  const endpoint = {
    url: '/certs/file'
  };

  const params = {
    certId
  };

  return {
    ...endpoint,
    params
  };
}

/**
 *
 * @returns {{}}
 */
export function getDefaultValuesCertReq(params) {
  const endpoint = {
    url: 'defaultValues/certReq',
    method: 'POST'
  };

  const data = {
    ...params
  };

  return {
    ...endpoint,
    data
  };
}

export function certValidation(params) {
  const endpoint = {
    url: 'certs/validation',
    method: 'POST'
  };

  const data = {
    ...params
  };

  return {
    ...endpoint,
    data
  };
}

/**
 * отправка сертификата в банк
 * @param docId
 * @param reqType
 * @returns {{query: {docId: *, reqType: string}}}
 */
export function sendCertToBank({ docId, reqType = 'CryptoProReqNew' }) {
  const endpoint = {
    url: 'certs/sendReq'
  };

  const params = {
    docId,
    reqType
  };

  return {
    ...endpoint,
    params
  };
}

export function deleteCert({ docId, reqType = 'CryptoProReqNew' }) {
  const endpoint = {
    url: 'certs/certReq',
    method: 'DELETE'
  };

  const params = {
    docId,
    reqType
  };

  return {
    ...endpoint,
    params
  };
}

/**
 *
 * @param docId
 * @param reqType
 * @param format
 * @returns {{query: {docId: *, reqType: string, format: string}}}
 */
export function certReqFormatPrint({ docId, reqType = 'CryptoProReqNew', format = 'pdf' }) {
  const endpoint = {
    url: 'certs/certReqFormat',
    responseType: 'blob'
  };

  const params = {
    docId,
    reqType,
    format
  };

  return {
    ...endpoint,
    params
  };
}

/**
 *
 * @param docId
 * @param signAuthId
 * @returns {{params: {docId: *, cryptoProfileId: *}}}
 */
export function initSignCert({ docId, signAuthId }) {
  const endpoint = {
    url: '/certs/certReq/signature',
    method: 'POST'
  };

  const data = {
    docId,
    signAuthId
  };

  return {
    ...endpoint,
    data
  };
}

/**
 *
 * @param docId
 * @param sign
 * @param onToken
 * @returns {{data: {docId: *, sign: *, onToken: boolean}}}
 */
export function sendSignToBank({ docId, sign, onToken = false }) {
  const endpoint = {
    url: '/certs/certReq/signature',
    method: 'PUT'
  };

  const data = {
    docId,
    sign,
    onToken
  };

  return {
    ...endpoint,
    data
  };
}

/**
 *
 * @param orgId
 * @returns {{params: {orgId: *}}}
 */
export function checkCerts({ orgId }) {
  const endpoint = {
    url: '/certs/checks',
  };

  const params = {
    orgId
  };

  return {
    ...endpoint,
    params
  };
}

