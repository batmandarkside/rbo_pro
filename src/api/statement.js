/**
 * Запрос платежа по его идентификатору
 * @param  {string} id Идентификатор выписки
 * @return {Object}
 */
export const findOneById = id => ({
  url   : `statements/${id}`
});

/**
 * Экспорт движений по счёту удовлетворяющих критериям
 * @param  {string}  orgId              Идентификатор организации
 * @param  {Number}  offset=0           Смещение в элементах
 * @param  {Number}  offsetStep=31      Количество записей
 * @param  {string}  sort='receiptDate  Поле для сортировки. Возможные значения: stateDate, recieptDate, account, id, number
 * @param  {boolean} desc               Флаг сортировки в обратном направлении
 * @param  {string}  account            Список счетов
 * @param  {string}  from               Начало перода запроса
 * @param  {string}  to                 Конец периода запроса
 * @param  {integer} stmtType           Тип выписки. 0 - итоговая. 1 - промежуточная.
 * @param  {boolean} zero               Включать в выборку нулевые выписки
 * @param  {string}  corrName           Наименование контрагента
 * @param  {string}  corrAccount        Счет контрагента
 * @param  {string}  currCodeIso        ISO Код валюты счета
 * @return {Object}
 */
export function findAllByQuery(
  {
    orgId,
    offset = 0,
    offsetStep = 31,
    sort = 'receiptDate',
    desc = true,
    account,
    from,
    to,
    stmtType,
    zero,
    corrName,
    corrAccount,
    currCodeIso
  }) {
  const endpoint = {
    url: 'statements'
  };
  const params = {
    orgId,
    offset,
    offsetStep,
    sort,
    desc,
    account,
    from,
    to,
    stmtType,
    zero,
    corrName,
    corrAccount,
    currCodeIso
  };

  Object.keys(params).forEach(key => {if (params[key] === null) delete params[key];});

  return { ...endpoint, params };
}
