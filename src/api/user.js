/**
 * Получить данные по текущему пользователю
 * @return {Object}
 */
export const findOneForCurrentSession = () => ({ url: 'user' });
