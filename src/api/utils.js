import { isNil } from 'lodash';

export function removeEmptyParams(params) {
  if (!params) return {};
  Object.keys(params).forEach(key => { if (isNil(params[key])) delete params[key]; });
  return params;
}
