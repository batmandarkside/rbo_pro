/**
 * Сохранение письма
 * @param  {Object}   config
 * @param  {String}   config.docId             Идентификатор документа
 * @param  {Number}   config.docNumber         Номер документа
 * @param  {String}   config.docDate           Например 2016-01-27T00:00:00+0300
 * @param  {String}   config.account           Счет
 * @param  {String}   config.topic             Тема письма
 * @param  {String}   config.branchId          @todo:[description]
 * @param  {String}   config.branchName        @todo:[description]
 * @param  {String}   config.receiver          Получатель
 * @param  {String}   config.orgId             Идентификатор организации
 * @param  {String}   config.message           Сообщение
 * @param  {Object[]} config.attaches
 * @param  {String}   config.attaches.name     Имя файла
 * @param  {String}   config.attaches.mimeType MIME Type файла
 * @param  {String}   config.attaches.content  Содержимое файла в base64
 * @param  {String}   config.attaches.comment  Комментарий к файлу
 * @return {Object}
 */
export function createByOutgoingMessageDetails({ docNumber, docDate, account, topic, branchId, branchName, receiver, orgId, message, attaches }) {
  const endpoint = {
    url: 'messages/outcome/validation',
    method: 'POST'
  };
  const data = {
    docNumber,
    docDate,
    account,
    topic,
    branchId,
    branchName,
    receiver,
    orgId,
    message,
    attaches
  };
  return { ...endpoint, data };
}

/**
 * Создание отзыва документа
 * @param  {Object} config
 * @param  {String} config.recordID    Идентификатор отзыва
 * @param  {String} config.recallDocID Идентификатор отзываемого документа
 * @param  {Number} config.docNumber   Номер отзыва
 * @param  {String} config.docDate     Дата документа
 * @param  {String} config.docInfo     Информация о платёжном поручении
 * @param  {String} config.reason      Причина отзыва
 * @return {Object}
 */
export function createByRecallDetails({ recordID, recallDocID, docNumber, docDate, docInfo, reason }) {
  const endpoint = {
    url: 'recalls/validation',
    method: 'POST'
  };
  const data = {
    recordID,
    recallDocID,
    docNumber,
    docDate,
    docInfo,
    reason
  };
  return { ...endpoint, data };
}

/**
 * Создание платёжного документа
 * @param {String} config.customerBankRecordID
 * @param {String} config.docNumber
 * @param {Number} config.amount
 * @param {String} config.docDate
 * @param {String} config.receiverName
 * @param {String} config.receiverINN
 * @param {String} config.receiverAccount
 * @param {String} config.receiverKPP
 * @param {String} config.receiverBankCorrAccount
 * @param {String} config.receiverBankBIC
 * @param {String} config.receiverBankName
 * @param {String} config.receiverBankCity
 * @param {String} config.receiverBankSettlementType
 * @param {String} config.paymentPurpose
 * @param {String} config.payerAccount
 * @param {String} config.recordID
 * @param {Number} config.vatSum
 * @param {Number} config.vatRate
 * @param {String} config.operationType
 * @param {String} config.payerKPP
 * @param {String} config.payerName
 * @param {String} config.payerINN
 * @param {String} config.payerBankBIC
 * @param {String} config.payerBankName
 * @param {String} config.payerBankCity
 * @param {String} config.payerBankCorrAccount
 * @param {String} config.payerBankSettlementType
 * @param {String} config.paymentKind
 * @param {String} config.currencyOperationType
 * @param {String} config.paymentPriority
 * @param {String} config.paymentCode
 * @param {String} config.vatCalculationRule
 * @return {Object}
 */
export function createByRpayDetails(
  customerBankRecordID,
  docNumber,
  amount,
  docDate,
  receiverName,
  receiverINN,
  receiverAccount,
  receiverKPP,
  receiverBankCorrAccount,
  receiverBankBIC,
  receiverBankName,
  receiverBankCity,
  receiverBankSettlementType,
  paymentPurpose,
  payerAccount,
  recordID,
  vatSum,
  vatRate,
  operationType,
  payerKPP,
  payerName,
  payerINN,
  payerBankBIC,
  payerBankName,
  payerBankCity,
  payerBankCorrAccount,
  payerBankSettlementType,
  paymentKind,
  currencyOperationType,
  paymentPriority,
  paymentCode,
  vatCalculationRule
 ) {
  const endpoint = {
    url: 'rpays/validation',
    method: 'POST'
  };
  const data = {
    customerBankRecordID,
    docNumber,
    amount,
    docDate,
    receiverName,
    receiverINN,
    receiverAccount,
    receiverKPP,
    receiverBankCorrAccount,
    receiverBankBIC,
    receiverBankName,
    receiverBankCity,
    receiverBankSettlementType,
    paymentPurpose,
    payerAccount,
    recordID,
    vatSum,
    vatRate,
    operationType,
    payerKPP,
    payerName,
    payerINN,
    payerBankBIC,
    payerBankName,
    payerBankCity,
    payerBankCorrAccount,
    payerBankSettlementType,
    paymentKind,
    currencyOperationType,
    paymentPriority,
    paymentCode,
    vatCalculationRule
  };
  return { ...endpoint, data };
}
