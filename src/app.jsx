import 'styles/components.css';
import 'styles/base.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { ConnectedRouter as ReduxRouter } from 'react-router-redux';
import { getProfileDataAction, getUserSettingsAction } from 'dal/profile/actions';
import { getOrganizationsAction } from 'dal/organizations/actions';
import { checkUserCertsAction } from 'dal/notification/actions';
import cryptoProPlugin from 'services/cryptopro/cadesplugin_api';
import AppRoot from 'components/app-root';
import { history } from './app-history';
import boot from './bootstrap';
import store from './store';

/**
 *
 * @todo
 * Старт приложения происходит только после авторизации
 *  и получения данных пользователя
 * Зашли на страницу. Keycloack определил что мы не авторизованы
 * Перебросил нас на страницу ( http://auth.dev.raiffeisen.ru/authorize !!!! ) с параметрами
 * После авторизации снова редирект на наше приложение
 *
 * Получаем { Organizations, UserSettings } и только потом авторизованного юзера
 */
function initApp() {
  const loadPlugin = cryptoProPlugin();
  loadPlugin();

  const unsubscribe = store.subscribe(() => {
    const profile = store.getState().get('profile');
    if (profile.has('login')) {
      unsubscribe();
      renderApp();
    }
  });

  /**
   *
   * @todo
   * Fix Issue EL-12124
   * Сервер возвращает ошибку 400 при запросе organization или settings, если отправлять запросы одновременно
   * Поэтому пока делаем их последовательно и ждем решения на back-end
   */

  store
    .dispatch(getOrganizationsAction())
    .then(() => store.dispatch(getUserSettingsAction()))
    .then(() => store.dispatch(getProfileDataAction()))
    .then(() => store.dispatch(checkUserCertsAction()));
}

function renderApp() {
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <ReduxRouter history={history}>
          <AppRoot />
        </ReduxRouter>
      </BrowserRouter>
    </Provider>,
    document.querySelector('#Application')
  );
}

window.__INIT_RBO_APPLICATION__ = () => {
  boot().then(initApp);
};

/**
 * app start
 */
document.addEventListener('DOMContentLoaded', window.__INIT_RBO_APPLICATION__);
