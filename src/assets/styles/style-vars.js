export const MEDIA = {
  mobile: 'max-width: 529px',
  tablet: 'max-width: 949px',
  desktop1024: 'max-width: 1024px',
  desktop: 'max-width: 1250px',
  desktop_xl: 'min-width: 1250px'
};

export const COLORS = {
  colorWhite: '#FFFFFF',
  colorWhiteThree: '#F5F5F5',

  colorBlack: '#000000',

  colorGreyishBrown: '#3C3C3C',
  colorGreyBrown: '#3a3a3a',
  colorGreyishBrownTwo: '#4A4A4A',
  colorWarmGrey: '#737373',
  colorWarmGreyThree: '#888888',
  colorWarmGreyTwo: '#A0A0A0',
  colorGreyish: '#B9B9B9',
  colorPinkishGrey: '#CCCCCC',
  colorLightGrey: '#DFDFDF',
  colorWhiteTwo: '#ECECEC',

  colorPeacockBlueDark: '#004E95',
  colorPeacockBlue: '#0055A2',
  colorCeruleanBlue: '#007AE8',
  colorBluish: '#2687C5',
  colorDarkSkyBlue: '#3F9BD6',
  colorPerrywinkle: '#72B3DD',
  colorIceBlue: '#ECF9FF',

  colorTomatoRed: '#D91E00',
  colorPaleRed: '#D9534F',
  colorSalmon: '#FF936F',

  colorTrueGreen: '#018E01',
  colorLightSage: '#B8E986',

  colorParchment: '#FFF2AE',
  colorSunflowerYellow: '#FFDA00',
  colorMarigold: '#FCC900',
  colorTangerine: '#F89500',
  colorTangerineDark: '#E58900',

  colorDarkLilac: '#9B5BA5',
  colorLighterPurple: '#9F5FF5'
};

export const FONTS = {
  Helvetica: 'Helvetica, Arial, sans-serif'
};

export const FONTS_SIZE = {
  fontSizeExtraSmall: '10px',
  fontSizeSmall: '12px',
  fontSizeNormal: '14px',
  fontSizeLarge: '16px',
  fontSizeExtraLarge: '18px',

  fontSizeH1: '26px',
  fontSizeH2Small: '20px',
  fontSizeH2: '22px',
  fontSizeH3: '18px',
  fontSizeH4: '16px',
  fontSizeH5: '14px'
};

export const INDENTING_STEP = 4;

