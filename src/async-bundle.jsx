import React from 'react';

export default (loader, collection) => (
  class AsyncComponent extends React.Component {
    constructor(props) {
      super(props);
      this.Component = null;
      this.state = { comp: AsyncComponent.Component };
    }

    componentWillMount() {
      if (!this.state.comp) {
        loader().then((Component) => {
          AsyncComponent.Component = Component;
          this.setState({ comp: Component });
        });
      }
    }

    render() {
      const { comp } = this.state;
      return comp
        ? React.createElement(comp, { ...this.props, ...collection })
        : null;
    }
  }
);
