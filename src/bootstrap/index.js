import { axiosConfig } from 'config/application/axios';
import { keycloak } from 'config/application/keycloak';
import serviceUrl from 'config/application/service-url';
import configureMoment from 'config/application/moment';
import configureNumeral from 'config/application/numeral';
import { isSlashTerminatedString } from './../utils';
import './middlewares';


/**
 * Bootstraping
 */
export default function boot() {
  configureMoment();
  configureNumeral();

  if (process.env.NODE_ENV === 'mock') {
    return System.import('api/mock')
      .then(({ configureMocks }) => configureMocks(axiosConfig.getAxiosInstance()));
  } 

  return axiosConfig
    .getAxiosInstance()
    .get('/app-settings.json')
    .then((res) => {
      const { SERVICE_BASE_URL, AUTH_BASE_URL } = res.data;

      if (isSlashTerminatedString(SERVICE_BASE_URL) && isSlashTerminatedString(AUTH_BASE_URL)) {
        serviceUrl.SERVICE_BASE_URL = SERVICE_BASE_URL;
        serviceUrl.AUTH_BASE_URL = AUTH_BASE_URL;
      } else {
        throw new Error(`SERVICE_BASE_URL="${SERVICE_BASE_URL}" and AUTH_BASE_URL="${AUTH_BASE_URL}" must be valid URLs terminated by slash character.`);
      }
    }).then(() => {
      keycloak.init();
      axiosConfig.init();
    })
    .then(() => keycloak.auth())
    .then(() => axiosConfig.setAuthToken(`Bearer ${keycloak.instance.token}`));
}
