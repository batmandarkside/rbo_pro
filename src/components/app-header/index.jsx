import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect }  from 'react-redux';
import mapStateToProps from 'components/app-root/selectors';
import * as NotificationAction from 'dal/notification/actions';
import * as ProfileAction from 'dal/profile/actions';
import * as OrganizationsAction from 'dal/organizations/actions';
import classNames from 'classnames';
import Immutable from 'immutable';
import Fade from 'ui-components/fade-overlay';
import Menu from './menu';
import Organisation from './org-list';
import './style.css';

export class AppHeader extends Component {

  static propTypes = {
    changeOrganizations: PropTypes.func.isRequired,
    organizations: PropTypes.instanceOf(Immutable.List).isRequired,
    selectedOrganization: PropTypes.instanceOf(Immutable.List).isRequired,
    updateUserSettingsAction: PropTypes.func.isRequired,
    addUserSettingsOrganizationAction: PropTypes.func.isRequired,
    selectedAllOrganizationsAction: PropTypes.func.isRequired,
    removeAllOrganizationsAction: PropTypes.func.isRequired,
    profileLogoutAction: PropTypes.func.isRequired,
    className: PropTypes.string,
    isIndex: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state.isOpenOrganizations = !props.selectedOrganization.size;
    this.state.selectedOrganization = props.selectedOrganization;
  }

  state = {
    isOpenOrganizations: false
  }

  onToggleOrganization = () => {
    this.setState({ isOpenOrganizations: !this.state.isOpenOrganizations });
  }

  onCloseOrganization = () => {
    const { changeOrganizations, selectedOrganization } = this.props;

    this.onToggleOrganization();

    if (!selectedOrganization.equals(this.state.selectedOrganization)) {
      this.setState({ selectedOrganization });
      changeOrganizations();
    }
  };

  onCheckedOrganization = (orgItem) => {
    const { addUserSettingsOrganizationAction, updateUserSettingsAction } = this.props;
    addUserSettingsOrganizationAction(orgItem);
    updateUserSettingsAction();
  }

  selectedAll = () => {
    const { selectedAllOrganizationsAction, organizations, updateUserSettingsAction } = this.props;
    selectedAllOrganizationsAction(organizations);
    updateUserSettingsAction();
  }

  removeAll = () => {
    const { removeAllOrganizationsAction, updateUserSettingsAction } = this.props;
    removeAllOrganizationsAction();
    updateUserSettingsAction();
  }

  _tryLogout = () => {
    const { profileLogoutAction } = this.props;
    profileLogoutAction && profileLogoutAction();
  }

  _stopPropagation = evt => (evt ? evt.stopPropagation() : null)

  /**
   * Общение Organisation и Menu через parent
   *   при открытии списка организаций меню узнает об этом через свойство isOpenOrganizations
   *
   * Когда открыт список организаций, оборачиваем компонент в Fade
   *  тем самым блокируя взаимодействие с сайтом
   *
   * @returns {XML}
   */
  render() {
    const { className, isIndex, selectedOrganization } = this.props;
    const { isOpenOrganizations } = this.state;

    const classSelectors = classNames('b-application__header', {
      [className]: !!className,
      isIndex,
    });

    const component = (
      <div className={classSelectors} onClick={this._stopPropagation} data-loc="layoutHeader">
        <Menu
          {...this.props}
          tryLogout={this._tryLogout}
          isOpenOrganizations={isOpenOrganizations || !selectedOrganization.size}
          onOpenOrganisation={this.onToggleOrganization}
        />
        {(isOpenOrganizations || !selectedOrganization.size) &&
        <Organisation
          {...this.props}
          selectedAll={this.selectedAll}
          removeAll={this.removeAll}
          onCheckedOrganizationAction={this.onCheckedOrganization}
          onCloseOrg={this.onCloseOrganization}
        />
        }
      </div>
    );

    const closeFade = isOpenOrganizations && selectedOrganization.size ?
      this.onCloseOrganization : null;

    return isOpenOrganizations || !selectedOrganization.size ?
      <Fade onClick={closeFade}>{component}</Fade> : component;
  }
}

export default connect(
  mapStateToProps,
  {
    ...NotificationAction,
    ...ProfileAction,
    ...OrganizationsAction
  }
)(AppHeader);
