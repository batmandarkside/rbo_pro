import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import classNames           from 'classnames';
import Immutable            from 'immutable';
import Icon                 from 'components/ui-components/icon';
import { Link }             from 'react-router-dom';
import './style.css';

export default class Menu extends Component {
  
  static propTypes = {
    fullName: PropTypes.string.isRequired,
    onOpenOrganisation: PropTypes.func.isRequired,
    tryLogout: PropTypes.func.isRequired,
    isOpenOrganizations: PropTypes.bool.isRequired,
    selectedOrganizationSize: PropTypes.number.isRequired
  }
  
  _toggleChooseOrganisations = () => {
    const { onOpenOrganisation, selectedOrganizationSize } = this.props;
    if (selectedOrganizationSize) {
      onOpenOrganisation();
    }
  }

  
  render() {
    const { isOpenOrganizations, tryLogout } = this.props;
    const { fullName, selectedOrganizationSize } = this.props;
    
    const classSelectors = classNames('b-application__header__menu', {
      'b-application__header__not-allow-close': !selectedOrganizationSize && isOpenOrganizations
    });
    
    return (
      <div className={classSelectors}>
        
        {(isOpenOrganizations) &&
        <hr className="b-application__header__menu-hr" />
        }
        
        <div className="b-application__header__menu-inner">
          <OrganizationsLabels {...this.props} toggleChooseOrganisations={this._toggleChooseOrganisations} />
          
          {/* Если открыт список выбора организаций, то не показываем эту секцию */}
          {!isOpenOrganizations &&
          <div className="b-application__header__user">
            <span className="b-application__header__user-name">{fullName}</span>
            <span className="b-application__header__user-logout" onClick={tryLogout}>
              <Icon type="logout" size="16" />
              <span className="b-application__header__logout m-like-link">Выйти</span>
            </span>
          </div>
          }
        </div>
      </div>
    );
  }
}

/**
 *
 * @param organizations
 * @param selectedOrganizationSize
 * @param selectedOrganization
 * @returns {XML}
 */
const OrganizationsLabels = ({
  organizations,
  selectedOrganizationSize,
  selectedOrganization,
  toggleChooseOrganisations
}) => {
  const classOrganisations = classNames('m-dashed-link', {
    'action-not-allowed': !selectedOrganizationSize
  });
  
  return (
    <div>
      {organizations.size > 1 &&
      <span className={classOrganisations} onClick={toggleChooseOrganisations}>
        Мои организации
      </span>
      }
      
      {organizations.size > 1 &&
      <span style={{ marginLeft: '10px', marginRight: '10px' }}>&middot;</span>
      }
      
      {selectedOrganizationSize > 1 || !selectedOrganizationSize ?
        <choose>Выбраны {selectedOrganization.size} из {organizations.size}</choose>
        : null}
      
      {selectedOrganizationSize === 1 &&
      <choose>{selectedOrganization.getIn([0, 'shortName'])}</choose>
      }
      <Link to="/correspondents" className="b-application__header__correspondents-link">Мои контрагенты</Link>
    </div>
  );
};

OrganizationsLabels.propTypes = {
  organizations: PropTypes.instanceOf(Immutable.List).isRequired,
  selectedOrganization: PropTypes.instanceOf(Immutable.List).isRequired,
  selectedOrganizationSize: PropTypes.number.isRequired,
  toggleChooseOrganisations: PropTypes.func.isRequired
};
