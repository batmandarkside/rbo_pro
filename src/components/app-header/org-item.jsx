import React, { Component } from 'react';
import PropTypes        from 'prop-types';
import classNames       from 'classnames';
import { is, Map }      from 'immutable';
import Checkbox         from '@rbo/components/lib/form/Checkbox';
import throttle         from 'lodash/throttle';

export default class OrgItem extends Component {

  static propTypes = {
    item                       : PropTypes.instanceOf(Map).isRequired,
    onCheckedOrganizationAction: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);
    const { onCheckedOrganizationAction, item } = this.props;
    this._onChange = throttle(onCheckedOrganizationAction.bind(this, item), 100);
  }

  shouldComponentUpdate(nextProps) {
    return !is(nextProps.item, this.props.item);
  }

  render() {
    const { item } = this.props;
    const classSelectors = classNames('b-organisation__item', {
      'b-organisation__item-selected': item.get('selected')
    });

    return (
      <div className={classSelectors}>
        <div className="b-organisation__item-label">
          <Checkbox
            checked={item.get('selected')}
            label={item.get('shortName')}
            onClick={this._onChange}
          />
        </div>
      </div>
    );
  }
}
