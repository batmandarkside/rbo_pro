import React, { Component } from 'react';
import PropTypes        from 'prop-types';
import classNames       from 'classnames';
import { List, is }     from 'immutable';
import Icon             from 'components/ui-components/icon';
import OrgItem          from './org-item';
import './style.css';

export default class Organisation extends Component {
  
  static propTypes = {
    organizations: PropTypes.instanceOf(List).isRequired,
    onCloseOrg: PropTypes.func.isRequired,
    onCheckedOrganizationAction: PropTypes.func.isRequired,
    selectedAll: PropTypes.func.isRequired,
    removeAll: PropTypes.func.isRequired,
    selectedOrganizationSize: PropTypes.number.isRequired
  }
  
  shouldComponentUpdate(nextProps) {
    return !is(nextProps.organizations, this.props.organizations);
  }
  
  render() {
    const { organizations, selectedOrganizationSize }  = this.props;
    const { selectedAll, removeAll, onCheckedOrganizationAction, onCloseOrg } = this.props;
    
    const classFiltersAll = classNames('m-dashed-link', {
      '_choose-all': true
    });
  
    const classFiltersRemove = classNames('m-dashed-link', {
      '_remove-all': true
    });

    return (
      <div className="b-organisation-selection">
        
        {!!selectedOrganizationSize &&
        <div className="b-organisation__close">
          <Icon size="16" type="close" title="Закрыть" onClick={onCloseOrg} />
        </div>
        }
        
        <div className="b-organisation__header">
          <h1>Мои организации</h1>
          <span className={classFiltersAll} onClick={selectedAll}>
              Выбрать всё
          </span>
          <span className={classFiltersRemove} onClick={removeAll}>
              Скрыть всё
          </span>
        </div>
        <div className="b-organisation__body">
          <p className="b-organisation__description">
            Для удобства пользования системой вы можете скрывать или отображать информацию о
            любой организации в любой момент времени. Никакие данные не будут потеряны.
          </p>
          {organizations.map((org, i) =>
            <OrgItem onCheckedOrganizationAction={onCheckedOrganizationAction} key={i} item={org} />)}
        </div>
      </div>
    );
  }
}

