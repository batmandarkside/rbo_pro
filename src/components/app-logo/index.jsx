import React                from 'react';
import Icon from 'ui-components/icon';
import { Link } from 'react-router-dom';
import { IconLogo } from './style.js';

const AppLogo = () => (
  <Link to="/" className="b-site-logo__img">
    <IconLogo size="32">
      <Icon type="logo" size="32" />
    </IconLogo>
  </Link>
);

export default AppLogo;
