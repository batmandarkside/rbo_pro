import styled from 'styled-components';
import { COLORS } from 'assets/styles/style-vars.js';

export const IconLogo = styled.div`
  display: block;
  border: 0 none;
  background: ${COLORS.colorSunflowerYellow};
  
  svg {
    fill: ${COLORS.colorBlack};
  }
`;
