import React, { Component } from 'react';
import { connect } from 'react-redux';
import ImmutableProptypes from 'react-immutable-proptypes';
import mapStateToProps from 'components/app-root/selectors';
import Logo from 'components/app-logo';
import MenuItem from './menu-item/item';
import SupportItem from './menu-item/support';
import SupportText from './support-text';
import  {
  NavBar,
  NavBarBlock,
  ModalSupport,
  NavBarLogo,
  NavBarSectionTop,
  NavBarSectionBottom
} from './style.js';

export class AppNavBar extends Component {
  
  static propTypes = {
    menuItems: ImmutableProptypes.list.isRequired,
  };
  
  state = {
    openContact: false,
    isMouseEnterMenu: false
  };

  /**
   * @todo
   * @param evt
   */
  onOpenContact = (evt) => {
    evt.stopPropagation();
    evt.preventDefault();
    this.setState({ openContact: true });
  };
  
  onCloseModal = () => {
    this.setState({ openContact: false });
  };

  render() {
    const { menuItems } = this.props;
    const { isMouseEnterMenu, openContact } = this.state;

    return (
      <NavBar>
        <NavBarBlock>
          <NavBarLogo>
            <Logo />
          </NavBarLogo>
          <NavBarSectionTop>
            {menuItems.filter(item => item.get('section') === 'top').map((item, i) =>
              <MenuItem
                item={item.toJS()}
                isMouseEnterMenu={isMouseEnterMenu}
                key={i}
              />
            )}
          </NavBarSectionTop>
          <NavBarSectionBottom>
            {menuItems.filter(item => item.get('section') === 'bottom').map((item, i) =>
              <MenuItem
                item={item.toJS()}
                isMouseEnterMenu={isMouseEnterMenu}
                key={i}
              />
            )}
            <SupportItem onClick={this.onOpenContact} />
          </NavBarSectionBottom>
          {openContact && (
            <ModalSupport
              className="modal-support"
              classNameContent="modal-support-content"
              onClose={this.onCloseModal}
              topContent
            >
              <SupportText />
            </ModalSupport>
          )}
        </NavBarBlock>
      </NavBar>
    );
  }
}

export default connect(
  mapStateToProps
)(AppNavBar);
