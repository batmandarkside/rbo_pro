import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import {
  SidebarItem,
  SidebarItemInner,
  SidebarText,
  SidebarIcon
} from './style.js';

/**
 *
 * @param icon
 * @param label
 * @param url
 * @param active
 */
const MenuItem = ({ item: { icon, label, url, active } }) => (
  <SidebarItem className={active ? 'active' : ''} to={url}>
    <SidebarItemInner>
      <SidebarIcon>
        <Icon type={icon} size="20" />
      </SidebarIcon>
      <SidebarText>
        {label}
      </SidebarText>
    </SidebarItemInner>
  </SidebarItem>
);

MenuItem.propTypes = {
  item: PropTypes.shape({
    active: PropTypes.bool,
    icon: PropTypes.string,
    label: PropTypes.string,
    url: PropTypes.string,
    isMouseEnterMenu: PropTypes.bool,
  })
};

export default MenuItem;
