import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { COLORS, FONTS, FONTS_SIZE, INDENTING_STEP } from 'assets/styles/style-vars.js';


export const SidebarItem = styled(Link)`
  display: block;
  position: relative; 
  box-sizing: border-box;
  width: ${INDENTING_STEP * 18}px;
  height: ${INDENTING_STEP * 18}px;
  padding: ${INDENTING_STEP * 2}px;
  line-height: ${INDENTING_STEP * 14}px;
  font-family: ${FONTS.Helvetica};
  font-size: ${FONTS_SIZE.fontSizeExtraSmall}; 
  text-align: center;
  text-decoration: none;
  color: ${COLORS.colorWhite};
  background-color: ${COLORS.colorGreyBrown};
  opacity: 0.5;
  transition-property: all; 
  transition-duration: 0.1s;
  transition-timing-function: ease-in-out;
    
  &:after {
    content: '';
    display: none;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    width: 4px;
    background-color: ${COLORS.colorSunflowerYellow};
  }
  
  &:hover {    
    opacity: 1;
    color: ${COLORS.colorWhite};
  }
   
  &.active {
    background-color: ${COLORS.colorBlack};
    opacity: 1;
    &:after {
      display: block;
    }
  }  
`;

export const SidebarItemInner = styled.div`
  line-height: normal;
  display: inline-block;
  vertical-align: middle;
`;

export const SidebarIcon = styled.div`
  display: block;
  height: ${INDENTING_STEP * 5}px;
  
  svg {
    fill: ${COLORS.colorWhite};
  }
`;

export const SidebarText = styled.div`   
  display: block;
  margin-top: ${INDENTING_STEP * 2}px;  
  line-height: ${INDENTING_STEP * 3}px;
`;
