import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import {
  SidebarItem,
  SidebarItemInner,
  SidebarText,
  SidebarIcon
} from './style.js';

const SupportItem = ({ onClick }) => (
  <SidebarItem to="#" onClick={onClick}>
    <SidebarItemInner>
      <SidebarIcon>
        <Icon type="menu-support" size="20" />
      </SidebarIcon>
      <SidebarText>
        Помощь
      </SidebarText>
    </SidebarItemInner>
  </SidebarItem>
);

SupportItem.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default SupportItem;
