import styled from 'styled-components';
import Modal from 'ui-components/modal';
import { rgba } from 'polished';
import { COLORS, FONTS_SIZE, INDENTING_STEP } from 'assets/styles/style-vars.js';

export const NavBar = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  width: ${INDENTING_STEP * 18}px;
  background: ${COLORS.colorGreyishBrown};
  box-shadow: ${INDENTING_STEP * 0.25}px 0 ${INDENTING_STEP * 2}px 0 rgba(0, 0, 0, 0.4);
  font-size: ${FONTS_SIZE.fontSizeNormal};
  color: ${COLORS.colorWhite};
  letter-spacing: 0.5px;
  z-index: 3;
  overflow: hidden;
`;


export const NavBarBlock = styled.nav`
  height: 100%;  
  overflow: hidden;
  font-size: ${FONTS_SIZE.fontSizeNormal};
  color: ${COLORS.colorWhite};
  letter-spacing: 0.5px;
  background: ${COLORS.colorGreyishBrown};
  box-shadow: 0 2px 4px 0 ${rgba(COLORS.colorBlack, 0.50)};
`;

export const NavBarLogo = styled.div`
  display: block;
  padding: 12px;
  position: relative; 
  box-sizing: border-box;
  width: ${INDENTING_STEP * 18}px;
  height: ${INDENTING_STEP * 18}px;
  box-shadow: 0 2px 4px 0 ${rgba(0, 0, 0, 0.8)};
  padding: ${INDENTING_STEP * 5}px;
  z-index: 1;
`;

export const NavBarSectionTop = styled.div`
  position: relative;
`;

export const NavBarSectionBottom = styled.div`
  position: absolute;
  bottom: 0; 
  left: 0; 
  right: 0; 
  border-top: 1px solid ${rgba(COLORS.colorWhite, 0.2)};
  background-color: ${COLORS.colorGreyBrown};
  z-index: 1;
`;

export const ModalSupport = styled(Modal)`
  .b-ui-modal__window {
    width: 100%;
    height: 100%;
    padding: 0 0 0 ${INDENTING_STEP * 18}px;
    min-height: 800px;
  }
  .b-ui-modal__window-content .b-modal-icon-close {
    bottom: 60px;
    top: inherit;
    right: 49%;
    border-radius: 50%;
    border: 2px solid;
    opacity: .6;
    &.b-icon{
        width: 45px;
        height: 45px;
        border-radius: 50%;
        border: 2px solid;
        opacity: .6;
        
        &.m-button.m-24 svg {
          top: 8px;
          left: 8px;
        }
    }
  }
  
  .modal-support-content {
    padding: 0 0 50px;
    width: 100%;
    height: 100%;
    color: ${COLORS.colorBlack};
    .wrap {
       text-align: center;
    }
    
    .msl1 {
      font-size: ${FONTS_SIZE.fontSizeH2Small};
      margin: 50px 0;
    }
    
    .msl2 {
      font-size: ${FONTS_SIZE.fontSizeNormal};
      margin: 0;
    }
    
    .msl3 {
      font-size: 64px;
      margin: 0 0 20px;
    }
    
    .msl4 {
      font-size: ${FONTS_SIZE.fontSizeNormal};
    }
    
    .msl5 {
      font-size: ${FONTS_SIZE.fontSizeH1};
      margin: 0 0 20px;
    }
    
    .msl6 {
      font-size: ${FONTS_SIZE.fontSizeNormal};
      color: ${COLORS.colorBlack};
      margin: 0 0 50px;
    }
    
    .coming-soon {
      background: ${rgba(COLORS.colorParchment, 0.6)};
      color: ${COLORS.colorBlack};
      text-align: center;
      padding: 50px;
      
      a {
        color: ${COLORS.colorPeacockBlue};
        border: none;
        padding: 0;
        margin: 0;
        background: none;
        text-transform: none;
        font-weight: bold;
        font-size: ${FONTS_SIZE.fontSizeH2};
        
        &:hover {
            border: none;
            background: none;
            color: ${COLORS.colorPeacockBlue};
        }
      }
      
      p {
        padding: 0;
        margin: 0;
      }
      
      .mslcs1 {
        font-weight: bold;
        font-size: ${FONTS_SIZE.fontSizeH2};
      }
      
      .mslcs2 {
        font-size: ${FONTS_SIZE.fontSizeLarge};
        padding-top: 25px;
      }
    }
    
    a {
      border: 2px solid ${COLORS.colorPeacockBlue};
      border-radius: 100px;
      font-size: ${FONTS_SIZE.fontSizeLarge};
      text-transform: uppercase;
      padding: 14px 44px;
      cursor: pointer;
      text-decoration: none;
      background: ${COLORS.colorPeacockBlue};
      color: ${COLORS.colorWhite};
    }

    
    .m-close {
      width: 50px;
      height: 50px;
      display: block;
      cursor: pointer;
      position: absolute;
      bottom: 100px;
      left: 50%;
      border: 0 none;
      transform: translateX(-20px);
    }
    .m-support-online {
      color: ${COLORS.colorTrueGreen};
    }
    .m-support-offline {
      color: ${COLORS.colorTomatoRed};
      display: block;
    }
  }
`;
