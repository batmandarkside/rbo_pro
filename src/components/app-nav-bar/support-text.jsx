import React from 'react';
import classnames from 'classnames';
import { isSupportWorkingHours } from 'utils';

const SupportText = () => {
  const isWorkingHours = isSupportWorkingHours();

  const classSelector = classnames({
    'm-support-online': isWorkingHours,
    'm-support-offline': !isWorkingHours
  });
  return (
    <div>
      <div className="coming-soon">
        <p className="mslcs1">
          Если вам требуются функции, которые мы еще не выпустили, воспользуйтесь <a href="https://elbrus.raiffeisen.ru/">ELBRUS Internet</a>
        </p>
        <p className="mslcs2">
          Для входа в ELBRUS Internet используйте ваш логин и пароль для Raiffeisen Business Online.
        </p>
      </div>
      <div className="wrap">
        <p className="msl1">Служба поддержки клиентов системы Банк-Клиент RBO</p>
        <p className="msl2">Для Москвы и Московской области</p>
        <p className="msl3">+7 (495) 225-91-92</p>
        <p className="msl2">Бесплатный номер для других городов</p>
        <p className="msl5">8 (800) 700-99-95</p>
        <p className="msl6">
          Время работы: с понедельника по пятницу с 3:00 до 19:00<br />по московскому времени&nbsp;
          <span className={classSelector}>
            {isWorkingHours
              ? '(сейчас работает)'
              : '(сегодня уже не работает, воспользуйтесь формой обратной связи)'
            }
          </span>
        </p>
        <a
          href="https://www.raiffeisen.ru/business/dist/form/"
          target="_blank"
          rel="noopener noreferrer"
        >Форма обратной связи</a>
      </div>
    </div>
  );
};

export default SupportText;
