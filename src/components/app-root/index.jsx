import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { withRouter } from 'react-router';
import { routes } from 'app/routes';
import { connect } from 'react-redux';
import * as NotificationAction from 'dal/notification/actions';
import * as ProfileAction from 'dal/profile/actions';
import * as OrganizationsAction from 'dal/organizations/actions';
import SignInitialize   from 'components/sign/sign-initialize';
import ConfirmInitialize from 'components/confirm/confirm-initialize';
import NotifyInitialize from 'components/notification/notify-initialize';
import AppHeader from 'components/app-header';
import AppNavBar from 'components/app-nav-bar';
import Loader from '@rbo/components/lib/loader/Loader';
import store from 'app/store';
import mapStateToProps from './selectors';
import './style.css';

class AppRoot extends Component {
  static propTypes = {
    location: PropTypes.object,
    globalLoaderShow: PropTypes.bool
  };

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { globalLoaderShow } = this.props;

    return (
      <div className="b-application__root">
        <AppHeader />
        <AppNavBar />
        <div className="b-application__content">
          {globalLoaderShow && <Loader centered />}
          <Switch>
            {routes.map((route, i) =>
              <Route {...route} key={i} />
            )}
          </Switch>
        </div>
        <SignInitialize store={store} />
        <ConfirmInitialize store={store} />
        <NotifyInitialize store={store} />
      </div>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    {
      ...NotificationAction,
      ...ProfileAction,
      ...OrganizationsAction
    }
  )(AppRoot)
);
