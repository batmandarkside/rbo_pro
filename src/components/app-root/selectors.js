import { createSelector } from 'reselect';
import { fromJS, List } from 'immutable';

const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
const settingsSelector = state => state.getIn(['profile', 'settings']);
const globalLoaderSelector = state => state.getIn(['rootReducer', 'globalLoaderShow']);
const currentLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions', 'currentLocation']);

const menuItems = fromJS([
  {
    id: 0,
    url: '',
    icon: 'menu-main',
    label: 'Главная',
    section: 'top'
  },
  {
    id: 1,
    url: '/r-payments',
    icon: 'menu-r-payments',
    label: 'Рублевые платежи',
    section: 'top'
  },
  {
    id: 2,
    url: '/cur-transfers',
    icon: 'menu-cur-transfers',
    label: 'Валютные переводы',
    section: 'top'
  },
  {
    id: 3,
    url: '/statements',
    icon: 'menu-statements',
    label: 'Выписки',
    section: 'top'
  },
  {
    id: 4,
    url: '/messages',
    label: 'Письма',
    icon: 'menu-messages',
    section: 'top'
  },
  {
    id: 5,
    url: '/settings',
    icon: 'menu-settings',
    label: 'Настройки',
    section: 'top'
  },
  {
    id: 6,
    url: '/tasks',
    icon: 'menu-tasks',
    label: 'Журнал заданий',
    section: 'bottom'
  }
]);

/**
 * Проходим по организациям и смотрим какие из них есть в
 *  settings -> selected_orgs
 *  { selected_orgs } организации выбранные пользователем
 * @param organizations
 * @param settings
 */
export const getSelectedOrg = createSelector(
  organizationsSelector,
  settingsSelector,
  (organizations, settings) => {
    const selected = organizations.filter(org => settings.get('selected_orgs').includes(org.get('id')));
    return selected ? selected.map(org => org.set('selected', true)) : new List([]);
  }
);

/**
 *
 * новый массив organizations для отображения в компоненте Header -> Organisation
 *  ставим свойство selected для организаций которые выбрал пользователь
 *
 * selectedOrgs -  массив id организаций - вида 2c85a962-bd82-4ed7-811f-d53040722de3
 *
 * @param organizations
 * @param settings
 */
export const organizationsMerge = createSelector(
  organizationsSelector,
  settingsSelector,
  (organizations, settings) => {
    const selectedOrgs = settings.get('selected_orgs');
    return organizations.map(org => org.set('selected', selectedOrgs.includes(org.get('id'))));
  }
);

// Учитываем дочерние роуты для всех url-ов, кроме корневого
const isActivePath = (url, currentPath) => (
  url === ''
    ? currentPath === '/'
    : currentPath.indexOf(url) === 0
);

export const setActiveMenu = createSelector(
  currentLocationSelector,
  currentLocation => menuItems.map(item => item.set('active', isActivePath(item.get('url'), currentLocation)))
);

/**
 * { selectedOrganization } новое свойство в profile для удобной работы в компонентах
 * @returns {function(*): {organizations: *, profile}}
 */
const mapStateToProps = (state) => {
  // выбранные организации
  const selectedOrganization = getSelectedOrg(state);

  // массив id выбранных организаций
  const selectedOrganizationIds = selectedOrganization.map(org => org.get('id'));

  // @todo буду изменения при разработке дизайна api
  // организации и организации из настроек пользователя, мержим их
  // такая логика из за того что есть отдельные сушности
  //   организации в настройках пользователя settings
  //   и сама сущность organizations
  const organizations = organizationsMerge(state);

  /**
   * organizations {Array} новая сущность
   *  организации и организации отмеченные пользователем
   */
  return {
    organizations,
    selectedOrganization,
    selectedOrganizationIds,
    profile: state.get('profile'),
    settings: state.get('settings'),
    fullName: state.getIn(['profile', 'fullName']),
    selectedOrganizationSize: selectedOrganization.size,
    globalLoaderShow: globalLoaderSelector(state),
    menuItems: setActiveMenu(state)
  };
};

export default mapStateToProps;
