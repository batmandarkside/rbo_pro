import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Confirm from './index'; // eslint-disable-line import/no-named-as-default

export default class ConfirmInitialize extends Component {
  constructor(props) {
    super(props);

    this.container = document.querySelector('#Confirm');

    if (!this.container) {
      this.container = document.createElement('div');
      this.container.setAttribute('id', 'Confirm');
      document.body.appendChild(this.container);
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this._render(this.props);
    }, 0);
  }

  shouldComponentUpdate() {
    return false;
  }


  componentWillUnmount() {
    ReactDOM.unmountComponentAtNode(this.container);
    document.body.removeChild(this.container);
  }

  /**
   *
   * @param props
   * @private
   */
  _render(props) {
    ReactDOM.render(
      <Provider store={props.store}>
        <Confirm {...props} dispatch={props.store.dispatch} />
      </Provider>,
      this.container
    );
  }

  render() { return null; }
}
