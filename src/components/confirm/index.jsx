import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'ui-components/modal';
import Icon from 'components/ui-components/icon';
import * as confirmActionCreators from 'dal/confirm/action-creators';
import mapStateToProps from './selectors';

import {
  ModalHeader,
  ModalHeaderTitle,
  ModalHeaderClose,
  ModalBody,
  ConfirmText,
  ButtonsRow,
  Button
} from './style';

function Confirm(props) {
  if (!props.visible) return null;

  const {
    title,
    message,
    successLabel,
    confirmSuccessAction,
    confirmCancelAction
  } = props;

  return (
    <Modal
      hideCloseButton
      onClose={confirmCancelAction}
      styleContent={{ width: '522px' }}
    >
      <ModalHeader>
        <ModalHeaderTitle>{title}</ModalHeaderTitle>
        <ModalHeaderClose>
          <Icon
            onClick={confirmCancelAction}
            type="close"
            size="24"
          />
        </ModalHeaderClose>
      </ModalHeader>
      <ModalBody>
        <ConfirmText>{message}</ConfirmText>
        <ButtonsRow>
          <Button
            small
            color="red"
            onClick={confirmSuccessAction}
          >
            {successLabel}
          </Button>
          <Button
            small
            color="white"
            onClick={confirmCancelAction}
          >
            Отмена
          </Button>
        </ButtonsRow>
      </ModalBody>
    </Modal>
  );
}

Confirm.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  successLabel: PropTypes.string,
  confirmSuccessAction: PropTypes.func,
  confirmCancelAction: PropTypes.func
};

export default connect(
  mapStateToProps,
  confirmActionCreators
)(Confirm);
