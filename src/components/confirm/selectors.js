import { createSelector } from 'reselect';

const confirmSelector = state => state.get('confirm');
const currentSelector = createSelector(confirmSelector, confirm => confirm.get('current'));

export default function mapStateToProps(state) {
  const currentConfirm = currentSelector(state);

  if (!currentConfirm) return { visible: false };

  const title = currentConfirm.get('title');
  const message = currentConfirm.get('message');
  const successLabel = currentConfirm.getIn(['success', 'label']);

  return {
    visible: true,
    title,
    message,
    successLabel
  };
}
