import styled from 'styled-components';

export const ModalHeader = styled.div`
  display: block;
  height: 64px;
  border-bottom: 1px solid #fcc900;
  position: relative;
`;

export const ModalHeaderTitle = styled.span`
  display: block;
  padding: 16px 32px;
  line-height: 32px;
  font-weight: bold;
  font-size: 20px;
  letter-spacing: .2px;
`;

export const ModalHeaderClose = styled.div`
  position: absolute;
  top: 12px;
  right: 24px;
  cursor: pointer;
`;

export const ModalBody = styled.div`
  padding: 8px 32px 24px;
`;

export const ConfirmText = styled.div`
  margin: 24px 0;
`;

export const Button = styled.div`
  ${(props) => {
    switch (props.color) {
      case 'white':
        return `
          color: #000000;
          border: 1px solid rgba(0, 0, 0, .2);
        `;
      case 'red':
        return `
          color: #ffffff;
          background: #D91E00;
          border: 1px solid #D91E00;
        `;
      default:
        return `
          background: #000000;
          color: #ffffff;
          border: 1px solid #000000;
        `;
    }
  }}
  font-size: 14px;
  opacity: .87;
  height: 44px;
  line-height: 42px;
  cursor: pointer;
  display: inline-block;
  width: ${props => (props.small ? 120 : 264)}px;
  text-align: center;
  position: relative;
  border-radius: 2px;

  &:not(:first-child) {
    margin-left: 24px;
  }
`;

export const ButtonsRow = styled.div`
  margin-top: 24px;
`;
