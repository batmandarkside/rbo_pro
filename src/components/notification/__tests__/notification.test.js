/* eslint-disable react/jsx-filename-extension */
import React                          from 'react';
import { expect }                     from 'chai';
import { List, fromJS }               from 'immutable';
import { mount }                      from 'enzyme';
import NotificationSystem             from 'react-notification-system-2';
import notifications                  from 'dal/notification/reducer';
import { Notification }               from '../index';


describe('Notification tests', () => {
  const defaultState = notifications(undefined, { type: null });

  const successNot = fromJS({
    type   : 'success',
    title  : 'success',
    message: 'success',
    uid    : '1111'
  });

  const warningNot = fromJS({
    type   : 'warning',
    title  : 'warning',
    message: 'warning',
    uid    : '1111'
  });


  const errorNot = fromJS({
    type   : 'error',
    title  : 'error',
    message: 'error',
    uid    : '1111',
  });

  const props = {
    getImportantMessagesAction   : jest.fn(),
    removeModalNotificationAction: jest.fn(),
    mapComponents                : {},
    selectedOrganizationIds      : List(),
  };

  const renderNotification = () =>
    mount(
      <Notification
        {...props}
        notifications={defaultState}
      />,
      document.body
    );

  it.skip('Renders <Notification />', () => {
    const wrapper = renderNotification();
    expect(wrapper).to.have.length(1);
    expect(wrapper.find(NotificationSystem)).to.have.length(1);
  });

  // renders ordinary success
  it.skip('Renders success <Notification />', () => {
    const wrapper = renderNotification();
    const state = defaultState.set('lastOrdinaryNotification', successNot);
    wrapper.setProps({ notifications: state });

    expect(wrapper.find('.notification-success')).to.have.length(1);
    expect(wrapper.find('.notification-message').text()).to.equal('success');
  });

  // renders ordinary warning
  it.skip('Renders warning <Notification />', () => {
    const wrapper = renderNotification();
    const state = defaultState.set('lastOrdinaryNotification', warningNot);
    wrapper.setProps({ notifications: state });
    expect(wrapper.find('.notification-message').text()).to.equal('warning');
  });


  // renders ordinary error
  it.skip('Renders error <Notification />', () => {
    const wrapper = renderNotification();
    const state = defaultState.set('lastOrdinaryNotification', errorNot);
    wrapper.setProps({ notifications: state });

    expect(wrapper.find('.notification-error')).to.have.length(1);
    expect(wrapper.find('.notification-message').text()).to.equal('error');
  });
});
