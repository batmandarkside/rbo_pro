import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export class ConfirmItem extends Component {
  static propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    labelSuccess: PropTypes.string.isRequired,
    labelCancel: PropTypes.string.isRequired,
    uid: PropTypes.string.isRequired,
    actionData: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.object,
      PropTypes.array
    ]),
    onClickOK: PropTypes.func.isRequired,
    onClickCancel: PropTypes.func,
    removeConfirmNotification: PropTypes.func.isRequired
  }

  state = {
    hideAnimate: false
  }

  componentDidMount() {
    setTimeout(this.showNotification, 0);
  }

  componentDidUpdate() {
    const hide = this.state.hideAnimate;

    if (hide) {
      this.deleteAfter(300);
    } else {
      this.hideAfter(10 * 1000);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.deleteTimeot);
    clearTimeout(this.hideTimeot);
  }

  onClickSuccessHandler = () => {
    const { onClickOK, actionData } = this.props;

    onClickOK(actionData);
    this.hide();
  }

  onClickCancelHandler = () => {
    const { onClickCancel, actionData } = this.props;

    onClickCancel && onClickCancel(actionData);
    this.hide();
  }

  showNotification = () => {
    this.setState({
      showAnimate: true
    });
  }

  hide = () => {
    this.setState({
      hideAnimate: true
    });
  }

  deleteAfter(ms) {
    const { removeConfirmNotification, uid } = this.props;

    clearTimeout(this.deleteTimeot);
    this.deleteTimeot = setTimeout(removeConfirmNotification.bind(this, uid), ms);
  }

  hideAfter(ms) {
    clearTimeout(this.hideTimeot);
    this.hideTimeot = setTimeout(this.hide, ms);
  }

  render() {
    const { title, text, labelSuccess, labelCancel } = this.props;

    const classSelectors = classNames({
      'confirm-item': true,
      'confirm-item-show-animate': this.state.showAnimate,
      'confirm-item-hide-animate': this.state.hideAnimate,
    });

    return (
      <div className={classSelectors}>
        <div className="confirm-item__title">{title}</div>
        <div className="confirm-item__text">{text}</div>
        <div className="confirm-item__buttons">
          <div className="confirm-item__button_success" onClick={this.onClickSuccessHandler}>{labelSuccess}</div>
          <div className="confirm-item__button_cancel" onClick={this.onClickCancelHandler}>{labelCancel}</div>
        </div>
      </div>
    );
  }
}
