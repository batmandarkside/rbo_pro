import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import {
  Confirm,
  Title,
  Message,
  Buttons,
  ButtonSuccess,
  ButtonCancel
} from './style.js';

class ConfirmNotify extends Component {
  static propTypes = {
    title: PropTypes.string,
    message: PropTypes.string,
    remove: PropTypes.func,
    success: PropTypes.shape({
      label: PropTypes.string,
      action: PropTypes.object.isRequired,
    }).isRequired,
    cancel: PropTypes.shape({
      label: PropTypes.string.isRequired,
      action: PropTypes.object,
    }),
    dispatch: PropTypes.func.isRequired
  }

  static defaultProps = {
    level: 'success',
  }

  _onSuccess = (evt) => {
    const { success, remove, dispatch } = this.props;
    evt.stopPropagation();
    if (success) {
      dispatch(success.action);
    }
    remove();
  }

  _onCancel = (evt) => {
    const { cancel, remove, dispatch } = this.props;
    evt.stopPropagation();
    if (cancel) {
      dispatch(cancel.action);
    }
    remove();
  }

  render() {
    const { title, message } = this.props;
    return (
      <Confirm>
        {title && <Title>{title}</Title>}
        {message && <Message>{message}</Message>}
        <ActionButtons
          {...this.props}
          onSuccess={this._onSuccess}
          onCancel={this._onCancel}
        />
      </Confirm>
    );
  }
}

/**
 *
 * @param success
 * @param cancel
 * @param onSuccess
 * @param onCancel
 */
const ActionButtons = ({ success, cancel, onSuccess, onCancel }) => (
  <Buttons>
    <ButtonSuccess primary onClick={onSuccess}>
      {(success.label) || 'Подтвердить'}
    </ButtonSuccess>

    {cancel && (
      <ButtonCancel onClick={onCancel}>
        {(cancel.label) || 'Отмена'}
      </ButtonCancel>
    )}
  </Buttons>
);

ActionButtons.propTypes = {
  success: PropTypes.shape({
    label: PropTypes.string,
    action: PropTypes.object.isRequired,
  }).isRequired,
  cancel: PropTypes.shape({
    label: PropTypes.string.isRequired,
    action: PropTypes.object,
  }),
  onSuccess: PropTypes.func,
  onCancel: PropTypes.func
};

export default ConfirmNotify;
