import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List } from 'immutable';
import * as NotificationsActions from 'dal/notification/actions';
import { ConfirmItem } from './confirm-item';
import './style.css';

const Confirm = props => (
  <div className="confirm-wrapper">
    {props.items.map(item => <ConfirmItem key={item.uid} {...item} {...props} />)}
  </div>
);

Confirm.propTypes = {
  items: PropTypes.instanceOf(List).isRequired
};

export default connect(state => ({
  items: state.getIn(['notifications', 'confirms'])
}), {
  ...NotificationsActions
})(Confirm);
