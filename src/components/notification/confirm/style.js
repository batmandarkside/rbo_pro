import styled from 'styled-components';
import Button from '@rbo/components/lib/button/Button';
import { rgba } from 'polished';
import { COLORS, FONTS, FONTS_SIZE } from 'assets/styles/style-vars.js';

export const Title = styled.div`
  font-family: ${FONTS.Helvetica};
  color: ${COLORS.colorBlack};
  opacity: 0.87;
  font-size: ${FONTS_SIZE.fontSizeNormal};
  font-weight: bold;
  margin-bottom: 12px; 
`;

export const Confirm = styled.div`
  &-success {
      background-color: var(--suceessNotification);
  }

  &-error {
      background-color: var(--errorNotification);
  }

  &-info {
      background-color: var(--infoNotification);
  } 
`;

export const Message = styled.div`
  font-size: ${FONTS_SIZE.fontSizeNormal};
  color: ${COLORS.colorBlack};
  opacity: 0.87;
  font-weight: normal;
  margin-bottom: 25px; 
`;

export const Buttons = styled.div`
  position: relative; 
`;

export const ButtonSuccess = styled(Button)`
  margin-right: 5px;
`;

export const ButtonCancel = styled(Button)`
  background-color: ${COLORS.colorWhite};
  border-radius: 2px;
  border: solid 1px ${rgba(COLORS.colorBlack, 0.2)};
  margin-left: 0;
  &:active {
    background-color: ${COLORS.colorWhite};
    border: solid 1px ${rgba(COLORS.colorBlack, 0.2)};
  }
`;
