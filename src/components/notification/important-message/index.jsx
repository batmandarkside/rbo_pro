import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment-timezone';
import { getFormattedFileSize, getMimeTypeTitleByMimeType } from 'utils';
import Icon from 'components/ui-components/icon';
import Button from '@rbo/components/lib/button/Button';
import Link from '@rbo/components/lib/link/Link';
import './style.css';

class ImportantMessage extends Component {
  static propTypes = {
    date: PropTypes.string.isRequired,
    topic: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    docId: PropTypes.string.isRequired,
    closeHandler: PropTypes.func.isRequired,
    receiverName: PropTypes.string,
    attachInfo: PropTypes.array,
    getAttachAction: PropTypes.func.isRequired
  };

  state = {
    isOpen: false
  }

  toggle = (e) => {
    e.preventDefault();
    this.setState({ isOpen: !this.state.isOpen });
  };

  renderAttach = (attach, index) => {
    const name = attach.name;
    const size = getFormattedFileSize(attach.size);
    const mimeTypeTitle = getMimeTypeTitleByMimeType(attach.mimeType);
    const created = moment(attach.date).format('DD.MM.YYYY');
    const describe = ` (${mimeTypeTitle})${size ? `, ${size}` : ''}${created ? `, создан: ${created}` : ''}`;
    const id = attach.id;
    const onClick = this.props.getAttachAction.bind(null, { id, name });

    return (
      <div key={index} className="b-important-message__attach">
        <div onClick={onClick} className="b-important-message__attach-icon">
          <Icon type="clip" size="16" />
        </div>
        <div className="b-important-message__attach-value">
          <span onClick={onClick} className="b-important-message__attach-name">{name}</span>
          <span className="b-important-message__attach-info">{describe}</span>
        </div>
      </div>
    );
  }


  render() {
    const {
      topic,
      message,
      receiverName,
      docId,
      closeHandler,
      attachInfo
    } = this.props;
    const date = moment(this.props.date).format('DD.MM.YYYY');
    const { isOpen } = this.state;

    const classSelector = classNames('b-important-message', {
      _open: isOpen
    });

    const tumbler = (
      <Link href="#" dashed className="b-important-message__tumbler" onClick={this.toggle}>
        { isOpen ? 'Скрыть' : 'Прочитать письмо'}
      </Link>
    );

    return (
      isOpen ? (
        <div className={classSelector}>
          <div className="b-important-message__meta">
            Отправлено <br />
            <span className="b-important-message__meta-value">{date}</span><br />
          </div>

          <div className="b-important-message__meta">
            Получатель <br />
            <span className="b-important-message__meta-value">{receiverName}</span>
          </div>

          <div className="b-important-message__message-body">
            <div className="b-important-message__title">{topic}</div>
            <div className="b-important-message__message">{message}</div>
            {attachInfo.map(this.renderAttach)}
          </div>

          <div className="b-important-message__actions">
            <Button primary onClick={() => closeHandler(docId)}>Закрыть</Button>
            <div className="button-tumbler">
              {tumbler}
            </div>
          </div>
        </div>
      ) : (
        <div className={classSelector}>
          <div className="b-important-message__title">{topic}</div>
          <div className="b-important-message__actions">
            {tumbler}
          </div>
        </div>
      )
    );
  }
}

export default ImportantMessage;
