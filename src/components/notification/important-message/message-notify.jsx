import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';
import { List }                     from 'immutable';
import Icon                         from 'components/ui-components/icon';
import * as NotificationAction      from 'dal/notification/actions';
import * as MessagesAction          from 'dal/messages/actions';
import ImportantMessage             from './index';
import Slider                       from '../slider';
import { selectors }                from '../selectors';
import '../style.css';

class ImportantMessageNotify extends Component {

  static propTypes = {
    messages: PropTypes.instanceOf(List).isRequired,
    uid: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
    remove: PropTypes.func.isRequired,
    closeImportantMessageAction: PropTypes.func.isRequired,
    getSingleMessageAction: PropTypes.func.isRequired,
    attachmentsAction: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      messages: this.props.messages.toJS(),
      uid: this.props.uid,
      remove: this.props.remove
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      messages: nextProps.messages.toJS()
    });
  }

  closeHandler = (docId) => {
    if (this.state.messages.length <= 1) {
      this.state.remove(this.state.uid);
    }
    this.props.getSingleMessageAction({ docId });  // mark as read by using side effect
    this.props.closeImportantMessageAction(docId);
  };

  render() {
    const { messages } = this.state;

    return (
      <div className="b-notify">
        <div className="b-notify__icon">
          <Icon type="message" size="24" />
        </div>
        <Slider>
          {messages.map(message => (
            <ImportantMessage
              {...message}
              key={message.docId}
              closeHandler={this.closeHandler}
              getAttachAction={this.props.attachmentsAction}
            />
          ))}
        </Slider>
      </div>
    );
  }
}

export default connect(
  selectors,
  dispatch => bindActionCreators({
    ...NotificationAction,
    ...MessagesAction
  }, dispatch)
)(ImportantMessageNotify);
