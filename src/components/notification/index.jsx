import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect }                  from 'react-redux';
import { Map, is }                  from 'immutable';
import NotificationSystem           from 'react-notification-system-2';
import * as NotificationAction      from 'dal/notification/actions';
import * as MessagesAction          from 'dal/messages/actions';
import ImportantMessageNotify       from './important-message/message-notify';
import ConfirmNotify                from './confirm/confirm-notify';
import MessagesListNotify           from './messages-list';
import { selectors }                from './selectors';
import './style.css';


const style = {
  NotificationItem: {
    DefaultStyle: {
      padding: '16px',
      margin: '0px',
      marginBottom: '10px',
      borderTop: 'none',
      boxShadow: 'none',
      borderRadius: '2px',
      order: 3,
      height: 'auto'
    }
  }
};

export const TIMEOUT_GET_NEW_MEGGAGES = (5 * 60 * 1000);


export class Notification extends Component {

  static propTypes = {
    notifications: PropTypes.instanceOf(Map),
    notifySettings: PropTypes.objectOf(PropTypes.func),
    getImportantMessagesAction: PropTypes.func.isRequired,
    removeOrdinaryNotificationAction: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  static defaultProps = {
    notifySettings: {
      base: () => ({
        level: 'warning',
        position: 'tr',
        autoDismiss: 4
      }),
      success: props => ({
        level: 'success',
        position: 'tr',
        autoDismiss: props.action ? 20 : 4
      }),
      error: () => ({
        level: 'error',
        position: 'tr',
        autoDismiss: 20
      }),
      warning: () => ({
        level: 'warning',
        position: 'tr',
        autoDismiss: 20
      }),
      important_message: props => ({
        position: 'tr',
        autoDismiss: 0,
        dismissible: false,
        getContentComponent: () => <ImportantMessageNotify {...props} />
      }),
      confirm: props => ({
        level: props.level || 'success',
        position: 'tr',
        autoDismiss: props.autoDismiss !== undefined ? props.autoDismiss : 20,
        getContentComponent: () => <ConfirmNotify {...props} />
      }),
      messagesList: props => ({
        level: props.level,
        position: 'tr',
        autoDismiss: 20,
        getContentComponent: () => <MessagesListNotify {...props} />
      })
    }
  }

  constructor(props) {
    super(props);
    this.NOTIFY = null;
    this.importantIsShown = false;
    this.timer = null;
    this.props.getImportantMessagesAction();

    if (!this.timer) {
      this.timer = setInterval(() => {
        this.props.getImportantMessagesAction();
      }, TIMEOUT_GET_NEW_MEGGAGES);
    }
  }

  /**
   * @todo При обновление компонента добавляем новую нотификацию
   *  основные нотификации в объекте ordinaryNotification
   *  importantMessages - это сообщения пользователя которые запрашиваем каждые 5 мин с сервера
   * */
  componentWillReceiveProps(nextProps) {
    if (!this.NOTIFY || !this.NOTIFY.addNotification) {
      return;
    }

    this.addImportantMessages(nextProps);
    this.addOrdinaryNotification(nextProps);
  }

  shouldComponentUpdate(nextProps) {
    return !is(nextProps.notifications, this.props.notifications);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  /**
   * сообощения приходят каждые 5 мин
   * проверка: показывать ли новое окно с нотификацией
   *
   * @param prevProps
   */
  addImportantMessages = (nextProps) => {
    const prevImportant = this.props.notifications.get('importantMessages');
    const nextImportant = nextProps.notifications.get('importantMessages');
    if (is(nextImportant, prevImportant) || this.importantIsShown) {
      return;
    }

    const componentProps = {
      ...nextImportant.toJS(),
      remove: (uid) => { // uid пробрасывается из компонента ImportantMessageNotify
        this.importantIsShown = !this.importantIsShown;
        this.NOTIFY.removeNotification(uid);
      }
    };

    this._getNotifySettingsAndShow(componentProps);
    this.importantIsShown = !this.importantIsShown;
  }

  /**
   *
   * @param uid
   * @private
   */
  _removeOrdinaryNotification = (uid) => {
    this.NOTIFY.removeNotification(uid);
  }  

  /**
   *
   * @param nextProps
   */
  addOrdinaryNotification = (nextProps) => {
    const prevNotification = this.props.notifications.get('lastOrdinaryNotification');
    const nextNotification = nextProps.notifications.get('lastOrdinaryNotification');
    if (!is(prevNotification, nextNotification) && nextNotification.size) {
      const componentProps = {
        ...nextNotification.toJS(),
        remove: () => {
          this._removeOrdinaryNotification(nextNotification.get('uid'));
        },
        onRemove: this._onRemove
      };
      this._getNotifySettingsAndShow(componentProps);
    }
  }
 
  /**
   * получаем компонент в зависимости от переданного типа
   * @param componentProps
   * @param notifyComponents
   *
   * @link https://github.com/batmandarkside/react-notification-system-2#getContentComponent
   */
  _getNotifySettingsAndShow = (componentProps) => {
    const { notifySettings, dispatch } = this.props;
    const { type } = componentProps;
    if (type && notifySettings[type]) {
      const settings = notifySettings[type]({ ...componentProps, dispatch });

      this.NOTIFY.addNotification({
        ...settings,
        ...componentProps
      });
    }
  }

  /**
   *
   * @param uid
   * @private
   */
  _removeOrdinaryNotification = (uid) => {
    this.NOTIFY.removeNotification(uid);
  }

  _onRemove = () => {
    this.props.removeOrdinaryNotificationAction();
  }


  render() {
    return (
      <div>
        <NotificationSystem
          ref={el => (this.NOTIFY = el)}
          style={style}
        />
      </div>
    );
  }
}

export default connect(
  selectors,
  {
    ...NotificationAction,
    ...MessagesAction
  }
)(Notification);
