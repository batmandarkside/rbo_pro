import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.css';

const MessagesListNotify = (props) => {
  const { collectionMessages, title, level } = props;
  const classSelector = classNames(
    'b-notify-messages-collection',
    `b-notify-messages-collection-${level}`
  );

  return (
    <div className={classSelector}>
      <div className="b-notify-messages-collection__title">
        {title}
      </div>
      <div className="b-notify-messages-collection__body">
        {collectionMessages
          .filter(error => error.text)
          .map((error, index) =>
            <div key={index} className="b-notify-messages-collection__item">
              <span className="b-notify-messages-collection__text">{error.text}</span>
            </div>
          )
        }
      </div>
    </div>
  );
};

MessagesListNotify.propTypes = {
  title: PropTypes.string.isRequired,
  level: PropTypes.string.isRequired,
  collectionMessages: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string
    })
  ).isRequired
};

export default MessagesListNotify;
