import React, { Component }       from 'react';
import ReactDOM                   from 'react-dom';
import { Provider }               from 'react-redux';
import Notification               from './index'; // eslint-disable-line import/no-named-as-default

/**
 * @todo рисуем Notify в body
 */
export default class NotifyInitialize extends Component {

  constructor(props) {
    super(props);
    this.div = document.querySelector('#Notification');
    if (!this.div) {
      this.div = document.createElement('div');
      this.div.setAttribute('id', 'Notification');
      document.body.appendChild(this.div);
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this._render(this.props);
    }, 0);
  }

  shouldComponentUpdate() {
    return false;
  }


  componentWillUnmount() {
    ReactDOM.unmountComponentAtNode(this.div);
    document.body.removeChild(this.div);
  }

  /**
   *
   * @param props
   * @private
   */
  _render(props) {
    ReactDOM.render(
      <Provider store={props.store}>
        <Notification {...props} dispatch={props.store.dispatch} />
      </Provider>,
      this.div
    );
  }

  render() {
    return null;
  }
}
