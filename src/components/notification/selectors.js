import { createSelector }         from 'reselect';
import { List }                   from 'immutable';

const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
const settingsSelector = state => state.getIn(['profile', 'settings']);
const messageItemsSelector = state => state.getIn(['notifications', 'importantMessages', 'messages']);

/**
 * Проходим по организациям и смотрим какие из них есть в
 *  settings -> selected_orgs
 *  { selected_orgs } организации выбранные пользователем
 * @param organizations
 * @param settings
 */
export const getSelectedOrg = createSelector(
  organizationsSelector,
  settingsSelector,
  (organizations, settings) => {
    const selected = organizations.filter(org => settings.get('selected_orgs').includes(org.get('id')));
    return selected ? selected.map(org => org.set('selected', true)) : new List([]);
  }
);

/**
 *
 * новый массив organizations для отображения в компоненте Header -> Organisation
 *  ставим свойство selected для организаций которые выбрал пользователь
 *
 * selectedOrgs -  массив id организаций - вида 2c85a962-bd82-4ed7-811f-d53040722de3
 *
 * @param organizations
 * @param settings
 */
export const organizationsMerge = createSelector(
  organizationsSelector,
  settingsSelector,
  (organizations, settings) => {
    const selectedOrgs = settings.get('selected_orgs');
    return organizations.map(org => org.set('selected', selectedOrgs.includes(org.get('id'))));
  }
);


/**
 *
 * @param {Array} messageItems
 * @param {Array} organizations
 * @return {Array}
 */
export const messageItemMemoize = (messageItems, organizations) => (
  messageItems.map((message) => {
    if ('receivers' in message) {
      const receivers = message.receivers;
      const receiver = organizations.find(o => o.get('id') === receivers[0]);
      const orgShortName = receivers.size > 1 ? 'Несколько получателей' : receiver && receiver.get('shortName');
      return Object.assign({}, message, { receiverName: orgShortName });
    }

    return message;
  })
);

/**
 * Подготавливаем новый список сообщений
 *  нужно проверить поле {receivers} - это список id организаций пользователя
 *  и достать {shortName} организации по этим id
 *
 * @param {Array} organizations
 * @returns {Selector<TInput, TOutput>}
 */
export const prepareMessage = createSelector(
  messageItemsSelector,
  organizationsSelector,
  (messageItems, organizations) =>
    messageItemMemoize(messageItems, organizations)
);

/**
 * { selectedOrganization } новое свойство в profile для удобной работы в компонентах
 * @returns {function(*): {organizations: *, profile}}
 */
export const selectors = (state) => {
  // выбранные организации
  const selectedOrganization = getSelectedOrg(state);

  // @todo буду изменения при разработке дизайна api
  // организации и организации из настроек пользователя, мержим их
  // такая логика из за того что есть отдельные сушности
  //   организации в настройках пользователя settings
  //   и сама сущность organizations
  const organizations = organizationsMerge(state);

  /**
   * organizations {Array} новая сущность
   *  организации и организации отмеченные пользователем
   */
  return {
    organizations,
    selectedOrganization,
    messages: prepareMessage(state),
    notifications: state.get('notifications')
  };
};

