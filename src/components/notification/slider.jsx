import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import './style.css';

class Slider extends Component {
  static propTypes = {
    children: PropTypes.array.isRequired
  };

  state = { currentIndex: 0 };

  componentWillReceiveProps(nextProps) {
    if (nextProps.children.length - 1 < this.state.currentIndex) {
      this.setState({ currentIndex: this.state.currentIndex - 1 });
    }
  }

  handler = (index) => {
    if (index < 0 || index >= this.props.children.length) return;
    this.setState({ currentIndex: index });
  };

  render() {
    const { children } = this.props;
    const { currentIndex } = this.state;

    const navigation = children.length > 1 ? (
      <div className="b-notify__navigation">
        <Icon type="arrow-left" onClick={() => this.handler(currentIndex - 1)} />
        <span> {currentIndex + 1} из {children.length} </span>
        <Icon type="arrow-right" onClick={() => this.handler(currentIndex + 1)} />
      </div>
    ) : null;

    return (
      <div>
        {children[this.state.currentIndex]}
        {navigation}
      </div>
    );
  }
}

export default Slider;
