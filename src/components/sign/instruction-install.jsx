import React from 'react';
import { TitleBorderBlock } from 'ui-components/title-border';
import { InstructionInstallText } from './instruction-install-text';


/**
 * инструкция по установки плагина
 * @constructor
 */
export const InstructionInstall = () => (
  <div className="b-sign-component-instructions">
    <TitleBorderBlock
      className="modal-signing-title"
      titleNotGrow
      title="Установка криптоплагина"
    />
    <div className="modal-signing-body">
      <InstructionInstallText />
    </div>
  </div>
);
