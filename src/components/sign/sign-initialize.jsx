import React, { Component }       from 'react';
import ReactDOM                   from 'react-dom';
import { Provider }               from 'react-redux';
import SignModal                  from './sign-modal';

/**
 * @todo рисуем Sign в body
 */
export default class SignInitialize extends Component {
  constructor(props) {
    super(props);
    this.div = document.querySelector('#SignContainer');
    if (!this.div) {
      this.div = document.createElement('div');
      this.div.setAttribute('id', 'SignContainer');
      document.body.appendChild(this.div);
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this._render(this.props);
    }, 0);
  }

  shouldComponentUpdate() {
    return false;
  }


  componentWillUnmount() {
    ReactDOM.unmountComponentAtNode(this.div);
    document.body.removeChild(this.div);
  }

  /**
   *
   * @param props
   * @private
   */
  _render(props) {
    ReactDOM.render(
      <Provider store={props.store}>
        <SignModal {...props} />
      </Provider>,
      this.div
    );
  }

  render() {
    return null;
  }
}
