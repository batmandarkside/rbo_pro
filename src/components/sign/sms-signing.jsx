import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'lodash';
import { connect }                 from 'react-redux';
import { Map, fromJS }             from 'immutable';
import * as SignAction             from 'dal/sign/actions';
import Button from '@rbo/components/lib/button/Button';
import Input from '@rbo/components/lib/form/Input';
import { ERRORS }                  from 'utils/errors';
import { secondsToTime }           from './util';
import { mapStateToProps }         from './selectors';

const wrongCredentialsError = ERRORS.sign.wrong_temporal_credentials;

class SmsSigning extends Component {
  static propTypes = {
    signCreateAction: PropTypes.func,
    signSmsDocumentAction: PropTypes.func,
    signSetSmsCodeAction: PropTypes.func,
    clearSignDataAction: PropTypes.func,
    cryptoProfileId: PropTypes.string.isRequired,
    error: PropTypes.instanceOf(Map),
  };

  static defaultProps = {
    createSignResult: fromJS({}),
    error: fromJS({
      message: ''
    }),
    cryptoProfileId: ''
  };

  state = {
    timeToExpire: '',
    value: '',
    timerId: null
  };

  componentDidMount() {
    setTimeout(this.focus, 0);
  }

  componentWillReceiveProps(nextProps) {
    const newData = nextProps.createSignResult.get('data');
    const errorMessage = nextProps.error.get('message');

    if (newData && !this.state.timerId) {
      this.setState({
        timeToExpire: newData,
        timerId: setInterval(this.timer, 1000)
      }, () => {
        setTimeout(this.focus, 0);
      });
    }

    if (errorMessage) {
      this.setState({
        value: ''
      }, () => {
        if (this.codeInput) {
          this.codeInput.value = '';
        }
      });
    }
    this.isTrySign = false;
  }

  componentWillUnmount() {
    clearInterval(this.state.timerId);
    this.setState({ timerId: null });
  }

  onSubmit = (e) => {            
    e.preventDefault();
    if (!this.isTrySign) {      
      this.isTrySign = true;
      this.sendSign();    
    }        
  }

  sendSign = debounce(() => {    
    this.props.signSetSmsCodeAction(this.state.value);
    this.props.signSmsDocumentAction();
  }, 500)

  timer = () => {
    if (this.state.timeToExpire > 0) {
      this.setState({ timeToExpire: this.state.timeToExpire - 1 });
    } else {
      clearInterval(this.state.timerId);
    }
  };

  handleChange = (e) => {
    this.setState({ value: e.target.value });
  }

  isTrySign = false;

  focus = () => {
    this.codeInput.focus();
  };


  _regenSmsCode = (e) => {
    e.preventDefault();
    const {
      cryptoProfileId,
      signCreateAction,
      clearSignDataAction,
    } = this.props;

    clearInterval(this.state.timerId);
    this.setState({ timerId: null });

    clearSignDataAction();

    signCreateAction({
      cryptoProfileId
    }, 'sms');
  }

  render() {
    const { timeToExpire, value } = this.state;

    const errorMessage = this.props.error.get('message');
    const errorIsNonBlocking = !errorMessage || (errorMessage === wrongCredentialsError);
    const isSendButtonDisable = !(value && value.length > 0) || (errorMessage && !errorIsNonBlocking) || !timeToExpire;

    return (
      <div className="b-sign__confirmation">
        <div className="b-sign__confirmation-text">Для подтверждения операции
          введите одноразовый пароль, который получили по SMS.</div>

        <div className="b-sign__confirmation-text _bold">
          { !!timeToExpire && `Пароль действителен: ${secondsToTime(timeToExpire)}` }
          { timeToExpire === 0 && 'Время действия кода истекло'} &nbsp;
        </div>
        <form onSubmit={this.onSubmit}>
          <div className="form-row">
            <Input
              ref={(input) => { this.codeInput = input; }}
              onChange={this.handleChange}
              maxLength={16}
              disabled={(errorMessage && !errorIsNonBlocking) || !timeToExpire}
              error={!!errorMessage}
              size="large"
            />

            <Button
              primary
              className="b-sign__accept"
              disabled={isSendButtonDisable}
              type="submit"
              size="large"
            >
              Подтвердить
            </Button>
          </div>
          {errorMessage && (
            <div className="b-sign__error-message">{errorMessage}</div>
          )}
          <div className="form-row">
            <Button
              disabled={errorIsNonBlocking && timeToExpire}
              onClick={this._regenSmsCode}
            >
              Получить код повторно
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  SignAction
)(SmsSigning);
