import React from 'react';
import PropTypes from 'prop-types';
import RboForm, {
  RboFormSection,
  RboFormRow,
  RboFormCell,
  RboFormError,
  RboFormFieldText,
  RboFormFieldButton
} from 'components/ui-components/rbo-form-compact';

const TemplateModalForm = (props) => {
  const { errorText, name, handleChange, onClose, onSubmit } = props;

  return (
    <RboForm
      initialFocusFieldId="nameTemplate"
      onSubmit={onSubmit}
    >
      <RboFormSection>
        <RboFormRow>
          <RboFormCell>
            <RboFormFieldText
              id="nameTemplate"
              label="Наименование шаблона"
              value={name}
              onChange={handleChange}
              isError={!!errorText}
              maxLength={255}
            />
            {errorText &&
            <RboFormError text={errorText} />
            }
          </RboFormCell>
        </RboFormRow>
      </RboFormSection>
      <RboFormSection>
        <RboFormRow>
          <RboFormCell size="1/2">
            <RboFormFieldButton
              align="left"
              id="saveAsTemplateCancel"
              title="Отменить"
              onClick={onClose}
            />
          </RboFormCell>
          <RboFormCell size="1/2">
            <RboFormFieldButton
              isPrimary
              align="right"
              id="saveAsTemplateSubmit"
              title="Сохранить"
              onClick={onSubmit}
            />
          </RboFormCell>
        </RboFormRow>
      </RboFormSection>
    </RboForm>
  );
};

TemplateModalForm.propTypes = {
  errorText: PropTypes.string,
  name: PropTypes.string,
  handleChange: PropTypes.func,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func
};

export default TemplateModalForm;
