import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'components/ui-components/modal';
import HeaderModal from 'components/ui-components/modal/header';
import ContentModal from 'components/ui-components/modal/content';
import * as TemplateActions from 'dal/template/actions';
import Form from './form';
import mapStateToProps from './selectors';
import './styles.css';

class TemplateModal extends Component {
  static propTypes = {
    showTemplateModal: PropTypes.bool.isRequired,
    showTemplateModalAction: PropTypes.func.isRequired,
    onSubmitAction: PropTypes.func.isRequired
  };

  state = {
    name: '',
    errorText: '',
  };

  componentWillReceiveProps(nextProps) {
    const { showTemplateModal } = this.props;
    const nextShowTemplateModal = nextProps.showTemplateModal;
    const nextSaveAsTemplateErrors = nextProps.saveAsTemplateErrors;

    if (nextSaveAsTemplateErrors.size) {
      this.setState({
        name: '',
        errorText: nextSaveAsTemplateErrors.first().get('text')
      });
    }

    if (showTemplateModal !== nextShowTemplateModal) {
      this.setState({
        name: '',
        errorText: ''
      });
    }
  }

  onClose = () => {
    const { showTemplateModalAction } = this.props;
    showTemplateModalAction(false);
  };

  onSubmit = () => {
    const { onSubmitAction } = this.props;
    const { name } = this.state;
    onSubmitAction(name);
  };

  handleChange = ({ fieldValue }) => {
    this.setState({ name: fieldValue });
  };


  render() {
    const { showTemplateModal } = this.props;
    const { errorText, name } = this.state;

    if (!showTemplateModal) {
      return null;
    }

    return (
      <Modal
        onClose={this.onClose}
        classNameWindow="p-template-modal"
      >
        <HeaderModal>
          Сохранение шаблона документа
        </HeaderModal>
        <ContentModal>
          {showTemplateModal &&
          <Form
            errorText={errorText}
            name={name}
            handleChange={this.handleChange}
            onClose={this.onClose}
            onSubmit={this.onSubmit}
          />}
        </ContentModal>
      </Modal>
    );
  }
}

export default connect(
  mapStateToProps,
  TemplateActions
)(TemplateModal);
