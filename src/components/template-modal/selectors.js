const showTemplateModalSelector = state => state.getIn(['template', 'showTemplateModal']);
const saveAsTemplateErrorsSelector = state => state.getIn(['template', 'errors']);

const mapStateToProps = state => ({
  showTemplateModal: showTemplateModalSelector(state),
  saveAsTemplateErrors: saveAsTemplateErrorsSelector(state),
});

export default mapStateToProps;
