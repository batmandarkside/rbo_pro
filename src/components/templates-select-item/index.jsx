import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import Icon from 'components/ui-components/icon';
import './style.css';

const TemplateSelectItem = (props) => {
  const { item, onEditClick } = props;

  const handleEditMouseDown = (e) => {
    e.stopPropagation();
  };

  const handleEditClick = (e) => {
    e.stopPropagation();
    onEditClick(item.get('id'));
  };

  return (
    <div className="b-template-select-item">
      <div className="b-template-select-item__content">
        <div className="b-template-select-item__title">
          {item.get('title')}
        </div>
        <div className="b-template-select-item__corr">
          {item.get('corr')}
        </div>
      </div>
      <div className="b-template-select-item__edit">
        <Icon
          type="edit"
          size="16"
          title="Редактировать шаблон"
          onMouseDown={handleEditMouseDown}
          onClick={handleEditClick}
        />
      </div>
    </div>
  );
};

TemplateSelectItem.propTypes = {
  item: PropTypes.instanceOf(Map).isRequired,
  onEditClick: PropTypes.func.isRequired
};


export default TemplateSelectItem;
