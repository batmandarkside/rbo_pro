import React from 'react';

const IconClip = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
    <defs>
      <path id="a" d="M0 0h20v20H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path stroke="#000" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.2" d="M13.96 14.184H5.498c-2.336 0-4.23-1.92-4.23-4.286 0-2.366 1.894-4.285 4.23-4.285h9.872c1.558 0 2.82 1.279 2.82 2.857 0 1.578-1.262 2.857-2.82 2.857H5.498a1.42 1.42 0 0 1-1.41-1.429 1.42 1.42 0 0 1 1.41-1.428h9.872" mask="url(#b)" transform="rotate(-45 10.434 9.184)" />
    </g>
  </svg>
);

export default IconClip;
