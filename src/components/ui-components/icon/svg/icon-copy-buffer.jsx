import React from 'react';

const IconCopyBuffer = () => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false">
    <g id="icon-copy-buffer">
      <path d="M5,19 L19,19 L19,5 L21,5 L21,21 L5,21 L5,19 Z M17,3 L17,17 L3,17 L3,3 L17,3 Z M5,15 L15,15 L15,5 L5,5 L5,15 Z" />
    </g>
  </svg>
);

export default IconCopyBuffer;
