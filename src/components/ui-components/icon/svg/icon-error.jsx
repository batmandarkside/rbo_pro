import React from 'react';

const IconError = () => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false">
    <g id="icon-error">
      <path d="M12,21 C7.02943725,21 3,16.9705627 3,12 C3,7.02943725 7.02943725,3 12,3 C16.9705627,3 21,7.02943725 21,12 C21,16.9705627 16.9705627,21 12,21 Z M16.5,14.8125 L13.6875,12 L16.5,9.1875 L14.8125,7.5 L12,10.3125 L9.1875,7.5 L7.5,9.1875 L10.3125,12 L7.5,14.8125 L9.1875,16.5 L12,13.6875 L14.8125,16.5 L16.5,14.8125 Z" />
    </g>
  </svg>
);

export default IconError;
