import React from 'react';

const IconMenuMain = () => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false">
    <g id="icon-menu-main">
      <path d="M3.6,18 L3.6,10.8 L6,10.8 L6,18 L3.6,18 Z M8.4,18 L8.4,10.8 L10.8,10.8 L10.8,18 L8.4,18 Z M13.2,18 L13.2,10.8 L15.6,10.8 L15.6,18 L13.2,18 Z M18,18 L18,10.8 L20.4,10.8 L20.4,18 L18,18 Z M2.4,20.4 L21.6,20.4 L21.6,22.8 L2.4,22.8 L2.4,20.4 Z M12,0 L24,8.4 L0,8.4 L12,0 Z M12,2.9292 L7.614,6 L16.386,6 L12,2.9292 Z" />
    </g>
  </svg>
);

export default IconMenuMain;
