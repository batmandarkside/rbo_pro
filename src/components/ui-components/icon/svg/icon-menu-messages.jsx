import React from 'react';

const IconMenuMessages = () => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false">
    <g id="icon-menu-messages">
      <path d="M21.6,2.4 L24,2.4 L24,21.6 L0,21.6 L0,2.4 L21.6,2.4 Z M2.4,19.2 L21.6012,19.2 L21.6,6.2136 L12.636,13.0176 C12.2467111,13.2602316 11.7532888,13.2602316 11.364,13.0176 L2.4,6.2148 L2.4,19.2 Z M19.3356,4.8 L4.6644,4.8 L12,10.584 L19.3356,4.8 Z" />
    </g>
  </svg>
);

export default IconMenuMessages;
