import React from 'react';

const IconMenuStatements = () => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false">
    <g id="icon-menu-statements">
      <path d="M4.8,2.4 L4.8,21.6 L19.2,21.6 L19.2,2.4 L4.8,2.4 Z M2.4,0 L21.6,0 L21.6,24 L2.4,24 L2.4,0 Z M7.2,4.8 L7.2,7.2 L16.8,7.2 L16.8,4.8 L7.2,4.8 Z M7.2,9.6 L7.2,12 L12,12 L12,9.6 L7.2,9.6 Z" />
    </g>
  </svg>
);

export default IconMenuStatements;
