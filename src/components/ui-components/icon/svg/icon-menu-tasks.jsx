import React from 'react';

const IconMenuTasks = () => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false">
    <g id="icon-menu-tasks">
      <path d="M1,0 L23,0 L23,24 L1,24 L1,0 Z M3.44444444,2.4 L3.44444444,21.6 L20.5555556,21.6 L20.5555556,2.4 L3.44444444,2.4 Z M12,2.4 L16.8888889,2.4 L16.8888889,13.2 L14.4664444,10.8 L12,13.2 L12,2.4 Z" />
    </g>
  </svg>
);

export default IconMenuTasks;
