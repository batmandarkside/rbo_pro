import React from 'react';

const IconRadioChecked = () => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false">
    <g id="icon-radio-checked">
      <circle cx="12" cy="12" r="6" />
    </g>
  </svg>
);

export default IconRadioChecked;
