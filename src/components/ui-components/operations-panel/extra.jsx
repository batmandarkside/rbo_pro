import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import classnames from 'classnames';
import Icon from 'ui-components/icon';
import PopupBox from 'ui-components/popup-box';
import Item from './item';

class OperationsPanelExtra extends Component {
  static propTypes = {
    operations: PropTypes.instanceOf(List).isRequired,
    onOperationClick: PropTypes.func.isRequired,
    containerElementSelector: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: false,
      isToggleActive: false
    };
  }

  handleToggleOnMouseDown = () => {
    this.setState({
      isToggleActive: true
    });
  };

  handleToggleOnMouseUp = () => {
    this.setState({
      isToggleActive: false
    });
  };

  handlePopupOpen = () => {
    this.setState({
      isPopupOpen: true
    });
  };

  handlePopupClose = () => {
    this.setState({
      isPopupOpen: false
    });
  };

  handleOperationsClick = (operationId) => {
    this.setState({
      isPopupOpen: false
    });
    this.props.onOperationClick(operationId);
  };

  render() {
    const { operations, containerElementSelector } = this.props;
    const { isPopupOpen, isToggleActive } = this.state;

    const toggleClassName = classnames(
      'b-operations-panel__extra-toggle',
      (isPopupOpen || isToggleActive) && 's-active'
    );

    return (
      <div className="b-operations-panel__extra">
        <div
          className={toggleClassName}
          ref={(element) => { this.popupOpenerElement = element; }}
          onClick={this.handlePopupOpen}
          onMouseDown={this.handleToggleOnMouseDown}
          onMouseUp={this.handleToggleOnMouseUp}
        >
          <div className="b-operations-panel__extra-toggle-icon">
            <Icon type="context-horizontal" size="24" />
          </div>
          <div className="b-operations-panel__extra-toggle-title">
            Еще
          </div>
        </div>
        {isPopupOpen &&
        <PopupBox
          openerElement={this.popupOpenerElement}
          containerElementSelector={containerElementSelector}
          onClose={this.handlePopupClose}
        >
          <div className="b-operations-panel__extra-popup">
            <div className="b-operations-panel__extra-items">
              {operations.map((operation, key) => (
                <Item
                  key={key}
                  id={operation.get('id')}
                  title={operation.get('title')}
                  icon={operation.get('icon')}
                  iconSize="16"
                  isDisabled={operation.get('disabled')}
                  isProgress={operation.get('progress')}
                  onClick={this.handleOperationsClick}
                />
              ))}
            </div>
          </div>
        </PopupBox>
        }
      </div>
    );
  }
}

export default OperationsPanelExtra;
