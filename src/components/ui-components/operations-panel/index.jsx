import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import Extra from './extra';
import Item from './item';
import './style.css';

class OperationsPanel extends Component {
  static propTypes = {
    operations: PropTypes.instanceOf(List).isRequired,
    onOperationClick: PropTypes.func.isRequired,
    containerElementSelector: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      offsetWidth: 0
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize);
    this.handleWindowResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
  }

  handleWindowResize = () => {
    const elementOffset = this.element.getBoundingClientRect();
    const offsetWidth = elementOffset.width || 0;

    this.setState({
      offsetWidth
    });
  };

  render() {
    const { operations, containerElementSelector, onOperationClick } = this.props;
    const { offsetWidth } = this.state;

    const operationQty = operations.size;
    const mainPlacesQty = parseInt(offsetWidth / 96, 10);
    const mainOperationQty = mainPlacesQty >= operationQty ? operationQty : mainPlacesQty - 1;
    const mainOperations = operations.slice(0, mainOperationQty);
    const extraOperations = operations.slice(mainOperationQty);

    return (
      <div className="b-operations-panel" ref={(element) => { this.element = element; }}>
        <div className="b-operations-panel__main">
          <div className="b-operations-panel__main-items">
            {mainOperations.map((operation, key) => (
              <Item
                key={key}
                id={operation.get('id')}
                title={operation.get('title')}
                icon={operation.get('icon')}
                iconSize="24"
                isDisabled={operation.get('disabled')}
                isProgress={operation.get('progress')}
                onClick={onOperationClick}
              />
            ))}
          </div>
        </div>
        {!!offsetWidth && !!extraOperations.size &&
          <Extra
            operations={extraOperations}
            onOperationClick={onOperationClick}
            containerElementSelector={containerElementSelector}
          />
        }
      </div>
    );
  }
}

export default OperationsPanel;
