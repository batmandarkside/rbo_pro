import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';

class OperationsPanelItem extends Component {
  static propTypes = {
    isDisabled: PropTypes.bool,
    isProgress: PropTypes.bool,

    id: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    iconSize: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,

    onClick: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isActive: false
    };
  }

  handleOnMouseDown = () => {
    const { isDisabled, isProgress } = this.props;
    if (!isDisabled && !isProgress) {
      this.setState({
        isActive: true
      });
    }
  };

  handleOnMouseUp = () => {
    this.setState({
      isActive: false
    });
  };

  handleOnClick = () => {
    const { isDisabled, isProgress, id, onClick } = this.props;
    if (!isDisabled && !isProgress) onClick(id);
  };

  render() {
    const { isDisabled, isProgress, icon, iconSize, title } = this.props;
    const { isActive } = this.state;

    const elementClassName = classnames(
      'b-operations-panel__item',
      isActive && 's-active',
      isDisabled && 's-disabled',
      isProgress && 's-progress',
    );

    return (
      <div
        className={elementClassName}
        onMouseDown={this.handleOnMouseDown}
        onMouseUp={this.handleOnMouseUp}
        onClick={this.handleOnClick}
      >
        <div className="b-operations-panel__item-icon">
          <Icon type={icon} size={iconSize} />
        </div>
        <div className="b-operations-panel__item-title">
          {title}
        </div>
      </div>
    );
  }
}

export default OperationsPanelItem;
