import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentBody = (props) => {
  const { children } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__body`}>
      {children}
    </div>
  );
};

RboDocumentBody.propTypes = {
  children: PropTypes.node.isRequired
};

export default RboDocumentBody;
