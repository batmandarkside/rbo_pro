export const COMPONENT_STYLE_NAME = 'b-document-compact';
export const COMPONENT_CONTENT_ELEMENT_SELECTOR = `.${COMPONENT_STYLE_NAME}__content`;
