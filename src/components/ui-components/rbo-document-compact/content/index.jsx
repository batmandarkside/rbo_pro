import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentContent = (props) => {
  const { children } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__content`}>
      {children}
    </div>
  );
};

RboDocumentContent.propTypes = {
  children: PropTypes.node.isRequired
};

export default RboDocumentContent;
