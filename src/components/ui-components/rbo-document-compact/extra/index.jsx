import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboDocumentExtra extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  state = {
    width: null,
    top: null,
    left: null,
    innerHeight: null,
    scrollTop: null,
    clientHeight: null
  };

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize);
    document.addEventListener('scroll', this.handleDocumentScroll);
    document.addEventListener('click', this.handleDocumentClick);
    setTimeout(this.setOffset, 0);
  }

  componentWillReceiveProps() {
    setTimeout(this.setOffset, 0);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
    document.addEventListener('scroll', this.handleDocumentScroll);
  }

  getElementBoundingClientRect = () => (
    this.element ? this.element.getBoundingClientRect() : null
  );

  getInnerElementBoundingClientRect = () => (
    this.innerElement ? this.innerElement.getBoundingClientRect() : null
  );

  setOffset = () => {
    const elementOffset = this.getElementBoundingClientRect();
    const innerElementOffset = this.getInnerElementBoundingClientRect();
    if (elementOffset) {
      if (this.state.top === null) {
        this.setState({
          top: elementOffset.top
        });
      }
      this.setState({
        width: elementOffset.width,
        left: elementOffset.left,
        innerHeight: innerElementOffset.height,
        scrollTop: window.pageYOffset,
        clientHeight: document.documentElement.clientHeight
      });
    }
  };

  handleWindowResize = () => {
    this.setOffset();
  };

  handleDocumentScroll = () => {
    this.setOffset();
  };

  handleDocumentClick = () => {
    this.setOffset();
  };

  render() {
    const { children } = this.props;
    const { width, left, top, innerHeight, scrollTop, clientHeight } = this.state;
    let calcTop = top;
    if (clientHeight - top < innerHeight) {
      calcTop = top - scrollTop;
      if (clientHeight - top + scrollTop > innerHeight) calcTop = clientHeight - innerHeight;
    }
    const elementStyle = innerHeight ? { minHeight: innerHeight } : {};
    const innerElementStyle = width ? { position: 'fixed', width, left, top: calcTop } : {};

    return (
      <div
        className={`${COMPONENT_STYLE_NAME}__extra`}
        ref={(element) => { this.element = element; }}
        style={elementStyle}
      >
        <div
          className={`${COMPONENT_STYLE_NAME}__extra-inner`}
          ref={(element) => { this.innerElement = element; }}
          style={innerElementStyle}
        >
          {children}
        </div>
      </div>
    );
  }
}

export default RboDocumentExtra;

export RboDocumentExtraSection from './section';

