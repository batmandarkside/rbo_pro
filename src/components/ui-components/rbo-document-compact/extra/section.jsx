import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboDocumentExtraSection extends Component {
  static propTypes = {
    title: PropTypes.string,
    isCollapsible: PropTypes.bool,
    isCollapsed: PropTypes.bool,
    onCollapseToggle: PropTypes.func,
    children: PropTypes.node.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isCollapsed: props.isCollapsed
    };
  }

  toggleCollapse = () => {
    this.setState({ isCollapsed: !this.state.isCollapsed });
    if (this.props.onCollapseToggle) this.props.onCollapseToggle({ isCollapsed: !this.state.isCollapsed });
  };

  render() {
    const { title, isCollapsible, children } = this.props;
    const { isCollapsed } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__extra-section`,
      isCollapsed && 's-collapsed'
    );

    return (
      <div className={elementClassName}>
        {title &&
        <div className={`${COMPONENT_STYLE_NAME}__extra-section-header`}>
          <div className={`${COMPONENT_STYLE_NAME}__extra-section-title`}>
            {isCollapsible ?
              <h3>
                <span
                  title={isCollapsed ? 'развернуть' : 'свернуть'}
                  onClick={this.toggleCollapse}
                  className="m-dashed-link"
                >
                  {title}
                </span>
                <Icon type={isCollapsed ? 'arrow-down' : 'arrow-up'} size="16" />
              </h3>
              :
              <h3>{title}</h3>
            }
          </div>
        </div>
        }
        <div className={`${COMPONENT_STYLE_NAME}__extra-section-content`}>
          {children}
        </div>
      </div>
    );
  }
}

export default RboDocumentExtraSection;
