import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentHead = (props) => {
  const { title, children, isTemplate } = props;
  const componentClass = classNames(
    `${COMPONENT_STYLE_NAME}__head`,
    isTemplate && 'm-template'
  );

  return (
    <div className={componentClass}>
      <div className={`${COMPONENT_STYLE_NAME}__head-title`}>
        <h2>
          {title}
        </h2>
      </div>
      {children &&
      <div className={`${COMPONENT_STYLE_NAME}__head-inner`}>
        {children}
      </div>
      }
    </div>
  );
};

RboDocumentHead.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
  isTemplate: PropTypes.bool
};

export default RboDocumentHead;

export RboDocumentHeadStatus from './status';
export RboDocumentHeadTabs from './tabs';
