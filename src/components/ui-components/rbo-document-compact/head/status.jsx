import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentHeadStatus = (props) => {
  const { date, status, statusClassName, describe } = props;

  const statusValueElementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__head-status-value`,
    statusClassName && statusClassName
  );

  return (
    <div className={`${COMPONENT_STYLE_NAME}__head-status`}>
      {describe &&
      <span className={`${COMPONENT_STYLE_NAME}__head-status-item`}>
        <span className={`${COMPONENT_STYLE_NAME}__head-status-value`}>
          {describe}
        </span>
      </span>
      }
      {date &&
      <span className={`${COMPONENT_STYLE_NAME}__head-status-item`}>
        <span className={`${COMPONENT_STYLE_NAME}__head-status-label`}>
          От
        </span>
        <span className={`${COMPONENT_STYLE_NAME}__head-status-value`}>
          {date}
        </span>
      </span>
      }
      {status &&
      <span className={`${COMPONENT_STYLE_NAME}__head-status-item`}>
        <span className={`${COMPONENT_STYLE_NAME}__head-status-label`}>
          Статус:
        </span>
        <span className={statusValueElementClassName}>
          {status}
        </span>
      </span>
      }
    </div>
  );
};

RboDocumentHeadStatus.propTypes = {
  date: PropTypes.string,
  status: PropTypes.string,
  statusClassName: PropTypes.string,
  describe: PropTypes.string
};

export default RboDocumentHeadStatus;
