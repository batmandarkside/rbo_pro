import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import RboTabs  from 'components/ui-components/rbo-tabs';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentHeadTabs = (props) => {
  const { tabs, onTabClick } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__head-tabs`}>
      <RboTabs
        tabs={tabs}
        align="right"
        onTabClick={onTabClick}
        containerElementSelector={`.${COMPONENT_STYLE_NAME}__main`}
      />
    </div>
  );
};

RboDocumentHeadTabs.propTypes = {
  tabs: PropTypes.instanceOf(List).isRequired,
  onTabClick: PropTypes.func.isRequired
};

export default RboDocumentHeadTabs;
