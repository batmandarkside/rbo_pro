import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';

const RboDocument = (props) => {
  const { children } = props;

  return (
    <div className={COMPONENT_STYLE_NAME}>
      {children}
    </div>
  );
};

RboDocument.propTypes = {
  children: PropTypes.node.isRequired
};

export default RboDocument;

export RboDocumentPanel from './panel';
export RboDocumentOperations from './operations';
export RboDocumentContent from './content';
export RboDocumentMain from './main';
export RboDocumentTop, { RboDocumentTopHistory, RboDocumentTopSignatures } from './top';
export RboDocumentHead, { RboDocumentHeadStatus, RboDocumentHeadTabs } from './head';
export RboDocumentBody from './body';
export RboDocumentExtra, { RboDocumentExtraSection } from './extra';
export RboDocumentNotes from './notes';
export RboDocumentInfo from './info';
export RboDocumentTemplates from './templates';
export RboDocumentStructure from './structure';
export RboDocumentLinkedDocs from './linked-docs';

export { COMPONENT_CONTENT_ELEMENT_SELECTOR } from './constants';
