import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentInfo = (props) => {
  const { items } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__info`}>
      <div className={`${COMPONENT_STYLE_NAME}__info-items`}>
        {items && !!items.length && items.map((item, key) => (
          <div
            className={`${COMPONENT_STYLE_NAME}__info-item`}
            key={key}
          >
            <div className={`${COMPONENT_STYLE_NAME}__info-item-label`}>
              {item.label}
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__info-item-value`}>
              {item.value}
            </div>
          </div>
        ))}
        {(!items || !items.length) &&
        <div className={`${COMPONENT_STYLE_NAME}__info-empty`}>
          <div className={`${COMPONENT_STYLE_NAME}__info-empty-icon`}>
            <Icon type="document" size="48" />
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__info-empty-title`}>
            Дополнительная информация для документа отсутствует
          </div>
        </div>
        }
      </div>
    </div>
  );
};

RboDocumentInfo.propTypes = {
  items: PropTypes.array
};

export default RboDocumentInfo;
