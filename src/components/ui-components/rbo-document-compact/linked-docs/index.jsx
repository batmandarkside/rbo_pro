import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { COMPONENT_STYLE_NAME } from '../constants';
import Item from './item';

const RboDocumentLinkedDocs = (props) => {
  const { docs, onDocClick } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__linked-docs`}>
      <div className={`${COMPONENT_STYLE_NAME}__linked-docs-items`}>
        {docs.map((doc, key) => (
          <Item
            key={key}
            id={doc.get('id')}
            title={doc.get('title')}
            onClick={onDocClick}
          />
        ))}
      </div>
    </div>
  );
};

RboDocumentLinkedDocs.propTypes = {
  docs: PropTypes.instanceOf(List).isRequired,
  onDocClick: PropTypes.func.isRequired
};

export default RboDocumentLinkedDocs;
