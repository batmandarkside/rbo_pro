import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentLinkedDocsItem = (props) => {
  const { id, title, onClick } = props;

  const handleClick = () => onClick({ docId: id });

  return (
    <div
      className={`${COMPONENT_STYLE_NAME}__linked-docs-item`}
      onClick={handleClick}
    >
      <span className="m-like-link">
        {title}
      </span>
    </div>
  );
};

RboDocumentLinkedDocsItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default RboDocumentLinkedDocsItem;
