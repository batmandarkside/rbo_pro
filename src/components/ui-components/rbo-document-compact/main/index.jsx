import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentMain = (props) => {
  const { children } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__main`}>
      <div className={`${COMPONENT_STYLE_NAME}__main-inner`}>
        {children}
      </div>
    </div>
  );
};

RboDocumentMain.propTypes = {
  children: PropTypes.node.isRequired
};

export default RboDocumentMain;
