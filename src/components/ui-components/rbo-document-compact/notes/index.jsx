import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import RboForm, {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRow,
  RboFormCell,
  RboFormHint,
  RboFormFieldTextarea
} from 'components/ui-components/rbo-form-compact';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentNotes = (props) => {
  const { isEditable, fieldId, value, onChange } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__notes`}>
      {isEditable &&
      <div className={`${COMPONENT_STYLE_NAME}__notes-form`}>
        <RboForm>
          <RboFormSection id="notes">
            <RboFormBlockset>
              <RboFormBlock isWide>
                <RboFormRow>
                  <RboFormCell>
                    <RboFormFieldTextarea
                      id={fieldId}
                      value={value}
                      maxLength={4000}
                      onChange={onChange}
                      placeholder="Пример: Платеж за аренду офиса"
                    />
                  </RboFormCell>
                </RboFormRow>
                <RboFormRow>
                  <RboFormCell>
                    <RboFormHint>
                      По заметкам удобно искать документы из списка платежей с использованием фильтров.
                      Заметки к документу не передаются в банк и сохраняются только у клиента.
                    </RboFormHint>
                  </RboFormCell>
                </RboFormRow>
              </RboFormBlock>
            </RboFormBlockset>
          </RboFormSection>
        </RboForm>
      </div>
      }
      {!isEditable && value &&
      <div className={`${COMPONENT_STYLE_NAME}__notes-value`}>
        {value}
      </div>
      }
      {!isEditable && !value &&
      <div className={`${COMPONENT_STYLE_NAME}__notes-empty`}>
        <div className={`${COMPONENT_STYLE_NAME}__notes-empty-icon`}>
          <Icon type="document" size="48" />
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__notes-empty-title`}>
          Заметки для документа отсутствуют
        </div>
      </div>
      }
    </div>
  );
};

RboDocumentNotes.propTypes = {
  isEditable: PropTypes.bool,
  fieldId: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

export default RboDocumentNotes;
