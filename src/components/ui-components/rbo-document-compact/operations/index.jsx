import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import OperationsPannel from 'components/ui-components/operations-panel';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentOperations = (props) => {
  const { operations, operationAction } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__operations`}>
      <OperationsPannel
        operations={operations}
        containerElementSelector={`.${COMPONENT_STYLE_NAME}`}
        onOperationClick={operationAction}
      />
    </div>
  );
};

RboDocumentOperations.propTypes = {
  operations: PropTypes.instanceOf(List).isRequired,
  operationAction: PropTypes.func.isRequired
};

export default RboDocumentOperations;
