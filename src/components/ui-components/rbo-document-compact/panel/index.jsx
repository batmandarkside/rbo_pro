import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboDocumentPanel extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  state = {
    width: null,
    height: null,
    top: null,
    left: null
  };

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize);
    document.addEventListener('scroll', this.handleDocumentScroll);
    setTimeout(this.setOffset, 0);
  }

  componentWillReceiveProps() {
    setTimeout(this.setOffset, 0);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
    document.addEventListener('scroll', this.handleDocumentScroll);
  }

  getBoundingClientRect = () => (this.element ? this.element.getBoundingClientRect() : null);

  setOffset = () => {
    const elementOffset = this.getBoundingClientRect();
    if (elementOffset) {
      if (this.state.height === null && this.state.top === null) {
        this.setState({
          height: elementOffset.height,
          top: elementOffset.top,
        });
      }
      this.setState({
        width: elementOffset.width,
        left: elementOffset.left
      });
    }
  };

  handleWindowResize = () => {
    this.setOffset();
  };

  handleDocumentScroll = () => {
    this.setOffset();
  };

  render() {
    const { children } = this.props;
    const { width, height, left, top } = this.state;
    const elementStyle = height ? { height } : {};
    const innerElementStyle = width ? { position: 'fixed', width, left, top } : {};

    return (
      <div
        className={`${COMPONENT_STYLE_NAME}__panel`}
        ref={(element) => { this.element = element; }}
        style={elementStyle}
      >
        <div
          className={`${COMPONENT_STYLE_NAME}__panel-inner`}
          style={innerElementStyle}
        >
          {children}
        </div>
      </div>
    );
  }
}

export default RboDocumentPanel;
