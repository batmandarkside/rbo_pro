import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentStructureError = (props) => {
  const { fieldsIds, tabId, sectionId, text, level, isActive, onClick, isClickable } = props;

  const handleErrorMouseDown = () => (isClickable && setTimeout(() => { onClick({ tabId, sectionId, fieldsIds }); }, 0));

  const iconType = () => {
    switch (level) {
      case '1': return 'warning';
      case '2': return 'error';
      case '3': return 'blocker';
      default: return 'error';
    }
  };
  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__structure-error`,
    isClickable && 'm-clickable',
    isActive && 's-active'
  );

  return (
    <div
      className={elementClassName}
      onMouseDown={handleErrorMouseDown}
    >
      <div className={`${COMPONENT_STYLE_NAME}__structure-error-icon`}>
        <Icon type={iconType()} size="16" />
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__structure-error-label`}>
        {text}
      </div>
    </div>
  );
};

RboDocumentStructureError.propTypes = {
  fieldsIds: PropTypes.array.isRequired,
  tabId: PropTypes.string.isRequired,
  sectionId: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  level: PropTypes.string,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  isClickable: PropTypes.bool
};

export default RboDocumentStructureError;
