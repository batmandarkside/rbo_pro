import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { COMPONENT_STYLE_NAME } from '../constants';
import Tab from './tab';

const RboDocumentStructure = (props) => {
  const { structure, onTabClick, onSectionClick, onErrorClick, isErrorClickable } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__structure`}>
      {structure.map((item, key) => (
        <Tab
          key={key}
          id={item.get('id')}
          title={item.get('title')}
          sections={item.get('sections')}
          isActive={item.get('isActive')}
          onClick={onTabClick}
          onSectionClick={onSectionClick}
          onErrorClick={onErrorClick}
          isErrorClickable={isErrorClickable}
        />
      ))}
    </div>
  );
};

RboDocumentStructure.propTypes = {
  isErrorClickable: PropTypes.bool,
  structure: PropTypes.instanceOf(List).isRequired,
  onTabClick: PropTypes.func.isRequired,
  onSectionClick: PropTypes.func.isRequired,
  onErrorClick: PropTypes.func.isRequired
};

export default RboDocumentStructure;
