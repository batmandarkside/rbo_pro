import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import Error from './error';

const RboDocumentStructureSection = (props) => {
  const { id, tabId, title, errors, isActive, onClick, onErrorClick, isErrorClickable } = props;

  const handleSectionMouseDown = () => setTimeout(() => { onClick({ tabId, sectionId: id }); }, 0);

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__structure-section`,
    isActive && 's-active'
  );

  return (
    <div className={elementClassName}>
      <div className={`${COMPONENT_STYLE_NAME}__structure-section-header`}>
        <span className={`${COMPONENT_STYLE_NAME}__structure-section-title`}>
          <span
            className="m-dashed-link"
            onMouseDown={handleSectionMouseDown}
          >
            {title}
          </span>
        </span>
      </div>
      {!!errors && !!errors.size &&
      <div className={`${COMPONENT_STYLE_NAME}__structure-section-content`}>
        {errors.map((item, key) => (
          <Error
            key={key}
            fieldsIds={item.get('fieldsIds').toJS()}
            tabId={tabId}
            sectionId={id}
            text={item.get('text')}
            level={item.get('level')}
            isActive={item.get('isActive')}
            onClick={onErrorClick}
            isClickable={isErrorClickable}
          />
        ))}
      </div>
      }
    </div>
  );
};

RboDocumentStructureSection.propTypes = {
  id: PropTypes.string.isRequired,
  tabId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  errors: PropTypes.instanceOf(List),
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  onErrorClick: PropTypes.func.isRequired,
  isErrorClickable: PropTypes.bool
};

export default RboDocumentStructureSection;
