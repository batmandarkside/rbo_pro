import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import Section from './section';

const RboDocumentStructureTab = (props) => {
  const { id, title, sections, isActive, onClick, onSectionClick, onErrorClick, isErrorClickable } = props;

  const handleTabMouseDown = () => (!isActive && setTimeout(() => { onClick({ tabId: id }); }, 0));

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__structure-tab`,
    isActive && 's-active'
  );

  return (
    <div className={elementClassName}>
      <div className={`${COMPONENT_STYLE_NAME}__structure-tab-header`}>
        <span
          className={`${COMPONENT_STYLE_NAME}__structure-tab-title`}
          onMouseDown={handleTabMouseDown}
        >
          {title}
        </span>
      </div>
      {sections &&
      <div className={`${COMPONENT_STYLE_NAME}__structure-tab-content`}>
        {sections.filter(item => item.get('isVisible')).map((item, key) => (
          <Section
            key={key}
            id={item.get('id')}
            tabId={id}
            title={item.get('title')}
            errors={item.get('errors')}
            isActive={item.get('isActive')}
            onClick={onSectionClick}
            onErrorClick={onErrorClick}
            isErrorClickable={isErrorClickable}
          />
        ))}
      </div>
      }
    </div>
  );
};

RboDocumentStructureTab.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  sections: PropTypes.instanceOf(List),
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  onSectionClick: PropTypes.func.isRequired,
  onErrorClick: PropTypes.func.isRequired,
  isErrorClickable: PropTypes.bool
};

export default RboDocumentStructureTab;
