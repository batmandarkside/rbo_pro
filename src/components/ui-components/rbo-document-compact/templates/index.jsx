import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { RboFormFieldPopupOptionInner, RboFormFieldSearch } from 'components/ui-components/rbo-form-compact';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentTemplates = (props) => {
  const { dictionary, popupContainerSelector, onFieldSearch, onFieldChange, linkToTemplates } = props;

  const formFieldOptionRenderer = (fieldId, option, highlight) => (
    <RboFormFieldPopupOptionInner
      label={option && option.title}
      describe={option && option.correspondentName}
      editLink={option && option.recordID && `${linkToTemplates}/${option.recordID}`}
      highlight={{
        value: highlight,
        inLabel: option && option.title,
        inDescribe: option && option.correspondentName
      }}
    />
  );

  return (
    <div className={`${COMPONENT_STYLE_NAME}__templates`}>
      <RboFormFieldSearch
        id="templatesSelect"
        inputType="Text"
        searchValue={dictionary.get('searchValue')}
        options={dictionary.get('items').toJS()}
        optionRenderer={formFieldOptionRenderer}
        popupContainerElementSelector={popupContainerSelector}
        isSearching={dictionary.get('isSearching')}
        onSearch={onFieldSearch}
        onChange={onFieldChange}
        placeholder="Выберите шаблон"
      />
    </div>
  );
};

RboDocumentTemplates.propTypes = {
  dictionary: PropTypes.instanceOf(Map).isRequired,
  popupContainerSelector: PropTypes.string,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  linkToTemplates: PropTypes.string.isRequired
};

export default RboDocumentTemplates;
