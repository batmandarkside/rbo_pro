import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentTopHistory = (props) => {
  const { items } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__top-history`}>
      <table>
        <thead>
          <tr>
            <th>Дата</th>
            <th>Логин</th>
            <th>Статус</th>
          </tr>
        </thead>
        <tbody>
          {items.map((item, index) => (
            <tr key={index}>
              <td>{item.get('date')}</td>
              <td>{item.get('user')}</td>
              <td>
                <span className={item.get('statusClassName')}>{item.get('endState')}</span>
                <span>{`${!index ? ' (текущий статус)' : ''}`}</span>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

RboDocumentTopHistory.propTypes = {
  items: PropTypes.instanceOf(List).isRequired
};

export default RboDocumentTopHistory;
