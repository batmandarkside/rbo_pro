import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboDocumentTop = (props) => {
  const { children, onCloseClick } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__top`}>
      <div className={`${COMPONENT_STYLE_NAME}__top-close`}>
        <Icon type="close" size="16" title="Закрыть" onClick={onCloseClick} />
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__top-content`}>
        {children}
      </div>
    </div>
  );
};

RboDocumentTop.propTypes = {
  children: PropTypes.node.isRequired,
  onCloseClick: PropTypes.func.isRequired
};

export default RboDocumentTop;

export RboDocumentTopHistory from './history';
export RboDocumentTopSignatures from './signatures';
