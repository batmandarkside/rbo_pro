import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { COMPONENT_STYLE_NAME } from '../../constants';
import Item from './item';

const RboDocumentTopSignatures = (props) => {
  const { items, onSignaturePrintClick, onSignatureDownloadClick } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__top-signatures`}>
      <table>
        <thead>
          <tr>
            <th>Дата</th>
            <th>Логин</th>
            <th>Имя</th>
            <th>Должность</th>
            <th>Тип подписи</th>
            <th>Результат проверки</th>
            <th>ЭП документа</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {items.map((item, index) => (
            <Item
              key={index}
              signId={item.get('signId')}
              signDateTime={item.get('signDateTime')}
              userLogin={item.get('userLogin')}
              userName={item.get('userName')}
              userPosition={item.get('userPosition')}
              signType={item.get('signType')}
              signValid={item.get('signValid')}
              signHash={item.get('signHash')}
              onSignatureDownloadClick={onSignatureDownloadClick}
              onSignaturePrintClick={onSignaturePrintClick}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
};

RboDocumentTopSignatures.propTypes = {
  items: PropTypes.instanceOf(List).isRequired,
  onSignaturePrintClick: PropTypes.func.isRequired,
  onSignatureDownloadClick: PropTypes.func.isRequired
};

export default RboDocumentTopSignatures;
