import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import { getSignValidationStatusClassNameMode } from 'utils';
import { COMPONENT_STYLE_NAME } from '../../constants';

const RboDocumentTopSignaturesItem = (props) => {
  const {
    signId,
    signDateTime,
    userLogin,
    userName,
    userPosition,
    signType,
    signValid,
    signHash,
    onSignaturePrintClick,
    onSignatureDownloadClick
  } = props;

  const handleSignaturePrintClick = () => onSignaturePrintClick(signId);
  const handleSignatureDownloadClick = () => onSignatureDownloadClick(signId);

  return (
    <tr className={`${COMPONENT_STYLE_NAME}__top-signatures-item`}>
      <td>{signDateTime}</td>
      <td>{userLogin}</td>
      <td>{userName}</td>
      <td>{userPosition}</td>
      <td>{signType}</td>
      <td className={getSignValidationStatusClassNameMode(signValid)}>{signValid}</td>
      <td>{signHash}</td>
      <td>
        <div className={`${COMPONENT_STYLE_NAME}__top-signatures-item-operations`}>
          <Icon type="download" size="16" title="Скачать" onClick={handleSignatureDownloadClick} />
          <Icon type="print" size="16" title="Распечатать" onClick={handleSignaturePrintClick} />
        </div>
      </td>
    </tr>
  );
};

RboDocumentTopSignaturesItem.propTypes = {
  signId: PropTypes.string.isRequired,
  signDateTime: PropTypes.string,
  userLogin: PropTypes.string,
  userName: PropTypes.string,
  userPosition: PropTypes.string,
  signType: PropTypes.string,
  signValid: PropTypes.string,
  signHash: PropTypes.string,
  onSignaturePrintClick: PropTypes.func.isRequired,
  onSignatureDownloadClick: PropTypes.func.isRequired
};

export default RboDocumentTopSignaturesItem;
