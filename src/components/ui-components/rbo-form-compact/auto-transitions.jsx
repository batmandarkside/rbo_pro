import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RboFormAutoTransitions extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  constructor(props) {
    super(props);

    this.inputs = props.children.map(child => ({
      id: child.props.id,
      maxLength: child.props.maxLength
    }));
  }

  componentDidMount() {
    this.inputs.forEach((input) => {
      document.querySelector(`#${input.id}`).addEventListener('input', this.handleChange, true);
      document.querySelector(`#${input.id}`).addEventListener('rboFormChange', this.handleRboFormChange, true);
    });
  }

  componentWillUnmount() {
    this.inputs.forEach((input) => {
      document.querySelector(`#${input.id}`).removeEventListener('input', this.handleChange, true);
      document.querySelector(`#${input.id}`).removeEventListener('rboFormChange', this.handleRboFormChange, true);
    });
  }

  handleChange = (e) => {
    const targetId = e.target.id;
    const targetValue = e.target.value;
    const targetValueLength = targetValue.length;
    const targetInput = this.inputs.find(i => i.id === targetId);
    const targetInputIndex = this.inputs.findIndex(i => i.id === targetId);
    const nextInputIndex = targetInputIndex + 1;
    if (nextInputIndex < this.inputs.length && targetValueLength === targetInput.maxLength) {
      this.goToNextFiled(nextInputIndex);
    }
  };

  handleRboFormChange = (e) => {
    const targetId = e.target.id;
    const targetInputIndex = this.inputs.findIndex(i => i.id === targetId);
    const nextInputIndex = targetInputIndex + 1;
    if (nextInputIndex < this.inputs.length) {
      this.goToNextFiled(nextInputIndex);
    }
  };

  goToNextFiled = (nextInputIndex) => {
    setTimeout(() => {
      const event = document.createEvent('Event');
      event.initEvent('mousedown', true, true);
      document.body.dispatchEvent(event);
      document.querySelector(`#${this.inputs[nextInputIndex].id}`).focus();
    }, 0);
  };

  render() {
    const { children } = this.props;

    return (
      <autoTransitionsBlock>{children}</autoTransitionsBlock>
    );
  }
}

export default RboFormAutoTransitions;
