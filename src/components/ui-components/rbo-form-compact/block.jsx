import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormBlock = (props) => {
  const { isWide, children } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__block`,
    isWide && 'm-wide'
  );

  return (
    <div className={elementClassName}>
      {children}
    </div>
  );
};

RboFormBlock.propTypes = {
  isWide: PropTypes.bool,
  children: PropTypes.node
};

export default RboFormBlock;
