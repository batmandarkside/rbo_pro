import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormBlockset = (props) => {
  const { children } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__blockset`}>
      {children}
    </div>
  );
};

RboFormBlockset.propTypes = {
  children: PropTypes.node.isRequired
};

export default RboFormBlockset;
