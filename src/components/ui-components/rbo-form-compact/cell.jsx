import React from 'react';
import PropTypes from 'prop-types';
import { sizeMap } from './utils';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormCell = (props) => {
  const { children, size } = props;
  const style = { width: sizeMap[size] || size || '100%' };

  return (
    <div
      className={`${COMPONENT_STYLE_NAME}__cell`}
      style={style}
    >
      {children}
    </div>
  );
};

RboFormCell.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.string
};

export default RboFormCell;
