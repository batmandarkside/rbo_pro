import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormCollapse = (props) => {
  const { title, collapsedTitle, pseudoLabel, isCollapsed, onCollapseToggle } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__collapse`,
    pseudoLabel && 'm-pseudo-label'
  );

  return (
    <div className={elementClassName}>
      <span
        onClick={onCollapseToggle}
        className="m-dashed-link"
      >
        {isCollapsed ? (collapsedTitle || title) : title}
      </span>
      <Icon type={isCollapsed ? 'arrow-down' : 'arrow-up'} size="16" />
    </div>
  );
};

RboFormCollapse.propTypes = {
  title: PropTypes.string.isRequired,
  collapsedTitle: PropTypes.string,
  pseudoLabel: PropTypes.bool,
  isCollapsed: PropTypes.bool,
  onCollapseToggle: PropTypes.func.isRequired,
};

export default RboFormCollapse;
