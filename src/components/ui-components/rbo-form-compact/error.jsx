import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormError = (props) => {
  const { text } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__error`}>
      {text}
    </div>
  );
};

RboFormError.propTypes = {
  text: PropTypes.string.isRequired
};

export default RboFormError;
