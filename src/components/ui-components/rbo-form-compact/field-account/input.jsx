import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import { getChar, getCaretPosition } from '../utils';

class RboFormFieldAccountInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value || '',
      formatValue: this.formatValue(this.props.value) || '',
      caretPosition: null
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: nextProps.value || '',
        formatValue: this.formatValue(nextProps.value) || ''
      });
    }
  }

  componentDidUpdate() {
    if (this.state.caretPosition !== null) {
      this.inputElement.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
  }

  onKeyDown = (e) => {
    this.setState({
      caretPosition: null
    });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  onKeyPress = (e) => {
    const char = getChar(e);
    if (!e.ctrlKey && !e.altKey && !e.metaKey && char !== null && (char < '0' || char > '9')) e.preventDefault();
  };

  onFocus = (e) => {
    const formatValue = this.formatValue(e.target.value);
    const deformatValue = this.deformatValue(formatValue);
    this.setState({
      formatValue,
      caretPosition: null
    });
    if (this.props.onFocus) this.props.onFocus(deformatValue);
  };

  onChange = (e) => {
    const value = e.target.value;
    const beforeCaretPosValue = value.substring(0, e.target.selectionEnd).replace(/\D/g, '');
    const formatValue = this.formatValue(value);
    const deformatValue = this.deformatValue(formatValue);
    const caretPosition = getCaretPosition(formatValue, beforeCaretPosValue);

    this.setState({
      value: deformatValue,
      formatValue,
      caretPosition
    });

    if (this.props.onChange) this.props.onChange(deformatValue, formatValue);
  };

  onBlur = (e) => {
    const formatValue = this.formatValue(e.target.value);
    const deformatValue = this.deformatValue(formatValue);
    this.setState({
      formatValue,
      caretPosition: null
    });
    if (this.props.onBlur) this.props.onBlur(deformatValue);
  };

  formatValue = (value) => {
    const val = value ? value.toString().replace(/\D/g, '').substring(0, 20) : '';

    const valResult = [];
    val.split('').forEach((v, i) => {
      valResult.push(v);
      if (i !== val.length - 1 && (i === 4 || i === 7 || i === 8)) valResult.push('.');
    });
    return valResult.join('');
  };

  deformatValue = value => value.replace(/\D/g, '');

  render() {
    const { id, isDisabled, placeholder } = this.props;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-account'
    );

    return (
      <input
        spellCheck="false"
        autoComplete="off"
        autoCorrect="off"
        type="text"
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
        id={id}
        value={this.state.formatValue}
        maxLength={23}
        disabled={isDisabled}
        onFocus={this.onFocus}
        onChange={this.onChange}
        onBlur={this.onBlur}
        onKeyDown={this.onKeyDown}
        onKeyPress={this.onKeyPress}
        placeholder={placeholder}
      />
    );
  }
}

export default RboFormFieldAccountInput;
