import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { sizeMap } from '../utils';
import { COMPONENT_STYLE_NAME } from '../constants';
import Input from './input';

class RboFormFieldButton extends Component {
  static propTypes = {
    id: PropTypes.string,
    size: PropTypes.string,
    pseudoLabel: PropTypes.bool,
    icon: PropTypes.string,
    title: PropTypes.string,
    iconTitle: PropTypes.string,
    align: PropTypes.oneOf(['left', 'center', 'right']),
    isPrimary: PropTypes.bool,
    isDisabled: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onClick: PropTypes.func.isRequired
  };

  state = { isFocus: false };

  handleOnFocus = () => {
    this.setState({ isFocus: true });
    if (this.props.onFocus) this.props.onFocus({ fieldId: this.props.id });
  };

  handleOnClick = (clickedElement) => {
    this.props.onClick({ fieldId: this.props.id, fieldElement: clickedElement });
  };

  handleOnBlur = () => {
    this.setState({ isFocus: false });
    if (this.props.onBlur) this.props.onBlur({ fieldId: this.props.id });
  };

  render() {
    const { id, size, pseudoLabel, align, icon, title, iconTitle, isPrimary, isDisabled } = this.props;
    const { isFocus } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__field`,
      'm-button',
      pseudoLabel && 'm-pseudo-label',
      align && `m-align-${align}`,
      isDisabled && 's-disabled',
      isFocus && 's-focused'
    );
    const style = { width: sizeMap[size] || size || '100%' };

    return (
      <div
        className={elementClassName}
        style={style}
      >
        <Input
          id={id}
          icon={icon}
          title={title}
          iconTitle={iconTitle}
          isAligned={!!align}
          isPrimary={isPrimary}
          isDisabled={isDisabled}
          onFocus={this.handleOnFocus}
          onChange={this.handleOnChange}
          onBlur={this.handleOnBlur}
          onClick={this.handleOnClick}
        />
      </div>
    );
  }
}

export default RboFormFieldButton;
