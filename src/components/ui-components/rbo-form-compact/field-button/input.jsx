import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboFormFieldIconButtonInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    icon: PropTypes.string,
    title: PropTypes.string,
    iconTitle: PropTypes.string,
    isPrimary: PropTypes.bool,
    isAligned: PropTypes.bool,
    isDisabled: PropTypes.bool,
    onFocus: PropTypes.func,
    onClick: PropTypes.func.isRequired,
    onBlur: PropTypes.func
  };

  state = { isFocus: false };

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  onFocus = () => {
    if (!this.props.isDisabled) {
      this.setState({ isFocus: true });
      if (this.props.onFocus) this.props.onFocus();
    }
  };

  onMousedown = () => {
    if (!this.props.isDisabled) this.props.onClick(this.inputElement);
  };

  onClick = (e) => {
    e.preventDefault();
  };

  onBlur = () => {
    if (!this.state.isOpened) {
      this.setState({ isFocus: false });
      if (this.props.onBlur) this.props.onBlur();
    }
  };

  handleKeyDown = (e) => {
    if (this.state.isFocus) {
      switch (e.which) {
        case 13: // enter
          e.preventDefault();
          this.enter();
          break;
        default:
          break;
      }
    }
  };

  enter = () => {
    if (!this.props.isDisabled) this.props.onClick(this.inputElement);
  };

  render() {
    const { id, icon, title, iconTitle, isAligned, isPrimary, isDisabled } = this.props;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__button`,
      isAligned && 'm-aligned',
      isPrimary && 'm-primary'
    );

    return (
      <a
        href="?click-button"
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
        tabIndex={isDisabled ? '-1' : null}
        id={id}
        disabled={isDisabled}
        onFocus={this.onFocus}
        onMouseDown={this.onMousedown}
        onClick={this.onClick}
        onBlur={this.onBlur}
      >
        {icon &&
        <span className={`${COMPONENT_STYLE_NAME}__button-icon`}>
          <Icon type={icon} size="20" title={title || iconTitle} />
        </span>
        }
        {title &&
        <span className={`${COMPONENT_STYLE_NAME}__button-title`}>{title}</span>
        }
      </a>
    );
  }
}

export default RboFormFieldIconButtonInput;
