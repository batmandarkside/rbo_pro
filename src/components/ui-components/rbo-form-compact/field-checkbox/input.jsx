import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboFormFieldCheckboxInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    isChecked: PropTypes.bool,
    title: PropTypes.node,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      isChecked: this.props.isChecked
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isChecked !== this.state.isChecked) {
      this.setState({ isChecked: nextProps.isChecked });
    }
  }

  onMouseDown = (e) => {
    e.preventDefault();
  };

  onFocus = (e) => {
    if (this.props.onFocus) this.props.onFocus(e.target.checked);
  };

  onChange = (e) => {
    if (this.props.onChange) this.props.onChange(e.target.checked);
    else this.setState({ isChecked: e.target.checked });
  };

  onBlur = (e) => {
    if (this.props.onBlur) this.props.onBlur(e.target.checked);
  };

  render() {
    const { id, title, isDisabled } = this.props;
    const { isChecked } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__checkbox`,
      isChecked && 's-checked'
    );

    return (
      <label
        className={elementClassName}
        onMouseDown={this.onMouseDown}
      >
        <input
          className={`${COMPONENT_STYLE_NAME}__checkbox-input`}
          ref={(input) => { this.inputElement = input; }}
          type="checkbox"
          id={id}
          checked={isChecked}
          disabled={isDisabled}
          onFocus={this.onFocus}
          onChange={this.onChange}
          onBlur={this.onBlur}
        />
        <span className={`${COMPONENT_STYLE_NAME}__checkbox-icon`}>
          {isChecked &&
          <Icon type="checkbox-checked" size="16" displayBlock />
          }
        </span>
        {title &&
        <span className={`${COMPONENT_STYLE_NAME}__checkbox-title`}>
          {title}
        </span>
        }
      </label>
    );
  }
}

export default RboFormFieldCheckboxInput;
