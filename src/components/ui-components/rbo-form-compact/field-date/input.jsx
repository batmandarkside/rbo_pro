import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment-timezone';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';
import { getChar, getCaretPosition } from '../utils';
import Popup from './popup';

class RboFormFieldDateInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    popupStyle: PropTypes.object,
    popupContainerElementSelector: PropTypes.string,
    isDisabled: PropTypes.bool,
    calendarShowToday: PropTypes.bool,
    calendarDisabledDate: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    const momentValue = this.props.value ? moment(this.props.value) : null;

    this.state = {
      value: momentValue ? momentValue.format() : null,
      formatValue: momentValue ? this.formatValue(momentValue.format('DD.MM.YYYY'), true) : '',
      caretPosition: null,
      isOpened: false,
      isFocus: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      const momentValue = nextProps.value ? moment(nextProps.value) : null;
      if (momentValue) {
        this.setState({
          value: momentValue.format(),
          formatValue: this.formatValue(momentValue.format('DD.MM.YYYY'), true),
        });
      } else {
        this.setState({
          value: null
        });
      }
    }
  }

  componentDidUpdate() {
    if (this.state.caretPosition !== null) {
      this.inputElement.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
  }

  onFocus = (e) => {
    this.setState({
      formatValue: this.formatValue(e.target.value),
      isFocus: true
    });
    if (!this.state.isFocus && this.props.onFocus) this.props.onFocus();
  };

  onKeyDown = (e) => {
    this.setState({
      caretPosition: null
    });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  onKeyPress = (e) => {
    const char = getChar(e);
    if (!e.ctrlKey && !e.altKey && !e.metaKey && char !== null && (char < '0' || char > '9')) e.preventDefault();
  };

  onChange = (e) => {
    const value = e.target.value;
    const beforeCaretPosValue = value.substring(0, e.target.selectionEnd).replace(/\D/g, '');
    const formatValue = this.formatValue(value);
    const deformatValue = this.deformatValue(formatValue);
    const caretPosition = getCaretPosition(formatValue, beforeCaretPosValue);

    this.setState({
      value: deformatValue,
      formatValue,
      caretPosition,
      isOpened: false
    });

    if (this.state.value !== deformatValue && this.props.onChange) this.props.onChange(deformatValue);
  };

  onBlur = (e) => {
    if (!this.state.isOpened) {
      this.setState({
        isFocus: false,
        formatValue: this.formatValue(e.target.value, true),
        caretPosition: null
      });
      this.props.onBlur && this.props.onBlur();
    }
  };

  onIconButtonMousedown = () => {
    if (!this.props.isDisabled) {
      this.setState({ isOpened: true });
      this.inputElement.focus();
    }
  };

  onIconButtonClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  onCalendarSelect = (value) => {
    const stringValue = value.set({ hour: 0, minute: 0, second: 0 }).format();
    this.setState({
      value: stringValue,
      formatValue: this.formatValue(value.format('DD.MM.YYYY')),
      caretPosition: null,
      isOpened: false
    });
    this.props.onChange && this.props.onChange(stringValue);
  };

  onPopupClose = () => {
    this.setState({ isOpened: false });
  };

  formatValue = (value, onBlurFlag) => {
    const val = value ? value.toString().replace(/\D/g, '').substring(0, 8) : '';

    const valResult = [];
    val.split('').forEach((v, i) => {
      valResult.push(v);
      if (i !== val.length - 1 && (i === 1 || i === 3)) valResult.push('.');
    });
    const valResultString = valResult.join('');

    if (onBlurFlag) {
      if (valResultString.length < 10) return '';
      const isValid = /^\d{2}\.\d{2}\.\d{4}$/gi.test(valResultString) &&
        moment(valResultString, 'DD.MM.YYYY').isValid();
      return isValid ? valResultString : '';
    }

    return valResultString;
  };

  deformatValue = (value) => {
    if (value.length < 10) return null;
    const isValid = /^\d{2}\.\d{2}\.\d{4}$/gi.test(value) && moment(value, 'DD.MM.YYYY').isValid();
    const momentValue = moment(value, 'DD.MM.YYYY');
    return isValid ? momentValue.format() : null;
  };

  render() {
    const {
      id,
      popupStyle,
      popupContainerElementSelector,
      isDisabled,
      calendarShowToday,
      calendarDisabledDate,
      placeholder
    } = this.props;
    const { value, formatValue, isOpened } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-date'
    );

    const iconButtonClassName = classnames(
      `${COMPONENT_STYLE_NAME}__icon`,
      'm-button',
      isOpened && 's-active',
    );

    const calendarValue = value ? moment(value) : null;

    return (
      <div style={{ position: 'relative' }}>
        <input
          spellCheck="false"
          autoComplete="off"
          autoCorrect="off"
          type="text"
          ref={(input) => { this.inputElement = input; }}
          className={elementClassName}
          id={id}
          value={formatValue}
          maxLength={10}
          disabled={isDisabled}
          onFocus={this.onFocus}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onKeyDown={this.onKeyDown}
          onKeyPress={this.onKeyPress}
          placeholder={placeholder}
        />
        <div
          className={iconButtonClassName}
          onMouseDown={this.onIconButtonMousedown}
          onClick={this.onIconButtonClick}
        >
          <Icon type="calendar" size="16" />
        </div>
        {isOpened &&
        <Popup
          openerElement={this.inputElement}
          style={popupStyle}
          containerElementSelector={popupContainerElementSelector}
          onClose={this.onPopupClose}
          value={calendarValue}
          onSelect={this.onCalendarSelect}
          calendarShowToday={calendarShowToday}
          calendarDisabledDate={calendarDisabledDate}

        />
        }
      </div>
    );
  }
}

export default RboFormFieldDateInput;
