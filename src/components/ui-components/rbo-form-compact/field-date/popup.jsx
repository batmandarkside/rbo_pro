import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Calendar from 'rc-calendar';
import 'rc-calendar/assets/index.css';
import ruRU from 'rc-calendar/lib/locale/ru_RU';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboFormFieldDatePopup extends Component {
  static propTypes = {
    openerElement: PropTypes.object.isRequired,
    value: PropTypes.object,
    style: PropTypes.object,
    containerElementSelector: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    calendarShowToday: PropTypes.bool,
    calendarDisabledDate: PropTypes.func
  };

  state = {
    containerElement: document.querySelector(this.props.containerElementSelector || 'body'),
    containerElementBounding: { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 },
    openerElementBounding: { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 },
    innerElementBounding: { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 }
  };

  componentDidMount() {
    this.elementId = `_popupBox_${Date.now()}`;
    window.addEventListener('resize', this.handleWindowResize);
    document.addEventListener('scroll', this.handleDocumentScroll);
    document.addEventListener('mousedown', this.handleMouseDown, true);
    document.addEventListener('touchstart', this.handleMouseDown, true);
    document.addEventListener('mouseup', this.handleMouseUp, true);
    document.addEventListener('keydown', this.handleKeyDown);
    this.setOffset();
    setTimeout(this.setOffset, 0);
  }

  componentWillReceiveProps() {
    setTimeout(this.setOffset, 0);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
    document.removeEventListener('scroll', this.handleDocumentScroll);
    document.removeEventListener('mousedown', this.handleMouseDown, true);
    document.removeEventListener('touchstart', this.handleMouseDown, true);
    document.removeEventListener('mouseup', this.handleMouseUp, true);
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  getOffset = () => {
    const containerElementBounding = this.state.containerElement.getBoundingClientRect();
    const openerElementBounding = this.props.openerElement.getBoundingClientRect();
    const innerElementBounding = this.innerElement ?
      this.innerElement.getBoundingClientRect() :
      { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 };

    return {
      containerElementBounding,
      openerElementBounding,
      innerElementBounding
    };
  };

  setOffset = () => {
    if (this.innerElement) {
      this.setState({
        ...this.getOffset(),
      });
    }
  };

  handleWindowResize = () => {
    this.setOffset();
  };

  handleDocumentScroll = () => {
    this.setOffset();
  };

  handleMouseDown = (e) => {
    if (!e.target.closest(`#${this.elementId}`)) this.props.onClose();
  };

  handleMouseUp = () => {
    this.props.openerElement.focus();
  };

  handleKeyDown = (e) => {
    switch (e.which) {
      case 9: // tab
      case 27: // esc
        this.props.onClose();
        break;
      default:
        break;
    }
  };

  render() {
    const { style, value, onSelect, calendarShowToday, calendarDisabledDate } = this.props;
    const { containerElementBounding, openerElementBounding, innerElementBounding } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__popup`,
      'm-date'
    );

    const topSpace = openerElementBounding.top - containerElementBounding.top;
    const bottomSpace = containerElementBounding.bottom - openerElementBounding.bottom;
    let top = 'auto';
    let bottom = 'auto';
    let maxHeight = 400;
    if (bottomSpace > 412 || bottomSpace > innerElementBounding.height + 12 || bottomSpace >= topSpace) {
      top = openerElementBounding.height + 4;
      maxHeight = bottomSpace - 12;
    } else {
      bottom = openerElementBounding.height + 4;
      maxHeight = topSpace - 12;
    }
    if (maxHeight > 400) maxHeight = 400;

    const elementStyle = {
      top,
      bottom,
      maxHeight,
      ...style
    };

    return (
      <div
        id={this.elementId}
        className={elementClassName}
        style={elementStyle}
      >
        <div
          className={`${COMPONENT_STYLE_NAME}__popup-inner`}
          ref={(element) => { this.innerElement = element; }}
        >
          <Calendar
            locale={ruRU}
            showDateInput={false}
            defaultValue={value}
            onSelect={onSelect}
            showToday={calendarShowToday}
            disabledDate={calendarDisabledDate}
          />
        </div>
      </div>
    );
  }
}

export default RboFormFieldDatePopup;
