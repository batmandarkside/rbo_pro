import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import { getChar, getCaretPosition } from '../utils';

class RboFormFieldDecimalInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.number,
    maxLength: PropTypes.number,
    isShowZero: PropTypes.bool,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    const formatValue = this.formatValue(this.props.value, true);
    this.state = {
      value: this.props.value || 0.00,
      formatValue: formatValue || (this.props.isShowZero && '0.00') || '',
      maxLength: this.props.maxLength ? this.props.maxLength + parseInt(this.props.maxLength / 3, 10) + 3 : 21,
      caretPosition: null,
      onKeyPressChar: null,
      isFocus: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      const formatValue = this.formatValue(nextProps.value, !this.state.isFocus);
      this.setState({
        value: nextProps.value || 0.00,
        formatValue: this.state.isFocus ? formatValue : (formatValue || (this.props.isShowZero && '0.00') || '')
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.caretPosition !== null) {
      this.inputElement.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
    if (prevState.isFocus && !this.state.isFocus && (prevState.value !== !this.state.value)) this.refreshState();
  }

  onFocus = (e) => {
    const formatValue = this.formatValue(e.target.value);
    this.setState({
      isFocus: true,
      formatValue: formatValue === '0.00' ? '' : formatValue
    });
    if (this.props.onFocus) this.props.onFocus(e);
  };

  onKeyDown = (e) => {
    this.setState({
      caretPosition: null,
      onKeyPressChar: null
    });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  onKeyPress = (e) => {
    const char = getChar(e);
    if (!e.ctrlKey && !e.altKey && !e.metaKey && e.target.value.indexOf('.') < 0 &&
      (char === '.' || char === ',')) return true;
    if (!e.ctrlKey && !e.altKey && !e.metaKey && char !== null && (char < '0' || char > '9')) {
      e.preventDefault();
      return false;
    }
    this.setState({ onKeyPressChar: char });
    return true;
  };

  onChange = (e) => {
    const value = e.target.value;
    const beforeCaretPosValue = value.substring(0, e.target.selectionEnd).replace(',', '.').replace(/[^\d\\.]/g, '');
    const formatValue = this.formatValue(value);
    const deformatValue = this.deformatValue(formatValue);
    const caretPosition = getCaretPosition(formatValue, beforeCaretPosValue);

    this.setState({
      value: deformatValue,
      formatValue,
      caretPosition
    });

    if (this.props.onChange) this.props.onChange(deformatValue);
  };

  onBlur = (e) => {
    const formatValue = this.formatValue(e.target.value, true);
    this.setState({
      isFocus: false,
      formatValue: formatValue || (this.props.isShowZero && '0.00') || '',
      caretPosition: null,
      onKeyPressChar: null
    });
    if (this.props.onBlur) this.props.onBlur(e);
  };

  refreshState = () => {
    const formatValue = this.formatValue(this.state.value, true);
    this.setState({ formatValue: formatValue || (this.props.isShowZero && '0.00') || '' });
  };

  formatValue = (value, onBlurFlag) => {
    const stringValue = value ? value.toString().replace(',', '.') : '';
    const dotIndex = stringValue.indexOf('.');
    const integer = stringValue ?
      parseInt(stringValue.substring(0, dotIndex >= 0 ? dotIndex : stringValue.length).replace(/\D/g, ''), 10) :
      0;
    const integerReversed = integer.toString()
      .substring(0, this.props.maxLength ? this.props.maxLength : 14).split('').reverse();
    const integerResult = [];
    integerReversed.forEach((v, i) => {
      if (i && !(i % 3)) integerResult.unshift(' ');
      integerResult.unshift(v);
    });

    const decimal = stringValue && dotIndex >= 0 ?
      stringValue.substring(dotIndex).replace(/\D/g, '').substring(0, 2) : '';

    if (onBlurFlag) {
      const formatDecimal = () => {
        switch (decimal.length) {
          case 1:
            return `${decimal}0`;
          case 2:
            return decimal;
          default:
            return '00';
        }
      };
      if (!integer && !parseInt(decimal, 10)) return '';
      return `${integerResult.join('')}.${formatDecimal()}`;
    }

    if (!integer) {
      if (dotIndex >= 0) {
        return `0.${decimal}`;
      }
      if (this.state && this.state.onKeyPressChar === '0') return '0';
      return '';
    }
    return `${integerResult.join('')}${dotIndex >= 0 ? `.${decimal}` : ''}`;
  };

  deformatValue = (value) => {
    const deformatValue = parseFloat(value.replace(/[^\d\\.]/g, ''));
    if (!deformatValue) return 0;
    return deformatValue;
  };

  render() {
    const { id, isDisabled, placeholder } = this.props;
    const { formatValue, maxLength } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-decimal-number'
    );

    return (
      <input
        spellCheck="false"
        autoComplete="off"
        autoCorrect="off"
        type="text"
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
        id={id}
        value={formatValue}
        maxLength={maxLength}
        disabled={isDisabled}
        onFocus={this.onFocus}
        onChange={this.onChange}
        onBlur={this.onBlur}
        onKeyDown={this.onKeyDown}
        onKeyPress={this.onKeyPress}
        placeholder={placeholder}
      />
    );
  }
}

export default RboFormFieldDecimalInput;
