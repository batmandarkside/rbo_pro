import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import { getChar, getCaretPosition } from '../utils';

class RboFormFieldDigitalInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    maxLength: PropTypes.number,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value || '',
      formatValue: this.formatValue(this.props.value) || '',
      caretPosition: null
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: nextProps.value || '',
        formatValue: this.formatValue(nextProps.value) || ''
      });
    }
  }

  componentDidUpdate() {
    if (this.state.caretPosition !== null) {
      this.inputElement.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
  }

  onFocus = (e) => {
    const formatValue = this.formatValue(e.target.value);

    this.setState({
      formatValue,
    });

    if (this.props.onFocus) this.props.onFocus(formatValue);
  };

  onKeyDown = (e) => {
    this.setState({
      caretPosition: null
    });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  onKeyPress = (e) => {
    const char = getChar(e);
    if (!e.ctrlKey && !e.altKey && !e.metaKey && char !== null && (char < '0' || char > '9')) e.preventDefault();
  };

  onChange = (e) => {
    const value = e.target.value;
    const beforeCaretPosValue = value.substring(0, e.target.selectionEnd).replace(/\D/g, '');
    const formatValue = this.formatValue(value);
    const caretPosition = getCaretPosition(formatValue, beforeCaretPosValue);

    this.setState({
      formatValue,
      caretPosition
    });

    if (this.props.onChange) this.props.onChange(formatValue, formatValue);
  };

  onBlur = (e) => {
    const formatValue = this.formatValue(e.target.value);

    this.setState({
      formatValue,
      caretPosition: null
    });

    if (this.props.onBlur) this.props.onBlur(formatValue);
  };

  formatValue = value => (value ? value.toString().replace(/\D/g, '') : '');

  render() {
    const { id, maxLength, isDisabled, placeholder } = this.props;
    const { formatValue } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-digital'
    );

    return (
      <input
        spellCheck="false"
        autoComplete="off"
        autoCorrect="off"
        type="text"
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
        id={id}
        value={formatValue}
        maxLength={maxLength}
        disabled={isDisabled}
        onFocus={this.onFocus}
        onChange={this.onChange}
        onBlur={this.onBlur}
        onKeyDown={this.onKeyDown}
        onKeyPress={this.onKeyPress}
        placeholder={placeholder}
      />
    );
  }
}

export default RboFormFieldDigitalInput;
