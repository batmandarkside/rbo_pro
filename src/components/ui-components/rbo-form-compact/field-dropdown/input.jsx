import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';
import Popup from './popup';

class RboFormFieldDropdownInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    ).isRequired,
    optionRenderer: PropTypes.func,
    popupStyle: PropTypes.object,
    popupContainerElementSelector: PropTypes.string,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      isOpened: false,
      value: props.value,
      formatValue: this.formatValue(props.value),
      active: props.value,
      selected: props.value,
    };
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: nextProps.value,
        formatValue: this.formatValue(nextProps.value),
        active: nextProps.value,
        selected: nextProps.value,
      });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  onFocus = () => {
    if (!this.props.isDisabled) {
      this.setState({ isFocus: true });
      if (!this.state.isFocus && this.props.onFocus) this.props.onFocus();
    }
  };

  onMousedown = () => {
    if (!this.props.isDisabled) this.setState({ isOpened: !this.state.isOpened });
  };

  onClick = (e) => {
    e.preventDefault();
  };

  onClose = () => {
    this.setState({ isOpened: false });
  };

  onBlur = () => {
    if (!this.state.isOpened) {
      this.setState({ isFocus: false });
      if (this.props.onBlur) this.props.onBlur();
    }
  };

  onOptionClick = (value) => {
    this.setState({
      value,
      formatValue: this.formatValue(value),
      selected: value,
      active: value,
      isOpened: false
    });
    if (this.props.onChange) this.props.onChange(value);
  };

  onOptionMouseEnter = (value) => {
    this.setState({ active: value });
  };

  onOptionMouseLeave = () => {
    this.setState({ active: null });
  };

  handleKeyDown = (e) => {
    if (this.state.isFocus) {
      switch (e.which) {
        case 38: // arrow up
          e.preventDefault();
          this.prev();
          break;
        case 40: // arrow down
          e.preventDefault();
          this.next();
          break;
        case 13: // enter
          e.preventDefault();
          this.enter();
          break;
        default:
          break;
      }
    }
  };

  prev = () => {
    const { options } = this.props;
    const { active, isOpened } = this.state;
    const activeOptionIndex = options.findIndex(option => option.value === active);
    const prevActiveOptionIndex = activeOptionIndex <= 0 ? options.length - 1 : activeOptionIndex - 1;
    const value = options[prevActiveOptionIndex].value;
    if (isOpened) {
      this.setState({
        active: value
      });
    } else {
      this.setState({
        value,
        formatValue: this.formatValue(value),
        selected: value,
        active: value
      });
      if (this.props.onChange) this.props.onChange(value);
    }
  };

  next = () => {
    const { options } = this.props;
    const { active, isOpened } = this.state;
    const activeOptionIndex = options.findIndex(option => option.value === active);
    const nextActiveOptionIndex = activeOptionIndex === options.length - 1 ? 0 : activeOptionIndex + 1;
    const value = options[nextActiveOptionIndex].value;
    if (isOpened) {
      this.setState({
        active: value
      });
    } else {
      this.setState({
        value,
        formatValue: this.formatValue(value),
        selected: value,
        active: value
      });
      if (this.props.onChange) this.props.onChange(value);
    }
  };

  enter = () => {
    if (this.state.isOpened) {
      this.setState({
        value: this.state.active,
        formatValue: this.formatValue(this.state.active),
        selected: this.state.active,
        isOpened: false
      });
      if (this.props.onChange) this.props.onChange(this.state.active);
    } else {
      this.setState({
        isOpened: true
      });
    }
  };

  formatValue = (value) => {
    const { options } = this.props;
    const selectedOption = options.find(option => option.value === value);
    return selectedOption ? selectedOption.title : null;
  };

  render() {
    const { id, options, optionRenderer, popupStyle, popupContainerElementSelector, isDisabled, placeholder } = this.props;
    const { isOpened, formatValue, selected, active } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-dropdown'
    );

    const formatOptions = options.map(option => ({
      ...option,
      selected: option.value === selected,
      active: option.value === active
    }));

    return (
      <div style={{ position: 'relative' }}>
        <a
          href="?open-dropdown-popup"
          ref={(input) => { this.inputElement = input; }}
          className={elementClassName}
          id={id}
          tabIndex={isDisabled ? '-1' : null}
          onFocus={this.onFocus}
          onMouseDown={this.onMousedown}
          onClick={this.onClick}
          onBlur={this.onBlur}
        >
          {formatValue || placeholder}
        </a>
        <div className={`${COMPONENT_STYLE_NAME}__icon`}>
          <Icon type="dropdown" size="16" />
        </div>
        {isOpened &&
        <Popup
          openerElement={this.inputElement}
          inputElement={this.inputElement || null}
          options={formatOptions}
          optionRenderer={optionRenderer}
          id={id}
          style={popupStyle}
          containerElementSelector={popupContainerElementSelector}
          onClose={this.onClose}
          onOptionClick={this.onOptionClick}
          onOptionMouseEnter={this.onOptionMouseEnter}
          onOptionMouseLeave={this.onOptionMouseLeave}
        />
        }
      </div>
    );
  }
}

export default RboFormFieldDropdownInput;
