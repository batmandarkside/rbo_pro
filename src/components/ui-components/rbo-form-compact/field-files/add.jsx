import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Dropzone from 'react-dropzone';
import { getFormattedFileSize } from 'utils';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboFormFieldFilesAdd extends Component {
  static propTypes = {
    isMultiple: PropTypes.bool,
    maxFileSize: PropTypes.number,
    onAddFiles: PropTypes.func.isRequired
  };

  state = {
    isActive: false,
    isError: false
  };

  onDrop = (acceptedFiles) => {
    this.setState({
      isActive: false,
      isError: false
    });
    this.props.onAddFiles(acceptedFiles);
  };

  onDropRejected = () => {
    this.setState({ isError: true });
  };

  onDragEnter = () => {
    this.setState({ isActive: true });
  };

  onDragLeave = () => {
    this.setState({ isActive: false });
  };

  onFileDialogCancel = () => {
    this.setState({
      isActive: false,
      isError: false
    });
  };

  render() {
    const { isMultiple, maxFileSize } = this.props;
    const { isActive, isError } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__file-add`,
      isActive && 's-active',
      isError && 's-error'
    );

    const dropzoneStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%'
    };

    return (
      <div
        className={elementClassName}
      >
        <Dropzone
          style={dropzoneStyle}
          multiple={isMultiple}
          onDrop={this.onDrop}
          onDragEnter={this.onDragEnter}
          onDragLeave={this.onDragLeave}
          maxSize={maxFileSize || Infinity}
          onDropRejected={this.onDropRejected}
          onFileDialogCancel={this.onFileDialogCancel}
        >
          <div className={`${COMPONENT_STYLE_NAME}__file-add-inner`}>
            {!isError &&
            <span>
              <span className="m-dashed-link">Выберите файл</span> или перетащите в эту область
            </span>
            }
            {isError &&
            <span>
              <span>
                Не удалось загрузить файл: превышен допустимый объем вложения ({getFormattedFileSize(maxFileSize)}).
              </span>
              <span className="m-dashed-link">Выберите другой файл</span>
            </span>
            }
          </div>
        </Dropzone>
      </div>
    );
  }
}

export default RboFormFieldFilesAdd;
