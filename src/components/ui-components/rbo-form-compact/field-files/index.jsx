import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import Input from './input';

class RboFormFieldFiles extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    values: ImmutablePropTypes.list,
    maxFileSize: PropTypes.number,
    maxFilesQty: PropTypes.number,
    isDisabled: PropTypes.bool,
    isWarning: PropTypes.bool,
    isError: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFileDownload: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      values: this.props.values
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClick, true);
    document.addEventListener('touchstart', this.handleClick, true);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.values !== this.state.values && nextProps.values !== undefined) {
      this.setState({ values: nextProps.values });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, true);
    document.removeEventListener('touchstart', this.handleClick, true);
  }

  handleClick = (e) => {
    if (!this.props.isDisabled) {
      if (e.target.closest(`[data-id="${this.props.id}"]`)) !this.state.isFocus && this.handleOnFocus();
      else this.state.isFocus && this.handleOnBlur();
    }
  };

  handleOnFocus = () => {
    this.setState({ isFocus: true });
    if (this.props.onFocus) this.props.onFocus({ fieldId: this.props.id });
  };

  handleOnChange = (values) => {
    this.setState({ values });
    if (this.props.onChange) this.props.onChange({ fieldId: this.props.id, fieldValue: values });
  };

  handleOnBlur = () => {
    this.setState({ isFocus: false });
    if (this.props.onBlur) this.props.onBlur({ fieldId: this.props.id });
  };

  render() {
    const { id, isDisabled, isWarning, isError, maxFileSize, maxFilesQty, onFileDownload } = this.props;
    const { values } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__field`,
      'm-files',
      isDisabled && 's-disabled',
      isWarning && 's-warning',
      isError && 's-error',
      values && !!values.length && 's-filled'
    );

    return (
      <div
        className={elementClassName}
        data-id={id}
      >
        <input
          type="text"
          id={id}
          onFocus={this.handleOnFocus}
          className={`${COMPONENT_STYLE_NAME}__hidden`}
          disabled={isDisabled}
        />
        <Input
          values={values}
          isDisabled={isDisabled}
          maxFileSize={maxFileSize}
          maxFilesQty={maxFilesQty}
          onChange={this.handleOnChange}
          onFileDownload={onFileDownload}
        />
      </div>
    );
  }
}

export default RboFormFieldFiles;
