import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { fromJS } from 'immutable';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import Item from './item';
import Add from './add';

class RboFormFieldFilesInput extends Component {
  static propTypes = {
    values: ImmutablePropTypes.list,
    maxFileSize: PropTypes.number,
    maxFilesQty: PropTypes.number,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFileDownload: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      values: props.values || []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.values !== this.state.values) {
      this.setState({ values: nextProps.values || [] });
    }
  }

  handleFileDownload = fileId => this.props.onFileDownload(fileId);

  handleFileRemove = (fileId) => {
    const values = this.state.values.filter(item => item.get('id') !== fileId);
    this.setState({ values });
    this.props.onChange && this.props.onChange(values);
  };

  handleFileCommentChange = (fileId, value) => {
    const values = this.state.values.map(item => (item.get('id') === fileId ? item.set('comment', value) : item));
    this.setState({ values });
    this.props.onChange && this.props.onChange(values);
  };

  handleAddFiles = (files) => {
    const { maxFilesQty, onChange } = this.props;
    const formattedFiles = files.map((file) => {
      const idTemp = file.preview.split('/');
      const id = idTemp[idTemp.length - 1];
      return {
        file,
        name: file.name,
        mimeType: file.type,
        date: file.lastModifiedDate,
        size: file.size,
        id
      };
    });
    let values = this.state.values.concat(fromJS(formattedFiles));
    if (maxFilesQty && values.size > maxFilesQty) values = values.setSize(maxFilesQty);
    this.setState({ values });
    onChange && onChange(values);
  };

  render() {
    const { isDisabled, maxFileSize, maxFilesQty } = this.props;
    const { values } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__files`
    );

    return (
      <div
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
      >
        {values.map((value, key) => (
          <Item
            key={key}
            id={value.get('id')}
            name={value.get('name')}
            size={value.get('size')}
            describe={value.get('describe')}
            comment={value.get('comment')}
            isDisabled={isDisabled}
            isDownloadable={value.get('isDownloadable')}
            onDownload={this.handleFileDownload}
            onRemove={this.handleFileRemove}
            onCommentChange={this.handleFileCommentChange}
          />
        ))}
        {!isDisabled && (!maxFilesQty || maxFilesQty > values.size) &&
        <Add
          onAddFiles={this.handleAddFiles}
          isMultiple={maxFilesQty !== 1}
          maxFileSize={maxFileSize}
        />
        }
      </div>
    );
  }
}

export default RboFormFieldFilesInput;
