import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import RboForm, {
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldTextarea,
  RboFormFieldButton
} from 'components/ui-components/rbo-form-compact';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboFormFieldFilesItemComment extends Component {
  static propTypes = {
    fileId: PropTypes.string.isRequired,
    value: PropTypes.string,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func.isRequired
  };

  state = {
    fieldId: `comment_${this.props.fileId}`,
    fieldValue: this.props.value || '',
    isOpen: false
  };

  handleEditClick = () => {
    this.setState({
      isOpen: true
    });
  };

  handleFieldChange = ({ fieldValue }) => {
    this.setState({ fieldValue });
  };

  handleFormSubmit = () => {
    this.setState({
      isOpen: false
    });
    this.props.onChange(this.state.fieldValue);
  };

  handleFormCancel = () => {
    this.setState({
      isOpen: false,
      fieldValue: this.props.value
    });
  };

  render() {
    const { fileId, value, isDisabled } = this.props;
    const { fieldId, fieldValue, isOpen } = this.state;

    const submitButtonId = `submit_${fileId}`;
    const cancelButtonId = `cancel_${fileId}`;

    return (
      <div className={`${COMPONENT_STYLE_NAME}__file-comment`}>
        {!isOpen &&
        <div className={`${COMPONENT_STYLE_NAME}__file-comment-content`}>
          {fieldValue &&
          <div>
            <span className={`${COMPONENT_STYLE_NAME}__file-comment-label`}>Комментарий: </span>
            <span className={`${COMPONENT_STYLE_NAME}__file-comment-value`}>{value}</span>
            {!isDisabled &&
            <span
              className={`${COMPONENT_STYLE_NAME}__file-comment-edit`}
              onClick={this.handleEditClick}
            >
              <Icon type="edit" size="16" />
            </span>
            }
          </div>
          }
          {!fieldValue && !isDisabled &&
          <span
            className="m-dashed-link"
            onClick={this.handleEditClick}
          >
            Добавить комментарий
          </span>
          }
        </div>
        }
        {isOpen &&
        <div className={`${COMPONENT_STYLE_NAME}__file-comment-form`}>
          <RboForm initialFocusFieldId={fieldId} onSubmit={this.handleFormSubmit}>
            <RboFormBlockset>
              <RboFormBlock isWide>
                <RboFormRowset>
                  <RboFormRow>
                    <RboFormCell>
                      <RboFormFieldTextarea
                        id={fieldId}
                        value={fieldValue}
                        height={2}
                        onChange={this.handleFieldChange}
                        placeholder="Описание файла"
                      />
                    </RboFormCell>
                  </RboFormRow>
                  <RboFormRow>
                    <RboFormCell size="1/2">
                      <RboFormFieldButton
                        id={cancelButtonId}
                        onClick={this.handleFormCancel}
                        title="Отменить"
                        align="left"
                      />
                    </RboFormCell>
                    <RboFormCell size="1/2">
                      <RboFormFieldButton
                        id={submitButtonId}
                        isPrimary
                        onClick={this.handleFormSubmit}
                        title="Добавить"
                        align="right"
                      />
                    </RboFormCell>
                  </RboFormRow>
                </RboFormRowset>
              </RboFormBlock>
            </RboFormBlockset>
          </RboForm>
        </div>
        }
      </div>
    );
  }
}

export default RboFormFieldFilesItemComment;
