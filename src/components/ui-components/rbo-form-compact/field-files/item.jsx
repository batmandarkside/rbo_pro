import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';
import Comment from './item-comment';

const RboFormFieldFilesItem = (props) => {
  const { id, isDisabled, isDownloadable, name, comment, describe, onDownload, onRemove, onCommentChange } = props;

  const handleOnDownload = () => onDownload(id);

  const handleOnRemove = () => onRemove(id);

  const handleCommentChange = value => onCommentChange(id, value);

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__file`,
    isDisabled && 's-disabled'
  );

  return (
    <div
      className={elementClassName}
    >
      <div className={`${COMPONENT_STYLE_NAME}__file-icon`}>
        <Icon type="attachment" displayBlock size="16" />
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__file-content`}>
        <div className={`${COMPONENT_STYLE_NAME}__file-name`}>
          {isDownloadable &&
          <span
            className="m-like-link"
            onClick={handleOnDownload}
          >
            {name}
          </span>
          }
          {!isDownloadable &&
            <span>{name}</span>
          }
        </div>
        {describe &&
        <div className={`${COMPONENT_STYLE_NAME}__file-describe`}>
          {describe}
        </div>
        }
        <Comment
          fileId={id}
          value={comment}
          isDisabled={isDisabled}
          onChange={handleCommentChange}
        />
      </div>
      {!isDisabled &&
      <div
        className={`${COMPONENT_STYLE_NAME}__file-remove`}
        onClick={handleOnRemove}
      >
        <Icon type="remove" displayBlock size="16" />
      </div>
      }
    </div>
  );
};

RboFormFieldFilesItem.propTypes = {
  id: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool,
  isDownloadable: PropTypes.bool,
  name: PropTypes.string.isRequired,
  comment: PropTypes.string,
  describe: PropTypes.string,
  onDownload: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onCommentChange: PropTypes.func.isRequired
};

export default RboFormFieldFilesItem;
