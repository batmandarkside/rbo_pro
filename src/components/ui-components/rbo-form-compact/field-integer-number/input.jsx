import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import { getChar, getCaretPosition } from '../utils';

class RboFormFieldIntegerInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.number,
    maxLength: PropTypes.number,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      value: parseInt(this.props.value, 10) || 0,
      formatValue: this.formatValue(parseInt(this.props.value, 10), true),
      maxLength: this.props.maxLength ? this.props.maxLength + parseInt(this.props.maxLength / 3, 10) : 18,
      caretPosition: null,
      onKeyPressChar: null,
      isFocus: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: nextProps.value || 0.00,
        formatValue: this.formatValue(nextProps.value, !this.state.isFocus),
      });
    }
  }

  componentDidUpdate() {
    if (this.state.caretPosition !== null) {
      this.inputElement.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
    }
  }

  onFocus = (e) => {
    this.setState({
      isFocus: true,
      formatValue: this.formatValue(e.target.value)
    });
    if (this.props.onFocus) this.props.onFocus();
  };

  onKeyDown = (e) => {
    this.setState({
      caretPosition: null,
      onKeyPressChar: null
    });
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 90) e.preventDefault(); // disable ctrl-z
  };

  onKeyPress = (e) => {
    const char = getChar(e);
    if (!e.ctrlKey && !e.altKey && !e.metaKey && char !== null && (char < '0' || char > '9')) e.preventDefault();
    this.setState({ onKeyPressChar: char });
  };

  onChange = (e) => {
    const value = e.target.value;
    const beforeCaretPosValue = value.substring(0, e.target.selectionEnd).replace(/\D/g, '');
    const formatValue = this.formatValue(value);
    const deformatValue = this.deformatValue(formatValue);
    const caretPosition = getCaretPosition(formatValue, beforeCaretPosValue);

    this.setState({
      value: deformatValue,
      formatValue,
      caretPosition
    });

    if (this.props.onChange) this.props.onChange(deformatValue);
  };

  onBlur = (e) => {
    this.setState({
      isFocus: false,
      formatValue: this.formatValue(e.target.value, true),
      caretPosition: null,
      onKeyPressChar: null
    });
    if (this.props.onBlur) this.props.onBlur();
  };

  formatValue = (value, onBlurFlag) => {
    const integer = value ? parseInt(value.toString().replace(/\D/g, ''), 10) : 0;
    if (!integer) {
      if (onBlurFlag) return '';
      if (this.state && this.state.onKeyPressChar === '0') return '0';
      return '';
    }
    const integerReversed = integer.toString()
      .substring(0, this.props.maxLength ? this.props.maxLength : 14).split('').reverse();
    const integerResult = [];
    integerReversed.forEach((v, i) => {
      if (i && !(i % 3)) integerResult.unshift(' ');
      integerResult.unshift(v);
    });
    return integerResult.join('');
  };

  deformatValue = (value) => {
    const deformatValue = parseInt(value.replace(/\D/g, ''), 10);
    if (!deformatValue) return 0;
    return deformatValue;
  };

  render() {
    const { id, isDisabled, placeholder } = this.props;
    const { formatValue, maxLength } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-integer-number'
    );

    return (
      <input
        spellCheck="false"
        autoComplete="off"
        autoCorrect="off"
        type="text"
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
        id={id}
        value={formatValue}
        maxLength={maxLength}
        disabled={isDisabled}
        onFocus={this.onFocus}
        onChange={this.onChange}
        onBlur={this.onBlur}
        onKeyDown={this.onKeyDown}
        onKeyPress={this.onKeyPress}
        placeholder={placeholder}
      />
    );
  }
}

export default RboFormFieldIntegerInput;
