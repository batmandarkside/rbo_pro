import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { sizeMap } from '../utils';
import { COMPONENT_STYLE_NAME } from '../constants';
import Input from './input';

class RboFormFieldRadio extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    group: PropTypes.string.isRequired,
    size: PropTypes.string,
    title: PropTypes.node,
    pseudoLabel: PropTypes.bool,
    value: PropTypes.string,
    isSelected: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isWarning: PropTypes.bool,
    isError: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      isSelected: this.props.isSelected
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected !== this.state.isSelected) {
      this.setState({ isSelected: nextProps.isSelected });
    }
  }

  handleOnFocus = () => {
    this.setState({ isFocus: true });
    if (this.props.onFocus) this.props.onFocus({ fieldId: this.props.group });
  };

  handleOnChange = (isSelected) => {
    this.setState({ isSelected });
    if (this.props.onChange) this.props.onChange({ fieldId: this.props.group, fieldValue: this.props.value });
  };

  handleOnBlur = () => {
    this.setState({ isFocus: false });
    if (this.props.onBlur) this.props.onBlur({ fieldId: this.props.group });
  };

  render() {
    const { id, group, size, title, pseudoLabel, isDisabled, isWarning, isError } = this.props;
    const { isFocus, isSelected } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__field`,
      'm-radio',
      pseudoLabel && 'm-pseudo-label',
      isDisabled && 's-disabled',
      isWarning && 's-warning',
      isError && 's-error',
      isFocus && 's-focused'
    );
    const style = { width: sizeMap[size] || size || '100%' };

    return (
      <div
        className={elementClassName}
        style={style}
      >
        <Input
          id={id}
          group={group}
          title={title}
          isChecked={isSelected}
          isDisabled={isDisabled}
          onFocus={this.handleOnFocus}
          onChange={this.handleOnChange}
          onBlur={this.handleOnBlur}
        />
      </div>
    );
  }
}

export default RboFormFieldRadio;
