import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { sizeMap } from '../utils';
import { COMPONENT_STYLE_NAME } from '../constants';
import Label from '../label';
import Input from './input';

class RboFormFieldSearch extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    size: PropTypes.string,
    label: PropTypes.string,
    pseudoLabel: PropTypes.bool,
    value: PropTypes.string,
    searchValue: PropTypes.string,
    inputType: PropTypes.oneOf(['Text', 'Textarea', 'Account', 'Digital']),
    options: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    ).isRequired,
    optionRenderer: PropTypes.func,
    popupStyle: PropTypes.object,
    popupContainerElementSelector: PropTypes.string,
    maxLength: PropTypes.number,
    isSearching: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isWarning: PropTypes.bool,
    isError: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      value: this.props.value,
      searchValue: this.props.searchValue
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value && nextProps.value !== undefined) {
      this.setState({ value: nextProps.value });
    }
    if (nextProps.searchValue !== this.state.searchValue && nextProps.searchValue !== undefined) {
      this.setState({ searchValue: nextProps.searchValue });
    }
  }

  handleOnFocus = () => {
    this.setState({ isFocus: true });
    this.props.onFocus && this.props.onFocus({ fieldId: this.props.id });
  };

  handleOnSearch = (searchValue) => {
    this.setState({ searchValue });
    this.props.onSearch({ fieldId: this.props.id, fieldSearchValue: searchValue });
  };

  handleOnChange = (value, innerFieldValue) => {
    this.setState({ value });
    this.props.onChange && this.props.onChange({
      fieldId: this.props.id,
      fieldValue: value,
      innerFieldValue
    });
  };

  handleOnBlur = () => {
    this.setState({ isFocus: false });
    this.props.onBlur && this.props.onBlur({ fieldId: this.props.id });
  };

  render() {
    const {
      id,
      size,
      label,
      pseudoLabel,
      inputType,
      options,
      optionRenderer,
      popupStyle,
      popupContainerElementSelector,
      maxLength,
      isSearching,
      isDisabled,
      isWarning,
      isError,
      placeholder
    } = this.props;
    const { isFocus, value, searchValue } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__field`,
      'm-search',
      pseudoLabel && 'm-pseudo-label',
      isDisabled && 's-disabled',
      isWarning && 's-warning',
      isError && 's-error',
      isFocus && 's-focused',
      value && 's-filled'
    );
    const style = { width: sizeMap[size] || size || '100%' };

    return (
      <div
        className={elementClassName}
        style={style}
      >
        {!!label &&
        <Label label={label} htmlFor={id} />
        }
        <Input
          id={id}
          value={value}
          searchValue={searchValue}
          inputType={inputType}
          options={options}
          optionRenderer={optionRenderer}
          popupStyle={popupStyle}
          popupContainerElementSelector={popupContainerElementSelector}
          maxLength={maxLength}
          isSearching={isSearching}
          isDisabled={isDisabled}
          onFocus={this.handleOnFocus}
          onSearch={this.handleOnSearch}
          onChange={this.handleOnChange}
          onBlur={this.handleOnBlur}
          placeholder={placeholder}
        />
      </div>
    );
  }
}

export default RboFormFieldSearch;
