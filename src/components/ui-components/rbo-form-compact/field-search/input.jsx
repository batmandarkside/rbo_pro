import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from '@rbo/components/lib/loader/Loader';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';
import AccountInput from '../field-account/input';
import DigitalInput from '../field-digital/input';
import TextInput from '../field-text/input';
import TextareaInput from '../field-textarea/input';
import Popup from './popup';

class RboFormFieldSearchInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    searchValue: PropTypes.string,
    inputType: PropTypes.oneOf(['Text', 'Textarea', 'Account', 'Digital']),
    options: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    ).isRequired,
    optionRenderer: PropTypes.func,
    popupStyle: PropTypes.object,
    popupContainerElementSelector: PropTypes.string,
    maxLength: PropTypes.number,
    isSearching: PropTypes.bool,
    isDisabled: PropTypes.bool,
    onSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      isOpened: false,
      value: props.value,
      active: props.value || (props.options.length ? props.options[0].value : null),
      selected: props.value,
      innerInputValue: props.searchValue || this.getInnerInputValue(props.value, props.options),
      innerInputFormatValue: null
    };
  }

  componentDidMount() {
    this.inputElement = document.querySelector(`#${this.props.id}`);
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: nextProps.value,
        active: nextProps.value || (nextProps.options.length ? nextProps.options[0].value : null),
        selected: nextProps.value,
        innerInputValue: nextProps.searchValue || this.getInnerInputValue(nextProps.value, nextProps.options)
      });
    } else {
      this.setState({ active: nextProps.options.length ? nextProps.options[0].value : null });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  onInnerInputFocus = (innerInputValue) => {
    if (!this.state.isFocus) {
      this.setState({
        isFocus: true,
        isOpened: true,
        innerInputValue,
        innerInputFormatValue: null
      });
      this.props.onSearch(innerInputValue);
      this.props.onFocus && this.props.onFocus();
    }
  };

  onInnerInputBlur = () => {
    if (!this.state.isOpened) {
      const innerInputValue = this.getInnerInputValue(this.state.value);
      this.setState({
        isFocus: false,
        innerInputValue,
        innerInputFormatValue: null
      });
      this.props.onSearch(innerInputValue);
      this.props.onBlur && this.props.onBlur();
    }
  };

  onInnerInputChange = (innerInputValue, innerInputFormatValue) => {
    this.setState({
      isOpened: true,
      value: null,
      selected: null,
      inputValue: null,
      innerInputValue,
      innerInputFormatValue
    });

    this.props.onSearch(innerInputValue);
    this.state.value !== null && this.props.onChange && this.props.onChange(null, '');
  };

  onOpenerMousedown = () => {
    if (!this.props.isDisabled) {
      this.setState({ isOpened: true });
    }
  };

  onOpenerClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  onClose = () => {
    this.setState({ isOpened: false });
  };

  onOptionClick = (value) => {
    const innerInputValue = this.getInnerInputValue(value);
    this.setState({
      isOpened: false,
      value,
      selected: value,
      active: value,
      innerInputValue,
      innerInputFormatValue: null
    });
    this.props.onSearch(innerInputValue);
    this.props.onChange && this.props.onChange(value, innerInputValue);
  };

  onOptionMouseEnter = (value) => {
    this.setState({ active: value });
  };

  onOptionMouseLeave = () => {
    this.setState({ active: null });
  };

  getInnerInputValue = (value, options) => {
    const usedOptions = options || this.props.options;
    const selectedOption = usedOptions.find(option => option.value === value);
    return selectedOption ? selectedOption.title : null;
  };

  getInputComponent = () => {
    switch (this.props.inputType) {
      case 'Account': return AccountInput;
      case 'Digital': return DigitalInput;
      case 'Textarea': return TextareaInput;
      case 'Text': default: return TextInput;
    }
  };

  handleKeyDown = (e) => {
    if (this.state.isFocus) {
      switch (e.which) {
        case 38: // arrow up
          e.preventDefault();
          this.prev();
          break;
        case 40: // arrow down
          e.preventDefault();
          this.next();
          break;
        case 13: // enter
          this.enter();
          break;
        default:
          break;
      }
    }
  };

  prev = () => {
    const { options } = this.props;
    const { active, isOpened } = this.state;
    if (isOpened && options.length) {
      const activeOptionIndex = options.findIndex(option => option.value === active);
      const prevActiveOptionIndex = activeOptionIndex <= 0 ? options.length - 1 : activeOptionIndex - 1;
      const value = options[prevActiveOptionIndex].value;
      this.setState({ active: value });
    }
  };

  next = () => {
    const { options } = this.props;
    const { active, isOpened } = this.state;
    if (isOpened && options.length) {
      const activeOptionIndex = options.findIndex(option => option.value === active);
      const nextActiveOptionIndex = activeOptionIndex === options.length - 1 ? 0 : activeOptionIndex + 1;
      const value = options[nextActiveOptionIndex].value;
      this.setState({ active: value });
    }
  };

  enter = () => {
    if (this.state.isOpened) {
      this.state.active && this.onOptionClick(this.state.active);
    } else {
      this.setState({ isOpened: true });
    }
  };

  render() {
    const {
      id,
      options,
      optionRenderer,
      popupStyle,
      popupContainerElementSelector,
      maxLength,
      isSearching,
      isDisabled,
      placeholder
    } = this.props;
    const { isOpened, innerInputValue, innerInputFormatValue, selected, active } = this.state;

    const formatOptions = options.map(option => ({
      ...option,
      selected: option.value === selected,
      active: option.value === active
    }));

    const InputComponent = this.getInputComponent();

    return (
      <div style={{ position: 'relative' }}>
        <div
          ref={(input) => { this.openerElement = input; }}
          onMouseDown={this.onOpenerMousedown}
          onClick={this.onOpenerClick}
        >
          <InputComponent
            id={id}
            value={innerInputValue}
            maxLength={maxLength}
            isDisabled={isDisabled}
            onFocus={this.onInnerInputFocus}
            onChange={this.onInnerInputChange}
            onBlur={this.onInnerInputBlur}
            placeholder={placeholder}
          />
          <div className={`${COMPONENT_STYLE_NAME}__icon`}>
            {isSearching ?
              <Loader className={`${COMPONENT_STYLE_NAME}__icon-loader`} size="mini" />
              :
              <Icon type="dropdown" size="16" />
            }
          </div>
        </div>
        {isOpened &&
        <Popup
          openerElement={this.openerElement}
          inputElement={this.inputElement || null}
          options={formatOptions}
          optionRenderer={optionRenderer}
          optionRendererHighlight={innerInputFormatValue}
          id={id}
          style={popupStyle}
          containerElementSelector={popupContainerElementSelector}
          onClose={this.onClose}
          onOptionClick={this.onOptionClick}
          onOptionMouseEnter={this.onOptionMouseEnter}
          onOptionMouseLeave={this.onOptionMouseLeave}
        />
        }
      </div>
    );
  }
}

export default RboFormFieldSearchInput;
