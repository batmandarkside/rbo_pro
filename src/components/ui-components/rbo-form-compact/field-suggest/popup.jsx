import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';
import Option from '../popup-option';

class RboFormFieldSearchPopup extends Component {
  static propTypes = {
    openerElement: PropTypes.object.isRequired,
    inputElement: PropTypes.object.isRequired,
    options: PropTypes.array.isRequired,
    optionRenderer: PropTypes.func,
    optionRendererHighlight: PropTypes.string,
    id: PropTypes.string,
    style: PropTypes.object,
    containerElementSelector: PropTypes.string,
    onOptionClick: PropTypes.func.isRequired,
    onOptionMouseEnter: PropTypes.func.isRequired,
    onOptionMouseLeave: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
  };

  state = {
    containerElement: document.querySelector(this.props.containerElementSelector || 'body'),
    containerElementBounding: { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 },
    openerElementBounding: { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 },
    innerElementBounding: { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 }
  };

  componentDidMount() {
    this.elementId = `_popupBox_${Date.now()}`;
    window.addEventListener('resize', this.handleWindowResize);
    document.addEventListener('scroll', this.handleDocumentScroll);
    document.addEventListener('mousedown', this.handleMouseDown, true);
    document.addEventListener('touchstart', this.handleMouseDown, true);
    document.addEventListener('mouseup', this.handleMouseUp, true);
    document.addEventListener('keydown', this.handleKeyDown);
    this.setOffset();
    setTimeout(this.setOffset, 0);
  }

  componentWillReceiveProps() {
    setTimeout(this.setOffset, 0);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
    document.removeEventListener('scroll', this.handleDocumentScroll);
    document.removeEventListener('mousedown', this.handleMouseDown, true);
    document.removeEventListener('touchstart', this.handleMouseDown, true);
    document.removeEventListener('mouseup', this.handleMouseUp, true);
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  getOffset = () => {
    const containerElementBounding = this.state.containerElement.getBoundingClientRect();
    const openerElementBounding = this.props.openerElement.getBoundingClientRect();
    const innerElementBounding = this.innerElement ?
      this.innerElement.getBoundingClientRect() :
      { top: 0, right: 0, bottom: 0, left: 0, width: 0, height: 0 };

    return {
      containerElementBounding,
      openerElementBounding,
      innerElementBounding
    };
  };

  setOffset = () => {
    if (this.innerElement) {
      this.setState({
        ...this.getOffset(),
      });
    }
  };

  handleWindowResize = () => {
    this.setOffset();
  };

  handleDocumentScroll = () => {
    this.setOffset();
  };

  handleMouseDown = (e) => {
    if (!e.target.closest(`#${this.elementId}`)) this.props.onClose();
  };

  handleMouseUp = () => {
    this.props.inputElement.focus();
  };

  handleKeyDown = (e) => {
    switch (e.which) {
      case 9: // tab
      // case 27: // esc
        this.props.onClose();
        break;
      default:
        break;
    }
  };

  render() {
    const {
      options,
      optionRenderer,
      optionRendererHighlight,
      id,
      style,
      onOptionClick,
      onOptionMouseEnter,
      onOptionMouseLeave
    } = this.props;
    const { containerElementBounding, openerElementBounding, innerElementBounding } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__popup`,
      'm-search'
    );

    const topSpace = openerElementBounding.top - containerElementBounding.top;
    const bottomSpace = containerElementBounding.bottom - openerElementBounding.bottom;
    let top = 'auto';
    let bottom = 'auto';
    let maxHeight = 400;
    if (bottomSpace > 412 || bottomSpace > innerElementBounding.height + 12 || bottomSpace >= topSpace) {
      top = openerElementBounding.height + 4;
      maxHeight = bottomSpace - 12;
    } else {
      bottom = openerElementBounding.height + 4;
      maxHeight = topSpace - 12;
    }
    if (maxHeight > 400) maxHeight = 400;

    const elementStyle = {
      top,
      bottom,
      maxHeight,
      ...style
    };

    const activeOption = options.find(option => option.active);
    if (activeOption) {
      const activeOptionElement =
        document.querySelector(`#${this.elementId} [data-value="${activeOption.value.replace(/"/g, '\\"')}"]`);
      if (this.element && activeOptionElement) {
        const elementOffset = this.element.getBoundingClientRect();
        const activeOptionElementOffset = activeOptionElement.getBoundingClientRect();
        if (elementOffset.top > activeOptionElementOffset.top) {
          this.element.scrollTop = this.element.scrollTop - (elementOffset.top - activeOptionElementOffset.top);
        }
        if (elementOffset.bottom < activeOptionElementOffset.bottom) {
          this.element.scrollTop = this.element.scrollTop + (activeOptionElementOffset.bottom - elementOffset.bottom);
        }
      }
    }

    return (
      <div
        id={this.elementId}
        className={elementClassName}
        style={elementStyle}
        ref={(element) => { this.element = element; }}
      >
        <div
          className={`${COMPONENT_STYLE_NAME}__popup-inner`}
          ref={(element) => { this.innerElement = element; }}
        >
          {!!options.length &&
          <div className={`${COMPONENT_STYLE_NAME}__popup-options`}>
            {options.map((option, key) => (
              <Option
                key={key}
                value={option.value}
                isSelected={option.selected}
                isActive={option.active}
                onClick={onOptionClick}
                onMouseEnter={onOptionMouseEnter}
                onMouseLeave={onOptionMouseLeave}
              >
                {optionRenderer ? optionRenderer(id, option, optionRendererHighlight) : option.title}
              </Option>
            ))}
          </div>
          }
          {!options.length &&
          <div className={`${COMPONENT_STYLE_NAME}__popup-empty`}>
            Ничего не найдено
          </div>
          }
        </div>
      </div>
    );
  }
}

export default RboFormFieldSearchPopup;
