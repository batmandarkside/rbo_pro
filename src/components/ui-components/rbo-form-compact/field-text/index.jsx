import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { sizeMap } from '../utils';
import { COMPONENT_STYLE_NAME } from '../constants';
import Label from '../label';
import Input from './input';

class RboFormFieldText extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    size: PropTypes.string,
    label: PropTypes.string,
    pseudoLabel: PropTypes.bool,
    value: PropTypes.string,
    maxLength: PropTypes.number,
    isDisabled: PropTypes.bool,
    isWarning: PropTypes.bool,
    isError: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    getConstraint: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      value: this.props.value
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value && nextProps.value !== undefined) {
      this.setState({ value: nextProps.value });
    }
  }

  handleOnFocus = () => {
    this.setState({ isFocus: true });
    if (this.props.onFocus) this.props.onFocus({ fieldId: this.props.id });
  };

  handleOnChange = (value) => {
    this.setState({ value });
    if (this.props.onChange) this.props.onChange({ fieldId: this.props.id, fieldValue: value });
  };

  handleOnBlur = () => {
    this.setState({ isFocus: false });
    if (this.props.onBlur) this.props.onBlur({ fieldId: this.props.id });
  };

  render() {
    const { id, size, label, pseudoLabel, maxLength, isDisabled, isWarning, isError, getConstraint, placeholder } = this.props;
    const { isFocus, value } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__field`,
      'm-text',
      pseudoLabel && 'm-pseudo-label',
      isDisabled && 's-disabled',
      isWarning && 's-warning',
      isError && 's-error',
      isFocus && 's-focused',
      value && 's-filled'
    );
    const style = { width: sizeMap[size] || size || '100%' };

    return (
      <div
        className={elementClassName}
        style={style}
      >
        {!!label &&
        <Label label={label} htmlFor={id} />
        }
        <Input
          id={id}
          value={value}
          maxLength={maxLength}
          isDisabled={isDisabled}
          onFocus={this.handleOnFocus}
          onChange={this.handleOnChange}
          onBlur={this.handleOnBlur}
          getConstraint={getConstraint}
          placeholder={placeholder}
        />
      </div>
    );
  }
}

export default RboFormFieldText;
