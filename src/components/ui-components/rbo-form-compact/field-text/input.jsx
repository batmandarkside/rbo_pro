import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboFormFieldTextInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    maxLength: PropTypes.number,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    getConstraint: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value || ''
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({ value: nextProps.value || '' });
    }
  }

  onFocus = () => {
    this.props.onFocus && this.props.onFocus(this.state.value);
  };

  onChange = (e) => {
    const { id, getConstraint } = this.props;
    const { value } = e.target;

    if (!getConstraint) {
      this.changeValue(value);
      return;
    }

    const constraint = getConstraint(id);
    const maxLength = constraint.get('maxLength', 0);
    const pattern = constraint.get('pattern', '').replace(']*', ']');

    const regExp = new RegExp(pattern);

    const validValue = value
      .split('')
      .filter(letter => regExp.test(letter))
      .join('')
      .slice(0, maxLength);
    if (this.state.value !== validValue) {
      this.changeValue(validValue);
    }
  };

  onBlur = () => {
    this.props.onBlur && this.props.onBlur(this.state.value);
  };

  changeValue = (value) => {
    this.setState({ value });
    this.props.onChange && this.props.onChange(value, value);
  };

  render() {
    const { id, maxLength, isDisabled, placeholder } = this.props;
    const { value } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-text'
    );

    return (
      <input
        spellCheck="false"
        autoComplete="off"
        autoCorrect="off"
        type="text"
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
        id={id}
        value={value}
        maxLength={maxLength}
        disabled={isDisabled}
        onFocus={this.onFocus}
        onChange={this.onChange}
        onBlur={this.onBlur}
        placeholder={placeholder}
      />
    );
  }
}

export default RboFormFieldTextInput;
