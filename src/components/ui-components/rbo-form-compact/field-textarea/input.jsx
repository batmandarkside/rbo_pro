import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';

class RboFormFieldTextareaInput extends Component {
  static propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    maxLength: PropTypes.number,
    height: PropTypes.number,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value || ''
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({ value: nextProps.value || '' });
    }
  }

  onFocus = () => {
    this.props.onFocus && this.props.onFocus(this.state.value);
  };

  onChange = (e) => {
    this.setState({ value: e.target.value });
    this.props.onChange && this.props.onChange(e.target.value, e.target.value);
  };

  onBlur = () => {
    this.props.onBlur && this.props.onBlur(this.state.value);
  };

  render() {
    const { id, maxLength, height, isDisabled, placeholder } = this.props;
    const { value } = this.state;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__input`,
      'm-textarea',
      height && `m-height-${height}`
    );

    return (
      <textarea
        spellCheck="false"
        autoComplete="off"
        autoCorrect="off"
        ref={(input) => { this.inputElement = input; }}
        className={elementClassName}
        id={id}
        value={value}
        maxLength={maxLength}
        disabled={isDisabled}
        onFocus={this.onFocus}
        onChange={this.onChange}
        onBlur={this.onBlur}
        placeholder={placeholder}
      />
    );
  }
}

export default RboFormFieldTextareaInput;
