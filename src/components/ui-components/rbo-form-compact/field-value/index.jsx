import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { sizeMap } from '../utils';
import { COMPONENT_STYLE_NAME } from '../constants';
import Label from '../label';

const RboFormFieldValue = (props) => {
  const { id, size, label, pseudoLabel, value, textAlign, isWarning, isError } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__field`,
    'm-value',
    pseudoLabel && 'm-pseudo-label',
    isWarning && 's-warning',
    isError && 's-error',
    value && 's-filled',
    !label && 'm-without-label'
  );

  const style = {
    width: sizeMap[size] || size || '100%',
    textAlign
  };

  return (
    <div
      className={elementClassName}
      style={style}
    >
      {!!label &&
      <Label label={label} />
      }
      <div className={`${COMPONENT_STYLE_NAME}__value`} id={id}>
        {value}
      </div>
    </div>
  );
};

RboFormFieldValue.propTypes = {
  id: PropTypes.string,
  size: PropTypes.string,
  label: PropTypes.string,
  pseudoLabel: PropTypes.bool,
  value: PropTypes.string,
  textAlign: PropTypes.oneOf(['left', 'center', 'right']),
  isWarning: PropTypes.bool,
  isError: PropTypes.bool,
};

export default RboFormFieldValue;
