import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Label from './label';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormFieldset = (props) => {
  const { label, pseudoLabel, children } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__fieldset`,
    pseudoLabel && 'm-pseudo-label',
  );

  return (
    <div className={elementClassName}>
      {!!label &&
      <Label label={label} />
      }
      {children}
    </div>
  );
};

RboFormFieldset.propTypes = {
  label: PropTypes.string,
  pseudoLabel: PropTypes.bool,
  children: PropTypes.node.isRequired
};

export default RboFormFieldset;
