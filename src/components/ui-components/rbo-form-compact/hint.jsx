import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormHint = (props) => {
  const { pseudoLabel, children } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__hint`,
    pseudoLabel && 'm-pseudo-label'
  );

  return (
    <div className={elementClassName}>
      {children}
    </div>
  );
};

RboFormHint.propTypes = {
  pseudoLabel: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default RboFormHint;
