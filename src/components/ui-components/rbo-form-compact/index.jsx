import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';

class RboForm extends Component {

  static propTypes = {
    children: PropTypes.node.isRequired,
    initialFocusFieldId: PropTypes.string,
    onSubmit: PropTypes.func
  };

  componentDidMount = () => {
    this.setInitialFocus();
  };

  setInitialFocus = () => {
    const { initialFocusFieldId } = this.props;
    if (initialFocusFieldId && document.querySelector(`#${initialFocusFieldId}`)) {
      document.querySelector(`#${initialFocusFieldId}`).focus();
    }
  };

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    e.preventDefault();
    onSubmit && onSubmit(e);
  };

  render() {
    const { children, onSubmit } = this.props;

    return (
      <div className={COMPONENT_STYLE_NAME}>
        {onSubmit &&
        <form onSubmit={this.handleSubmit}>
          {children}
        </form>
        }
        {!onSubmit && children}
      </div>
    );
  }
}

export default RboForm;

export RboFormSection from './section';
export RboFormBlockset from './blockset';
export RboFormBlock from './block';
export RboFormRowset from './rowset';
export RboFormRow from './row';
export RboFormCell from './cell';
export RboFormFieldset from './fieldset';
export RboFormLabel from './label';
export RboFormHint from './hint';
export RboFormError from './error';
export RboFormCollapse from './collapse';

export RboFormFieldAccount from './field-account';
export RboFormFieldButton from './field-button';
export RboFormFieldCheckbox from './field-checkbox';
export RboFormFieldDate from './field-date';
export RboFormFieldDecimalNumber from './field-decimal-number';
export RboFormFieldDigital from './field-digital';
export RboFormFieldDropdown from './field-dropdown';
export RboFormFieldFiles from './field-files';
export RboFormFieldIntegerNumber from './field-integer-number';
export RboFormFieldRadio from './field-radio';
export RboFormFieldSearch from './field-search';
export RboFormFieldSuggest from './field-suggest';
export RboFormFieldText from './field-text';
export RboFormFieldTextarea from './field-textarea';
export RboFormFieldValue from './field-value';

export RboFormAutoTransitions from './auto-transitions';

export { RboFormFieldPopupOptionInner } from './popup-option';
