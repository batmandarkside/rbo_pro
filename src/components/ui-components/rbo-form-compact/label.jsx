import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormLabel = (props) => {
  const { label, htmlFor } = props;

  return (
    <label
      className={`${COMPONENT_STYLE_NAME}__label`}
      htmlFor={htmlFor}
    >
      {label}
    </label>
  );
};

RboFormLabel.propTypes = {
  label: PropTypes.string.isRequired,
  htmlFor: PropTypes.string
};

export default RboFormLabel;
