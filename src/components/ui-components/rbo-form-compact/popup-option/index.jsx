import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboFormFieldPopupOption = (props) => {
  const { value, isSelected, isActive, onClick, onMouseEnter, onMouseLeave, children } = props;

  const handleOnClick = () => onClick(value);

  const handleOnMouseEnter = () => onMouseEnter(value);

  const handleOnMouseLeave = () => onMouseLeave();

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__popup-option`,
    isActive && 's-active',
    isSelected && 's-selected'
  );

  return (
    <div
      data-value={value}
      className={elementClassName}
      onMouseEnter={handleOnMouseEnter}
      onMouseLeave={handleOnMouseLeave}
      onClick={handleOnClick}
    >
      {children}
    </div>
  );
};

RboFormFieldPopupOption.propTypes = {
  value: PropTypes.string.isRequired,
  isSelected: PropTypes.bool,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  onMouseEnter: PropTypes.func.isRequired,
  onMouseLeave: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired
};

export default RboFormFieldPopupOption;

export RboFormFieldPopupOptionInner from './inner';
