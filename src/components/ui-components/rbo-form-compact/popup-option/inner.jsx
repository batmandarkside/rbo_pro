import React  from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Highlighter from 'react-highlight-words';
import { Link }  from 'react-router-dom';
import { regExpEscape } from 'utils';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../constants';

const RboFormFieldPopupOptionInner = (props) => {
  const { isInline, label, title, describe, extra, editLink, highlight } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__popup-option-inner`,
    isInline && 'm-inline'
  );

  const escapedHighlight = highlight && {
    value: highlight.value ? regExpEscape(highlight.value) : null,
    inTitleValue: highlight.inTitleValue ? regExpEscape(highlight.inTitleValue) : null,
    inLabelValue: highlight.inLabelValue ? regExpEscape(highlight.inLabelValue) : null,
    inExtraValue: highlight.inExtraValue ? regExpEscape(highlight.inExtraValue) : null,
    inDescribeValue: highlight.inDescribeValue ? regExpEscape(highlight.inDescribeValue) : null,
  };

  return (
    <div className={elementClassName}>
      <div className={`${COMPONENT_STYLE_NAME}__popup-option-main`}>
        {title &&
        <div className={`${COMPONENT_STYLE_NAME}__popup-option-title`}>
          {highlight && highlight.inTitle ?
            <Highlighter
              searchWords={[escapedHighlight.inTitleValue, escapedHighlight.value]}
              textToHighlight={highlight.inTitle}
            />
            :
            title
          }
        </div>
        }
        {label &&
        <div className={`${COMPONENT_STYLE_NAME}__popup-option-label`}>
          {highlight && highlight.inLabel ?
            <Highlighter
              searchWords={[escapedHighlight.inLabelValue, escapedHighlight.value]}
              textToHighlight={highlight.inLabel}
            />
            :
            label
          }
        </div>
        }
        {extra &&
        <div className={`${COMPONENT_STYLE_NAME}__popup-option-extra`}>
          {highlight && highlight.inExtra ?
            <Highlighter
              searchWords={[escapedHighlight.inExtraValue, escapedHighlight.value]}
              textToHighlight={highlight.inExtra}
            />
            :
            extra
          }
        </div>
        }
      </div>
      {describe &&
      <div className={`${COMPONENT_STYLE_NAME}__popup-option-describe`}>
        {highlight && highlight.inDescribe ?
          <Highlighter
            searchWords={[escapedHighlight.inDescribeValue, escapedHighlight.value]}
            textToHighlight={highlight.inDescribe}
          />
          :
          describe
        }
      </div>
      }
      {editLink &&
      <Link
        className={`${COMPONENT_STYLE_NAME}__popup-option-edit-link`}
        to={editLink}
        onClick={e => e.stopPropagation()}
        title="Редактировать"
      >
        <Icon type="edit" size="16" displayBlock />
      </Link>
      }
    </div>
  );
};

RboFormFieldPopupOptionInner.propTypes = {
  isInline: PropTypes.bool,
  title: PropTypes.string,
  label: PropTypes.string,
  describe: PropTypes.string,
  extra: PropTypes.string,
  editLink: PropTypes.string,
  highlight: PropTypes.shape({
    value: PropTypes.string,
    inTitle: PropTypes.string,
    inTitleValue: PropTypes.string,
    inLabel: PropTypes.string,
    inLabelValue: PropTypes.string,
    inDescribe: PropTypes.string,
    inDescribeValue: PropTypes.string,
    inExtra: PropTypes.string,
    inExtraValue: PropTypes.string
  })
};

export default RboFormFieldPopupOptionInner;
