import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormRow = (props) => {
  const { children } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__row`}>
      {children}
    </div>
  );
};

RboFormRow.propTypes = {
  children: PropTypes.node.isRequired
};

export default RboFormRow;
