import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormRowset = (props) => {
  const { isMain, children } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__rowset`,
    isMain && 'm-main'
  );

  return (
    <div className={elementClassName}>
      {children}
    </div>
  );
};

RboFormRowset.propTypes = {
  isMain: PropTypes.bool,
  children: PropTypes.node.isRequired
};

export default RboFormRowset;
