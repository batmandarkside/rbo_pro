import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from './constants';

const RboFormSection = (props) => {
  const { id, title, note, isCollapsed, children } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__section`,
    isCollapsed && 's-collapsed'
  );

  return (
    <div
      data-id={id}
      className={elementClassName}
    >
      {title &&
      <div className={`${COMPONENT_STYLE_NAME}__section-header`}>
        <div className={`${COMPONENT_STYLE_NAME}__section-title`}>
          <h3>{title}</h3>
        </div>
        {note &&
        <div className={`${COMPONENT_STYLE_NAME}__section-note`}>
          {note}
        </div>
        }
      </div>
      }
      <div className={`${COMPONENT_STYLE_NAME}__section-content`}>
        {children}
      </div>
    </div>
  );
};

RboFormSection.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  note: PropTypes.string,
  isCollapsed: PropTypes.bool,
  children: PropTypes.node.isRequired
};

export default RboFormSection;
