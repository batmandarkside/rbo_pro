export const getChar = (e) => {
  if (e.which === null) {
    if (e.keyCode < 32) return null;
    return String.fromCharCode(e.keyCode);
  }
  if (e.which !== 0 && e.charCode !== 0) {
    if (e.which < 32) return null;
    return String.fromCharCode(e.which);
  }
  return null;
};

export const getCaretPosition = (value, beforeCaretPosValue) => {
  let pos = 0; let conj = 0;
  if (beforeCaretPosValue) {
    value.split('').some((symbol) => {
      pos += 1;
      if (symbol === beforeCaretPosValue[conj]) {
        conj += 1;
        if (conj === beforeCaretPosValue.length) return true;
      }
      return false;
    });
  }
  return pos;
};

export const sizeMap = {
  '1/2': '50%',
  '1/3': '33.33%',
  '2/3': '66.66%',
  '1/4': '25%',
  '2/4': '50%',
  '3/4': '75%',
  '1/6': '16.66%',
  '2/6': '33.33%',
  '3/6': '50%',
  '4/6': '66.66%',
  '5/6': '83.33%',
};

export const getFieldElementSelectorByIds = (fieldIds) => {
  const foundFields = fieldIds.filter(fieldId => !!document.querySelector(`#${fieldId}`));
  return foundFields.length ? document.querySelector(`#${foundFields[0]}`) : null;
};
