import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import Icon from 'ui-components/icon';
import PopupBox from 'components/ui-components/popup-box';
import { COMPONENT_STYLE_NAME } from './constants';
import Item from './item';

class RboTabsExtra extends Component {
  static propTypes = {
    tabs: PropTypes.instanceOf(List).isRequired,
    onTabClick: PropTypes.func.isRequired,
    containerElementSelector: PropTypes.string
  };

  state = { isPopupOpen: false };

  handlePopupOpen = () => this.setState({ isPopupOpen: true });

  handlePopupClose = () => this.setState({ isPopupOpen: false });

  handleTabClick = (tabId) => {
    this.setState({ isPopupOpen: false });
    this.props.onTabClick(tabId);
  };

  render() {
    const { tabs, containerElementSelector } = this.props;
    const { isPopupOpen } = this.state;

    return (
      <div className={`${COMPONENT_STYLE_NAME}__extra`}>
        <div
          className={`${COMPONENT_STYLE_NAME}__extra-toggle`}
          ref={(element) => { this.popupOpenerElement = element; }}
          onClick={this.handlePopupOpen}
          onMouseDown={this.handleToggleOnMouseDown}
          onMouseUp={this.handleToggleOnMouseUp}
        >
          <Icon type={isPopupOpen ? 'arrow-up' : 'arrow-down'} size="16" displayBlock />
        </div>
        {isPopupOpen &&
        <PopupBox
          openerElement={this.popupOpenerElement}
          containerElementSelector={containerElementSelector}
          onClose={this.handlePopupClose}
        >
          <div className={`${COMPONENT_STYLE_NAME}__extra-popup`}>
            <div className={`${COMPONENT_STYLE_NAME}__extra-items`}>
              {tabs.map((tab, key) => (
                <Item
                  key={key}
                  id={tab.get('id')}
                  title={tab.get('title')}
                  isActive={tab.get('isActive')}
                  onClick={this.handleTabClick}
                />
              ))}
            </div>
          </div>
        </PopupBox>
        }
      </div>
    );
  }
}

export default RboTabsExtra;
