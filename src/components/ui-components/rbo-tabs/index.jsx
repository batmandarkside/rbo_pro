import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import classnames from 'classnames';
import Extra from './extra';
import Item from './item';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';

class RboTabs extends Component {
  static propTypes = {
    tabs: PropTypes.instanceOf(List).isRequired,
    align: PropTypes.oneOf(['left', 'center', 'right']),
    onTabClick: PropTypes.func.isRequired,
    containerElementSelector: PropTypes.string
  };

  state = { isCompactView: false };

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize);
    this.handleWindowResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
  }

  handleWindowResize = () => {
    const elementOffset = this.element.getBoundingClientRect();
    const offsetWidth = elementOffset.width || 0;
    const hiddenElementOffset = this.hiddenElement.getBoundingClientRect();
    const itemsWidth = hiddenElementOffset.width || 0;
    const isCompactView = offsetWidth < itemsWidth;
    this.setState({ isCompactView });
  };

  render() {
    const { tabs, align, containerElementSelector, onTabClick } = this.props;
    const { isCompactView } = this.state;

    const activeTab = tabs.find(item => item.get('isActive'));
    const extraTabs = tabs.filter(item => !item.get('isActive'));

    const elementClassName = classnames(
      COMPONENT_STYLE_NAME,
      align && `m-align-${align}`
    );

    return (
      <div
        className={elementClassName}
        ref={(element) => { this.element = element; }}
      >
        <div
          className={`${COMPONENT_STYLE_NAME}__hidden`}
          ref={(element) => { this.hiddenElement = element; }}
        >
          <div className={`${COMPONENT_STYLE_NAME}__items`}>
            {tabs.map((tab, key) => (
              <Item
                key={key}
                id={tab.get('id')}
                title={tab.get('title')}
                isActive={tab.get('isActive')}
                onClick={onTabClick}
              />
            ))}
          </div>
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__main`}>
          <div className={`${COMPONENT_STYLE_NAME}__items`}>
            {!isCompactView && tabs.map((tab, key) => (
              <Item
                key={key}
                id={tab.get('id')}
                title={tab.get('title')}
                isActive={tab.get('isActive')}
                onClick={onTabClick}
              />
            ))}
            {isCompactView &&
            <Item
              id={activeTab.get('id')}
              title={activeTab.get('title')}
              isActive={activeTab.get('isActive')}
              onClick={onTabClick}
            />
            }
          </div>
        </div>
        {isCompactView &&
        <Extra
          tabs={extraTabs}
          onTabClick={onTabClick}
          containerElementSelector={containerElementSelector}
        />
        }
      </div>
    );
  }
}

export default RboTabs;
