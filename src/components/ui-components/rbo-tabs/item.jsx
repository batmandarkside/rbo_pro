import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from './constants';

const RboTabsItem = (props) => {
  const { id, title, isActive, onClick } = props;

  const handleOnClick = () => {
    if (!isActive) onClick({ tabId: id });
  };

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__item`,
    isActive && 's-active'
  );

  return (
    <div
      className={elementClassName}
      onClick={handleOnClick}
    >
      <div className={`${COMPONENT_STYLE_NAME}__item-inner`}>
        {title}
      </div>
    </div>
  );
};

RboTabsItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};

export default RboTabsItem;
