/* eslint-disable react/jsx-filename-extension */
import React            from 'react';
import { expect }       from 'chai';
import { shallow }      from 'enzyme';
import { ShadowBlock }  from '../index';

describe('<ShadowBlock /> tests', () => {
  it('Renders <ShadowBlock />', () => {
    const wrapper = shallow(
      <ShadowBlock className="ShadowBlockClass">
        <div />
      </ShadowBlock>
    );
    expect(wrapper.find('.ShadowBlockClass')).to.have.length(1);
  });
});
