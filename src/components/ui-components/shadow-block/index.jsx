import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './style.css';

/**
 *
 * @param props
 * @returns {XML}
 */
export const ShadowBlock = (props) => {
  const { className, children } = props;
  const classSelectors = classNames('b-shadow-block', {
    [className]: !!className
  });
  return (
    <div className={classSelectors}>
      {children}
    </div>
  );
};

ShadowBlock.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

export default ShadowBlock;
