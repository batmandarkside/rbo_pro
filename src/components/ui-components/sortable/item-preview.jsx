import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DragLayer } from 'react-dnd';

function getItemStyles(props) {
  const { currentOffset, containerId } = props;
  if (!currentOffset) {
    return {
      display: 'none'
    };
  }

  const { x, y } = currentOffset;
  const transform = `translate(${x}px, ${y}px)`;
  const maxWidth = document.getElementById(containerId).offsetWidth;
  return {
    transform,
    maxWidth
  };
}

@DragLayer(monitor => ({
  item: monitor.getItem(),
  currentOffset: monitor.getSourceClientOffset(),
  isDragging: monitor.isDragging()
}))
export default class SortableItemPreview extends Component {
  static propTypes = {
    item: PropTypes.object,
    isDragging: PropTypes.bool,
    className: PropTypes.string,
    itemClassName: PropTypes.string
  };

  render() {
    const { item, isDragging, className, itemClassName } = this.props;
    if (!isDragging) {
      return null;
    }

    return (
      <div className={className}>
        <div className={itemClassName} style={getItemStyles(this.props)}>
          {item.children}
        </div>
      </div>
    );
  }
}
