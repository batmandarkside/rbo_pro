/**
 * @link https://react-dnd.github.io/react-dnd/
 */

import React, {
  Component,
  PropTypes
}  from 'react';
import { findDOMNode }                  from 'react-dom';
import { DragSource, DropTarget }       from 'react-dnd';

export const ITEM_TYPES = {
  CARD: 'card'
};


const style = {
  backgroundColor: 'white'
};

/**
 * @link https://react-dnd.github.io/react-dnd/docs-drag-source.html
 * @type {{beginDrag: ((props)), endDrag: ((props))}}
 */
const cardSource = {
  beginDrag(props) {
    return {
      id   : props.id,
      index: props.index,
      children: props.children
    };
  },

  canDrag(props) {
    return props.isCanDrag;
  },

  endDrag(props) {
    props.endDrag && props.endDrag({
      id: props.id,
      index: props.index,
      children: props.children
    });
  }
};

const cardTarget = {
  canDrop() {
    return false;
  },
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect(); // eslint-disable-line react/no-find-dom-node

    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the top
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%

    // Dragging downwards
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }

    // Dragging upwards
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }

    // Time to actually perform the action
    props.moveCard(dragIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex; // eslint-disable-line no-param-reassign
  },
};

@DropTarget(ITEM_TYPES.CARD, cardTarget, connect => ({
  connectDropTarget: connect.dropTarget(),
}))
@DragSource(ITEM_TYPES.CARD, cardSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))
export default class SortableItem extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired, // eslint-disable-line react/no-unused-prop-types
    id: PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
      PropTypes.number,
      PropTypes.string,
    ]).isRequired, // eslint-disable-line react/no-unused-prop-types
    moveCard: PropTypes.func.isRequired, // eslint-disable-line react/no-unused-prop-types
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    className: PropTypes.string,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.element),
      PropTypes.node
    ]).isRequired,
  };

  render() {
    const { children, isDragging, connectDragSource, connectDropTarget, className } = this.props;
    const opacity = isDragging ? 0 : 1;

    return connectDragSource(connectDropTarget(
      <div style={{ ...style, opacity }} className={className}>
        {children}
      </div>,
    ));
  }
}
