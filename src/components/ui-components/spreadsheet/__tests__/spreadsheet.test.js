/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { List, Map } from 'immutable';
import Spreadsheet, { Toolbar, Filters, Table, Selection, Export, Pagination } from '../index';

describe('Spreadsheet UI Component', () => {
  it('render', () => {
    const tableProps = {
      columns: List(),
      list: List()
    };

    const filtersProps = {
      conditions: List(),
      filters: List(),
      changeFiltersAction: jest.fn()
    };

    const selectionProps = {
      main: {
        label: '',
        value: ''
      }
    };

    const exportProps = {
      label: '',
      onClick: jest.fn()
    };

    const paginationProps = {
      pathname: '/',
      pages: Map(),
      onClick: jest.fn()
    };

    const wrapper = shallow(
      <Spreadsheet>
        <header>
          <Toolbar />
          <Filters
            {...filtersProps}
          />
        </header>
        <main>
          <Table
            {...tableProps}
          />
        </main>
        <footer>
          <Selection
            {...selectionProps}
          />
          <Export
            {...exportProps}
          />
          <Pagination
            {...paginationProps}
          />
        </footer>
      </Spreadsheet>
    );

    expect(wrapper.find('.b-spreadsheet')).to.have.length(1);
    expect(wrapper.find('.b-spreadsheet__header')).to.have.length(1);
    expect(wrapper.find('.b-spreadsheet__main')).to.have.length(1);
    expect(wrapper.find('.b-spreadsheet__footer')).to.have.length(1);
    wrapper.unmount();
  });
});
