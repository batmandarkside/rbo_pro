export const LOCATORS = {
  TABLE_GROUP: 'spreadsheetTableGroup',
  TABLE_ITEM: 'spreadsheetTableItem',
  TABLE_HEAD_ROW: 'spreadsheetTableHeadRow',
  TABLE_ROW: 'spreadsheetTableRow',
  TABLE_HEAD_CELL: 'spreadsheetTableHeadCell',
  TABLE_CELL: 'spreadsheetTableCell'
};
