import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const SpreadsheetExport = (props) => {
  const { dataLoc, label, onClick, isLoading } = props;
  return (
    <div className="b-spreadsheet__export" data-loc={dataLoc}>
      <div className="b-spreadsheet__export-inner">
        {isLoading ?
          <span>
            Идет загрузка...
          </span>
          :
          <span className="m-dashed-link" onClick={onClick}>
            {label}
          </span>
        }
      </div>
    </div>
  );
};

SpreadsheetExport.propTypes = {
  dataLoc: PropTypes.string,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  isLoading: PropTypes.bool
};

export default SpreadsheetExport;
