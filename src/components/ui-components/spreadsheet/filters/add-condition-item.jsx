import React from 'react';
import PropTypes from 'prop-types';

const SpreadsheetFiltersAddConditionItem = (props) => {
  const { id, title, onClick } = props;

  const handleOnClick = (e) => {
    e.stopPropagation();
    onClick(id);
  };

  return (
    <div className="b-spreadsheet__filters-add-condition-item" onClick={handleOnClick}>
      {title}
    </div>
  );
};

SpreadsheetFiltersAddConditionItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default SpreadsheetFiltersAddConditionItem;
