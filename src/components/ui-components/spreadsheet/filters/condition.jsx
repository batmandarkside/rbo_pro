import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import classnames from 'classnames';
import moment from 'moment-timezone';
import PopupBox from 'components/ui-components/popup-box';
import Icon from 'components/ui-components/icon';
import ConditionStringForm from './form/condition-string';
import ConditionMultipleSelectForm, {
  NAMES as CONDITION_MULTIPLE_SELECT_FORM_NAMES
} from './form/condition-multiple-select';
import ConditionSelectForm from './form/condition-select';
import ConditionDatesRangeForm, {
  NAMES as CONDITION_DATES_RANGE_FORM_NAMES
} from './form/condition-dates-range';
import ConditionAmountsRangeForm, {
  NAMES as CONDITION_AMOUNT_RANGE_FORM_NAMES
} from './form/condition-amounts-range';
import { CONDITION_FIELD_TYPE } from './constants';
import ConditionSuggestForm from './form/condition-suggest';
import ConditionSearchForm from './form/condition-search';

class SpreadsheetFiltersCondition extends PureComponent {
  static propTypes = {
    dataLocs: PropTypes.object,
    id: PropTypes.string.isRequired,
    condition: PropTypes.instanceOf(Map).isRequired,
    params: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
      PropTypes.instanceOf(Map),
      PropTypes.instanceOf(List)
    ]),
    value: PropTypes.string.isRequired,
    isChanging: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    onPopupClose: PropTypes.func.isRequired,
    onRemoveClick: PropTypes.func,
    dictionaryFormFieldOptionRenderer: PropTypes.func,
    onDictionaryFormInputChange: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: false
    };
  }

  componentDidMount() {
    setTimeout(this.setPopupState, 10);
    document.addEventListener('click', this.handleClick, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClick, true);
  }

  setPopupState = () => {
    this.setState({
      isPopupOpen: this.props.condition.get('type') !== CONDITION_FIELD_TYPE.BOOLEAN && this.props.isChanging
    });
  };

  handleClick = (e) => {
    if (this.state.isPopupOpen && this.props.condition.get('type') === CONDITION_FIELD_TYPE.DATE_RANGE &&
      !e.target.closest('.b-spreadsheet__filters-condition-popup')) {
      e.stopPropagation();
      this.handlePopupClose();
    }
  };

  handleOnRemoveClick = (e) => {
    e.stopPropagation();
    this.props.onRemoveClick(this.props.id);
  };

  handlePopupOpen = () => {
    this.setState({
      isPopupOpen: this.props.condition.get('type') !== CONDITION_FIELD_TYPE.BOOLEAN
    });
  };

  handlePopupClose = () => {
    this.setState({
      isPopupOpen: false
    });

    this.props.onPopupClose(this.props.id);
  };

  handleChangeConditionString = ({ conditionValue }) => {
    this.setState({
      isPopupOpen: false
    });

    const value = conditionValue;

    if (value === this.props.params) {
      this.props.onPopupClose(this.props.id);
    } else {
      this.props.onChange(this.props.id, value);
    }
  };

  handleChangeConditionSelect = ({ conditionValue }) => {
    this.setState({
      isPopupOpen: false
    });

    const value =  this.props.condition.getIn(['values', parseInt(conditionValue, 10)]);

    if (value === this.props.params) {
      this.props.onPopupClose(this.props.id);
    } else {
      this.props.onChange(this.props.id, value);
    }
  };

  handleChangeConditionMultipleSelect = ({ conditionValue }) => {
    this.setState({
      isPopupOpen: false
    });

    const selectedKeys = conditionValue.filter(v => v === true).reduce(
        (result, item, key) =>
          result.push(
            parseInt(key.replace(`${CONDITION_MULTIPLE_SELECT_FORM_NAMES.VALUE}__`, ''), 10)
          ),
        List()
    );

    const value = this.props.condition.get('values').filter(
      (condition, key) => !isNaN(selectedKeys.find(selectedKey => selectedKey === key))
    );

    if (value.equals(this.props.params || List([]))) {
      this.props.onPopupClose(this.props.id);
    } else {
      this.props.onChange(this.props.id, value.size ? value : null);
    }
  };

  handleChangeConditionDatesRange = (formValues) => {
    this.setState({
      isPopupOpen: false
    });

    const values = Map({
      dateFrom: formValues.get(CONDITION_DATES_RANGE_FORM_NAMES.VALUE_FROM),
      dateTo: formValues.get(CONDITION_DATES_RANGE_FORM_NAMES.VALUE_TO)
    });

    if (values.equals(this.props.params || Map())) {
      this.props.onPopupClose(this.props.id);
    } else {
      this.props.onChange(this.props.id, values.get('dateFrom') || values.get('dateTo') ? values : null);
    }
  };

  handleChangeConditionAmountsRange = (formValues) => {
    this.setState({
      isPopupOpen: false
    });

    const valueFrom = formValues.get(CONDITION_AMOUNT_RANGE_FORM_NAMES.VALUE_FROM);
    const valueTo = formValues.get(CONDITION_AMOUNT_RANGE_FORM_NAMES.VALUE_TO);
    let values = Map();

    if (valueFrom) values = values.set('amountFrom', valueFrom);
    if (valueTo) values = values.set('amountTo', valueTo);

    if (values.equals(this.props.params || Map())) this.props.onPopupClose(this.props.id);
    else this.props.onChange(this.props.id, values.get('amountFrom') || values.get('amountTo') ? values : null);
  };

  handleChangeConditionSuggest = ({ value }) => {
    this.setState({
      isPopupOpen: false
    });

    if ((value && value === this.props.params) || (!value && !this.props.params)) {
      this.props.onPopupClose(this.props.id);
    } else {
      this.props.onChange(this.props.id, value);
    }
  };

  handleChangeConditionSearch = ({ value }) => {
    this.setState({
      isPopupOpen: false
    });

    if ((value && value === this.props.params) || (!value && !this.props.params)) {
      this.props.onPopupClose(this.props.id);
    } else {
      this.props.onChange(this.props.id, value);
    }
  };

  renderForm = () => {
    const {
      condition,
      params,
      dictionaryFormFieldOptionRenderer,
      onDictionaryFormInputChange,
    } = this.props;

    switch (condition.get('type')) {
      case CONDITION_FIELD_TYPE.STRING:
        return (
          <ConditionStringForm
            condition={condition}
            value={params}
            onSubmit={this.handleChangeConditionString}
          />
        );
      case CONDITION_FIELD_TYPE.SELECT:
        return (
          <ConditionSelectForm
            condition={condition}
            value={
              condition.get('values').findIndex(value => value === params) < 0
                ? '0'
                : condition.get('values').findIndex(value => value === params).toString()
            }
            onSubmit={this.handleChangeConditionSelect}
          />
        );
      case CONDITION_FIELD_TYPE.MULTIPLE_SELECT:
        return (
          <ConditionMultipleSelectForm
            condition={condition}
            value={
              condition.get('values').reduce(
                (result, item, key) =>
                  result.set(
                    `${CONDITION_MULTIPLE_SELECT_FORM_NAMES.VALUE}__${key}`,
                    !!params && !!params.find(param => param === item)
                  ),
                Map()
              )
            }
            onSubmit={this.handleChangeConditionMultipleSelect}
          />
        );
      case CONDITION_FIELD_TYPE.DATE_RANGE:
        return (
          <ConditionDatesRangeForm
            value={{
              [CONDITION_DATES_RANGE_FORM_NAMES.VALUE_FROM]:
                params && params.get('dateFrom') && moment(params.get('dateFrom')),
              [CONDITION_DATES_RANGE_FORM_NAMES.VALUE_TO]:
                params && params.get('dateTo') && moment(params.get('dateTo'))
            }}
            onSubmit={this.handleChangeConditionDatesRange}
          />
        );
      case CONDITION_FIELD_TYPE.AMOUNT_RANGE:
        return (
          <ConditionAmountsRangeForm
            value={{
              [CONDITION_AMOUNT_RANGE_FORM_NAMES.VALUE_FROM]: params && params.get('amountFrom'),
              [CONDITION_AMOUNT_RANGE_FORM_NAMES.VALUE_TO]: params && params.get('amountTo')
            }}
            onSubmit={this.handleChangeConditionAmountsRange}
          />
        );
      case CONDITION_FIELD_TYPE.SUGGEST:
        return (
          <ConditionSuggestForm
            condition={condition}
            value={params}
            renderValueItem={dictionaryFormFieldOptionRenderer}
            onInputChange={onDictionaryFormInputChange}
            onSubmit={this.handleChangeConditionSuggest}
          />
        );
      case CONDITION_FIELD_TYPE.SEARCH:
        return (
          <ConditionSearchForm
            condition={condition}
            value={params}
            renderValueItem={dictionaryFormFieldOptionRenderer}
            onInputChange={onDictionaryFormInputChange}
            onSubmit={this.handleChangeConditionSearch}
          />
        );
      default:
        return null;
    }
  };

  render() {
    const { dataLocs, condition, value, isChanging } = this.props;
    const { isPopupOpen } = this.state;

    const elementClassNmae = classnames(
      'b-spreadsheet__filters-condition',
      isPopupOpen && 's-selected',
      condition.get('type') === CONDITION_FIELD_TYPE.BOOLEAN && 'm-bool'
    );

    return (
      <div
        className={elementClassNmae}
        data-loc={dataLocs && dataLocs.main}
        ref={(element) => { this.element = element; }}
        onClick={this.handlePopupOpen}
      >
        <div className="b-spreadsheet__filters-condition-label">
          <strong>{condition.get('title')}{condition.get('type') !== CONDITION_FIELD_TYPE.BOOLEAN && ':'} </strong>
          <span>{value}</span>
        </div>
        {!isChanging &&
        <div className="b-spreadsheet__filters-condition-remove">
          <Icon
            type="close"
            size="12"
            title="Удалить условие"
            onClick={this.handleOnRemoveClick}
            locator={dataLocs && dataLocs.remove}
          />
        </div>
        }
        {isPopupOpen &&
        <PopupBox
          openerElement={this.element}
          isDontCloseByClick={condition.get('type') === CONDITION_FIELD_TYPE.DATE_RANGE}
          onClose={this.handlePopupClose}
          dataLoc={dataLocs && dataLocs.popup}
        >
          <div className="b-spreadsheet__filters-condition-popup">
            {this.renderForm()}
          </div>
        </PopupBox>
        }
      </div>
    );
  }
}

export default SpreadsheetFiltersCondition;
