export const CONDITION_FIELD_TYPE = {
  STRING: 'string',
  SELECT: 'select',
  MULTIPLE_SELECT: 'multipleSelect',
  DATE_RANGE: 'datesRange',
  AMOUNT_RANGE: 'amountsRange',
  SEARCH: 'search',
  SUGGEST: 'suggest',
  BOOLEAN: 'bool'
};
