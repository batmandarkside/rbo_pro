import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';

const SpreadsheetFiltersDropDownBoxOption = (props) => {
  const { dataLocs, value, title, isRemove, onRemoveClick } = props;

  const handleOnRemoveClick = (e) => {
    e.stopPropagation();
    onRemoveClick(value);
  };

  return (
    <div className="b-spreadsheet__filters-dropdown-box-option">
      <div className="b-spreadsheet__filters-dropdown-box-option-title">
        {title}
      </div>
      {isRemove &&
      <div className="b-spreadsheet__filters-dropdown-box-option-remove">
        <Icon
          locator={dataLocs && dataLocs.remove}
          type="remove"
          size="16"
          title="Удалить фильтр"
          onClick={handleOnRemoveClick}
        />
      </div>
      }
    </div>
  );
};

SpreadsheetFiltersDropDownBoxOption.propTypes = {
  dataLocs: PropTypes.object,
  value: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isRemove: PropTypes.bool,
  onRemoveClick: PropTypes.func.isRequired
};

export default SpreadsheetFiltersDropDownBoxOption;
