export const NAMES = {
  FORM: 'spreadsheetFiltersConditionAmountsRangeForm',
  VALUE_FROM: 'spreadsheetFiltersConditionAmountsRangeFormValueFrom',
  VALUE_TO: 'spreadsheetFiltersConditionAmountsRangeFormValueTo',
  BUTTON: 'spreadsheetFiltersConditionAmountsRangeFormButton'
};
