export const NAMES = {
  FORM: 'spreadsheetFiltersConditionDatesRangeForm',
  VALUE_FROM: 'spreadsheetFiltersConditionDatesRangeFormValueFrom',
  VALUE_TO: 'spreadsheetFiltersConditionDatesRangeFormValueTo',
  BUTTON: 'spreadsheetFiltersConditionDatesRangeFormButton'
};
