import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import RboForm, {
  RboFormFieldDate,
  RboFormFieldButton,
  RboFormRow,
  RboFormCell
} from 'components/ui-components/rbo-form-compact';
import { NAMES } from './constants';

export default class SpreadsheetFiltersConditionDatesRangeForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    value: PropTypes.object
  };

  static defaultProps = {
    value: {}
  };

  constructor(props) {
    super(props);

    this.state = {
      value: Map({
        [NAMES.VALUE_FROM]: props.value[NAMES.VALUE_FROM] && props.value[NAMES.VALUE_FROM].format(),
        [NAMES.VALUE_TO]: props.value[NAMES.VALUE_TO] && props.value[NAMES.VALUE_TO].format()
      })
    };
  }

  componentDidMount = () => {
    setTimeout(this.setInputFocus, 0);
  };

  onChange = ({ fieldId, fieldValue }) => {
    this.setState({
      value: this.state.value.merge({
        [fieldId]: fieldValue
      })
    });
  };

  setInputFocus = () => {
    document.querySelector(`#${NAMES.VALUE_FROM}`).focus();
  };

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { value } = this.state;

    onSubmit(value);
  };

  render() {
    const { value } = this.state;

    return (
      <RboForm
        dataLoc={NAMES.FORM}
        className="b-spreadsheet__filters-form"
        onSubmit={this.handleSubmit}
      >
        <RboFormRow>
          <RboFormCell size="auto">
            <RboFormFieldDate
              id={NAMES.VALUE_FROM}
              label="с"
              onChange={this.onChange}
              value={value.get(NAMES.VALUE_FROM)}
            />
          </RboFormCell>
          <RboFormCell size="auto">
            <RboFormFieldDate
              id={NAMES.VALUE_TO}
              label="по"
              onChange={this.onChange}
              value={value.get(NAMES.VALUE_TO)}
            />
          </RboFormCell>
          <RboFormCell size="auto">
            <RboFormFieldButton
              id={`${NAMES.FORM}_submit`}
              isPrimary
              pseudoLabel
              onClick={this.handleSubmit}
              title="OK"
              align="left"
            />
          </RboFormCell>
        </RboFormRow>
      </RboForm>
    );
  }
}

export { NAMES } from './constants';
