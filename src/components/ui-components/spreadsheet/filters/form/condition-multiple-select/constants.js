export const NAMES = {
  FORM: 'spreadsheetFiltersConditionMultipleSelectForm',
  VALUE: 'spreadsheetFiltersConditionMultipleSelectFormValue',
  BUTTON: 'spreadsheetFiltersConditionMultipleSelectFormButton'
};
