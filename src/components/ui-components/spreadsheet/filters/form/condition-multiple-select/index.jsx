import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import RboForm, {
  RboFormFieldCheckbox,
  RboFormFieldButton,
  RboFormRow,
  RboFormCell
} from 'components/ui-components/rbo-form-compact';
import { NAMES } from './constants';

export default class SpreadsheetFiltersConditionMultipleSelectForm extends Component {

  static propTypes = {
    condition: PropTypes.instanceOf(Map).isRequired,
    onSubmit: PropTypes.func.isRequired,
    value: PropTypes.instanceOf(Map)
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value
    };
  }

  onChange = ({ fieldId, fieldValue }) => {
    this.setState({
      value: this.state.value.merge({
        [fieldId]: fieldValue
      })
    });
  };

  handleSubmit = () => {
    const { onSubmit, condition } = this.props;
    const { value } = this.state;

    onSubmit({
      conditionId: condition.get('id'),
      conditionValue: value
    });
  };

  render() {
    const { condition } = this.props;
    const { value } = this.state;

    return (
      <RboForm
        dataLoc={NAMES.FORM}
        className="b-spreadsheet__filters-form"
        onSubmit={this.handleSubmit}
      >
        <div className="b-spreadsheet__filters-form-multiple-select">
          {condition.get('values').map((item, key) => (
            <RboFormRow key={key}>
              <RboFormCell size="auto">
                <RboFormFieldCheckbox
                  id={`${NAMES.VALUE}__${key}`}
                  title={item}
                  onChange={this.onChange}
                  value={value.get(`${NAMES.VALUE}__${key}`)}
                />
              </RboFormCell>
            </RboFormRow>
          ))}
        </div>
        <RboFormRow>
          <RboFormCell size="auto">
            <RboFormFieldButton
              id={`${NAMES.FORM}_submit`}
              isPrimary
              onClick={this.handleSubmit}
              title="OK"
              align="left"
            />
          </RboFormCell>
        </RboFormRow>
      </RboForm>
    );
  }
}

export { NAMES } from './constants';
