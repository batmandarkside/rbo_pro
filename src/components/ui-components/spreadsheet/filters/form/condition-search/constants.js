export const NAMES = {
  FORM: 'spreadsheetFiltersConditionSearchForm',
  VALUE: 'spreadsheetFiltersConditionSearchFormValue',
  BUTTON: 'spreadsheetFiltersConditionSearchFormButton'
};

export const INPUT_SIZE = '300px';
