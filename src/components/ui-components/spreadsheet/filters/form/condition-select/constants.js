export const NAMES = {
  FORM: 'spreadsheetFiltersConditionSelectForm',
  VALUE: 'spreadsheetFiltersConditionSelectFormValue',
  BUTTON: 'spreadsheetFiltersConditionSelectFormButton'
};
