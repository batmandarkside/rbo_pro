import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import RboForm, {
  RboFormFieldRadio,
  RboFormFieldButton,
  RboFormRow,
  RboFormCell
} from 'components/ui-components/rbo-form-compact';
import { NAMES } from './constants';

export default class SpreadsheetFiltersConditionSelectForm extends Component {

  static propTypes = {
    condition: PropTypes.instanceOf(Map).isRequired,
    onSubmit: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value
    };
  }

  onChange = ({ fieldValue }) => {
    this.setState({
      value: fieldValue
    });
  };

  handleSubmit = () => {
    const { onSubmit, condition } = this.props;
    const { value } = this.state;

    onSubmit({
      conditionId: condition.get('id'),
      conditionValue: value
    });
  };

  render() {
    const { condition } = this.props;
    const { value } = this.state;

    return (
      <RboForm
        dataLoc={NAMES.FORM}
        className="b-spreadsheet__filters-form"
        onSubmit={this.handleSubmit}
      >
        <div className="b-spreadsheet__filters-form-select">
          {condition.get('values').map((item, key) => (
            <RboFormRow key={key}>
              <RboFormCell size="auto">
                <RboFormFieldRadio
                  id={`${NAMES.VALUE}_${key}`}
                  group={NAMES.VALUE}
                  title={item}
                  value={key.toString()}
                  isSelected={value === key.toString()}
                  onChange={this.onChange}
                />
              </RboFormCell>
            </RboFormRow>
          ))}
        </div>
        <RboFormRow>
          <RboFormCell size="auto">
            <RboFormFieldButton
              id={`${NAMES.FORM}_submit`}
              isPrimary
              onClick={this.handleSubmit}
              title="OK"
              align="left"
            />
          </RboFormCell>
        </RboFormRow>
      </RboForm>
    );
  }
}

export { NAMES } from './constants';
