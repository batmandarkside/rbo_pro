export const NAMES = {
  FORM: 'spreadsheetFiltersConditionStringForm',
  VALUE: 'spreadsheetFiltersConditionStringFormValue',
  BUTTON: 'spreadsheetFiltersConditionStringFormButton'
};

export const SIZE = '300px';
