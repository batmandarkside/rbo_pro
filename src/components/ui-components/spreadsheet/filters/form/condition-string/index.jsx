import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import RboForm, {
    RboFormFieldText,
    RboFormFieldAccount,
    RboFormFieldButton,
    RboFormRow,
    RboFormCell
  } from 'components/ui-components/rbo-form-compact';
import { NAMES, SIZE } from './constants';

export default class SpreadsheetFiltersConditionStringForm extends Component {
  static propTypes = {
    condition: PropTypes.instanceOf(Map).isRequired,
    onSubmit: PropTypes.func.isRequired,
    value: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value
    };
  }

  componentDidMount = () => {
    setTimeout(this.setInputFocus, 7);
  };

  onChange = ({ fieldValue }) => {
    this.setState({
      value: fieldValue
    });
  };

  setInputFocus = () => {
    document.querySelector(`#${this.props.condition.get('id')}`).focus();
  };

  getInputComponent = (props) => {
    const { condition } = this.props;
    switch (condition.get('inputType')) {
      case 'Account':
        return <RboFormFieldAccount {...props} />;
      default:
        return <RboFormFieldText {...props} />;
    }
  };

  handleSubmit = () => {
    const { onSubmit, condition } = this.props;
    const { value } = this.state;

    onSubmit({
      conditionId: condition.get('id'),
      conditionValue: value
    });
  };

  render() {
    const { condition } = this.props;
    const { value } = this.state;

    return (
      <RboForm
        dataLoc={NAMES.FORM}
        className="b-spreadsheet__filters-form"
        onSubmit={this.handleSubmit}
      >
        <RboFormRow>
          <RboFormCell size="auto">
            {this.getInputComponent({
              id: condition.get('id'),
              maxLength: condition.get('maxLength'),
              placeholder: condition.get('title'),
              onChange: this.onChange,
              size: condition.get('size', SIZE),
              value
            })}
          </RboFormCell>
          <RboFormCell size="auto">
            <RboFormFieldButton
              id={`${NAMES.FORM}_submit`}
              isPrimary
              onClick={this.handleSubmit}
              title="OK"
              align="left"
            />
          </RboFormCell>
        </RboFormRow>
      </RboForm>
    );
  }
}

export { NAMES } from './constants';
