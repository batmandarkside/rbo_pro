export const NAMES = {
  FORM: 'spreadsheetFiltersConditionSuggestForm',
  VALUE: 'spreadsheetFiltersConditionSuggestFormValue',
  BUTTON: 'spreadsheetFiltersConditionSuggestFormButton'
};

export const INPUT_SIZE = '300px';
