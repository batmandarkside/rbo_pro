import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import RboForm, {
  RboFormFieldSuggest,
  RboFormFieldButton,
  RboFormRow,
  RboFormCell
} from 'components/ui-components/rbo-form-compact';
import { NAMES, INPUT_SIZE } from './constants';

class SpreadsheetFiltersConditionSuggestForm extends Component {
  static propTypes = {
    condition: ImmutablePropTypes.map.isRequired,
    value: PropTypes.string,
    renderValueItem: PropTypes.func,
    onInputChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
  };

  state = {
    value: this.props.value
  };

  onSearch = ({ fieldSearchValue }) => {
    this.props.onInputChange(this.props.condition.get('id'), fieldSearchValue);
  };

  onChange = ({ fieldValue }) => this.setState({ value: fieldValue });

  onSubmit = () => this.props.onSubmit({ value: this.state.value });

  renderValueItem = (id, option, highlight) => {
    const { condition } = this.props;
    return this.props.renderValueItem({ conditionId: condition.get('id'), option, highlight });
  };

  render() {
    const { condition } = this.props;
    const { value } = this.state;

    return (
      <RboForm
        initialFocusFieldId={NAMES.VALUE}
        onSubmit={this.onSubmit}
      >
        <RboFormRow>
          <RboFormCell size="auto">
            <RboFormFieldSuggest
              id={NAMES.VALUE}
              inputType={condition.get('inputType')}
              value={value}
              options={condition.getIn(['values', 'items']).toJS()}
              size={condition.get('inputSize') || INPUT_SIZE}
              maxLength={condition.get('maxLength')}
              optionRenderer={this.renderValueItem}
              isSearching={condition.getIn(['values', 'isSearching'])}
              onSearch={this.onSearch}
              onChange={this.onChange}
            />

          </RboFormCell>
          <RboFormCell size="auto">
            <RboFormFieldButton
              id={NAMES.BUTTON}
              isPrimary
              onClick={this.onSubmit}
              title="OK"
              align="left"
            />
          </RboFormCell>
        </RboFormRow>
      </RboForm>
    );
  }
}

export default SpreadsheetFiltersConditionSuggestForm;

export { NAMES } from './constants';
