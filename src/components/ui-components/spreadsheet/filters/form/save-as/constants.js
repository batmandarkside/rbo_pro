export const NAMES = {
  FORM: 'spreadsheetFiltersSaveAsForm',
  TITLE: 'spreadsheetFiltersSaveAsFormTitle',
  BUTTON: 'spreadsheetFiltersSaveAsFormButton'
};
