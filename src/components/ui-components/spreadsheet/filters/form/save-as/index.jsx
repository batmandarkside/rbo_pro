import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import RboForm, {
  RboFormFieldText,
  RboFormFieldButton,
  RboFormRow,
  RboFormCell
} from 'components/ui-components/rbo-form-compact';
import { NAMES } from './constants';

export default class SpreadsheetFiltersSaveAsForm extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isValid: false,
      value: ''
    };
  }

  componentDidMount = () => {
    setTimeout(this.setInputFocus, 7);
  };

  onFilterTitleChange = ({ fieldValue }) => {
    const isValid = !!fieldValue;

    this.setState({
      isValid,
      value: fieldValue
    });
  };

  setInputFocus = () => {
    document.querySelector(`#${NAMES.TITLE}`).focus();
  };

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { value } = this.state;

    onSubmit(value);
  };

  render() {
    const { isValid } = this.state;

    return (
      <RboForm
        dataLoc={NAMES.FORM}
        className="b-spreadsheet__filters-form"
        onSubmit={this.handleSubmit}
      >
        <RboFormRow>
          <RboFormCell size="auto">
            <RboFormFieldText
              id={NAMES.TITLE}
              placeholder="Название фильтра"
              onChange={this.onFilterTitleChange}
              maxLength={200}
            />
          </RboFormCell>
          <RboFormCell size="auto">
            <RboFormFieldButton
              id={`${NAMES.BUTTON}`}
              isPrimary
              onClick={this.handleSubmit}
              isDisabled={!isValid}
              title="Сохранить"
              align="center"
            />
          </RboFormCell>
        </RboFormRow>
      </RboForm>
    );
  }
}

export { NAMES } from './constants';
