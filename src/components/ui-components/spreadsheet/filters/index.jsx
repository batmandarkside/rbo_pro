import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { List, Map, fromJS } from 'immutable';
import { RboFormFieldDropdown } from 'components/ui-components/rbo-form-compact';
import Icon from 'components/ui-components/icon';
import PopupBox from 'components/ui-components/popup-box';
import Condition from './condition';
import DropdownBoxOption from './dropdown-box-option';
import AddConditionItem from './add-condition-item';
import SaveAsForm from './form/save-as';
import './style.css';

class SpreadsheetFilters extends PureComponent {
  static propTypes = {
    dataLocs: PropTypes.object,
    conditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    dictionaryFormFieldOptionRenderer: PropTypes.func,
    onConditionDictionaryFormInputChange: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      isSaveAsPopupOpen: false,
      isAddConditionPopupOpen: false
    };
  }

  checkFilters = filters => filters
    .filter(
      filter =>
        (filter.get('isNew') && filter.get('conditions') && filter.get('conditions').size) || !filter.get('isNew')
    )
    .map((filter) => {
      const conditionsSize = filter.get('conditions') && filter.get('conditions').size;
      if (!conditionsSize) {
        return filter.merge({
          selected: false,
          conditions: null,
          isChanged: false
        });
      }
      return filter;
    }
  );

  handleSelectFilter = ({ fieldValue }) => {
    const filters = this.props.filters.map(filter => filter.set('selected', filter.get('id') === fieldValue));

    this.props.changeFiltersAction(filters, true, false);
  };

  handleDeselectFilter = () => {
    const filters = this.props.filters.map(filter => filter.merge({
      selected: false,
      conditions: null,
      isChanged: false
    })).filter(filter => !filter.get('isNew'));

    this.props.changeFiltersAction(filters, true, false);
  };

  handleRemoveFilter = (filterId) => {
    const filters = this.props.filters.filter(f => f.get('id') !== filterId);

    this.props.changeFiltersAction(filters, false, true);
  };

  handleCreateNewFilter = () => {
    const filters = this.props.filters.push(fromJS({
      title: 'Новый',
      savedConditions: [],
      isPreset: true,
      isNew: true,
      selected: true
    }));

    this.props.changeFiltersAction(filters, false, false);

    setTimeout(this.handleAddConditionPopupOpen, 100);
  };

  handleSaveFilter = () => {
    const filters = this.props.filters.map(filter => (
        filter.get('selected') ?
          filter.merge({
            savedConditions: filter.get('conditions'),
            isChanged: false
          }) : filter
      )
    );

    this.props.changeFiltersAction(filters, true, true);
  };

  handleSaveAsFilter = (value) => {
    const title = value;
    const selectedFilter = this.props.filters.find(filter => filter.get('selected'));

    const filters = this.props.filters.map(filter => filter.merge({
      selected: false,
      conditions: null,
      isChanged: false
    })).push(selectedFilter.merge({
      id: `spreadsheetFilter_${Date.now()}`,
      title,
      selected: true,
      savedConditions: selectedFilter.get('conditions'),
      isChanged: false,
      isNew: false,
      isPreset: false
    }));

    this.setState({
      isSaveAsPopupOpen: false
    });

    this.props.changeFiltersAction(filters, true, true);
  };

  handleSaveAsFilterPopupOpen = () => {
    this.setState({
      isSaveAsPopupOpen: true
    });
  };

  handleSaveAsFilterPopupClose = () => {
    this.setState({
      isSaveAsPopupOpen: false
    });
  };

  handleConditionRemove = (filterConditionId) => {
    const filters = this.props.filters.map(filter => (
      filter.get('selected') ?
        filter.merge({
          conditions: filter.get('conditions')
            .splice(filter.get('conditions').findIndex(item => item.get('id') === filterConditionId), 1),
          isChanged: true
        })
        :
        filter
      )
    );

    this.props.changeFiltersAction(this.checkFilters(filters), true, false);
  };

  handleConditionChange = (filterConditionId, params) => {
    if (!params) this.handleConditionRemove(filterConditionId);
    else {
      const selectedFilter = this.props.filters.find(filter => filter.get('selected'));
      const selectedFilterConditions = selectedFilter.get('conditions');

      const newSelectedFilterConditions = selectedFilterConditions.map(condition => (
        condition.get('id') === filterConditionId ?
          condition.merge({
            params,
            isChanging: false
          })
          :
          condition
      ));

      const filters = this.props.filters.map(filter => (
          filter.get('selected') ?
            filter.merge({
              conditions: newSelectedFilterConditions,
              isChanged: true
            })
            :
            filter
        )
      );

      this.props.changeFiltersAction(filters, true, false);
    }
  };

  handleConditionPopupClose = (filterConditionId) => {
    const selectedFilter = this.props.filters.find(filter => filter.get('selected'));
    const selectedFilterConditions = selectedFilter.get('conditions');
    const selectedFilterCondition = selectedFilterConditions.find(
      condition => condition.get('id') === filterConditionId
    );
    const newSelectedFilterConditions = selectedFilterCondition.get('params') ?
      selectedFilterConditions
      :
      selectedFilterConditions.splice(
        selectedFilterConditions.findIndex(conditions => conditions.get('id') === filterConditionId), 1
      );

    const filters = this.props.filters.map(filter => (
        filter.get('selected') ?
          filter.merge({
            conditions: newSelectedFilterConditions
          })
          :
          filter
      )
    );

    this.props.changeFiltersAction(this.checkFilters(filters), false, false);
  };

  handleAddCondition = (conditionId) => {
    const conditionType = this.props.conditions.find(condition => condition.get('id') === conditionId).get('type');

    const filters = this.props.filters.map(filter => (
        filter.get('selected') ?
          filter.merge({
            conditions: conditionType === 'bool' ?
              filter.get('conditions').push(Map({
                id: conditionId,
                params: true
              }))
              :
              filter.get('conditions').push(Map({
                id: conditionId,
                isChanging: true
              })),
            isChanged: conditionType === 'bool' ? true : filter.get('isChanged')
          })
          :
          filter
      )
    );

    this.setState({
      isAddConditionPopupOpen: false
    });

    this.props.changeFiltersAction(filters, conditionType === 'bool', false);
  };

  handleAddConditionPopupOpen = () => {
    this.setState({
      isAddConditionPopupOpen: true
    });
  };

  handleAddConditionPopupClose = () => {
    this.setState({
      isAddConditionPopupOpen: false
    });

    this.props.changeFiltersAction(this.checkFilters(this.props.filters), false, false);
  };

  renderDropdownBoxOption = (id, filter) => {
    const { dataLocs } = this.props;

    return (
      <DropdownBoxOption
        value={filter.value}
        title={filter.title}
        isRemove={filter.isRemove}
        dataLocs={dataLocs && { remove: dataLocs.remove }}
        onRemoveClick={this.handleRemoveFilter}
      />
    );
  };

  render() {
    const {
      dataLocs,
      filters,
      conditions,
      dictionaryFormFieldOptionRenderer,
      onConditionDictionaryFormInputChange,
    } = this.props;
    const { isSaveAsPopupOpen, isAddConditionPopupOpen } = this.state;

    const selectedFilter = filters.find(filter => filter.get('selected'));

    const freeConditions = selectedFilter ?
      conditions.filter(
        condition => !selectedFilter.get('conditions').find(
          selectedFilterCondition => condition.get('id') === selectedFilterCondition.get('id')
        )
      )
      :
      null;

    const isConditionChanging = selectedFilter &&
      selectedFilter.get('conditions').find(condition => condition.get('isChanging'));

    const dropdownBoxOptions = filters.map(filter => ({
      value: filter.get('id'),
      title: filter.get('title'),
      selected: filter.get('selected'),
      isRemove: !filter.get('isPreset')
    })).toJS();

    return (
      <div className="b-spreadsheet__filters" data-loc={dataLocs && dataLocs.main}>
        <div className="b-spreadsheet__filters-inner">
          <div className="b-spreadsheet__filters-header">
            <div className="b-spreadsheet__filters-title">
              <h4>
                <span>Фильтр</span>
                {selectedFilter && !selectedFilter.get('isNew') &&
                <span> &laquo;{selectedFilter.get('title')}&raquo;</span>
                }
              </h4>
            </div>
            {selectedFilter &&
            <div className="b-spreadsheet__filters-operations" data-loc={dataLocs && dataLocs.operations}>
              {selectedFilter.get('isChanged') &&
              <div className="b-spreadsheet__filters-operations-item">
                ({selectedFilter.get('isNew') ? 'не сохранен' : 'изменен'})
              </div>
              }
              {selectedFilter.get('isChanged') && !selectedFilter.get('isPreset') &&
              <div className="b-spreadsheet__filters-operations-item">
                <span
                  className="m-dashed-link"
                  data-loc={dataLocs && dataLocs.operationSave}
                  onClick={this.handleSaveFilter}
                >
                  сохранить
                </span>
              </div>
              }
              {selectedFilter.get('isChanged') &&
              <div
                className="b-spreadsheet__filters-operations-item"
                ref={(element) => { this.saveAsFilterOpenerElement = element; }}
              >
                <span
                  className="m-dashed-link"
                  data-loc={dataLocs && dataLocs.operationSaveAs}
                  onClick={this.handleSaveAsFilterPopupOpen}
                >
                  сохранить как...
                </span>
                {isSaveAsPopupOpen &&
                <PopupBox
                  openerElement={this.saveAsFilterOpenerElement}
                  onClose={this.handleSaveAsFilterPopupClose}
                  dataLoc={dataLocs && dataLocs.operationSaveAsPopup}
                >
                  <div className="b-spreadsheet__filters-save-as-popup">
                    <SaveAsForm
                      onSubmit={this.handleSaveAsFilter}
                    />
                  </div>
                </PopupBox>
                }
              </div>
              }
              {!!selectedFilter.get('conditions').size &&
              <div className="b-spreadsheet__filters-operations-item">
                <span
                  className="m-dashed-link"
                  data-loc={dataLocs && dataLocs.operationClear}
                  onClick={this.handleDeselectFilter}
                >
                  сбросить фильтр
                </span>
              </div>
              }
            </div>
            }
          </div>
          <div className="b-spreadsheet__filters-content">
            {!!filters.size && !selectedFilter &&
            <div className="b-spreadsheet__filters-dropdown-box">
              <RboFormFieldDropdown
                id="filters-dropdown-box"
                testLocator={dataLocs && dataLocs.select}
                placeholder="выберите фильтр"
                options={dropdownBoxOptions}
                optionRenderer={this.renderDropdownBoxOption}
                onChange={this.handleSelectFilter}
              />
            </div>
            }
            {selectedFilter &&
            <div className="b-spreadsheet__filters-conditions" data-loc={dataLocs && dataLocs.conditions}>
              {selectedFilter.get('conditions').map((condition, key) => (
                <Condition
                  key={key}
                  dataLocs={dataLocs && {
                    main: dataLocs.condition,
                    popup: dataLocs.conditionPopup,
                    remove: dataLocs.conditionRemove
                  }}
                  id={condition.get('id')}
                  condition={conditions.find(item => item.get('id') === condition.get('id'))}
                  params={condition.get('params')}
                  value={condition.get('value')}
                  isChanging={condition.get('isChanging')}
                  onChange={this.handleConditionChange}
                  onPopupClose={this.handleConditionPopupClose}
                  onRemoveClick={this.handleConditionRemove}
                  dictionaryFormFieldOptionRenderer={dictionaryFormFieldOptionRenderer}
                  onDictionaryFormInputChange={onConditionDictionaryFormInputChange}
                />
              ))}
              {!isConditionChanging && !!freeConditions.size &&
              <div
                className="b-spreadsheet__filters-condition m-add"
                ref={(element) => {
                  this.addConditionOpenerElement = element;
                }}
              >
                <span
                  className="m-dashed-link"
                  data-loc={dataLocs && dataLocs.addCondition}
                  onClick={this.handleAddConditionPopupOpen}
                >
                  добавить условие
                </span>
                {isAddConditionPopupOpen &&
                <PopupBox
                  openerElement={this.addConditionOpenerElement}
                  onClose={this.handleAddConditionPopupClose}
                  dataLoc={dataLocs && dataLocs.addConditionPopup}
                >
                  <div className="b-spreadsheet__filters-add-condition-popup">
                    <div className="b-spreadsheet__filters-add-condition-items">
                      {freeConditions.map((condition, key) => (
                        <AddConditionItem
                          key={key}
                          id={condition.get('id')}
                          title={condition.get('title')}
                          onClick={this.handleAddCondition}
                        />
                      ))}
                    </div>
                  </div>
                </PopupBox>
                }
              </div>
              }
            </div>
            }
            {!!filters.size && !selectedFilter &&
            <div className="b-spreadsheet__filters-create-new">
              <span>или </span>
              <span
                className="m-dashed-link"
                data-loc={dataLocs && dataLocs.add}
                onClick={this.handleCreateNewFilter}
              >
                создайте новый
              </span>
            </div>
            }
            {!filters.size &&
            <div className="b-spreadsheet__filters-create-new">
              <span
                className="m-dashed-link"
                data-loc={dataLocs && dataLocs.add}
                onClick={this.handleCreateNewFilter}
              >
                добавить условие
              </span>
            </div>
            }
          </div>
          {selectedFilter &&
          <div className="b-spreadsheet__filters-close">
            <Icon
              type="close"
              size="16"
              title="Сбросить фильтр"
              onClick={this.handleDeselectFilter}
              locator={dataLocs && dataLocs.close}
            />
          </div>
          }
        </div>
      </div>
    );
  }
}

export default SpreadsheetFilters;
