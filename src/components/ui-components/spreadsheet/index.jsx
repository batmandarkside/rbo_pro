import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const Spreadsheet = (props) => {
  const { children, dataLoc } = props;

  return (
    <div className="b-spreadsheet" data-loc={dataLoc}>
      {React.Children.map(children, element => (
        <div className={`b-spreadsheet__${element.type}`}>
          {element.props.children}
        </div>
      ))}
    </div>
  );
};

Spreadsheet.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  dataLoc: PropTypes.string
};

export default Spreadsheet;

export Toolbar from './toolbar';
export Operations from './operations';
export Filters from './filters';
export Table from './table';
export Selection from './selection';
export Export from './export';
export Pagination from './pagination';

export { CONDITION_FIELD_TYPE } from './filters/constants';
