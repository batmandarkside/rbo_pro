import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import OperationsPannel from 'components/ui-components/operations-panel';
import './style.css';

const SpreadsheetOperations = (props) => {
  const { dataLoc, operations, noAllowedOperationMessage, operationAction, selectedItems } = props;

  const handleOnOperationClick = (operationId) => {
    operationAction(operationId, selectedItems.toJS());
  };

  return (
    <div className="b-spreadsheet__operations" data-loc={dataLoc}>
      {!!operations.size &&
      <OperationsPannel
        operations={operations}
        onOperationClick={handleOnOperationClick}
      />
      }
      {!operations.size &&
      <div className="b-spreadsheet__operations-empty">
        {noAllowedOperationMessage}
      </div>
      }
    </div>
  );
};

SpreadsheetOperations.propTypes = {
  dataLoc: PropTypes.string,
  operations: PropTypes.instanceOf(List).isRequired,
  noAllowedOperationMessage: PropTypes.string,
  operationAction: PropTypes.func.isRequired,
  selectedItems: PropTypes.instanceOf(List).isRequired
};

export default SpreadsheetOperations;
