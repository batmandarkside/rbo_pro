import React from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Item from './item';
import './style.css';

const SpreadsheetPagination = (props) => {
  const { dataLoc, pages, onClick } = props;
  const selectedIndex = pages.get('selected');
  const visibleQty = pages.get('visible') - 1;
  const lastIndex = pages.get('size') - 1;
  let endIndex = lastIndex - selectedIndex < (visibleQty / 2) ? lastIndex : selectedIndex + (visibleQty / 2);
  let startIndex = endIndex - visibleQty;
  if (startIndex < 0) {
    startIndex = 0;
    endIndex = visibleQty;
  }

  return (
    <div className="b-spreadsheet__pagination" data-loc={dataLoc}>
      <div className="b-spreadsheet__pagination-inner">
        <div className="b-spreadsheet__pagination-items">
          {selectedIndex !== 0 &&
          <Item
            index={0}
            icon="arrow-left-double"
            label="в&nbsp;начало"
            onClick={onClick}
          />
          }
          {selectedIndex !== 0 &&
          <Item
            index={selectedIndex - 1}
            icon="arrow-left"
            title="Предыдущая страница"
            onClick={onClick}
          />
          }
          {List().setSize(pages.get('size')).map((page, key) => (
            key >= startIndex && key <= endIndex &&
            <Item
              index={key}
              label={key + 1}
              selected={key === selectedIndex}
              onClick={onClick}
              key={key}
            />
          ))}
          {selectedIndex !== pages.get('size') - 1 &&
          <Item
            index={selectedIndex + 1}
            icon="arrow-right"
            title="Следующая страница"
            onClick={onClick}
          />
          }
        </div>
      </div>
    </div>
  );
};

SpreadsheetPagination.propTypes = {
  dataLoc: PropTypes.string,
  pages: PropTypes.instanceOf(Map).isRequired,
  onClick: PropTypes.func.isRequired
};

export default SpreadsheetPagination;
