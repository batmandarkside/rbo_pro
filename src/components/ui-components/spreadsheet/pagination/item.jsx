import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';

const SpreadsheetPaginationItem = (props) => {
  const { index, icon, label, title, selected, onClick } = props;

  const handleOnClick = (e) => {
    e.stopPropagation();
    if (!selected) onClick(index);
  };

  const elementClassName = classnames('b-spreadsheet__pagination-item', selected && 's-selected');

  return (
    <div className={elementClassName} title={title}>
      <span onClick={handleOnClick}>
        {icon && <Icon type={icon} size="16" />}
        {label}
      </span>
    </div>
  );
};

SpreadsheetPaginationItem.propTypes = {
  index: PropTypes.number.isRequired,
  icon: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  title: PropTypes.string,
  selected: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};

export default SpreadsheetPaginationItem;
