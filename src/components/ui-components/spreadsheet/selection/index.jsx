import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const SpreadsheetSelection = (props) => {
  const { dataLoc, main, extra } = props;

  return (
    <div className="b-spreadsheet__selection" data-loc={dataLoc}>
      <div className="b-spreadsheet__selection-inner">
        <div className="b-spreadsheet__selection-main">
          <div className="b-spreadsheet__selection-label">
            {main.label}
          </div>
          <div className="b-spreadsheet__selection-value">
            {main.value}
          </div>
        </div>
        {extra &&
        <div className="b-spreadsheet__selection-extra">
          <div className="b-spreadsheet__selection-label">
            {extra.label}
          </div>
          <div className="b-spreadsheet__selection-value">
            {extra.value}
          </div>
        </div>
        }
      </div>
    </div>
  );
};

SpreadsheetSelection.propTypes = {
  dataLoc: PropTypes.string,
  main: PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
  }).isRequired,
  extra: PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
  })
};

export default SpreadsheetSelection;
