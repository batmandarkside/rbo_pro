import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import Group from './../group';
import Item from './../item';

const SpreadsheetTableBody = (props) => {
  const { dataLoc, columns, list, renderCells, handleOnItemClick,
    select, selectGroupTitle, selectItemTitle,
    handleToggleSelectItem, handleToggleSelectGroup,
    operations, operationAction, offsetWidth, contentScrollLeft } = props;
  const grouped = !!columns.filter(column => column.get('grouped')).size;

  return (
    <div className="b-spreadsheet__table-body" data-loc={dataLoc}>
      <div className="b-spreadsheet__table-body-inner">
        {grouped ?
          list.map((group, key) => (
            <Group
              key={key}
              columns={columns}
              group={group}
              renderCells={renderCells}
              handleOnItemClick={handleOnItemClick}
              select={select}
              selectGroupTitle={selectGroupTitle}
              selectItemTitle={selectItemTitle}
              handleToggleSelectItem={handleToggleSelectItem}
              handleToggleSelectGroup={handleToggleSelectGroup}
              operations={operations}
              operationAction={operationAction}
              offsetWidth={offsetWidth}
              contentScrollLeft={contentScrollLeft}
            />
          ))
          :
          list.map(group => (
            group.get('items').map((item, key) => (
              <Item
                key={key}
                columns={columns}
                item={item}
                renderCells={renderCells}
                handleOnItemClick={handleOnItemClick}
                select={select}
                selectItemTitle={selectItemTitle}
                handleToggleSelectItem={handleToggleSelectItem}
                operations={operations}
                operationAction={operationAction}
                offsetWidth={offsetWidth}
                contentScrollLeft={contentScrollLeft}
              />
            ))
          ))
        }
      </div>
    </div>
  );
};

SpreadsheetTableBody.propTypes = {
  dataLoc: PropTypes.string,
  columns: PropTypes.instanceOf(List).isRequired,
  list: PropTypes.instanceOf(List).isRequired,
  renderCells: PropTypes.objectOf(PropTypes.func),
  handleOnItemClick: PropTypes.func,
  select: PropTypes.bool.isRequired,
  selectGroupTitle: PropTypes.string,
  selectItemTitle: PropTypes.string,
  handleToggleSelectItem: PropTypes.func,
  handleToggleSelectGroup: PropTypes.func,
  operations: PropTypes.instanceOf(List),
  operationAction: PropTypes.func,
  offsetWidth: PropTypes.number.isRequired,
  contentScrollLeft: PropTypes.number.isRequired
};

export default SpreadsheetTableBody;
