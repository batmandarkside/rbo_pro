import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { LOCATORS } from '../../constants';

class SpreadsheetTableHeadCell extends Component {
  static propTypes = {
    columnId: PropTypes.string.isRequired,
    itemId: PropTypes.string,
    renderInner: PropTypes.func,
    width: PropTypes.number.isRequired,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element, PropTypes.object,
      PropTypes.string, PropTypes.number, PropTypes.bool]),
    resize: PropTypes.bool,
    handleResize: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      width: this.props.width,
      isTaken: false,
      clientX: null,
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleMouseDown);
    document.addEventListener('mousemove', this.handleMouseMove);
    document.addEventListener('mouseup', this.handleMouseUp);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleMouseDown);
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseDown(e) {
    if (e.target === this.resizeElement) {
      e.preventDefault();
      this.setState({
        width: this.props.width,
        isTaken: true,
        clientX: e.clientX
      });
    }
  }

  handleMouseMove(e) {
    if (this.state.isTaken) {
      e.preventDefault();
      const width = this.state.width - (this.state.clientX - e.clientX);
      const { columnId, handleResize } = this.props;
      handleResize(columnId, width);
    }
  }

  handleMouseUp(e) {
    if (this.state.isTaken) {
      e.preventDefault();
      const width = this.state.width - (this.state.clientX - e.clientX);
      this.setState({
        isTaken: false
      });
      const { columnId, handleResize } = this.props;
      handleResize(columnId, width, true);
    }
  }

  render() {
    const { columnId, itemId, renderInner, children, resize, width } = this.props;
    const elementClassName = classnames('b-spreadsheet__table-cell', `m-${columnId}`);
    const style = { width };

    return (
      <div className={elementClassName} style={style} data-loc={LOCATORS.TABLE_HEAD_CELL}>
        <div className="b-spreadsheet__table-cell-inner">
          <div className="b-spreadsheet__table-cell-value">
            {renderInner ? renderInner(children, itemId) : children}
          </div>
        </div>
        {resize &&
        <div className="b-spreadsheet__table-cell-resize" ref={(element) => { this.resizeElement = element; }} />
        }
      </div>
    );
  }
}

export default SpreadsheetTableHeadCell;
