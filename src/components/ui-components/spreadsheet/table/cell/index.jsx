import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { LOCATORS } from '../../constants';

const SpreadsheetTableCell = (props) => {
  const { itemId, renderInner, modeClassName, width, children } = props;
  const elementClassName = classnames('b-spreadsheet__table-cell', modeClassName && `m-${modeClassName}`);
  const style = { width };

  return (
    <div className={elementClassName} style={style} data-loc={LOCATORS.TABLE_CELL}>
      <div className="b-spreadsheet__table-cell-inner">
        <div className="b-spreadsheet__table-cell-value">
          {renderInner ? renderInner(children, itemId) : children}
        </div>
      </div>
    </div>
  );
};

SpreadsheetTableCell.propTypes = {
  itemId: PropTypes.string,
  renderInner: PropTypes.func,
  modeClassName: PropTypes.string,
  width: PropTypes.number.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element, PropTypes.object,
    PropTypes.string, PropTypes.number, PropTypes.bool])
};

export default SpreadsheetTableCell;
