import React from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import { RboFormFieldCheckbox } from 'components/ui-components/rbo-form-compact';
import { LOCATORS } from '../../constants';
import Item from '../item';

const SpreadsheetTableGroup = (props) => {
  const { columns, group, renderCells, handleOnItemClick,
    select, selectGroupTitle, selectItemTitle,
    handleToggleSelectItem, handleToggleSelectGroup,
    operations, operationAction, offsetWidth, contentScrollLeft } = props;

  const handleToggleCheckbox = () => select && handleToggleSelectGroup(group.get('id'));

  return (
    <div className="b-spreadsheet__table-group" data-loc={LOCATORS.TABLE_GROUP}>
      <div className="b-spreadsheet__table-group-header">
        {select &&
        <div className="b-spreadsheet__table-group-checkbox" title={selectGroupTitle}>
          <RboFormFieldCheckbox
            id={group.get('id')}
            onChange={handleToggleCheckbox}
            value={group.get('selected')}
          />
        </div>
        }
        {group.get('title') &&
        <div className="b-spreadsheet__table-group-title">
          {group.get('title')}
        </div>
        }
      </div>
      <div className="b-spreadsheet__table-group-content">
        {group.get('items').map((item, key) => (
          <Item
            key={key}
            columns={columns}
            item={item}
            renderCells={renderCells}
            handleOnItemClick={handleOnItemClick}
            select={select}
            selectItemTitle={selectItemTitle}
            handleToggleSelectItem={handleToggleSelectItem}
            operations={operations}
            operationAction={operationAction}
            offsetWidth={offsetWidth}
            contentScrollLeft={contentScrollLeft}
          />
        ))}
      </div>
    </div>
  );
};

SpreadsheetTableGroup.propTypes = {
  columns: PropTypes.instanceOf(List).isRequired,
  group: PropTypes.instanceOf(Map).isRequired,
  renderCells: PropTypes.objectOf(PropTypes.func),
  handleOnItemClick: PropTypes.func,
  select: PropTypes.bool.isRequired,
  selectGroupTitle: PropTypes.string,
  selectItemTitle: PropTypes.string,
  handleToggleSelectItem: PropTypes.func,
  handleToggleSelectGroup: PropTypes.func,
  operations: PropTypes.instanceOf(List),
  operationAction: PropTypes.func,
  offsetWidth: PropTypes.number.isRequired,
  contentScrollLeft: PropTypes.number.isRequired
};

export default SpreadsheetTableGroup;
