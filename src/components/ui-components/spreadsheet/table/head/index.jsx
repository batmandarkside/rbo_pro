import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { List } from 'immutable';
import { RboFormFieldCheckbox } from 'components/ui-components/rbo-form-compact';
import Row from '../row/head-row';
import Sorting from './sorting';

class SpreadsheetTableHead extends Component {
  static propTypes = {
    dataLoc: PropTypes.string,
    columns: PropTypes.instanceOf(List).isRequired,
    renderCells: PropTypes.objectOf(PropTypes.func),
    offsetTop: PropTypes.number.isRequired,
    offsetLeft: PropTypes.number.isRequired,
    contentScrollTop: PropTypes.number.isRequired,
    contentScrollLeft: PropTypes.number.isRequired,
    contentWidth: PropTypes.number.isRequired,
    sorting: PropTypes.bool.isRequired,
    handleToggleSorting: PropTypes.func.isRequired,
    select: PropTypes.bool.isRequired,
    selectAllTitle: PropTypes.string,
    selectedAllItems: PropTypes.bool,
    handleToggleSelectAll: PropTypes.func,
    resize: PropTypes.bool.isRequired,
    handleResizeColumn: PropTypes.func
  };

  getBoundingClientRect = () => this.element.getBoundingClientRect();

  render() {
    const {
      dataLoc,
      columns, renderCells,
      offsetTop, offsetLeft, contentScrollTop, contentScrollLeft, contentWidth,
      sorting, handleToggleSorting,
      select, selectAllTitle, selectedAllItems, handleToggleSelectAll,
      resize, handleResizeColumn
    } = this.props;
    const elementClassName = classnames('b-spreadsheet__table-head', contentScrollTop && 's-scrolled');
    const style = {
      top: offsetTop,
      left: offsetLeft - contentScrollLeft,
      minWidth: contentWidth
    };

    const data = () => {
      const result = {};
      columns.forEach((column) => {
        result[column.get('id')] = sorting && !column.get('canNotSorting') ? (
          <Sorting
            id={column.get('id')}
            title={column.get('title')}
            sorting={column.get('sorting')}
            handleToggleSorting={handleToggleSorting}
          />
        )
        :
        column.get('title');
      });
      return result;
    };

    return (
      <div className={elementClassName} style={style} ref={(element) => { this.element = element; }} data-loc={dataLoc}>
        {select &&
        <div className="b-spreadsheet__table-head-checkbox" title={selectAllTitle}>
          <RboFormFieldCheckbox
            id="select-all-items-checkbox"
            onChange={handleToggleSelectAll}
            value={selectedAllItems}
          />
        </div>
        }
        <div className="b-spreadsheet__table-head-inner">
          <Row
            columns={columns}
            renderCells={renderCells}
            resize={resize}
            handleResizeColumn={handleResizeColumn}
            {...data()}
          />
        </div>
      </div>
    );
  }
}

export default SpreadsheetTableHead;
