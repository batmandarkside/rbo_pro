import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';

const SpreadsheetTableHeadSorting = (props) => {
  const { id, title, sorting, handleToggleSorting } = props;
  const handleClick = () => handleToggleSorting(id);
  const iconType = () => {
    switch (sorting) {
      case 1: return 'sorting-asc';
      case -1: return 'sorting-desc';
      default: return 'sorting';
    }
  };

  return (
    <div className="b-spreadsheet__table-head-sorting" onClick={handleClick}>
      <div className="b-spreadsheet__table-head-sorting-title">
        {title}
      </div>
      <div className="b-spreadsheet__table-head-sorting-icon">
        <Icon type={iconType()} size="16" />
      </div>
    </div>
  );
};

SpreadsheetTableHeadSorting.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  sorting: PropTypes.number.isRequired,
  handleToggleSorting: PropTypes.func.isRequired
};

export default SpreadsheetTableHeadSorting;
