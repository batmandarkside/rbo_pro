import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import classnames from 'classnames';
import Loader from '@rbo/components/lib/loader/Loader';
import Head from './head';
import Body from './body';
import Settings from './settings';
import './style.css';

class SpreadsheetTable extends Component {
  static propTypes = {
    dataLocs: PropTypes.object,
    columns: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    renderBodyCells: PropTypes.objectOf(PropTypes.func),
    renderHeadCells: PropTypes.objectOf(PropTypes.func),
    isListLoading: PropTypes.bool,
    emptyListMessage: PropTypes.string,
    settings: PropTypes.shape({
      iconTitle: PropTypes.string,
      changeColumnsAction: PropTypes.func,
      showGrouping: PropTypes.bool,
      changeGroupingAction: PropTypes.func,
      showPagination: PropTypes.bool,
      paginationOptions: PropTypes.arrayOf(
        PropTypes.shape({
          value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
          title: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.element),
            PropTypes.element,
            PropTypes.string,
            PropTypes.number
          ]).isRequired,
          selected: PropTypes.bool
        })
      ),
      changePaginationAction: PropTypes.func
    }),
    sorting: PropTypes.shape({
      changeSortingAction: PropTypes.func
    }),
    select: PropTypes.shape({
      selectItemTitle: PropTypes.string,
      selectGroupTitle: PropTypes.string,
      selectAllTitle: PropTypes.string,
      changeSelectAction: PropTypes.func
    }),
    onItemClick: PropTypes.func,
    resize: PropTypes.shape({
      resizeColumnsAction: PropTypes.func
    }),
    operations: PropTypes.instanceOf(List),
    operationAction: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      list: this.props.list,
      columns: this.props.columns,
      offsetWidth: 0,
      offsetTop: 0,
      offsetLeft: 0,
      contentScrollTop: 0,
      contentScrollLeft: 0,
      contentWidth: 0,
      headerHeight: 0,
      selectedAllItems: false
    };

    this.handleWindowResize = this.handleWindowResize.bind(this);
    this.handleDocumentScroll = this.handleDocumentScroll.bind(this);
    this.handleElementScroll = this.handleElementScroll.bind(this);

    this.handleToggleSettingsColumn = this.handleToggleSettingsColumn.bind(this);
    this.handleCheckAllSettingsColumns = this.handleCheckAllSettingsColumns.bind(this);
    this.handleToggleSettingsGrouping = this.handleToggleSettingsGrouping.bind(this);
    this.handleToggleSettingsPagination = this.handleToggleSettingsPagination.bind(this);
    this.handleToggleSorting = this.handleToggleSorting.bind(this);
    this.handleToggleSelectItem = this.handleToggleSelectItem.bind(this);
    this.handleToggleSelectGroup = this.handleToggleSelectGroup.bind(this);
    this.handleToggleSelectAll = this.handleToggleSelectAll.bind(this);
    this.handleResizeColumn = this.handleResizeColumn.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize);
    document.addEventListener('scroll', this.handleDocumentScroll);
    this.innerElement.addEventListener('scroll', this.handleElementScroll);
    this.getHeaderHeight();
    setTimeout(this.handleWindowResize, 0);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      list: nextProps.list,
      columns: nextProps.columns,
      selectedAllItems: nextProps.list.size ?
        nextProps.list.filter(group => group.get('selected')).size === nextProps.list.size :
        false
    });
    setTimeout(this.handleWindowResize, 0);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
    document.removeEventListener('scroll', this.handleDocumentScroll);
    this.innerElement.removeEventListener('scroll', this.handleElementScroll);
  }

  getHeaderHeight = () => {
    const headerElementOffset = this.headerElement.getBoundingClientRect();
    const headerHeight = headerElementOffset.height ? headerElementOffset.height - 1 : 0;

    this.setState({
      headerHeight
    });
  };

  getOffset = () => {
    const elementOffset = this.element.getBoundingClientRect();
    const offsetWidth = elementOffset.width || 0;
    const offsetTop = elementOffset.top || 0;
    const offsetLeft = elementOffset.left || 0;
    const contentElementOffset = this.contentElement.getBoundingClientRect();
    const contentWidth = contentElementOffset.width || 0;

    return {
      offsetWidth,
      offsetTop,
      offsetLeft,
      contentWidth
    };
  };

  handleWindowResize() {
    if (!this.element) return;
    this.setState({
      ...this.getOffset(),
    });
  }

  handleDocumentScroll() {
    this.setState({
      ...this.getOffset(),
    });
  }

  handleElementScroll(e) {
    const contentScrollLeft = !e ? 0 : e.target.scrollLeft;
    const contentScrollTop = !e ? 0 : e.target.scrollTop;

    this.setState({
      ...this.getOffset(),
      contentScrollLeft,
      contentScrollTop
    });
  }

  handleToggleSettingsColumn(columnId) {
    const columns = this.state.columns.map((column) => {
      if (column.get('id') === columnId) {
        return column.set('visible', !column.get('visible'));
      }
      return column;
    });

    this.props.settings.changeColumnsAction ?
      this.props.settings.changeColumnsAction(columns) :
      this.setState({ columns });
  }

  handleCheckAllSettingsColumns() {
    const columns = this.state.columns.map(column => column.set('visible', true));

    this.props.settings.changeColumnsAction ?
      this.props.settings.changeColumnsAction(columns) :
      this.setState({ columns });
  }

  handleToggleSettingsGrouping(columnId) {
    const columns = this.state.columns.map((column) => {
      if (columnId === 'withoutGrouped') {
        return column.set('grouped', false);
      }
      return column.set('grouped', column.get('id') === columnId)
        .set('sorting', (column.get('id') === columnId) ? (column.get('sorting') || 1) : 0);
    });

    this.props.settings.changeGroupingAction ?
      this.props.settings.changeGroupingAction(columns) :
      this.setState({ columns });
  }

  handleToggleSettingsPagination(value) {
    this.props.settings.changePaginationAction && this.props.settings.changePaginationAction(parseInt(value, 10));
  }

  handleToggleSorting(columnId) {
    const columns = this.state.columns.map(column => (
      (column.get('id') === columnId) ?
        column.set('sorting', column.get('sorting') === 1 ? -1 : 1) :
        column.set('sorting', 0).set('grouped', false)
    ));

    this.props.sorting.changeSortingAction ?
      this.props.sorting.changeSortingAction(columns) :
      this.setState({ columns });
  }

  handleToggleSelectItem(itemId) {
    let list = this.state.list.map(group => group
      .set('items',
        group.get('items').map((item) => {
          if (item.get('id') === itemId) {
            return item.set('selected', !item.get('selected'));
          }
          return item;
        }))
    );

    list = list.map(
      group => group.set('selected',
        group.get('items').filter(item => item.get('selected')).size === group.get('items').size)
    );

    const selectedAllItems = list.filter(group => group.get('selected')).size === list.size;

    const selectedItems = [];
    list.forEach((group) => {
      group.get('items').filter(item => item.get('selected')).forEach((item) => {
        selectedItems.push(item.get('id'));
      });
    });

    this.props.select.changeSelectAction ?
      this.props.select.changeSelectAction(List(selectedItems)) :
      this.setState({ list, selectedAllItems });
  }

  handleToggleSelectGroup(groupId) {
    const list = this.state.list.map((group) => {
      if (group.get('id') === groupId) {
        const selected = !group.get('selected');
        return group
          .set('selected', selected)
          .set('items', group.get('items').map(item => item.set('selected', selected)));
      }
      return group;
    });

    const selectedAllItems = list.filter(group => group.get('selected')).size === list.size;

    const selectedItems = [];
    list.forEach((group) => {
      group.get('items').filter(item => item.get('selected')).forEach((item) => {
        selectedItems.push(item.get('id'));
      });
    });

    this.props.select.changeSelectAction ?
      this.props.select.changeSelectAction(List(selectedItems)) :
      this.setState({ list, selectedAllItems });
  }

  handleToggleSelectAll() {
    const { selectedAllItems } = this.state;
    const list = this.state.list.map(group => group
        .set('selected', !selectedAllItems)
        .set('items', group.get('items').map(item => item.set('selected', !selectedAllItems))));

    const selectedItems = [];
    list.forEach((group) => {
      group.get('items').filter(item => item.get('selected')).forEach((item) => {
        selectedItems.push(item.get('id'));
      });
    });

    this.props.select.changeSelectAction ?
      this.props.select.changeSelectAction(List(selectedItems)) :
      this.setState({ list, selectedAllItems: !selectedAllItems });
  }

  handleResizeColumn(columnId, width, saveState) {
    const columns = this.state.columns.map((column) => {
      if (column.get('id') === columnId) {
        return column.set('width', width > 80 ? width : 80);
      }
      return column;
    });

    this.setState({
      columns,
      ...this.getOffset(),
    });

    saveState && this.props.resize.resizeColumnsAction && this.props.resize.resizeColumnsAction(columns);
  }

  render() {
    const {
      dataLocs,
      renderBodyCells,
      renderHeadCells,
      isListLoading,
      emptyListMessage,
      settings,
      sorting,
      select,
      onItemClick,
      operations,
      operationAction,
      resize
    } = this.props;

    const {
      list,
      columns,
      offsetWidth,
      offsetTop,
      offsetLeft,
      contentScrollTop,
      contentScrollLeft,
      contentWidth,
      headerHeight,
      selectedAllItems
    } = this.state;

    const innerStyle = { top: headerHeight };
    const contentClassName = classnames(
      'b-spreadsheet__table-content',
      settings && 'm-with-settings',
      operations && 'm-with-operations'
    );

    return (
      <div
        className="b-spreadsheet__table"
        ref={(element) => { this.element = element; }}
        data-loc={dataLocs && dataLocs.main}
      >
        <div
          className="b-spreadsheet__table-inner"
          ref={(element) => { this.innerElement = element; }}
          style={innerStyle}
        >
          <div
            className={contentClassName}
            ref={(element) => { this.contentElement = element; }}
          >
            <Head
              dataLoc={dataLocs && dataLocs.head}
              ref={(element) => { this.headerElement = element; }}
              columns={columns}
              renderCells={renderHeadCells}
              offsetTop={offsetTop}
              offsetLeft={offsetLeft} 
              contentScrollTop={contentScrollTop}
              contentScrollLeft={contentScrollLeft}
              contentWidth={contentWidth}
              sorting={!!sorting}
              handleToggleSorting={this.handleToggleSorting}
              select={!!select}
              selectAllTitle={select && select.selectAllTitle}
              selectedAllItems={selectedAllItems}
              handleToggleSelectAll={this.handleToggleSelectAll}
              resize={!!resize}
              handleResizeColumn={this.handleResizeColumn}
            />
            <Body
              dataLoc={dataLocs && dataLocs.body}
              columns={columns}
              list={list}
              renderCells={renderBodyCells}
              handleOnItemClick={onItemClick}
              select={!!select}
              selectGroupTitle={select && select.selectGroupTitle}
              selectItemTitle={select && select.selectItemTitle}
              handleToggleSelectItem={this.handleToggleSelectItem}
              handleToggleSelectGroup={this.handleToggleSelectGroup}
              operations={operations}
              operationAction={operationAction}
              offsetWidth={offsetWidth}
              contentScrollLeft={contentScrollLeft}
            />
          </div>
          {isListLoading && <Loader centered />}
          {!isListLoading && !list.size &&
          <div className="b-spreadsheet__table-empty">
            {emptyListMessage}
          </div>}
        </div>
        {settings && <Settings
          dataLocs={dataLocs && dataLocs.settings}
          iconTitle={settings.iconTitle}
          columns={columns}
          handleToggleColumn={this.handleToggleSettingsColumn}
          handleCheckAllColumns={this.handleCheckAllSettingsColumns}
          showGrouping={settings.showGrouping}
          handleToggleGrouping={this.handleToggleSettingsGrouping}
          showPagination={settings.showPagination}
          paginationOptions={settings.paginationOptions}
          handleTogglePagination={this.handleToggleSettingsPagination}
        />}
      </div>
    );
  }
}

export default SpreadsheetTable;
