import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { List, Map } from 'immutable';
import { RboFormFieldCheckbox } from 'components/ui-components/rbo-form-compact';
import { LOCATORS } from '../../constants';
import Row from '../row';
import Operations from './operations';

class SpreadsheetTableItem extends Component {
  static propTypes = {
    columns: PropTypes.instanceOf(List).isRequired,
    item: PropTypes.instanceOf(Map).isRequired,
    renderCells: PropTypes.objectOf(PropTypes.func),
    handleOnItemClick: PropTypes.func,
    select: PropTypes.bool.isRequired,
    selectItemTitle: PropTypes.string,
    handleToggleSelectItem: PropTypes.func,
    operations: PropTypes.instanceOf(List),
    operationAction: PropTypes.func,
    offsetWidth: PropTypes.number.isRequired,
    contentScrollLeft: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      operationsPopupOpen: false
    };

    this.handleOnClick = this.handleOnClick.bind(this);
    this.handleToggleCheckbox = this.handleToggleCheckbox.bind(this);
    this.handleOperationsPopupOpen = this.handleOperationsPopupOpen.bind(this);
    this.handleOperationsPopupClose = this.handleOperationsPopupClose.bind(this);
    this.handleOnMouseLeave = this.handleOnMouseLeave.bind(this);
  }

  handleOnClick() {
    const { item } = this.props;
    this.props.handleOnItemClick && this.props.handleOnItemClick(item.get('id'));
  }

  handleToggleCheckbox() {
    const { item } = this.props;
    this.props.handleToggleSelectItem && this.props.handleToggleSelectItem(item.get('id'));
  }

  handleOperationsPopupOpen() {
    this.setState({
      operationsPopupOpen: true
    });
  }

  handleOperationsPopupClose() {
    this.setState({
      operationsPopupOpen: false
    });
  }

  handleOnMouseLeave(e) {
    if (!e.relatedTarget || !e.relatedTarget.closest || !e.relatedTarget.closest('.b-popup-box')) {
      this.handleOperationsPopupClose();
    }
  }

  render() {
    const {
      columns, item, handleOnItemClick,
      renderCells, select, selectItemTitle, operations, operationAction, offsetWidth, contentScrollLeft
    } = this.props;
    const { operationsPopupOpen } = this.state;

    const elementClassName = classnames(
      'b-spreadsheet__table-item',
      item.get('selected') && 's-selected',
      handleOnItemClick && 'm-link'
    );

    const itemOperations = item.get('operations');
    const isHaveAllowedOperations = itemOperations &&
      !!Object.keys(itemOperations).filter(key => itemOperations[key]).length;

    return (
      <div className={elementClassName} onMouseLeave={this.handleOnMouseLeave} data-loc={LOCATORS.TABLE_ITEM}>
        {select &&
        <div
          className="b-spreadsheet__table-item-checkbox"
          title={selectItemTitle}
        >
          <RboFormFieldCheckbox
            id={item.get('id')}
            onChange={this.handleToggleCheckbox}
            value={item.get('selected')}
          />
        </div>
        }
        <div className="b-spreadsheet__table-item-content" onClick={this.handleOnClick}>
          <Row
            columns={columns}
            renderCells={renderCells}
            {...item.toJS()}
          />
        </div>
        {operations && isHaveAllowedOperations &&
        <Operations
          itemId={item.get('id')}
          itemOperations={item.get('operations')}
          operations={operations}
          operationAction={operationAction}
          offsetWidth={offsetWidth}
          contentScrollLeft={contentScrollLeft}
          popupOpen={operationsPopupOpen}
          handleOperationsPopupOpen={this.handleOperationsPopupOpen}
          handleOperationsPopupClose={this.handleOperationsPopupClose}
        />
        }
      </div>
    );
  }
}

export default SpreadsheetTableItem;
