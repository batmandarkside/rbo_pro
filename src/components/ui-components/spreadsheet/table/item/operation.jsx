import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'ui-components/icon';

const SpreadsheetTableItemOperation = (props) => {
  const { id, title, icon, action, itemId, handleOperationsPopupClose } = props;
  const handleOnClick = () => {
    action(id, [itemId]);
    handleOperationsPopupClose();
  };

  return (
    <div className="b-spreadsheet__table-item-operations-item" onClick={handleOnClick}>
      <div className="b-spreadsheet__table-item-operations-item-icon">
        <Icon type={icon} size="16" />
      </div>
      <div className="b-spreadsheet__table-item-operations-item-title">
        {title}
      </div>
    </div>
  );
};

SpreadsheetTableItemOperation.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
  itemId: PropTypes.string.isRequired,
  handleOperationsPopupClose: PropTypes.func.isRequired
};

export default SpreadsheetTableItemOperation;
