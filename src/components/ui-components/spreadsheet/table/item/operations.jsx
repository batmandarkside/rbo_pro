import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import Icon from 'ui-components/icon';
import PopupBox from 'ui-components/popup-box';
import Operation from './operation';

class SpreadsheetTableItemOperations extends Component {
  static propTypes = {
    itemId: PropTypes.string.isRequired,
    itemOperations: PropTypes.object.isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    operationAction: PropTypes.func.isRequired,
    offsetWidth: PropTypes.number.isRequired,
    contentScrollLeft: PropTypes.number.isRequired,
    popupOpen: PropTypes.bool.isRequired,
    handleOperationsPopupOpen: PropTypes.func.isRequired,
    handleOperationsPopupClose: PropTypes.func.isRequired
  };

  render() {
    const { itemId, itemOperations, operations, operationAction, offsetWidth, contentScrollLeft,
      popupOpen, handleOperationsPopupOpen, handleOperationsPopupClose } = this.props;
    const style = { left: offsetWidth + contentScrollLeft };

    return (
      <div className="b-spreadsheet__table-item-operations" style={style}>
        <div className="b-spreadsheet__table-item-operations-toggle">
          <div
            className="b-spreadsheet__table-item-operations-toggle-icon"
            ref={(element) => { this.popupOpenerElement = element; }}
          >
            <Icon type="context-vertical" size="16" title="Операции" onClick={handleOperationsPopupOpen} />
          </div>
        </div>
        {popupOpen &&
        <PopupBox
          openerElement={this.popupOpenerElement}
          onClose={handleOperationsPopupClose}
          closeOnMouseLeave
        >
          <div className="b-spreadsheet__table-item-operations-popup">
            <div className="b-spreadsheet__table-item-operations-items">
              {Object.keys(itemOperations).filter(key => !!itemOperations[key]).map(key => (
                <Operation
                  key={key}
                  id={key}
                  title={operations.find(operation => operation.get('id') === key).get('title')}
                  icon={operations.find(operation => operation.get('id') === key).get('icon')}
                  action={operationAction}
                  itemId={itemId}
                  handleOperationsPopupClose={handleOperationsPopupClose}
                />
              ))}
            </div>
          </div>
        </PopupBox>
        }
      </div>
    );
  }
}

export default SpreadsheetTableItemOperations;
