import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { LOCATORS } from '../../constants';
import Cell from '../cell/head-cell';

const SpreadsheetTableHeadRow = (props) => {
  const { columns, renderCells, resize, handleResizeColumn } = props;
  return (
    <div className="b-spreadsheet__table-row" data-loc={LOCATORS.TABLE_HEAD_ROW}>
      {columns.filter(column => column.get('visible')).map((column, key) => (
        <Cell
          key={key}
          itemId={props.id}
          renderInner={renderCells && renderCells[column.get('id')] && renderCells[column.get('id')]}
          columnId={column.get('id')}
          width={column.get('width')}
          resize={resize}
          handleResize={handleResizeColumn}
        >
          {props[column.get('id')] && props[column.get('id')]}
        </Cell>
      ))}
    </div>
  );
};

SpreadsheetTableHeadRow.propTypes = {
  columns: PropTypes.instanceOf(List).isRequired,
  renderCells: PropTypes.objectOf(PropTypes.func),
  resize: PropTypes.bool.isRequired,
  handleResizeColumn: PropTypes.func
};

export default SpreadsheetTableHeadRow;
