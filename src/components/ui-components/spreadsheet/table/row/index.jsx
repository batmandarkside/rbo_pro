import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { LOCATORS } from '../../constants';
import Cell from '../cell';

const SpreadsheetTableRow = (props) => {
  const { columns, renderCells } = props;
  return (
    <div className="b-spreadsheet__table-row" data-loc={LOCATORS.TABLE_ROW}>
      {columns.filter(column => column.get('visible')).map((column, key) => (
        <Cell
          key={key}
          itemId={props.id}
          renderInner={renderCells && renderCells[column.get('id')] && renderCells[column.get('id')]}
          modeClassName={column.get('id')}
          width={column.get('width')}
        >
          {props[column.get('id')] && props[column.get('id')]}
        </Cell>
      ))}
    </div>
  );
};

SpreadsheetTableRow.propTypes = {
  columns: PropTypes.instanceOf(List).isRequired,
  renderCells: PropTypes.objectOf(PropTypes.func),
};

export default SpreadsheetTableRow;
