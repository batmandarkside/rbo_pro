import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { List } from 'immutable';
import { RboFormFieldCheckbox } from 'components/ui-components/rbo-form-compact';


class SpreadsheetTableSettingsColumns extends Component {
  static propTypes = {
    columns: PropTypes.instanceOf(List).isRequired,
    handleToggleColumn: PropTypes.func.isRequired,
    handleCheckAllColumns: PropTypes.func.isRequired
  };

  handleToggleColumn = ({ fieldId }) => {
    this.props.handleToggleColumn(fieldId);
  };

  handleCheckAllColumns = () => (
    this.props.columns.filter(column => !column.get('visible')).size && this.props.handleCheckAllColumns()
  );

  renderItem = (column, key) => (
    <div className="b-spreadsheet__table-settings-columns-item" key={key}>
      <RboFormFieldCheckbox
        id={column.get('id')}
        title={column.get('title')}
        value={column.get('visible')}
        onChange={this.handleToggleColumn}
      />
    </div>
  );

  render() {
    const { columns } = this.props;
    const checkAllLinkClassName = classnames('m-dashed-link', !columns.filter(column => !column.get('visible')).size && 's-disabled');

    return (
      <div className="b-spreadsheet__table-settings-block">
        <div className="b-spreadsheet__table-settings-block-header">
          <div className="b-spreadsheet__table-settings-block-title">
            <div className="b-spreadsheet__table-settings-block-check-all">
              <span className={checkAllLinkClassName} onClick={this.handleCheckAllColumns}>Показать все</span>
            </div>
            <h5>Столбцы:</h5>
          </div>
        </div>
        <div className="b-spreadsheet__table-settings-block-content">
          <div className="b-spreadsheet__table-settings-columns">
            {columns.map(this.renderItem)}
          </div>
        </div>
      </div>
    );
  }
}

export default SpreadsheetTableSettingsColumns;
