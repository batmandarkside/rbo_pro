import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { RboFormFieldDropdown } from 'components/ui-components/rbo-form-compact';

const SpreadsheetTableSettingsGrouping = (props) => {
  const { columns, handleToggleGrouping } = props;
  const clearGrouping = [{
    value: 'withoutGrouped',
    title: 'Без группировки',
    selected: !columns.find(column => column.get('canGrouped') && column.get('grouped'))
  }];
  const options = clearGrouping.concat(columns.filter(column => !!column.get('canGrouped')).map(column => (
    {
      value: column.get('id'),
      title: column.get('title'),
      selected: column.get('grouped')
    }
  )).toJS());
  const selectedOption = options.find(column => column.selected);
  const initValue = selectedOption && selectedOption.value;

  return (
    <div className="b-spreadsheet__table-settings-block">
      <div className="b-spreadsheet__table-settings-block-header">
        <div className="b-spreadsheet__table-settings-block-title">
          <h5>Группировать по:</h5>
        </div>
      </div>
      <div className="b-spreadsheet__table-settings-block-content">
        <div className="b-spreadsheet__table-settings-grouping">
          <RboFormFieldDropdown
            id="grouping-dropdown-box"
            options={options}
            onChange={handleToggleGrouping}
            value={initValue}
          />
        </div>
      </div>
    </div>
  );
};

SpreadsheetTableSettingsGrouping.propTypes = {
  columns: PropTypes.instanceOf(List).isRequired,
  handleToggleGrouping: PropTypes.func.isRequired,
};

export default SpreadsheetTableSettingsGrouping;
