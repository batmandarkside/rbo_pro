import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import Icon from 'components/ui-components/icon';
import Popup from './popup';

class SpreadsheetTableSettings extends Component {
  static propTypes = {
    dataLocs: PropTypes.object,
    columns: PropTypes.instanceOf(List).isRequired,
    iconTitle: PropTypes.string,
    handleToggleColumn: PropTypes.func.isRequired,
    handleCheckAllColumns: PropTypes.func.isRequired,
    showGrouping: PropTypes.bool,
    handleToggleGrouping: PropTypes.func.isRequired,
    showPagination: PropTypes.bool,
    paginationOptions: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
        title: PropTypes.oneOfType([
          PropTypes.arrayOf(PropTypes.element),
          PropTypes.element,
          PropTypes.string,
          PropTypes.number
        ]).isRequired,
        selected: PropTypes.bool
      })
    ),
    handleTogglePagination: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      popupOpen: false
    };

    this.handlePopupOpen = this.handlePopupOpen.bind(this);
    this.handlePopupClose = this.handlePopupClose.bind(this);
    this.handleChangeGrouping = this.handleChangeGrouping.bind(this);
    this.handleChangePagination = this.handleChangePagination.bind(this);
  }

  handlePopupOpen() {
    this.setState({
      popupOpen: true
    });
  }

  handlePopupClose() {
    this.setState({
      popupOpen: false
    });
  }

  handleChangeGrouping({ fieldValue }) {
    this.props.handleToggleGrouping(fieldValue);
    this.handlePopupClose();
  }

  handleChangePagination({ fieldValue }) {
    this.props.handleTogglePagination(fieldValue);
    this.handlePopupClose();
  }

  render() {
    const { popupOpen } = this.state;
    const {
      dataLocs,
      columns, iconTitle, handleToggleColumn, handleCheckAllColumns,
      showGrouping,
      showPagination, paginationOptions
    } = this.props;

    return (
      <div className="b-spreadsheet__table-settings" data-loc={dataLocs && dataLocs.main}>
        <div className="b-spreadsheet__table-settings-toggle" data-loc={dataLocs && dataLocs.toggle}>
          <div
            className="b-spreadsheet__table-settings-toggle-icon"
            ref={(element) => { this.popupOpenerElement = element; }}
          >
            <Icon
              type="settings"
              size="16"
              title={iconTitle}
              onClick={this.handlePopupOpen}
              locator={dataLocs && dataLocs.toggleButton}
            />
          </div>
        </div>
        {popupOpen &&
          <Popup
            dataLoc={dataLocs && dataLocs.popup}
            columns={columns}
            handleToggleColumn={handleToggleColumn}
            handleCheckAllColumns={handleCheckAllColumns}
            showGrouping={showGrouping}
            handleToggleGrouping={this.handleChangeGrouping}
            showPagination={showPagination}
            paginationOptions={paginationOptions}
            handleTogglePagination={this.handleChangePagination}
            openerElement={this.popupOpenerElement}
            handlePopupClose={this.handlePopupClose}
          />
        }
      </div>
    );
  }
}

export default SpreadsheetTableSettings;
