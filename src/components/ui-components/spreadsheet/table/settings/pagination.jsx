import React from 'react';
import PropTypes from 'prop-types';
import { RboFormFieldDropdown } from 'components/ui-components/rbo-form-compact';

const SpreadsheetTableSettingsPagination = (props) => {
  const { options, handleTogglePagination } = props;
  const dropOptions = options.map(option => ({
    ...option,
    title: option.title.toString(),
    value: option.value.toString()
  }));
  const selectedOption = dropOptions.find(option => option.selected);
  const initValue = selectedOption && selectedOption.value;

  return (
    <div className="b-spreadsheet__table-settings-block">
      <div className="b-spreadsheet__table-settings-block-header">
        <div className="b-spreadsheet__table-settings-block-title">
          <h5>Показывать по:</h5>
        </div>
      </div>
      <div className="b-spreadsheet__table-settings-block-content">
        <div className="b-spreadsheet__table-settings-pagination">
          <div className="b-spreadsheet__table-settings-pagination-dropdown-box">
            <RboFormFieldDropdown
              id="grouping-dropdown-box"
              options={dropOptions}
              onChange={handleTogglePagination}
              value={initValue}
            />
          </div>
          <div className="b-spreadsheet__table-settings-pagination-label">
            строк на&nbsp;странице
          </div>
        </div>
      </div>
    </div>
  );
};

SpreadsheetTableSettingsPagination.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
      title: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.element),
        PropTypes.element,
        PropTypes.string,
        PropTypes.number
      ]).isRequired,
      selected: PropTypes.bool
    })
  ).isRequired,
  handleTogglePagination: PropTypes.func.isRequired,
};

export default SpreadsheetTableSettingsPagination;
