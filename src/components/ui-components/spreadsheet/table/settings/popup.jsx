import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import PopupBox from 'ui-components/popup-box';
import Columns from './columns';
import Grouping from './grouping';
import Pagination from './pagination';

const SpreadsheetTableSettingsPopup = (props) => {
  const {
    dataLoc,
    columns,
    handleToggleColumn, handleCheckAllColumns,
    showGrouping, handleToggleGrouping,
    showPagination, paginationOptions, handleTogglePagination,
    openerElement, handlePopupClose
  } = props;

  return (
    <PopupBox
      dataLoc={dataLoc}
      openerElement={openerElement}
      onClose={handlePopupClose}
    >
      <div className="b-spreadsheet__table-settings-popup">
        <Columns
          columns={columns}
          handleToggleColumn={handleToggleColumn}
          handleCheckAllColumns={handleCheckAllColumns}
        />
        {showGrouping &&
        <Grouping
          columns={columns}
          handleToggleGrouping={handleToggleGrouping}
        />
        }
        {showPagination &&
        <Pagination
          options={paginationOptions}
          handleTogglePagination={handleTogglePagination}
        />
        }
      </div>
    </PopupBox>
  );
};

SpreadsheetTableSettingsPopup.propTypes = {
  dataLoc: PropTypes.string,
  columns: PropTypes.instanceOf(List).isRequired,
  handleToggleColumn: PropTypes.func.isRequired,
  handleCheckAllColumns: PropTypes.func.isRequired,
  showGrouping: PropTypes.bool,
  handleToggleGrouping: PropTypes.func,
  showPagination: PropTypes.bool,
  paginationOptions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
      title: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.element),
        PropTypes.element,
        PropTypes.string,
        PropTypes.number
      ]).isRequired,
      selected: PropTypes.bool
    })
  ),
  handleTogglePagination: PropTypes.func,
  openerElement: PropTypes.object.isRequired,
  handlePopupClose: PropTypes.func.isRequired,
};

export default SpreadsheetTableSettingsPopup;
