import React from 'react';
import PropTypes from 'prop-types';
import Item from './item';

const SpreadsheetToolbarButtons = (props) => {
  const { dataLocs, items } = props;
  return (
    <div className="b-spreadsheet__toolbar-buttons" data-loc={dataLocs && dataLocs.main}>
      <div className="b-spreadsheet__toolbar-buttons-items">
        {items.map((item, key) => (
          <Item
            key={key}
            isDisabled={item.isDisabled}
            isProgress={item.isProgress}
            dataLoc={dataLocs && dataLocs[item.id]}
            type={item.type}
            title={item.title}
            items={item.items}
            onClick={item.onClick}
          />
        ))}
      </div>
    </div>
  );
};

SpreadsheetToolbarButtons.propTypes = {
  dataLocs: PropTypes.object,
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    isDisabled: PropTypes.bool,
    isProgress: PropTypes.bool,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
      isDefault: PropTypes.bool,
      id: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    })),
    onClick: PropTypes.func.isRequired
  }))
};

export default SpreadsheetToolbarButtons;
