import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/ui-components/button';

const SpreadsheetToolbarButtonsItem = (props) => {
  const { isDisabled, isProgress, dataLoc, type, title, items, onClick } = props;

  return (
    <div className="b-spreadsheet__toolbar-buttons-item">
      {type === 'refresh' &&
      <Button
        isDisabled={isDisabled}
        isProgress={isProgress}
        dataLoc={dataLoc}
        type="tertiary"
        icon="refresh"
        iconTitle={title}
        onClick={onClick}
      />
      }
      {type === 'simple' &&
      <Button
        isDisabled={isDisabled}
        isProgress={isProgress}
        dataLoc={dataLoc}
        type="primary"
        title={title}
        onClick={onClick}
      />
      }
      {type === 'multiple' &&
      <Button
        isDisabled={isDisabled}
        isProgress={isProgress}
        dataLoc={dataLoc}
        type="primary"
        multiple
        items={items}
        title={title}
        onClick={onClick}
      />
      }
    </div>
  );
};

SpreadsheetToolbarButtonsItem.propTypes = {
  isDisabled: PropTypes.bool,
  isProgress: PropTypes.bool,
  dataLoc: PropTypes.string,
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    isDefault: PropTypes.bool,
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  })),
  onClick: PropTypes.func.isRequired
};

export default SpreadsheetToolbarButtonsItem;
