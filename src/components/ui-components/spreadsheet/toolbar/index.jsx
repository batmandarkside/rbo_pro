import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import Buttons from './buttons';
import Tabs from './tabs';
import './style.css';

const SpreadsheetToolbar = (props) => {
  const { dataLocs, title, renderTitle, buttons, tabs } = props;
  return (
    <div className="b-spreadsheet__toolbar" data-loc={dataLocs && dataLocs.main}>
      <div className="b-spreadsheet__toolbar-inner">
        <div className="b-spreadsheet__toolbar-title">
          {renderTitle ? renderTitle() : <h2>{title}</h2>}
        </div>
        <div className="b-spreadsheet__toolbar-content">
          {buttons && <Buttons items={buttons} dataLocs={dataLocs && dataLocs.buttons} />}
          {tabs && <Tabs {...tabs} dataLoc={dataLocs && dataLocs.tabs} />}
        </div>
      </div>
    </div>
  );
};

SpreadsheetToolbar.propTypes = {
  dataLocs: PropTypes.object,
  title: PropTypes.string,
  renderTitle: PropTypes.func,
  buttons: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    isDisabled: PropTypes.bool,
    isProgress: PropTypes.bool,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
      isDefault: PropTypes.bool,
      id: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    })),
    onClick: PropTypes.func.isRequired
  })),
  tabs: PropTypes.shape({
    items: PropTypes.instanceOf(List).isRequired,
    selected  : PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
  })
};

export default SpreadsheetToolbar;
