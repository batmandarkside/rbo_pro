import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import RboTabs  from 'components/ui-components/rbo-tabs';

const SpreadsheetToolbarTabs = (props) => {
  const { dataLoc, items, onChange } = props;
  return (
    <div className="b-spreadsheet__toolbar-tabs" data-loc={dataLoc}>
      <RboTabs
        tabs={items}
        align="right"
        onTabClick={onChange}
      />
    </div>
  );
};

SpreadsheetToolbarTabs.propTypes = {
  dataLoc   : PropTypes.string,
  items     : PropTypes.instanceOf(List).isRequired,
  onChange  : PropTypes.func.isRequired
};

export default SpreadsheetToolbarTabs;
