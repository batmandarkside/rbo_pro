/* eslint-disable react/jsx-filename-extension */
import React            from 'react';
import { expect }       from 'chai';
import { mount }        from 'enzyme';
import { TitleBorderBlock }   from '../index';

describe('<TitleBorderBlock /> tests', () => {
  it('Renders <TitleBorderBlock /> ', () => {
    const wrapper = mount(<TitleBorderBlock className="TitleBorderBlockClass" title="Hello world" />);
    expect(wrapper.find('.TitleBorderBlockClass')).to.have.length(1);
    expect(wrapper.find('.b-title-border-block')).to.have.length(1);
    expect(wrapper.find('.b-title-border-block__title')).to.have.length(1);
    expect(wrapper.find('.b-title-border-block__header')).to.have.length(1);
    wrapper.unmount();
  });

  it('Custom class title <TitleBorderBlock /> ', () => {
    const wrapper = mount(<TitleBorderBlock classNameTitle="CustomClassTitle" />);
    expect(wrapper.find('.b-title-border-block__title.CustomClassTitle')).to.have.length(1);
    expect(wrapper.find('.b-title-border-block__header')).to.have.length(1);
    wrapper.unmount();
  });

  it('Render <TitleBorderBlock /> with children', () => {
    const wrapper = mount(
      <TitleBorderBlock classNameTitle="CustomClassTitle">
        <div className="children">
          <p className="p">
            lorem ipsum
          </p>
        </div>
      </TitleBorderBlock>
    );
    expect(wrapper.find('.children')).to.have.length(1);
    expect(wrapper.find('.p')).to.have.length(1);
    expect(wrapper.find('.p').text()).to.equal('lorem ipsum');
    wrapper.unmount();
  });
});
