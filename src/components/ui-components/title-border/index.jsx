import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Field } from 'redux-form/immutable';
import InputField from './input-field';
import './style.css';

const _handlerDefault = () => {};

/**
 *
 * @param props
 * @returns {XML}
 */
export const TitleBorderBlock = (props) => {
  const {
    className,
    children,
    title,
    titleNotGrow,
    classNameTitle,
    classNameHeader,
    renderControls,
    dataLocHeader,
    dataLocBlock,
    inputName,
    onChangeHandler,
    form,
    warning,
    error
  } = props;

  const classSelectors = classNames('b-title-border-block', {
    [className]: !!className
  });

  const classTitleSelector = classNames('b-title-border-block__title', {
    [classNameTitle]: !!classNameTitle,
    '_title-not-grow': !!titleNotGrow
  });

  const classHeader  = classNames('b-title-border-block__header', {
    [classNameHeader]: !!classNameHeader,
    'b-title-border-block__with-plate': form
  });

  return (
    <div className={classSelectors} data-loc={dataLocBlock}>
      {form &&
        <span className="b-title-border-block__plate">
          шаблон
        </span>
      }
      <div className={classHeader}>
        {form
          ?
            <Field
              name={inputName}
              onChange={onChangeHandler || _handlerDefault}
              component={InputField}
              className="b-title-border-block__field"
              warning={warning}
              error={error}
            />
          :
            <h3 className={classTitleSelector} data-loc={dataLocHeader} >
              {title}
            </h3>
        }
        {renderControls && renderControls()}
      </div>
      {children && <div>{children}</div>}
    </div>
  );
};

TitleBorderBlock.propTypes = {
  dataLocBlock: PropTypes.string,
  dataLocHeader: PropTypes.string,
  title: PropTypes.string,
  titleNotGrow: PropTypes.bool,
  classNameTitle: PropTypes.string,
  classNameHeader: PropTypes.string,
  className: PropTypes.string,
  renderControls: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]),
  inputName: PropTypes.string,
  onChangeHandler: PropTypes.func,
  form: PropTypes.bool,
  warning: PropTypes.bool,
  error: PropTypes.bool
};
