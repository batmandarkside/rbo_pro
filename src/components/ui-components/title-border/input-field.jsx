import React from 'react';
import PropTypes from 'prop-types';
import Input from '@rbo/components/lib/form/Input';

const InputField = ({ input, tabIndex, type, className, error, warning }) => (
  <Input
    {...input}
    tabIndex={tabIndex}
    type={type}
    className={className}
    error={error}
    warning={warning}
  />
);

InputField.propTypes = {
  input: PropTypes.object,
  tabIndex: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  error: PropTypes.bool,
  warning: PropTypes.bool
};

export default InputField;
