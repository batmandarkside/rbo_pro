export const DOC_TYPES = {
  MESSAGE: 'message',
  RECALL: 'recall',
  R_PAYMENT: 'r-payment'
};

export const ROUTER_ALIASES = {
  R_PAYMENTS: {
    INDEX: '/r-payments',
    ITEM: '/r-payments/{:id}',
    NEW: '/r-payments/new'
  },
  INCOMING_MESSAGES: {
    INDEX: '/messages',
    ITEM: '/messages/{:id}',
    NEW: '/messages/new'
  },
  OUTGOING_MESSAGES: {
    INDEX: '/messages/outgoing',
    ITEM: '/messages/outgoing/{:id}',
    NEW: '/messages/outgoing/new'
  },
  STATEMENTS: {
    INDEX: '/statements',
    ITEM: '/statements/{:id}',
    MOVEMENTS: '/statements/movements'
  }
};

export const DOC_STATUSES = {
  NEW: 'Новый',
  CREATED: 'Создан',
  DELIVERED: 'Доставлен',
  IMPORTED: 'Импортирован',
  UNLOADED: 'Выгружен',
  PROCESSED: 'Обработан',
  EXECUTED: 'Исполнен',
  DECLINED: 'Отвергнут Банком',
  DECLINED_BY_ABS: 'Отказан АБС',
  DELAYED: 'Отложен',
  RECALLED: 'Отозван',
  INVALID: 'Ошибка контроля',
  INVALID_PROPS: 'Ошибка реквизитов',
  SIGNED: 'Подписан',
  ACCEPTED: 'Принят',
  DELETED: 'Удален',
  PARTLY_SIGNED: 'Частично подписан',
  INVALID_SIGN: 'ЭП/АСП неверна',
  QUEUED: 'Помещен в очередь',

  CERTIFIED: 'Заверен' // для контрагента
};

export const SIGN_TYPES = {
  FIRST: 'Первая подпись',
  SECOND: 'Вторая подпись',
  VISA: 'Визирующая подпись',
  SINGLE: 'Единственная подпись',
  VK: 'Подпись для ВК',
  CONTROLLER: 'Ревизор'
};

export const SIGN_VALIDATION_STATUSES = {
  VALID: 'Подпись верна',
  INVALID: 'Подпись неверна'
};
