import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  BENEFICIARIES_CONTAINER_GET_SETTINGS_REQUEST: null,
  BENEFICIARIES_CONTAINER_GET_SETTINGS_SUCCESS: null,
  BENEFICIARIES_CONTAINER_GET_QUERY_PARAMS_REQUEST: null,
  BENEFICIARIES_CONTAINER_GET_QUERY_PARAMS_SUCCESS: null,
  BENEFICIARIES_CONTAINER_CHANGE_PAGE: null,
  BENEFICIARIES_CONTAINER_RELOAD_PAGES: null,
  BENEFICIARIES_CONTAINER_CHANGE_FILTERS: null,
  BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST: null,
  BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS: null,
  BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL: null,
  BENEFICIARIES_CONTAINER_SAVE_SETTINGS_REQUEST: null,
  BENEFICIARIES_CONTAINER_SAVE_SETTINGS_SUCCESS: null,
  BENEFICIARIES_CONTAINER_SAVE_SETTINGS_FAIL: null,
  BENEFICIARIES_CONTAINER_GET_LIST_REQUEST: null,
  BENEFICIARIES_CONTAINER_GET_LIST_SUCCESS: null,
  BENEFICIARIES_CONTAINER_GET_LIST_FAIL: null,
  BENEFICIARIES_CONTAINER_EXPORT_LIST_REQUEST: null,
  BENEFICIARIES_CONTAINER_EXPORT_LIST_SUCCESS: null,
  BENEFICIARIES_CONTAINER_EXPORT_LIST_FAIL: null,
  BENEFICIARIES_CONTAINER_UNMOUNT_REQUEST: null,
  BENEFICIARIES_CONTAINER_RESIZE_COLUMNS: null,
  BENEFICIARIES_CONTAINER_CHANGE_COLUMN: null,
  BENEFICIARIES_CONTAINER_CHANGE_GROUPING: null,
  BENEFICIARIES_CONTAINER_CHANGE_SORTING: null,
  BENEFICIARIES_CONTAINER_CHANGE_PAGINATION: null,
  BENEFICIARIES_CONTAINER_CHANGE_SELECT: null,
  BENEFICIARIES_CONTAINER_NEW_BENEFICIARY: null,
  BENEFICIARIES_CONTAINER_ROUTE_TO_ITEM: null,
  BENEFICIARIES_CONTAINER_ROUTE_TO_ITEM_RECALL: null,

  BENEFICIARIES_CONTAINER_OPERATION: null,
  BENEFICIARIES_CONTAINER_OPERATION_REMOVE_CONFIRM_SHOW: null,
  BENEFICIARIES_CONTAINER_OPERATION_REMOVE_CONFIRM_CANCEL: null,
  BENEFICIARIES_CONTAINER_OPERATION_REMOVE_REQUEST: null,
  BENEFICIARIES_CONTAINER_OPERATION_REMOVE_SUCCESS: null,
  BENEFICIARIES_CONTAINER_OPERATION_REMOVE_FAIL: null,
  BENEFICIARIES_CONTAINER_OPERATION_EDIT: null
});

export const LOCAL_REDUCER = 'beneficiariesContainerReducers';

export const LOCAL_ROUTER_ALIAS = '/beneficiaries';

export const SETTINGS_ALIAS = 'beneficiariesContainer';

export const COMPONENT_STYLE_SELECTOR = 'b-beneficiaries-container';

export const LOCATORS = {
  CONTAINER: 'beneficiariesContainer',
  SPREADSHEET: 'beneficiariesContainerSpreadsheet',

  TOOLBAR: 'beneficiariesContainerSpreadsheetToolbar',
  TOOLBAR_BUTTONS: 'beneficiariesContainerSpreadsheetToolbarButtons',
  TOOLBAR_BUTTON_REFRESH_LIST: 'beneficiariesContainerSpreadsheetToolbarButtonRefreshList',
  TOOLBAR_BUTTON_NEW_BENEFICIARY: 'beneficiariesContainerSpreadsheetToolbarButtonNewBeneficiary',
  TOOLBAR_TABS: 'beneficiariesContainerSpreadsheetToolbarTabs',

  FILTERS: 'beneficiariesContainerSpreadsheetFilters',
  FILTERS_SELECT: 'beneficiariesContainerSpreadsheetFiltersSelect',
  FILTERS_ADD: 'beneficiariesContainerSpreadsheetFiltersAdd',
  FILTERS_REMOVE: 'beneficiariesContainerSpreadsheetFiltersRemove',
  FILTERS_OPERATIONS: 'beneficiariesContainerSpreadsheetFiltersOperations',
  FILTERS_OPERATION_SAVE: 'beneficiariesContainerSpreadsheetFiltersOperationSave',
  FILTERS_OPERATION_SAVE_AS: 'beneficiariesContainerSpreadsheetFiltersOperationSaveAs',
  FILTERS_OPERATION_SAVE_AS_POPUP: 'beneficiariesContainerSpreadsheetFiltersOperationSaveAsPopup',
  FILTERS_OPERATION_CLEAR: 'beneficiariesContainerSpreadsheetFiltersOperationClear',
  FILTERS_CONDITIONS: 'beneficiariesContainerSpreadsheetFiltersConditions',
  FILTERS_CONDITION: 'beneficiariesContainerSpreadsheetFiltersCondition',
  FILTERS_CONDITION_POPUP: 'beneficiariesContainerSpreadsheetFiltersConditionPopup',
  FILTERS_CONDITION_REMOVE: 'beneficiariesContainerSpreadsheetFiltersConditionRemove',
  FILTERS_ADD_CONDITION: 'beneficiariesContainerSpreadsheetFiltersAddCondition',
  FILTERS_ADD_CONDITION_POPUP: 'beneficiariesContainerSpreadsheetFiltersAddConditionPopup',
  FILTERS_CLOSE: 'beneficiariesContainerSpreadsheetFiltersClose',

  OPERATIONS: 'beneficiariesContainerSpreadsheetOperations',

  TABLE: 'beneficiariesContainerSpreadsheetTable',
  TABLE_HEAD: 'beneficiariesContainerSpreadsheetTableHead',
  TABLE_BODY: 'beneficiariesContainerSpreadsheetTableBody',
  TABLE_SETTINGS: 'beneficiariesContainerSpreadsheetTableSettings',
  TABLE_SETTINGS_TOGGLE: 'beneficiariesContainerSpreadsheetTableSettingsToggle',
  TABLE_SETTINGS_TOGGLE_BUTTON: 'beneficiariesContainerSpreadsheetTableSettingsToggleButton',
  TABLE_SETTINGS_POPUP: 'beneficiariesContainerSpreadsheetTableSettingsPopup',

  SELECTION: 'beneficiariesContainerSpreadsheetSelection',
  EXPORT: 'beneficiariesContainerSpreadsheetExport',
  PAGINATION: 'beneficiariesContainerSpreadsheetPagination',
};

export const FILTERS = {
  BENEFICIARY: 'benefName',
  BENEFICIARY_ACCOUNT: 'benefAccount',
  BENEFICIARY_COUNTRY_CODE: 'benefCountryCode',
  BENEFICIARY_BANK_BIC: 'benefBankSWIFT',
  BENEFICIARY_BANK_COUNTRY_CODE: 'benefBankCountryCode',
  I_MEDIA_BANK_BIC: 'iMediaBankSWIFT',
  SIGNED: 'signed'
};

export const COLUMNS = {
  BENEFICIARY: 'benefName',
  BENEFICIARY_ACCOUNT: 'benefAccount',
  BENEFICIARY_COUNTRY_CODE: 'benefCountryCode',
  BENEFICIARY_BANK_BIC: 'benefBankSWIFT',
  BENEFICIARY_BANK_COUNTRY_CODE: 'benefBankCountryCode',
  I_MEDIA_BANK_BIC: 'iMediaBankSWIFT',
  SIGNED: 'signed'
};

export const FILTER_VALUES = {
  SIGNED: ' Заверен',
  NOT_SIGNED: 'Не заверен'
};

export const VALIDATE_SETTINGS = settings =>
  !!settings &&
  !!settings.visible &&
  !!settings.sorting &&
  !!settings.pagination &&
  !!settings.width &&
  !!settings.width[COLUMNS.BENEFICIARY] &&
  !!settings.width[COLUMNS.BENEFICIARY_ACCOUNT] &&
  !!settings.width[COLUMNS.BENEFICIARY_COUNTRY_CODE] &&
  !!settings.width[COLUMNS.BENEFICIARY_BANK_BIC] &&
  !!settings.width[COLUMNS.BENEFICIARY_BANK_COUNTRY_CODE] &&
  !!settings.width[COLUMNS.I_MEDIA_BANK_BIC] &&
  !!settings.width[COLUMNS.SIGNED];
