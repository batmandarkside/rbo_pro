import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Icon from 'components/ui-components/icon';
import { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import Spreadsheet, {
  Toolbar,
  Filters,
  Operations,
  Table,
  Selection,
  Export,
  Pagination
} from 'components/ui-components/spreadsheet';
import ConfirmNotification from 'components/notification/confirm';
import { LOCATORS, COMPONENT_STYLE_SELECTOR, FILTERS, COLUMNS } from './constants';
import './style.css';

export default class BeneficiariesContainer extends Component {
  static propTypes = {
    isExportListLoading: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,

    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    location: PropTypes.object.isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,
    panelOperations: PropTypes.instanceOf(List).isRequired,
    selectedItems: PropTypes.instanceOf(List).isRequired,

    getSettingsAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getDictionaryForFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    exportListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeGroupingAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
    changeSelectAction: PropTypes.func.isRequired,
    newBeneficiaryAction: PropTypes.func.isRequired,
    routeToItemAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    operationRemoveConfirmAction: PropTypes.func.isRequired
  };

  componentWillMount() {
    this.props.getSettingsAction();
    this.props.getQueryParamsAction();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getSettingsAction();
      this.props.getQueryParamsAction();
    } else if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  filterDictionaryFormFieldOptionRenderer = ({ conditionId, option: item, highlight: inputValue }) => {
    switch (conditionId) {
      case FILTERS.BENEFICIARY_ACCOUNT:
        return (
          <RboFormFieldPopupOptionInner
            title={item && item.title}
            describe={item && item.name}
            extra={item && item.bic}
            highlight={{
              value: inputValue,
              inTitle: item && item.title
            }}
          />
        );
      case FILTERS.BENEFICIARY_COUNTRY_CODE:
        return (
          <RboFormFieldPopupOptionInner
            title={item && item.title}
            extra={item && item.code}
            describe={item && item.shortName}
            highlight={{
              value: inputValue,
              inTitle: item && item.title
            }}
          />
        );
      case FILTERS.BENEFICIARY_BANK_BIC:
        return (
          <RboFormFieldPopupOptionInner
            title={item && item.title}
            extra={item && item.country}
            describe={item && item.name}
            highlight={{
              value: inputValue,
              inTitle: item && item.title
            }}
          />
        );
      case FILTERS.BENEFICIARY_BANK_COUNTRY_CODE:
        return (
          <RboFormFieldPopupOptionInner
            title={item && item.title}
            extra={item && item.code}
            describe={item && item.shortName}
            highlight={{
              value: inputValue,
              inTitle: item && item.title
            }}
          />
        );
      case FILTERS.I_MEDIA_BANK_BIC:
        return (
          <RboFormFieldPopupOptionInner
            title={item && item.title}
            extra={item && item.country}
            describe={item && item.name}
            highlight={{
              value: inputValue,
              inTitle: item && item.title
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner
            label={item && item.label}
            highlight={{
              value: inputValue,
              inTitle: item && item.label
            }}
          />
        );
    }
  };

  renderItemBoolElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>
        <Icon
          type={value ? 'bool-true' : 'bool-false'}
          size="16"
        />
      </div>
    );
  };

  render() {
    const {
      isExportListLoading,
      isListLoading,
      columns,
      filtersConditions,
      filters,
      list,
      operations,
      pages,
      paginationOptions,
      panelOperations,
      selectedItems,
      exportListAction,
      getListAction,
      changeColumnsAction,
      changeGroupingAction,
      changeFiltersAction,
      getDictionaryForFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSelectAction,
      changeSortingAction,
      newBeneficiaryAction,
      operationAction,
      operationRemoveConfirmAction,
      resizeColumnsAction,
      routeToItemAction,
    } = this.props;

    return (
      <div className={COMPONENT_STYLE_SELECTOR} data-loc={LOCATORS.CONTAINER}>
        <ConfirmNotification onClickOK={operationRemoveConfirmAction} />
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                  newBeneficiary: LOCATORS.TOOLBAR_BUTTON_NEW_BENEFICIARY
                }
              }}
              title="Бенефициары"
              buttons={[
                {
                  id: 'refreshList',
                  isProgress: isListLoading,
                  type: 'refresh',
                  title: 'Обновить',
                  onClick: getListAction
                },
                {
                  id: 'newBeneficiary',
                  type: 'simple',
                  title: 'Создать',
                  onClick: newBeneficiaryAction
                }
              ]}
            />
            {selectedItems.size ?
              <Operations
                dataLoc={LOCATORS.OPERATIONS}
                operations={panelOperations}
                operationAction={operationAction}
                selectedItems={selectedItems}
              />
              :
              <Filters
                dataLocs={{
                  main: LOCATORS.FILTERS,
                  select: LOCATORS.FILTERS_SELECT,
                  add: LOCATORS.FILTERS_ADD,
                  remove: LOCATORS.FILTERS_REMOVE,
                  operations: LOCATORS.FILTERS_OPERATIONS,
                  operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                  operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                  operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                  operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                  conditions: LOCATORS.FILTERS_CONDITIONS,
                  condition: LOCATORS.FILTERS_CONDITION,
                  conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                  conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                  addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                  addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                  close: LOCATORS.FILTERS_CLOSE
                }}
                conditions={filtersConditions}
                filters={filters}
                changeFiltersAction={changeFiltersAction}
                dictionaryFormFieldOptionRenderer={this.filterDictionaryFormFieldOptionRenderer}
                onConditionDictionaryFormInputChange={getDictionaryForFiltersAction}
              />
            }
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                [COLUMNS.SIGNED]: this.renderItemBoolElement
              }}
              settings={{
                iconTitle: 'Настройка таблицы бенефициаров',
                changeColumnsAction,
                showGrouping: true,
                changeGroupingAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              sorting={{
                changeSortingAction
              }}
              select={{
                selectItemTitle: 'Выделить бенефициара',
                selectGroupTitle: 'Выделить группу бенефициаров',
                selectAllTitle: 'Выделить всех бенефециаров',
                changeSelectAction
              }}
              resize={{
                resizeColumnsAction
              }}
              onItemClick={routeToItemAction}
              operations={operations}
              operationAction={operationAction}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {!isListLoading && !!selectedItems.size &&
            <Selection
              dataLoc={LOCATORS.SELECTION}
              main={{
                label: 'Выбрано:',
                value: selectedItems.size
              }}
            />
            }
            {!isListLoading && !!list.size && !selectedItems.size &&
            <Export
              dataLoc={LOCATORS.EXPORT}
              label="Выгрузить список в Excel"
              onClick={exportListAction}
              isLoading={isExportListLoading}
            />
            }
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
      </div>
    );
  }
}
