import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import { mapStateToProps } from './selectors';
import * as Actions from './actions';
import saga from './sagas';
import reducer from './reducer';
import BeneficiariesContainer from './container';


const withConnect = connect(mapStateToProps, Actions);
const withReducer = injectReducer({ key: 'beneficiariesContainerReducers', reducer });
const withSaga = injectSaga({ key: 'beneficiariesContainerSaga', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(BeneficiariesContainer);
