import { fromJS, Map } from 'immutable';
import { CONDITION_FIELD_TYPE } from 'components/ui-components/spreadsheet';
import {
  ACTIONS,
  FILTERS,
  COLUMNS,
  LOCAL_ROUTER_ALIAS,
  FILTER_VALUES
} from './constants';

export const initialSections = [
  {
    id: 'beneficiaries',
    title: 'Бенефициары',
    route: LOCAL_ROUTER_ALIAS,
    selected: true
  }
];

export const initialFiltersConditions = [
  {
    id: FILTERS.BENEFICIARY,
    title: 'Наименование бенефициара',
    type: CONDITION_FIELD_TYPE.STRING,
    maxLength: 70,
    sections: ['beneficiaries']
  },
  {
    id: FILTERS.BENEFICIARY_ACCOUNT,
    title: 'Счет бенефициара',
    type: CONDITION_FIELD_TYPE.SUGGEST,
    maxLength: 34,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['beneficiaries']
  },
  {
    id: FILTERS.BENEFICIARY_COUNTRY_CODE,
    title: 'Код страны бенефициара',
    type: CONDITION_FIELD_TYPE.SEARCH,
    maxLength: 3,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['beneficiaries']
  },
  {
    id: FILTERS.BENEFICIARY_BANK_BIC,
    title: 'Бик банка бенефициара',
    type: CONDITION_FIELD_TYPE.SUGGEST,
    maxLength: 11,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['beneficiaries']
  },
  {
    id: FILTERS.BENEFICIARY_BANK_COUNTRY_CODE,
    title: 'Код страны банка бенефициара',
    type: CONDITION_FIELD_TYPE.SEARCH,
    maxLength: 3,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['beneficiaries']
  },
  {
    id: FILTERS.I_MEDIA_BANK_BIC,
    title: 'Бик банка посредника',
    type: CONDITION_FIELD_TYPE.SUGGEST,
    maxLength: 11,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['beneficiaries']
  },
  {
    id: FILTERS.SIGNED,
    title: 'Заверен',
    type: 'select',
    values: [
      FILTER_VALUES.SIGNED,
      FILTER_VALUES.NOT_SIGNED
    ],
  }
];

export const initialColumns = [
  {
    id: COLUMNS.BENEFICIARY,
    title: 'Наименование бенефициара',
    canGrouped: false
  },
  {
    id: COLUMNS.BENEFICIARY_ACCOUNT,
    title: 'Счет бенефициара',
    canGrouped: false

  },
  {
    id: COLUMNS.BENEFICIARY_COUNTRY_CODE,
    title: 'Код страны бенефициара',
    canGrouped: false
  },
  {
    id: COLUMNS.BENEFICIARY_BANK_BIC,
    title: 'Бик банка бенефициара',
    canGrouped: false
  },
  {
    id: COLUMNS.BENEFICIARY_BANK_COUNTRY_CODE,
    title: 'Код страны банка бенефициара',
    canGrouped: false
  },
  {
    id: COLUMNS.I_MEDIA_BANK_BIC,
    title: 'Бик банка посредника',
    canGrouped: false
  },
  {
    id: COLUMNS.SIGNED,
    title: 'Заверен',
    canGrouped: false
  }
];

export const initialOperations = [
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'edit',
    title: 'Редактировать',
    icon: 'edit',
    disabled: false,
    progress: false,
    grouped: false
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialSettings = {
  visible: [
    COLUMNS.BENEFICIARY,
    COLUMNS.BENEFICIARY_ACCOUNT,
    COLUMNS.BENEFICIARY_COUNTRY_CODE,
    COLUMNS.BENEFICIARY_BANK_BIC,
    COLUMNS.BENEFICIARY_BANK_COUNTRY_CODE,
    COLUMNS.I_MEDIA_BANK_BIC,
    COLUMNS.SIGNED
  ],
  sorting: { id: COLUMNS.BENEFICIARY, direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    [COLUMNS.BENEFICIARY]: 220,
    [COLUMNS.BENEFICIARY_ACCOUNT]: 92,
    [COLUMNS.BENEFICIARY_COUNTRY_CODE]: 92,
    [COLUMNS.BENEFICIARY_BANK_BIC]: 196,
    [COLUMNS.BENEFICIARY_BANK_COUNTRY_CODE]: 92,
    [COLUMNS.I_MEDIA_BANK_BIC]: 196,
    [COLUMNS.SIGNED]: 110
  }
};

export const initialState = fromJS({
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: initialFiltersConditions,
  filters: [],
  list: [],
  operations: initialOperations,
  selectedItems: []
});

export const mapSettings = (state, settings) => {
  const immutableSettings = fromJS(settings).delete('filters');
  const columns = state.get('columns');
  const width = columns.reduce((result, current) =>
    result.set(current.get('id'), immutableSettings.getIn(['width', current.get('id')])), Map()
  );

  return immutableSettings.set('width', width);
};

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings
    ? mapSettings(state, settings)
    : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters = state.get('filters').map(item => item.set('selected', false));
  if (filter) {
    if (filters.find(item => item.get('id') === filter.id)) {
      filters = filters.map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = filters.push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => ([CONDITION_FIELD_TYPE.SUGGEST, CONDITION_FIELD_TYPE.SEARCH].includes(condition.get('type'))
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const isSearching = conditionId !== 'import';
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], isSearching)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      [CONDITION_FIELD_TYPE.SUGGEST, CONDITION_FIELD_TYPE.SEARCH].includes(condition.get('type'))
        ? condition.setIn(['values', 'isSearching'], false)
        : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  selectedItems: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const idsList = fromJS(list).map(item => item.get('id'));
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!idsList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (idsList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: idsList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: idsList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([])
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

/**
 * установить операцию в статус - активна ( в прогрессе )
 * @param state
 * @param operationId
 */
export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

/**
 * оброс активной операции и опциональный релоад скроллера
 * @param state
 * @param operationId
 * @param isListUpdateRequired
 */
export const operationFinish = (state, operationId, isListUpdateRequired = false) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

export default function beneficiariesContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.BENEFICIARIES_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.BENEFICIARIES_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.BENEFICIARIES_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.BENEFICIARIES_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.BENEFICIARIES_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.BENEFICIARIES_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, action.payload);

    case ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_CONFIRM_SHOW:
      return operationRequest(state, 'remove');

    case ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationFinish(state, 'remove', true);

    case ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationFinish(state, 'remove', false);

    default:
      return state;
  }
}
