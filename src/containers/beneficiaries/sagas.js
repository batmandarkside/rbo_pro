import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction
} from 'dal/notification/actions';
// todo реализовать и раскомментировать
// import {
//   exportBeneficiariesListToExcel as dalExportBeneficiariesListToExcel,
// } from 'dal/beneficiaries/sagas';

import {
  getBeneficiars as dalGetBeneficiars,
  fetchBeneficiars as dalFetchBeneficiars,
  removeBeneficiary as dalRemoveBeneficiaries,
} from 'dal/beneficiaries/sagas';
import {
  getCountries as dalGetCountries,
  getForeignBanks as dalGetForeignBanks,
} from 'dal/dictionaries/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';

import {
  ACTIONS,
  LOCAL_ROUTER_ALIAS,
  LOCAL_REDUCER,
  SETTINGS_ALIAS,
  VALIDATE_SETTINGS,
  FILTERS,
  FILTER_VALUES
} from './constants';

const getParamsForDalOperation = ({ sections, settings, pages, filters, type }) => {
  const section = sections.find(item => item.get('selected')).get('api');
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const visibleColumns = settings.get('visible').toJS();
  const selectedFilter = filters.find(filter => filter.get('selected'));
  const conditions = selectedFilter && selectedFilter.get('conditions');
  const filter = {};

  conditions && conditions.forEach((condition) => {
    const conditionId = condition.get('id');
    switch (conditionId) {
      case FILTERS.BENEFICIARY:
      case FILTERS.BENEFICIARY_ACCOUNT:
      case FILTERS.BENEFICIARY_COUNTRY_CODE:
      case FILTERS.BENEFICIARY_BANK_BIC:
      case FILTERS.BENEFICIARY_BANK_COUNTRY_CODE:
      case FILTERS.I_MEDIA_BANK_BIC:
        filter[conditionId] = condition.get('params', null);
        break;
      case FILTERS.SIGNED:
        filter[conditionId] = condition.get('params') === FILTER_VALUES.SIGNED;
        break;
      default:
        break;
    }
  });

  const params = {
    section,
    sort,
    desc,
    ...filter
  };

  switch (type) {
    case 'list':
      params.offset = pages.get('selected') * settings.get('pagination');
      params.offsetStep = settings.get('pagination') + 1;
      break;
    case 'export':
      params.visibleColumns = visibleColumns;
      break;
    default:
      break;
  }

  return params;
};

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);

export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload: { pageIndex, filter } });
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;
  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* getDictionaryForFilters({ payload }) {
  try {
    const { conditionId, value = '' } = payload;
    let result = [];
    switch (conditionId) {
      case FILTERS.BENEFICIARY_ACCOUNT:
        // справочник БЕНЕФЕЦИАРОВ
        result = yield call(dalFetchBeneficiars, { payload: { [conditionId]: value } });
        break;
      case FILTERS.BENEFICIARY_COUNTRY_CODE:
        // справочник СТРАН
        result = yield call(dalGetCountries, { code: value });
        break;
      case FILTERS.BENEFICIARY_BANK_BIC:
        // справочник SWIFT
        result = yield call(dalGetForeignBanks, { bicInt: value });
        break;
      case FILTERS.BENEFICIARY_BANK_COUNTRY_CODE:
        // справочник СТРАН
        result = yield call(dalGetCountries, { code: value });
        break;
      case FILTERS.I_MEDIA_BANK_BIC:
        // справочник SWIFT
        result = yield call(dalGetForeignBanks, { bicInt: value });
        break;
      default:
        break;
    }
    yield put({
      type: ACTIONS.BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
      payload: {
        conditionId,
        items: result
      }
    });
  } catch (error) {
    yield put({
      type: ACTIONS.BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getList() {
  try {
    const sections = yield select(localSectionsSelector);
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ sections, settings, pages, filters, type: 'list' });
    const result = yield call(dalGetBeneficiars, { payload: params });

    yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_GET_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.BENEFICIARIES_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* exportList() {
  try {
    // const sections = yield select(localSectionsSelector);
    // const settings = yield select(localSettingsSelector);
    // const filters = yield select(localFiltersSelector);
    // const params = getParamsForDalOperation({ sections, settings, pages: null, filters, type: 'export' });
    // todo реализовать запрос на экспорт
    // const result = yield call(dalExportBeneficiariesListToExcel, { payload: params });
    yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_EXPORT_LIST_SUCCESS/* , payload: result */ });
  } catch (error) {
    yield put({
      type: ACTIONS.BENEFICIARIES_CONTAINER_EXPORT_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* newBeneficiary(action) {
  const { payload } = action;
  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/new`,
    search: `?${qs.stringify(payload)}`
  }));
}

export function* routeToItem(action) {
  const { payload } = action;
  yield put(push(`${LOCAL_ROUTER_ALIAS}/${payload}`));
}


export function* operation(action) {
  const { payload: { operationId, items } } = action;

  switch (operationId) {
    case 'remove':
      yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_CONFIRM_SHOW, payload: items });
      break;
    case 'edit':
      yield put({
        type: ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_EDIT,
        payload: items[0]
      });
      break;
    default:
      break;
  }
}

export function* removeOperation({ payload }) {
  const many = payload.length > 1;

  try {
    yield call(dalRemoveBeneficiaries, { payload });
    yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_SUCCESS });
    yield put(
      dalAddOrdinaryNotificationAction({
        type: 'success',
        title: `Бенефициар${many ? 'ы' : ''} успешно удален${many ? 'ы' : ''}`
      })
    );
  } catch (error) {
    yield put({ type: ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_FAIL });
    yield put(
      dalAddOrdinaryNotificationAction({
        type: 'error',
        title: 'Произошла ошибка на сервере'
      })
    );
  }
}


export function* removeConfirm(action) {
  const { payload } = action;
  const many = payload.length > 1;
  yield put(dalAddOrdinaryNotificationAction({
    type: 'confirm',
    level: 'error',
    message: `Вы действительно хотите удалить бенефициар${many ? 'ов' : 'а'}?`,
    success: {
      label: 'Подтвердить',
      action: {
        type: ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_REQUEST,
        payload
      }
    },
    cancel: {
      label: 'Отмена',
      action: {
        type: ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_CONFIRM_CANCEL
      }
    }
  }));
}


export function* successNotification(action) {
  const { payload } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'success', title: payload }));
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}

export default function* saga() {
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST, getDictionaryForFilters);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_EXPORT_LIST_REQUEST, exportList);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_EXPORT_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_GROUPING, changeSettingsColumns);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_NEW_BENEFICIARY, newBeneficiary);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_ROUTE_TO_ITEM, routeToItem);

  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_OPERATION, operation);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_REQUEST, removeOperation);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_REMOVE_CONFIRM_SHOW, removeConfirm);
  yield takeLatest(ACTIONS.BENEFICIARIES_CONTAINER_OPERATION_EDIT, routeToItem);
}
