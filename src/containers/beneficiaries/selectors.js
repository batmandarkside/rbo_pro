import { createSelector } from 'reselect';
import { List, Map, OrderedMap } from 'immutable';
import { CONDITION_FIELD_TYPE } from 'components/ui-components/spreadsheet';
import { LOCAL_REDUCER, COLUMNS, FILTERS } from './constants';

const isExportListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isExportListLoading']);
const isListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isListLoading']);
const isListUpdateRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isListUpdateRequired']);
const isReloadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isReloadRequired']);

const columnsSelector = state => state.getIn([LOCAL_REDUCER, 'columns']);
const dalListSelector = state => state.getIn(['beneficiaries', 'list']);
const filtersConditionsSelector = state => state.getIn([LOCAL_REDUCER, 'filtersConditions']);
const filtersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
const listSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);
const selectedItemsSelector = state => state.getIn([LOCAL_REDUCER, 'selectedItems']);
const settingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
const pagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
const paginationOptionsSelector = state => state.getIn([LOCAL_REDUCER, 'paginationOptions']);

const mapListItemOperations = (allowedSmActions, operations) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'remove':
          return allowedSmActions.get('delete');
        case 'edit':
          return allowedSmActions.get('delete');
        default:
          return false;
      }
    })), OrderedMap()
  ).toJS()
);

const mapListItem = (item, selected, operations) => Map({
  id: item.get('id'),
  [COLUMNS.BENEFICIARY]: item.getIn(['benefInfo', COLUMNS.BENEFICIARY]),
  [COLUMNS.BENEFICIARY_ACCOUNT]: item.getIn(['benefInfo', COLUMNS.BENEFICIARY_ACCOUNT]),
  [COLUMNS.BENEFICIARY_COUNTRY_CODE]: item.getIn(['benefInfo', COLUMNS.BENEFICIARY_COUNTRY_CODE]),
  [COLUMNS.BENEFICIARY_BANK_BIC]: item.getIn(['beBankInfo', COLUMNS.BENEFICIARY_BANK_BIC]),
  [COLUMNS.BENEFICIARY_BANK_COUNTRY_CODE]: item.getIn(['beBankInfo', COLUMNS.BENEFICIARY_BANK_COUNTRY_CODE]),
  [COLUMNS.I_MEDIA_BANK_BIC]: item.getIn(['iMeBankInfo', COLUMNS.I_MEDIA_BANK_BIC]),
  [COLUMNS.SIGNED]: item.get(COLUMNS.SIGNED),

  selected,
  operations: mapListItemOperations(item.get('allowedSmActions'), operations)
});

const mapFilterConditionDictionaryItems = condition =>
  condition.setIn(
    ['values', 'items'],
    condition.getIn(['values', 'items']).map((item) => {
      const conditionId = condition.get('id');
      switch (conditionId) {
        case FILTERS.BENEFICIARY_ACCOUNT:
          return item.get('benefInfo').merge({
            [conditionId]: item.getIn(['benefInfo', 'benefAccount']),
            label: item.getIn(['benefInfo', 'benefAccount']),
            bic: item.getIn(['beBankInfo', 'benefBankSWIFT']),
            name: item.getIn(['benefInfo', 'benefName']),
            title: item.getIn(['benefInfo', 'benefAccount']),
            value: item.get('id')
          });
        case FILTERS.BENEFICIARY_COUNTRY_CODE:
          return item.merge({
            [conditionId]: item.get('code'),
            label: item.get('code'),
            code: item.get('mnem02'),
            shortName: item.get('shortName'),
            title: item.get('code'),
            value: item.get('id')
          });
        case FILTERS.BENEFICIARY_BANK_BIC:
          return item.merge({
            [conditionId]: item.get('bicInt'),
            label: item.get('bicInt'),
            country: item.get('countryName'),
            name: item.get('name'),
            title: item.get('bicInt'),
            value: item.get('id')
          });
        case FILTERS.BENEFICIARY_BANK_COUNTRY_CODE:
          return item.merge({
            [conditionId]: item.get('code'),
            label: item.get('code'),
            code: item.get('mnem02'),
            shortName: item.get('shortName'),
            title: item.get('code'),
            value: item.get('id')
          });
        case FILTERS.I_MEDIA_BANK_BIC:
          return item.merge({
            [conditionId]: item.get('bicInt'),
            label: item.get('bicInt'),
            country: item.get('countryName'),
            name: item.get('name'),
            title: item.get('bicInt'),
            value: item.get('id')
          });
        default:
          return null;
      }
    })
  );

const mapFilterConditionParamsToValue = (params, condition) => {
  if (!params) return '...';
  switch (condition.get('id')) {
    case FILTERS.BENEFICIARY:
    case FILTERS.SIGNED:
    case FILTERS.BENEFICIARY_ACCOUNT:
    case FILTERS.BENEFICIARY_COUNTRY_CODE:
    case FILTERS.BENEFICIARY_BANK_BIC:
    case FILTERS.BENEFICIARY_BANK_COUNTRY_CODE:
    case FILTERS.I_MEDIA_BANK_BIC:
      return params;
    default:
      return null;
  }
};

const mapFilterConditions = (conditions, savedConditions, filtersConditions) => (
  (conditions && conditions.size ? conditions : savedConditions).map(condition =>
    condition.merge({
      params: condition.get('params'),
      value: mapFilterConditionParamsToValue(
        condition.get('params'),
        filtersConditions.find(filtersCondition => filtersCondition.get('id') === condition.get('id'))
      )
    })
  )
);

export const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) => columns.map(item => item.merge({
    visible: !!settings.get('visible').find(visible => visible === item.get('id')),
    sorting: settings.getIn(['sorting', 'id']) === item.get('id') ? settings.getIn(['sorting', 'direction']) : 0,
    grouped: settings.get('grouped') === item.get('id'),
    width: settings.getIn(['width', item.get('id')])
  }))
);

export const filtersConditionsCreatedSelector = createSelector(
  filtersConditionsSelector,
  filtersConditions => filtersConditions.map(
    condition => ([CONDITION_FIELD_TYPE.SUGGEST, CONDITION_FIELD_TYPE.SEARCH].includes(condition.get('type'))
      ? mapFilterConditionDictionaryItems(condition)
      : condition
    )
  )
);

export const filtersCreatedSelector = createSelector(
  filtersSelector,
  filtersConditionsSelector,
  (filters, filtersConditions) => filters
    .map(filter => filter.merge({
      conditions: mapFilterConditions(filter.get('conditions'), filter.get('savedConditions'), filtersConditions)
    }))
    .sort((a, b) => {
      if (a.get('isPreset') && b.get('isPreset')) return 0;
      if (a.get('isPreset') && !b.get('isPreset')) return -1;
      if (!a.get('isPreset') && b.get('isPreset')) return 1;
      return a.get('title').localeCompare(b.get('title'));
    })
);

export const listCreatedSelector = createSelector(
  listSelector,
  selectedItemsSelector,
  dalListSelector,
  settingsSelector,
  operationsSelector,
  state => state,
  (list, selectedItems, dalList, settings, operations) => {
    if (!list.size) return list;
    const mappedList = list.map(item =>
      mapListItem(
        dalList.find(dalListItem => dalListItem.get('id') === item),
        !!selectedItems.filter(selectedItem => selectedItem === item).size,
        operations
      ));
    const grouped = settings.get('grouped');
    if (!grouped) {
      return List([Map({
        id: 'notGrouped',
        selected: mappedList.filter(item => item.get('selected')).size === mappedList.size,
        items: mappedList
      })]);
    }
    return mappedList.map(item => item.get(grouped)).toOrderedSet().toList().map((group, i) => {
      const items = mappedList.filter(item => item.get(grouped) === group);
      return Map({
        id: `${grouped}_${i}`,
        title: group,
        selected: items.filter(item => item.get('selected')).size === items.size,
        items
      });
    });
  }
);

export const paginationOptionsCreatedSelector = createSelector(
  paginationOptionsSelector,
  settingsSelector,
  (paginationOptions, settings) => paginationOptions.map(item => ({
    value: item,
    title: item,
    selected: settings.get('pagination') === item
  }))
);

export const panelOperationsCreatedSelector = createSelector(
  operationsSelector,
  selectedItemsSelector,
  dalListSelector,
  (operations, selectedItems, list) => operations.filter((operation) => {
    const itemsWithOperations = selectedItems.map(item => Map({
      id: item,
      operations: mapListItemOperations(
        list.find(listItem => listItem.get('id') === item).get('allowedSmActions'),
        operations
      )
    }));
    return (
      itemsWithOperations.size ===
      itemsWithOperations.filter(item => !!item.get('operations')[operation.get('id')]).size
      ) &&
      (selectedItems.size === 1 || operation.get('grouped'));
  })
);

export const mapStateToProps = state => ({
  isListLoading: isListLoadingSelector(state),
  isExportListLoading: isExportListLoadingSelector(state),
  isListUpdateRequired: isListUpdateRequiredSelector(state),
  isReloadRequired: isReloadRequiredSelector(state),
  columns: columnsCreatedSelector(state),
  filtersConditions: filtersConditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  list: listCreatedSelector(state),
  operations: operationsSelector(state),
  pages: pagesSelector(state),
  paginationOptions: paginationOptionsCreatedSelector(state),
  panelOperations: panelOperationsCreatedSelector(state),
  selectedItems: selectedItemsSelector(state),
  settings: settingsSelector(state),
});
