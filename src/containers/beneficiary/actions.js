import { ACTIONS } from './constants';

/**
 *    action загрузки контейнера
 */
export const mountAction = params => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_MOUNT,
  payload: { params }
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_UNMOUNT
});

/**
 *    action переключения табов
 *    @param {string} tabId
 */
export const changeTabAction = tabId => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_CHANGE_TAB,
  payload: { tabId }
});

/**
 *    action focus на поле формы
 *    @param {string} fieldId
 */
export const fieldFocusAction = fieldId => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_FIELD_FOCUS,
  payload: { fieldId }
});

/**
 *    action blur на поле формы
 *    @param {string} fieldId
 */
export const fieldBlurAction = fieldId => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_FIELD_BLUR,
  payload: { fieldId }
});

/**
 *    action изменения данных
 *    @param {string} fieldId
 *    @param {string} fieldValue
 *    @param {string} dictionaryValue
 */
export const changeDataAction = (fieldId, fieldValue, dictionaryValue) => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_CHANGE_DATA,
  payload: { fieldId, fieldValue, dictionaryValue }
});

/**
 *    action поиска по справочникам
 *    @param {string} dictionaryId
 *    @param {string} dictionarySearchValue
 */
export const dictionarySearchAction = (dictionaryId, dictionarySearchValue) => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH,
  payload: { dictionaryId, dictionarySearchValue }
});

/**
 *    action сворачивания / разворачивания секции формы
 *    @param {string} sectionId
 *    @param {boolean} isCollapsed
 */
export const sectionCollapseToggleAction = (sectionId, isCollapsed) => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_SECTION_COLLAPSE_TOGGLE,
  payload: { sectionId, isCollapsed }
});

/**
 *    action выполнения операции
 *    @param {string} operationId
 */
export const operationAction = operationId => ({
  type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION,
  payload: { operationId }
});
