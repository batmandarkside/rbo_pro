import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Loader from '@rbo/components/lib/loader/Loader';
import { getDocStatusClassNameMode, getScrollTopByDataId, smoothScrollTo } from 'utils';
import RboDocument, {
  RboDocumentPanel,
  RboDocumentOperations,
  RboDocumentContent,
  RboDocumentMain,
  RboDocumentHead,
  RboDocumentHeadStatus,
  RboDocumentHeadTabs,
  RboDocumentBody,
  RboDocumentExtra,
  RboDocumentExtraSection,
  RboDocumentStructure,
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import RboForm, { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import { getFieldElementSelectorByIds } from 'components/ui-components/rbo-form-compact/utils';
import {
  SectionPrimary,
  SectionPurpose,
  SectionBeneficiaryBank,
  SectionIntermediaryBank,
  SectionInfo
} from './sections';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';

export default class BeneficiaryContainer extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),

    isDataError: PropTypes.bool,
    isDataLoading: PropTypes.bool,
    isEditable: PropTypes.bool.isRequired,

    documentTabs: PropTypes.instanceOf(List).isRequired,
    documentStructure: PropTypes.instanceOf(List).isRequired,
    documentFieldsIsDisables: PropTypes.instanceOf(Map).isRequired,
    documentData: PropTypes.instanceOf(Map).isRequired,
    documentDictionaries: PropTypes.instanceOf(Map).isRequired,
    formSections: PropTypes.instanceOf(Map).isRequired,
    formWarnings: PropTypes.instanceOf(Map).isRequired,
    formErrors: PropTypes.instanceOf(Map).isRequired,
    operations: PropTypes.instanceOf(List).isRequired,

    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    changeTabAction: PropTypes.func.isRequired,
    fieldFocusAction: PropTypes.func.isRequired,
    fieldBlurAction: PropTypes.func.isRequired,
    changeDataAction: PropTypes.func.isRequired,
    dictionarySearchAction: PropTypes.func.isRequired,
    sectionCollapseToggleAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    getConstraint: PropTypes.func.isRequired
  };

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.mountAction(params);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search)
        && nextProps.history.action !== 'REPLACE') {
      this.props.mountAction(nextProps.match.params);
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  handleTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleFormSectionCollapseToggle = ({ sectionId, isCollapsed }) => {
    this.props.sectionCollapseToggleAction(sectionId, isCollapsed);
  };

  handleSearch = ({ fieldId, fieldSearchValue }) => {
    this.props.dictionarySearchAction(fieldId, fieldSearchValue);
  };

  handleFocus = ({ fieldId }) => {
    this.props.fieldFocusAction(fieldId);
  };

  handleBlur = ({ fieldId }) => {
    this.props.fieldBlurAction(fieldId);
  };

  handleChange = ({ fieldId, fieldValue, dictionaryValue }) => {
    this.props.changeDataAction(fieldId, fieldValue, dictionaryValue);
  };

  handleStructureTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleStructureSectionClick = ({ tabId, sectionId }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    setTimeout(() => {
      const fieldId = parentTab.get('sections').find(section => section.get('id') === sectionId).get('firstFieldId');
      smoothScrollTo(
        getScrollTopByDataId(sectionId, DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR),
        () => {
          document.querySelector(`#${fieldId}`) &&
          setTimeout(() => { document.querySelector(`#${fieldId}`).focus(); }, 0);
        }
      );
    }, 0);
  };

  handleStructureErrorClick = ({ tabId, fieldsIds }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    const fieldElementSelector = getFieldElementSelectorByIds(fieldsIds);
    fieldElementSelector && setTimeout(() => { fieldElementSelector.focus(); }, 0);
  };

  formFieldOptionRenderer = (fieldId, option, highlight) => (
    <RboFormFieldPopupOptionInner
      title={option && option.title}
      describe={option && option.describe}
      extra={option && option.subtitle}
      highlight={{
        value: highlight,
        inTitle: option && option.title
      }}
    />
  );

  render() {
    const {
      isDataError,
      isDataLoading,
      isEditable,
      documentTabs,
      documentFieldsIsDisables,
      documentData,
      documentStructure,
      documentDictionaries,
      formSections,
      formWarnings,
      formErrors,
      operations,
      operationAction,
      getConstraint
    } = this.props;

    const sectionsProps = {
      isEditable,
      documentFieldsIsDisables,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formFieldOptionRenderer: this.formFieldOptionRenderer,
      onCollapseToggle: this.handleFormSectionCollapseToggle,
      onFieldFocus: this.handleFocus,
      onFieldBlur: this.handleBlur,
      onFieldChange: this.handleChange,
      onFieldSearch: this.handleSearch,
      getConstraint
    };

    return (
      <div className={COMPONENT_STYLE_NAME}>
        <div className={`${COMPONENT_STYLE_NAME}__inner`}>
          {isDataLoading && <Loader centered />}
          {!isDataLoading && !isDataError &&
          <RboDocument>
            <RboDocumentPanel>
              <RboDocumentOperations
                operations={operations}
                operationAction={operationAction}
              />
            </RboDocumentPanel>
            <RboDocumentContent data-id="">
              <RboDocumentMain>
                <RboDocumentHead title="Бенефициар">
                  <RboDocumentHeadStatus
                    date={documentData.get('docDate')}
                    statusClassName={getDocStatusClassNameMode(documentData.get('status'))}
                    status={documentData.get('status')}
                  />
                  <RboDocumentHeadTabs
                    tabs={documentTabs}
                    onTabClick={this.handleTabClick}
                  />
                </RboDocumentHead>
                <RboDocumentBody>
                  <RboForm initialFocusFieldId="receiverINN">
                    {formSections.getIn(['primary', 'isVisible']) &&
                    <SectionPrimary
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['primary', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['primary', 'isCollapsed'])}
                      title={formSections.getIn(['primary', 'title'])}
                    />
                    }
                    {formSections.getIn(['purpose', 'isVisible']) &&
                    <SectionPurpose
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['purpose', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['purpose', 'isCollapsed'])}
                      title={formSections.getIn(['purpose', 'title'])}
                    />
                    }
                    {formSections.getIn(['beneficiaryBank', 'isVisible']) &&
                    <SectionBeneficiaryBank
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['beneficiaryBank', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['beneficiaryBank', 'isCollapsed'])}
                      title={formSections.getIn(['beneficiaryBank', 'title'])}
                    />
                    }
                    {formSections.getIn(['intermediaryBank', 'isVisible']) &&
                    <SectionIntermediaryBank
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['intermediaryBank', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['intermediaryBank', 'isCollapsed'])}
                      title={formSections.getIn(['intermediaryBank', 'title'])}
                    />
                    }
                    {formSections.getIn(['info', 'isVisible']) &&
                    <SectionInfo
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['info', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['info', 'isCollapsed'])}
                      title={formSections.getIn(['info', 'title'])}
                    />
                    }
                  </RboForm>
                </RboDocumentBody>
              </RboDocumentMain>
              <RboDocumentExtra>
                <RboDocumentExtraSection title="Структура документа">
                  <RboDocumentStructure
                    structure={documentStructure}
                    onTabClick={this.handleStructureTabClick}
                    onSectionClick={this.handleStructureSectionClick}
                    onErrorClick={this.handleStructureErrorClick}
                    isErrorClickable={isEditable}
                  />
                </RboDocumentExtraSection>
              </RboDocumentExtra>
            </RboDocumentContent>
          </RboDocument>
          }
        </div>
      </div>
    );
  }
}
