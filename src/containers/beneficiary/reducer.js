import { fromJS } from 'immutable';
import { DOC_STATUSES } from 'constants';
import { getIsDocEditable } from 'utils';
import { ACTIONS, FIELDS } from './constants';

export const initialDocumentTabs = [
  {
    id: 'main',
    title: 'Основные поля',
    isActive: true
  }
];

export const initialFormSections = {
  primary: {
    title: 'Общая информация',
    isVisible: true,
    isCollapsible: false,
    order: 1
  },
  purpose: {
    title: 'Адрес, назначение платежа',
    isVisible: true,
    isCollapsible: false,
    isCollapsed: false,
    order: 2
  },
  beneficiaryBank: {
    title: '57: Банк бенефициара',
    isVisible: true,
    isCollapsible: false,
    isCollapsed: false,
    order: 3
  },
  intermediaryBank: {
    title: '56: Банк-посредник',
    isVisible: true,
    isCollapsible: false,
    isCollapsed: false,
    order: 4
  },
  info: {
    title: 'Дополнительная информация',
    isVisible: true,
    isCollapsible: false,
    order: 5
  }
};

export const initialDocumentDictionaries = {
  benefSWIFT: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  benefCountryCode02: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  paymentDetails: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  benefBankSWIFT: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  beBankClearCodeShort: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  benefBankCountryCode02: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  iMediaBankSWIFT: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  iMeBankClearCodeShort: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  iMediaBankCountryCode02: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  comissionTypes: {
    items: [],
    searchValue: '',
    isSearching: false
  }
};

export const initialOperations = [
  {
    id: 'back',
    title: 'Назад',
    icon: 'back',
    disabled: false,
    progress: false
  },
  {
    id: 'save',
    title: 'Сохранить',
    icon: 'save',
    disabled: false,
    progress: false
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false
  }
];

export const initialState = fromJS({
  isMounted: false,
  isDataError: false,
  isDataLoading: true,
  isEditable: false,
  documentTabs: initialDocumentTabs,
  documentData: {},
  documentErrors: [],
  documentDictionaries: initialDocumentDictionaries,
  formSections: initialFormSections,
  activeFormField: null,
  operations: initialOperations,
  constraints: {}
});

export const mount = () => initialState.set('isMounted', true);
export const unmount = () => initialState.set('isMounted', false);
export const getDataRequest = state => state.set('isDataLoading', true);

export const getDataSuccess = (state, { documentData, documentErrors, documentDictionaries }) => {
  const documentDataImmutable = fromJS(documentData);
  const documentErrorsImmutable = fromJS(documentErrors).get('errors', fromJS([]));
  const documentDictionariesImmutable = fromJS(documentDictionaries);
  const status = documentDataImmutable.get('status', DOC_STATUSES.NEW);
  const isEditable = getIsDocEditable(status, false);

  return state.merge({
    isDataLoading: false,
    isEditable,
    documentData: documentDataImmutable.merge({
      status,

      signed: documentDataImmutable.get('signed'),
      [FIELDS.BENEF_NAME]: documentDataImmutable.getIn(['benefInfo', 'benefName'], ''),
      [FIELDS.BENEF_ACCOUNT]: documentDataImmutable.getIn(['benefInfo', 'benefAccount'], ''),
      [FIELDS.BENEF_SWIFT]: documentDataImmutable.getIn(['benefInfo', 'benefSWIFT'], ''),

      [FIELDS.BENEF_ADDRESS]: documentDataImmutable.getIn(['benefInfo', 'benefAddress'], ''),
      [FIELDS.BENEF_PLACE]: documentDataImmutable.getIn(['benefInfo', 'benefPlace'], ''),
      [FIELDS.BENEF_COUNTRY_CODE]: documentDataImmutable.getIn(['benefInfo', 'benefCountryCode'], ''),
      [FIELDS.BENEF_COUNTRY_CODE_02]: documentDataImmutable.getIn(['benefInfo', 'benefCountryCode02'], ''),
      [FIELDS.BENEF_COUNTRY_NAME]: documentDataImmutable.getIn(['benefInfo', 'benefCountryName'], ''),
      [FIELDS.PAYMENT_DETAILS]: documentDataImmutable.getIn(['benefInfo', 'paymentDetails'], ''),

      [FIELDS.BENEF_BANK_SWIFT]: documentDataImmutable.getIn(['beBankInfo', 'benefBankSWIFT'], ''),
      [FIELDS.BE_BANK_CLEAR_CODE]: documentDataImmutable.getIn(['beBankInfo', 'beBankClearCode'], ''),
      [FIELDS.BE_BANK_CLEAR_CODE_SHORT]: documentDataImmutable.getIn(['beBankInfo', 'beBankClearCodeShort'], ''),
      [FIELDS.BE_BANK_CLEAR_CODE_02]: documentDataImmutable.getIn(['beBankInfo', 'beBankClearCode02'], ''),
      [FIELDS.BENEF_BANK_NAME]: documentDataImmutable.getIn(['beBankInfo', 'benefBankName'], ''),
      [FIELDS.BENEF_BANK_CORR_ACCOUNT]: documentDataImmutable.getIn(['beBankInfo', 'benefBankCorrAccount'], ''),
      [FIELDS.BENEF_BANK_ADDRESS]: documentDataImmutable.getIn(['beBankInfo', 'benefBankAddress'], ''),
      [FIELDS.BENEF_BANK_PLACE]: documentDataImmutable.getIn(['beBankInfo', 'benefBankPlace'], ''),
      [FIELDS.BENEF_BANK_COUNTRY_CODE]: documentDataImmutable.getIn(['beBankInfo', 'benefBankCountryCode'], ''),
      [FIELDS.BENEF_BANK_COUNTRY_CODE_02]: documentDataImmutable.getIn(['beBankInfo', 'benefBankCountryCode02'], ''),
      [FIELDS.BENEF_BANK_COUNTRY_NAME]: documentDataImmutable.getIn(['beBankInfo', 'benefBankCountryName'], ''),

      [FIELDS.I_MEDIA_BANK_SWIFT]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaBankSWIFT'], ''),
      [FIELDS.I_ME_BANK_CLEAR_CODE_SHORT]: documentDataImmutable.getIn(['iMeBankInfo', 'iMeBankClearCodeShort'], ''),
      [FIELDS.I_ME_BANK_CLEAR_CODE_02]: documentDataImmutable.getIn(['iMeBankInfo', 'iMeBankClearCode02'], ''),
      [FIELDS.I_ME_BANK_CLEAR_CODE]: documentDataImmutable.getIn(['iMeBankInfo', 'iMeBankClearCode'], ''),
      [FIELDS.I_ME_BANK_CLEAR_COUNTRY_CODE_02]: documentDataImmutable.getIn(['iMeBankInfo', 'iMeBankClearCountryCode02'], ''),
      [FIELDS.I_MEDIA_BANK_NAME]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaBankName'], ''),
      [FIELDS.I_MEDIA_BANK_ADDRESS]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaBankAddress'], ''),
      [FIELDS.I_MEDIA_BANK_PLACE]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaBankPlace'], ''),
      [FIELDS.I_MEDIA_BANK_COUNTRY_CODE]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaBankCountryCode'], ''),
      [FIELDS.I_MEDIA_BANK_COUNTRY_CODE_02]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaBankCountryCode02'], ''),
      [FIELDS.I_MEDIA_BANK_COUNTRY_NAME]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaBankCountryName'], ''),

      [FIELDS.CHARGES_TYPE]: documentDataImmutable.get('chargesType', ''),
      [FIELDS.CHARGES_TYPE_INFO]: documentDataImmutable.get('chargesTypeInfo', ''),
      [FIELDS.COMMENTS]: documentDataImmutable.getIn(['iMeBankInfo', 'iMediaInfo'])
    }),
    documentErrors: documentErrorsImmutable,
    documentDictionaries: documentDictionariesImmutable
  });
};

export const getDataFail = state => state.merge({
  isDataError: true,
  isDataLoading: false
});

export const saveDataSuccess = (state, { recordID, status, allowedSmActions, errors }) => state.merge({
  documentData: state.get('documentData').merge({
    id: recordID,
    status,
    allowedSmActions
  }),
  documentErrors: errors
});

export const saveDataFail = (state, { error }) => state.merge({
  documentErrors: error.name === 'RBO Controls Error' ? error.stack : state.get('documentErrors')
});

export const fieldFocus = (state, { fieldId }) => state.set('activeFormField', fieldId);
export const fieldBlur = state => state.set('activeFormField', null);

export const sectionCollapseToggle = (state, { sectionId, isCollapsed }) =>
  state.setIn(['formSections', sectionId, 'isCollapsed'], isCollapsed);

export const changeFieldsValue = (state, documentData) => state.mergeDeep({
  documentData: {
    ...documentData
  }
});

export const dictionarySearchRequest = (state, { dictionaryId, dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', dictionaryId, 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', dictionaryId, 'isSearching'], true);

export const dictionarySearchSuccess = (state, { dictionaryId, items }) =>
  state.setIn(['documentDictionaries', dictionaryId, 'items'], fromJS(items))
    .setIn(['documentDictionaries', dictionaryId, 'isSearching'], false);

export const dictionarySearchFail = (state, { dictionaryId }) =>
  state.setIn(['documentDictionaries', dictionaryId, 'isSearching'], false);

export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case operationId:
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationFinish = (state, operationId) => state.mergeDeep({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case operationId:
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationRemoveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  documentErrors: fromJS([]),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

function BeneficiaryContainerReducer(state = initialState, action) {
  const isMounted = state.get('isMounted');

  switch (action.type) {
    case ACTIONS.BENEFICIARY_CONTAINER_MOUNT:
      return mount();

    case ACTIONS.BENEFICIARY_CONTAINER_UNMOUNT:
      return unmount();

    case ACTIONS.BENEFICIARY_CONTAINER_GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.BENEFICIARY_CONTAINER_GET_DATA_SUCCESS:
      return isMounted ? getDataSuccess(state, action.payload) : state;

    case ACTIONS.BENEFICIARY_CONTAINER_GET_DATA_FAIL:
      return isMounted ? getDataFail(state) : state;

    case ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_SUCCESS:
      return saveDataSuccess(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_FAIL:
      return saveDataFail(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_FIELD_FOCUS:
      return fieldFocus(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_FIELD_BLUR:
      return fieldBlur(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_SECTION_COLLAPSE_TOGGLE:
      return sectionCollapseToggle(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_CHANGE_FIELDS_VALUE:
      return changeFieldsValue(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH_REQUEST:
      return dictionarySearchRequest(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH_SUCCESS:
      return isMounted ? dictionarySearchSuccess(state, action.payload) : state;

    case ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH_FAIL:
      return isMounted ? dictionarySearchFail(state, action.payload) : state;

    case ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_REQUEST:
      return operationRequest(state, 'save');

    case ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_SUCCESS:
    case ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_FAIL:
      return operationFinish(state, 'save');

    case ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_REQUEST:
      return operationRequest(state, 'remove');

    case ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationRemoveSuccess(state, action.payload);

    case ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_CANCEL:
    case ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationFinish(state, 'remove');

    default:
      return state;
  }
}

export default BeneficiaryContainerReducer;
