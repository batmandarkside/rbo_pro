import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { goBack, replace } from 'react-router-redux';
import { Map, fromJS } from 'immutable';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import {
  getCountries as dalGetCountries,
  getForeignBanks as dalGetForeignBanks,
  getPaymentPurposes as dalGetPaymentPurposes,
  getClearingCodes as dalGetClearingCodes,
  getComissionTypes as dalGetComissionTypes,
} from 'dal/dictionaries/sagas';
import {
  getBeneficiaryById as dalGetData,
  validateBeneficiary as dalGetErrors,
  saveBeneficiary as dalSaveData,
  removeBeneficiary as dalOperationRemove
} from 'dal/beneficiaries/sagas';
import {
  fetchConstraints as dalFetchConstraints
} from 'dal/curtransfers/sagas';
import { DOC_STATUSES } from 'constants';
import { getIsDocNeedGetErrors } from 'utils';
import { ACTIONS, LOCAL_REDUCER, LOCAL_ROUTER_ALIASES, FIELDS } from './constants';
import { normalizeDataToREST } from './utils';

export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
export const docIdSelector = state => state.getIn([LOCAL_REDUCER, 'documentData', 'id']);

export function* successNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'success',
    title: title || 'Операция выполнена успешно',
    message: message || ''
  }));
}

export function* warningNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'warning',
    title: title || '',
    message: message || ''
  }));
}

export function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

export function* confirmNotification(params) {
  const { level, title, message, success, cancel } = params;
  yield put(dalNotificationAction({
    type: 'confirm',
    level: level || 'success',
    title: title || '',
    message: message || '',
    success,
    cancel
  }));
}

export function getDictionaryById(dictionaryId) {
  switch (dictionaryId) {
    case 'benefSWIFT':
      return bicInt => dalGetForeignBanks({ bicInt });
    case 'benefCountryCode02':
      return every => dalGetCountries({ every });
    case 'paymentDetails':
      return purpose => dalGetPaymentPurposes({ purpose });
    case 'benefBankSWIFT':
      return bicInt => dalGetForeignBanks({ bicInt });
    case 'beBankClearCodeShort':
      return every => dalGetClearingCodes({ every });
    case 'benefBankCountryCode02':
      return every => dalGetCountries({ every });
    case 'iMediaBankSWIFT':
      return bicInt => dalGetForeignBanks({ bicInt });
    case 'iMeBankClearCodeShort':
      return every => dalGetClearingCodes({ every });
    case 'iMediaBankCountryCode02':
      return every => dalGetCountries({ every });
    default:
      return _ => _;
  }
}

export function* refreshData() {
  const id = yield select(docIdSelector);
  try {
    return yield call(dalGetData, { payload: id });
  } catch (error) {
    throw error;
  }
}

export function* mountSaga(action) {
  const { payload: { params: { id } } } = action;
  yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_GET_DATA_REQUEST, payload: { id } });
}

export function* getDataSaga(action) {
  const { payload: { id } } = action;
  try {
    const documentData = id === 'new'
      ? { status: DOC_STATUSES.NEW }
      : yield call(dalGetData, { payload: id });

    const [
      benefSWIFT,
      benefCountryCode02,
      paymentDetails,
      benefBankSWIFT,
      beBankClearCodeShort,
      benefBankCountryCode02,
      iMediaBankSWIFT,
      iMeBankClearCodeShort,
      iMediaBankCountryCode02,
      comissionTypes,
      constraints
    ] = yield all([
      dalGetForeignBanks({ bicInt: '' }),
      dalGetCountries({ code: '' }),
      dalGetPaymentPurposes({ purpose: '' }),
      dalGetForeignBanks({ bicInt: '' }),
      dalGetClearingCodes({ shortName: '' }),
      dalGetCountries({ code: '' }),
      dalGetForeignBanks({ bicInt: '' }),
      dalGetClearingCodes({ shortName: '' }),
      dalGetCountries({ code: '' }),
      dalGetComissionTypes(),
      dalFetchConstraints()
    ]);

    documentData.status = documentData.status || (documentData.signed ? DOC_STATUSES.CERTIFIED : DOC_STATUSES.CREATED);

    const documentErrors = (getIsDocNeedGetErrors(documentData.status))
      ? yield call(dalGetErrors, { payload: {
        id,
        ...normalizeDataToREST(fromJS(documentData))
      }
      })
      : {};

    yield put({
      type: ACTIONS.BENEFICIARY_CONTAINER_GET_DATA_SUCCESS,
      payload: {
        documentData,
        documentErrors,
        constraints,
        documentDictionaries: {
          benefSWIFT: {
            items: benefSWIFT
          },
          benefCountryCode02: {
            items: benefCountryCode02
          },
          paymentDetails: {
            items: paymentDetails
          },
          benefBankSWIFT: {
            items: benefBankSWIFT
          },
          beBankClearCodeShort: {
            items: beBankClearCodeShort
          },
          benefBankCountryCode02: {
            items: benefBankCountryCode02
          },
          iMediaBankSWIFT: {
            items: iMediaBankSWIFT
          },
          iMeBankClearCodeShort: {
            items: iMeBankClearCodeShort
          },
          iMediaBankCountryCode02: {
            items: iMediaBankCountryCode02
          },
          comissionTypes: {
            items: comissionTypes
          }
        }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_GET_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Бенефициар не найден.',
      message: 'Неверно указан идентификатор бенефициара.'
    });
  }
}

export function* saveDataSaga(action) {
  const { channels } = action;
  const documentData = yield select(documentDataSelector);
  const data = normalizeDataToREST(documentData);
  const allowedSave = documentData.getIn(['allowedSmActions', 'save']);
  const status = documentData.get('status');
  if (status === DOC_STATUSES.NEW || allowedSave) {
    try {
      const result = yield call(dalSaveData, { payload:data });
      if (status === DOC_STATUSES.NEW) yield put(replace(LOCAL_ROUTER_ALIASES.BENEFICIARY.replace('{:id}', result.recordID)));
      yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_SUCCESS, payload: result, channels });
    } catch (error) {
      yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_FAIL, payload: { error }, channels });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'doNotAllowed' } });
  }
}

export function* saveDataSuccessSaga(action) {
  const { payload: { errors }, channels } = action;
  if (errors && errors.length) {
    if (errors.filter(error => error.level === '2').length) {
      yield put({ type: channels.success, payload: { status: 'haveErrors' } });
    } else if (errors.filter(error => error.level === '1').length) {
      yield put({ type: channels.success, payload: { status: 'haveWarnings' } });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'success' } });
  }
}

export function* saveDataFailSaga(action) {
  const { payload: { error }, channels } = action;
  yield put({ type: channels.fail });
  yield call(errorNotification, error.name === 'RBO Controls Error'
    ? {
      title: 'Бенефициар не может быть сохранен.',
      message: 'Для сохранения бенефициара требуется исправить ошибки. ' +
      'При наличии блокирующих ошибок бенефициар не может быть сохранен.'
    }
    : {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно сохранить бенефициара'
    }
  );
}

export function* dictionarySearchSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  yield put({
    type: ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH_REQUEST,
    payload: {
      dictionaryId,
      dictionarySearchValue
    }
  });

  try {
    const result = yield call(getDictionaryById(dictionaryId), dictionarySearchValue);

    yield put({
      type: ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH_SUCCESS,
      payload: {
        dictionaryId,
        items: result
      }
    });
  } catch (error) {
    yield put({
      type: ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH_FAIL,
      payload: {
        dictionaryId
      }
    });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник'
    });
  }
}

const getFieldsValuesBenefSWIFT = (item, value) => (
  !item ?
  { [FIELDS.BENEF_SWIFT]: value } :
  {
    [FIELDS.BENEF_SWIFT]: item.get('bicInt'),
    [FIELDS.BENEF_NAME]: item.get('name'),
    [FIELDS.BENEF_ADDRESS]: item.get('address'),
    [FIELDS.BENEF_PLACE]: item.get('city'),
    [FIELDS.BENEF_COUNTRY_CODE_02]: item.get('countryCode02'),
    [FIELDS.BENEF_COUNTRY_CODE]: item.get('countryCode'),
    [FIELDS.BENEF_COUNTRY_NAME]: item.get('countryName')
  }
);

const getFieldsValuesBenefCountryCode02 = item => ({
  [FIELDS.BENEF_COUNTRY_CODE_02]: item.get('code', null),
  [FIELDS.BENEF_COUNTRY_CODE]: item.get('mnem03', null),
  [FIELDS.BENEF_COUNTRY_NAME]: item.get('name', null)
});

const getFieldsValuesPaymentDetails = (item, value) => (
  !item ?
  { [FIELDS.PAYMENT_DETAILS]: value } :
  { [FIELDS.PAYMENT_DETAILS]: item.get('purpose') }
);

const getFieldsValuesBenefBankSWIFT = (item, value) => (
  !item ?
  { [FIELDS.BENEF_BANK_SWIFT]: value } :
  {
    [FIELDS.BENEF_BANK_SWIFT]: item.get('bicInt'),
    [FIELDS.BENEF_BANK_NAME]: item.get('name'),
    [FIELDS.BENEF_BANK_ADDRESS]: item.get('address'),
    [FIELDS.BENEF_BANK_PLACE]: item.get('city'),
    [FIELDS.BENEF_BANK_COUNTRY_CODE_02]: item.get('countryCode02'),
    [FIELDS.BENEF_BANK_COUNTRY_CODE]: item.get('countryCode'),
    [FIELDS.BENEF_BANK_COUNTRY_NAME]: item.get('countryName')
  }
);

const getFieldsValuesBenefBankClearCodeShort = item => ({
  [FIELDS.BE_BANK_CLEAR_CODE_SHORT]: item.get('countryCode02', null),
  [FIELDS.BE_BANK_CLEAR_CODE_02]: item.get('countryCode02', null),
  [FIELDS.BE_BANK_CLEAR_CODE]: item.get('format', null)
});

const getFieldsValuesBenefBankCountryCode02 = item => ({
  [FIELDS.BENEF_BANK_COUNTRY_CODE_02]: item.get('code', null),
  [FIELDS.BENEF_BANK_COUNTRY_CODE]: item.get('mnem03', null),
  [FIELDS.BENEF_BANK_COUNTRY_NAME]: item.get('name', null)
});

const getFieldsValuesIMediaBankSWIFT = (item, value) => (
  !item ?
  { [FIELDS.I_MEDIA_BANK_SWIFT]: value } :
  {
    [FIELDS.I_MEDIA_BANK_SWIFT]: item.get('bicInt'),
    [FIELDS.I_MEDIA_BANK_NAME]: item.get('name'),
    [FIELDS.I_MEDIA_BANK_ADDRESS]: item.get('address'),
    [FIELDS.I_MEDIA_BANK_PLACE]: item.get('city'),
    [FIELDS.I_MEDIA_BANK_COUNTRY_CODE_02]: item.get('countryCode02'),
    [FIELDS.I_MEDIA_BANK_COUNTRY_CODE]: item.get('countryCode'),
    [FIELDS.I_MEDIA_BANK_COUNTRY_NAME]: item.get('countryName')
  }
);

const getFieldsValuesIMeBankClearCodeShort = item => ({
  [FIELDS.I_ME_BANK_CLEAR_CODE_SHORT]: item.get('countryCode02', null),
  [FIELDS.I_ME_BANK_CLEAR_CODE_02]: item.get('countryCode02', null),
  [FIELDS.I_ME_BANK_CLEAR_CODE]: item.get('format', null)
});

const getFieldsValuesIMediaBankCountryCode02 = item => ({
  [FIELDS.I_MEDIA_BANK_COUNTRY_CODE_02]: item.get('code', null),
  [FIELDS.I_MEDIA_BANK_COUNTRY_CODE]: item.get('mnem03', null),
  [FIELDS.I_MEDIA_BANK_COUNTRY_NAME]: item.get('name', null)
});

const dictionaryItemsSelector = dictionaryId => state =>
  state.getIn([LOCAL_REDUCER, 'documentDictionaries', dictionaryId, 'items']);

export function* changeDataSaga({ payload }) {
  const { fieldId, fieldValue, dictionaryValue = null } = payload;
  const selectedDocumentDictionaryItems = yield select(dictionaryItemsSelector(fieldId));
  let selectedDictionaryItem = null;
  let fieldValuesChanged;

  switch (fieldId) {
    // ОБЩАЯ ИНФОРМАЦИЯ
    case FIELDS.BENEF_SWIFT: // suggester
      selectedDictionaryItem = dictionaryValue !== null && selectedDocumentDictionaryItems.find(item => item.get('bicInt') === fieldValue);
      fieldValuesChanged = getFieldsValuesBenefSWIFT(selectedDictionaryItem, fieldValue);
      break;

    // АДРЕС, НАЗНАЧЕНИЕ ПЛАТЕЖА
    case FIELDS.BENEF_COUNTRY_CODE_02:// search
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('code') === fieldValue) || Map();
      fieldValuesChanged = getFieldsValuesBenefCountryCode02(selectedDictionaryItem);
      break;
    case FIELDS.PAYMENT_DETAILS:// suggester
      selectedDictionaryItem = dictionaryValue !== null && selectedDocumentDictionaryItems.find(item => item.get('purpose') === fieldValue);
      fieldValuesChanged = getFieldsValuesPaymentDetails(selectedDictionaryItem, fieldValue);
      break;

    // БАНК БЕНЕФИЦИАРА
    case FIELDS.BENEF_BANK_SWIFT: // suggester
      selectedDictionaryItem = dictionaryValue !== null && selectedDocumentDictionaryItems.find(item => item.get('bicInt') === fieldValue);
      fieldValuesChanged = getFieldsValuesBenefBankSWIFT(selectedDictionaryItem, fieldValue);
      break;
    case FIELDS.BE_BANK_CLEAR_CODE_SHORT: // search
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('countryCode02') === fieldValue) || Map();
      fieldValuesChanged = getFieldsValuesBenefBankClearCodeShort(selectedDictionaryItem);
      break;
    case FIELDS.BENEF_BANK_COUNTRY_CODE_02: // search
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('code') === fieldValue) || Map();
      fieldValuesChanged = getFieldsValuesBenefBankCountryCode02(selectedDictionaryItem);
      break;

    // БАНК ПОСРЕДНИК
    case FIELDS.I_MEDIA_BANK_SWIFT:// suggester
      selectedDictionaryItem = dictionaryValue !== null && selectedDocumentDictionaryItems.find(item => item.get('bicInt') === fieldValue);
      fieldValuesChanged = getFieldsValuesIMediaBankSWIFT(selectedDictionaryItem, fieldValue);
      break;
    case FIELDS.I_ME_BANK_CLEAR_CODE_SHORT: // search
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('countryCode02') === fieldValue) || Map();
      fieldValuesChanged = getFieldsValuesIMeBankClearCodeShort(selectedDictionaryItem);
      break;
    case FIELDS.I_MEDIA_BANK_COUNTRY_CODE_02: // search
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('code') === fieldValue) || Map();
      fieldValuesChanged = getFieldsValuesIMediaBankCountryCode02(selectedDictionaryItem);
      break;
    default:
      fieldValuesChanged = { [fieldId]: fieldValue };
  }

  yield put({
    type: ACTIONS.BENEFICIARY_CONTAINER_CHANGE_FIELDS_VALUE,
    payload: fieldValuesChanged
  });
}


export function* operationSaga(action) {
  const { payload: { operationId } } = action;
  switch (operationId) {
    case 'back':
      yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_BACK });
      break;
    case 'save':
      yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_REQUEST });
      break;
    case 'remove':
      yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_REQUEST });
      break;
    default:
      break;
  }
}

export function* operationBackSaga() {
  const location = yield select(routingLocationSelector);
  const prevLocation = location.get('prevLocation');
  if (prevLocation && prevLocation.get('pathname') !== location.get('pathname')) yield put(goBack());
  else yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
}

export function* operationSaveSaga() {
  yield put({
    type: ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_SUCCESS,
      fail: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_FAIL
    }
  });
}

export function* operationSaveSuccessSaga(action) {
  const { payload: { status } } = action;
  switch (status) {
    case 'haveErrors':
      yield call(errorNotification, {
        title: 'Бенефициар сохранен с ошибками.'
      });
      break;
    case 'haveWarnings':
      yield call(warningNotification, {
        title: 'Бенефициар сохранен с предупреждениями.',
        message: 'Обратите внимание на предупреждения из банка.'
      });
      break;
    default:
      yield call(successNotification, {
        title: 'Бенефициар успешно сохранен.'
      });
  }
}

export function* operationRemoveSaga() {
  yield call(confirmNotification, {
    level: 'error',
    title: 'Удаление бенефициара',
    message: 'Вы действительно хотите удалить бенефициара?',
    success: {
      label: 'Подтвердить',
      action: { type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_CONFIRM }
    },
    cancel: {
      label: 'Отмена',
      action: { type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_CANCEL }
    }
  });
}

export function* operationRemoveConfirmSaga() {
  const id = yield select(docIdSelector);
  try {
    yield call(dalOperationRemove, { payload: [id] });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: { documentData: result }
    });
    yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
    yield call(successNotification, { title: 'Бенефициар успешно удален.' });
  } catch (error) {
    yield put({ type: ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно удалить бенефициара'
    });
  }
}

export default function* saga() {
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_MOUNT, mountSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_GET_DATA_REQUEST, getDataSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_REQUEST, saveDataSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_SUCCESS, saveDataSuccessSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_SAVE_DATA_FAIL, saveDataFailSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_DICTIONARY_SEARCH, dictionarySearchSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_CHANGE_DATA, changeDataSaga);

  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_OPERATION, operationSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_OPERATION_BACK, operationBackSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_REQUEST, operationSaveSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemoveSaga);
  yield takeLatest(ACTIONS.BENEFICIARY_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
}
