import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRow,
  RboFormRowset,
  RboFormCell,
  RboFormFieldSuggest,
  RboFormFieldSearch,
  RboFormFieldText,
  RboFormCollapse,
  RboFormFieldValue
} from 'components/ui-components/rbo-form-compact/index';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import { FIELDS } from '../constants';

const BeneficiarySectionBeneficiaryBank = (props) => {
  const {
    isEditable,
    documentFieldsIsDisables,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange,
    onCollapseToggle,
    isCollapsed,
    isCollapsible,
    title
  } = props;

  const handleCollapseToggle = () => {
    onCollapseToggle({ sectionId: 'beneficiaryBank', isCollapsed: !isCollapsed });
  };

  return (
    <RboFormSection
      title={title}
      id="beneficiaryBank"
      isCollapsible={isCollapsible}
      isCollapsed={isCollapsed}
    >
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                  ?
                    <RboFormFieldSuggest
                      id={FIELDS.BENEF_BANK_SWIFT}
                      label="SWIFT-код"
                      value={documentData.get(FIELDS.BENEF_BANK_SWIFT)}
                      options={documentDictionaries.getIn(['benefBankSWIFT', 'items']).toJS()}
                      optionRenderer={formFieldOptionRenderer}
                      onSearch={onFieldSearch}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_SWIFT)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_SWIFT)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_BANK_SWIFT)}
                      popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                      // getConstraint={getConstraint}
                    />
                  :
                    <RboFormFieldValue
                      id={FIELDS.BENEF_BANK_SWIFT}
                      label="SWIFT-код"
                      value={documentData.get(FIELDS.BENEF_BANK_SWIFT)}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_SWIFT)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_SWIFT)}
                    />
                }

              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          {isEditable &&
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                <RboFormCollapse
                  pseudoLabel
                  title="Скрыть детали"
                  collapsedTitle="Показать детали"
                  isCollapsed={isCollapsed}
                  onCollapseToggle={handleCollapseToggle}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
          }
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="1/2">
                {isEditable
                  ?
                    <RboFormFieldSearch
                      id={FIELDS.BE_BANK_CLEAR_CODE_SHORT}
                      label="Клиринговый код"
                      value={documentData.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                      options={documentDictionaries.getIn(['beBankClearCodeShort', 'items']).toJS()}
                      optionRenderer={formFieldOptionRenderer}
                      onSearch={onFieldSearch}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                      isError={formErrors.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                      popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    />
                  :
                    <RboFormFieldValue
                      id="beBankClearCodeShort"
                      label="Клиринговый код"
                      value={documentData.get('beBankClearCodeShort')}
                      isWarning={formWarnings.get('beBankClearCodeShort')}
                      isError={formErrors.get('beBankClearCodeShort')}
                    />
                }
              </RboFormCell>
              <RboFormCell size="1/6">
                <RboFormFieldValue
                  id={FIELDS.BE_BANK_CLEAR_CODE_SHORT}
                  pseudoLabel
                  value={documentData.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                  isWarning={formWarnings.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                  isError={formErrors.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                />
              </RboFormCell>
              <RboFormCell size="1/3">
                {isEditable
                  ?
                    <RboFormFieldText
                      id={FIELDS.BE_BANK_CLEAR_CODE}
                      placeholder="9!n"
                      label="Значение кода"
                      value={documentData.get(FIELDS.BE_BANK_CLEAR_CODE)}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BE_BANK_CLEAR_CODE)}
                      isError={formErrors.get(FIELDS.BE_BANK_CLEAR_CODE)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BE_BANK_CLEAR_CODE)}
                      // getConstraint={getConstraint}
                    />
                  :
                    <RboFormFieldValue
                      id={FIELDS.BE_BANK_CLEAR_CODE}
                      label="Значение кода"
                      value={documentData.get(FIELDS.BE_BANK_CLEAR_CODE)}
                      isWarning={formWarnings.get(FIELDS.BE_BANK_CLEAR_CODE)}
                      isError={formErrors.get(FIELDS.BE_BANK_CLEAR_CODE)}
                    />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                  ?
                    <RboFormFieldText
                      id={FIELDS.BENEF_BANK_NAME}
                      label="Наименование"
                      value={documentData.get(FIELDS.BENEF_BANK_NAME)}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_NAME)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_NAME)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_BANK_NAME)}
                      // getConstraint={getConstraint}
                    />
                  :
                    <RboFormFieldValue
                      id={FIELDS.BENEF_BANK_NAME}
                      label="Наименование"
                      value={documentData.get(FIELDS.BENEF_BANK_NAME)}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_NAME)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_NAME)}
                    />
                }

              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="2/3">
                {isEditable
                  ?
                    <RboFormFieldText
                      id={FIELDS.BENEF_BANK_PLACE}
                      label="Город"
                      value={documentData.get(FIELDS.BENEF_BANK_PLACE)}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_PLACE)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_PLACE)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_BANK_PLACE)}
                      // getConstraint={getConstraint}
                    />
                  :
                    <RboFormFieldValue
                      id={FIELDS.BENEF_BANK_PLACE}
                      label="Город"
                      value={documentData.get(FIELDS.BENEF_BANK_PLACE)}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_PLACE)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_PLACE)}
                    />
                }

              </RboFormCell>
              <RboFormCell size="1/3">
                {isEditable
                  ?
                    <RboFormFieldSearch
                      id={FIELDS.BENEF_BANK_COUNTRY_CODE_02}
                      label="Страна"
                      value={documentData.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02)}
                      options={documentDictionaries.getIn(['benefBankCountryCode02', 'items']).toJS()}
                      optionRenderer={formFieldOptionRenderer}
                      onSearch={onFieldSearch}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02)}
                      popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    />
                  :
                    <RboFormFieldValue
                      id={FIELDS.BENEF_BANK_COUNTRY_CODE_02}
                      label="Страна"
                      value={documentData.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02)}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02)}
                    />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                  ?
                    <RboFormFieldText
                      id={FIELDS.BENEF_BANK_CORR_ACCOUNT}
                      label="Корр. счет в банке-посреднике"
                      value={documentData.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                      // getConstraint={getConstraint}
                    />
                  :
                    <RboFormFieldValue
                      id={FIELDS.BENEF_BANK_CORR_ACCOUNT}
                      label="Корр. счет в банке-посреднике"
                      value={documentData.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                    />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                  ?
                    <RboFormFieldText
                      id={FIELDS.BENEF_BANK_ADDRESS}
                      label="Адрес"
                      value={documentData.get(FIELDS.BENEF_BANK_ADDRESS)}
                      onChange={onFieldChange}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      isWarning={formWarnings.get(FIELDS.BENEF_BANK_ADDRESS)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_ADDRESS)}
                      isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_BANK_ADDRESS)}
                      // getConstraint={getConstraint}
                    />
                  :
                    <RboFormFieldValue
                      id={FIELDS.BENEF_BANK_ADDRESS}
                      label="Адрес"
                      value={documentData.get(FIELDS.BENEF_BANK_ADDRESS)}
                      sWarning={formWarnings.get(FIELDS.BENEF_BANK_ADDRESS)}
                      isError={formErrors.get(FIELDS.BENEF_BANK_ADDRESS)}
                    />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id={FIELDS.BENEF_BANK_COUNTRY_NAME}
                  pseudoLabel
                  value={documentData.get(FIELDS.BENEF_BANK_COUNTRY_NAME)}
                  isWarning={formWarnings.get(FIELDS.BENEF_BANK_COUNTRY_NAME)}
                  isError={formErrors.get(FIELDS.BENEF_BANK_COUNTRY_NAME)}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

BeneficiarySectionBeneficiaryBank.propTypes = {
  isEditable: PropTypes.bool,
  documentFieldsIsDisables: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  onCollapseToggle: PropTypes.func.isRequired,
  isCollapsed: PropTypes.bool.isRequired,
  isCollapsible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
};

export default BeneficiarySectionBeneficiaryBank;
