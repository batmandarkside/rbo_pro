export SectionPrimary from './primary';
export SectionPurpose from './purpose';
export SectionBeneficiaryBank from './beneficiary-bank';
export SectionIntermediaryBank from './intermediary-bank';
export SectionInfo from './info';
