import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRow,
  RboFormCell,
  RboFormFieldTextarea,
  RboFormFieldRadio,
  RboFormFieldValue
} from 'components/ui-components/rbo-form-compact/index';
import { FIELDS } from '../constants';

const BeneficiarySectionInfo = (props) => {
  const {
    isEditable,
    documentFieldsIsDisables,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    onFieldFocus,
    onFieldBlur,
    onFieldChange,
    title
  } = props;

  const comissionTypes = documentDictionaries.getIn(['comissionTypes', 'items']);
  return (
    <RboFormSection title={title} id="info">
      {comissionTypes.size >= 3 &&
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormFieldRadio
              id="TransferFeePayerOur"
              group={FIELDS.CHARGES_TYPE}
              title={comissionTypes.getIn([1, 'desc'])}
              value={comissionTypes.getIn([1, 'code'])}
              isSelected={comissionTypes.getIn([1, 'code']) === documentData.get(FIELDS.CHARGES_TYPE)}
              onChange={onFieldChange}
              isDisabled={documentFieldsIsDisables.get(FIELDS.CHARGES_TYPE) || !isEditable}
            />
            <RboFormFieldRadio
              id="TransferFeePayerBen"
              group={FIELDS.CHARGES_TYPE}
              title={comissionTypes.getIn([0, 'desc'])}
              value={comissionTypes.getIn([0, 'code'])}
              isSelected={comissionTypes.getIn([0, 'code']) === documentData.get(FIELDS.CHARGES_TYPE)}
              onChange={onFieldChange}
              isDisabled={documentFieldsIsDisables.get(FIELDS.CHARGES_TYPE) || !isEditable}
            />
          </RboFormBlock>
          <RboFormBlock>
            <RboFormFieldRadio
              id="TransferFeePayerSha"
              group={FIELDS.CHARGES_TYPE}
              title={comissionTypes.getIn([2, 'desc'])}
              value={comissionTypes.getIn([2, 'code'])}
              isSelected={comissionTypes.getIn([2, 'code']) === documentData.get(FIELDS.CHARGES_TYPE)}
              onChange={onFieldChange}
              isDisabled={documentFieldsIsDisables.get(FIELDS.CHARGES_TYPE) || !isEditable}
            />
          </RboFormBlock>
        </RboFormBlockset>
      }
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRow>
            <RboFormCell>
              {isEditable
              ?
                <RboFormFieldTextarea
                  id={FIELDS.COMMENTS}
                  label="Комментарии"
                  value={documentData.get(FIELDS.COMMENTS)}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.COMMENTS)}
                  isError={formErrors.get(FIELDS.COMMENTS)}
                  isDisabled={documentFieldsIsDisables.get(FIELDS.COMMENTS)}
                />
              :
                <RboFormFieldValue
                  id={FIELDS.COMMENTS}
                  label="Комментарии"
                  value={documentData.get(FIELDS.COMMENTS)}
                  isWarning={formWarnings.get(FIELDS.COMMENTS)}
                  isError={formErrors.get(FIELDS.COMMENTS)}
                />
              }

            </RboFormCell>
          </RboFormRow>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

BeneficiarySectionInfo.propTypes = {
  isEditable: PropTypes.bool,
  documentFieldsIsDisables: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default BeneficiarySectionInfo;
