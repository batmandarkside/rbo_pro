import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldText,
  RboFormFieldSuggest,
  RboFormFieldValue
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import { FIELDS } from '../constants';

const BeneficiarySectionPrimary = (props) => {
  const {
    isEditable,
    documentFieldsIsDisables,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange
  } = props;

  return (
    <RboFormSection id="primary">
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                ?
                  <RboFormFieldText
                    id={FIELDS.BENEF_NAME}
                    label="Наименование"
                    value={documentData.get(FIELDS.BENEF_NAME)}
                    isWarning={formWarnings.get(FIELDS.BENEF_NAME)}
                    isError={formErrors.get(FIELDS.BENEF_NAME)}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                    isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_NAME)}
                  />
                :
                  <RboFormFieldValue
                    id={FIELDS.BENEF_NAME}
                    label="Наименование"
                    value={documentData.get(FIELDS.BENEF_NAME)}
                    isWarning={formWarnings.get(FIELDS.BENEF_NAME)}
                    isError={formErrors.get(FIELDS.BENEF_NAME)}
                  />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock />
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                ?
                  <RboFormFieldText
                    id={FIELDS.BENEF_ACCOUNT}
                    label="Счёт № (IBAN)"
                    value={documentData.get(FIELDS.BENEF_ACCOUNT)}
                    isWarning={formWarnings.get(FIELDS.BENEF_ACCOUNT)}
                    isError={formErrors.get(FIELDS.BENEF_ACCOUNT)}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                    isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_ACCOUNT)}
                  />
                :
                  <RboFormFieldValue
                    id={FIELDS.BENEF_ACCOUNT}
                    label="Счет"
                    value={documentData.get(FIELDS.BENEF_ACCOUNT)}
                    isWarning={formWarnings.get(FIELDS.BENEF_ACCOUNT)}
                    isError={formErrors.get(FIELDS.BENEF_ACCOUNT)}
                  />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                ?
                  <RboFormFieldSuggest
                    id={FIELDS.BENEF_SWIFT}
                    label="SWIFT BIC"
                    value={documentData.get(FIELDS.BENEF_SWIFT)}
                    options={documentDictionaries.getIn(['benefSWIFT', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_SWIFT)}
                    isError={formErrors.get(FIELDS.BENEF_SWIFT)}
                    isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_SWIFT)}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    // getConstraint={getConstraint}
                  />
                :
                  <RboFormFieldValue
                    id={FIELDS.BENEF_SWIFT}
                    label="SWIFT BIC"
                    value={documentData.get(FIELDS.BENEF_SWIFT)}
                    isWarning={formWarnings.get(FIELDS.BENEF_SWIFT)}
                    isError={formErrors.get(FIELDS.BENEF_SWIFT)}
                  />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

BeneficiarySectionPrimary.propTypes = {
  isEditable: PropTypes.bool,
  documentFieldsIsDisables: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default BeneficiarySectionPrimary;
