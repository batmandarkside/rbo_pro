import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldSearch,
  RboFormFieldText,
  RboFormFieldSuggest,
  RboFormFieldValue,
  RboFormCollapse
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import { FIELDS } from '../constants';

const BeneficiarySectionPurpose = (props) => {
  const {
    isEditable,
    documentFieldsIsDisables,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange,
    onCollapseToggle,
    isCollapsed,
    isCollapsible,
    title
  } = props;

  const handleCollapseToggle = () => {
    onCollapseToggle({ sectionId: 'purpose', isCollapsed: !isCollapsed });
  };

  return (
    <RboFormSection
      id="purpose"
      title={title}
      isCollapsible={isCollapsible}
      isCollapsed={isCollapsed}
    >
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                ?
                  <RboFormFieldText
                    id={FIELDS.BENEF_ADDRESS}
                    label="Адрес"
                    value={documentData.get(FIELDS.BENEF_ADDRESS)}
                    isWarning={formWarnings.get(FIELDS.BENEF_ADDRESS)}
                    isError={formErrors.get(FIELDS.BENEF_ADDRESS)}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                    isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_ADDRESS)}
                  />
                :
                  <RboFormFieldValue
                    id={FIELDS.BENEF_ADDRESS}
                    label="Адрес"
                    value={documentData.get(FIELDS.BENEF_ADDRESS)}
                    isWarning={formWarnings.get(FIELDS.BENEF_ADDRESS)}
                    isError={formErrors.get(FIELDS.BENEF_ADDRESS)}
                  />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          {isEditable &&
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                <RboFormCollapse
                  pseudoLabel
                  title="Скрыть детали"
                  collapsedTitle="Показать детали"
                  isCollapsed={isCollapsed}
                  onCollapseToggle={handleCollapseToggle}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
          }
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="3/4">
                {isEditable
                ?
                  <RboFormFieldText
                    id={FIELDS.BENEF_PLACE}
                    label="Город"
                    value={documentData.get(FIELDS.BENEF_PLACE)}
                    isWarning={formWarnings.get(FIELDS.BENEF_PLACE)}
                    isError={formErrors.get(FIELDS.BENEF_PLACE)}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                    isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_PLACE)}
                  />
                :
                  <RboFormFieldValue
                    id={FIELDS.BENEF_PLACE}
                    label="Город"
                    value={documentData.get(FIELDS.BENEF_PLACE)}
                    isWarning={formWarnings.get(FIELDS.BENEF_PLACE)}
                    isError={formErrors.get(FIELDS.BENEF_PLACE)}
                  />
                }
              </RboFormCell>
              <RboFormCell size="1/4">
                {isEditable
                ?
                  <RboFormFieldSearch
                    id={FIELDS.BENEF_COUNTRY_CODE_02}
                    label="Страна"
                    value={documentData.get(FIELDS.BENEF_COUNTRY_CODE_02)}
                    options={documentDictionaries.getIn(['benefCountryCode02', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_COUNTRY_CODE_02)}
                    isError={formErrors.get(FIELDS.BENEF_COUNTRY_CODE_02)}
                    isDisabled={documentFieldsIsDisables.get(FIELDS.BENEF_COUNTRY_CODE_02)}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    // getConstraint={getConstraint}
                  />
                :
                  <RboFormFieldValue
                    id={FIELDS.BENEF_COUNTRY_CODE_02}
                    label="Страна"
                    value={documentData.get(FIELDS.BENEF_COUNTRY_CODE_02)}
                    isWarning={formWarnings.get(FIELDS.BENEF_COUNTRY_CODE_02)}
                    isError={formErrors.get(FIELDS.BENEF_COUNTRY_CODE_02)}
                  />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id={FIELDS.BENEF_COUNTRY_NAME}
                  pseudoLabel
                  value={documentData.get(FIELDS.BENEF_COUNTRY_NAME)}
                  isWarning={formWarnings.get(FIELDS.BENEF_COUNTRY_NAME)}
                  isError={formErrors.get(FIELDS.BENEF_COUNTRY_NAME)}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable
                ?
                  <RboFormFieldSuggest
                    id={FIELDS.PAYMENT_DETAILS}
                    label="70: Назначение платежа"
                    inputType="Textarea"
                    value={documentData.get(FIELDS.PAYMENT_DETAILS)}
                    options={documentDictionaries.getIn(['paymentDetails', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.PAYMENT_DETAILS)}
                    isError={formErrors.get(FIELDS.PAYMENT_DETAILS)}
                    isDisabled={documentFieldsIsDisables.get(FIELDS.PAYMENT_DETAILS)}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    // getConstraint={getConstraint}
                  />
                :
                  <RboFormFieldValue
                    id={FIELDS.PAYMENT_DETAILS}
                    label="70: Назначение платежа"
                    value={documentData.get(FIELDS.PAYMENT_DETAILS)}
                    isWarning={formWarnings.get(FIELDS.PAYMENT_DETAILS)}
                    isError={formErrors.get(FIELDS.PAYMENT_DETAILS)}
                  />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

BeneficiarySectionPurpose.propTypes = {
  isEditable: PropTypes.bool,
  documentFieldsIsDisables: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  onCollapseToggle: PropTypes.func.isRequired,
  isCollapsed: PropTypes.bool.isRequired,
  isCollapsible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
};

export default BeneficiarySectionPurpose;
