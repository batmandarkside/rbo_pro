import { createSelector } from 'reselect';
import { fromJS, Map } from 'immutable';
import { DOC_STATUSES } from 'constants';
import {
  getDocFormErrors,
  getMappingDocSectionsAndErrors
} from 'utils';
import { LOCAL_REDUCER, FIELDS } from './constants';
import {
  documentFieldsMap,
  documentSectionsFieldsMap,
  documentErrorsMap,
  isSpecialSWIFT,
} from './utils';

const isDataErrorSelector = state => state.getIn([LOCAL_REDUCER, 'isDataError']);
const isDataLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isDataLoading']);
const isEditableSelector = state => state.getIn([LOCAL_REDUCER, 'isEditable']);
const documentTabsSelector = state => state.getIn([LOCAL_REDUCER, 'documentTabs']);
const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
const documentErrorsSelector = state => state.getIn([LOCAL_REDUCER, 'documentErrors']);
const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
const formSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'formSections']);
const activeFormFieldSelector = state => state.getIn([LOCAL_REDUCER, 'activeFormField']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);
const constraintsSelector = state => state.getIn([LOCAL_REDUCER, 'constraints']);

const documentDataCreatedSelector = createSelector(
  documentDataSelector,
  documentData => (
    Map({
      status: documentData.get('status'),
      signed: documentData.get('signed'),

      // ОБЩАЯ ИНФОРМАЦИЯ
      [FIELDS.BENEF_NAME]: documentData.get(FIELDS.BENEF_NAME, ''),
      [FIELDS.BENEF_ACCOUNT]: documentData.get(FIELDS.BENEF_ACCOUNT, ''),
      [FIELDS.BENEF_SWIFT]: documentData.get(FIELDS.BENEF_SWIFT, ''),

      // АДРЕС, НАЗНАЧЕНИЕ ПЛАТЕЖА
      [FIELDS.BENEF_ADDRESS]: documentData.get(FIELDS.BENEF_ADDRESS, ''),
      [FIELDS.BENEF_PLACE]: documentData.get(FIELDS.BENEF_PLACE, ''),
      [FIELDS.BENEF_COUNTRY_CODE_02]: documentData.get(FIELDS.BENEF_COUNTRY_CODE_02, ''),
      [FIELDS.BENEF_COUNTRY_NAME]: documentData.get(FIELDS.BENEF_COUNTRY_NAME, ''),
      [FIELDS.PAYMENT_DETAILS]: documentData.get(FIELDS.PAYMENT_DETAILS, ''),

      // БАНК БЕНЕФЕЦИАРА
      [FIELDS.BENEF_BANK_SWIFT]: documentData.get(FIELDS.BENEF_BANK_SWIFT, ''),
      [FIELDS.BE_BANK_CLEAR_CODE_SHORT]: documentData.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT, ''),
      [FIELDS.BE_BANK_CLEAR_CODE_02]: documentData.get(FIELDS.BE_BANK_CLEAR_CODE_02, ''),
      [FIELDS.BE_BANK_CLEAR_CODE]: documentData.get(FIELDS.BE_BANK_CLEAR_CODE, ''),
      [FIELDS.BENEF_BANK_NAME]: documentData.get(FIELDS.BENEF_BANK_NAME, ''),
      [FIELDS.BENEF_BANK_CORR_ACCOUNT]: documentData.get(FIELDS.BENEF_BANK_CORR_ACCOUNT, ''),
      [FIELDS.BENEF_BANK_ADDRESS]: documentData.get(FIELDS.BENEF_BANK_ADDRESS, ''),
      [FIELDS.BENEF_BANK_PLACE]: documentData.get(FIELDS.BENEF_BANK_PLACE, ''),
      [FIELDS.BENEF_BANK_COUNTRY_CODE_02]: documentData.get(FIELDS.BENEF_BANK_COUNTRY_CODE_02, ''),
      [FIELDS.BENEF_BANK_COUNTRY_NAME]: documentData.get(FIELDS.BENEF_BANK_COUNTRY_NAME, ''),

      // БАНК ПОСРЕДНИКА
      [FIELDS.I_MEDIA_BANK_SWIFT]: documentData.get(FIELDS.I_MEDIA_BANK_SWIFT, ''),
      [FIELDS.I_ME_BANK_CLEAR_CODE_SHORT]: documentData.get(FIELDS.I_ME_BANK_CLEAR_CODE_SHORT, ''),
      [FIELDS.I_ME_BANK_CLEAR_CODE_02]: documentData.get(FIELDS.I_ME_BANK_CLEAR_CODE_02, ''),
      [FIELDS.I_ME_BANK_CLEAR_CODE]: documentData.get(FIELDS.I_ME_BANK_CLEAR_CODE, ''),
      [FIELDS.I_MEDIA_BANK_NAME]: documentData.get(FIELDS.I_MEDIA_BANK_NAME, ''),
      [FIELDS.I_MEDIA_BANK_ADDRESS]: documentData.get(FIELDS.I_MEDIA_BANK_ADDRESS, ''),
      [FIELDS.I_MEDIA_BANK_PLACE]: documentData.get(FIELDS.I_MEDIA_BANK_PLACE, ''),
      [FIELDS.I_MEDIA_BANK_COUNTRY_CODE_02]: documentData.get(FIELDS.I_MEDIA_BANK_COUNTRY_CODE_02, ''),
      [FIELDS.I_MEDIA_BANK_COUNTRY_NAME]: documentData.get(FIELDS.I_MEDIA_BANK_COUNTRY_NAME, ''),

      // ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ
      [FIELDS.CHARGES_TYPE]: documentData.get(FIELDS.CHARGES_TYPE, ''),
      [FIELDS.CHARGES_TYPE_INFO]: documentData.get(FIELDS.CHARGES_TYPE_INFO, ''),
      [FIELDS.COMMENTS]: documentData.get(FIELDS.COMMENTS, ''),
    })
  )
);

const documentFieldsIsDisabledCreatedSelector = createSelector(
  documentDataSelector,
  (documentData) => {
    const DISABLED = true;
    const ENABLED = false;

    const benefSWIFT = documentData.get(FIELDS.BENEF_SWIFT, '');
    const benefBankSWIFT = documentData.get(FIELDS.BENEF_BANK_SWIFT, '');

    const existsBenefSWIFT = !!benefSWIFT;
    const existsBeBankClearCode02 = !!documentData.get(FIELDS.BE_BANK_CLEAR_CODE_02, '');
    const existsBenefBankSWIFT = !!benefBankSWIFT;
    const existsIMeBankClearCode02 = !!documentData.get(FIELDS.I_ME_BANK_CLEAR_CODE_02, '');

    return Map({
      // ОБЩАЯ ИНФОРМАЦИЯ
      [FIELDS.BENEF_NAME]: existsBenefSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_ACCOUNT]: ENABLED,
      [FIELDS.BENEF_SWIFT]: ENABLED,

      // АДРЕС, НАЗНАЧЕНИЕ ПЛАТЕЖА
      [FIELDS.BENEF_ADDRESS]: existsBenefSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_PLACE]: existsBenefSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_COUNTRY_CODE_02]: existsBenefSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_COUNTRY_NAME]: DISABLED,
      [FIELDS.PAYMENT_DETAILS]: ENABLED,

      // БАНК БЕНЕФЕЦИАРА
      [FIELDS.BENEF_BANK_SWIFT]: ENABLED,
      [FIELDS.BE_BANK_CLEAR_CODE_SHORT]: ENABLED,
      [FIELDS.BE_BANK_CLEAR_CODE_02]: DISABLED,
      [FIELDS.BE_BANK_CLEAR_CODE]: existsBeBankClearCode02 ? ENABLED : DISABLED,
      [FIELDS.BENEF_BANK_NAME]: existsBenefBankSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_BANK_CORR_ACCOUNT]: isSpecialSWIFT(benefSWIFT) ? DISABLED : ENABLED,
      [FIELDS.BENEF_BANK_ADDRESS]: existsBenefBankSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_BANK_PLACE]: existsBenefBankSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_BANK_COUNTRY_CODE_02]: existsBenefBankSWIFT ? DISABLED : ENABLED,
      [FIELDS.BENEF_BANK_COUNTRY_NAME]: DISABLED,

      // БАНК ПОСРЕДНИКА
      [FIELDS.I_MEDIA_BANK_SWIFT]: isSpecialSWIFT(benefBankSWIFT) ? DISABLED : ENABLED,
      [FIELDS.I_ME_BANK_CLEAR_CODE_SHORT]: isSpecialSWIFT(benefSWIFT) ? DISABLED : ENABLED,
      [FIELDS.I_ME_BANK_CLEAR_CODE_02]: DISABLED,
      [FIELDS.I_ME_BANK_CLEAR_CODE]: existsIMeBankClearCode02 ? ENABLED : DISABLED,
      [FIELDS.I_MEDIA_BANK_NAME]: isSpecialSWIFT(benefBankSWIFT) ? DISABLED : ENABLED,
      [FIELDS.I_MEDIA_BANK_ADDRESS]: isSpecialSWIFT(benefBankSWIFT) ? DISABLED : ENABLED,
      [FIELDS.I_MEDIA_BANK_PLACE]: isSpecialSWIFT(benefBankSWIFT) ? DISABLED : ENABLED,
      [FIELDS.I_MEDIA_BANK_COUNTRY_CODE_02]: isSpecialSWIFT(benefBankSWIFT) ? DISABLED : ENABLED,
      [FIELDS.I_MEDIA_BANK_COUNTRY_NAME]: DISABLED,

      // ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ
      [FIELDS.CHARGES_TYPE]: ENABLED,
      [FIELDS.COMMENTS]: ENABLED,
    });
  }
);

const documentErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  activeFormFieldSelector,
  (documentErrors, activeFormField) => documentErrors
    .map(error => error.merge({
      fieldId: documentErrorsMap[error.get('id')] && documentErrorsMap[error.get('id')][0],
      fieldsIds: documentErrorsMap[error.get('id')],
      isActive: documentErrorsMap[error.get('id')] &&
        !!documentErrorsMap[error.get('id')].find(i => i === activeFormField)
    }))
    .filter(error => !!error.get('fieldId'))
    .sort((a, b) => documentFieldsMap.indexOf(a.get('fieldId')) - documentFieldsMap.indexOf(b.get('fieldId')))
);

const documentStructureCreatedSelector = createSelector(
  documentTabsSelector,
  formSectionsSelector,
  documentErrorsCreatedSelector,
  activeFormFieldSelector,
  (documentTabs, formSections, documentErrors, activeFormField) => documentTabs.map((tab) => {
    switch (tab.get('id')) {
      case 'main':
        return tab.set('sections',
          getMappingDocSectionsAndErrors(formSections, documentSectionsFieldsMap, documentErrors, activeFormField));
      default:
        return tab;
    }
  })
);

const documentDictionariesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => documentDictionaries.map((dictionary, key) => {
    const items = dictionary.get('items');
    switch (key) {
      case 'benefSWIFT':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('id'),
            title: item.get('bicInt'),
            subtitle: item.get('countryName'),
            describe: item.get('name')
          }))
        });
      case 'benefCountryCode02':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('code'),
            title: item.get('mnem02'),
            subtitle: item.get('code'),
            describe: item.get('shortName')
          }))
        });
      case 'paymentDetails':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('purpose'),
            title: item.get('purpose'),
            subtitle: '',
            describe: ''
          }))
        });
      case 'benefBankSWIFT':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('id'),
            title: item.get('bicInt'),
            subtitle: item.get('countryName'),
            describe: item.get('name')
          }))
        });
      case 'beBankClearCodeShort':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('countryCode02'),
            title: item.get('shortName'),
            subtitle: item.get('format'),
            describe: `${item.get('countryCode02')} ${item.get('name')}`
          }))
        });
      case 'benefBankCountryCode02':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('code'),
            title: item.get('mnem02'),
            subtitle: item.get('code'),
            describe: item.get('shortName')
          }))
        });
      case 'iMediaBankSWIFT':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('id'),
            title: item.get('bicInt'),
            subtitle: item.get('countryName'),
            describe: item.get('name')
          }))
        });
      case 'iMeBankClearCodeShort':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('countryCode02'),
            title: item.get('shortName'),
            subtitle: item.get('format'),
            describe: `${item.get('countryCode02')} ${item.get('name')}`
          }))
        });
      case 'iMediaBankCountryCode02':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('code'),
            title: item.get('mnem02'),
            subtitle: item.get('code'),
            describe: item.get('shortName')
          }))
        });
      default:
        return dictionary;
    }
  })
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['1'])
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['2', '3'])
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  documentDataSelector,
  (operations, documentData) => {
    const status = documentData.get('status');
    const allowedSmActions = documentData.get('allowedSmActions', fromJS({}));
    return operations.filter((operation) => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'back':
          return true;
        case 'save':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save');
        case 'remove':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('delete');
        default:
          return false;
      }
    });
  }
);

export const constraintSelector = createSelector(
  constraintsSelector,
  constraints => fieldId =>
    constraints.get(fieldId, Map())
);

const mapStateToProps = state => ({
  isDataError: isDataErrorSelector(state),
  isDataLoading: isDataLoadingSelector(state),
  isEditable: isEditableSelector(state),
  documentTabs: documentTabsSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentFieldsIsDisables: documentFieldsIsDisabledCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentDictionaries: documentDictionariesCreatedSelector(state),
  formSections: formSectionsSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  operations: operationsCreatedSelector(state),
  getConstraint: constraintSelector(state)
});

export default mapStateToProps;
