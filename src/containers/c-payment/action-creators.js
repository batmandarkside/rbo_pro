import { ACTION_TYPES } from './constants';

/**
 *    action входа в контейнер
 */
export const currencyPaymentMount = params => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_MOUNT,
  payload: { params }
});

/**
 *    action выхода из контейнера
 */
export const currencyUnmountAction = () => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_UNMOUNT
});

export const currencyPaymentFetchDataSuccess = data => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_FETCH_DATA_SUCCESS,
  payload: { data }
});

export const dictionarySearchAction = (dictionaryId, dictionarySearchValue) => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_DICTIONARY_SEARCH,
  payload: { dictionaryId, dictionarySearchValue }
});

export const currencyPaymentUpdateDictionaryValuesAction = (dictionaryId, values) => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_UPDATE_DICTIONARY_VALUES,
  payload: { dictionaryId, values }
});

export const currencyOperationNewAction = () => ({
  type: ACTION_TYPES.CURRENCY_OPERATION_NEW
});

export const currencyOperationCopyAction = id => ({
  type: ACTION_TYPES.CURRENCY_OPERATION_COPY,
  payload: { id }
});

export const currencyOperationEditAction = id => ({
  type: ACTION_TYPES.CURRENCY_OPERATION_EDIT,
  payload: { id }
});

export const currencyOperationClearAction = () => ({
  type: ACTION_TYPES.CURRENCY_OPERATION_CLEAR
});

export const currencyOperationSaveAction = data => ({
  type: ACTION_TYPES.CURRENCY_OPERATION_SAVE,
  payload: { data }
});

export const currencyOperationDeleteAction = id => ({
  type: ACTION_TYPES.CURRENCY_OPERATION_DELETE,
  payload: { id }
});

export const currencyOperationChangeFieldAction = (fieldName, value) => ({
  type: ACTION_TYPES.CURRENCY_OPERATION_CHANGE_FIELD,
  payload: { fieldName, value }
});

export const currencyPaymentChangeDataAction = (fieldName, value) => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CHANGE_DATA,
  payload: { fieldName, value }
});

/**
 *    action сворачивания / разворачивания секции формы
 *    @param {string} sectionId
 *    @param {boolean} isCollapsed
 */
export const sectionCollapseToggleAction = (sectionId, isCollapsed) => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SECTION_COLLAPSE_TOGGLE,
  payload: { sectionId, isCollapsed }
});

/**
 *    action переключения табов
 *    @param {string} tabId
 */
export const changeTabAction = tabId => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_CHANGE_TAB,
  payload: { tabId }
});


/**
 *    action focus на поле формы
 *    @param {string} fieldId
 */
export const fieldFocusAction = fieldId => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_FIELD_FOCUS,
  payload: { fieldId }
});

/**
 *    action blur на поле формы
 *    @param {string} fieldId
 */
export const fieldBlurAction = fieldId => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_FIELD_BLUR,
  payload: { fieldId }
});


/**
 *    action перехода к отзыву
 *    @param {string} recallId
 */
export const routeToRecallAction = recallId => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_ROUTE_TO_RECALL,
  payload: { recallId }
});

/**
 *    action выполнения операции
 *    @param {string} operationId
 */
export const operationAction = operationId => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION,
  payload: { operationId }
});

/**
 *    action закрытия верхней панели документа
 */
export const closeDocumentTopAction = () => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_CLOSE_DOCUMENT_TOP
});

/**
 *    action печати подписи документа
 */
export const signaturePrintAction = signId => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_PRINT_REQUEST,
  payload: { signId }
});

/**
 *    action скачивание подписи документа
 */
export const signatureDownloadAction = signId => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_REQUEST,
  payload: { signId }
});


/**
 *    action выполнения операции сохранения шаблона
 *    @param {string} templateName
 */
export const operationSaveAsTemplateContinueAction = templateName => ({
  type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_CONTINUE,
  payload: { templateName }
});
