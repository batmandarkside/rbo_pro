import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { List, Map } from 'immutable';

import RboDocument, {
  RboDocumentPanel,
  RboDocumentOperations,
  RboDocumentContent,
  RboDocumentMain,
  RboDocumentHead,
  RboDocumentHeadStatus,
  RboDocumentBody,
  RboDocumentExtra,
  RboDocumentExtraSection,
  RboDocumentStructure,
  RboDocumentHeadTabs,
  RboDocumentNotes,
  RboDocumentInfo,
  RboDocumentLinkedDocs,
  RboDocumentTop,
  RboDocumentTopHistory,
  RboDocumentTemplates,
  RboDocumentTopSignatures,
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import RboForm from 'components/ui-components/rbo-form-compact';

import TemplateModal from 'components/template-modal';

import { LOCAL_ROUTER_ALIASES } from './constants';

import { getScrollTopById, smoothScrollTo } from './utils';

import {
  CtrPayerSection,
  CtrTransferSection,
  CtrBeneficiarySection,
  CtrBeneficiaryBankSection,
  CtrIntermediaryBankSection,
  CtrPaymentPurposeSection,
  CtrAdditionalInfoSection,
  CtrTransferFeeSection,
  CtrCommonInformationSection,
  CtrPayedTaxesSection,
  CtrCurrencyOperationsSection,
  CtrCurrencyControlSection,
} from './sections';

class FormCtr extends Component {
  static propTypes = {
    changeTabAction: PropTypes.func.isRequired,

    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),
    closeDocumentTopAction: PropTypes.func.isRequired,
    currencyPaymentMount: PropTypes.func.isRequired,
    currencyUnmountAction: PropTypes.func.isRequired,
    currencyPaymentChangeDataAction: PropTypes.func.isRequired,
    dictionarySearchAction: PropTypes.func.isRequired,
    documentDictionaries: PropTypes.instanceOf(Map).isRequired,
    documentTabs: PropTypes.instanceOf(List).isRequired,
    documentStructure: PropTypes.instanceOf(List).isRequired,
    documentRecalls: PropTypes.instanceOf(List).isRequired,
    documentHistory: PropTypes.instanceOf(Map).isRequired,
    documentSignatures: PropTypes.instanceOf(Map).isRequired,
    formData: ImmutablePropTypes.map.isRequired,
    formValues:  PropTypes.instanceOf(Map).isRequired,
    fieldFocusAction: PropTypes.func.isRequired,
    fieldBlurAction: PropTypes.func.isRequired,
    formSections: PropTypes.instanceOf(Map).isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    operationAction: PropTypes.func.isRequired,
    operationSaveAsTemplateContinueAction: PropTypes.func.isRequired,
    routeToRecallAction: PropTypes.func.isRequired,
    sectionCollapseToggleAction: PropTypes.func.isRequired,
    signaturePrintAction: PropTypes.func.isRequired,
    signatureDownloadAction: PropTypes.func.isRequired,
    formWarnings: PropTypes.instanceOf(Map).isRequired,
    formErrors: PropTypes.instanceOf(Map).isRequired
  };

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.currencyPaymentMount(params);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search)
      && nextProps.history.action !== 'REPLACE') {
      this.props.currencyPaymentMount(nextProps.match.params);
    }
  }

  componentWillUnmount() {
    this.props.currencyUnmountAction();
  }

  onSectionClick = () => {};
  onErrorClick = () => {};

  getDocTitle() {
    const { formData } = this.props;
    if (!formData || !formData.getIn(['docInfo', 'docNumber'])) return 'Валютный перевод';
    return `Валютный перевод № ${formData.getIn(['docInfo', 'docNumber'])}`;
  }

  getDocStatus() {
    return this.props.formData ? this.props.formData.get('status') : '';
  }

  handleStructureTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleStructureSectionClick = ({ tabId, sectionId }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    setTimeout(() => {
      const fieldId = parentTab.get('sections').find(section => section.get('id') === sectionId).get('firstFieldId');
      smoothScrollTo(
        getScrollTopById(sectionId, DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR),
        () => {
          document.querySelector(`#${fieldId}`) &&
          setTimeout(() => { document.querySelector(`#${fieldId}`).focus(); }, 0);
        }
      );
    }, 0);
  };

  handleFormSectionCollapseToggle = ({ sectionId, isCollapsed }) => {
    this.props.sectionCollapseToggleAction(sectionId, isCollapsed);
  };

  handleRecallClick = ({ docId }) => {
    this.props.routeToRecallAction(docId);
  };

  handleFieldSearch = ({ fieldId, fieldSearchValue }) => this.props.dictionarySearchAction(fieldId, fieldSearchValue);

  handleFieldChange = ({ fieldId, fieldValue }) => this.props.currencyPaymentChangeDataAction(fieldId, fieldValue);

  handleTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleFocus = ({ fieldId }) => {
    this.props.fieldFocusAction(fieldId);
  };

  handleBlur = ({ fieldId }) => {
    // const { formWarnings, formErrors, fieldBlurAction, validateDataAction } = this.props;
    this.props.fieldBlurAction(fieldId);
    // if (formWarnings.size || formErrors.size) validateDataAction();
  };

  render() {
    const {
      documentTabs,
      documentStructure,
      formData,
      formSections,
      documentRecalls,
      operations,
      operationAction,
      formValues,
      documentDictionaries,
      documentHistory,
      documentSignatures,
      closeDocumentTopAction,
      signaturePrintAction,
      signatureDownloadAction,
      operationSaveAsTemplateContinueAction,
      formWarnings,
      formErrors
    } = this.props;

    const sectionsProps = {
      onFieldSearch: this.handleFieldSearch,
      onFieldChange: this.handleFieldChange,
      onCollapseToggle: this.handleFormSectionCollapseToggle,
      onFieldFocus: this.handleFocus,
      onFieldBlur: this.handleBlur,
      formData,
      formWarnings,
      formErrors
    };

    return (
      <div>
        <RboDocument>
          <RboDocumentPanel>
            <RboDocumentOperations
              operations={operations}
              operationAction={operationAction}
            />
          </RboDocumentPanel>
          <RboDocumentContent>
            <RboDocumentMain>
              {(documentHistory.get('isVisible') || documentSignatures.get('isVisible')) &&
              <RboDocumentTop onCloseClick={closeDocumentTopAction}>
                {documentHistory.get('isVisible') &&
                <RboDocumentTopHistory
                  items={documentHistory.get('items')}
                />
                }
                {documentSignatures.get('isVisible') &&
                <RboDocumentTopSignatures
                  items={documentSignatures.get('items')}
                  onSignaturePrintClick={signaturePrintAction}
                  onSignatureDownloadClick={signatureDownloadAction}
                />
                }
              </RboDocumentTop>
              }
              <RboDocumentHead title={this.getDocTitle()}>
                <RboDocumentHeadStatus status={this.getDocStatus()} />
                <RboDocumentHeadTabs
                  onTabClick={this.handleTabClick}
                  tabs={documentTabs}
                />
              </RboDocumentHead>
              <RboDocumentBody>
                {!!documentStructure.find(tab => tab.get('id') === 'main').get('isActive') &&
                <RboForm>
                  <CtrCommonInformationSection
                    isCollapsible={formSections.getIn(['commonInformation', 'isCollapsible'])}
                    isCollapsed={formSections.getIn(['commonInformation', 'isCollapsed'])}
                    {...sectionsProps}
                  />
                  <CtrPayerSection
                    isCollapsible={formSections.getIn(['payer', 'isCollapsible'])}
                    isCollapsed={formSections.getIn(['payer', 'isCollapsed'])}
                    {...sectionsProps}
                  />
                  <CtrTransferSection {...sectionsProps} />
                  <CtrBeneficiarySection {...sectionsProps} />
                  <CtrBeneficiaryBankSection
                    isCollapsible={formSections.getIn(['beneficiaryBank', 'isCollapsible'])}
                    isCollapsed={formSections.getIn(['beneficiaryBank', 'isCollapsed'])}
                    {...sectionsProps}
                  />
                  <CtrIntermediaryBankSection
                    isCollapsible={formSections.getIn(['intermediaryBank', 'isCollapsible'])}
                    isCollapsed={formSections.getIn(['intermediaryBank', 'isCollapsed'])}
                    {...sectionsProps}
                  />
                  <CtrPaymentPurposeSection {...sectionsProps} />
                  <CtrAdditionalInfoSection {...sectionsProps} />
                  <CtrTransferFeeSection {...sectionsProps} />
                  <CtrPayedTaxesSection {...sectionsProps} />
                  <CtrCurrencyOperationsSection {...sectionsProps} />
                  <CtrCurrencyControlSection
                    isCollapsible={formSections.getIn(['currencyControl', 'isCollapsible'])}
                    isCollapsed={formSections.getIn(['currencyControl', 'isCollapsed'])}
                    {...sectionsProps}
                  />
                </RboForm>
                }
                {!!documentStructure.find(tab => tab.get('id') === 'notes').get('isActive') &&
                <RboDocumentNotes
                  isEditable
                  fieldId="docNote"
                  value={formData.getIn(['docInfo', 'docNote'])}
                  onChange={this.handleFieldChange}
                />
                }
                {!!documentStructure.find(tab => tab.get('id') === 'info').get('isActive') &&
                <RboDocumentInfo items={formValues.get('bankInfo')} />
                }
              </RboDocumentBody>
            </RboDocumentMain>
            <RboDocumentExtra>
              <RboDocumentExtraSection title="Шаблоны поручений">
                <RboDocumentTemplates
                  dictionary={documentDictionaries.get('templates')}
                  popupContainerSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  onFieldSearch={() => {}}
                  onFieldChange={() => {}}
                  linkToTemplates={LOCAL_ROUTER_ALIASES.CURRENCY_PAYMENT_TEMPLATES}
                />
              </RboDocumentExtraSection>
              <RboDocumentExtraSection title="Структура документа">
                <RboDocumentStructure
                  structure={documentStructure}
                  onTabClick={this.handleStructureTabClick}
                  onSectionClick={this.handleStructureSectionClick}
                  onErrorClick={this.onErrorClick}
                />
              </RboDocumentExtraSection>
              {!!documentRecalls.size &&
              <RboDocumentExtraSection title="Отзывы">
                <RboDocumentLinkedDocs
                  docs={documentRecalls}
                  onDocClick={this.handleRecallClick}
                />
              </RboDocumentExtraSection>
              }
            </RboDocumentExtra>
          </RboDocumentContent>
        </RboDocument>
        <TemplateModal onSubmitAction={operationSaveAsTemplateContinueAction} />
      </div>
    );
  }
}

export default FormCtr;
