import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';

import saga from './sagas';
import * as localActionCreators from './action-creators';
import reducer from './reducer';
import mapStateToProps from './selectors';
import FormCtr from './container';

const withConnect = connect(
  mapStateToProps,
  localActionCreators
);
const withReducer = injectReducer({ key: 'currencyPayment', reducer });
const withSaga = injectSaga({ key: 'currencyPaymentSagas', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FormCtr);
