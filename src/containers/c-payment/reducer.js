import { fromJS } from 'immutable';
import { ACTION_TYPES, FIELDS } from './constants';

const emptyCurrencyOperation = fromJS({
  code: '',
  amount: 0,
  curType: '',
});

const initialFormSections = {
  commonInformation: {
    title: 'Общая информация',
    isVisible: true,
    isCollapsible: true,
    isCollapsed: true,
    order: 1
  },
  payer: {
    title: '50: Перевододатель',
    isVisible: true,
    isCollapsible: true,
    isCollapsed: true,
    order: 2
  },
  transfer: {
    title: '32А: Перевод',
    isVisible: true,
    isCollapsible: true,
    order: 3
  },
  beneficiary: {
    title: '59: Бенефициар',
    isVisible: true,
    isCollapsible: false,
    order: 4
  },
  beneficiaryBank: {
    title: '57: Банк бенефициара',
    isVisible: true,
    isCollapsible: false,
    isCollapsed: false,
    order: 5
  },
  intermediaryBank: {
    title: '56: Банк-посредник',
    isVisible: true,
    isCollapsible: false,
    isCollapsed: true,
    order: 6
  },
  paymentPurpose: {
    title: '70: Назначение платежа',
    isVisible: true,
    isCollapsible: false,
    order: 7
  },
  additionalInfo: {
    title: '72: Дополнительная информация',
    isVisible: true,
    isCollapsible: true,
    order: 8
  },
  transferId: {
    title: '71A: Комиссия за перевод',
    isVisible: true,
    isCollapsible: false,
    order: 9
  },
  payedTaxes: {
    title: 'Проплаченные налоги',
    isVisible: true,
    isCollapsible: false,
    order: 10
  },
  currencyOperations: {
    title: 'Валютные операции',
    isVisible: true,
    isCollapsible: false,
    order: 11
  },
  currencyControl: {
    title: 'Валютный контроль',
    isVisible: true,
    isCollapsible: false,
    isCollapsed: false,
    order: 12
  }
};

const initialDictionaries = {
  payerAccount: { items: [], searchValue: '', isSearching: false },
  chargesAccount: { items: [], searchValue: '', isSearching: false },
  currCodes: { items: [] },
  currencyOperationCurType: { items: [], searchValue: '', isSearching: false },
  transferSumCur: { items: [], searchValue: '', isSearching: false },
  transferChargedSumCur: { items: [], searchValue: '', isSearching: false },
  clearingCodes: { items: [] },
  iMeBankClearCodeShort: { items: [], searchValue: '', isSearching: false },
  beBankClearCodeShort: { items: [], searchValue: '', isSearching: false },
  senderOfficials: { items: [], searchValue: '', isSearching: false },
  paymentDetails: { items: [], searchValue: '', isSearching: false },
  foreignBanks: { items: [] },
  benefBankSWIFT: { items: [], searchValue: '', isSearching: false },
  comissionTypes: { items: [] },
  beneficiaryCountries: { items: [], searchValue: '', isSearching: false },
  beneficiaryBankCountries: { items: [], searchValue: '', isSearching: false },
  intermidiaryBankCountries: { items: [], searchValue: '', isSearching: false },
  operCode: { items: [], searchValue: '', isSearching: false },
  benefAccount: { items: [], searchValue: '', isSearching: false },
  benefName: { items: [], searchValue: '', isSearching: false },
  benefSWIFT: { items: [], searchValue: '', isSearching: false },
  iMediaBankSWIFT: { items: [], searchValue: '', isSearching: false },
  dealPassNum1: { items: [], searchValue: '', isSearching: false },
  dealPassNum4: { items: [], searchValue: '', isSearching: false },
  dealPassNum5: { items: [], searchValue: '', isSearching: false },
  templates: { items: [], searchValue: '', isSearching: false }
};

export const initialOperations = [
  {
    id: 'back',
    title: 'Назад',
    icon: 'back',
    disabled: false,
    progress: false
  },
  {
    id: 'save',
    title: 'Сохранить',
    icon: 'save',
    disabled: false,
    progress: false
  },
  {
    id: 'signAndSend',
    title: 'Подписать и отправить',
    icon: 'sign-and-send',
    disabled: false,
    progress: false
  },
  {
    id: 'sign',
    title: 'Подписать',
    icon: 'sign',
    disabled: false,
    progress: false
  },
  {
    id: 'unarchive',
    title: 'Вернуть из архива',
    icon: 'unarchive',
    disabled: false,
    progress: false
  },
  {
    id: 'repeat',
    title: 'Повторить',
    icon: 'repeat',
    disabled: false,
    progress: false
  },
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false
  },
  {
    id: 'recall',
    title: 'Отозвать',
    icon: 'recall',
    disabled: false,
    progress: false
  },
  {
    id: 'send',
    title: 'Отправить',
    icon: 'send',
    disabled: false,
    progress: false
  },
  {
    id: 'history',
    title: 'История статустов',
    icon: 'history',
    disabled: false,
    progress: false
  },
  {
    id: 'archive',
    title: 'Переместить в архив',
    icon: 'archive',
    disabled: false,
    progress: false
  },
  {
    id: 'visa',
    title: 'Виза',
    icon: 'visa',
    disabled: false,
    progress: false
  },
  {
    id: 'checkSign',
    title: 'Проверить подпись',
    icon: 'check-sign',
    disabled: false,
    progress: false
  },
  {
    id: 'saveAsTemplate',
    title: 'Сохранить как шаблон',
    icon: 'save-as-template',
    disabled: false,
    progress: false
  }
];

export const initialDocumentTabs = [
  {
    id: 'main',
    title: 'Основные поля',
    isActive: true
  },
  {
    id: 'notes',
    title: 'Заметки',
    isActive: false
  },
  {
    id: 'info',
    title: 'Информация из банка',
    isActive: false
  }
];

export const initialDocumentHistory = {
  isVisible: false,
  items: []
};

export const initialDocumentSignatures = {
  isVisible: false,
  items: []
};

const initialState = fromJS({
  isMounted: false,
  formData: {
    currencyOperations: []
  },
  constraints: {},
  currentCurrencyOperation: {
    entity: null
  },
  dictionaries: initialDictionaries,
  documentTabs: initialDocumentTabs,
  documentHistory: initialDocumentHistory,
  documentSignatures: initialDocumentSignatures,
  formSections: initialFormSections,
  operations: initialOperations,
  activeFormField: null,
  documentErrors: []
});


export const mount = () => initialState.set('isMounted', true);

export const unmount = () => initialState.set('isMounted', false);

function currencyPaymentFetchDataSuccess(state, data) {
  return state.mergeDeep(fromJS(data));
}

function currencyPaymentDictionarySearch(state, { dictionaryId, dictionarySearchValue }) {
  return state
    .setIn(['dictionaries', dictionaryId, 'searchValue'], dictionarySearchValue)
    .setIn(['dictionaries', dictionaryId, 'isSearching'], true);
}

function currencyPaymentUpdateDictionaryValues(state, { dictionaryId, values }) {
  return state
    .setIn(['dictionaries', dictionaryId, 'items'], fromJS(values))
    .setIn(['dictionaries', dictionaryId, 'isSearching'], false);
}

function currencyOperationNew(state) {
  return state.setIn(['currentCurrencyOperation', 'entity'], emptyCurrencyOperation);
}

function currencyOperationCopy(state, { id }) {
  const operationToCopy =
    state.getIn(['formData', 'currencyOperations']).find(operation => operation.get('id') === id);
  const operation = operationToCopy.delete('id');
  return state.setIn(['currentCurrencyOperation', 'entity'], operation);
}

function currencyOperationEdit(state, { id }) {
  const operationToEdit =
    state.getIn(['formData', 'currencyOperations']).find(operation => operation.get('id') === id);
  return state.setIn(['currentCurrencyOperation', 'entity'], operationToEdit);
}

function currencyOperationClear(state) {
  return state.setIn(['currentCurrencyOperation', 'entity'], null);
}

function currencyOperationSave(state) {
  const data = state.getIn(['currentCurrencyOperation', 'entity']);
  const operations = state.getIn(['formData', 'currencyOperations']);

  let newOperations = fromJS([]);
  if (typeof data.get('id') !== 'undefined') {
    const index = operations.findIndex(operation => operation.get('id') === data.get('id'));
    newOperations = operations.update(index, () => data);
  } else {
    const newOperationId = operations.size
      ? operations.last().get('id') + 1
      : 1;
    newOperations = operations.push(data.set('id', newOperationId));
  }

  return state.setIn(['formData', 'currencyOperations'], newOperations);
}

function currencyOperationDelete(state, { id }) {
  const operations = state.getIn(['formData', 'currencyOperations']);
  const newOperations = operations.filter(operation => operation.get('id') !== id);
  return state.setIn(['formData', 'currencyOperations'], newOperations);
}

function currencyOperationChangeField(state, { fieldName, value }) {
  return state.setIn(['currentCurrencyOperation', 'entity', fieldName], value);
}

export const sectionCollapseToggle = (state, { sectionId, isCollapsed }) =>
  state.setIn(['formSections', sectionId, 'isCollapsed'], isCollapsed);

function curPayChangeDataPayerAccount(state, value) {
  const accData = state
    .getIn(['dictionaries', 'payerAccount', 'items'])
    .find(acc => acc.get('accNum') === value);

  const accCurrCode = accData && accData.get('currCode');
  const isMultiCurr = state.getIn(['formData', 'moneyInfo', 'multiCurr']);

  let result = state
    .setIn(['formData', 'payer', 'account'], value)
    // код валюты списания всегда должен соответствовать коду выбранного счета
    .setIn(['formData', 'moneyInfo', 'currOffCode'], accCurrCode);

  if (!isMultiCurr) { // если не мультивалютный, то валюта перевода = валюте аккаунта
    result = result.setIn(['formData', 'moneyInfo', 'currTransCode'], accCurrCode);
  }

  return result;
}

function curPayChangeDataMultiCurr(state, value) {
  return state
    .setIn(['formData', 'moneyInfo', 'multiCurr'], value)
    // при изменении статуса мультивалютности платежа
    // чистим сумму перевода и сумму списания
    .setIn(['formData', 'moneyInfo', 'amountTrans'], null)
    .setIn(['formData', 'moneyInfo', 'amountOff'], null);
}

function curPayChangeDataSenderOfficials(state, value) {
  const result = state.setIn(['formData', 'officials', 'senderOfficials'], value);

  const dictionary = state
    .getIn(['dictionaries', 'SenderOfficials', 'items'])
    .find(sender => sender.get('fio') === value);
  const phone = dictionary ? dictionary.get('phone') : '';

  return result.setIn(['formData', 'officials', 'phoneOfficials'], phone);
}

export const changeTab = (state, { tabId }) => state.merge({
  documentTabs: state.get('documentTabs').map(tab => tab.set('isActive', tab.get('id') === tabId))
});

export const fieldFocus = (state, { fieldId }) => state.set('activeFormField', fieldId);

export const fieldBlur = (state, { fieldId }) => {
  switch (fieldId) {
    default:
      return state.set('activeFormField', null);
  }
};

export const operationHistoryRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', true) : operation))
});

export const operationHistorySuccess = (state, payload) => state.merge({
  documentHistory: {
    isVisible: true,
    items: payload
  },
  documentSignatures: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});

export const operationHistoryFail = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});


function currencyPaymentChangeData(state, { fieldName, value }) {
  const dictionaries = state.get('dictionaries');

  switch (fieldName) {
    case FIELDS.SENDER_OFFICIALS:
      return curPayChangeDataSenderOfficials(state, value);
    case FIELDS.DOC_DATE:
      return state.setIn(['formData', 'docInfo', 'docDate'], value);
    case FIELDS.PAY_UNTIL_DATE:
      return state.setIn(['formData', 'docInfo', 'payUntilDate'], value);
    case FIELDS.PHONE_OFFICIALS:
      return state.setIn(['formData', 'officials', 'phoneOfficials'], value);
    case FIELDS.PAYER_ACCOUNT:
      return curPayChangeDataPayerAccount(state, value);
    case FIELDS.PAYER_NAME_INT:
      return state.setIn(['formData', 'payer', 'nameInt'], value);
    case FIELDS.PAYER_BANK_NAME:
      return state.setIn(['formData', 'payer', 'bankName'], value);
    case FIELDS.PAYER_ADDRESS:
      return state.setIn(['formData', 'payer', 'address'], value);
    case FIELDS.AMOUNT_TRANS:
      return state.setIn(['formData', 'moneyInfo', 'amountTrans'], value);
    case FIELDS.TRANSFER_SUM_CUR:
      return state.setIn(['formData', 'moneyInfo', 'currTransCode'], value);
    case FIELDS.MULTI_CURR:
      return curPayChangeDataMultiCurr(state, value);
    case FIELDS.AMOUNT_OFF:
      return state.setIn(['formData', 'moneyInfo', 'amountOff'], value);
    case FIELDS.TRANSFER_CHANGED_SUM_CUR:
      return state.setIn(['formData', 'moneyInfo', 'currOffCode'], value);
    case FIELDS.BENEF_ACCOUNT:
      return state.setIn(['formData', 'beneficiar', 'benefAccount'], value);
    case FIELDS.BENEF_SWIFT:
      return state.setIn(['formData', 'beneficiar', 'benefSWIFT'], value);
    case FIELDS.BENEF_NAME:
      return state.setIn(['formData', 'beneficiar', 'benefName'], value);
    case FIELDS.BENEF_PLACE:
      return state.setIn(['formData', 'beneficiar', 'benefPlace'], value);
    case FIELDS.BENEFICIARY_COUNTRIES:
      return state.setIn(['formData', 'beneficiar', 'benefCountryCode'], value);
    case FIELDS.BENEF_ADDRESS:
      return state.setIn(['formData', 'beneficiar', 'benefAddress'], value);
    case FIELDS.BENEFICIARY_COUNTRY_NAME:
      return state.setIn(['formData', 'beneficiar', 'benefCountryName'], value);
    case FIELDS.BENEF_BANK_SWIFT:
      return state.setIn(['formData', 'benefBank', 'SWIFT'], value);
    case FIELDS.BE_BANK_CLEAR_CODE_SHORT:
      return state.setIn(['formData', 'benefBank', 'clearCodeShort'], value);
    case FIELDS.BE_BANK_CLEAR_CODE_02:
      return state.setIn(['formData', 'benefBank', 'clearCode02'], value);
    case FIELDS.BE_BANK_CLEAR_CODE:
      return state.setIn(['formData', 'benefBank', 'clearCode'], value);
    case FIELDS.BENEF_BANK_NAME:
      return state.setIn(['formData', 'benefBank', 'name'], value);
    case FIELDS.BENEF_BANK_PLACE:
      return state.setIn(['formData', 'benefBank', 'place'], value);
    case FIELDS.BENEFICIARY_BANK_COUNTRIES:
      return state.setIn(['formData', 'benefBank', 'countryCode'], value);
    case FIELDS.BENEF_BANK_CORR_ACCOUNT:
      return state.setIn(['formData', 'benefBank', 'corrAccount'], value);
    case FIELDS.BENEF_BANK_FILIAL_NAME:
      return state.setIn(['formData', 'benefBank', 'filialName'], value);
    case FIELDS.BENEF_BANK_ADDRESS:
      return state.setIn(['formData', 'benefBank', 'address'], value);
    case FIELDS.BENEF_BANK_COUNTRY_NAME:
      return state.setIn(['formData', 'benefBank', 'countryName'], value);
    case FIELDS.I_MEDIA_BANK_SWIFT:
      return state.setIn(['formData', 'iMediaBank', 'SWIFT'], value);
    case FIELDS.I_ME_BANK_CLEAR_CODE_SHORT:
      return state.setIn(['formData', 'iMediaBank', 'clearCodeShort'], value);
    case FIELDS.I_ME_BANK_CLEAR_CODE_02:
      return state.setIn(['formData', 'iMediaBank', 'clearCode02'], value);
    case FIELDS.I_ME_BANK_CLEAR_CODE:
      return state.setIn(['formData', 'iMediaBank', 'clearCode'], value);
    case FIELDS.I_MEDIA_BANK_NAME:
      return state.setIn(['formData', 'iMediaBank', 'name'], value);
    case FIELDS.I_MEDIA_BANK_PLACE:
      return state.setIn(['formData', 'iMediaBank', 'place'], value);
    case FIELDS.INTERMEDIARY_BANK_COUNTRIES:
      return state.setIn(['formData', 'iMediaBank', 'countryCode'], value);
    case FIELDS.I_MEDIA_BANK_ADDRESS:
      return state.setIn(['formData', 'iMediaBank', 'address'], value);
    case FIELDS.I_MEDIA_BANK_COUNTRY_NAME:
      return state.setIn(['formData', 'iMediaBank', 'countryName'], value);
    case FIELDS.PAYMENT_DETAILS:
      return state.setIn(['formData', 'docInfo', 'paymentDetails'], value);
    case FIELDS.I_MEDIA_INFO:
      return state.setIn(['formData', 'iMediaBank', 'info'], value);
    case FIELDS.CHARGES_ACCOUNT:
      return state.setIn(['formData', 'chargesInfo', 'chargesAccount'], value);
    case FIELDS.CHARGES_BANK_BIC:
      return state.setIn(['formData', 'chargesInfo', 'chargesBankBIC'], value);
    case FIELDS.CHARGES_BANK_NAME:
      return state.setIn(['formData', 'chargesInfo', 'chargesBankName'], value);
    case FIELDS.TAX_CONFIRM_PAY_DOC:
      return state.setIn(['formData', 'docInfo', 'branchShortName'], value);
    case FIELDS.RESIDENT_TYPE:
      return state.setIn(['formData', 'docInfo', 'residentType'], value);
    case FIELDS.CURR_CONTROL_INFO:
      return state.setIn(['formData', 'docInfo', 'currControlInfo'], value);
    case FIELDS.CHARGES_TYPE:
      return state
        .setIn(['formData', 'chargesInfo', 'chargesType'], value)
        .setIn(
          ['formData', 'chargesInfo', 'chargesTypeInfo'],
          dictionaries.getIn(['comissionTypes', 'items']).find(item => item.get('code') === value).get('desc')
        );
    case 'docNote':
      return state
        .setIn(['formData', 'docInfo', 'docNote'], value);

    default: return state;
  }
}

export const operationCloseDocumentTop = state => state.merge({
  documentHistory: {
    isVisible: false,
    items: []
  },
  documentSignatures: {
    isVisible: false,
    items: []
  }
});

export const operationCheckSignSuccess = (state, payload) => state.merge({
  documentSignatures: {
    isVisible: true,
    items: payload
  },
  documentHistory: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', false) : operation))
});

export const operationSaveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSaveFinish = state => state.mergeDeep({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const saveDataSuccess = (state, { recordID, status, allowedSmActions, errors }) => state.merge({
  documentData: state.get('formData').merge({
    id: recordID,
    status,
    allowedSmActions
  }),
  documentErrors: errors
});

export const saveDataFail = (state, { error }) => state.merge({
  documentErrors: error.name === 'RBO Controls Error' ? error.stack : state.get('documentErrors')
});

export default function currencyPayment(state = initialState, { type, payload }) {
  switch (type) {
    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_MOUNT:
      return mount();

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_UNMOUNT:
      return unmount();

    case ACTION_TYPES.CURRENCY_PAYMENT_FETCH_DATA_SUCCESS:
      return currencyPaymentFetchDataSuccess(state, payload.data);

    case ACTION_TYPES.CURRENCY_PAYMENT_DICTIONARY_SEARCH:
      return currencyPaymentDictionarySearch(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_UPDATE_DICTIONARY_VALUES:
      return currencyPaymentUpdateDictionaryValues(state, payload);

    case ACTION_TYPES.CURRENCY_OPERATION_NEW:
      return currencyOperationNew(state);

    case ACTION_TYPES.CURRENCY_OPERATION_COPY:
      return currencyOperationCopy(state, payload);

    case ACTION_TYPES.CURRENCY_OPERATION_EDIT:
      return currencyOperationEdit(state, payload);

    case ACTION_TYPES.CURRENCY_OPERATION_CLEAR:
      return currencyOperationClear(state);

    case ACTION_TYPES.CURRENCY_OPERATION_SAVE:
      return currencyOperationSave(state);

    case ACTION_TYPES.CURRENCY_OPERATION_DELETE_APPROVED:
      return currencyOperationDelete(state, payload);

    case ACTION_TYPES.CURRENCY_OPERATION_CHANGE_FIELD:
      return currencyOperationChangeField(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_HISTORY_REQUEST:
      return operationHistoryRequest(state);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_HISTORY_SUCCESS:
      return operationHistorySuccess(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_HISTORY_FAIL:
      return operationHistoryFail(state);

    case ACTION_TYPES.CURRENCY_PAYMENT_CHANGE_DATA:
      return currencyPaymentChangeData(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SECTION_COLLAPSE_TOGGLE:
      return sectionCollapseToggle(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_CHANGE_TAB:
      return changeTab(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_FIELD_FOCUS:
      return fieldFocus(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_FIELD_BLUR:
      return fieldBlur(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_SUCCESS:
      return saveDataSuccess(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_FAIL:
      return saveDataFail(state, payload);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_REQUEST:
      return operationSaveRequest(state);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_SUCCESS:
    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_FAIL:
      return operationSaveFinish(state);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_CLOSE_DOCUMENT_TOP:
      return operationCloseDocumentTop(state);

    case ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS:
      return operationCheckSignSuccess(state, payload);

    default:
      return state;
  }
}
