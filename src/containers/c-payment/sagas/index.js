import { take, takeLatest, put, call, all, select } from 'redux-saga/effects';
import { goBack, push, replace } from 'react-router-redux';
import { confirmRequestAction } from 'dal/confirm/action-creators';
import { DOC_STATUSES } from 'constants';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import { signDocAddAndTrySigningAction as dalSignAction } from 'dal/sign/actions';
import {
  printSignature as dalSignaturePrint,
  downloadSignature as dalSignatureDownload,
} from 'dal/sign/sagas/operations-saga';
import {
  showTemplateModalAction as dalShowTemplateModalAction,
  saveAsTemplateFailAction as dalSaveAsTemplateFailAction
}  from 'dal/template/actions';
import { smoothScrollTo } from 'utils';
import {
  getCurrencyCodes as dalGetCurrCodes,
  getClearingCodes as dalGetClearingCodes,
  getOfficials as dalGetOfficials,
  getPaymentPurposes as dalGetPaymentPurposes,
  getForeignBanks as dalGetForeignBanks,
  getCommissionTypes as dalGetCommissionTypes,
  getCountries as dalGetCountries,
  getCurrencyOperationCodes as dalGetCurrencyOperationCodes,
  getDealPassNums as dalGetDealPassNums,
  getContractTypes as dalGetContractTypes,
  getResidentAttributes as dalGetResidentAttributes,
} from 'dal/dictionaries/sagas';

import {
  archive as dalOperationArchive,
  unarchive as dalOperationUnarchive,
  remove as dalOperationRemove,
  sendToBank as dalOperationSend,
  stateHistoryById as dalOperationHistory,
  signsCheckById as dalOperationSignsCheck
} from 'dal/documents/sagas';

import {
  getPaymentData as dalGetData,
  fetchDefaultCurPayment as dalFetchDefaultCurPayment,
  fetchCurPayment as dalFetchCurPayment,
  saveCurtransfers as dalSaveData,
  validate as dalFetchErrors,
  printPayment as dalOperationPrint,
  savePaymentAsTemplate as dalOperationSaveAsTemplate,
  fetchConstraints as dalFetchConstraints
} from 'dal/curtransfers/sagas';

import {
  getBeneficiars as dalGetBeneficiars
} from 'dal/beneficiaries/sagas';

import {
  getAccounts as dalGetAccounts
} from 'dal/accounts/sagas';

import {
  currencyPaymentFetchDataSuccess,
  currencyPaymentUpdateDictionaryValuesAction,
  dictionarySearchAction,
  currencyPaymentChangeDataAction
} from '../action-creators';

import {
  ACTION_TYPES, LOCAL_ROUTER_ALIASES,
  LOCAL_REDUCER as REDUCER
} from '../constants';

const formDataSelector = state => state.getIn([REDUCER, 'formData']);

function* currencyPaymentMountSaga(action) {
  const { payload: { params: { id } } } = action;
  yield call(currencyPaymentGetDataSaga, id);
}

// Map методов для поиска по справочникам
const dictionaryMethodsMap = {
  payerAccount: accNum => dalGetAccounts({ payload: { accNum } }),
  beneficiaryCountries: code => dalGetCountries({ code }),
  beneficiaryBankCountries: code => dalGetCountries({ code }),
  intermidiaryBankCountries: code => dalGetCountries({ code }),
  operCode: dalGetCurrencyOperationCodes,
  senderOfficials: dalGetOfficials,
  paymentDetails: purpose => dalGetPaymentPurposes({ purpose }),
  transferSumCur: every => dalGetCurrCodes({ payload: { every } }),
  transferChargedSumCur: every => dalGetCurrCodes({ payload: { every } }),
  currencyOperationCurType: every => dalGetCurrCodes({ payload: { every } }),
  benefAccount: benefAccount => dalGetBeneficiars({ payload: { benefAccount } }),
  benefName: benefName => dalGetBeneficiars({ payload: { benefName } }),
  benefSWIFT: bicInt => dalGetForeignBanks({ bicInt }),
  benefBankSWIFT: bicInt => dalGetForeignBanks({ bicInt }),
  iMediaBankSWIFT: bicInt => dalGetForeignBanks({ bicInt }),
  beBankClearCodeShort: shortName => dalGetClearingCodes({ shortName }),
  iMeBankClearCodeShort: shortName => dalGetClearingCodes({ shortName }),
  chargesAccount: accNum => dalGetAccounts({ payload: { type: '1', accNum } }),
  dealPassNum1: number => dalGetDealPassNums({ number })
};

export const documentDataSelector = state => state.getIn([REDUCER, 'formData']);
export const docIdSelector = state => state.getIn([REDUCER, 'formData', 'recordID']);

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const documentSignaturesSelector = state => state.getIn([REDUCER, 'documentSignatures']);


export function* successNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'success',
    title: title || 'Операция выполнена успешно',
    message: message || ''
  }));
}

export function* warningNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'warning',
    title: title || '',
    message: message || ''
  }));
}

export function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

export function* confirmNotification(params) {
  const { level, title, message, success, cancel } = params;
  yield put(dalNotificationAction({
    type: 'confirm',
    level: level || 'success',
    title: title || '',
    message: message || '',
    success,
    cancel
  }));
}

export function* refreshData() {
  const id = yield select(docIdSelector);
  try {
    return yield call(dalGetData, { payload: { id } });
  } catch (error) {
    throw error;
  }
}


function fetchPayment(id) {
  if (id === 'new') {
    return dalFetchDefaultCurPayment();
  }
  return dalFetchCurPayment(id);
}

function* fetchDictionaries(paymentData) {
  const payerAccount = paymentData.payer && paymentData.payer.account;

  const currTransCode = paymentData.moneyInfo && paymentData.moneyInfo.currTransCode;
  const currOffCode = paymentData.moneyInfo && paymentData.moneyInfo.currOffCode;
  const beneficiaryCountry = paymentData.beneficiar && paymentData.beneficiar.benefCountryCode;
  const beneficiaryBankCountry = paymentData.benefBank && paymentData.benefBank.countryCode;
  const intermidiaryBankCountry = paymentData.iMediaBank && paymentData.iMediaBank.countryCode;

  const [
    PayerAccount,
    ChargesAccount,
    currCodes,
    transferSumCur,
    transferChargedSumCur,
    clearingCodes,
    SenderOfficials,
    paymentPurposes,
    foreignBanks,
    comissionTypes,
    beneficiaryCountries,
    beneficiaryBankCountries,
    intermidiaryBankCountries,
    operCode,
    beneficiaries,
    dealPassNum1,
    dealPassNum4,
    dealPassNum5
  ] = yield all([
    dictionaryMethodsMap.payerAccount(payerAccount),
    dictionaryMethodsMap.chargesAccount(),
    dalGetCurrCodes(),
    dictionaryMethodsMap.transferSumCur(currTransCode),
    dictionaryMethodsMap.transferChargedSumCur(currOffCode),
    dalGetClearingCodes(),
    dalGetOfficials(),
    dalGetPaymentPurposes(),
    dalGetForeignBanks(),
    dalGetCommissionTypes(),
    dictionaryMethodsMap.beneficiaryCountries(beneficiaryCountry),
    dictionaryMethodsMap.beneficiaryBankCountries(beneficiaryBankCountry),
    dictionaryMethodsMap.intermidiaryBankCountries(intermidiaryBankCountry),
    dalGetCurrencyOperationCodes(),
    dalGetBeneficiars({}),
    dalGetDealPassNums(),
    dalGetContractTypes(),
    dalGetResidentAttributes()
  ]);

  return {
    payerAccount: { items: PayerAccount, searchValue: '', isSearching: false },
    chargesAccount: { items: ChargesAccount, searchValue: '', isSearching: false },
    operCode: { items: operCode, searchValue: '', isSearching: false },
    senderOfficials: { items: SenderOfficials, searchValue: '', isSearching: false },
    paymentDetails: { items: paymentPurposes, searchValue: '', isSearching: false },
    comissionTypes: { items: comissionTypes, searchValue: '', isSearching: false },
    dealPassNum1: { items: dealPassNum1, searchValue: '', isSearching: false },
    dealPassNum4: { items: dealPassNum4, searchValue: '', isSearching: false },
    dealPassNum5: { items: dealPassNum5, searchValue: '', isSearching: false },
    transferSumCur: { items: transferSumCur, searchValue: '', isSearching: false },
    transferChargedSumCur: { items: transferChargedSumCur, searchValue: '', isSearching: false },
    // clearingCodes
    clearingCodes: { items: clearingCodes, searchValue: '', isSearching: false },
    beBankClearCodeShort: { items: clearingCodes, searchValue: '', isSearching: false },
    iMeBankClearCodeShort: { items: clearingCodes, searchValue: '', isSearching: false },
    // beneficiaries
    benefAccount: { items: beneficiaries, searchValue: '', isSearching: false },
    benefName: { items: beneficiaries, searchValue: '', isSearching: false },
    // foreignBanks
    foreignBanks: { items: foreignBanks, searchValue: '', isSearching: false },
    benefSWIFT: { items: foreignBanks, searchValue: '', isSearching: false },
    benefBankSWIFT: { items: foreignBanks, searchValue: '', isSearching: false },
    iMediaBankSWIFT: { items: foreignBanks, searchValue: '', isSearching: false },
    // currCodes
    currCodes: { items: currCodes, searchValue: '', isSearching: false },
    currencyOperationCurType: { items: currCodes, searchValue: '', isSearching: false },
    // countries
    beneficiaryCountries: { items: beneficiaryCountries, searchValue: '', isSearching: false },
    beneficiaryBankCountries: { items: beneficiaryBankCountries, searchValue: '', isSearching: false },
    intermidiaryBankCountries: { items: intermidiaryBankCountries, searchValue: '', isSearching: false }
  };
}

function* currencyPaymentGetDataSaga(id) {
  const paymentData = yield call(fetchPayment, id);

  const [
    constraintsRequest,
    dictionaries,
    documentErrors
  ] = yield all([
    call(dalFetchConstraints),
    call(fetchDictionaries, paymentData),
    dalFetchErrors(paymentData)
  ]);

  // данные об ограничениях полей
  // массив массивов смаплен в объект для удобного поиска в стейте по id поля
  const constraints = constraintsRequest
    .map(constrArray => constrArray[0])
    .reduce((_obj, current) => {
      const obj = _obj;
      obj[current.fieldName] = current;
      return obj;
    }, {});

  const formData = {
    ...paymentData,
    status: paymentData.docInfo.status || DOC_STATUSES.NEW,
    currencyOperations: []
  };

  yield put(currencyPaymentFetchDataSuccess({
    formData,
    dictionaries,
    constraints,
    documentErrors
  }));
}

export function* operationRecallSaga() {
  const docId = yield select(docIdSelector);
  yield put(push(LOCAL_ROUTER_ALIASES.CURRENCY_PAYMENT_RECALL_NEW.replace('{:id}', docId)));
}

export function* operationSaga(action) {
  const { payload: { operationId } } = action;
  switch (operationId) {
    case 'back':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_BACK });
      break;
    case 'save':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_REQUEST });
      break;
    case 'signAndSend':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST });
      break;
    case 'sign':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_REQUEST });
      break;
    case 'send':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST });
      break;
    case 'repeat':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REPEAT });
      break;
    case 'print':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_PRINT_REQUEST });
      break;
    case 'remove':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REMOVE_REQUEST });
      break;
    case 'recall':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_RECALL });
      break;
    case 'archive':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_ARCHIVE_REQUEST });
      break;
    case 'unarchive':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_REQUEST });
      break;
    case 'visa':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_REQUEST });
      break;
    case 'saveAsTemplate':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_REQUEST });
      break;
    case 'history':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_HISTORY_REQUEST });
      break;
    case 'checkSign':
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_REQUEST });
      break;
    default:
      break;
  }
}

export function* operationBackSaga() {
  const location = yield select(routingLocationSelector);
  const prevLocation = location.get('prevLocation');
  if (prevLocation && prevLocation.get('pathname') !== location.get('pathname')) yield put(goBack());
  else yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
}

function cropDictionarySearchValue(id, value) {
  if (typeof value !== 'string') return value;

  switch (id) {
    // валюты в поиске режем до 3 символов с начала строки
    case 'transferSumCur':
    case 'transferChargedSumCur':
      return value.slice(0, 3);

    // валюты в поиске режем до 3 символов с последнего пробела до конца строки
    case 'beneficiaryCountries':
    case 'beneficiaryBankCountries':
    case 'intermidiaryBankCountries':
      return value.slice(value.lastIndexOf(' ') + 1);

    default:
      return value;
  }
}

function* currencyPaymentDictionarySearchSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  const searchValue = cropDictionarySearchValue(dictionaryId, dictionarySearchValue);
  const newData = yield dictionaryMethodsMap[dictionaryId](searchValue);
  yield put(currencyPaymentUpdateDictionaryValuesAction(dictionaryId, newData));
}

function* currencyPaymentChangeDataMultiCurrSaga(value) {
  if (value) {
    // убираем валюту перевода если выбран мультивалютный перевод
    // делаем в саге, чтобы заодно очистилось поле суммы перевода
    yield put(currencyPaymentChangeDataAction('transferSumCur', null));
    return;
  }

  const currentAccountNum = yield select(state => state.getIn(['currencyPayment', 'formData', 'payer', 'account']));
  const accounts = yield select(state => state.getIn(['currencyPayment', 'dictionaries', 'payerAccount', 'items']));
  const currentAccount = accounts.find(acc => acc.get('accNum') === currentAccountNum);

  if (currentAccount) {
    const accCurrCode = currentAccount.get('currCode');
    yield put(dictionarySearchAction('transferSumCur', accCurrCode));
    yield take(ACTION_TYPES.CURRENCY_PAYMENT_UPDATE_DICTIONARY_VALUES);
    yield put(currencyPaymentChangeDataAction('transferSumCur', accCurrCode));
  } else {
    yield put(currencyPaymentChangeDataAction('transferSumCur', null));
  }
}

export function* routeToRecallSaga(action) {
  const docId = yield select(docIdSelector);
  const { payload: { recallId } } = action;
  yield put(push(LOCAL_ROUTER_ALIASES.CURRENCY_PAYMENT_RECALL.replace('{:id}', docId).replace('{:recallId}', recallId)));
}

function* currencyPaymentChangeDataPayerAccountSaga(value) {
  if (!value) return;

  const payerAccountItems = yield select(state => state.getIn(['currencyPayment', 'dictionaries', 'payerAccount', 'items']));
  const payerAccount = payerAccountItems.find(account => account.get('accNum') === value);

  const orgId = payerAccount.get('orgId');
  const branchId = payerAccount.get('branchId');

  const organization = yield select(state => state
    .getIn(['organizations', 'orgs'])
    .find(org => org.get('id') === orgId)
  );

  const branch = yield select(state => state
    .getIn(['organizations', 'branches'])
    .find(_branch => _branch.get('id') === branchId)
  );

  const intName = organization.get('intName');
  const intAddress = organization.get('intAddress');
  const intPayName = branch.get('intPayName');

  yield all([
    put(currencyPaymentChangeDataAction('PayerNameInt', intName)),
    put(currencyPaymentChangeDataAction('PayerAddress', intAddress)),
    put(currencyPaymentChangeDataAction('PayerBankName', intPayName))
  ]);
}

function* currencyPaymentChangeDataSaga({ payload }) {
  const { fieldName, value } = payload;
  switch (fieldName) {
    case 'multiCurr':
      yield currencyPaymentChangeDataMultiCurrSaga(value);
      break;

    case 'payerAccount':
      yield currencyPaymentChangeDataPayerAccountSaga(value);
      break;

    default:
  }
}

function* currencyOperationDeleteSaga({ payload }) {
  const { id } = payload;

  const successAction = {
    type: ACTION_TYPES.CURRENCY_OPERATION_DELETE_APPROVED,
    payload: { id }
  };

  yield put(confirmRequestAction({
    title: 'Удаление валютной операции',
    message: 'Вы действительно хотите удалить валютную операцию?',
    success: {
      label: 'Удалить',
      action: successAction
    }
  }));
}

export function* operationPrintSaga() {
  const documentData = yield select(documentDataSelector);
  const docId = yield select(docIdSelector);
  const docNumber = documentData.getIn(['docInfo', 'docNumber']);
  const docDate = documentData.getIn(['docInfo', 'docDate']);
  try {
    yield call(dalOperationPrint, {
      payload: [{ docId, docNumber, docDate }]
    });
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_PRINT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_PRINT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно распечатать документ'
    });
  }
}

export function* operationSignAndSendSaga() {
  yield put({
    type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_CONTINUE,
      fail: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL
    }
  });
}

export function* operationSignAndSendContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      channel: {
        cancel: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_CANCEL,
        success: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_SUCCESS,
        fail: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_FAIL
      }
    }));
  }
}

export function* operationSignAndSendSigningDalCancelSaga() {
  yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_CANCEL });
}

export function* operationSignAndSendSigningDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({
      type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_SUCCESS,
      payload: { documentData: result }
    });
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SENDING_REQUEST });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationSignAndSendSigningDalFailSaga() {
  yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
}

export function* operationSignAndSendSendingRequestSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationSend, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно подписан и отправлен в банк.' });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, error.name === 'RBO Error' ? error
      : {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно отправить документ в банк'
      }
    );
  }
}

export function* operationSignSaga() {
  yield put({
    type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_CONTINUE,
      fail: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL
    }
  });
}

export function* operationSignContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      channel: {
        cancel: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_CANCEL,
        success: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_SUCCESS,
        fail: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_FAIL
      }
    }));
  }
}

export function* operationSignDalCancelSaga() {
  yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_CANCEL });
}

export function* operationSignDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_SUCCESS, payload: { documentData: result } });
    yield call(confirmNotification, {
      level: 'success',
      title: 'Документ успешно подписан.',
      success: {
        label: 'Отправить',
        action: { type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST }
      }
    });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationSignDalFailSaga() {
  yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL });
}

export function* operationRepeatSaga() {
  const docId = yield select(docIdSelector);
  yield put(push({ pathname: LOCAL_ROUTER_ALIASES.CURRENCY_PAYMENT_NEW, search: `?type=repeat&itemId=${docId}` }));
}


export function* operationRemoveSaga() {
  yield call(confirmNotification, {
    level: 'error',
    title: 'Удаление документа',
    message: 'Вы действительно хотите удалить документ?',
    autoDismiss: 0,
    success: {
      label: 'Подтвердить',
      action: { type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REMOVE_CONFIRM }
    },
    cancel: {
      label: 'Отмена',
      action: { type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REMOVE_CANCEL }
    }
  });
}


export function* operationRemoveConfirmSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationRemove, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно удален.' });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REMOVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно удалить документ'
    });
  }
}

export function* operationSendSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationSend, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SEND_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно отправлен в банк.' });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SEND_FAIL });
    yield call(errorNotification, error.name === 'RBO Error' ? error
      : {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно отправить документ в банк'
      }
    );
  }
}

export function* operationArchiveSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationArchive, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ перемещен в архив.' });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_ARCHIVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно переместить документ в архив'
    });
  }
}

export function* operationUnarchiveSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationUnarchive, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ возвращен из архива.' });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно вернуть документ из архива'
    });
  }
}

export function* operationHistorySaga() {
  const docId = yield select(docIdSelector);
  try {
    const result = yield call(dalOperationHistory, docId);
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_HISTORY_SUCCESS, payload: result });
    smoothScrollTo(0);
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_HISTORY_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить историю статустов'
    });
  }
}


export function* operationVisaSaga() {
  yield put({
    type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_CONTINUE,
      fail: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_FAIL
    }
  });
}

export function* operationVisaContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      visa: true,
      channel: {
        cancel: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_DAL_CANCEL,
        success: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_DAL_SUCCESS,
        fail: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_DAL_FAIL
      }
    }));
  }
}

export function* operationVisaDalCancelSaga() {
  yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_CANCEL });
}

export function* operationVisaDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_SUCCESS, payload: { documentData: result } });
    yield call(confirmNotification, {
      level: 'success',
      title: 'Документ успешно подписан.',
      success: {
        label: 'Отправить',
        action: { type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST }
      }
    });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationVisaDalFailSaga() {
  yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_FAIL });
}

export function* operationCheckSignSaga() {
  const docId = yield select(docIdSelector);
  try {
    const result = yield call(dalOperationSignsCheck, docId);
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS, payload: result });
    smoothScrollTo(0);
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно проверить подписи'
    });
  }
}


export function* signaturePrintSaga(action) {
  const docId = yield select(docIdSelector);
  const documentSignatures = yield select(documentSignaturesSelector);
  const { payload: { signId } } = action;
  const signature = documentSignatures.get('items').find(item => item.get('signId') === signId);
  const signDate = signature.get('signDateTime');
  try {
    yield call(dalSignaturePrint, { payload: { docId, signId, signDate } });
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_PRINT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_PRINT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно распечатать подпись'
    });
  }
}

export function* signatureDownloadSaga(action) {
  const docId = yield select(docIdSelector);
  const documentSignatures = yield select(documentSignaturesSelector);
  const { payload: { signId } } = action;
  const signature = documentSignatures.get('items').find(item => item.get('signId') === signId);
  const signDate = signature.get('signDateTime');
  try {
    yield call(dalSignatureDownload, { payload: { docId, signId, signDate } });
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_SUCCESS });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно скачать подпись'
    });
  }
}

export function* operationSaveAsTemplateSaga() {
  yield put(dalShowTemplateModalAction(true));
}

export function* operationSaveAsTemplateContinueSaga(action) {
  const docId = yield select(docIdSelector);
  const { payload: { templateName } } = action;
  try {
    const result = yield call(dalOperationSaveAsTemplate, { payload: { docId, name: templateName } });
    yield put(dalShowTemplateModalAction(false));
    yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_SUCCESS, payload: result });
    yield call(successNotification, {
      title: 'Шаблон успешно сохранен.',
      message: `Шаблон сохранен с именем: ${templateName}`
    });
  } catch (error) {
    if (error.stack) yield put(dalSaveAsTemplateFailAction(error.stack));
    else {
      put(dalShowTemplateModalAction(false));
      yield call(errorNotification, {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно сохранить документ как шаблон'
      });
    }
    yield put({ type: ACTION_TYPES.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_FAIL, payload: { error } });
  }
}


export function* operationSaveSaga() {
  yield put({
    type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_SUCCESS,
      fail: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_FAIL
    }
  });
}

function* saveRequest({ channels }) {
  const documentData = yield select(formDataSelector);
  // todo в allowedSmActions нет поля 'save' когда будет приходить - убрать true
  const allowedSave = documentData.getIn(['allowedSmActions', 'save'], true);
  const status = documentData.get('status');
  if (status === DOC_STATUSES.NEW || allowedSave) {
    try {
      const result = yield call(dalSaveData, { payload: documentData.toJS() });
      if (status === DOC_STATUSES.NEW) yield put(replace(LOCAL_ROUTER_ALIASES.CURRENCY_PAYMENT.replace('{:id}', result.recordID)));
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_SUCCESS, payload: result, channels });
    } catch (error) {
      yield put({ type: ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_FAIL, payload: { error }, channels });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'doNotAllowed' } });
  }
}

export function* saveDataSuccessSaga(action) {
  const { payload: { errors }, channels } = action;
  if (errors && errors.length) {
    if (errors.filter(error => error.level === '2').length) {
      yield put({ type: channels.success, payload: { status: 'haveErrors' } });
    } else if (errors.filter(error => error.level === '1').length) {
      yield put({ type: channels.success, payload: { status: 'haveWarnings' } });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'success' } });
  }
}

export function* operationSaveSuccessSaga(action) {
  const { payload: { status } } = action;
  switch (status) {
    case 'haveErrors':
      yield call(errorNotification, {
        title: 'Валютный перевод сохранен с ошибками.'
      });
      break;
    case 'haveWarnings':
      yield call(warningNotification, {
        title: 'Валютный перевод сохранен с предупреждениями.',
        message: 'Обратите внимание на предупреждения из банка.'
      });
      break;
    default:
      yield call(successNotification, {
        title: 'Валютный перевод успешно сохранен.'
      });
  }
}

export function* saveDataFailSaga(action) {
  const { payload: { error }, channels } = action;
  yield put({ type: channels.fail });
  yield call(errorNotification, error.name === 'RBO Controls Error'
    ? {
      title: 'Валютный перевод не может быть сохранен.',
      message: 'Для сохранения валютного перевода требуется исправить ошибки. ' +
      'При наличии блокирующих ошибок валютный перевод не может быть сохранен.'
    }
    : {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно сохранить валютный перевод'
    }
  );
}

export default function* currencyPaymentSagas() {
  yield takeLatest(ACTION_TYPES.CURRENCY_OPERATION_DELETE, currencyOperationDeleteSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_DICTIONARY_SEARCH, currencyPaymentDictionarySearchSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CHANGE_DATA, currencyPaymentChangeDataSaga);

  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_ROUTE_TO_RECALL, routeToRecallSaga);

  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_MOUNT, currencyPaymentMountSaga);

  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_REQUEST, operationSaveSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_REQUEST, saveRequest);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_SUCCESS, saveDataSuccessSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SAVE_DATA_FAIL, saveDataFailSaga);

  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_RECALL, operationRecallSaga);

  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION, operationSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_BACK, operationBackSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_PRINT_REQUEST, operationPrintSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_REQUEST, operationSignSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_CONTINUE, operationSignContinueSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_CANCEL, operationSignDalCancelSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_SUCCESS, operationSignDalSuccessSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_FAIL, operationSignDalFailSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST, operationSignAndSendSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_CONTINUE, operationSignAndSendContinueSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_CANCEL, operationSignAndSendSigningDalCancelSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_SUCCESS, operationSignAndSendSigningDalSuccessSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_FAIL, operationSignAndSendSigningDalFailSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SENDING_REQUEST, operationSignAndSendSendingRequestSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemoveSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_RECALL, operationRecallSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_REPEAT, operationRepeatSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST, operationSendSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_ARCHIVE_REQUEST, operationArchiveSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_REQUEST, operationUnarchiveSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_HISTORY_REQUEST, operationHistorySaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_REQUEST, operationVisaSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_CONTINUE, operationVisaContinueSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_DAL_CANCEL, operationVisaDalCancelSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_DAL_SUCCESS, operationVisaDalSuccessSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_VISA_DAL_FAIL, operationVisaDalFailSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_REQUEST, operationCheckSignSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_REQUEST, operationSaveAsTemplateSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_CONTINUE, operationSaveAsTemplateContinueSaga);

  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_PRINT_REQUEST, signaturePrintSaga);
  yield takeLatest(ACTION_TYPES.CURRENCY_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_REQUEST, signatureDownloadSaga);
}
