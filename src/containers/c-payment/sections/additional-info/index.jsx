import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldTextarea as FieldTextarea,
  RboFormFieldCheckbox as FieldCheckbox
} from 'components/ui-components/rbo-form-compact';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrAdditionalInfoSection extends Component {
  static propTypes = {
    onFieldChange: PropTypes.func,
    formData: ImmutablePropTypes.map,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  render() {
    const {
      formData,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section id="additionalInfo" title="72: ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ">
        <Blockset>
          <Block>
            <FieldCheckbox
              id="AdditionalIsAny"
              title="Ввести дополнительную информацию"
            />
          </Block>
          <Block>
            <Row>
              <Cell>
                <FieldTextarea
                  id={FIELDS.I_MEDIA_INFO}
                  value={formData.getIn(['iMediaBank', 'info'])}
                  onChange={onFieldChange}
                  placeholder="Необязательное поле"
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.I_MEDIA_INFO)}
                  isError={formErrors.get(FIELDS.I_MEDIA_INFO)}
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default CtrAdditionalInfoSection;
