import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormRowset as Rowset,
  RboFormCell as Cell,
  RboFormFieldSuggest as FieldSuggest,
  RboFormFieldSearch as FieldSearch,
  RboFormFieldText as FieldText,
  RboFormCollapse as Collapse
} from 'components/ui-components/rbo-form-compact';

import { formFieldOptionRenderer } from '../../utils';
import mapStateToProps from './selectors';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrBeneficiaryBankSection extends Component {
  static propTypes = {
    countriesItems: ImmutablePropTypes.list,
    BenefBankSWIFTItems: ImmutablePropTypes.list,
    BeBankClearCodeShortItems: ImmutablePropTypes.list,
    onFieldSearch: PropTypes.func,
    onFieldChange: PropTypes.func,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    getConstraint: PropTypes.func,
    formData: ImmutablePropTypes.map,
    onCollapseToggle: PropTypes.func.isRequired,
    isCollapsed: PropTypes.bool,
    isCollapsible: PropTypes.bool,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  handleCollapseToggle = () => {
    const { isCollapsed, onCollapseToggle } = this.props;
    onCollapseToggle({ sectionId: 'beneficiaryBank', isCollapsed: !isCollapsed });
  };

  render() {
    const {
      countriesItems,
      BenefBankSWIFTItems,
      BeBankClearCodeShortItems,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      getConstraint,
      formData,
      isCollapsed,
      isCollapsible,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section
        id="beneficiaryBank"
        title="57: БАНК БЕНЕФИЦИАРА"
        isCollapsible={isCollapsible}
        isCollapsed={isCollapsed}
      >
        <Blockset>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <FieldSuggest
                    id={FIELDS.BENEF_BANK_SWIFT}
                    label="SWIFT-код"
                    value={formData.getIn(['benefBank', 'SWIFT'])}
                    options={BenefBankSWIFTItems.toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_BANK_SWIFT)}
                    isError={formErrors.get(FIELDS.BENEF_BANK_SWIFT)}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <Collapse
                    pseudoLabel
                    title="Клиринговый код, адрес"
                    isCollapsed={isCollapsed}
                    onCollapseToggle={this.handleCollapseToggle}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
        </Blockset>
        <Blockset>
          <Block>
            <Rowset>
              <Row>
                <Cell size="1/2">
                  <FieldSearch
                    id={FIELDS.BE_BANK_CLEAR_CODE_SHORT}
                    label="Клиринговый код"
                    value={formData.getIn(['benefBank', 'clearCodeShort'])}
                    options={BeBankClearCodeShortItems.toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                    isError={formErrors.get(FIELDS.BE_BANK_CLEAR_CODE_SHORT)}
                  />
                </Cell>
                <Cell size="1/6">
                  <FieldText
                    id={FIELDS.BE_BANK_CLEAR_CODE_02}
                    value={formData.getIn(['benefBank', 'clearCode02'])}
                    getConstraint={getConstraint}
                    onChange={onFieldChange}
                    isWarning={formWarnings.get(FIELDS.BE_BANK_CLEAR_CODE_02)}
                    isError={formErrors.get(FIELDS.BE_BANK_CLEAR_CODE_02)}
                    pseudoLabel
                    isDisabled
                  />
                </Cell>
                <Cell size="1/3">
                  <FieldText
                    id={FIELDS.BE_BANK_CLEAR_CODE_02}
                    placeholder="9!n"
                    label="Значение кода"
                    value={formData.getIn(['benefBank', 'clearCode'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BE_BANK_CLEAR_CODE_02)}
                    isError={formErrors.get(FIELDS.BE_BANK_CLEAR_CODE_02)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.BENEF_BANK_NAME}
                    label="Наименование"
                    value={formData.getIn(['benefBank', 'name'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_BANK_NAME)}
                    isError={formErrors.get(FIELDS.BENEF_BANK_NAME)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell size="2/3">
                  <FieldText
                    id={FIELDS.BENEF_BANK_PLACE}
                    label="Город"
                    value={formData.getIn(['benefBank', 'place'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_BANK_PLACE)}
                    isError={formErrors.get(FIELDS.BENEF_BANK_PLACE)}
                  />
                </Cell>
                <Cell size="1/3">
                  <FieldSearch
                    id={FIELDS.BENEFICIARY_BANK_COUNTRIES}
                    label="Страна"
                    value={formData.getIn(['benefBank', 'countryCode'])}
                    options={countriesItems.toJS()}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEFICIARY_BANK_COUNTRIES)}
                    isError={formErrors.get(FIELDS.BENEFICIARY_BANK_COUNTRIES)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.BENEF_BANK_CORR_ACCOUNT}
                    label="Корр. счет в банке-посреднике"
                    value={formData.getIn(['benefBank', 'corrAccount'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                    isError={formErrors.get(FIELDS.BENEF_BANK_CORR_ACCOUNT)}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.BENEF_BANK_FILIAL_NAME}
                    label="Филиал"
                    value={formData.getIn(['benefBank', 'filialName'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_BANK_FILIAL_NAME)}
                    isError={formErrors.get(FIELDS.BENEF_BANK_FILIAL_NAME)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.BENEF_BANK_ADDRESS}
                    label="Адрес"
                    value={formData.getIn(['benefBank', 'address'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.BENEF_BANK_ADDRESS)}
                    isError={formErrors.get(FIELDS.BENEF_BANK_ADDRESS)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.BENEF_BANK_COUNTRY_NAME}
                    value={formData.getIn(['benefBank', 'countryName'])}
                    getConstraint={getConstraint}
                    onChange={onFieldChange}
                    isWarning={formWarnings.get(FIELDS.BENEF_BANK_COUNTRY_NAME)}
                    isError={formErrors.get(FIELDS.BENEF_BANK_COUNTRY_NAME)}
                    pseudoLabel
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(
  mapStateToProps
)(CtrBeneficiaryBankSection);
