import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import {
  dictionariesSelector,
  clearingCodesItemsSelector,
  constraintSelector
} from '../../selectors';

const countriesItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['beneficiaryBankCountries', 'items'])
    .map(country => fromJS({
      value: country.get('code'),
      title: `${country.get('mnem02')} / ${country.get('code')}`
    }))
);

const BenefBankSWIFTItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['benefBankSWIFT', 'items'])
    .map(bank => fromJS({
      value: bank.get('id'),
      title: bank.get('bicInt'),
      extra: bank.get('countryName'),
      describe: bank.get('name')
    }))
);

const BeBankClearCodeShortItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['beBankClearCodeShort', 'items'])
    .map(code => fromJS({
      value: code.get('shortName'),
      title: code.get('shortName'),
      extra: code.get('clearingCode'),
      describe: `${code.get('countryCode02')} ${code.get('name')}`
    }))
);

export default state => ({
  clearingCodesItems: clearingCodesItemsSelector(state),
  countriesItems: countriesItemsSelector(state),
  BenefBankSWIFTItems: BenefBankSWIFTItemsSelector(state),
  getConstraint: constraintSelector(state),
  BeBankClearCodeShortItems: BeBankClearCodeShortItemsSelector(state)
});
