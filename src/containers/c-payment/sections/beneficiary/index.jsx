import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldSuggest as FieldSuggest,
  RboFormFieldSearch as FieldSearch,
  RboFormFieldText as FieldText,
  RboFormFieldButton as FieldButton,
  RboFormCollapse as Collapse
} from 'components/ui-components/rbo-form-compact';
import { FIELDS } from '../../constants';

import { formFieldOptionRenderer } from '../../utils';

import mapStateToProps from './selectors';

// eslint-disable-next-line
class CtrBeneficiarySection extends Component {
  static propTypes = {
    BenefAccountItems: ImmutablePropTypes.list,
    BenefSWIFTItems: ImmutablePropTypes.list,
    BenefNameItems: ImmutablePropTypes.list,
    countriesItems: ImmutablePropTypes.list,
    onFieldSearch: PropTypes.func,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    onFieldChange: PropTypes.func,
    getConstraint: PropTypes.func,
    formData: ImmutablePropTypes.map,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  render() {
    const {
      BenefAccountItems,
      BenefSWIFTItems,
      BenefNameItems,
      countriesItems,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      getConstraint,
      formData,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section id="beneficiary" title="59: БЕНЕФИЦИАР">
        <Blockset>
          <Block>
            <Row>
              <Cell>
                <FieldSuggest
                  id={FIELDS.BENEF_ACCOUNT}
                  label="Счет №"
                  value={formData.getIn(['beneficiar', 'benefAccount'])}
                  getConstraint={getConstraint}
                  optionRenderer={formFieldOptionRenderer}
                  options={BenefAccountItems.toJS()}
                  onChange={onFieldChange}
                  onSearch={onFieldSearch}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.BENEF_ACCOUNT)}
                  isError={formErrors.get(FIELDS.BENEF_ACCOUNT)}
                />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <FieldSuggest
                  id={FIELDS.BENEF_SWIFT}
                  label="SWIFT BIC"
                  value={formData.getIn(['beneficiar', 'benefSWIFT'])}
                  getConstraint={getConstraint}
                  optionRenderer={formFieldOptionRenderer}
                  options={BenefSWIFTItems.toJS()}
                  onChange={onFieldChange}
                  onSearch={onFieldSearch}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.BENEF_SWIFT)}
                  isError={formErrors.get(FIELDS.BENEF_SWIFT)}
                />
              </Cell>
            </Row>
          </Block>
          <Block>
            <Row>
              <Cell size="1/2">
                <Collapse
                  pseudoLabel
                  title="Реквизиты банка"
                  isCollapsed={false}
                  onCollapseToggle={() => {}}
                />
              </Cell>
              <Cell size="1/2">
                <FieldButton
                  id="BeneficiaryAddToDictionary"
                  pseudoLabel
                  icon="dictionary-add"
                  title="Добавить в справочник"
                  onClick={() => {}}
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
        <Blockset>
          <Block>
            <Row>
              <Cell>
                <FieldSuggest
                  id={FIELDS.BENEF_NAME}
                  label="Наименование"
                  value={formData.getIn(['beneficiar', 'benefName'])}
                  getConstraint={getConstraint}
                  options={BenefNameItems.toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  onChange={onFieldChange}
                  onSearch={onFieldSearch}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.BENEF_NAME)}
                  isError={formErrors.get(FIELDS.BENEF_NAME)}
                />
              </Cell>
            </Row>
            <Row>
              <Cell size="2/3">
                <FieldText
                  id={FIELDS.BENEF_PLACE}
                  label="Город"
                  value={formData.getIn(['beneficiar', 'benefPlace'])}
                  onChange={onFieldChange}
                  getConstraint={getConstraint}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.BENEF_PLACE)}
                  isError={formErrors.get(FIELDS.BENEF_PLACE)}
                />
              </Cell>
              <Cell size="1/3">
                <FieldSearch
                  id={FIELDS.BENEFICIARY_COUNTRIES}
                  label="Страна"
                  value={formData.getIn(['beneficiar', 'benefCountryCode'])}
                  options={countriesItems.toJS()}
                  onChange={onFieldChange}
                  onSearch={onFieldSearch}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.BENEFICIARY_COUNTRIES)}
                  isError={formErrors.get(FIELDS.BENEFICIARY_COUNTRIES)}
                />
              </Cell>
            </Row>
          </Block>
          <Block>
            <Row>
              <Cell>
                <FieldText
                  id={FIELDS.BENEF_ADDRESS}
                  label="Адрес"
                  value={formData.getIn(['beneficiar', 'benefAddress'])}
                  onChange={onFieldChange}
                  getConstraint={getConstraint}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.BENEF_ADDRESS)}
                  isError={formErrors.get(FIELDS.BENEF_ADDRESS)}
                />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <FieldText
                  id={FIELDS.BENEFICIARY_COUNTRY_NAME}
                  pseudoLabel
                  value={formData.getIn(['beneficiar', 'benefCountryName'])}
                  getConstraint={getConstraint}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.BENEFICIARY_COUNTRY_NAME)}
                  isError={formErrors.get(FIELDS.BENEFICIARY_COUNTRY_NAME)}
                  isDisabled
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(
  mapStateToProps
)(CtrBeneficiarySection);
