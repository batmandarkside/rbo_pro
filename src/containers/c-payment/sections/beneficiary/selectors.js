import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import { getFormattedAccNum } from 'utils';
import { dictionariesSelector, constraintSelector } from '../../selectors';

const countriesItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['beneficiaryCountries', 'items'])
    .map(country => fromJS({
      value: country.get('code'),
      title: `${country.get('mnem02')} / ${country.get('code')}`
    }))
);

const BenefAccountItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['benefAccount', 'items'])
    .map(account => fromJS({
      value: account.get('id'),
      title: getFormattedAccNum(account.getIn(['benefInfo', 'benefAccount'])),
      extra: account.getIn(['beBankInfo', 'benefBankSWIFT']),
      describe: account.getIn(['benefInfo', 'benefName'])
    }))
);

const BenefSWIFTItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['benefSWIFT', 'items'])
    .map(bank => fromJS({
      value: bank.get('id'),
      title: bank.get('bicInt'),
      extra: bank.get('countryName'),
      describe: bank.get('name')
    }))
);

const BenefNameItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['benefName', 'items'])
    .map(account => fromJS({
      value: account.get('id'),
      title: account.getIn(['benefInfo', 'benefName']),
      extra: account.getIn(['beBankInfo', 'benefBankSWIFT']),
      describe: getFormattedAccNum(account.getIn(['benefInfo', 'benefAccount']))
    }))
);

export default state => ({
  countriesItems: countriesItemsSelector(state),
  BenefAccountItems: BenefAccountItemsSelector(state),
  BenefSWIFTItems: BenefSWIFTItemsSelector(state),
  BenefNameItems: BenefNameItemsSelector(state),
  getConstraint: constraintSelector(state)
});
