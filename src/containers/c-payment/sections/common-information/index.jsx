import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldSuggest as FieldSuggest,
  RboFormFieldText as FieldText,
  RboFormFieldDate as FieldDate,
  RboFormCollapse as Collapse,
  RboFormRowset as Rowset
} from 'components/ui-components/rbo-form-compact';

import { formFieldOptionRenderer } from '../../utils';
import mapStateToProps from './selectors';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrCommonInformationSection extends Component {
  static propTypes = {
    officialsItems: ImmutablePropTypes.list,
    onFieldSearch: PropTypes.func,
    onFieldChange: PropTypes.func,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    getConstraint: PropTypes.func,
    formData: ImmutablePropTypes.map,
    onCollapseToggle: PropTypes.func.isRequired,
    isCollapsed: PropTypes.bool,
    isCollapsible: PropTypes.bool,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  handleCollapseToggle = () => {
    const { isCollapsed, onCollapseToggle } = this.props;
    onCollapseToggle({ sectionId: 'commonInformation', isCollapsed: !isCollapsed });
  };

  render() {
    const {
      officialsItems,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      getConstraint,
      formData,
      isCollapsed,
      isCollapsible,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section
        id="commonInformation"
        title="ОБЩАЯ ИНФОРМАЦИЯ"
        isCollapsible={isCollapsible}
        isCollapsed={isCollapsed}
      >
        <Blockset>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell size="1/3">
                  <FieldText
                    id={FIELDS.DOC_NUMBER}
                    label="Номер"
                    getConstraint={getConstraint}
                    value={formData.getIn(['docInfo', 'docNumber'])}
                    isWarning={formWarnings.get(FIELDS.DOC_NUMBER)}
                    isError={formErrors.get(FIELDS.DOC_NUMBER)}
                    isDisabled
                  />
                </Cell>
                <Cell size="1/3">
                  <FieldDate
                    id={FIELDS.DOC_DATE}
                    label="Дата"
                    value={formData.getIn(['docInfo', 'docDate'])}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.DOC_DATE)}
                    isError={formErrors.get(FIELDS.DOC_DATE)}
                  />
                </Cell>
                <Cell size="1/3">
                  <FieldDate
                    id={FIELDS.PAY_UNTIL_DATE}
                    label="Дата списания"
                    value={formData.getIn(['docInfo', 'payUntilDate'])}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.PAY_UNTIL_DATE)}
                    isError={formErrors.get(FIELDS.PAY_UNTIL_DATE)}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <Collapse
                    pseudoLabel
                    title="Плательщик, ИНН/КИО, ОКПО"
                    isCollapsed={isCollapsed}
                    onCollapseToggle={this.handleCollapseToggle}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
        </Blockset>
        <Blockset>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <FieldSuggest
                    id={FIELDS.SENDER_OFFICIALS}
                    label="Ответственное лицо"
                    value={formData.getIn(['officials', 'senderOfficials'])}
                    options={officialsItems.toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    getConstraint={getConstraint}
                    isWarning={formWarnings.get(FIELDS.SENDER_OFFICIALS)}
                    isError={formErrors.get(FIELDS.SENDER_OFFICIALS)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.COMMON_PAYER}
                    label="Плательщик"
                    value={formData.getIn(['payer', 'name'])}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.COMMON_PAYER)}
                    isError={formErrors.get(FIELDS.COMMON_PAYER)}
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.COMMON_SERVICE_UNIT}
                    label="Обслуживающее подразделение"
                    value={formData.getIn(['docInfo', 'branchShortName'])}
                    getConstraint={getConstraint}
                    isWarning={formWarnings.get(FIELDS.COMMON_SERVICE_UNIT)}
                    isError={formErrors.get(FIELDS.COMMON_SERVICE_UNIT)}
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.PHONE_OFFICIALS}
                    label="Телефон"
                    value={formData.getIn(['officials', 'phoneOfficials'])}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                    isWarning={formWarnings.get(FIELDS.PHONE_OFFICIALS)}
                    isError={formErrors.get(FIELDS.PHONE_OFFICIALS)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell size="1/2">
                  <FieldText
                    id={FIELDS.COMMON_INN_KIO}
                    label="ИНН/КИО"
                    value={formData.getIn(['payer', 'INN'])}
                    getConstraint={getConstraint}
                    isWarning={formWarnings.get(FIELDS.COMMON_INN_KIO)}
                    isError={formErrors.get(FIELDS.COMMON_INN_KIO)}
                    isDisabled
                  />
                </Cell>
                <Cell size="1/2">
                  <FieldText
                    id={FIELDS.COMMON_OKPO}
                    label="ОКПО"
                    value={formData.getIn(['payer', 'OKPO'])}
                    getConstraint={getConstraint}
                    isWarning={formWarnings.get(FIELDS.COMMON_OKPO)}
                    isError={formErrors.get(FIELDS.COMMON_OKPO)}
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(
  mapStateToProps
)(CtrCommonInformationSection);
