import { createSelector } from 'reselect';
import { dictionariesSelector, constraintSelector } from '../../selectors';

const officialsItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['senderOfficials', 'items'])
    .map(official => ({
      value: official.get('fio'),
      title: official.get('fio'),
      describe: official.get('phone'),
      extra: official.get('position')
    }))
);

export default state => ({
  officialsItems: officialsItemsSelector(state),
  getConstraint: constraintSelector(state)
});
