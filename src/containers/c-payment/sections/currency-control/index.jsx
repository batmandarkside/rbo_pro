import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormRowset as Rowset,
  RboFormCell as Cell,
  RboFormFieldTextarea as FieldTextarea,
  RboFormFieldDropdown as FieldDropdown,
  RboFormCollapse as Collapse
} from 'components/ui-components/rbo-form-compact';
import { FIELDS } from '../../constants';

export default function CtrCurrencyControlSection(props) {
  const residentTypes = [
    {
      value: 'Резидент',
      title: 'Резидент'
    }, {
      value: 'Нерезидент',
      title: 'Нерезидент'
    }
  ];

  const {
    formData,
    onFieldChange,
    onFieldFocus,
    onFieldBlur,
    isCollapsible,
    isCollapsed,
    onCollapseToggle,
    formWarnings,
    formErrors
  } = props;

  const handleCollapseToggle = () => {
    onCollapseToggle({ sectionId: 'currencyControl', isCollapsed: !isCollapsed });
  };

  return (
    <Section
      id="currencyControl"
      title="ВАЛЮТНЫЙ КОНТРОЛЬ"
      isCollapsible={isCollapsible}
      isCollapsed={isCollapsed}
    >
      <Blockset>
        <Block>
          <Rowset isMain>
            <Row>
              <Cell size="1/2">
                <FieldDropdown
                  id={FIELDS.RESIDENT_TYPE}
                  label="Тип контрагента"
                  value={formData.getIn(['docInfo', 'residentType'])}
                  options={residentTypes}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.RESIDENT_TYPE)}
                  isError={formErrors.get(FIELDS.RESIDENT_TYPE)}
                />
              </Cell>
            </Row>
          </Rowset>
          <Rowset>
            <Row>
              <Cell>
                <FieldTextarea
                  id={FIELDS.CURR_CONTROL_INFO}
                  value={formData.getIn(['docInfo', 'currControlInfo'])}
                  onChange={onFieldChange}
                  label="Дополнительная информация для валютного контроля"
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.CURR_CONTROL_INFO)}
                  isError={formErrors.get(FIELDS.CURR_CONTROL_INFO)}
                />
              </Cell>
            </Row>
          </Rowset>
        </Block>
        <Block>
          <Rowset isMain>
            <Row>
              <Cell>
                <Collapse
                  pseudoLabel
                  title="Дополнительная информация для ВК"
                  isCollapsed={isCollapsed}
                  onCollapseToggle={handleCollapseToggle}
                />
              </Cell>
            </Row>
          </Rowset>
        </Block>
      </Blockset>
    </Section>
  );
}

CtrCurrencyControlSection.propTypes = {
  formData: ImmutablePropTypes.map,
  onFieldChange: PropTypes.func,
  isCollapsible: PropTypes.bool,
  isCollapsed: PropTypes.bool,
  onCollapseToggle: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  formWarnings: ImmutablePropTypes.map,
  formErrors: ImmutablePropTypes.map
};
