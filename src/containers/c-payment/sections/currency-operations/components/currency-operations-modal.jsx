import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldset as Fieldset,
  RboFormFieldDropdown as FieldDropdown,
  RboFormFieldSuggest as FieldSuggest,
  RboFormFieldSearch as FieldSearch,
  RboFormFieldText as FieldText,
  RboFormFieldDecimalNumber as DecimalNumber,
  RboFormFieldTextarea as FieldTextarea,
  RboFormFieldCheckbox as FieldCheckbox,
  RboFormFieldRadio as FieldRadio,
  RboFormFieldDate as FieldDate
} from 'components/ui-components/rbo-form-compact';
import Modal from 'ui-components/modal';
import Icon from 'ui-components/icon';

import {
  currencyOperationChangeFieldAction,
  currencyOperationSaveAction
} from '../../../action-creators';
import { formFieldOptionRenderer } from '../../../utils';
import mapStateToProps from '../selectors';

import {
  Button,
  ButtonsRow,
  ModalHeader,
  ModalHeaderTitle,
  ModalHeaderClose,
  ModalBody
} from '../style';

function CurrencyOperationsModal(props) {
  const {
    active,
    save,
    close,
    changeField,
    operationInEdit,
    currencyOperationCurTypeItems,
    onFieldSearch,
    currencyOperationCodeItems,
    dealPassNum1Items,
    dealPassNum4Items,
    dealPassNum5Items
  } = props;

  if (!active) return null; // модальное окно скрыто

  const {
    id,
    code,
    amount,
    curType
  } = operationInEdit.toJS();

  const headerTitle = id ? `Валютная операция № ${id}` : 'Новая валютная операция';

  return (
    <Modal
      onClose={close}
      hideCloseButton
      styleContent={{ width: '904px' }}
    >
      <ModalHeader>
        <ModalHeaderTitle>{headerTitle}</ModalHeaderTitle>
        <ModalHeaderClose>
          <Icon onClick={close} type="close" size="24" />
        </ModalHeaderClose>
      </ModalHeader>
      <ModalBody>
        <Blockset>
          <Block>
            <Row>
              <Cell size="1/3">
                <FieldSearch
                  id="operCode"
                  label="Код вида ВО"
                  options={currencyOperationCodeItems.toJS()}
                  onSearch={onFieldSearch}
                  value={code}
                  onChange={({ fieldValue }) => { changeField('code', fieldValue); }}
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
        <Blockset>
          <Block>
            <Row>
              <Cell>
                <FieldTextarea
                  isDisabled
                  id="nameOperCode"
                  label="Наименование валютной операции 138-И"
                />
              </Cell>
            </Row>
          </Block>
          <Block>
            <Row>
              <Cell size="1/2">
                <DecimalNumber
                  id="partOfVOAmount"
                  label="Сумма операции"
                  value={amount}
                  onChange={({ fieldValue }) => { changeField('amount', parseFloat(fieldValue)); }}
                />
              </Cell>
              <Cell size="1/2">
                <FieldSearch
                  id="currencyOperationCurType"
                  label="Валюта операции"
                  options={currencyOperationCurTypeItems.toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  value={curType}
                  onSearch={onFieldSearch}
                  onChange={({ fieldValue }) => { changeField('curType', fieldValue); }}
                />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <FieldCheckbox
                  isDisabled
                  id="hasContractPs"
                  title="Указать сведения о ПС или контракте"
                  onChange={() => {}}
                  value={false}
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
        <Blockset>
          <Block>
            <Row>
              <Cell>
                <FieldRadio
                  id="currencyOperationTypePassport"
                  group="documentType"
                  value="passport"
                  title="Паспорт сделки"
                  onChange={() => {}}
                />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <Fieldset label="Номер паспорта">
                  <FieldSuggest
                    id="dealPassNum1"
                    size="1/3"
                    popupStyle={{ width: '300px' }}
                    options={dealPassNum1Items.toJS()}
                    onSearch={onFieldSearch}
                    optionRenderer={formFieldOptionRenderer}
                    onChange={() => {}}
                  />
                  <FieldText
                    id="dealPassNum2"
                    size="1/6"
                  />
                  <FieldText
                    id="dealPassNum3"
                    size="1/6"
                  />
                  <FieldDropdown
                    id="dealPassNum4"
                    size="1/6"
                    popupStyle={{ left: 'auto', right: 0, width: '300px' }}
                    optionRenderer={formFieldOptionRenderer}
                    options={dealPassNum4Items.toJS()}
                    onChange={() => {}}
                  />
                  <FieldDropdown
                    id="dealPassNum5"
                    size="1/6"
                    popupStyle={{ left: 'auto', right: 0, width: '300px' }}
                    optionRenderer={formFieldOptionRenderer}
                    options={dealPassNum5Items.toJS()}
                    onChange={() => {}}
                  />
                </Fieldset>
              </Cell>
            </Row>
            <Row>
              <Cell size="1/2">
                <FieldText
                  id="psAmountCurrency"
                  label="Сумма контракта"
                />
              </Cell>
              <Cell size="1/2">
                <FieldDropdown
                  id="psCurr"
                  label="Валюта операции"
                  options={[]}
                />
              </Cell>
            </Row>
            <Row>
              <Cell size="1/2">
                <FieldDate
                  id="expectedPeriod"
                  label="Ожидаемый срок"
                />
              </Cell>
            </Row>
          </Block>
          <Block>
            <Row>
              <Cell>
                <FieldRadio
                  id="currencyOperationTypeNum"
                  group="documentType"
                  value="num"
                  title="Номер / Дата контракта"
                  onChange={() => {}}
                />
              </Cell>
            </Row>
            <Row>
              <Cell size="2/3">
                <FieldText
                  id="contractNumber"
                  label="Номер контракта"
                  onChange={() => {}}
                />
              </Cell>
              <Cell size="1/3">
                <FieldCheckbox
                  pseudoLabel
                  id="contractWithoutNumber"
                  title="Без номера"
                  onChange={() => {}}
                  value={false}
                />
              </Cell>
            </Row>
            <Row>
              <Cell size="1/2">
                <FieldDate
                  id="contractDate"
                  label="Дата"
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
        <ButtonsRow>
          <Button small onClick={() => { save(); close(); /* TODO: remove that) */ }}>Сохранить</Button>
          <Button small white onClick={close}>Отмена</Button>
        </ButtonsRow>
      </ModalBody>
    </Modal>
  );
}

CurrencyOperationsModal.propTypes = {
  active: PropTypes.bool,
  save: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  changeField: PropTypes.func.isRequired,
  operationInEdit: ImmutablePropTypes.map,
  currencyOperationCurTypeItems: ImmutablePropTypes.list,
  currencyOperationCodeItems: ImmutablePropTypes.list,
  dealPassNum1Items: ImmutablePropTypes.list,
  dealPassNum4Items: ImmutablePropTypes.list,
  dealPassNum5Items: ImmutablePropTypes.list
};

export default connect(
  mapStateToProps,
  {
    save: currencyOperationSaveAction,
    changeField: currencyOperationChangeFieldAction
  }
)(CurrencyOperationsModal);
