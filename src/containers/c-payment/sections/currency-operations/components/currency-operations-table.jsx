import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import {
  currencyOperationEditAction,
  currencyOperationCopyAction,
  currencyOperationDeleteAction
} from '../../../action-creators';

import {
  Table,
  TableHead,
  TableRow,
  TableHeadCell,
  TableCell,
  TableCellEmpty,
  ActionIcon
} from '../style';

class CurrencyOperationsTable extends Component {

  static propTypes = {
    operations: ImmutablePropTypes.list.isRequired,
    editAction: PropTypes.func.isRequired,
    copyAction: PropTypes.func.isRequired,
    deleteAction: PropTypes.func.isRequired
  };

  renderBody() {
    const { operations, editAction, copyAction, deleteAction } = this.props;

    if (!operations.size) {
      return (
        <TableRow>
          <TableCellEmpty
            id="currency-operations-empty-table-content"
          >
            Здесь будут показаны ваши валютные операции
          </TableCellEmpty>
        </TableRow>
      );
    }

    return operations.map((operation) => {
      const id = operation.get('id');
      const editId = `currency-operation-edit-${operation.get('id')}`;
      const copyId = `currency-operation-copy-${operation.get('id')}`;
      const removeId = `currency-operation-remove-${operation.get('id')}`;
      return (
        <TableRow key={id}>
          <TableCell>{operation.get('code')}</TableCell>
          <TableCell right>{operation.get('amount').toFixed(2)}</TableCell>
          <TableCell>{operation.get('currencyFull')}</TableCell>
          <TableCell right>
            <ActionIcon
              type="edit"
              size="20"
              id={editId}
              onClick={() => editAction(id)}
            />
            <ActionIcon
              type="copy"
              size="20"
              id={copyId}
              onClick={() => copyAction(id)}
            />
            <ActionIcon
              type="remove"
              size="20"
              id={removeId}
              onClick={() => deleteAction(id)}
            />
          </TableCell>
        </TableRow>
      );
    });
  }

  render() {
    const body = this.renderBody();

    return (
      <Table id="currency-operations-table">
        <tbody>
          <TableHead>
            <TableHeadCell width="120px">Код вида ВО</TableHeadCell>
            <TableHeadCell width="120px" right>Сумма</TableHeadCell>
            <TableHeadCell width="100px">Валюта</TableHeadCell>
            <TableHeadCell />
          </TableHead>
          {body}
        </tbody>
      </Table>
    );
  }
}

const mapStateToProps = () => ({});

export default connect(
  mapStateToProps,
  {
    editAction: currencyOperationEditAction,
    copyAction: currencyOperationCopyAction,
    deleteAction: currencyOperationDeleteAction
  }
)(CurrencyOperationsTable);
