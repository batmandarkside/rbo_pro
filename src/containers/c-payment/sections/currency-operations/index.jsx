import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { RboFormSection } from 'components/ui-components/rbo-form-compact';

import CurrencyOperationsTable from './components/currency-operations-table';
import CurrencyOperationsModal from './components/currency-operations-modal';

import mapStateToProps from './selectors';

import { Button, ControlsLine, TotalText, MoreOperatios } from './style.js';
import {
  currencyOperationNewAction,
  currencyOperationClearAction
} from '../../action-creators';

const VISIBLE_OPERATIONS_COUNT = 5;

class CtrCurrencyOperationsSection extends Component {
  static propTypes = {
    isModalActive: PropTypes.bool.isRequired,
    operations: ImmutablePropTypes.list.isRequired,
    newCurrencyOperation: PropTypes.func,
    clearCurrencyOperation: PropTypes.func,
    onFieldSearch: PropTypes.func,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  state = { isFullList: false };

  getVisibleOperations() {
    const { operations } = this.props;
    const { isFullList } = this.state;

    const visibleOperations = isFullList ? operations : operations.take(VISIBLE_OPERATIONS_COUNT);
    const overAllowedCount = operations.size > VISIBLE_OPERATIONS_COUNT
      ? operations.size - VISIBLE_OPERATIONS_COUNT
      : 0;

    return [visibleOperations, overAllowedCount];
  }

  getNumberLastDigit(number) {
    const numberString = `${number}`; // stringify
    return parseInt(numberString[numberString.length - 1], 10);
  }

  getNumberHandredthPart(number) {
    const numberString = `${number}`; // stringify
    return parseInt(numberString.slice(-2), 10);
  }

  // nominative - Именительный падеж
  getOperationsTextNominative(count) {
    const countHandredth = this.getNumberHandredthPart(count);
    switch (countHandredth < 21 ? countHandredth : countHandredth % 10) {
      case 1:
        return 'операция';
      case 2:
      case 3:
      case 4:
        return 'операции';
      default:
        return 'операций';
    }
  }

  // accusative – Винительный падеж
  getOperationsTextAccusative(count) {
    const countHandredth = this.getNumberHandredthPart(count);
    switch (countHandredth < 21 ? countHandredth : countHandredth % 10) {
      case 1:
        return 'операцию';
      case 2:
      case 3:
      case 4:
        return 'операции';
      default:
        return 'операций';
    }
  }

  getOperationsTotalSum() {
    const { operations } = this.props;
    if (!operations.size) return '';

    const currencyType = operations
      .map(operation => operation.get('currency'))
      .reduce(
        (accum, current) => (accum === current ? current : false),
        operations.first().get('currency')
      );

    // возвращаем сумму, только если есть валютные операции
    // и во всех операциях используется одна валюта
    if (!currencyType) return '';

    const sum = operations
      .map(operation => operation.get('amount'))
      .reduce((prev, current) => prev + current, 0);

    return ` на сумму ${sum.toFixed(2)} ${currencyType}`;
  }

  toggleFullVisible = () => this.setState({ isFullList: !this.state.isFullList });

  render() {
    const {
      isModalActive,
      operations,
      onFieldSearch,
      formWarnings,
      formErrors
    } = this.props;
    const { isFullList } = this.state;

    const [visibleOperations, overAllowedCount] = this.getVisibleOperations();
    const operationsText = this.getOperationsTextNominative(operations.size);
    const operationsTotalSum = this.getOperationsTotalSum();
    const overAllowedText = isFullList
      ? this.getOperationsTextAccusative(overAllowedCount)
      : this.getOperationsTextNominative(overAllowedCount);

    return (
      <RboFormSection id="currencyOperations" title="ВАЛЮТНЫЕ ОПЕРАЦИИ">
        <CurrencyOperationsTable
          operations={visibleOperations}
          formWarnings={formWarnings}
          formErrors={formErrors}
        />
        {!!overAllowedCount &&
          <ControlsLine center>
            <MoreOperatios
              id="toggle-extra-currency-operations"
              onClick={this.toggleFullVisible}
            >
              {isFullList ? 'Скрыть' : '+'} {overAllowedCount} {overAllowedText}
            </MoreOperatios>
          </ControlsLine>
        }
        <ControlsLine>
          {!!(operations.size > 1) &&
            <TotalText id="currency-operations-total-text">
              {operations.size} {operationsText}{operationsTotalSum}
            </TotalText>
          }
          <div>
            <Button
              id="add-currency-operation"
              onClick={this.props.newCurrencyOperation}
            >
              Добавить валютную операцию
            </Button>
          </div>
        </ControlsLine>
        <CurrencyOperationsModal
          active={isModalActive}
          onFieldSearch={onFieldSearch}
          close={this.props.clearCurrencyOperation}
        />
      </RboFormSection>
    );
  }
}

export default connect(
  mapStateToProps,
  {
    newCurrencyOperation: currencyOperationNewAction,
    clearCurrencyOperation: currencyOperationClearAction
  }
)(CtrCurrencyOperationsSection);
