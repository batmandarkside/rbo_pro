import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import { LOCAL_REDUCER as REDUCER } from '../../constants';

import {
  formDataSelector,
  dictionariesSelector,
  currCodesItemsSelector
} from '../../selectors.js';

const currentCurrencyOperationSelector = state => state.getIn([REDUCER, 'currentCurrencyOperation']);

const operationsSelector = createSelector(
  formDataSelector,
  dictionariesSelector,
  (formData, dictionaries) => formData
    .get('currencyOperations')
    .map((operation) => {
      const currId = operation.get('curType');
      const currency = dictionaries
        .getIn(['currCodes', 'items'])
        .find(currCode => currCode.get('id') === currId);
      return operation
        .set('currency', currency ? currency.get('isoCode') : '')
        .set('currencyFull', currency ? `${currency.get('isoCode')} / ${currency.get('code')}` : '');
    })
);

const currencyOperationCurTypeItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['currencyOperationCurType', 'items'])
    .map(currency => fromJS({
      value: currency.get('id'),
      title: currency.get('isoCode'),
      extra: currency.get('code'),
      describe: currency.get('name')
    }))
);

const currencyOperationCodeItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['operCode', 'items'])
    .map(operation => fromJS({
      value: operation.get('code'),
      title: operation.get('code')
    }))
);

const isModalActiveSelector = createSelector(
  currentCurrencyOperationSelector,
  currentCurrencyOperation => !!currentCurrencyOperation.get('entity')
);

const operationInEditSelector = createSelector(
  currentCurrencyOperationSelector,
  currentCurrencyOperation => currentCurrencyOperation.get('entity')
);

const dealPassNum1ItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['dealPassNum1', 'items'])
    .map(num => ({
      value: num.get('number'),
      title: num.get('number'),
      extra: num.get('date'),
      describe: num.get('comment')
    }))
);

const dealPassNum4ItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['dealPassNum4', 'items'])
    .map(num => ({
      value: num.get('code'),
      title: num.get('code'),
      describe: num.get('desc')
    }))
);
const dealPassNum5ItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['dealPassNum5', 'items'])
    .map(num => ({
      value: num.get('code'),
      title: num.get('code'),
      describe: num.get('desc')
    }))
);

export default state => ({
  currCodesItems: currCodesItemsSelector(state),
  currencyOperationCurTypeItems: currencyOperationCurTypeItemsSelector(state),
  operationInEdit: operationInEditSelector(state),
  operations: operationsSelector(state),
  isModalActive: isModalActiveSelector(state),
  currencyOperationCodeItems: currencyOperationCodeItemsSelector(state),
  dealPassNum1Items: dealPassNum1ItemsSelector(state),
  dealPassNum4Items: dealPassNum4ItemsSelector(state),
  dealPassNum5Items: dealPassNum5ItemsSelector(state)
});
