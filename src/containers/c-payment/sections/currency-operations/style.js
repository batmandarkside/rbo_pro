import styled from 'styled-components';
import Icon from 'components/ui-components/icon';

export const Button = styled.div`
  ${props => (
    props.white
    ? `
      color: #000000;
    `
    : `
      background: #000000;
      color: #ffffff;
    `
  )}
  border: 1px solid #000000;
  font-size: 14px;
  opacity: .87;
  height: 36px;
  line-height: 36px;
  cursor: pointer;
  display: inline-block;
  width: ${props => (props.small ? 120 : 264)}px;
  text-align: center;
  position: relative;
  border-radius: 2px;

  &:not(:first-child) {
    margin-left: 24px;
  }
`;

export const ButtonsRow = styled.div`
  margin-top: 40px;
`;

export const ControlsLine = styled.div`
  margin-top: 24px;
  text-align: ${props => (props.center ? 'center' : 'left')};
`;

export const TotalText = styled.span`
  color: #848484;
  line-height: 32px;
  float: right;
  font-style: italic;
  letter-spacing: 0.2px;
  text-align: right;
`;

export const MoreOperatios = styled.span`
  color: #0055a2;
  border-bottom: 1px dashed #0055a2;
  cursor: pointer;
`;

export const ModalHeader = styled.div`
  display: block;
  height: 64px;
  border-bottom: 1px solid #fcc900;
  position: relative;
`;

export const ModalHeaderTitle = styled.span`
  display: block;
  padding: 16px 32px;
  line-height: 32px;
  font-weight: bold;
  font-size: 20px;
  letter-spacing: .2px;
`;

export const ModalHeaderClose = styled.div`
  position: absolute;
  top: 12px;
  right: 24px;
  cursor: pointer;
`;

export const ModalBody = styled.div`
  padding: 8px 32px 24px;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  border-radius: 2px;
  border-style: hidden;
  box-shadow: 0 0 0 1px #d0cccc;

  td,
  th {
    padding: 8px 16px;
  }
`;

export const TableHead = styled.tr`
  background-color: #ececec;
  font-weight: bold;
  font-size: 12px;
  border-bottom: 1px solid #d0cccc;
`;

export const TableRow = styled.tr`
  border-bottom: 1px solid #d0cccc;
`;

export const TableHeadCell = styled.th`
  text-align: ${props => (props.right ? 'right' : 'left')};
  width: ${props => (props.width || 'auto')};
`;

export const TableCell = styled.td`
  font-size: 14px;
  text-align: ${props => (props.right ? 'right' : 'left')}
`;

export const TableCellEmpty = styled.td.attrs({
  colSpan: '4'
})`
  color: #848484;
  font-style: oblique;
`;

export const ActionIcon = styled(Icon)`
  position: relative;
  cursor: pointer;
  margin-left: 16px;

  svg {
    fill: #000 !important;
  }
`;
