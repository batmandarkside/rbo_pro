import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormRowset as Rowset,
  RboFormCell as Cell,
  RboFormFieldSuggest as FieldSuggest,
  RboFormFieldSearch as FieldSearch,
  RboFormFieldText as FieldText,
  RboFormCollapse as Collapse
} from 'components/ui-components/rbo-form-compact';

import { formFieldOptionRenderer } from '../../utils';
import mapStateToProps from './selectors';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrIntermediaryBankSection extends Component {
  static propTypes = {
    IMediaBankSWIFTItems: ImmutablePropTypes.list,
    IMeBankClearCodeShortItems: ImmutablePropTypes.list,
    countriesItems: ImmutablePropTypes.list,
    onFieldSearch: PropTypes.func,
    onFieldChange: PropTypes.func,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    getConstraint: PropTypes.func,
    formData: ImmutablePropTypes.map,
    onCollapseToggle: PropTypes.func.isRequired,
    isCollapsed: PropTypes.bool,
    isCollapsible: PropTypes.bool,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  handleCollapseToggle = () => {
    const { isCollapsed, onCollapseToggle } = this.props;
    onCollapseToggle({ sectionId: 'intermediaryBank', isCollapsed: !isCollapsed });
  };

  render() {
    const {
      IMediaBankSWIFTItems,
      IMeBankClearCodeShortItems,
      countriesItems,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      getConstraint,
      formData,
      isCollapsed,
      isCollapsible,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section
        id="intermediaryBank"
        title="56: БАНК-ПОСРЕДНИК"
        isCollapsible={isCollapsible}
        isCollapsed={isCollapsed}
      >
        <Blockset>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <FieldSuggest
                    id={FIELDS.I_MEDIA_BANK_SWIFT}
                    label="SWIFT-код"
                    value={formData.getIn(['iMediaBank', 'SWIFT'])}
                    options={IMediaBankSWIFTItems.toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.I_MEDIA_BANK_SWIFT)}
                    isError={formErrors.get(FIELDS.I_MEDIA_BANK_SWIFT)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell size="1/2">
                  <FieldSearch
                    id={FIELDS.I_ME_BANK_CLEAR_CODE_SHORT}
                    label="Клиринговый код"
                    value={formData.getIn(['iMediaBank', 'clearCodeShort'])}
                    options={IMeBankClearCodeShortItems.toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onChange={onFieldChange}
                    onSearch={onFieldSearch}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.I_ME_BANK_CLEAR_CODE_SHORT)}
                    isError={formErrors.get(FIELDS.I_ME_BANK_CLEAR_CODE_SHORT)}
                  />
                </Cell>
                <Cell size="1/6">
                  <FieldText
                    id={FIELDS.I_ME_BANK_CLEAR_CODE_02}
                    value={formData.getIn(['iMediaBank', 'clearCode02'])}
                    getConstraint={getConstraint}
                    onChange={onFieldChange}
                    isWarning={formWarnings.get(FIELDS.I_ME_BANK_CLEAR_CODE_02)}
                    isError={formErrors.get(FIELDS.I_ME_BANK_CLEAR_CODE_02)}
                    pseudoLabel
                    isDisabled
                  />
                </Cell>
                <Cell size="1/3">
                  <FieldText
                    id={FIELDS.I_ME_BANK_CLEAR_CODE}
                    label="Значение кода"
                    placeholder="9!n"
                    value={formData.getIn(['iMediaBank', 'clearCode'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.I_ME_BANK_CLEAR_CODE)}
                    isError={formErrors.get(FIELDS.I_ME_BANK_CLEAR_CODE)}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <Collapse
                    pseudoLabel
                    title="Клиринговый код, адрес"
                    isCollapsed={isCollapsed}
                    onCollapseToggle={this.handleCollapseToggle}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
        </Blockset>
        <Blockset>
          <Block>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.I_MEDIA_BANK_NAME}
                    label="Наименование"
                    value={formData.getIn(['iMediaBank', 'name'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.I_MEDIA_BANK_NAME)}
                    isError={formErrors.get(FIELDS.I_MEDIA_BANK_NAME)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell size="2/3">
                  <FieldText
                    id={FIELDS.I_MEDIA_BANK_PLACE}
                    label="Город"
                    value={formData.getIn(['iMediaBank', 'place'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.I_MEDIA_BANK_PLACE)}
                    isError={formErrors.get(FIELDS.I_MEDIA_BANK_PLACE)}
                  />
                </Cell>
                <Cell size="1/3">
                  <FieldSearch
                    id={FIELDS.INTERMEDIARY_BANK_COUNTRIES}
                    label="Страна"
                    value={formData.getIn(['iMediaBank', 'countryCode'])}
                    onChange={onFieldChange}
                    options={countriesItems.toJS()}
                    onSearch={onFieldSearch}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.INTERMEDIARY_BANK_COUNTRIES)}
                    isError={formErrors.get(FIELDS.INTERMEDIARY_BANK_COUNTRIES)}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.I_MEDIA_BANK_ADDRESS}
                    label="Адрес"
                    value={formData.getIn(['iMediaBank', 'address'])}
                    onChange={onFieldChange}
                    getConstraint={getConstraint}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.I_MEDIA_BANK_ADDRESS)}
                    isError={formErrors.get(FIELDS.I_MEDIA_BANK_ADDRESS)}
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.I_MEDIA_BANK_COUNTRY_NAME}
                    getConstraint={getConstraint}
                    value={formData.getIn(['iMediaBank', 'countryName'])}
                    onChange={onFieldChange}
                    isWarning={formWarnings.get(FIELDS.I_MEDIA_BANK_COUNTRY_NAME)}
                    isError={formErrors.get(FIELDS.I_MEDIA_BANK_COUNTRY_NAME)}
                    pseudoLabel
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(
  mapStateToProps
)(CtrIntermediaryBankSection);
