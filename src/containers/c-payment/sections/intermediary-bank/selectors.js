import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import { dictionariesSelector, constraintSelector } from '../../selectors';

const countriesItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['intermidiaryBankCountries', 'items'])
    .map(country => fromJS({
      value: country.get('code'),
      title: `${country.get('mnem02')} / ${country.get('code')}`
    }))
);

const IMediaBankSWIFTItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['iMediaBankSWIFT', 'items'])
    .map(bank => fromJS({
      value: bank.get('id'),
      title: bank.get('bicInt'),
      extra: bank.get('countryName'),
      describe: bank.get('name')
    }))
);

const IMeBankClearCodeShortItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['iMeBankClearCodeShort', 'items'])
    .map(code => fromJS({
      value: code.get('shortName'),
      title: code.get('shortName'),
      extra: code.get('clearingCode'),
      describe: `${code.get('countryCode02')} ${code.get('name')}`
    }))
);

export default state => ({
  countriesItems: countriesItemsSelector(state),
  IMediaBankSWIFTItems: IMediaBankSWIFTItemsSelector(state),
  IMeBankClearCodeShortItems: IMeBankClearCodeShortItemsSelector(state),
  getConstraint: constraintSelector(state)
});
