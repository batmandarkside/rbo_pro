import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldTextarea as FieldTextarea
} from 'components/ui-components/rbo-form-compact';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrPayedTaxesSection extends Component {
  static propTypes = {
    onFieldChange: PropTypes.func,
    formData: ImmutablePropTypes.map,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  render() {
    const {
      formData,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section id="payedTaxes" title="ПРОПЛАЧЕННЫЕ НАЛОГИ">
        <Blockset>
          <Block>
            <Row>
              <Cell>
                <span>Реквизиты (номер, дата, сумма, прочее) предоставленного платежного поручения:</span>
              </Cell>
            </Row>
          </Block>
          <Block>
            <Row>
              <Cell>
                <FieldTextarea
                  id={FIELDS.TAX_CONFIRM_PAY_DOC}
                  value={formData.getIn(['docInfo', 'branchShortName'])}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.TAX_CONFIRM_PAY_DOC)}
                  isError={formErrors.get(FIELDS.TAX_CONFIRM_PAY_DOC)}
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default CtrPayedTaxesSection;
