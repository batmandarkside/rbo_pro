import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormRowset as Rowset,
  RboFormCell as Cell,
  RboFormFieldSearch as FieldSearch,
  RboFormFieldText as FieldText,
  RboFormCollapse as Collapse
} from 'components/ui-components/rbo-form-compact';

import { formFieldOptionRenderer } from '../../utils';
import mapStateToProps from './selectors';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrPayerSection extends Component {
  static propTypes = {
    payerAccount: PropTypes.string,
    accountsItems: ImmutablePropTypes.list,
    getConstraint: PropTypes.func,
    onFieldSearch: PropTypes.func,
    onFieldChange: PropTypes.func,
    formData: ImmutablePropTypes.map,
    onCollapseToggle: PropTypes.func.isRequired,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    isCollapsed: PropTypes.bool,
    isCollapsible: PropTypes.bool,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  handleCollapseToggle = () => {
    const { isCollapsed, onCollapseToggle } = this.props;
    onCollapseToggle({ sectionId: 'payer', isCollapsed: !isCollapsed });
  };

  render() {
    const {
      payerAccount,
      accountsItems,
      getConstraint,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      formData,
      isCollapsed,
      isCollapsible,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section
        id="payer"
        title="50: ПЕРЕВОДОДАТЕЛЬ"
        isCollapsible={isCollapsible}
        isCollapsed={isCollapsed}
      >
        <Blockset>
          <Block>
            <Rowset isMain>
              <Row>
                <Cell>
                  <FieldSearch
                    id={FIELDS.PAYER_ACCOUNT}
                    label="Счет №"
                    value={payerAccount}
                    options={accountsItems.toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    onChange={onFieldChange}
                    onSearch={onFieldSearch}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    isWarning={formWarnings.get(FIELDS.PAYER_ACCOUNT)}
                    isError={formErrors.get(FIELDS.PAYER_ACCOUNT)}
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Collapse
              pseudoLabel
              title="Название, адрес"
              isCollapsed={isCollapsed}
              onCollapseToggle={this.handleCollapseToggle}
            />
          </Block>
        </Blockset>
        <Blockset>
          <Block>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.PAYER_NAME_INT}
                    label="Международное наименование"
                    value={formData.getIn(['payer', 'nameInt'])}
                    getConstraint={getConstraint}
                    isWarning={formWarnings.get(FIELDS.PAYER_NAME_INT)}
                    isError={formErrors.get(FIELDS.PAYER_NAME_INT)}
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.PAYER_BANK_NAME}
                    label="52: Банк перевододателя"
                    value={formData.getIn(['payer', 'bankName'])}
                    getConstraint={getConstraint}
                    isWarning={formWarnings.get(FIELDS.PAYER_BANK_NAME)}
                    isError={formErrors.get(FIELDS.PAYER_BANK_NAME)}
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
          <Block>
            <Rowset>
              <Row>
                <Cell>
                  <FieldText
                    id={FIELDS.PAYER_ADDRESS}
                    label="Адрес"
                    value={formData.getIn(['payer', 'address'])}
                    getConstraint={getConstraint}
                    isWarning={formWarnings.get(FIELDS.PAYER_ADDRESS)}
                    isError={formErrors.get(FIELDS.PAYER_ADDRESS)}
                    isDisabled
                  />
                </Cell>
              </Row>
            </Rowset>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(
  mapStateToProps
)(CtrPayerSection);
