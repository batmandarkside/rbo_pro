import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { getFormattedAccNum } from 'utils';
import { dictionariesSelector, formDataSelector, constraintSelector } from '../../selectors';

const accountsItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['payerAccount', 'items'])
    .map(accounts => fromJS({
      value: accounts.get('accNum'),
      title: getFormattedAccNum(accounts.get('accNum')),
      describe: accounts.get('name'),
      extra: `${accounts.get('currentBalance')} / ${accounts.get('currCodeIso')}`
    }))
);

const payerAccountSelector = createSelector(
  accountsItemsSelector,
  formDataSelector,
  (accountsItems, formData) => {
    const accItem = accountsItems
      .find(acc => acc.get('value') === formData.getIn(['payer', 'account']));
    return accItem ? accItem.get('value') : '';
  }
);

export default state => ({
  payerAccount: payerAccountSelector(state),
  accountsItems: accountsItemsSelector(state),
  getConstraint: constraintSelector(state)
});
