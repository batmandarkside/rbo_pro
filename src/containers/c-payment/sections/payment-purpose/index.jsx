import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldSuggest as FieldSuggest
} from 'components/ui-components/rbo-form-compact';

import mapStateToProps from './selectors';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrPaymentPurposeSection extends Component {
  static propTypes = {
    PaymentDetailsItems: ImmutablePropTypes.list,
    onFieldSearch: PropTypes.func,
    onFieldChange: PropTypes.func,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    getConstraint: PropTypes.func,
    formData: ImmutablePropTypes.map,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  render() {
    const {
      PaymentDetailsItems,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      getConstraint,
      formData,
      formWarnings,
      formErrors
    } = this.props;

    return (
      <Section id="paymentPurpose" title="70: НАЗНАЧЕНИЕ ПЛАТЕЖА">
        <Blockset>
          <Block>
            Укажите назначение платежа
          </Block>
          <Block>
            <Row>
              <Cell>
                <FieldSuggest
                  id={FIELDS.PAYMENT_DETAILS}
                  value={formData.getIn(['docInfo', 'paymentDetails'])}
                  options={PaymentDetailsItems.toJS()}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  getConstraint={getConstraint}
                  isWarning={formWarnings.get(FIELDS.PAYMENT_DETAILS)}
                  isError={formErrors.get(FIELDS.PAYMENT_DETAILS)}
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(
  mapStateToProps
)(CtrPaymentPurposeSection);
