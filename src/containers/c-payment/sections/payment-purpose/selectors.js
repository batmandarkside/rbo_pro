import { createSelector } from 'reselect';
import { dictionariesSelector, constraintSelector } from '../../selectors';

const PaymentDetailsItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['paymentDetails', 'items'])
    .map(paymentPurpose => ({
      value: paymentPurpose.get('purpose'),
      title: paymentPurpose.get('purpose')
    }))
);

export default state => ({
  PaymentDetailsItems: PaymentDetailsItemsSelector(state),
  getConstraint: constraintSelector(state)
});
