import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldText as FieldText,
  RboFormFieldSearch as FieldSearch,
  RboFormFieldRadio as FieldRadio
} from 'components/ui-components/rbo-form-compact';

import { formFieldOptionRenderer } from '../../utils';
import mapStateToProps from './selectors';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrTransferFeeSection extends Component {
  static propTypes = {
    comissionTypesItems: ImmutablePropTypes.list,
    ChargesAccountItems: ImmutablePropTypes.list,
    onFieldSearch: PropTypes.func,
    onFieldChange: PropTypes.func,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    getConstraint: PropTypes.func,
    formData: ImmutablePropTypes.map,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  render() {
    const {
      comissionTypesItems,
      ChargesAccountItems,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      getConstraint,
      formData,
      formWarnings,
      formErrors
    } = this.props;

    const chargesTypeValue = formData.getIn(['chargesInfo', 'chargesType']);

    return (
      <Section id="transferId" title="71А: КОМИССИЯ ЗА ПЕРЕВОД">
        {comissionTypesItems.size === 3 && 
          <Blockset>
            <Block>
              <FieldRadio
                id="TransferFeePayerOur"
                group={FIELDS.CHARGES_TYPE}
                title={comissionTypesItems.get(1).get('title')}
                value={comissionTypesItems.get(1).get('value')}
                isSelected={comissionTypesItems.get(1).get('value') === chargesTypeValue}
                onChange={onFieldChange}
                onFocus={onFieldFocus}
                onBlur={onFieldBlur}
                isWarning={formWarnings.get(FIELDS.CHARGES_TYPE)}
                isError={formErrors.get(FIELDS.CHARGES_TYPE)}
              />
              <FieldRadio
                id="TransferFeePayerBen"
                group={FIELDS.CHARGES_TYPE}
                title={comissionTypesItems.get(0).get('title')}
                value={comissionTypesItems.get(0).get('value')}
                isSelected={comissionTypesItems.get(0).get('value') === chargesTypeValue}
                onChange={onFieldChange}
                onFocus={onFieldFocus}
                onBlur={onFieldBlur}
                isWarning={formWarnings.get(FIELDS.CHARGES_TYPE)}
                isError={formErrors.get(FIELDS.CHARGES_TYPE)}
              />
            </Block>
            <Block>
              <FieldRadio
                id="TransferFeePayerSha"
                group={FIELDS.CHARGES_TYPE}
                title={comissionTypesItems.get(2).get('title')}
                value={comissionTypesItems.get(2).get('value')}
                isSelected={comissionTypesItems.get(2).get('value') === chargesTypeValue}
                onChange={onFieldChange}
                onFocus={onFieldFocus}
                onBlur={onFieldBlur}
                isWarning={formWarnings.get(FIELDS.CHARGES_TYPE)}
                isError={formErrors.get(FIELDS.CHARGES_TYPE)}
              />
            </Block>
          </Blockset>
        }
        <Blockset>
          <Block>
            <Row>
              <Cell size="2/3">
                <FieldSearch
                  id={FIELDS.CHARGES_ACCOUNT}
                  label="Расходы банка списать со счета"
                  value={formData.getIn(['chargesInfo', 'chargesAccount'])}
                  optionRenderer={formFieldOptionRenderer}
                  options={ChargesAccountItems.toJS()}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.CHARGES_ACCOUNT)}
                  isError={formErrors.get(FIELDS.CHARGES_ACCOUNT)}
                />
              </Cell>
              <Cell size="1/3">
                <FieldText
                  id={FIELDS.CHARGES_BANK_BIC}
                  label="БИК"
                  value={formData.getIn(['chargesInfo', 'chargesBankBIC'])}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  getConstraint={getConstraint}
                  isWarning={formWarnings.get(FIELDS.CHARGES_BANK_BIC)}
                  isError={formErrors.get(FIELDS.CHARGES_BANK_BIC)}
                  isDisabled
                />
              </Cell>
            </Row>
          </Block>
          <Block>
            <Row>
              <Cell>
                <FieldText
                  id={FIELDS.CHARGES_BANK_NAME}
                  label="Наименование банка"
                  value={formData.getIn(['chargesInfo', 'chargesBankName'])}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  getConstraint={getConstraint}
                  isWarning={formWarnings.get(FIELDS.CHARGES_BANK_NAME)}
                  isError={formErrors.get(FIELDS.CHARGES_BANK_NAME)}
                  isDisabled
                />
              </Cell>
            </Row>
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(
  mapStateToProps
)(CtrTransferFeeSection);
