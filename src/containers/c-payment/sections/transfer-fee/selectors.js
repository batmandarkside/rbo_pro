import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import { getFormattedAccNum } from 'utils';
import { dictionariesSelector, constraintSelector } from '../../selectors';

const comissionTypesItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['comissionTypes', 'items'])
    .slice(0, 3)
    .map(account => fromJS({
      value: account.get('code'),
      title: `${account.get('code')}: ${account.get('desc')}`
    }))
);

const ChargesAccountItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['chargesAccount', 'items'])
    .map(account => fromJS({
      value: account.get('accNum'),
      title: getFormattedAccNum(account.get('accNum')),
      describe: account.get('name'),
      extra: `${account.get('currentBalance')} / ${account.get('currCodeIso')}`
    }))
);

export default state => ({
  comissionTypesItems: comissionTypesItemsSelector(state),
  ChargesAccountItems: ChargesAccountItemsSelector(state),
  getConstraint: constraintSelector(state)
});
