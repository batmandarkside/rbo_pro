import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  RboFormSection as Section,
  RboFormBlockset as Blockset,
  RboFormBlock as Block,
  RboFormRow as Row,
  RboFormCell as Cell,
  RboFormFieldDecimalNumber as FieldDecimalNumber,
  RboFormFieldSearch as FieldSearch,
  RboFormFieldCheckbox as FieldCheckbox
} from 'components/ui-components/rbo-form-compact';

import { formFieldOptionRenderer } from '../../utils';
import mapStateToProps from './selectors';
import { FIELDS } from '../../constants';

// eslint-disable-next-line
class CtrTransferSection extends Component {
  static propTypes = {
    formData: ImmutablePropTypes.map,
    transferSumCurItems: ImmutablePropTypes.list,
    transferChargedSumCurItems: ImmutablePropTypes.list,
    onFieldSearch: PropTypes.func,
    onFieldChange: PropTypes.func,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    getConstraint: PropTypes.func,
    isAmountTransDisabled: PropTypes.bool,
    isAmountOffDisabled: PropTypes.bool,
    formWarnings: ImmutablePropTypes.map,
    formErrors: ImmutablePropTypes.map
  };

  render() {
    const {
      transferSumCurItems,
      transferChargedSumCurItems,
      onFieldSearch,
      onFieldChange,
      onFieldFocus,
      onFieldBlur,
      getConstraint,
      formData,
      isAmountTransDisabled,
      isAmountOffDisabled,
      formWarnings,
      formErrors
    } = this.props;

    const multiCurr = formData.getIn(['moneyInfo', 'multiCurr']);
    return (
      <Section id="transfer" title="32А: ПЕРЕВОД">
        <Blockset>
          <Block>
            <Row>
              <Cell size="1/2">
                <FieldDecimalNumber
                  id={FIELDS.AMOUNT_TRANS}
                  value={formData.getIn(['moneyInfo', 'amountTrans'])}
                  label="Сумма перевода"
                  getConstraint={getConstraint}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isDisabled={isAmountTransDisabled}
                  isWarning={formWarnings.get(FIELDS.AMOUNT_TRANS)}
                  isError={formErrors.get(FIELDS.AMOUNT_TRANS)}
                />
              </Cell>
              <Cell size="1/3">
                <FieldSearch
                  id={FIELDS.TRANSFER_SUM_CUR}
                  label="Валюта перевода"
                  value={formData.getIn(['moneyInfo', 'currTransCode'])}
                  options={transferSumCurItems.toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  onChange={onFieldChange}
                  onSearch={onFieldSearch}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.TRANSFER_SUM_CUR)}
                  isError={formErrors.get(FIELDS.TRANSFER_SUM_CUR)}
                />
              </Cell>
            </Row>
            <Row>
              <Cell size="1/2">
                <FieldDecimalNumber
                  id={FIELDS.AMOUNT_OFF}
                  value={formData.getIn(['moneyInfo', 'amountOff'])}
                  label="Сумма списания"
                  getConstraint={getConstraint}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isDisabled={isAmountOffDisabled}
                  isWarning={formWarnings.get(FIELDS.AMOUNT_OFF)}
                  isError={formErrors.get(FIELDS.AMOUNT_OFF)}
                />
              </Cell>
              <Cell size="1/3">
                <FieldSearch
                  id={FIELDS.TRANSFER_CHANGED_SUM_CUR}
                  label="Валюта списания"
                  value={formData.getIn(['moneyInfo', 'currOffCode'])}
                  options={transferChargedSumCurItems.toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  isWarning={formWarnings.get(FIELDS.TRANSFER_CHANGED_SUM_CUR)}
                  isError={formErrors.get(FIELDS.TRANSFER_CHANGED_SUM_CUR)}
                  isDisabled={!multiCurr}
                />
              </Cell>
            </Row>
          </Block>
          <Block>
            <FieldCheckbox
              pseudoLabel
              id={FIELDS.MULTI_CURR}
              title="Мультивалютный перевод"
              value={multiCurr}
              onChange={onFieldChange}
              onFocus={onFieldFocus}
              onBlur={onFieldBlur}
              isWarning={formWarnings.get(FIELDS.MULTI_CURR)}
              isError={formErrors.get(FIELDS.MULTI_CURR)}
            />
          </Block>
        </Blockset>
      </Section>
    );
  }
}

export default connect(mapStateToProps)(CtrTransferSection);
