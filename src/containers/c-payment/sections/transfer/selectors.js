import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import {
  dictionariesSelector,
  constraintSelector,
  formDataSelector
} from '../../selectors.js';

const transferSumCurItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['transferSumCur', 'items'])
    .map(currCode => fromJS({
      value: currCode.get('code'),
      title: `${currCode.get('isoCode')} / ${currCode.get('code')}`,
      describe: currCode.get('name')
    }))
);

const transferChargedSumCurItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['transferChargedSumCur', 'items'])
    .map(currCode => fromJS({
      value: currCode.get('code'),
      title: `${currCode.get('isoCode')} / ${currCode.get('code')}`,
      describe: currCode.get('name')
    }))
);


const isAmountTransDisabledSelector = createSelector(
  formDataSelector,
  formData => !!formData.getIn(['moneyInfo', 'amountOff'])
);

const isAmountOffDisabledSelector = createSelector(
  formDataSelector,
  formData => !!formData.getIn(['moneyInfo', 'amountTrans'])
);

export default state => ({
  transferSumCurItems: transferSumCurItemsSelector(state),
  transferChargedSumCurItems: transferChargedSumCurItemsSelector(state),
  getConstraint: constraintSelector(state),
  isAmountTransDisabled: isAmountTransDisabledSelector(state),
  isAmountOffDisabled: isAmountOffDisabledSelector(state)
});
