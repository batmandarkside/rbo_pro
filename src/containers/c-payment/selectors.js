import { createSelector } from 'reselect';
import moment from 'moment-timezone';
import { Map, List } from 'immutable';
import { DOC_STATUSES, SIGN_VALIDATION_STATUSES } from 'constants';
import {
  getDocStatusClassNameMode,
  getSignType,
  getMappingDocSectionsAndErrors,
  getDocFormErrors
} from 'utils';
import { flatMap } from 'lodash';
import { LOCAL_REDUCER as REDUCER } from './constants';
import {
  getBankInfo,
  documentErrorsMap,
  documentSectionsFieldsMap
} from './utils';

export const formDataSelector = state => state.getIn([REDUCER, 'formData']);
export const dictionariesSelector = state => state.getIn([REDUCER, 'dictionaries']);
export const constraintsSelector = state => state.getIn([REDUCER, 'constraints']);

const activeFormFieldSelector = state => state.getIn([REDUCER, 'activeFormField']);
const formSectionsSelector = state => state.getIn([REDUCER, 'formSections']);
const documentTabsSelector = state => state.getIn([REDUCER, 'documentTabs']);
const documentHistorySelector = state => state.getIn([REDUCER, 'documentHistory']);
const documentSignaturesSelector = state => state.getIn([REDUCER, 'documentSignatures']);
const documentDictionariesSelector = state => state.getIn([REDUCER, 'dictionaries']);
const operationsSelector = state => state.getIn([REDUCER, 'operations']);
const documentErrorsSelector = state => state.getIn([REDUCER, 'documentErrors']);

const documentErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  activeFormFieldSelector,
  (documentErrors, activeFormField) => documentErrors
    .map(error => error.merge({
      fieldId: documentErrorsMap[error.get('id')] && documentErrorsMap[error.get('id')][0],
      fieldsIds: documentErrorsMap[error.get('id')],
      isActive: documentErrorsMap[error.get('id')] &&
      !!documentErrorsMap[error.get('id')].find(i => i === activeFormField)
    }))
    .filter(error => !!error.get('fieldId'))
    .sort((a, b) => flatMap(documentSectionsFieldsMap).indexOf(a.get('fieldId')) - flatMap(documentSectionsFieldsMap).indexOf(b.get('fieldId')))
);

const documentHistoryCreatedSelector = createSelector(
  documentHistorySelector,
  documentHistory => documentHistory.set('items', documentHistory.get('items')
    .reverse()
    .map(item => Map({
      date: item.get('date') ? moment(item.get('date')).format('DD.MM.YYYY HH:mm:ss') : '',
      user: item.get('user'),
      endState: item.get('endState'),
      statusClassName: getDocStatusClassNameMode(item.get('endState'))
    }))
  )
);

const documentSignaturesCreatedSelector = createSelector(
  documentSignaturesSelector,
  documentSignatures => documentSignatures.set('items', documentSignatures.get('items').map(item => Map({
    signId: item.get('signId'),
    signDateTime: item.get('signDateTime') ? moment(item.get('signDateTime')).format('DD.MM.YYYY HH:mm:ss') : '',
    userLogin: item.get('userLogin'),
    userName: item.get('userName'),
    userPosition: item.get('userPosition'),
    signType: getSignType(item.get('signType')),
    signValid: item.get('signValid') ? SIGN_VALIDATION_STATUSES.VALID : SIGN_VALIDATION_STATUSES.INVALID,
    signHash: item.get('signHash')
  })))
);


const documentStructureCreatedSelector = createSelector(
  documentTabsSelector,
  formSectionsSelector,
  documentErrorsCreatedSelector,
  activeFormFieldSelector,
  (documentTabs, formSections, documentErrors, activeFormField) => documentTabs.map((tab) => {
    switch (tab.get('id')) {
      case 'main':
        return tab.set('sections',
          getMappingDocSectionsAndErrors(formSections, documentSectionsFieldsMap, documentErrors, activeFormField));
      default:
        return tab;
    }
  })
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['1'])
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['2', '3'])
);

export const currCodesItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['currCodes', 'items'])
    .map(currCode => ({
      value: currCode.get('id'),
      title: `${currCode.get('isoCode')} / ${currCode.get('code')}`
    }))
);

export const clearingCodesItemsSelector = createSelector(
  dictionariesSelector,
  dictionaries => dictionaries
    .getIn(['clearingCodes', 'items'])
    .map(clearingCode => ({
      value: clearingCode.get('name'),
      title: clearingCode.get('name')
    }))
);

export const constraintSelector = createSelector(
  constraintsSelector,
  constraints => fieldId =>
    constraints.get(fieldId, Map())
  );


const documentRecallsCreatedSelector = createSelector(
  formDataSelector,
  formData => formData.get('linkedDocs', List([]))
    .filter(doc => doc.get('docType') === 'Recall')
    .sort((a, b) => {
      // сортируем по дате, если дата одинаковая (хранится только число, месяц, год) то сортируем по номеру
      const dateA = moment(a.get('docDate'));
      const dateB = moment(b.get('docDate'));
      const different = dateA.diff(dateB);

      if (!different) {
        const numberA = a.get('docNumber');
        const numberB = b.get('docNumber');

        return parseInt(numberA, 10) < parseInt(numberB, 10) ? 1 : -1;
      }

      return (different < 0) ? 1 : -1;
    })
    .map(doc => Map({
      id: doc.get('recordID'),
      title: `Отзыв № ${doc.get('docNumber')} от ${moment(doc.get('docDate')).format('DD.MM.YYYY')}`
    }))
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  formDataSelector,
  (operations, formData) => {
    const status = formData.getIn(['docInfo', 'status']);
    const allowedSmActions = formData.get('allowedSmActions', Map());
    return operations.filter((operation) => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'back':
          return true;
        case 'save':
          // todo пока с бэка ничего об операции сохранения не приходит
          return true;
        case 'signAndSend':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save') || allowedSmActions.get('sign');
        case 'sign':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('sign');
        case 'unarchive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('fromArchive');
        case 'repeat':
          return status !== DOC_STATUSES.NEW;
        case 'print':
          return status !== DOC_STATUSES.NEW;
        case 'remove':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('delete');
        case 'recall':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('recall');
        case 'send':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('send');
        case 'history':
          return status !== DOC_STATUSES.NEW;
        case 'archive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('archive');
        case 'visa':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('sign');
        case 'checkSign':
          return !![
            DOC_STATUSES.DELIVERED,
            DOC_STATUSES.PROCESSED,
            DOC_STATUSES.EXECUTED,
            DOC_STATUSES.DECLINED,
            DOC_STATUSES.RECALLED,
            DOC_STATUSES.INVALID_PROPS,
            DOC_STATUSES.SIGNED,
            DOC_STATUSES.ACCEPTED,
            DOC_STATUSES.PARTLY_SIGNED,
            DOC_STATUSES.INVALID_SIGN
          ].find(v => v === status);
        case 'saveAsTemplate':
          return status !== DOC_STATUSES.NEW;
        default:
          return false;
      }
    });
  }
);

const documentValuesCreatedSelector = createSelector(
  formDataSelector,
  formData => Map({
    bankInfo: getBankInfo(formData)
  })
);

export default function mapStateToProps(state) {
  return {
    documentDictionaries: documentDictionariesSelector(state),
    documentStructure: documentStructureCreatedSelector(state),
    documentHistory: documentHistoryCreatedSelector(state),
    documentSignatures: documentSignaturesCreatedSelector(state),
    documentTabs: documentTabsSelector(state),
    formData: formDataSelector(state),
    formSections: formSectionsSelector(state),
    documentRecalls: documentRecallsCreatedSelector(state),
    operations: operationsCreatedSelector(state),
    formValues: documentValuesCreatedSelector(state),
    formWarnings: formWarningsCreatedSelector(state),
    formErrors: formErrorsCreatedSelector(state)
  };
}
