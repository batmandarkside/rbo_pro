import { ACTIONS } from './constants';

/**
 *    action получения номера выбранного раздела из pathname
 */
export const getSectionIndexAction = () => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_GET_SECTION_INDEX_REQUEST
});

/**
 *    action получение query-параметров (номер страницы, фильтр)
 */
export const getQueryParamsAction = () => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST
});

/**
 *    action перехода между секциями (получает tabId)
 */
export const changeSectionAction = ({ tabId }) => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SECTION,
  payload: tabId
});

/**
 *    action перехода на страницу (получает index выбранной страницы )
 *    @param {number} pageIndex
 */
export const changePageAction = pageIndex => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_PAGE,
  payload: pageIndex
});

/**
 *    action перезагрузки страниц (сброс на первую страницу)
 */
export const reloadPagesAction = () => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_RELOAD_PAGES
});

/**
 *    action изменения фильтра (получает изменненную коллекцию filters, флаг обновления листа и флаг
 *    необходимости сохранения фильтра)
 *    @param {List} filters - коллекцию filter
 *    @param {bool} isChanged  - флаг обновления фильтра
 *    @param {bool} isSaveSettingRequired - флаг необходимости сохранения фильтров в настройках
 */
export const changeFiltersAction = (filters, isChanged, isSaveSettingRequired) => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_FILTERS,
  payload: { filters, isChanged, isSaveSettingRequired }
});

/**
 *    action запроса справочников для фильтра
 *    @param {string} conditionId - id условия фильтра
 *    @param {string} value - значение поля
 */
export const getDictionaryForFiltersAction = (conditionId, value) => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST,
  payload: { conditionId, value }
});

/**
 *    action получение списка (получает параметры для запроса списка)
 *    @param {object} params
 */
export const getListAction = params => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_GET_LIST_REQUEST,
  payload: params
});

/**
 *    action экспорта списка в excel
 */
export const exportListAction = () => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_REQUEST
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_UNMOUNT_REQUEST
});

/**
 *    action изменения размера колонки (получает изменненную коллекцию columns)
 *    @param {List} columns
 */
export const resizeColumnsAction = columns => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_RESIZE_COLUMNS,
  payload: columns
});

/**
 *    action изменения отображаемых столбцов (получает изменненную коллекцию columns)
 *    @param {List} columns
 */
export const changeColumnsAction = columns => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_COLUMN,
  payload: columns
});

/**
 *    action изменения группировки в таблице (получает изменненную коллекцию columns)
 *    @param {List} columns
 */
export const changeGroupingAction = columns => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_GROUPING,
  payload: columns
});

/**
 *    action изменения сортировки столбцов (получает изменненную коллекцию columns)
 *    @param {List} columns
 */
export const changeSortingAction = columns => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SORTING,
  payload: columns
});

/**
 *    action изменения кол-ва отображаемых строк (получает кол-во отображаемых строк)
 *    @param {number} value
 */
export const changePaginationAction = value => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_PAGINATION,
  payload: value
});

/**
 *    action изменения выбранных items (получает изменненную коллекцию list)
 *    @param {List} list
 */
export const changeSelectAction = list => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SELECT,
  payload: list
});

/**
 *    action создание нового платежа
 *    @param {string} type
 */
export const newPaymentAction = type => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_NEW_PAYMENT,
  payload: { type }
});

/**
 *    action перехода к item (получает itemId)
 *    @param {string} itemId
 */
export const routeToItemAction = itemId => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_ROUTE_TO_ITEM,
  payload: itemId
});

/**
 *    action перехода к отзыву (получает itemId, recallId)
 *    @param {string} itemId
 *    @param {string} recallId
 */
export const routeToItemRecallAction = (itemId, recallId) => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_ROUTE_TO_ITEM_RECALL,
  payload: { itemId, recallId }
});

/**
 *    action выполнения операции
 *    @param {string} operationId
 *    @param {array} items
 */
export const operationAction = (operationId, items) => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION,
  payload: { operationId, items }
});

/**
 *    action подтверждения выполнения операции удаления
 *    @param {array} items
 */
export const operationRemoveConfirmAction = items => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_CONFIRM,
  payload: items
});

/**
 *    action выполнения операции с отзывом
 *    @param {string} operationId
 *    @param {string} recallId
 */
export const recallOperationAction = (operationId, recallId) => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION,
  payload: { operationId, recallId }
});

export const mountAction = () => ({
  type: ACTIONS.CPAYMENTS_CONTAINER_MOUNT_ACTION
});
