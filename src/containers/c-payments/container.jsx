import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Spreadsheet, {
  Toolbar,
  Filters,
  Operations,
  Table,
  Selection,
  Export,
  Pagination
} from 'components/ui-components/spreadsheet';
import { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import ConfirmNotification from 'components/notification/confirm';
import PaymentsItemStatus from './payments-item-status';
import { LOCATORS, COMPONENT_STYLE_SELECTOR, FILTERS, COLUMNS } from './constants';
import './style.css';

class CPaymentsContainer extends Component {
  static propTypes = {
    isExportListLoading: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,

    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    location: PropTypes.object.isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,
    panelOperations: PropTypes.instanceOf(List).isRequired,
    sections: PropTypes.instanceOf(List).isRequired,
    selectedItems: PropTypes.instanceOf(List).isRequired,
    selectedItemsSumAmount: PropTypes.string.isRequired,

    getSectionIndexAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changeSectionAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getDictionaryForFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    exportListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeGroupingAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
    changeSelectAction: PropTypes.func.isRequired,
    newPaymentAction: PropTypes.func.isRequired,
    routeToItemAction: PropTypes.func.isRequired,
    routeToItemRecallAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    operationRemoveConfirmAction: PropTypes.func.isRequired,
    recallOperationAction: PropTypes.func.isRequired,
    mountAction: PropTypes.func.isRequired,
    tabs: PropTypes.instanceOf(List).isRequired
  };

  componentWillMount() {
    this.props.getSectionIndexAction();
    this.props.getQueryParamsAction();
    this.props.mountAction();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getSectionIndexAction();
      this.props.getQueryParamsAction();
    } else if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  filterDictionaryFormFieldOptionRenderer = ({ conditionId, option, highlight }) => {
    switch (conditionId) {
      case FILTERS.BENEFICIARY_ACCOUNT:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option[conditionId]}
            describe={option && option.benefName}
            extra={option && option.bankSWIFT}
            highlight={{
              value: highlight,
              inTitle: option && option[conditionId]
            }}
          />
        );
      case FILTERS.PAYER_ACCOUNT:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option[conditionId]}
            describe={option && option.intName}
            extra={option && option.meta}
            highlight={{
              value: highlight,
              inTitle: option && option[conditionId]
            }}
          />
        );
      case FILTERS.CURRENCY:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.title}
            describe={option && option.label}
            extra={option && option.code}
            highlight={{
              value: highlight,
              inTitle: option && option.title
            }}
          />
        );
      case FILTERS.BENEFICIARY:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option[conditionId]}
            describe={option && option.benefAccount}
            extra={option && option.benefSWIFT}
            highlight={{
              value: highlight,
              inTitle: option && option[conditionId]
            }}
          />
        );
      case FILTERS.PURPOSE:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option[conditionId]}
            highlight={{
              value: highlight,
              inTitle: option && option[conditionId]
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  renderItemAlignRightElement = (value) => {
    const style = { textAlign: 'right' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemStatusElement = (status, id) => {
    const { routeToItemRecallAction, recallOperationAction, operations } = this.props;
    let renderedItem = null;
    this.props.list.forEach((group) => {
      group.get('items').forEach((item) => {
        if (item.get('id') === id) renderedItem = item;
      });
    });

    return (
      <PaymentsItemStatus
        item={renderedItem}
        onRecallClickAction={routeToItemRecallAction}
        recallOperationAction={recallOperationAction}
        operations={operations}
      />
    );
  };

  render() {
    const {
      isExportListLoading,
      isListLoading,
      columns,
      filtersConditions,
      filters,
      list,
      operations,
      pages,
      paginationOptions,
      panelOperations,
      sections,
      selectedItems,
      selectedItemsSumAmount,
      exportListAction,
      getListAction,
      changeColumnsAction,
      changeGroupingAction,
      changeFiltersAction,
      getDictionaryForFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSectionAction,
      changeSelectAction,
      changeSortingAction,
      newPaymentAction,
      operationAction,
      operationRemoveConfirmAction,
      resizeColumnsAction,
      routeToItemAction,
      tabs
    } = this.props;

    return (
      <div className={COMPONENT_STYLE_SELECTOR} data-loc={LOCATORS.CONTAINER}>
        <ConfirmNotification onClickOK={operationRemoveConfirmAction} />
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                  newPayment: LOCATORS.TOOLBAR_BUTTON_NEW_PAYMENT
                },
                tabs: LOCATORS.TOOLBAR_TABS
              }}
              title="Валютные переводы"
              buttons={[
                {
                  id: 'refreshList',
                  isProgress: isListLoading,
                  type: 'refresh',
                  title: 'Обновить',
                  onClick: getListAction
                },
                {
                  id: 'newCPayment',
                  type: 'simple',
                  title: 'Создать валютный перевод',
                  onClick: newPaymentAction
                }
              ]}
              tabs={{
                items: tabs,
                selected: sections.findKey(section => section.get('selected')),
                onChange: changeSectionAction
              }}
            />
            {selectedItems.size ?
              <Operations
                dataLoc={LOCATORS.OPERATIONS}
                operations={panelOperations}
                operationAction={operationAction}
                selectedItems={selectedItems}
              />
              :
              <Filters
                dataLocs={{
                  main: LOCATORS.FILTERS,
                  select: LOCATORS.FILTERS_SELECT,
                  add: LOCATORS.FILTERS_ADD,
                  remove: LOCATORS.FILTERS_REMOVE,
                  operations: LOCATORS.FILTERS_OPERATIONS,
                  operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                  operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                  operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                  operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                  conditions: LOCATORS.FILTERS_CONDITIONS,
                  condition: LOCATORS.FILTERS_CONDITION,
                  conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                  conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                  addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                  addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                  close: LOCATORS.FILTERS_CLOSE
                }}
                conditions={filtersConditions}
                filters={filters}
                changeFiltersAction={changeFiltersAction}
                dictionaryFormFieldOptionRenderer={this.filterDictionaryFormFieldOptionRenderer}
                onConditionDictionaryFormInputChange={getDictionaryForFiltersAction}
              />
            }
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                [COLUMNS.DATE]: this.renderItemAlignRightElement,
                [COLUMNS.AMOUNT]: this.renderItemAlignRightElement,
                [COLUMNS.STATUS]: this.renderItemStatusElement
              }}
              renderHeadCells={{
                [COLUMNS.DATE]: this.renderItemAlignRightElement,
                [COLUMNS.AMOUNT]: this.renderItemAlignRightElement
              }}
              settings={{
                iconTitle: 'Настройка таблицы платежей',
                changeColumnsAction,
                showGrouping: true,
                changeGroupingAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              sorting={{
                changeSortingAction
              }}
              select={{
                selectItemTitle: 'Выделить платеж',
                selectGroupTitle: 'Выделить группу платежей',
                selectAllTitle: 'Выделить все платежи',
                changeSelectAction
              }}
              resize={{
                resizeColumnsAction
              }}
              onItemClick={routeToItemAction}
              operations={operations}
              operationAction={operationAction}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {!isListLoading && !!selectedItems.size &&
            <Selection
              dataLoc={LOCATORS.SELECTION}
              main={{
                label: 'Выбрано:',
                value: selectedItems.size
              }}
              extra={{
                label: selectedItemsSumAmount,
                value: ''
              }}
            />
            }
            {!isListLoading && !!list.size && !selectedItems.size &&
            <Export
              dataLoc={LOCATORS.EXPORT}
              label="Выгрузить список в Excel"
              onClick={exportListAction}
              isLoading={isExportListLoading}
            />
            }
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
      </div>
    );
  }
}

export default CPaymentsContainer;
