import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import * as LoacalActions from './actions';
import { mapStateToProps }  from './selectors';
import saga from './sagas';
import reducer from './reducer';
import CPaymentsContainer  from './container';

const withConnect = connect(
  mapStateToProps,
  LoacalActions
);
const withReducer = injectReducer({ key: 'cPaymentsContainerReducers', reducer });
const withSaga = injectSaga({ key: 'cPaymentsContainerSagas', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(CPaymentsContainer);
