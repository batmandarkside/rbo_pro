import React from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Operation from './recall-operation';

const PaymentsItemStatusRecall = (props) => {
  const { item, onClick, onOperationClick, operations } = props;

  const handleOnClick = (e) => {
    e.preventDefault();
    onClick(item);
  };

  const renderOperations = () => {
    const recallOperations = item.get('operations');

    return (
      <div className="b-payments-item-status__recall-item-operations">
        {Object.keys(recallOperations).filter(key => !!recallOperations[key]).map(key => (
          <Operation
            key={key}
            id={key}
            title={operations.find(operation => operation.get('id') === key).get('title')}
            icon={operations.find(operation => operation.get('id') === key).get('icon')}
            recallId={item.get('id')}
            onClick={onOperationClick}
          />
        ))}

      </div>
    );
  };

  return (
    <div className="b-payments-item-status__recall-item">
      <div className="b-payments-item-status__recall-item-row">
        <div className="b-payments-item-status__recall-item-row-label">
          Отзыв
        </div>
        <div className="b-payments-item-status__recall-item-row-value">
          <span className="m-like-link" onClick={handleOnClick}>
            № {item.get('number')} от {item.get('date')}
          </span>
        </div>
      </div>
      <div className="b-payments-item-status__recall-item-row">
        <div className="b-payments-item-status__recall-item-row-label">
          Статус
        </div>
        <div className="b-payments-item-status__recall-item-row-value">
          {item.get('status')}
        </div>
      </div>
      <div className="b-payments-item-status__recall-item-row">
        <div className="b-payments-item-status__recall-item-row-label">
          Причина
        </div>
        <div className="b-payments-item-status__recall-item-row-value">
          <div className="b-payments-item-status__recall-item-reason">
            {item.get('reason')}
          </div>
        </div>
      </div>
      {renderOperations()}
    </div>
  );
};

PaymentsItemStatusRecall.propTypes = {
  item: PropTypes.instanceOf(Map).isRequired,
  onClick: PropTypes.func.isRequired,
  onOperationClick: PropTypes.func.isRequired,
  operations: PropTypes.instanceOf(List).isRequired
};

export default PaymentsItemStatusRecall;
