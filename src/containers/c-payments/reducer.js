import { fromJS, Map } from 'immutable';
import moment from 'moment-timezone';
import { CONDITION_FIELD_TYPE } from 'components/ui-components/spreadsheet';
import {
  ACTIONS,
  FILTERS,
  COLUMNS,
  FILTER_RECALL_VALUES,
  FILTER_MULTI_CURRENCY_VALUES,
  LOCAL_ROUTER_ALIAS
} from './constants';

export const initialSections = [
  {
    id: 'working',
    title: 'Рабочие документы',
    route: LOCAL_ROUTER_ALIAS,
    api: '',
    selected: true
  },
  {
    id: 'archive',
    title: 'Архив',
    route: `${LOCAL_ROUTER_ALIAS}/archive`,
    api: 'archived'
  },
  {
    id: 'trash',
    title: 'Удаленные',
    route: `${LOCAL_ROUTER_ALIAS}/trash`,
    api: 'deleted'
  }
];

export const initialFiltersConditions = [
  {
    id: FILTERS.DATE,
    title: 'Дата',
    type: CONDITION_FIELD_TYPE.DATE_RANGE,
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.STATUS,
    title: 'Статус',
    type: CONDITION_FIELD_TYPE.MULTIPLE_SELECT,
    values: [
      'Доставлен',
      'Импортирован',
      'Отвергнут Банком',
      'Отказан АБС',
      'Отложен',
      'Отозван',
      'Ошибка контроля',
      'Ошибка реквизитов',
      'Подписан',
      'Подтвержден/Обработан',
      'Принят',
      'Принят ВК',
      'Принят в обработку',
      'Создан',
      'Удален',
      'Частично подписан',
      'ЭП/АСП неверна'
    ],
    sections: ['working', 'archive']
  },
  {
    id: FILTERS.AMOUNT,
    title: 'Сумма платежа',
    type: CONDITION_FIELD_TYPE.AMOUNT_RANGE,
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.CURRENCY,
    title: 'Валюта платежа',
    type: CONDITION_FIELD_TYPE.SEARCH,
    maxLength: 3,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.BENEFICIARY,
    title: 'Бенефициар',
    type: CONDITION_FIELD_TYPE.SUGGEST,
    maxLength: 70,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.BENEFICIARY_ACCOUNT,
    title: 'Счет бенефициара',
    type: CONDITION_FIELD_TYPE.SEARCH,
    maxLength: 34,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.PURPOSE,
    title: 'Назначение платежа',
    type: CONDITION_FIELD_TYPE.SUGGEST,
    maxLength: 140,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.PAYER,
    title: 'Перевододатель',
    type: CONDITION_FIELD_TYPE.STRING,
    maxLength: 160,
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.PAYER_ACCOUNT,
    title: 'Счет перевододателя',
    type: CONDITION_FIELD_TYPE.SEARCH,
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.NOTE,
    title: 'Заметки',
    type: CONDITION_FIELD_TYPE.STRING,
    maxLength: 400,
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.RECALL,
    title: 'Отзыв',
    type: CONDITION_FIELD_TYPE.SELECT,
    values: [
      FILTER_RECALL_VALUES.WITH_RECALL,
      FILTER_RECALL_VALUES.WITHOUT_RECALL
    ],
    sections: ['working', 'archive', 'trash']
  },
  {
    id: FILTERS.MULTI_CURRENCY,
    title: 'Мультивалютный',
    type: CONDITION_FIELD_TYPE.SELECT,
    values: [
      FILTER_MULTI_CURRENCY_VALUES.MULTI,
      FILTER_MULTI_CURRENCY_VALUES.SINGLE
    ],
    sections: ['working', 'archive', 'trash']
  }
];

export const initialFilters = [
  {
    id: 'paymentsContainerPresetToday',
    title: 'За сегодня',
    isPreset: true,
    savedConditions: [
      {
        id: FILTERS.DATE,
        params: {
          dateFrom: moment({ hour: 0, minute: 0, seconds: 0 }).format(),
          dateTo: moment({ hour: 0, minute: 0, seconds: 0 }).format()
        }
      }
    ],
    sections: ['working']
  },
  {
    id: 'paymentsContainerPresetToSign',
    title: 'На подпись',
    isPreset: true,
    savedConditions: [
      {
        id: FILTERS.STATUS,
        params: ['Создан', 'Импортирован', 'Частично подписан']
      }
    ],
    sections: ['working']
  },
  {
    id: 'paymentsContainerPresetRejected',
    title: 'Отвергнутые',
    isPreset: true,
    savedConditions: [
      {
        id: FILTERS.STATUS,
        params: ['ЭП/АСП неверна', 'Ошибка реквизитов', 'Отвергнут Банком']
      }
    ],
    sections: ['working']
  }
];

export const initialColumns = [
  {
    id: COLUMNS.NUMBER,
    title: 'Номер',
    canGrouped: false
  },
  {
    id: COLUMNS.DATE,
    title: 'Дата',
    canGrouped: true
  },
  {
    id: COLUMNS.STATUS,
    title: 'Статус',
    canGrouped: true
  },
  {
    id: COLUMNS.AMOUNT,
    title: 'Сумма платежа',
    canGrouped: false
  },
  {
    id: COLUMNS.CURRENCY,
    title: 'Валюта платежа',
    canGrouped: true
  },
  {
    id: COLUMNS.BENEFICIARY,
    title: 'Бенефициар',
    canGrouped: true
  },
  {
    id: COLUMNS.BENEFICIARY_ACCOUNT,
    title: 'Счет бенефециара',
    canGrouped: true
  },
  {
    id: COLUMNS.PURPOSE,
    title: 'Назначение платежа',
    canGrouped: true
  },
  {
    id: COLUMNS.PAYER,
    title: 'Перевододатель',
    canGrouped: true
  },
  {
    id: COLUMNS.PAYER_ACCOUNT,
    title: 'Счет перевододателя',
    canGrouped: true
  },
  {
    id: COLUMNS.BIC,
    title: 'БИК',
    canGrouped: true
  },
  {
    id: COLUMNS.NOTE,
    title: 'Заметки',
    canGrouped: true
  }
];

export const initialOperations = [
  {
    id: 'signAndSend',
    title: 'Подписать и отправить',
    icon: 'sign-and-send',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'sign',
    title: 'Подписать',
    icon: 'sign',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'unarchive',
    title: 'Вернуть из архива',
    icon: 'unarchive',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'repeat',
    title: 'Повторить',
    icon: 'repeat',
    disabled: false,
    progress: false,
    grouped: false
  },
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'recall',
    title: 'Отозвать',
    icon: 'recall',
    disabled: false,
    progress: false,
    grouped: false
  },
  {
    id: 'send',
    title: 'Отправить',
    icon: 'send',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'archive',
    title: 'Переместить в архив',
    icon: 'archive',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'visa',
    title: 'Виза',
    icon: 'visa',
    disabled: false,
    progress: false,
    grouped: true
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialSettings = {
  visible: [
    COLUMNS.NUMBER,
    COLUMNS.DATE,
    COLUMNS.AMOUNT,
    COLUMNS.STATUS,
    COLUMNS.CURRENCY,
    COLUMNS.BENEFICIARY,
    COLUMNS.BENEFICIARY_ACCOUNT,
    COLUMNS.PURPOSE,
    COLUMNS.PAYER,
    COLUMNS.PAYER_ACCOUNT,
    COLUMNS.BIC,
    COLUMNS.NOTE
  ],
  sorting: { id: COLUMNS.DATE, direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    [COLUMNS.NUMBER]: 92,
    [COLUMNS.DATE]: 92,
    [COLUMNS.STATUS]: 160,
    [COLUMNS.AMOUNT]: 160,
    [COLUMNS.CURRENCY]: 180,
    [COLUMNS.BENEFICIARY]: 196,
    [COLUMNS.BENEFICIARY_ACCOUNT]: 240,
    [COLUMNS.PURPOSE]: 196,
    [COLUMNS.PAYER]: 196,
    [COLUMNS.PAYER_ACCOUNT]: 240,
    [COLUMNS.BIC]: 148,
    [COLUMNS.NOTE]: 360
  }
};

export const initialState = fromJS({
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: [],
  filters: [],
  list: [],
  operations: initialOperations,
  selectedItems: []
});

export const getSectionIndexSuccess = (state, sectionIndex) => {
  const selectedSectionId = state.get('sections').find((section, key) => key === sectionIndex).get('id');
  return state.merge({
    sections: state.get('sections').map((section, key) => section.set('selected', key === sectionIndex)),
    filtersConditions: fromJS(initialFiltersConditions)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections')),
    filters: fromJS(initialFilters)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections'))
  });
};

export const mapSettings = (state, settings) => {
  const immutableSettings = fromJS(settings).delete('filters');
  const columns = state.get('columns');
  const width = columns.reduce((result, current) =>
    result.set(current.get('id'), immutableSettings.getIn(['width', current.get('id')])), Map()
  );

  return immutableSettings.set('width', width);
};

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings
    ? mapSettings(state, settings)
    : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters = state.get('filters').map(item => item.set('selected', false));
  if (filter) {
    if (filters.find(item => item.get('id') === filter.id)) {
      filters = filters.map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = filters.push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const changeSection = (state, sectionIndex) => (
  state.getIn(['sections', sectionIndex, 'selected']) ?
    state :
    state.merge({
      columns: fromJS(initialColumns),
      settings: fromJS(initialSettings),
      pages: fromJS(initialPages),
      paginationOptions: fromJS(initialPaginationOptions)
    })
);

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], true)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  selectedItems: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const idsList = fromJS(list).map(item => item.get('recordID'));
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!idsList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (idsList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: idsList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: idsList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([])
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

/**
 * установить операцию в статус - активна ( в прогрессе )
 * @param state
 * @param operationId
 */
export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

/**
 * оброс активной операции и опциональный релоад скроллера
 * @param state
 * @param operationId
 * @param isListUpdateRequired
 */
export const operationFinish = (state, operationId, isListUpdateRequired = false) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

export const recallOperationFinish = state => state.merge({
  isListUpdateRequired: true
});

export default function cpaymentsContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.CPAYMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS:
      return getSectionIndexSuccess(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SECTION:
      return changeSection(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.CPAYMENTS_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.CPAYMENTS_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.CPAYMENTS_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, action.payload);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationRequest(state, 'print');

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_FAIL:
      return operationFinish(state, 'print', false);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_REQUEST:
      return operationRequest(state, 'archive');

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_SUCCESS:
      return operationFinish(state, 'archive', true);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_FAIL:
      return operationFinish(state, 'archive', false);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_REQUEST:
      return operationRequest(state, 'unarchive');

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_SUCCESS:
      return operationFinish(state, 'unarchive', true);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_FAIL:
      return operationFinish(state, 'unarchive', false);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_CONFIRM:
      return operationRequest(state, 'remove');

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationFinish(state, 'remove', true);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationFinish(state, 'remove', false);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_REQUEST:
      return operationRequest(state, 'sign');

    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_SUCCESS:
      return operationFinish(state, 'sign', true);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_CANCEL:
    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_FAIL:
      return operationFinish(state, 'sign', false);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST:
      return operationRequest(state, 'signAndSend');

    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS:
      return operationFinish(state, 'signAndSend', true);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_FAIL:
    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_CANCEL:
      return operationFinish(state, 'signAndSend', false);
      
    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_VISA_REQUEST:
      return operationRequest(state, 'visa');

    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_SUCCESS:
      return operationFinish(state, 'visa', true);

    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_FAIL:
    case ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_CANCEL:
      return operationFinish(state, 'visa', false);

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_REQUEST:
      return operationRequest(state, 'send');

    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_SUCCESS:
    case ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_FAIL:
      return operationFinish(state, 'send', true);

    case ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_SUCCESS:
    case ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SUCCESS:
      return recallOperationFinish(state);

    default:
      return state;
  }
}
