import { put, call, select, takeLatest, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction,
  addConfirmNotification as dalAddConfirmNotification
} from 'dal/notification/actions';
// todo реализовать и раскомментировать
// import {
//   exportCPaymentsListToExcel as dalExportCPaymentsListToExcel,
// } from 'dal/curtransfers/sagas';

import {
  archive as dalArchiveCPayments,
  unarchive as dalUnarchiveCPayments,
  remove as dalRemoveCPayments,
  documentSendToBankSaga
} from 'dal/documents/sagas';

import {
  warningSignDocSaga,
  successSignDocSaga
} from 'dal/sign/sagas/notifications-saga';

import { signDocAddAndTrySigningAction } from 'dal/sign/actions';

import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import { getBeneficiars as dalGetBeneficiars } from 'dal/beneficiaries/sagas';
import {
  getCurrencyCodes as dalGetCurrCodes,
  getPaymentPurposes as dalGetPaymentPurposes
} from 'dal/dictionaries/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import {
  fetchCurtransfers as dalFetchCurtransfers,
  printPayment as dalPrintCPayments
} from 'dal/curtransfers/sagas';

import {
  ACTIONS,
  LOCAL_ROUTER_ALIAS,
  LOCAL_REDUCER,
  SETTINGS_ALIAS,
  VALIDATE_SETTINGS,
  FILTERS,
  FILTER_RECALL_VALUES,
  FILTER_MULTI_CURRENCY_VALUES
} from './constants';

import { calculateAmount } from './util';

const getParamsForDalOperation = ({ sections, settings, pages, filters, type }) => {
  const section = sections.find(item => item.get('selected')).get('api');
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const visibleColumns = settings.get('visible').toJS();
  const selectedFilter = filters.find(filter => filter.get('selected'));
  const conditions = selectedFilter && selectedFilter.get('conditions');
  const filter = {};

  conditions && conditions.forEach((condition) => {
    const conditionId = condition.get('id');
    switch (conditionId) {
      case FILTERS.DATE:
        filter.dateFrom = condition.getIn(['params', 'dateFrom'], null);
        filter.dateTo = condition.getIn(['params', 'dateTo'], null);
        break;
      case FILTERS.AMOUNT:
        filter.sumMin = condition.getIn(['params', 'amountFrom'], null);
        filter.sumMax = condition.getIn(['params', 'amountTo'], null);
        break;
      case FILTERS.STATUS:
        filter[conditionId] = condition.get('params')
          ? condition.get('params').toJS()
          : null;
        break;
      case FILTERS.CURRENCY:
        filter[conditionId] = condition.get('params', null);
        break;
      case FILTERS.BENEFICIARY:
        filter[conditionId] = condition.get('params', null);
        break;
      case FILTERS.BENEFICIARY_ACCOUNT:
        filter[conditionId] = condition.get('params', null);
        break;
      case FILTERS.PURPOSE:
        filter[conditionId] = condition.get('params', null);
        break;
      case FILTERS.PAYER:
        filter.payerName = condition.get('params', null);
        break;
      case FILTERS.PAYER_ACCOUNT:
        filter.payerAccount = condition.get('params', null);
        break;
      case FILTERS.NOTE:
        filter[conditionId] = condition.get('params', null);
        break;
      case FILTERS.RECALL:
        filter[conditionId] = condition.get('params') === FILTER_RECALL_VALUES.WITH_RECALL;
        break;
      case FILTERS.MULTI_CURRENCY:
        filter[conditionId] = condition.get('params') === FILTER_MULTI_CURRENCY_VALUES.MULTI;
        break;
      default:
        break;
    }
  });

  const params = {
    section,
    sort,
    desc,
    ...filter
  };

  switch (type) {
    case 'list':
      params.offset = pages.get('selected') * settings.get('pagination');
      params.offsetStep = settings.get('pagination') + 1;
      break;
    case 'export':
      params.visibleColumns = visibleColumns;
      break;
    default:
      break;
  }

  return params;
};

const cpaysListSelector = state => state.getIn(['curtransfers', 'list']);
export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);

// sign
export const compileForSignSelector = state => state.getIn(['sign', 'saveAfterTrySignGroupDocuments', 'compileForSign']);
export const compileNotSignSelector = state => state.getIn(['sign', 'saveAfterTrySignGroupDocuments', 'compileNotSign']);
export const realitySignSelector = state => state.getIn(['sign', 'saveRealitySignGroupDocuments']);
export const saveDocIdForFutureSignSelector = state => state.getIn(['sign', 'saveDocIdForFutureSign']);

export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);

const curtransfersPrintSelector = (state) => {
  const selectedItems = state.getIn([LOCAL_REDUCER, 'selectedItems']);

  if (selectedItems.size > 1) {
    return selectedItems.toJS();
  }

  const data = state.getIn(['curtransfers', 'list']);
  const filteredData = data
    .filter(item => selectedItems.indexOf(item.get('recordID')) !== -1)
    .map(item => ({
      docId: item.get('recordID'),
      docNumber: item.getIn(['docInfo', 'docNumber']),
      docDate: item.getIn(['docInfo', 'docDate'])
    }))
  ;
  return filteredData.toJS();
};

export function* getSectionIndex() {
  const location = yield select(routingLocationSelector);
  const sections = yield select(localSectionsSelector);
  const pathname = location.get('pathname', '');
  let sectionIndex = 0;
  sections.forEach((section, key) => {
    if (pathname.match(new RegExp(section.get('route'), 'gi'))) {
      sectionIndex = key;
    }
  });
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: sectionIndex });
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_GET_SETTINGS_REQUEST });
}

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload: { pageIndex, filter } });
}

export function* changeSection(action) {
  const { payload } = action;
  const sections = yield select(localSectionsSelector);
  const activeSection = sections.find(section => section.get('id') === payload);
  yield put(push({ pathname: activeSection.get('route'), search: null }));
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;

  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* getDictionaryForFilters({ payload }) {
  try {
    const { conditionId, value = '' } = payload;
    let result = [];
    switch (conditionId) {
      case FILTERS.CURRENCY:
        // справочник КОД ВАЛЮТЫ
        result = yield call(dalGetCurrCodes, { payload: { every: value } });
        break;
      case FILTERS.BENEFICIARY:
        // справочник БЕНЕФЕЦИАРОВ
        result = yield call(dalGetBeneficiars, { payload: { [conditionId]: value } });
        break;
      case FILTERS.BENEFICIARY_ACCOUNT:
        // справочник БЕНЕФЕЦИАРОВ
        result = yield call(dalGetBeneficiars, { payload: { [conditionId]: value } });
        break;
      case FILTERS.PURPOSE:
        // справочник НАЗНАЧЕНИЙ ПЛАТЕЖА
        result = yield call(dalGetPaymentPurposes, { payload: { purpose: value } });
        break;
      case FILTERS.PAYER_ACCOUNT:
        // справочник СЧЕТА
        result = yield call(dalGetAccounts, { payload: { accNum: value } });
        break;
      default:
        break;
    }
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
      payload: {
        conditionId,
        items: result
      }
    });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getList() {
  try {
    const sections = yield select(localSectionsSelector);
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ sections, settings, pages, filters, type: 'list' });
    const result = yield call(dalFetchCurtransfers, { payload: params });

    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_GET_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* exportList() {
  try {
    // const sections = yield select(localSectionsSelector);
    // const settings = yield select(localSettingsSelector);
    // const filters = yield select(localFiltersSelector);
    // const params = getParamsForDalOperation({ sections, settings, pages: null, filters, type: 'export' });
    // todo реализовать запрос на экспорт
    // const result = yield call(dalExportCPaymentsListToExcel, { payload: params });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_SUCCESS/* , payload: result */ });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* newPayment(action) {
  const { payload } = action;
  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/new`,
    search: `?${qs.stringify(payload)}`
  }));
}

export function* routeToItem(action) {
  const { payload } = action;
  yield put(push(`${LOCAL_ROUTER_ALIAS}/${payload}`));
}

export function* routeToItemRecall(action) {
  const { payload } = action;
  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/${payload.itemId}/recalls/${payload.recallId}`,
  }));
}

export function* operation(action) {
  const { payload: { operationId, items } } = action;

  switch (operationId) {
    case 'print':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_REQUEST, payload: items });
      break;
    case 'archive':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_REQUEST, payload: items });
      break;
    case 'unarchive':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_REQUEST, payload: items });
      break;
    case 'remove':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_REQUEST, payload: items });
      break;
    case 'sign':
      yield put({
        type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'signAndSend':
      yield put({
        type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'visa':
      yield put({
        type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_VISA_REQUEST,
        payload: {
          docIds: items,
          visa: true
        }
      });
      break;
    case 'send':
      yield put({
        type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'repeat':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REPEAT, payload: items });
      break;
    case 'recall':
      yield put({
        type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_RECALL,
        payload: {
          id: items[0]
        }
      });
      break;
    default:
      break;
  }
}

export function* operationRecall(action) {
  const { id } = action.payload;

  yield put(push(`${LOCAL_ROUTER_ALIAS}/${id}/recalls/new`));
}

export function* operationPrint() {
  const selectedData = yield select(curtransfersPrintSelector);
  try {
    yield call(dalPrintCPayments, {
      payload: selectedData
    });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_SUCCESS, selectedData });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationArchive(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalArchiveCPayments, { payload: { docId: payload } });
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
      payload: `Документ${many ? 'ы' : ''} помещен${many ? 'ы' : ''} в архив`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationUnarchive(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalUnarchiveCPayments, { payload: { docId: payload } });
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_SUCCESS,
      payload: `Документ${many ? 'ы' : ''} возвращен${many ? 'ы' : ''} из архива`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationRemove(action) {
  const { payload } = action;
  const many = payload.length > 1;
  yield put(dalAddConfirmNotification({
    title: `Удаление документ${many ? 'ов' : 'а'}`,
    text: `Вы действительно хотите удалить документ${many ? 'ы' : ''}?`,
    labelSuccess: 'Подтвердить',
    labelCancel: 'Отмена',
    uid: payload[0],
    actionData: payload
  }));
}

export function* operationRemoveConfirm(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalRemoveCPayments, { payload: { docId: payload } });
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: `Документ${many ? 'ы' : ''} успешно удален${many ? 'ы' : ''}`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* signDocument({ payload }) {
  const payloadWithChannel = {
    ...payload,

    // уникальный канал подписи для контейнера
    channel: {
      success: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_SUCCESS,
      cancel: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_CANCEL,
      fail: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(payloadWithChannel));
}

export function* signAndSend({ payload }) {
  const payloadWithChannel = {
    ...payload,

    // уникальный канал подписи для контейнера
    channel: {
      success: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS,
      cancel: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_CANCEL,
      fail: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(payloadWithChannel));
}

export function* signVisa({ payload }) {
  const payloadWithChannel = {
    ...payload,

    // уникальный канал подписи для контейнера
    channel: {
      success: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_SUCCESS,
      cancel: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_CANCEL,
      fail: ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(payloadWithChannel));
}

export function* operationSend(action) {
  const { payload: { docIds } } = action;
  try {
    yield call(documentSendToBankSaga, { payload: { docId: docIds } });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_SUCCESS });
  } catch (error) {
    put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_FAIL });
  }
}

export function* operationSendAfterSign(action) {
  const { payload: { resultData } } = action;
  const { docId } = resultData[0];
  try {
    yield call(documentSendToBankSaga, { payload: { docId } });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
  }
}

export function* operationRepeat({ payload }) {
  yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_NEW_PAYMENT, payload: { type: 'repeat', itemId: payload[0] } });
}

export function* recallOperation(action) {
  const { payload: { operationId, recallId } } = action;
  switch (operationId) {
    case 'print':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_REQUEST, payload: recallId });
      break;
    case 'sign':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_REQUEST, payload: recallId });
      break;
    case 'signAndSend':
      yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_REQUEST, payload: recallId });
      break;
    case 'send':
      yield put({
        type: ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_REQUEST,
        payload: {
          docIds: [recallId]
        }
      });
      break;

    default:
      break;
  }
}

export function* recallOperationPrint(action) {
  const { payload } = action;
  try {
    const result = yield call(dalPrintCPayments, { payload: { id: payload } });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_SUCCESS, payload: result  });
  } catch (error) {
    yield put({
      type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* recallOperationSign(action) {
  const { payload } = action;

  const params = {
    docIds: [payload],
    channel: {
      success: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_SUCCESS,
      cancel: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_CANCEL,
      fail: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(params));
}

export function* recallOperationSignSuccess() {
  yield put(successNotification({ payload: 'Отзыв успешно подписан' }));
}

export function* recallOperationSignAndSend(action) {
  const { payload } = action;

  const params = {
    docIds: [payload],
    channel: {
      success: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SIGNED,
      cancel: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_CANCEL,
      fail: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(params));
}

export function* recallOperationSignAndSendSigned(action) {
  const { payload: { resultData } } = action;
  const docIds = resultData.map(r => r.docId);
  try {
    yield call(documentSendToBankSaga, { payload: { docId: docIds } });
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_FAIL });
  }
}

export function* successNotification(action) {
  const { payload } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'success', title: payload }));
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}


/**
 * логика нотификаций после успешной подписи документа
 * зависит от того групповая ли это подпись или одного документа
 * есть ли не подписанные документы в группе
 */
export function* notificationSignSaga() {
  const compileForSign = yield select(compileForSignSelector);
  const compileNotSign = yield select(compileNotSignSelector);
  const realitySign = yield select(realitySignSelector);
  const saveDocIdForFutureSign = yield select(saveDocIdForFutureSignSelector);
  const cpays = yield select(cpaysListSelector);
  const isGroupSign = saveDocIdForFutureSign.size > 1;
  const countAmount = calculateAmount(realitySign.map(r => r.get('docId')), cpays);

  // ГРУППОВЫЕ ОПЕРАЦИИ
  if (isGroupSign) {
    if (compileNotSign.size && realitySign.size) { // при условии что хоть один из группы документов подписан
      yield warningSignDocSaga(
        compileForSign.size,
        realitySign.size,
        compileNotSign.size,
        countAmount
      );
    }

    if (!compileNotSign.size && realitySign.size) { // все выбранные документы подписаны
      yield successSignDocSaga(
        compileForSign.size,
        realitySign.size,
        countAmount
      );
    }
  }

  // НЕ ГРУППОВЫЕ ОПЕРАЦИИ
  if (!isGroupSign) {
    yield put(dalAddOrdinaryNotificationAction({
      type: 'success',
      message: 'Документ успешно подписан'
    }));
  }
}

export function* mountSaga() {
  yield all([
    dalGetCurrCodes({ payload: {} })
  ]);
}

export default function* saga() {
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_MOUNT_ACTION, mountSaga);

  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_GET_SECTION_INDEX_REQUEST, getSectionIndex);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SECTION, changeSection);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST, getDictionaryForFilters);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_REQUEST, exportList);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_EXPORT_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANGE_GROUPING, changeSettingsColumns);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_NEW_PAYMENT, newPayment);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_ROUTE_TO_ITEM, routeToItem);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_ROUTE_TO_ITEM_RECALL, routeToItemRecall);

  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION, operation);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_REQUEST, operationPrint);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_PRINT_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_REQUEST, operationArchive);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_ARCHIVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_REQUEST, operationUnarchive);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_UNARCHIVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemove);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirm);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REMOVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_REPEAT, operationRepeat);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_RECALL, operationRecall);

  // подпись
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_REQUEST, signDocument);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST, signAndSend);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_VISA_REQUEST, signVisa);

  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_SUCCESS, notificationSignSaga);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_SUCCESS, notificationSignSaga);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS, notificationSignSaga);

  // отправить в банк
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_OPERATION_SEND_REQUEST, operationSend);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS, operationSendAfterSign);

  // отзыв
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION, recallOperation);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_REQUEST, recallOperationPrint);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_REQUEST, recallOperationSign);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_SUCCESS, recallOperationSignSuccess);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_REQUEST, recallOperationSignAndSend);
  yield takeLatest(ACTIONS.CPAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SIGNED, recallOperationSignAndSendSigned);
}
