import { createSelector } from 'reselect';
import { List, Map, OrderedMap } from 'immutable';
import numeral from 'numeral';
import moment from 'moment-timezone';
import { getFormattedAccNum } from 'utils';
import { CONDITION_FIELD_TYPE } from 'components/ui-components/spreadsheet';
import { LOCAL_REDUCER, COLUMNS, FILTERS } from './constants';
import { calculateAmount } from './util';

const isExportListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isExportListLoading']);
const isListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isListLoading']);
const isListUpdateRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isListUpdateRequired']);
const isReloadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isReloadRequired']);

const columnsSelector = state => state.getIn([LOCAL_REDUCER, 'columns']);
const dalListSelector = state => state.getIn(['curtransfers', 'list']);
const filtersConditionsSelector = state => state.getIn([LOCAL_REDUCER, 'filtersConditions']);
const filtersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
const listSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);
const sectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
const selectedItemsSelector = state => state.getIn([LOCAL_REDUCER, 'selectedItems']);
const settingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
const pagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
const paginationOptionsSelector = state => state.getIn([LOCAL_REDUCER, 'paginationOptions']);

const mapListItemOperations = (allowedSmActions, operations) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'repeat':
        case 'print':
        case 'saveAsTemplate':
          return true;
        case 'signAndSend':
        case 'visa':
          return allowedSmActions.get('sign');
        case 'remove':
          return allowedSmActions.get('delete');
        case 'unarchive':
          return allowedSmActions.get('fromArchive');
        default:
          return allowedSmActions.get(operationId);
      }
    })), OrderedMap()
  ).toJS()
);

const mapListRecallOperations = (allowedSmActions, operations) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'print':
          return true;
        case 'signAndSend':
          return allowedSmActions.get('sign');
        default:
          return allowedSmActions.get(operationId);
      }
    })), OrderedMap()
  ).toJS()
);

const mapListItem = (item, selected, operations) => {
  const docDate = item.getIn(['docInfo', COLUMNS.DATE])
    ? moment(item.getIn(['docInfo', COLUMNS.DATE])).format('DD.MM.YYYY')
    : null;
  const amount = !isNaN(item.getIn(['moneyInfo', COLUMNS.AMOUNT]))
    ? numeral(item.getIn(['moneyInfo', COLUMNS.AMOUNT])).format('0,0.00')
    : null;

  return Map({
    id: item.get('recordID'),
    [COLUMNS.NUMBER]: item.getIn(['docInfo', COLUMNS.NUMBER]),
    [COLUMNS.DATE]: docDate,
    [COLUMNS.AMOUNT]: amount,
    [COLUMNS.CURRENCY]: item.getIn(['moneyInfo', COLUMNS.CURRENCY]),
    [COLUMNS.STATUS]: item.getIn(['docInfo', COLUMNS.STATUS]),
    [COLUMNS.BENEFICIARY]: item.getIn(['beneficiar', COLUMNS.BENEFICIARY]),
    [COLUMNS.BENEFICIARY_ACCOUNT]: item.getIn(['beneficiar', COLUMNS.BENEFICIARY_ACCOUNT]),
    [COLUMNS.PURPOSE]: item.getIn(['docInfo', COLUMNS.PURPOSE]),
    [COLUMNS.PAYER]: item.getIn(['payer', COLUMNS.PAYER]),
    [COLUMNS.PAYER_ACCOUNT]: getFormattedAccNum(item.getIn(['payer', COLUMNS.PAYER_ACCOUNT])),
    [COLUMNS.BIC]: item.getIn(['payer', COLUMNS.BIC]),
    [COLUMNS.NOTE]: item.getIn(['docInfo', COLUMNS.NOTE]),
    selected,
    operations: mapListItemOperations(item.get('allowedSmActions'), operations),
    recalls: item.get('linkedDocs')
      .filter(doc => doc.get('docType') === 'Recall')
      .sort((a, b) => {
        const aNumber = parseInt(a.get('docNumber'), 10);
        const bNumber = parseInt(b.get('docNumber'), 10);
        if (aNumber < bNumber) { return 1; }
        if (aNumber > bNumber) { return -1; }
        return 0;
      })
      .map(recall => Map({
        id: recall.get('recordID'),
        parentId: recall.get('linkedRecordID'),
        number: recall.get('docNumber'),
        date: moment(recall.get('docDate')).format('DD.MM.YYYY'),
        status: recall.get('status'),
        reason: recall.get('reason'),
        operations: mapListRecallOperations(recall.get('allowedSmActions'), operations)
      }))
  });
};

const mapFilterConditionDictionaryItems = condition =>
  condition.setIn(
    ['values', 'items'],
    condition.getIn(['values', 'items']).map((item) => {
      const conditionId = condition.get('id');
      switch (conditionId) {
        case FILTERS.CURRENCY:
          return item.merge({
            [conditionId]: item.get('isoCode'),
            label: item.get('name'),
            title: item.get('isoCode'),
            value: item.get('id')
          });
        case FILTERS.PURPOSE:
          return item.merge({
            [conditionId]: item.get('purpose'),
            label: item.get('purpose'),
            title: item.get('purpose'),
            value: item.get('id')
          });
        case FILTERS.BENEFICIARY:
          return item.get('benefInfo').merge({
            label: item.getIn(['benefInfo', conditionId]),
            bankSWIFT: item.getIn(['beBankInfo', 'benefBankSWIFT']),
            title: item.getIn(['benefInfo', conditionId]),
            value: item.get('id')
          });
        case FILTERS.BENEFICIARY_ACCOUNT:
          return item.get('benefInfo').merge({
            label: item.getIn(['benefInfo', conditionId]),
            bankSWIFT: item.getIn(['beBankInfo', 'benefBankSWIFT']),
            title: item.getIn(['benefInfo', conditionId]),
            value: item.get('id')
          });
        case FILTERS.PAYER_ACCOUNT:
          return item.merge({
            [conditionId]: item.get('accNum'),
            label: getFormattedAccNum(item.get('accNum')),
            meta: `${item.get('availableBalance')} ${item.get('currCodeIso')}`,
            title: getFormattedAccNum(item.get('accNum')),
            value: item.get('id')
          });
        default:
          return null;
      }
    })
  );

const mapFilterConditionParamsToValue = (params, condition) => {
  if (!params) return '...';
  switch (condition.get('type')) {
    case CONDITION_FIELD_TYPE.DATE_RANGE:
      return `${params.get('dateFrom') ?
        `с ${moment(params.get('dateFrom')).format('DD.MM.YYYY')}` : ''} ${params.get('dateTo') ?
        `по ${moment(params.get('dateTo')).format('DD.MM.YYYY')}` : ''}`;
    case CONDITION_FIELD_TYPE.AMOUNT_RANGE:
      return `${params.get('amountFrom') ? 
        `от ${numeral(params.get('amountFrom')).format('0,0.00')}` : ''} ${params.get('amountTo') ?
        `до ${numeral(params.get('amountTo')).format('0,0.00')}` : ''}`;
    case CONDITION_FIELD_TYPE.BOOLEAN:
      return '';
    case CONDITION_FIELD_TYPE.MULTIPLE_SELECT:
      return params.join(', ');
    case CONDITION_FIELD_TYPE.SEARCH:
    case CONDITION_FIELD_TYPE.SUGGEST:
      switch (condition.get('id')) {
        case FILTERS.BENEFICIARY:
          return condition.getIn(['values', 'items']);
        case FILTERS.BENEFICIARY_ACCOUNT:
          return params.get(condition.getIn(['values', 'labelKey']));
        case FILTERS.PAYER_ACCOUNT:
          return getFormattedAccNum(params.get(condition.getIn(['values', 'labelKey'])));
        case FILTERS.CURRENCY:
        default:
          return params;
      }
    default:
      return params;
  }
};

const mapFilterConditions = (conditions, savedConditions, filtersConditions) => (
  (conditions && conditions.size ? conditions : savedConditions).map(condition =>
    condition.merge({
      params: condition.get('params'),
      value: mapFilterConditionParamsToValue(
        condition.get('params'),
        filtersConditions.find(filtersCondition => filtersCondition.get('id') === condition.get('id'))
      )
    })
  )
);

export const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) => columns.map(item => item.merge({
    visible: !!settings.get('visible').find(visible => visible === item.get('id')),
    sorting: settings.getIn(['sorting', 'id']) === item.get('id') ? settings.getIn(['sorting', 'direction']) : 0,
    grouped: settings.get('grouped') === item.get('id'),
    width: settings.getIn(['width', item.get('id')])
  }))
);

export const filtersConditionsCreatedSelector = createSelector(
  filtersConditionsSelector,
  filtersConditions => filtersConditions.map(
    condition => (condition.get('type') === CONDITION_FIELD_TYPE.DICTIONARY
      ? mapFilterConditionDictionaryItems(condition)
      : condition
    )
  )
);

export const filtersCreatedSelector = createSelector(
  filtersSelector,
  filtersConditionsSelector,
  (filters, filtersConditions) => filters
    .map(filter => filter.merge({
      conditions: mapFilterConditions(filter.get('conditions'), filter.get('savedConditions'), filtersConditions)
    }))
    .sort((a, b) => {
      if (a.get('isPreset') && b.get('isPreset')) return 0;
      if (a.get('isPreset') && !b.get('isPreset')) return -1;
      if (!a.get('isPreset') && b.get('isPreset')) return 1;
      return a.get('title').localeCompare(b.get('title'));
    })
);

export const listCreatedSelector = createSelector(
  listSelector,
  selectedItemsSelector,
  dalListSelector,
  settingsSelector,
  operationsSelector,
  (list, selectedItems, dalList, settings, operations) => {
    if (!list.size) return list;
    const mappedList = list.map(item =>
      mapListItem(
        dalList.find(dalListItem => dalListItem.get('recordID') === item),
        !!selectedItems.filter(selectedItem => selectedItem === item).size,
        operations
      ));
    const grouped = settings.get('grouped');
    if (!grouped) {
      return List([Map({
        id: 'notGrouped',
        selected: mappedList.filter(item => item.get('selected')).size === mappedList.size,
        items: mappedList
      })]);
    }
    return mappedList.map(item => item.get(grouped)).toOrderedSet().toList().map((group, i) => {
      const items = mappedList.filter(item => item.get(grouped) === group);
      return Map({
        id: `${grouped}_${i}`,
        title: group,
        selected: items.filter(item => item.get('selected')).size === items.size,
        items
      });
    });
  }
);

export const paginationOptionsCreatedSelector = createSelector(
  paginationOptionsSelector,
  settingsSelector,
  (paginationOptions, settings) => paginationOptions.map(item => ({
    value: item,
    title: item,
    selected: settings.get('pagination') === item
  }))
);

export const panelOperationsCreatedSelector = createSelector(
  operationsSelector,
  selectedItemsSelector,
  dalListSelector,
  (operations, selectedItems, list) => operations.filter((operation) => {
    const itemsWithOperations = selectedItems.map(item => Map({
      id: item,
      operations: mapListItemOperations(
        list.find(listItem => listItem.get('recordID') === item).get('allowedSmActions'),
        operations
      )
    }));
    return (
      itemsWithOperations.size ===
      itemsWithOperations.filter(item => !!item.get('operations')[operation.get('id')]).size
      ) &&
      (selectedItems.size === 1 || operation.get('grouped'));
  })
);

export const selectedItemsSumCreatedSelector = createSelector(
  selectedItemsSelector,
  dalListSelector,
  (selectedItems, dalList) =>
    calculateAmount(selectedItems, dalList)
);

export const tabCreateSelector = createSelector(
  sectionsSelector,
  sections => sections.map(section => section.merge({
    isActive: section.get('selected')
  }))
);

export const mapStateToProps = state => ({
  isListLoading: isListLoadingSelector(state),
  isExportListLoading: isExportListLoadingSelector(state),
  isListUpdateRequired: isListUpdateRequiredSelector(state),
  isReloadRequired: isReloadRequiredSelector(state),
  columns: columnsCreatedSelector(state),
  filtersConditions: filtersConditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  list: listCreatedSelector(state),
  operations: operationsSelector(state),
  pages: pagesSelector(state),
  paginationOptions: paginationOptionsCreatedSelector(state),
  panelOperations: panelOperationsCreatedSelector(state),
  sections: sectionsSelector(state),
  selectedItems: selectedItemsSelector(state),
  selectedItemsSumAmount: selectedItemsSumCreatedSelector(state),
  settings: settingsSelector(state),
  tabs: tabCreateSelector(state)
});
