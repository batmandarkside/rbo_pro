import { Map, List } from 'immutable';
import numeral from 'numeral';
/**
 *
 * @param selectedItems
 * @param cpays
 * @returns {string}
 */
export function calculateAmount(selectedItems, cpays) {
  let result = List();

  selectedItems.forEach((id) => {
    const document = cpays.find(dalListItem => dalListItem.get('recordID') === id);

    const amountTrans = document.getIn(['moneyInfo', 'amountTrans'], null);
    const currency = document.getIn(['moneyInfo', 'currTransISOCode'], null);
    const amount = amountTrans !== null
      ? numeral(amountTrans).format('0,0.00')
      : amountTrans;

    if (currency && amount) {
      if (result.find(item => item.get('currency') === currency)) {
        result = result.map(item => (
          item.get('currency') === currency
            ? item.set('amount', numeral(numeral(item.get('amount')).value() + numeral(amount).value()).format('0,0.00'))
            : item
        ));
      } else {
        result = result.push(Map({
          currency,
          amount
        }));
      }
    }
  });

  return result.map(item => (`${item.get('amount')} ${item.get('currency')}`)).join(' | ');
}
