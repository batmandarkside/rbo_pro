import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Loader from '@rbo/components/lib/loader/Loader';
import { getDocStatusClassNameMode, getScrollTopByDataId, smoothScrollTo } from 'utils';
import RboDocument, {
  RboDocumentPanel,
  RboDocumentOperations,
  RboDocumentContent,
  RboDocumentMain,
  RboDocumentHead,
  RboDocumentHeadStatus,
  RboDocumentHeadTabs,
  RboDocumentBody,
  RboDocumentExtra,
  RboDocumentExtraSection,
  RboDocumentStructure,
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import RboForm, { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import { getFieldElementSelectorByIds } from 'components/ui-components/rbo-form-compact/utils';
import { SectionPrimary } from './sections';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';

class CorrespondentContainer extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),

    isDataError: PropTypes.bool,
    isDataLoading: PropTypes.bool,
    isEditable: PropTypes.bool.isRequired,

    documentTabs: PropTypes.instanceOf(List).isRequired,
    documentStructure: PropTypes.instanceOf(List).isRequired,
    documentValues: PropTypes.instanceOf(Map).isRequired,
    documentData: PropTypes.instanceOf(Map).isRequired,
    documentDictionaries: PropTypes.instanceOf(Map).isRequired,
    formSections: PropTypes.instanceOf(Map).isRequired,
    formWarnings: PropTypes.instanceOf(Map).isRequired,
    formErrors: PropTypes.instanceOf(Map).isRequired,
    operations: PropTypes.instanceOf(List).isRequired,

    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    changeTabAction: PropTypes.func.isRequired,
    fieldFocusAction: PropTypes.func.isRequired,
    fieldBlurAction: PropTypes.func.isRequired,
    changeDataAction: PropTypes.func.isRequired,
    dictionarySearchAction: PropTypes.func.isRequired,
    sectionCollapseToggleAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
  };

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.mountAction(params);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search)
        && nextProps.history.action !== 'REPLACE') {
      this.props.mountAction(nextProps.match.params);
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  handleTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleFormSectionCollapseToggle = ({ sectionId, isCollapsed }) => {
    this.props.sectionCollapseToggleAction(sectionId, isCollapsed);
  };

  handleSearch = ({ fieldId, fieldSearchValue }) => {
    this.props.dictionarySearchAction(fieldId, fieldSearchValue);
  };

  handleFocus = ({ fieldId }) => {
    this.props.fieldFocusAction(fieldId);
  };

  handleBlur = ({ fieldId }) => {
    this.props.fieldBlurAction(fieldId);
  };

  handleChange = ({ fieldId, fieldValue }) => {
    this.props.changeDataAction(fieldId, fieldValue);
  };

  handleStructureTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleStructureSectionClick = ({ tabId, sectionId }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    setTimeout(() => {
      const fieldId = parentTab.get('sections').find(section => section.get('id') === sectionId).get('firstFieldId');
      smoothScrollTo(
        getScrollTopByDataId(sectionId, DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR),
        () => {
          document.querySelector(`#${fieldId}`) &&
          setTimeout(() => { document.querySelector(`#${fieldId}`).focus(); }, 0);
        }
      );
    }, 0);
  };

  handleStructureErrorClick = ({ tabId, fieldsIds }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    const fieldElementSelector = getFieldElementSelectorByIds(fieldsIds);
    fieldElementSelector && setTimeout(() => { fieldElementSelector.focus(); }, 0);
  };

  formFieldOptionRenderer = (fieldId, option, highlight) => {
    switch (fieldId) {
      case 'receiverBankBIC':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.title}
            describe={option && option.bankName}
            extra={option && option.corrAccount}
            highlight={{
              value: highlight,
              inTitle: option && option.title
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  render() {
    const {
      isDataError,
      isDataLoading,
      isEditable,
      documentTabs,
      documentValues,
      documentData,
      documentStructure,
      documentDictionaries,
      formSections,
      formWarnings,
      formErrors,
      operations,
      operationAction,
    } = this.props;

    const sectionsProps = {
      isEditable,
      documentValues,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formFieldOptionRenderer: this.formFieldOptionRenderer,
      onCollapseToggle: this.handleFormSectionCollapseToggle,
      onFieldFocus: this.handleFocus,
      onFieldBlur: this.handleBlur,
      onFieldChange: this.handleChange,
      onFieldSearch: this.handleSearch,
    };

    return (
      <div className={COMPONENT_STYLE_NAME}>
        <div className={`${COMPONENT_STYLE_NAME}__inner`}>
          {isDataLoading && <Loader centered />}
          {!isDataLoading && !isDataError &&
          <RboDocument>
            <RboDocumentPanel>
              <RboDocumentOperations
                operations={operations}
                operationAction={operationAction}
              />
            </RboDocumentPanel>
            <RboDocumentContent data-id="">
              <RboDocumentMain>
                <RboDocumentHead title={documentValues.get('title')}>
                  <RboDocumentHeadStatus
                    date={documentValues.get('docDate')}
                    statusClassName={getDocStatusClassNameMode(documentValues.get('status'))}
                    status={documentValues.get('status')}
                  />
                  <RboDocumentHeadTabs
                    tabs={documentTabs}
                    onTabClick={this.handleTabClick}
                  />
                </RboDocumentHead>
                <RboDocumentBody>
                  <RboForm initialFocusFieldId="receiverINN">
                    {formSections.getIn(['primary', 'isVisible']) &&
                    <SectionPrimary
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['primary', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['primary', 'isCollapsed'])}
                    />
                    }
                  </RboForm>
                </RboDocumentBody>
              </RboDocumentMain>
              <RboDocumentExtra>
                <RboDocumentExtraSection title="Структура документа">
                  <RboDocumentStructure
                    structure={documentStructure}
                    onTabClick={this.handleStructureTabClick}
                    onSectionClick={this.handleStructureSectionClick}
                    onErrorClick={this.handleStructureErrorClick}
                    isErrorClickable={isEditable}
                  />
                </RboDocumentExtraSection>
              </RboDocumentExtra>
            </RboDocumentContent>
          </RboDocument>
          }
        </div>
      </div>
    );
  }
}

export default CorrespondentContainer;
