import { fromJS } from 'immutable';
import { DOC_STATUSES } from 'constants';
import { getIsDocEditable } from 'utils';
import { ACTIONS } from './constants';

export const initialDocumentTabs = [
  {
    id: 'main',
    title: 'Основные поля',
    isActive: true
  }
];

export const initialFormSections = {
  primary: {
    title: 'Общая информация',
    isVisible: true,
    isCollapsible: false,
    order: 0
  }
};

export const initialDocumentDictionaries = {
  organizations: {
    items: []
  },
  receiverBankBIC: {
    items: [],
    searchValue: '',
    isSearching: false
  }
};

export const initialOperations = [
  {
    id: 'back',
    title: 'Назад',
    icon: 'back',
    disabled: false,
    progress: false
  },
  {
    id: 'save',
    title: 'Сохранить',
    icon: 'save',
    disabled: false,
    progress: false
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false
  }
];

export const initialState = fromJS({
  isMounted: false,
  isDataError: false,
  isDataLoading: true,
  isEditable: false,
  documentTabs: initialDocumentTabs,
  documentData: {},
  documentErrors: [],
  documentDictionaries: initialDocumentDictionaries,
  formSections: initialFormSections,
  activeFormField: null,
  operations: initialOperations
});

export const mount = () => initialState.set('isMounted', true);

export const unmount = () => initialState.set('isMounted', false);

export const getDataRequest = state => state.merge({
  isDataLoading: true
});

export const getDataSuccess = (state, { documentData, documentErrors, documentDictionaries }) => {
  const documentDataImmutable = fromJS(documentData);
  const documentErrorsImmutable = fromJS(documentErrors).get('errors', fromJS([]));
  const documentDictionariesImmutable = fromJS(documentDictionaries);
  const status = documentDataImmutable.get('status', DOC_STATUSES.NEW);
  const isEditable = getIsDocEditable(status);

  const receiverBankBICDictionaryItems = documentDictionariesImmutable.getIn(['receiverBankBIC', 'items']);
  const receiverBankBICDictionarySelectedItem = receiverBankBICDictionaryItems
    .find(item => item.get('bic') === documentDataImmutable.get('receiverBankBIC'));

  return state.mergeDeep({
    isDataLoading: false,
    isEditable,
    documentData: documentDataImmutable.merge({
      status,
      receiverBankCorrAccount: receiverBankBICDictionarySelectedItem
        ? receiverBankBICDictionarySelectedItem.get('corrAccount')
        : null,
      receiverBankName: receiverBankBICDictionarySelectedItem
        ? receiverBankBICDictionarySelectedItem.get('bankName')
        : null,
      receiverBankCity: receiverBankBICDictionarySelectedItem
        ? receiverBankBICDictionarySelectedItem.get('locationName')
        : null,
      receiverBankSettlementType: receiverBankBICDictionarySelectedItem
        ? receiverBankBICDictionarySelectedItem.get('locationType')
        : null
    }),
    documentErrors: documentErrorsImmutable,
    documentDictionaries: documentDictionariesImmutable
  });
};

export const getDataFail = state => state.merge({
  isDataError: true,
  isDataLoading: false
});

export const saveDataSuccess = (state, { recordID, status, allowedSmActions, errors }) => state.merge({
  documentData: state.get('documentData').merge({
    id: recordID,
    status,
    allowedSmActions
  }),
  documentErrors: errors
});

export const saveDataFail = (state, { error }) => state.merge({
  documentErrors: error.name === 'RBO Controls Error' ? error.stack : state.get('documentErrors')
});

export const fieldFocus = (state, { fieldId }) => state.set('activeFormField', fieldId);

export const fieldBlur = (state, { fieldId }) => {
  switch (fieldId) {
    default:
      return state.set('activeFormField', null);
  }
};

export const sectionCollapseToggle = (state, { sectionId, isCollapsed }) =>
  state.setIn(['formSections', sectionId, 'isCollapsed'], isCollapsed);

export const changeTab = (state, { tabId }) => state.merge({
  documentTabs: state.get('documentTabs').map(tab => tab.set('isActive', tab.get('id') === tabId))
});

const changeDataReceiverBankBIC = (state, { bic }) => state.mergeDeep({
  documentData: {
    receiverBankBIC: bic ? bic.get('bic') : null,
    receiverBankCorrAccount: bic ? bic.get('corrAccount') : null,
    receiverBankName: bic ? bic.get('bankName') : null,
    receiverBankCity: bic ? bic.get('locationName') : null,
    receiverBankSettlementType: bic ? bic.get('locationType') : null
  }
});

export const changeData = (state, { fieldId, fieldValue }) => {
  const selectedDocumentDictionaryItems = state.getIn(['documentDictionaries', fieldId, 'items']);
  let selectedDictionaryItem;
  switch (fieldId) {
    case 'receiverBankBIC':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataReceiverBankBIC(state, { bic: selectedDictionaryItem });
    default:
      return state.setIn(['documentData', fieldId], fieldValue);
  }
};

export const dictionarySearchReceiverBankBICRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'receiverBankBIC', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'receiverBankBIC', 'isSearching'], true);

export const dictionarySearchReceiverBankBICSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'receiverBankBIC', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'receiverBankBIC', 'isSearching'], false);

export const dictionarySearchReceiverBankBICFail = state =>
  state.setIn(['documentDictionaries', 'receiverBankBIC', 'isSearching'], false);

export const operationSaveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSaveFinish = state => state.mergeDeep({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationRemoveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationRemoveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  documentErrors: fromJS([]),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationRemoveFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

function CorrespondentContainerReducer(state = initialState, action) {
  const isMounted = state.get('isMounted');

  switch (action.type) {
    case ACTIONS.CORRESPONDENT_CONTAINER_MOUNT:
      return mount();

    case ACTIONS.CORRESPONDENT_CONTAINER_UNMOUNT:
      return unmount();

    case ACTIONS.CORRESPONDENT_CONTAINER_GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.CORRESPONDENT_CONTAINER_GET_DATA_SUCCESS:
      return isMounted ? getDataSuccess(state, action.payload) : state;

    case ACTIONS.CORRESPONDENT_CONTAINER_GET_DATA_FAIL:
      return isMounted ? getDataFail(state) : state;

    case ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_SUCCESS:
      return saveDataSuccess(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_FAIL:
      return saveDataFail(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_FIELD_FOCUS:
      return fieldFocus(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_FIELD_BLUR:
      return fieldBlur(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_SECTION_COLLAPSE_TOGGLE:
      return sectionCollapseToggle(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_CHANGE_DATA:
      return changeData(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST:
      return dictionarySearchReceiverBankBICRequest(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_SUCCESS:
      return isMounted ? dictionarySearchReceiverBankBICSuccess(state, action.payload) : state;

    case ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_FAIL:
      return isMounted ? dictionarySearchReceiverBankBICFail(state) : state;

    case ACTIONS.CORRESPONDENT_CONTAINER_CHANGE_TAB:
      return changeTab(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_REQUEST:
      return operationSaveRequest(state);

    case ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_SUCCESS:
    case ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_FAIL:
      return operationSaveFinish(state);

    case ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_REQUEST:
      return operationRemoveRequest(state);

    case ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationRemoveSuccess(state, action.payload);

    case ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_CANCEL:
    case ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationRemoveFail(state);

    default:
      return state;
  }
}

export default CorrespondentContainerReducer;
