import { call, put, select, takeLatest } from 'redux-saga/effects';
import { goBack, replace } from 'react-router-redux';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import {
  getCorrespondentData as dalGetData,
  getCorrespondentErrors as dalGetErrors,
  saveCorrespondentData as dalSaveData,
  removeCorrespondent as dalOperationRemove,
} from 'dal/correspondents/sagas';
import {
  getBIC as dalGetBIC,
} from 'dal/dictionaries/sagas';
import { DOC_STATUSES } from 'constants';
import { getIsDocNeedGetErrors } from 'utils';
import { ACTIONS, LOCAL_REDUCER, LOCAL_ROUTER_ALIASES } from './constants';

export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
export const docIdSelector = state => state.getIn([LOCAL_REDUCER, 'documentData', 'id']);

export function* successNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'success',
    title: title || 'Операция выполнена успешно',
    message: message || ''
  }));
}

export function* warningNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'warning',
    title: title || '',
    message: message || ''
  }));
}

export function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

export function* confirmNotification(params) {
  const { level, title, message, success, cancel, autoDismiss } = params;
  yield put(dalNotificationAction({
    type: 'confirm',
    level: level || 'success',
    title: title || '',
    message: message || '',
    autoDismiss: typeof autoDismiss === 'number' ? autoDismiss : 20,
    success,
    cancel
  }));
}

export function* refreshData() {
  const id = yield select(docIdSelector);
  try {
    return yield call(dalGetData, { payload: { id } });
  } catch (error) {
    throw error;
  }
}

export function* mountSaga(action) {
  const { payload: { params: { id } } } = action;
  yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_GET_DATA_REQUEST, payload: { id } });
}

export function* getDataSaga(action) {
  const { payload: { id } } = action;
  try {
    const organizations = yield select(organizationsSelector);
    const documentData = id === 'new'
      ? { status: DOC_STATUSES.NEW }
      : yield call(dalGetData, { payload: { id } });
    const receiverBankBIC = yield call(dalGetBIC, { payload: { code: documentData.receiverBankBIC || '' } });

    documentData.status = documentData.status || (documentData.signed ? DOC_STATUSES.CERTIFIED : DOC_STATUSES.CREATED);

    const documentErrors = (getIsDocNeedGetErrors(documentData.status))
      ? yield call(dalGetErrors, { payload: documentData })
      : {};

    yield put({
      type: ACTIONS.CORRESPONDENT_CONTAINER_GET_DATA_SUCCESS,
      payload: {
        documentData,
        documentErrors,
        documentDictionaries: {
          organizations: { items: organizations },
          receiverBankBIC: { items: receiverBankBIC }
        }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_GET_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Контрагент не найден.',
      message: 'Неверно указан идентификатор контрагента.'
    });
  }
}

export function* saveDataSaga(action) {
  const { channels } = action;
  const documentData = yield select(documentDataSelector);
  const allowedSave = documentData.getIn(['allowedSmActions', 'save']);
  const status = documentData.get('status');
  if (status === DOC_STATUSES.NEW || allowedSave) {
    try {
      const result = yield call(dalSaveData, { payload: documentData.toJS() });
      if (status === DOC_STATUSES.NEW) yield put(replace(LOCAL_ROUTER_ALIASES.CORRESPONDENT.replace('{:id}', result.recordID)));
      yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_SUCCESS, payload: result, channels });
    } catch (error) {
      yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_FAIL, payload: { error }, channels });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'doNotAllowed' } });
  }
}

export function* saveDataSuccessSaga(action) {
  const { payload: { errors }, channels } = action;
  if (errors && errors.length) {
    if (errors.filter(error => error.level === '2').length) {
      yield put({ type: channels.success, payload: { status: 'haveErrors' } });
    } else if (errors.filter(error => error.level === '1').length) {
      yield put({ type: channels.success, payload: { status: 'haveWarnings' } });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'success' } });
  }
}

export function* saveDataFailSaga(action) {
  const { payload: { error }, channels } = action;
  yield put({ type: channels.fail });
  yield call(errorNotification, error.name === 'RBO Controls Error'
    ? {
      title: 'Контрагент не может быть сохранен.',
      message: 'Для сохранения контрагента требуется исправить ошибки. ' +
      'При наличии блокирующих ошибок контрагент не может быть сохранен.'
    }
    : {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно сохранить контрагента'
    }
  );
}

export function* dictionarySearchSaga(action) {
  const { payload: { dictionaryId, dictionarySearchValue } } = action;
  switch (dictionaryId) {
    case 'receiverBankBIC':
      yield put({
        type: ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    default:
      break;
  }
}

export function* dictionarySearchReceiverBankBICSaga(action) {
  const { payload: { dictionarySearchValue, updateDataFlag } } = action;
  try {
    const result = yield call(dalGetBIC, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_SUCCESS,
      payload: result
    });
    if (updateDataFlag) {
      const selected = result.find(item => item.bic === dictionarySearchValue);
      yield put({
        type: ACTIONS.CORRESPONDENT_CONTAINER_CHANGE_DATA,
        payload: { fieldId: 'receiverBankBIC', fieldValue: selected ? selected.id : '' }
      });
    }
  } catch (error) {
    yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник БИК'
    });
  }
}

export function* operationSaga(action) {
  const { payload: { operationId } } = action;
  switch (operationId) {
    case 'back':
      yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_BACK });
      break;
    case 'save':
      yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_REQUEST });
      break;
    case 'remove':
      yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_REQUEST });
      break;
    default:
      break;
  }
}

export function* operationBackSaga() {
  const location = yield select(routingLocationSelector);
  const prevLocation = location.get('prevLocation');
  if (prevLocation && prevLocation.get('pathname') !== location.get('pathname')) yield put(goBack());
  else yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
}

export function* operationSaveSaga() {
  yield put({
    type: ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_SUCCESS,
      fail: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_FAIL
    }
  });
}

export function* operationSaveSuccessSaga(action) {
  const { payload: { status } } = action;
  switch (status) {
    case 'haveErrors':
      yield call(errorNotification, {
        title: 'Контрагент сохранен с ошибками.'
      });
      break;
    case 'haveWarnings':
      yield call(warningNotification, {
        title: 'Контрагент сохранен с предупреждениями.',
        message: 'Обратите внимание на предупреждения из банка.'
      });
      break;
    default:
      yield call(successNotification, {
        title: 'Контрагент успешно сохранен.'
      });
  }
}

export function* operationRemoveSaga() {
  yield call(confirmNotification, {
    level: 'error',
    title: 'Удаление контрагента',
    message: 'Вы действительно хотите удалить контрагента?',
    autoDismiss: 0,
    success: {
      label: 'Подтвердить',
      action: { type: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_CONFIRM }
    },
    cancel: {
      label: 'Отмена',
      action: { type: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_CANCEL }
    }
  });
}

export function* operationRemoveConfirmSaga() {
  const id = yield select(docIdSelector);
  try {
    yield call(dalOperationRemove, { payload: { id } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: { documentData: result }
    });
    yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
    yield call(successNotification, { title: 'Контрагент успешно удален.' });
  } catch (error) {
    yield put({ type: ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно удалить контрагента'
    });
  }
}

export default function* saga() {
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_MOUNT, mountSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_GET_DATA_REQUEST, getDataSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_REQUEST, saveDataSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_SUCCESS, saveDataSuccessSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_SAVE_DATA_FAIL, saveDataFailSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH, dictionarySearchSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST, dictionarySearchReceiverBankBICSaga);

  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_OPERATION, operationSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_BACK, operationBackSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_REQUEST, operationSaveSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemoveSaga);
  yield takeLatest(ACTIONS.CORRESPONDENT_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
}
