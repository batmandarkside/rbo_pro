import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldAccount,
  RboFormFieldDigital,
  RboFormFieldSearch,
  RboFormFieldText,
  RboFormFieldTextarea,
  RboFormFieldValue
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

const MessageSectionPrimary = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange
  } = props;

  return (
    <RboFormSection id="primary">
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="1/2">
                {isEditable &&
                <RboFormFieldDigital
                  id="receiverINN"
                  label="ИНН/КИО"
                  value={documentData.get('receiverINN')}
                  maxLength={12}
                  isWarning={formWarnings.get('receiverINN')}
                  isError={formErrors.get('receiverINN')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="receiverINN"
                  label="ИНН/КИО"
                  value={documentValues.get('receiverINN')}
                  isWarning={formWarnings.get('receiverINN')}
                  isError={formErrors.get('receiverINN')}
                />
                }
              </RboFormCell>
              <RboFormCell size="1/2">
                {isEditable &&
                <RboFormFieldText
                  id="receiverKPP"
                  label="КПП"
                  value={documentData.get('receiverKPP')}
                  maxLength={9}
                  isWarning={formWarnings.get('receiverKPP')}
                  isError={formErrors.get('receiverKPP')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="receiverKPP"
                  label="КПП"
                  value={documentValues.get('receiverKPP')}
                  isWarning={formWarnings.get('receiverKPP')}
                  isError={formErrors.get('receiverKPP')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock />
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="receiverName"
                  label="Наименование получателя"
                  value={documentData.get('receiverName')}
                  maxLength={160}
                  isWarning={formWarnings.get('receiverName')}
                  isError={formErrors.get('receiverName')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="receiverName"
                  label="Наименование получателя"
                  value={documentValues.get('receiverName')}
                  isWarning={formWarnings.get('receiverName')}
                  isError={formErrors.get('receiverName')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldAccount
                  id="account"
                  label="Счет №"
                  value={documentData.get('account')}
                  isWarning={formWarnings.get('account')}
                  isError={formErrors.get('account')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="account"
                  label="Счет №"
                  value={documentValues.get('account')}
                  isWarning={formWarnings.get('account')}
                  isError={formErrors.get('account')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id="receiverBankName"
                  label="Банк получателя"
                  value={documentValues.get('receiverBankName')}
                  isWarning={formWarnings.get('receiverBankName')}
                  isError={formErrors.get('receiverBankName')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="1/3">
                {isEditable &&
                <RboFormFieldSearch
                  id="receiverBankBIC"
                  label="БИК"
                  value={documentData.get('receiverBankBIC')}
                  searchValue={documentDictionaries.getIn(['receiverBankBIC', 'searchValue'])}
                  options={documentDictionaries.getIn(['receiverBankBIC', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupStyle={{ width: 362 }}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('receiverBankBIC')}
                  isError={formErrors.get('receiverBankBIC')}
                  isSearching={documentDictionaries.getIn(['receiverBankBIC', 'isSearching'])}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="receiverBankBIC"
                  label="БИК"
                  value={documentValues.get('receiverBankBIC')}
                  isWarning={formWarnings.get('receiverBankBIC')}
                  isError={formErrors.get('receiverBankBIC')}
                />
                }
              </RboFormCell>
              <RboFormCell size="2/3">
                <RboFormFieldValue
                  id="receiverBankCorrAccount"
                  label="Кор. счет"
                  value={documentValues.get('receiverBankCorrAccount')}
                  isWarning={formWarnings.get('receiverBankCorrAccount')}
                  isError={formErrors.get('receiverBankCorrAccount')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="paymentPurpose"
                  label="Назначение платежа"
                  value={documentData.get('paymentPurpose')}
                  isWarning={formWarnings.get('paymentPurpose')}
                  isError={formErrors.get('paymentPurpose')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="paymentPurpose"
                  value={documentValues.get('paymentPurpose')}
                  isWarning={formWarnings.get('paymentPurpose')}
                  isError={formErrors.get('paymentPurpose')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="comment"
                  label="Комментарий"
                  value={documentData.get('comment')}
                  isWarning={formWarnings.get('comment')}
                  isError={formErrors.get('comment')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="comment"
                  value={documentValues.get('comment')}
                  isWarning={formWarnings.get('comment')}
                  isError={formErrors.get('comment')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

MessageSectionPrimary.propTypes = {
  isEditable: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default MessageSectionPrimary;
