import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import { DOC_STATUSES } from 'constants';
import {
  getFormattedAccNum,
  getDocFormErrors,
  getMappingDocSectionsAndErrors
} from 'utils';
import { LOCAL_REDUCER } from './constants';
import {
  documentFieldsMap,
  documentSectionsFieldsMap,
  documentErrorsMap
} from './utils';

const isDataErrorSelector = state => state.getIn([LOCAL_REDUCER, 'isDataError']);
const isDataLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isDataLoading']);
const isEditableSelector = state => state.getIn([LOCAL_REDUCER, 'isEditable']);
const documentTabsSelector = state => state.getIn([LOCAL_REDUCER, 'documentTabs']);
const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
const documentErrorsSelector = state => state.getIn([LOCAL_REDUCER, 'documentErrors']);
const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
const formSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'formSections']);
const activeFormFieldSelector = state => state.getIn([LOCAL_REDUCER, 'activeFormField']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);

const getDictionaryId = (value, paramName, dictionary, idName) => {
  const selectedItem = dictionary.get('items').find(item => item.get(paramName) === value);
  return selectedItem ? selectedItem.get(idName) : null;
};

const documentValuesCreatedSelector = createSelector(
  documentDataSelector,
  documentData => fromJS({
    title: 'Контрагент',
    status: documentData.get('status', ''),

    receiverINN: documentData.get('receiverINN', ''),
    receiverKPP: documentData.get('receiverKPP', ''),
    receiverName: documentData.get('receiverName', ''),
    account: getFormattedAccNum(documentData.get('account', '')),
    receiverBankBIC: documentData.get('receiverBankBIC', ''),
    receiverBankCorrAccount: getFormattedAccNum(documentData.get('receiverBankCorrAccount', '')),
    receiverBankName: documentData.get('receiverBankName')
      ? `${documentData.get('receiverBankName')} ${documentData.get('receiverBankSettlementType') ?
        `${documentData.get('receiverBankSettlementType')}.` : ''} ${documentData.get('receiverBankCity', '')}`
      : '',
    paymentPurpose: documentData.get('paymentPurpose', ''),
    comment: documentData.get('comment', '')
  })
);

const documentDataCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => fromJS({
    account: documentData.get('account'),
    receiverINN: documentData.get('receiverINN'),
    receiverKPP: documentData.get('receiverKPP'),
    receiverName: documentData.get('receiverName'),
    receiverBankBIC: getDictionaryId(
      documentData.get('receiverBankBIC'), 'bic',
      documentDictionaries.get('receiverBankBIC'), 'id'
    ),
    paymentPurpose: documentData.get('paymentPurpose'),
    comment: documentData.get('comment')
  })
);

const documentErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  activeFormFieldSelector,
  (documentErrors, activeFormField) => documentErrors
    .map(error => error.merge({
      fieldId: documentErrorsMap[error.get('id')] && documentErrorsMap[error.get('id')][0],
      fieldsIds: documentErrorsMap[error.get('id')],
      isActive: documentErrorsMap[error.get('id')] &&
        !!documentErrorsMap[error.get('id')].find(i => i === activeFormField)
    }))
    .filter(error => !!error.get('fieldId'))
    .sort((a, b) => documentFieldsMap.indexOf(a.get('fieldId')) - documentFieldsMap.indexOf(b.get('fieldId')))
);

const documentStructureCreatedSelector = createSelector(
  documentTabsSelector,
  formSectionsSelector,
  documentErrorsCreatedSelector,
  activeFormFieldSelector,
  (documentTabs, formSections, documentErrors, activeFormField) => documentTabs.map((tab) => {
    switch (tab.get('id')) {
      case 'main':
        return tab.set('sections',
          getMappingDocSectionsAndErrors(formSections, documentSectionsFieldsMap, documentErrors, activeFormField));
      default:
        return tab;
    }
  })
);

const documentDictionariesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => documentDictionaries.map((dictionary, key) => {
    const items = dictionary.get('items');
    switch (key) {
      case 'receiverBankBIC':
        return dictionary.merge({
          items: items.map(item => fromJS({
            value: item.get('id'),
            title: item.get('bic'),
            bankName: item.get('bankName'),
            corrAccount: getFormattedAccNum(item.get('corrAccount'))
          }))
        });
      default:
        return dictionary;
    }
  })
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['1'])
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['2', '3'])
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  documentDataSelector,
  (operations, documentData) => {
    const status = documentData.get('status');
    const allowedSmActions = documentData.get('allowedSmActions', fromJS({}));
    return operations.filter((operation) => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'back':
          return true;
        case 'save':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save');
        case 'signAndSend':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save') || allowedSmActions.get('sign');
        case 'sign':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('sign');
        case 'unarchive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('fromArchive');
        case 'repeat':
          return status !== DOC_STATUSES.NEW;
        case 'print':
          return status !== DOC_STATUSES.NEW;
        case 'remove':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('delete');
        case 'send':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('send');
        case 'history':
          return status !== DOC_STATUSES.NEW;
        case 'archive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('archive');
        case 'checkSign':
          return !![
            DOC_STATUSES.DELIVERED,
            DOC_STATUSES.PROCESSED,
            DOC_STATUSES.EXECUTED,
            DOC_STATUSES.DECLINED,
            DOC_STATUSES.RECALLED,
            DOC_STATUSES.INVALID_PROPS,
            DOC_STATUSES.SIGNED,
            DOC_STATUSES.ACCEPTED,
            DOC_STATUSES.PARTLY_SIGNED,
            DOC_STATUSES.INVALID_SIGN
          ].find(v => v === status);
        case 'saveAsTemplate':
          return status !== DOC_STATUSES.NEW;
        default:
          return false;
      }
    });
  }
);

const mapStateToProps = state => ({
  isDataError: isDataErrorSelector(state),
  isDataLoading: isDataLoadingSelector(state),
  isEditable: isEditableSelector(state),
  documentTabs: documentTabsSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentValues: documentValuesCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentDictionaries: documentDictionariesCreatedSelector(state),
  formSections: formSectionsSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  operations: operationsCreatedSelector(state)
});

export default mapStateToProps;
