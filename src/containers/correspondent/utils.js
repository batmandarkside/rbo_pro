export const documentFieldsMap = [
  'receiverINN',
  'receiverKPP',
  'receiverName',
  'account',
  'receiverBankName',
  'receiverBankBIC',
  'receiverBankCorrAccount',
  'paymentPurpose',
  'comment'
];

export const documentSectionsFieldsMap = {
  primary: [
    'receiverINN',
    'receiverKPP',
    'receiverName',
    'account',
    'receiverBankName',
    'receiverBankBIC',
    'receiverBankCorrAccount',
    'paymentPurpose',
    'comment'
  ]
};

export const documentErrorsMap = {
  '1.1.1': ['receiverName'],
  '1.1.2': ['receiverName'],
  '1.2.1': ['receiverKPP'],
  '1.3.1': ['receiverINN'],
  '1.3.2': ['receiverINN'],
  '1.3.3': ['receiverKPP'],
  '1.4.1': ['receiverBankBIC'],
  '1.4.2': ['account'],
  '1.4.3': ['receiverBankBIC'],
  '1.4.4': ['receiverBankBIC'],
  '1.4.5': ['account'],
  '1.4.6': ['account'],
  '2.1'  : ['account', 'receiverBankBIC', 'receiverINN'],
  '3'    : ['account']
};
