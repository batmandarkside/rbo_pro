import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  CORRESPONDENTS_CONTAINER_GET_SETTINGS_REQUEST: null,
  CORRESPONDENTS_CONTAINER_GET_SETTINGS_SUCCESS: null,
  CORRESPONDENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST: null,
  CORRESPONDENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS: null,
  CORRESPONDENTS_CONTAINER_CHANGE_PAGE: null,
  CORRESPONDENTS_CONTAINER_RELOAD_PAGES: null,
  CORRESPONDENTS_CONTAINER_CHANGE_FILTERS: null,
  CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_REQUEST: null,
  CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_SUCCESS: null,
  CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_FAIL: null,
  CORRESPONDENTS_CONTAINER_GET_LIST_REQUEST: null,
  CORRESPONDENTS_CONTAINER_GET_LIST_SUCCESS: null,
  CORRESPONDENTS_CONTAINER_GET_LIST_FAIL: null,
  CORRESPONDENTS_CONTAINER_UNMOUNT_REQUEST: null,
  CORRESPONDENTS_CONTAINER_RESIZE_COLUMNS: null,
  CORRESPONDENTS_CONTAINER_CHANGE_COLUMN: null,
  CORRESPONDENTS_CONTAINER_CHANGE_SORTING: null,
  CORRESPONDENTS_CONTAINER_CHANGE_PAGINATION: null,
  CORRESPONDENTS_CONTAINER_CHANGE_SELECT: null,
  CORRESPONDENTS_CONTAINER_ROUTE_TO_ITEM: null,
  CORRESPONDENTS_CONTAINER_NEW_PAYMENT: null,

  CORRESPONDENTS_CONTAINER_OPERATION: null,

  CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_REQUEST: null,
  CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_SUCCESS: null,
  CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_FAIL: null,

  CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_CONFIRM_SHOW: null,
  CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_CONFIRM_CANCEL: null,
  CORRESPONDENTS_CONTAINER_OPERATION_EDIT: null,

  CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST: null,
  CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS: null,
  CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL: null
});

export const LOCAL_REDUCER = 'CorrespondentsContainerReducer';

export const LOCAL_ROUTER_ALIAS = '/correspondents';

export const SETTINGS_ALIAS = 'correspondentsContainer';

export const LOCATORS = {
  CONTAINER: 'correspondentsContainer',
  SPREADSHEET: 'correspondentsContainerSpreadsheet',

  TOOLBAR: 'correspondentsContainerSpreadsheetToolbar',
  TOOLBAR_BUTTONS: 'correspondentsContainerSpreadsheetToolbarButtons',
  TOOLBAR_BUTTON_REFRESH_LIST: 'correspondentsContainerSpreadsheetToolbarButtonRefreshList',

  FILTERS: 'correspondentsContainerSpreadsheetFilters',
  FILTERS_SELECT: 'correspondentsContainerSpreadsheetFiltersSelect',
  FILTERS_ADD: 'correspondentsContainerSpreadsheetFiltersAdd',
  FILTERS_REMOVE: 'correspondentsContainerSpreadsheetFiltersRemove',
  FILTERS_OPERATIONS: 'correspondentsContainerSpreadsheetFiltersOperations',
  FILTERS_OPERATION_SAVE: 'correspondentsContainerSpreadsheetFiltersOperationSave',
  FILTERS_OPERATION_SAVE_AS: 'correspondentsContainerSpreadsheetFiltersOperationSaveAs',
  FILTERS_OPERATION_SAVE_AS_POPUP: 'correspondentsContainerSpreadsheetFiltersOperationSaveAsPopup',
  FILTERS_OPERATION_CLEAR: 'correspondentsContainerSpreadsheetFiltersOperationClear',
  FILTERS_CONDITIONS: 'correspondentsContainerSpreadsheetFiltersConditions',
  FILTERS_CONDITION: 'correspondentsContainerSpreadsheetFiltersCondition',
  FILTERS_CONDITION_POPUP: 'correspondentsContainerSpreadsheetFiltersConditionPopup',
  FILTERS_CONDITION_REMOVE: 'correspondentsContainerSpreadsheetFiltersConditionRemove',
  FILTERS_ADD_CONDITION: 'correspondentsContainerSpreadsheetFiltersAddCondition',
  FILTERS_ADD_CONDITION_POPUP: 'correspondentsContainerSpreadsheetFiltersAddConditionPopup',
  FILTERS_CLOSE: 'correspondentsContainerSpreadsheetFiltersClose',

  TABLE: 'correspondentsContainerSpreadsheetTable',
  TABLE_HEAD: 'correspondentsContainerSpreadsheetTableHead',
  TABLE_BODY: 'correspondentsContainerSpreadsheetTableBody',
  TABLE_SETTINGS: 'correspondentsContainerSpreadsheetTableSettings',
  TABLE_SETTINGS_TOGGLE: 'correspondentsContainerSpreadsheetTableSettingsToggle',
  TABLE_SETTINGS_TOGGLE_BUTTON: 'correspondentsContainerSpreadsheetTableSettingsToggleButton',
  TABLE_SETTINGS_POPUP: 'correspondentsContainerSpreadsheetTableSettingsPopup',

  PAGINATION: 'correspondentsContainerSpreadsheetPagination'
};

export const COLUMNS = {
  NAME: 'receiverName',
  INN: 'receiverINN',
  KPP: 'receiverKPP',
  SIGNED: 'signed',
  ACCOUNT: 'account',
  BANK_NAME: 'receiverBankName',
  BIC: 'receiverBankBIC',
  PURPOSE: 'paymentPurpose',
  COMMENT: 'comment'
};

export const FILTERS = {
  NAME: 'receiverName',
  INN: 'receiverINN',
  KPP: 'receiverKPP',
  SIGNED: 'signed',
  ACCOUNT: 'receiverAccount',
  BANK_NAME: 'receiverBankName',
  BIC: 'receiverBankBIC',
  PURPOSE: 'paymentPurpose',
  COMMENT: 'comment'
};

export const FILTER_VALUES = {
  SIGNED: ' Заверен',
  NOT_SIGNED: 'Не заверен'
};

export const VALIDATE_SETTINGS = settings => !!settings &&
  !!settings.visible &&
  !!settings.sorting &&
  !!settings.pagination &&
  !!settings.width &&
  !!settings.width[COLUMNS.NAME] &&
  !!settings.width[COLUMNS.INN] &&
  !!settings.width[COLUMNS.KPP] &&
  !!settings.width[COLUMNS.SIGNED] &&
  !!settings.width[COLUMNS.ACCOUNT] &&
  !!settings.width[COLUMNS.BANK_NAME] &&
  !!settings.width[COLUMNS.BIC] &&
  !!settings.width[COLUMNS.PURPOSE] &&
  !!settings.width[COLUMNS.COMMENT];
