import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Icon from 'components/ui-components/icon';
import Spreadsheet, {
  Toolbar,
  Filters,
  Table,
  Pagination,
  Operations
} from 'components/ui-components/spreadsheet';
import { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import { LOCATORS, COLUMNS, FILTERS } from './constants';
import './style.css';

class CorrespondentsContainer extends Component {

  static propTypes = {
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,

    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    location: PropTypes.object.isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,

    getSettingsAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
    changeSelectAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    routeToItemAction: PropTypes.func.isRequired,
    newCorrespondentAction: PropTypes.func.isRequired,
    selectedItems: PropTypes.instanceOf(List).isRequired,
    operations: PropTypes.instanceOf(List),
    getDictionaryForFiltersAction: PropTypes.func.isRequired
  };

  componentWillMount() {
    this.props.getSettingsAction();
    this.props.getQueryParamsAction();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getQueryParamsAction();
    } else if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  filterDictionaryFormFieldOptionRenderer = ({ conditionId, option, highlight }) => {
    switch (conditionId) {
      case FILTERS.INN:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.receiverINN}
            describe={option && option.receiverName}
            highlight={{
              value: highlight,
              inTitle: option && option.receiverINN
            }}
          />
        );
      case FILTERS.KPP:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.receiverKPP}
            describe={option && option.receiverName}
            highlight={{
              value: highlight,
              inTitle: option && option.receiverKPP
            }}
          />
        );
      case FILTERS.ACCOUNT:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.account}
            describe={option && option.receiverName}
            highlight={{
              value: highlight,
              inTitle: option && option.account
            }}
          />
        );
      case FILTERS.BIC:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.bic}
            describe={option && option.bankName}
            highlight={{
              value: highlight,
              inTitle: option && option.bic
            }}
          />
        );
      case FILTERS.BANK_NAME:
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.bankName}
            describe={option && option.bic}
            highlight={{
              value: highlight,
              inTitle: option && option.bankName
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  renderItemBoolElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>
        <Icon
          type={value ? 'bool-true' : 'bool-false'}
          size="16"
        />
      </div>
    );
  };

  render() {
    const {
      isListLoading,
      columns,
      filtersConditions,
      filters,
      list,
      pages,
      paginationOptions,
      getListAction,
      changeColumnsAction,
      changeFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSortingAction,
      resizeColumnsAction,
      changeSelectAction,
      selectedItems,
      operations,
      operationAction,
      routeToItemAction,
      newCorrespondentAction,
      getDictionaryForFiltersAction
    } = this.props;

    return (
      <div className="b-correspondents-container" data-loc={LOCATORS.CONTAINER}>
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                },
                tabs: LOCATORS.TOOLBAR_TABS
              }}
              title="Контрагенты"
              buttons={[
                {
                  id: 'refreshList',
                  isProgress: isListLoading,
                  type: 'refresh',
                  title: 'Обновить',
                  onClick: getListAction
                },
                {
                  id: 'newCorrespondent',
                  type: 'simple',
                  title: 'Создать',
                  onClick: newCorrespondentAction
                }
              ]}
            />
            {selectedItems.size ?
              <Operations
                dataLoc={LOCATORS.OPERATIONS}
                operations={operations}
                operationAction={operationAction}
                selectedItems={selectedItems}
              />
              :
              <Filters
                dataLocs={{
                  main: LOCATORS.FILTERS,
                  select: LOCATORS.FILTERS_SELECT,
                  add: LOCATORS.FILTERS_ADD,
                  remove: LOCATORS.FILTERS_REMOVE,
                  operations: LOCATORS.FILTERS_OPERATIONS,
                  operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                  operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                  operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                  operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                  conditions: LOCATORS.FILTERS_CONDITIONS,
                  condition: LOCATORS.FILTERS_CONDITION,
                  conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                  conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                  addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                  addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                  close: LOCATORS.FILTERS_CLOSE
                }}
                conditions={filtersConditions}
                filters={filters}
                changeFiltersAction={changeFiltersAction}
                dictionaryFormFieldOptionRenderer={this.filterDictionaryFormFieldOptionRenderer}
                onConditionDictionaryFormInputChange={getDictionaryForFiltersAction}
              />
            }
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                [COLUMNS.SIGNED]: this.renderItemBoolElement
              }}
              settings={{
                iconTitle: 'Настройка таблицы контрагентов',
                changeColumnsAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              select={{
                selectItemTitle: 'Выделить контрагента',
                selectGroupTitle: 'Выделить группу контрагентов',
                selectAllTitle: 'Выделить всех контрагентов',
                changeSelectAction
              }}
              sorting={{
                changeSortingAction
              }}
              resize={{
                resizeColumnsAction
              }}
              onItemClick={routeToItemAction}
              operations={operations}
              operationAction={operationAction}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {selectedItems.size ?
              <div className="b-selected-counter">
                {`Выбрано: ${selectedItems.size}`}
              </div>
              : ''
            }
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
      </div>
    );
  }
}

export default CorrespondentsContainer;
