import { fromJS } from 'immutable';
import { ACTIONS, COLUMNS, FILTERS, FILTER_VALUES } from './constants';

export const initialSections = [
  {
    id: 'correspondents',
    title: 'Контрагенты',
    route: '/correspondents',
    selected: true
  }
];

export const initialFiltersConditions = [
  {
    id: FILTERS.NAME,
    title: 'Наименование',
    type: 'string'
  },
  {
    id: FILTERS.INN,
    title: 'ИНН',
    type: 'search',
    inputType: 'Digital',
    maxLength: 12,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    }
  },
  {
    id: FILTERS.KPP,
    title: 'КПП',
    type: 'search',
    inputType: 'Text',
    maxLength: 9,
    values: {
      discrete: true,
      labelKey: 'receiverKPP',
      items: []
    }
  },
  {
    id: FILTERS.SIGNED,
    title: 'Заверен',
    type: 'select',
    values: [
      FILTER_VALUES.SIGNED,
      FILTER_VALUES.NOT_SIGNED
    ],
  },
  {
    id: FILTERS.ACCOUNT,
    title: 'Счет',
    type: 'search',
    inputType: 'Account',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    }
  },
  {
    id: FILTERS.BIC,
    title: 'БИК',
    type: 'search',
    inputType: 'Text',
    maxLength: 9,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    }
  },
  {
    id: FILTERS.BANK_NAME,
    title: 'Наименование банка',
    type: 'search',
    inputType: 'Text',
    maxLength: 255,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    }
  },
  {
    id: FILTERS.PURPOSE,
    title: 'Назначение платежа',
    type: 'string'
  },
  {
    id: FILTERS.COMMENT,
    title: 'Комментарий',
    type: 'string'
  }
];

export const initialColumns = [
  {
    id: COLUMNS.NAME,
    title: 'Наименование',
    canGrouped: false
  },
  {
    id: COLUMNS.INN,
    title: 'ИНН',
    canGrouped: false
  },
  {
    id: COLUMNS.KPP,
    title: 'КПП',
    canGrouped: false
  },
  {
    id: COLUMNS.SIGNED,
    title: 'Заверен',
    canGrouped: false
  },
  {
    id: COLUMNS.ACCOUNT,
    title: 'Счет',
    canGrouped: false
  },
  {
    id: COLUMNS.BIC,
    title: 'БИК',
    canGrouped: false
  },
  {
    id: COLUMNS.BANK_NAME,
    title: 'Наименование банка',
    canGrouped: false
  },
  {
    id: COLUMNS.PURPOSE,
    title: 'Назначение платежа',
    canGrouped: false
  },
  {
    id: COLUMNS.COMMENT,
    title: 'Комментарий',
    canGrouped: false
  }
];

export const initialSettings = {
  visible: [
    COLUMNS.NAME,
    COLUMNS.INN,
    COLUMNS.KPP,
    COLUMNS.SIGNED,
    COLUMNS.ACCOUNT,
    COLUMNS.BANK_NAME,
    COLUMNS.BIC,
    COLUMNS.PURPOSE,
    COLUMNS.COMMENT
  ],
  sorting: { id: COLUMNS.NAME, direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    [COLUMNS.NAME]: 230,
    [COLUMNS.INN]: 110,
    [COLUMNS.KPP]: 230,
    [COLUMNS.SIGNED]: 150,
    [COLUMNS.ACCOUNT]: 180,
    [COLUMNS.BANK_NAME]: 180,
    [COLUMNS.BIC]: 180,
    [COLUMNS.PURPOSE]: 200,
    [COLUMNS.COMMENT]: 200
  }
};

export const initialOperations = [
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'edit',
    title: 'Редактировать',
    icon: 'edit',
    disabled: false,
    progress: false,
    grouped: false
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialState = fromJS({
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: initialFiltersConditions,
  filters: [],
  list: [],
  selectedItems: [],
  operations: initialOperations
});

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters;
  if (filter) {
    if (state.get('filters').find(item => item.get('id') === filter.id)) {
      filters = state.get('filters').map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = state.get('filters').push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  } else {
    filters = state.get('filters').map(item => item.set('selected', false));
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], true)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false,
  selectedItems: fromJS([])
});

export const mapItemToColumn = item => ({
  [COLUMNS.ACCOUNT]: item.account,
  [COLUMNS.NAME]: item.receiverName,
  [COLUMNS.INN]: item.receiverINN,
  [COLUMNS.KPP]: item.receiverKPP,
  [COLUMNS.SIGNED]: item.signed,
  [COLUMNS.BANK_NAME]: item.receiverBankName,
  [COLUMNS.BIC]: item.receiverBankBIC,
  [COLUMNS.PURPOSE]: item.paymentPurpose,
  [COLUMNS.COMMENT]: item.comment,
  id: item.id,
  allowedSmActions: item.allowedSmActions
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list.map(mapItemToColumn));
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([])
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

export const operationFinish = (state, operationId, isListUpdateRequired) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

function CorrespondentsContainerReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ACTIONS.CORRESPONDENTS_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.CORRESPONDENTS_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.CORRESPONDENTS_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_REQUEST:
      return operationRequest(state, 'remove');

    case ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_SUCCESS:
    case ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationFinish(state, 'remove', true);

    case ACTIONS.CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, payload);

    case ACTIONS.CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    default:
      return state;
  }
}

export default CorrespondentsContainerReducer;
