import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction
} from 'dal/notification/actions';
import {
  getCorrespondents as dalGetList,
  removeCorrespondent as dalRemoveCorrespondent
} from 'dal/correspondents/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { getBIC as dalGetBIC } from 'dal/dictionaries/sagas';
import {
  ACTIONS,
  LOCAL_REDUCER,
  SETTINGS_ALIAS,
  VALIDATE_SETTINGS,
  LOCAL_ROUTER_ALIAS,
  FILTER_VALUES,
  FILTERS
} from './constants';

const getParamsForDalOperation = ({ settings, pages, filters }) => {
  const offset = pages.get('selected') * settings.get('pagination');
  const offsetStep = settings.get('pagination') + 1;
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const selectedFilter = filters.find(filter => filter.get('selected'));
  const conditions = selectedFilter && selectedFilter.get('conditions');
  const filter = {};

  conditions && conditions.forEach((condition) => {
    const conditionId = condition.get('id');
    switch (conditionId) {
      case FILTERS.INN:
        filter[FILTERS.INN] = condition.get('params');
        break;
      case FILTERS.KPP:
        filter[FILTERS.KPP] = condition.get('params');
        break;
      case FILTERS.ACCOUNT:
        filter[FILTERS.ACCOUNT] = condition.get('params');
        break;
      case FILTERS.SIGNED:
        filter[conditionId] = condition.get('params') === FILTER_VALUES.SIGNED;
        break;
        // фильтры bankName и bic оба ищут значения по bic
      case FILTERS.BANK_NAME:
        if (filter[FILTERS.BIC]) {
          filter[FILTERS.BIC] = filter[FILTERS.BIC].includes(condition.get('params'))
            ? filter[FILTERS.BIC]
            : filter[FILTERS.BIC] + condition.get('params');
        } else {
          filter[FILTERS.BIC] = condition.get('params');
        }
        break;
      case FILTERS.BIC:
        if (filter[FILTERS.BIC]) {
          filter[FILTERS.BIC] = filter[FILTERS.BIC].includes(condition.get('params'))
            ? filter[FILTERS.BIC]
            : filter[FILTERS.BIC] + condition.get('params');
        } else {
          filter[FILTERS.BIC] = condition.get('params');
        }
        break;
      default:
        filter[conditionId] = condition.get('params');
    }
  });

  return {
    offset,
    offsetStep,
    sort,
    desc,
    ...filter
  };
};

export function* getDictionaryForFilter({ payload }) {
  const { conditionId, value } = payload;
  let items = [];

  try {
    switch (conditionId) {
      case FILTERS.INN:
        items = yield call(dalGetList, { payload : { [FILTERS.INN]: value } });
        break;
      case FILTERS.KPP:
        items = yield call(dalGetList, { payload : { [FILTERS.KPP]: value } });
        break;
      case FILTERS.ACCOUNT:
        items = yield call(dalGetList, { payload : { [FILTERS.ACCOUNT]: value } });
        break;
      case FILTERS.BIC:
        items = yield call(dalGetBIC, { payload : { code: value } });
        break;
      case FILTERS.BANK_NAME:
        items = yield call(dalGetBIC, { payload : { code: value } });
        break;
      default:
        break;
    }
    yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS, payload: { conditionId, items } });
  } catch (error) {
    yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL, error });
  }
}

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);
export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload: { pageIndex, filter } });
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;

  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* getList() {
  try {
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ settings, pages, filters });
    const payload = yield call(dalGetList, { payload: params });
    yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_GET_LIST_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.CORRESPONDENTS_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}

export function* operation(action) {
  const { payload: { operationId, items } } = action;
  switch (operationId) {
    case 'remove':
      yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_CONFIRM_SHOW, payload: items });
      break;
    case 'edit':
      yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_EDIT, payload: items });
      break;
    default:
      break;
  }
}

export function* removeConfirm(action) {
  const { payload } = action;
  const many = payload.length > 1;
  yield put(dalAddOrdinaryNotificationAction({
    type: 'confirm',
    level: 'error',
    message: `Вы действительно хотите удалить контрагент${many ? 'ов' : 'а'}?`,
    success: {
      label: 'Подтвердить',
      action: {
        type: ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_REQUEST,
        payload
      }
    },
    cancel: {
      label: 'Отмена',
      action: {
        type: ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_CONFIRM_CANCEL
      }
    }
  }));
}

export function* removeOperation({ payload }) {
  const many = payload.length > 1;

  try {
    yield call(dalRemoveCorrespondent, { payload: { id: payload } });
    yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_SUCCESS });
    yield put(
      dalAddOrdinaryNotificationAction({
        type: 'success',
        title: `Удаление контрагент${many ? 'ов' : 'а'}`,
        message: `Контрагент${many ? 'ы' : ''} успешно удален${many ? 'ы' : ''}`
      })
    );
  } catch (error) {
    yield put({ type: ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_FAIL });
    yield put(
      dalAddOrdinaryNotificationAction({
        type: 'error',
        title: 'Произошла ошибка на сервере'
      })
    );
  }
}

export function* routeToItem(action) {
  const { payload } = action;

  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/${payload}`
  }));
}

export function* routeToNewCorrespondent() {
  yield* routeToItem({ payload: 'new' });
}

export default function* saga() {
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);

  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION, operation);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_CONFIRM_SHOW, removeConfirm);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_EDIT, routeToItem);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_OPERATION_REMOVE_REQUEST, removeOperation);

  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_NEW_PAYMENT, routeToNewCorrespondent);
  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_ROUTE_TO_ITEM, routeToItem);

  yield takeLatest(ACTIONS.CORRESPONDENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST, getDictionaryForFilter);
}
