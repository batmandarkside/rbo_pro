import { createSelector } from 'reselect';
import { List, Map, OrderedMap } from 'immutable';
import { getFormattedAccNum } from 'utils';
import { LOCAL_REDUCER, COLUMNS, FILTERS } from './constants';

const isListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isListLoading']);
const isListUpdateRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isListUpdateRequired']);
const isReloadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isReloadRequired']);

const columnsSelector = state => state.getIn([LOCAL_REDUCER, 'columns']);
const filtersConditionsSelector = state => state.getIn([LOCAL_REDUCER, 'filtersConditions']);
const filtersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
const listSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
const settingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
const pagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
const paginationOptionsSelector = state => state.getIn([LOCAL_REDUCER, 'paginationOptions']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);
const selectedItemsSelector = state => state.getIn([LOCAL_REDUCER, 'selectedItems']);

const mapListItemOperations = (allowedSmActions, operations) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'remove':
          return allowedSmActions.get('delete');
        case 'edit':
          return allowedSmActions.get('delete');
        default:
          return false;
      }
    })), OrderedMap()
  ).toJS()
);

const mapListItem = (item, selected, operations) => Map({
  id: item.get('id'),
  [COLUMNS.NAME]: item.get(COLUMNS.NAME, ''),
  [COLUMNS.INN]: item.get(COLUMNS.INN, ''),
  [COLUMNS.KPP]: item.get(COLUMNS.KPP, ''),
  [COLUMNS.SIGNED]: item.get(COLUMNS.SIGNED, ''),
  [COLUMNS.ACCOUNT]: getFormattedAccNum(item.get(COLUMNS.ACCOUNT, '')),
  [COLUMNS.BANK_NAME]: item.get(COLUMNS.BANK_NAME, ''),
  [COLUMNS.BIC]: item.get(COLUMNS.BIC, ''),
  [COLUMNS.PURPOSE]: item.get(COLUMNS.PURPOSE, ''),
  [COLUMNS.COMMENT]: item.get(COLUMNS.COMMENT, ''),
  selected,
  operations: mapListItemOperations(item.get('allowedSmActions'), operations)
});

const mapFilterConditionParamsToValue = (params, condition) => {
  if (!params) return '...';
  switch (condition.get('type')) {
    case 'search':
      switch (condition.get('id')) {
        case FILTERS.ACCOUNT:
          return getFormattedAccNum(params);
        default:
          return params;
      }
    default:
      return params;
  }
};

const mapFilterConditionDictionaryItems = condition => condition.setIn(
  ['values', 'items'],
  condition.getIn(['values', 'items'], List()).map((item) => {
    switch (condition.get('id')) {
      case FILTERS.INN:
        return Map({
          receiverINN: item.get('receiverINN'),
          receiverName: item.get('receiverName'),
          title: item.get('receiverINN'),
          value: item.get('id')
        });
      case FILTERS.KPP:
        return Map({
          receiverKPP: item.get('receiverKPP'),
          receiverName: item.get('receiverName'),
          title: item.get('receiverKPP'),
          value: item.get('id')
        });
      case FILTERS.ACCOUNT:
        return Map({
          account: getFormattedAccNum(item.get('account')),
          receiverName: item.get('receiverName'),
          receiverAccount: item.get('account'),
          title: item.get('account'),
          value: item.get('id')
        });
      case FILTERS.BIC:
        return Map({
          bic: item.get('bic'),
          bankName: item.get('bankName'),
          title: item.get('bic'),
          value: item.get('id')
        });
      case FILTERS.BANK_NAME:
        return Map({
          bic: item.get('bic'),
          bankName: item.get('bankName'),
          title: item.get('bankName'),
          value: item.get('id')
        });
      default:
        return null;
    }
  })
);

const mapFilterConditions = (conditions, savedConditions, filtersConditions) => (
  (conditions && conditions.size ? conditions : savedConditions).map(condition =>
    condition.merge({
      params: condition.get('params'),
      value: mapFilterConditionParamsToValue(
        condition.get('params'),
        filtersConditions.find(filtersCondition => filtersCondition.get('id') === condition.get('id'))
      )
    })
  )
);

export const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) => columns.map(item => item.merge({
    visible: !!settings.get('visible').find(visible => visible === item.get('id')),
    sorting: settings.getIn(['sorting', 'id']) === item.get('id') ? settings.getIn(['sorting', 'direction']) : 0,
    grouped: settings.get('grouped') === item.get('id'),
    width: settings.getIn(['width', item.get('id')])
  }))
);

export const filtersCreatedSelector = createSelector(
  filtersSelector,
  filtersConditionsSelector,
  (filters, filtersConditions) => filters
    .map(filter => filter.merge({
      conditions: mapFilterConditions(filter.get('conditions'), filter.get('savedConditions'), filtersConditions)
    }))
    .sort((a, b) => {
      if (a.get('isPreset') && b.get('isPreset')) return 0;
      if (a.get('isPreset') && !b.get('isPreset')) return -1;
      if (!a.get('isPreset') && b.get('isPreset')) return 1;
      return a.get('title').localeCompare(b.get('title'));
    })
);

export const panelOperationsCreatedSelector = createSelector(
  operationsSelector,
  selectedItemsSelector,
  listSelector,
  (operations, selectedItems, list) => operations.filter((operation) => {
    const itemsWithOperations = selectedItems.map(item => Map({
      id: item,
      operations: mapListItemOperations(
        list.find(listItem => listItem.get('id') === item).get('allowedSmActions'),
        operations
      )
    }));

    return (selectedItems.size === 0)
      || ((itemsWithOperations.size === itemsWithOperations.filter(item => !!item.get('operations')[operation.get('id')]).size)
      && (selectedItems.size === 1 || operation.get('grouped')));
  })
);

export const listCreatedSelector = createSelector(
  listSelector,
  selectedItemsSelector,
  settingsSelector,
  panelOperationsCreatedSelector,
  (list, selectedItems, settings, operations) => {
    if (!list.size) return list;
    const mappedList = list.map(item =>
      mapListItem(
        item,
        !!selectedItems.find(selectedItem => selectedItem === item.get('id')),
        operations
      ));
    const grouped = settings.get('grouped');
    if (!grouped) {
      return List([Map({
        id: 'notGrouped',
        selected: mappedList.filter(item => item.get('selected')).size === mappedList.size,
        items: mappedList
      })]);
    }
    return mappedList.map(item => item.get(grouped)).toOrderedSet().toList().map((group, i) => {
      const items = mappedList.filter(item => item.get(grouped) === group);
      return Map({
        id: `${grouped}_${i}`,
        title: group,
        selected: items.filter(item => item.get('selected')).size === items.size,
        items
      });
    });
  }
);

export const paginationOptionsCreatedSelector = createSelector(
  paginationOptionsSelector,
  settingsSelector,
  (paginationOptions, settings) => paginationOptions.map(item => ({
    value: item,
    title: item,
    selected: settings.get('pagination') === item
  }))
);

export const filtersConditionsCreatedSelector = createSelector(
  filtersConditionsSelector,
  filtersConditions => filtersConditions.map(
    condition => (condition.get('type') === 'suggest' || condition.get('type') === 'search'
      ? mapFilterConditionDictionaryItems(condition)
      : condition)
  )
);

const mapStateToProps = state => ({
  isListLoading: isListLoadingSelector(state),
  isListUpdateRequired: isListUpdateRequiredSelector(state),
  isReloadRequired: isReloadRequiredSelector(state),
  columns: columnsCreatedSelector(state),
  filtersConditions: filtersConditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  list: listCreatedSelector(state),
  pages: pagesSelector(state),
  paginationOptions: paginationOptionsCreatedSelector(state),
  settings: settingsSelector(state),
  operations: panelOperationsCreatedSelector(state),
  selectedItems: selectedItemsSelector(state)
});

export default mapStateToProps;
