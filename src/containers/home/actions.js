import { ACTIONS } from './constants';

/**
 *    action загрузки контейнера
 */
export const mountAction = params => ({
  type: ACTIONS.HOME_CONTAINER_MOUNT,
  payload: { params }
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.HOME_CONTAINER_UNMOUNT
});

/**
 *    action изменения вида в блоке счетов
 */
export const accountsPreviewViewsChangeAction = id => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_VIEWS_CHANGE,
  payload: { id }
});

/**
 *    action выбора счета в блоке счетов (компактный вид)
 */
export const accountsPreviewCompactListSelectItemAction = id => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_LIST_SELECT_ITEM,
  payload: { id }
});

/**
 *    action изменения вкладки в блоке счетов (корпоративный вид)
 */
export const accountsPreviewCorpTabsChangeAction = ({ tabId }) => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_TABS_CHANGE,
  payload: { tabId }
});

/**
 *    action изменения видимости колонок таблицы выписок (корпоративный вид)
 */
export const accountsPreviewCorpSettingsColumnsChangeAction = columns => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_SETTINGS_COLUMNS_CHANGE,
  payload: { columns }
});

/**
 *    action изменения отображения Итого в таблице выписок (корпоративный вид)
 */
export const accountsPreviewCorpSettingsSummaryShowedChangeAction = isSummaryShowed => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_SETTINGS_SUMMARY_SHOWED_CHANGE,
  payload: { isSummaryShowed }
});

/**
 *    action печати выписки в блоке счетов (корпоративный вид)
 */
export const accountsPreviewCorpItemPrintAction = id => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_PRINT_REQUEST,
  payload: id
});

/**
 *    action экспорта выписки в блоке счетов (корпоративный вид)
 */
export const accountsPreviewCorpItemExportAction = id => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_EXPORT_REQUEST,
  payload: id
});

/**
 *    action изменения избранных счетов
 */
export const accountsSettingsFavoritesChangeAction = favorites => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_SETTINGS_FAVORITES_CHANGE,
  payload: { favorites }
});

/**
 *    action обновления блока счетов
 */
export const accountsRefreshAction = () => ({
  type: ACTIONS.HOME_CONTAINER_ACCOUNTS_REFRESH
});

/**
 *    action изменения фильтра рублевых платежей
 */
export const rPaymentsFilterChangeAction = items => ({
  type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_FILTER_CHANGE,
  payload: { statuses: items }
});

/**
 *    action печати платежного поручения
 */
export const rPaymentsItemPrintAction = id => ({
  type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_ITEM_PRINT_REQUEST,
  payload: { docId: id }
});

/**
 *    action повтора платежного поручения
 */
export const rPaymentsItemRepeatAction = id => ({
  type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_ITEM_REPEAT,
  payload: { docId: id }
});

/**
 *    action изменения фильтра входящих сообщений
 */
export const incomingMessagesFilterChangeAction = items => ({
  type: ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_FILTER_CHANGE,
  payload: { items }
});

/**
 *    action печати платежного поручения
 */
export const incomingMessagesItemDownloadAttachmentAction = id => ({
  type: ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_ITEM_DOWNLOAD_ATTACHMENT_REQUEST,
  payload: { docId: id }
});
