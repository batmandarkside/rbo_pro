import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import { COMPONENT_STYLE_NAME } from '../../constants';
import Card from '../card';
import Preview from './preview';
import Settings from './settings';

export class HomeAccounts extends Component {

  static propTypes = {
    isCompactMovementsLoading: PropTypes.bool,
    isCorpListLoading: PropTypes.bool,
    views: immutablePropTypes.list.isRequired,
    onPreviewViewToggleItemClick: PropTypes.func.isRequired,
    compactList: immutablePropTypes.list.isRequired,
    compactMovements: immutablePropTypes.list.isRequired,
    linkToAllCompactMovements: PropTypes.string,
    onPreviewCompactListItemClick: PropTypes.func.isRequired,
    corpTabs: immutablePropTypes.list.isRequired,
    corpList: immutablePropTypes.list.isRequired,
    corpColumns: immutablePropTypes.list.isRequired,
    isCorpSummaryShowed: PropTypes.bool,
    onPreviewCorpTabClick: PropTypes.func.isRequired,
    onPreviewCorpItemPrintClick: PropTypes.func.isRequired,
    onPreviewCorpItemExportClick: PropTypes.func.isRequired,
    onPreviewCorpSettingsColumnsChange: PropTypes.func.isRequired,
    onPreviewCorpSettingsSummaryShowedChange: PropTypes.func.isRequired,
    settings: immutablePropTypes.map.isRequired,
    onSettingsFavoritesChange: PropTypes.func.isRequired,
    onSettingsClose: PropTypes.func.isRequired
  };

  state = {
    isSettingsOpen: false
  };

  handleOpenSettings = () => {
    this.setState({ isSettingsOpen: true });
  };

  handleCloseSettings = () => {
    this.setState({ isSettingsOpen: false });
    this.props.onSettingsClose();
  };

  render() {
    const {
      views,
      onPreviewViewToggleItemClick,
      isCompactMovementsLoading,
      compactList,
      compactMovements,
      linkToAllCompactMovements,
      onPreviewCompactListItemClick,
      isCorpListLoading,
      corpTabs,
      corpList,
      corpColumns,
      isCorpSummaryShowed,
      onPreviewCorpTabClick,
      onPreviewCorpItemPrintClick,
      onPreviewCorpItemExportClick,
      onPreviewCorpSettingsColumnsChange,
      onPreviewCorpSettingsSummaryShowedChange,
      settings,
      onSettingsFavoritesChange
    } = this.props;

    const { isSettingsOpen } = this.state;

    return (
      <Card>
        <div className={`${COMPONENT_STYLE_NAME}__accounts`} data-loc="indexAccounts">
          {!isSettingsOpen &&
          <Preview
            views={views}
            onViewToggleItemClick={onPreviewViewToggleItemClick}
            isCompactMovementsLoading={isCompactMovementsLoading}
            compactList={compactList}
            compactMovements={compactMovements}
            linkToAllCompactMovements={linkToAllCompactMovements}
            onCompactListItemClick={onPreviewCompactListItemClick}
            isCorpListLoading={isCorpListLoading}
            corpTabs={corpTabs}
            corpList={corpList}
            corpColumns={corpColumns}
            isCorpSummaryShowed={isCorpSummaryShowed}
            onCorpTabClick={onPreviewCorpTabClick}
            onCorpItemPrintClick={onPreviewCorpItemPrintClick}
            onCorpItemExportClick={onPreviewCorpItemExportClick}
            onCorpSettingsColumnsChange={onPreviewCorpSettingsColumnsChange}
            onCorpSettingsSummaryShowedChange={onPreviewCorpSettingsSummaryShowedChange}
            onSettingIconClick={this.handleOpenSettings}
          />
          }
          {isSettingsOpen &&
          <Settings
            favorites={settings.get('favorites')}
            list={settings.get('list')}
            onFavoritesChange={onSettingsFavoritesChange}
            onClose={this.handleCloseSettings}
          />
          }
        </div>
      </Card>
    );
  }
}

export default HomeAccounts;
