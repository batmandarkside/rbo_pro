import React from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import { Link } from 'react-router-dom';
import Loader from '@rbo/components/lib/loader/Loader';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../../constants';
import { CardContent, CardNotFound } from '../../../card';
import ListItem from './list-item';
import MovementsGroup from './movements-group';

const HomeAccountsPreviewCompact = (props) => {
  const {
    isMovementsLoading,
    list,
    movements,
    linkToAllMovements,
    onSettingIconClick,
    onListItemClick
  } = props;

  const selectedAccount = list.find(item => item.get('isSelected'));

  return (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-compact`}>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-blocks`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-block`}>
          <CardContent>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-list`} data-loc="indexAccountsPreviewCompactList">
              <div className={`${COMPONENT_STYLE_NAME}__accounts-list-content`}>
                {!list.size &&
                <CardNotFound>
                  <div>
                    <span>
                      Чтобы добавить информацию об&nbsp;избранных счетах в&nbsp;эту область экрана,
                      выберите несколько счетов из&nbsp;списка, нажав 
                    </span>
                    <Icon type="settings" size="16" />
                    <span> или просто </span>
                    <span className="m-dashed-link" onClick={onSettingIconClick}>нажмите здесь</span>
                  </div>
                </CardNotFound>
                }
                {!!list.size &&
                <div className={`${COMPONENT_STYLE_NAME}__accounts-list-items`}>
                  {list.map((item, key) => (
                    <ListItem
                      key={key}
                      id={item.get('id')}
                      isSelected={item.get('isSelected')}
                      isBlocked={item.get('isBlocked')}
                      isClosed={item.get('isClosed')}
                      accNum={item.get('accNum')}
                      name={item.get('name')}
                      organization={item.get('organization')}
                      currentBalance={item.get('currentBalance')}
                      balanceDate={item.get('balanceDate')}
                      balance={item.get('balance')}
                      onClick={onListItemClick}
                    />
                  ))}
                </div>
                }
              </div>
            </div>
          </CardContent>
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-block`}>
          <CardContent>
            <div
              className={`${COMPONENT_STYLE_NAME}__accounts-movements`}
              data-loc="indexAccountsPreviewCompactOperations"
            >
              {selectedAccount &&
              <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-header`}>
                <h5>Последние 30 операций по счету {selectedAccount.get('accNum')}</h5>
              </div>
              }
              <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-content`}>
                {isMovementsLoading && <Loader centered />}
                {!isMovementsLoading && !movements.size &&
                <CardNotFound>
                  <p>Операции по выбранному счету отсутствуют</p>
                </CardNotFound>
                }
                {!isMovementsLoading && !!movements.size &&
                <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-groups`}>
                  {movements.map((item, key) => (
                    <MovementsGroup
                      key={key}
                      title={item.get('title')}
                      items={item.get('items')}
                    />
                  ))}
                </div>
                }
              </div>
              <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-footer`}>
                {!!movements.size &&
                <Link className="m-dashed-link" to={linkToAllMovements}>
                  Все операции
                </Link>
                }
              </div>
            </div>
          </CardContent>
        </div>
      </div>
    </div>
  );
};

HomeAccountsPreviewCompact.propTypes = {
  isMovementsLoading: PropTypes.bool,
  list: immutablePropTypes.list.isRequired,
  movements: immutablePropTypes.list.isRequired,
  linkToAllMovements: PropTypes.string,
  onSettingIconClick: PropTypes.func.isRequired,
  onListItemClick: PropTypes.func.isRequired,
};

export default HomeAccountsPreviewCompact;
