import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../../constants';

class HomeAccountsPreviewCompactListItem extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    isSelected: PropTypes.bool,
    isBlocked: PropTypes.bool,
    isClosed: PropTypes.bool,
    accNum: PropTypes.string,
    name: PropTypes.string,
    organization: PropTypes.string,
    currentBalance: PropTypes.string,
    balanceDate: PropTypes.string,
    balance: PropTypes.string,
    onClick: PropTypes.func.isRequired
  };

  handleOnClick = () => {
    if (!this.props.isSelected) this.props.onClick(this.props.id);
  };

  render() {
    const {
      isSelected,
      isBlocked,
      isClosed,
      accNum,
      name,
      organization,
      currentBalance,
      balanceDate,
      balance
    } = this.props;

    const elementClassName = classnames(
      `${COMPONENT_STYLE_NAME}__accounts-list-item`,
      isSelected && 's-selected'
    );

    const itemNumberClassName = classnames(
      `${COMPONENT_STYLE_NAME}__accounts-list-item-number`,
      isBlocked && 'm-blocked',
      isClosed && 'm-closed'
    );

    return (
      <div className={elementClassName} onClick={this.handleOnClick}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-title`}>
          <span className={itemNumberClassName} >
            {accNum}
          </span>
          {(isBlocked || isClosed) &&
          <span className={`${COMPONENT_STYLE_NAME}__accounts-list-item-icon`}>
            {isBlocked && <Icon type="account-blocked" size="16" title="Счет заблокирован" />}
            {isClosed && <Icon type="account-closed" size="16" title="Счет закрыт" />}
          </span>
          }
          <span className={`${COMPONENT_STYLE_NAME}__accounts-list-item-name`}>
            {name}
          </span>
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-content`}>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-content-left`}>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-organization`}>
              {organization}
            </div>
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-content-right`}>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-balance`}>
              <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-balance-item m-today`}>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-balance-item-label`}>
                  Доступно
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-balance-item-value`}>
                  {currentBalance}
                </div>
              </div>
              <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-balance-item`}>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-balance-item-label`}>
                  Баланс на {balanceDate}
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-list-item-balance-item-value`}>
                  {balance}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HomeAccountsPreviewCompactListItem;
