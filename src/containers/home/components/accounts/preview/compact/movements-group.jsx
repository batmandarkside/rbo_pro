import React from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import { COMPONENT_STYLE_NAME } from '../../../../constants';
import MovementsItem from './movements-item';

const HomeAccountsPreviewCompactMovementsGroup = (props) => {
  const { title, items } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-group`}>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-group-title`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-group-date`}>
          {title}
        </div>
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-group-content`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-items`}>
          {items.map((item, key) => (
            <MovementsItem
              key={key}
              isCredit={item.get('isCredit')}
              credit={item.get('credit')}
              debit={item.get('debit')}
              name={item.get('name')}
              purpose={item.get('purpose')}
              number={item.get('number')}
              account={item.get('account')}
              inn={item.get('inn')}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

HomeAccountsPreviewCompactMovementsGroup.propTypes = {
  title: PropTypes.string,
  items: immutablePropTypes.list.isRequired
};

export default HomeAccountsPreviewCompactMovementsGroup;
