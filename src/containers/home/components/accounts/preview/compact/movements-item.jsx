import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../../../../constants';

class HomeAccountsPreviewCompactMovementsItem extends Component {

  static propTypes = {
    isCredit: PropTypes.bool,
    credit: PropTypes.string,
    debit: PropTypes.string,
    name: PropTypes.string,
    purpose: PropTypes.string,
    number: PropTypes.string,
    account: PropTypes.string,
    inn: PropTypes.string
  };

  state = {
    isOpen: false
  };

  handleToggleOpen = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const { isOpen } = this.state;

    const { isCredit, credit, debit, name, purpose, number, account, inn } = this.props;

    const amountClassName = classnames(
      `${COMPONENT_STYLE_NAME}__accounts-movements-item-amount`,
      isCredit ? 'm-credit' : 'm-debit'
    );

    return (
      <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item`} onClick={this.handleToggleOpen}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-left`}>
          <div className={amountClassName}>
            {isCredit ? credit : debit}
          </div>
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-right`}>
          {!isOpen &&
          <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr`}>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr-name`}>
              {name}
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr-purpose m-collapsed`}>
              {purpose}
            </div>
          </div>
          }
          {isOpen &&
          <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr`}>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr-number`}>
              {number}
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr-purpose`}>
              {purpose}
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr-name`}>
              {name}
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr-account`}>
              {account}
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-movements-item-corr-inn`}>
              {inn}
            </div>
          </div>
          }
        </div>
      </div>
    );
  }
}

export default HomeAccountsPreviewCompactMovementsItem;
