import React from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../../../../constants';
import Item from './item';

const HomeAccountsPreviewCorpGroup = (props) => {
  const { columns, title, blocks, isSummaryShowed, onItemPrintClick, onItemExportClick } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group`}>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group-header`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group-title`}>
          {title}
        </div>
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group-content`}>
        {blocks.map((block, blockKey) => (
          <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group-block`} key={blockKey}>
            <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group-items`}>
              {block.get('items').map((item, itemKey) => (
                <Item
                  key={itemKey}
                  id={item.get('id')}
                  link={item.get('link')}
                  data={item.get('data')}
                  isBlocked={item.get('isBlocked')}
                  isClosed={item.get('isClosed')}
                  columns={columns}
                  onPrintClick={onItemPrintClick}
                  onExportClick={onItemExportClick}
                />
              ))}
            </div>
            {isSummaryShowed &&
            <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group-summary`}>
              <div className={`${COMPONENT_STYLE_NAME}__accounts-table-row`}>
                {columns.map((column, columnKey) => (
                  <div
                    key={columnKey}
                    className={classnames(
                      `${COMPONENT_STYLE_NAME}__accounts-table-cell`,
                      `m-${column.get('id')}`,
                      column.get('modeClassName') && `m-${column.get('modeClassName')}`,
                      !column.get('isVisible') && 's-hidden'
                    )}
                  >
                    {column.get('isVisible') && block.getIn(['summary', column.get('id')])}
                  </div>
                ))}
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-cell m-operations m-text-align-right`} />
              </div>
            </div>
            }
          </div>
        ))}
      </div>
    </div>
  );
};

HomeAccountsPreviewCorpGroup.propTypes = {
  columns: immutablePropTypes.list.isRequired,
  title: PropTypes.string.isRequired,
  blocks: immutablePropTypes.list.isRequired,
  isSummaryShowed: PropTypes.bool,
  onItemPrintClick: PropTypes.func.isRequired,
  onItemExportClick: PropTypes.func.isRequired
};

export default HomeAccountsPreviewCorpGroup;
