import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import classnames from 'classnames';
import Loader from '@rbo/components/lib/loader/Loader';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../../constants';
import { CardContent, CardNotFound } from '../../../card';
import Group from './group';
import Settings from './settings';

class HomeAccountsPreviewCorp extends Component {

  static propTypes = {
    isLoading: PropTypes.bool,
    list: immutablePropTypes.list.isRequired,
    columns: immutablePropTypes.list.isRequired,
    isSummaryShowed: PropTypes.bool,
    onItemPrintClick: PropTypes.func.isRequired,
    onItemExportClick: PropTypes.func.isRequired,
    onSettingsColumnsChange: PropTypes.func.isRequired,
    onSettingsSummaryShowedChange: PropTypes.func.isRequired
  };

  state = {
    isFilterOpen: false
  };

  handleOpenSettings = () => {
    this.setState({ isSettingsOpen: true });
  };

  handleCloseSettings = () => {
    this.setState({ isSettingsOpen: false });
  };

  render() {
    const {
      isLoading,
      list,
      columns,
      isSummaryShowed,
      onItemPrintClick,
      onItemExportClick,
      onSettingsColumnsChange,
      onSettingsSummaryShowedChange
    } = this.props;
    const { isSettingsOpen } = this.state;

    return (
      <div className={`${COMPONENT_STYLE_NAME}__accounts-corp`}>
        <div
          className={`${COMPONENT_STYLE_NAME}__card-filters`}
          ref={(element) => { this.filterElement = element; }}
        >
          {isSettingsOpen &&
          <Settings
            openerElement={this.filterElement}
            columns={columns}
            isSummaryShowed={isSummaryShowed}
            onColumnsChange={onSettingsColumnsChange}
            onSummaryShowedChange={onSettingsSummaryShowedChange}
            onClose={this.handleCloseSettings}
          />
          }
        </div>
        <CardContent>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-statements`} data-loc="indexAccountsPreviewStatements">
            {isLoading && <Loader centered />}
            {!isLoading && !!list.size &&
              <div className={`${COMPONENT_STYLE_NAME}__accounts-statements-settings`}>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-statements-settings-icon`}>
                  <Icon type="settings" size="16" title="Фильтр" onClick={this.handleOpenSettings} />
                </div>
              </div>
            }
            {!isLoading && !!list.size &&
            <div className={`${COMPONENT_STYLE_NAME}__accounts-table`}>
              <div className={`${COMPONENT_STYLE_NAME}__accounts-table-content`}>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-header`}>
                  <div className={`${COMPONENT_STYLE_NAME}__accounts-table-row`}>
                    {columns.map((column, key) => (
                      <div
                        key={key}
                        className={classnames(
                          `${COMPONENT_STYLE_NAME}__accounts-table-cell`,
                          `m-${column.get('id')}`,
                          column.get('modeClassName') && `m-${column.get('modeClassName')}`,
                          !column.get('isVisible') && 's-hidden'
                        )}
                      >
                        {column.get('isVisible') && column.get('name')}
                      </div>
                    ))}
                    <div className={`${COMPONENT_STYLE_NAME}__accounts-table-cell m-operations m-text-align-right`} />
                  </div>
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-body`}>
                  {list.map((group, key) =>
                    <Group
                      key={key}
                      title={group.get('title')}
                      blocks={group.get('blocks')}
                      columns={columns}
                      isSummaryShowed={isSummaryShowed}
                      onItemPrintClick={onItemPrintClick}
                      onItemExportClick={onItemExportClick}
                    />
                  )}
                </div>
              </div>
            </div>
            }
            {!isLoading && !list.size &&
            <CardNotFound>
              <p>Выписки по избранным счетам за этот день отсутствуют</p>
            </CardNotFound>
            }
          </div>
        </CardContent>
      </div>
    );
  }
}

export default HomeAccountsPreviewCorp;
