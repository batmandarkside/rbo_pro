import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../../constants';

class HomeAccountsPreviewCorpItem extends Component {

  static propTypes = {
    id: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    isBlocked: PropTypes.bool,
    isClosed: PropTypes.bool,
    data: immutablePropTypes.map.isRequired,
    columns: immutablePropTypes.list.isRequired,
    onPrintClick: PropTypes.func.isRequired,
    onExportClick: PropTypes.func.isRequired
  };

  handlePrintClick = (e) => {
    e.stopPropagation();
    e.preventDefault();
    this.props.onPrintClick(this.props.id);
  };

  handleExportClick = (e) => {
    e.stopPropagation();
    e.preventDefault();
    this.props.onExportClick(this.props.id);
  };

  render() {
    const { columns, link, isBlocked, isClosed, data } = this.props;

    return (
      <div className={`${COMPONENT_STYLE_NAME}__accounts-table-group-item`}>
        <Link to={link}>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-table-row`}>
            {columns.map((column, key) => (
              <div
                key={key}
                className={classnames(
                  `${COMPONENT_STYLE_NAME}__accounts-table-cell`,
                  `m-${column.get('id')}`,
                  column.get('modeClassName') && `m-${column.get('modeClassName')}`,
                  !column.get('isVisible') && 's-hidden',
                  column.get('id') === 'account' && isBlocked && 'm-blocked',
                  column.get('id') === 'account' && isClosed && 'm-closed',
              )}
              >
                {column.get('isVisible') &&
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-value`}>
                  {data.get(column.get('id'))}
                </div>
                }
                {column.get('id') === 'account' && isBlocked &&
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-icon`}>
                  <Icon type="account-blocked" size="16" title="Счет заблокирован" />
                </div>
                }
                {column.get('id') === 'account' && isClosed &&
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-icon`}>
                  <Icon type="account-closed" size="16" title="Счет закрыт" />
                </div>
                }
              </div>
            ))}
            <div className={`${COMPONENT_STYLE_NAME}__accounts-table-cell m-operations m-text-align-right`}>
              <div className={`${COMPONENT_STYLE_NAME}__accounts-table-operations`}>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-operations-item`}>
                  <Icon type="download" size="16" title="Экспортировать в 1C" onClick={this.handleExportClick} />
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__accounts-table-operations-item`}>
                  <Icon type="print" size="16" title="Распечатать" onClick={this.handlePrintClick} />
                </div>
              </div>
            </div>
          </div>
        </Link>
      </div>
    );
  }
}

export default HomeAccountsPreviewCorpItem;
