import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import classnames from 'classnames';
import PopupBox from 'components/ui-components/popup-box';
import { RboFormFieldCheckbox } from 'components/ui-components/rbo-form-compact';
import { COMPONENT_STYLE_NAME } from '../../../../constants';

class HomeAccountsPreviewCorpSettings extends Component {

  static propTypes = {
    openerElement: PropTypes.object.isRequired,
    columns: immutablePropTypes.list.isRequired,
    isSummaryShowed: PropTypes.bool,
    onColumnsChange: PropTypes.func.isRequired,
    onSummaryShowedChange: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
  };

  onColumnsChange = (id, selected) => {
    const columns = this.props.columns.map(item => (item.get('id') === id ? item.set('isVisible', selected) : item));
    this.props.onColumnsChange(columns);
  };

  onSummaryShowedChange = (selected) => {
    this.props.onSummaryShowedChange(selected);
  };

  handleSelectAllColumns = () => {
    const { columns } = this.props;
    if (columns.filter(item => item.get('isVisible')).size !== columns.size) {
      const newColumns = columns.map(item => item.set('isVisible', true));
      this.props.onColumnsChange(newColumns);
    }
  };

  render() {
    const { openerElement, columns, isSummaryShowed, onClose } = this.props;

    const selectAllClassName = classnames(
      'm-dashed-link',
      columns.filter(item => item.get('isVisible')).size === columns.size && 's-disabled'
    );

    return (
      <PopupBox
        openerElement={openerElement}
        onClose={onClose}
        containerElementSelector={`.${COMPONENT_STYLE_NAME}__accounts-corp`}
      >
        <div className={`${COMPONENT_STYLE_NAME}__card-filters-popup`}>
          <div className={`${COMPONENT_STYLE_NAME}__card-filters-title m-inline`}>
            <h5>Столбцы:</h5>
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__card-filters-operations m-inline`}>
            <div className={`${COMPONENT_STYLE_NAME}__card-filters-operations-item`}>
              <span
                className={selectAllClassName}
                onClick={this.handleSelectAllColumns}
                data-loc="indexAccountsStatementsFilterCheckAll"
              >
                Показать все
              </span>
            </div>
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__card-filters-list`}>
            {columns.filter(column => column.get('id') !== 'account').map((column, key) => (
              <div
                key={key}
                className={`${COMPONENT_STYLE_NAME}__card-filters-list-item`}
              >
                <RboFormFieldCheckbox
                  id={column.get('id')}
                  title={column.get('name')}
                  value={column.get('isVisible')}
                  onChange={({ fieldValue }) => { this.onColumnsChange(column.get('id'), fieldValue); }}
                />
              </div>
            ))}
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__card-filters-separator`} />
          <div className={`${COMPONENT_STYLE_NAME}__card-filters-list`}>
            <div className={`${COMPONENT_STYLE_NAME}__card-filters-list-item`}>
              <RboFormFieldCheckbox
                id="isSummaryShowed"
                title="Отображать &laquo;Итого&raquo;"
                value={isSummaryShowed}
                onChange={({ fieldValue }) => { this.onSummaryShowedChange(fieldValue); }}
              />
            </div>
          </div>
        </div>
      </PopupBox>
    );
  }
}

export default HomeAccountsPreviewCorpSettings;
