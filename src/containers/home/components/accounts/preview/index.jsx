import React from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import RboTabs  from 'components/ui-components/rbo-tabs';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../constants';
import { CardHeader } from '../../card';
import ViewToggleItem from './view-toggle-item';
import CompactView from './compact';
import CorpView from './corp';

class HomeAccountsPreview extends React.Component {

  static propTypes = {
    isCompactMovementsLoading: PropTypes.bool,
    views: immutablePropTypes.list.isRequired,
    onViewToggleItemClick: PropTypes.func.isRequired,

    compactList: immutablePropTypes.list.isRequired,
    compactMovements: immutablePropTypes.list.isRequired,
    linkToAllCompactMovements: PropTypes.string,
    onCompactListItemClick: PropTypes.func.isRequired,

    isCorpListLoading: PropTypes.bool,
    corpTabs: immutablePropTypes.list.isRequired,
    corpList: immutablePropTypes.list.isRequired,
    corpColumns: immutablePropTypes.list.isRequired,
    isCorpSummaryShowed: PropTypes.bool,
    onCorpTabClick: PropTypes.func.isRequired,
    onCorpItemPrintClick: PropTypes.func.isRequired,
    onCorpItemExportClick: PropTypes.func.isRequired,
    onCorpSettingsColumnsChange: PropTypes.func.isRequired,
    onCorpSettingsSummaryShowedChange: PropTypes.func.isRequired,

    onSettingIconClick: PropTypes.func.isRequired
  };

  renderHeaderTitle = () => (
    <h2>Информация по счетам</h2>
  );

  renderHeaderTabs = () => {
    const { views, corpTabs, onCorpTabClick } = this.props;

    const selectedView = views.find(view => view.get('isActive')).get('id');

    return (
      <div className={`${COMPONENT_STYLE_NAME}__accounts-header-tabs`}>
        {selectedView === 'corp' &&
        <div className={`${COMPONENT_STYLE_NAME}__accounts-header-tabs-inner`}>
          <RboTabs
            tabs={corpTabs}
            onTabClick={onCorpTabClick}
            containerElementSelector={`.${COMPONENT_STYLE_NAME}__accounts-header-tabs`}
          />
        </div>
        }
      </div>
    );
  };

  renderHeaderControls = () => {
    const { views, onSettingIconClick, onViewToggleItemClick } = this.props;

    return (
      <div className={`${COMPONENT_STYLE_NAME}__accounts-header-controls`}>
        <div className={`${COMPONENT_STYLE_NAME}__card-header-controls-item`}>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-view-toggle`}>
            {views.map((view, i) =>
              <ViewToggleItem
                key={i}
                id={view.get('id')}
                label={view.get('label')}
                isActive={view.get('isActive')}
                onClick={onViewToggleItemClick}
              />
            )}
          </div>
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__card-header-controls-item`}>
          <Icon
            type="settings"
            size="16"
            title="Настройки"
            onClick={onSettingIconClick}
            locator="indexAccountsButtonSettingsOpen"
          />
        </div>
      </div>
    );
  };

  render() {
    const {
      isCompactMovementsLoading,
      views,
      compactList,
      compactMovements,
      linkToAllCompactMovements,
      onCompactListItemClick,
      isCorpListLoading,
      corpList,
      corpColumns,
      isCorpSummaryShowed,
      onCorpItemPrintClick,
      onCorpItemExportClick,
      onCorpSettingsColumnsChange,
      onCorpSettingsSummaryShowedChange,
      onSettingIconClick
    } = this.props;

    const selectedView = views.find(view => view.get('isActive')).get('id');

    return (
      <div className={`${COMPONENT_STYLE_NAME}__accounts-preview`} data-loc="indexAccountsPreview">
        <div className={`${COMPONENT_STYLE_NAME}__accounts-header`}>
          <CardHeader
            title={this.renderHeaderTitle()}
            tabs={this.renderHeaderTabs()}
            controls={this.renderHeaderControls()}
          />
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-content`}>
          {selectedView === 'compact' &&
          <CompactView
            isMovementsLoading={isCompactMovementsLoading}
            list={compactList}
            movements={compactMovements}
            linkToAllMovements={linkToAllCompactMovements}
            onSettingIconClick={onSettingIconClick}
            onListItemClick={onCompactListItemClick}
          />
          }
          {selectedView === 'corp' &&
          <CorpView
            isLoading={isCorpListLoading}
            list={corpList}
            columns={corpColumns}
            isSummaryShowed={isCorpSummaryShowed}
            onItemPrintClick={onCorpItemPrintClick}
            onItemExportClick={onCorpItemExportClick}
            onSettingsColumnsChange={onCorpSettingsColumnsChange}
            onSettingsSummaryShowedChange={onCorpSettingsSummaryShowedChange}
          />
          }
        </div>
      </div>
    );
  }
}

export default HomeAccountsPreview;
