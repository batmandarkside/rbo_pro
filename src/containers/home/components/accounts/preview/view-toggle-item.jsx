import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../constants';

const HomeAccountsPreviewViewToggleItem = (props) => {
  const { id, label, isActive, onClick } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__accounts-view-toggle-item`,
    isActive && 's-active'
  );

  const handleClick = () => {
    if (!isActive) onClick(id);
  };

  return (
    <div className={elementClassName}>
      <Icon
        type={`view-${id}`}
        size="16"
        title={label}
        onClick={handleClick}
        active={isActive}
      />
    </div>
  );
};

HomeAccountsPreviewViewToggleItem.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};

export default HomeAccountsPreviewViewToggleItem;
