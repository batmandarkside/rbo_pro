import  React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import { RboFormFieldCheckbox } from 'components/ui-components/rbo-form-compact';
import { COMPONENT_STYLE_NAME } from '../../../constants';

const HomeAccountsSettingsFavoritesItem = (props) => {
  const { id, accNum, organization, isSelected } = props;

  const onChange = ({ fieldValue }) => props.onChange(props.id, fieldValue);

  const title = (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-item-label`}>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-item-icon`}>
        {isSelected && <Icon type="star-fill" size="16" />}
        {!isSelected && <Icon type="star-stroke" size="16" />}
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-item-title`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-item-number`}>
          {accNum}
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-item-organization`}>
          {organization}
        </div>
      </div>
    </div>
  );

  return (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-item`}>
      <RboFormFieldCheckbox
        id={id}
        title={title}
        value={isSelected}
        onChange={onChange}
      />
    </div>
  );
};

HomeAccountsSettingsFavoritesItem.propTypes = {
  id: PropTypes.string.isRequired,
  accNum: PropTypes.string,
  organization: PropTypes.string,
  isSelected: PropTypes.bool,
  onChange: PropTypes.func.isRequired
};

export default HomeAccountsSettingsFavoritesItem;
