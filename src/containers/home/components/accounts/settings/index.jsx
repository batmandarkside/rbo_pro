import React from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import { fromJS } from 'immutable';
import classnames from 'classnames';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../constants';
import { CardHeader, CardContent, CardNotFound } from '../../card';
import Sortable from './sortable';
import FavoritesItem from './favorites-item';

const HomeAccountsSettings = (props) => {
  const { favorites, list, onFavoritesChange, onClose } = props;

  const onFavoritesItemChange = (id, selected) => {
    const newFavorites = selected
      ? favorites.push(list.find(item => item.get('id') === id))
      : favorites.filter(item => item.get('id') !== id);
    onFavoritesChange(newFavorites.map(item => item.get('id')));
  };

  const handleFavoritesSelectAll = () => {
    if (list.filter(item => item.get('isSelected')).size !== list.size) {
      const addedItems = list.filter(item => !favorites.find(f => f.get('id') === item.get('id')));
      const newFavorites = favorites.concat(addedItems);
      onFavoritesChange(newFavorites.map(item => item.get('id')));
    }
  };

  const handleFavoritesDeselectAll = () => onFavoritesChange(fromJS([]));

  const renderListHeaderTitle = () => (
    <h2>Информация по&nbsp;счетам</h2>
  );

  const renderFavoritesHeaderTitle = () => {
    const selectAllClassName = classnames(
      'm-dashed-link',
      list.filter(item => item.get('isSelected')).size === list.size && 's-disabled'
    );

    const deselectAllClassName = classnames(
      'm-dashed-link',
      list.filter(item => item.get('isSelected')).size === 0 && 's-disabled'
    );

    return (
      <div className={`${COMPONENT_STYLE_NAME}__accounts-settings-operations`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-settings-operations-items`}>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-settings-operations-item`}>
            <span className={selectAllClassName} onClick={handleFavoritesSelectAll}>Выбрать все</span>
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-settings-operations-item`}>
            <span className={deselectAllClassName} onClick={handleFavoritesDeselectAll}>Снять выбор</span>
          </div>
        </div>
      </div>
    );
  };

  const renderFavoritesHeaderControls = () => (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-header-controls`}>
      <div className={`${COMPONENT_STYLE_NAME}__card-header-controls-item`}>
        <Icon
          type="close"
          size="16"
          title="Закрыть"
          onClick={onClose}
          locator="indexAccountsButtonSettingsClose"
        />
      </div>
    </div>
  );

  return (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-settings`} data-loc="indexAccountsSettings">
      <div className={`${COMPONENT_STYLE_NAME}__accounts-blocks`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-block`}>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-header`}>
            <CardHeader
              title={renderListHeaderTitle()}
            />
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-content`}>
            <CardContent>
              <div className={`${COMPONENT_STYLE_NAME}__accounts-list`} data-loc="indexAccountsSettingsList">
                <div className={`${COMPONENT_STYLE_NAME}__accounts-list-content`}>
                  {!!favorites.size &&
                  <Sortable
                    items={favorites}
                    onChange={onFavoritesChange}
                  />
                  }
                  {!favorites.size &&
                  <CardNotFound>
                    <p>Чтобы добавить информацию об избранных счетах в эту область экрана,
                      выберите несколько счетов из списка</p>
                  </CardNotFound>
                  }
                </div>
              </div>
            </CardContent>
          </div>
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-block m-settings`}>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-header`}>
            <CardHeader
              borderNoneFlag
              title={renderFavoritesHeaderTitle()}
              controls={renderFavoritesHeaderControls()}
            />
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__accounts-content`}>
            <CardContent>
              <div
                className={`${COMPONENT_STYLE_NAME}__accounts-favorites`}
                data-loc="indexAccountsSettingsFavorites"
              >
                <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-content`}>
                  <div className={`${COMPONENT_STYLE_NAME}__accounts-favorites-items`}>
                    {list.map((item, key) => (
                      <FavoritesItem
                        key={key}
                        id={item.get('id')}
                        accNum={item.get('accNum')}
                        organization={item.get('organization')}
                        isSelected={item.get('isSelected')}
                        onChange={onFavoritesItemChange}
                      />
                    ))}
                  </div>
                </div>
              </div>
            </CardContent>
          </div>
        </div>
      </div>
    </div>
  );
};

HomeAccountsSettings.propTypes = {
  favorites: immutablePropTypes.list.isRequired,
  list: immutablePropTypes.list.isRequired,
  onFavoritesChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default HomeAccountsSettings;
