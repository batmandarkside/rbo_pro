import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../../constants';

const HomeAccountsSettingsListItem = (props) => {
  const { accNum, name, organization } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item-inner`}>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item-content`}>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item-title`}>
          <span className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item-number`}>
            {accNum}
          </span>
          <span className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item-name`}>
            {name}
          </span>
        </div>
        <div className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item-organization`}>
          {organization}
        </div>
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item-icon`}>
        <Icon type="draggable" size="16" />
      </div>
    </div>
  );
};

HomeAccountsSettingsListItem.propTypes = {
  accNum: PropTypes.string,
  name: PropTypes.string,
  organization: PropTypes.string
};

export default HomeAccountsSettingsListItem;
