import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import { is, fromJS } from 'immutable';
import { isEqual } from 'lodash';
import update from 'react/lib/update';
import { DragDropContext } from 'react-dnd';
import TouchBackend from 'react-dnd-touch-backend';
import SortableItem from 'components/ui-components/sortable/item';
import SortableItemPreview from 'components/ui-components/sortable/item-preview';
import { COMPONENT_STYLE_NAME } from '../../../constants';
import ListItem from './list-item';

class HomeAccountsSettingsSortable extends Component {

  static propTypes = {
    items: immutablePropTypes.list.isRequired,
    onChange: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      items: props.items.toJS()
    };

    this.prevStateItems = this.state.items;
  }

  componentWillReceiveProps(nextProps) {
    if (!is(nextProps.items, this.props.items)) {
      this.setState({
        items: nextProps.items.toJS()
      });
    }
  }

  prevStateItems = [];

  moveCard = (dragIndex, hoverIndex) => {
    const { items } = this.state;
    const dragCard = items[dragIndex];
    const itemsUpdate = update(this.state, {
      items: {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, dragCard],
        ],
      },
    });

    this.setState(itemsUpdate);
  };

  endDrag = () => {
    if (!isEqual(this.prevStateItems, this.state.items)) {
      this.prevStateItems = this.state.items;
      this.props.onChange(fromJS(this.state.items).map(item => item.get('id')));
    }
  };

  render() {
    const { items } = this.state;

    return (
      <div
        className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable`}
        id="homeAccountsSettingsListSortable"
      >
        <div className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-items`}>
          {fromJS(items).map((item, key) => (
            <SortableItem
              isCanDrag
              key={item.get('id')}
              id={item.get('id')}
              index={key}
              moveCard={this.moveCard}
              endDrag={this.endDrag}
              className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item`}
            >
              <ListItem
                accNum={item.get('accNum')}
                name={item.get('name')}
                organization={item.get('organization')}
              />
            </SortableItem>
          ))}
        </div>
        <SortableItemPreview
          className={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-preview`}
          itemClassName={`${COMPONENT_STYLE_NAME}__accounts-list-sortable-item`}
          containerId="homeAccountsSettingsListSortable"
        />
      </div>
    );
  }
}

export default DragDropContext(TouchBackend({ enableMouseEvents: true }))(HomeAccountsSettingsSortable);
