import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../../constants';

const HomeCardContent = (props) => {
  const { children } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__card-content`}>
      {children}
    </div>
  );
};

HomeCardContent.propTypes = {
  children: PropTypes.node
};

export default HomeCardContent;
