import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { COMPONENT_STYLE_NAME } from '../../constants';

const HomeCardHeader = (props) => {
  const { title, tabs, controls, borderNoneFlag } = props;

  const elementClassName = classnames(
    `${COMPONENT_STYLE_NAME}__card-header`,
    borderNoneFlag && 'm-border-none'
  );

  return (
    <div className={elementClassName}>
      {title &&
      <div className={`${COMPONENT_STYLE_NAME}__card-header-title`}>
        {title}
      </div>
      }
      {tabs ?
        <div className={`${COMPONENT_STYLE_NAME}__card-header-tabs`}>
          {tabs}
        </div>
        :
        <div className={`${COMPONENT_STYLE_NAME}__card-header-spacer`} />
      }
      {controls &&
      <div className={`${COMPONENT_STYLE_NAME}__card-header-controls`}>
        {controls}
      </div>
      }
    </div>
  );
};

HomeCardHeader.propTypes = {
  borderNoneFlag: PropTypes.bool,
  title: PropTypes.node,
  tabs: PropTypes.node,
  controls: PropTypes.node
};

export default HomeCardHeader;
