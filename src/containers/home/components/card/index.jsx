import React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../../constants';

const HomeCard = (props) => {
  const { children } = props;
  return (
    <div className={`${COMPONENT_STYLE_NAME}__card`}>
      {children}
    </div>
  );
};

HomeCard.propTypes = {
  children: PropTypes.node,
};

export default HomeCard;

export CardHeader from './header';
export CardContent from './content';
export CardNotFound from './not-found';
