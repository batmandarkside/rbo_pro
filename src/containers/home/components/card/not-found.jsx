import  React from 'react';
import PropTypes from 'prop-types';
import { COMPONENT_STYLE_NAME } from '../../constants';

const HomeCardNotFound = ({ children }) => (
  <div className={`${COMPONENT_STYLE_NAME}__card-not-found`}>
    {children}
  </div>
);

HomeCardNotFound.propTypes = {
  children: PropTypes.node
};

export default HomeCardNotFound;
