import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RboFormFieldRadio } from 'components/ui-components/rbo-form-compact';
import { COMPONENT_STYLE_NAME } from '../../constants';

class HomeIncomingMessagesFilterItem extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    selected: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
  };

  onChange = () => this.props.onChange(this.props.value);

  render() {
    const { id, value, title, selected } = this.props;

    return (
      <div className={`${COMPONENT_STYLE_NAME}__card-filters-list-item`}>
        <RboFormFieldRadio
          id={id}
          group="HomeIncomingMessagesFilter"
          title={title}
          value={value}
          isSelected={selected}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default HomeIncomingMessagesFilterItem;
