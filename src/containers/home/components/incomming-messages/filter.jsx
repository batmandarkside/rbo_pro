import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import PopupBox from 'components/ui-components/popup-box';
import { COMPONENT_STYLE_NAME } from '../../constants';
import Item from './filter-item';

class HomeIncomingMessagesFilter extends Component {

  static propTypes = {
    items: immutablePropTypes.list.isRequired,
    openerElement: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
  };

  onChange = (value) => {
    const items = this.props.items.map(item => item.set('selected', item.get('value') === value));
    this.props.onChange(items);
  };

  render() {
    const { items, openerElement, onClose } = this.props;

    return (
      <PopupBox
        openerElement={openerElement}
        onClose={onClose}
        containerElementSelector={`.${COMPONENT_STYLE_NAME}__incoming-messages`}
      >
        <div className={`${COMPONENT_STYLE_NAME}__card-filters-popup`}>
          <div className="b-index-card__filters-list">
            {items.map((item, key) =>
              <Item
                key={key}
                id={`outgoingMessagesFilterItem_${key}`}
                value={item.get('value')}
                title={item.get('title')}
                selected={item.get('selected')}
                onChange={this.onChange}
              />
            )}
          </div>
        </div>
      </PopupBox>
    );
  }
}

export default HomeIncomingMessagesFilter;
