import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import classnames from 'classnames';
import { Link }  from 'react-router-dom';
import Loader from '@rbo/components/lib/loader/Loader';
import Icon from 'components/ui-components/icon';
import Card, { CardHeader, CardContent, CardNotFound } from '../card';
import { COMPONENT_STYLE_NAME } from '../../constants';
import Filter from './filter';
import Item from './item';

class HomeIncomingMessages extends Component {

  static propTypes = {
    isLoading: PropTypes.bool,
    filter: immutablePropTypes.map.isRequired,
    list: immutablePropTypes.list.isRequired,
    linkToAll: PropTypes.string.isRequired,
    onFilterChange: PropTypes.func.isRequired,
    onItemAttachmentClick: PropTypes.func.isRequired,
  };

  state = {
    isFilterOpen: false
  };

  handleOpenFilter = () => {
    this.setState({ isFilterOpen: true });
  };

  handleCloseFilter = () => {
    this.setState({ isFilterOpen: false });
  };

  renderHeaderTitle = () => (
    <h2>Письма из банка</h2>
  );

  renderHeaderControls = () => {
    const { filter } = this.props;

    const settingsElementClassName = classnames(`${COMPONENT_STYLE_NAME}__card-header-controls-item`,
      filter.get('isChanged') && 's-changed'
    );

    return (
      <div className={`${COMPONENT_STYLE_NAME}__card-header-controls`}>
        <div className={settingsElementClassName}>
          <Icon
            type="settings"
            size="16"
            title="Фильтр"
            onClick={this.handleOpenFilter} locator="indexMessagesButtonSettingsOpen"
          />
        </div>
      </div>
    );
  };

  render() {
    const {
      isLoading,
      list,
      filter,
      linkToAll,
      onFilterChange,
      onItemAttachmentClick,
    } = this.props;
    const { isFilterOpen } = this.state;

    return (
      <Card>
        <div className={`${COMPONENT_STYLE_NAME}__incoming-messages`} data-loc="indexMessages">
          <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-header`}>
            <CardHeader
              title={this.renderHeaderTitle()}
              controls={this.renderHeaderControls()}
            />
          </div>
          <div
            className={`${COMPONENT_STYLE_NAME}__card-filters`}
            ref={(element) => { this.filterElement = element; }}
          >
            {isFilterOpen &&
            <Filter
              items={filter.get('items')}
              openerElement={this.filterElement}
              onClose={this.handleCloseFilter}
              onChange={onFilterChange}
            />
            }
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-content`}>
            <CardContent>
              <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list`}>
                <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-content`}>
                  {isLoading && <Loader centered />}
                  {!isLoading && !list.size &&
                  <CardNotFound>
                    <p>Письма из банка отсутствуют</p>
                  </CardNotFound>
                  }
                  {!isLoading && !!list.size &&
                  <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-items`}>
                    {list.map((item, key) => (
                      <Item
                        key={key}
                        id={item.get('id')}
                        link={item.get('link')}
                        isRead={item.get('isRead')}
                        isHasAttachment={item.get('isHasAttachment')}
                        topic={item.get('topic')}
                        date={item.get('date')}
                        receiver={item.get('receiver')}
                        onAttachmentClick={onItemAttachmentClick}
                      />
                    ))}
                  </div>
                  }
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-footer`}>
                  <Link className="m-dashed-link" to={linkToAll}>
                    Все письма
                  </Link>
                </div>
              </div>
            </CardContent>
          </div>
        </div>
      </Card>
    );
  }
}

export default HomeIncomingMessages;
