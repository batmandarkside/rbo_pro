import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import Icon from 'components/ui-components/icon';
import { COMPONENT_STYLE_NAME } from '../../constants';

class HomeIncomingMessagesItem extends Component {

  static propTypes = {
    id: PropTypes.string.isRequired,
    link: PropTypes.string,
    isRead: PropTypes.bool,
    isHasAttachment: PropTypes.bool,
    topic: PropTypes.string,
    date: PropTypes.string,
    receiver: PropTypes.string,
    onAttachmentClick: PropTypes.func.isRequired
  };


  handleAttachmentClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.props.onAttachmentClick(this.props.id);
  };

  render() {
    const { link, isRead, isHasAttachment, topic, date, receiver } = this.props;

    const topicClassName = classnames(
      `${COMPONENT_STYLE_NAME}__incoming-messages-list-item-topic`,
      !isRead && 'm-unread'
    );
    return (
      <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item`}>
        <Link to={link}>
          <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-row`}>
            <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-cell m-main`}>
              <div className={topicClassName}>
                {topic}
              </div>
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-cell m-extra`}>
              {isHasAttachment &&
                <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-attachment`}>
                  <Icon type="attachment" size="16" title="Скачать" onClick={this.handleAttachmentClick} />
                </div>
              }
              <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-date`}>{date}</div>
            </div>
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-row`}>
            <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-cell m-main`}>
              <div className={`${COMPONENT_STYLE_NAME}__incoming-messages-list-item-receiver`}>
                {receiver}
              </div>
            </div>
          </div>
        </Link>
      </div>
    );
  }
}

export default HomeIncomingMessagesItem;
