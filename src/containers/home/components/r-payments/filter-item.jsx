import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RboFormFieldCheckbox } from 'components/ui-components/rbo-form-compact';
import { COMPONENT_STYLE_NAME } from '../../constants';

class HomeRPaymentsFilterItem extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    selected: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
  };

  onChange = ({ fieldValue }) => this.props.onChange(this.props.value, fieldValue);

  render() {
    const { id, title, selected } = this.props;

    return (
      <div className={`${COMPONENT_STYLE_NAME}__card-filters-list-item`}>
        <RboFormFieldCheckbox
          id={id}
          title={title}
          value={selected}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default HomeRPaymentsFilterItem;
