import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import classnames from 'classnames';
import PopupBox from 'components/ui-components/popup-box';
import { COMPONENT_STYLE_NAME } from '../../constants';
import Item from './filter-item';

class HomeRPaymentsFilter extends Component {

  static propTypes = {
    items: immutablePropTypes.list.isRequired,
    openerElement: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
  };

  onChange = (value, selected) => {
    const items = this.props.items.map(item => (item.get('value') === value ? item.set('selected', selected) : item));
    this.props.onChange(items);
  };

  handleSelectAll = () => {
    const { items } = this.props;
    if (items.filter(item => item.get('selected')).size !== items.size) {
      const newItems = items.map(item => item.set('selected', true));
      this.props.onChange(newItems);
    }
  };

  handleDeselectAll = () => {
    const { items } = this.props;
    if (items.filter(item => item.get('selected')).size !== 0) {
      const newItems = items.map(item => item.set('selected', false));
      this.props.onChange(newItems);
    }
  };

  render() {
    const { items, openerElement, onClose } = this.props;

    const selectAllClassName = classnames(
      'm-dashed-link',
      items.filter(item => item.get('selected')).size === items.size && 's-disabled'
    );

    const deselectAllClassName = classnames(
      'm-dashed-link',
      items.filter(item => item.get('selected')).size === 0 && 's-disabled'
    );

    return (
      <PopupBox
        openerElement={openerElement}
        onClose={onClose}
        containerElementSelector={`.${COMPONENT_STYLE_NAME}__r-payments`}
      >
        <div className={`${COMPONENT_STYLE_NAME}__card-filters-popup`}>
          <div className={`${COMPONENT_STYLE_NAME}__card-filters-title`}>
            <h5>Статусы:</h5>
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__card-filters-operations`}>
            <div className={`${COMPONENT_STYLE_NAME}__card-filters-operations-item`}>
              <span
                className={selectAllClassName}
                onClick={this.handleSelectAll}
                data-loc="indexPaymentsFilterCheckAll"
              >
                Выбрать все
              </span>
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__card-filters-operations-item`}>
              <span
                className={deselectAllClassName}
                onClick={this.handleDeselectAll}
                data-loc="indexPaymentsFilterUnCheckAll"
              >
                Снять выбор
              </span>
            </div>
          </div>
          <div className="b-index-card__filters-list">
            {items.map((item, key) =>
              <Item
                key={key}
                id={`rPaymentFilterStatus_${key}`}
                value={item.get('value')}
                title={item.get('value')}
                selected={item.get('selected')}
                onChange={this.onChange}
              />
            )}
          </div>
        </div>
      </PopupBox>
    );
  }
}

export default HomeRPaymentsFilter;

