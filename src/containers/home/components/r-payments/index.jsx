import React, { Component } from 'react';
import PropTypes from 'prop-types';
import immutablePropTypes from 'react-immutable-proptypes';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import Loader from '@rbo/components/lib/loader/Loader';
import Icon from 'components/ui-components/icon';
import Card, { CardHeader, CardContent, CardNotFound } from '../card';
import { COMPONENT_STYLE_NAME } from '../../constants';
import Filter from './filter';
import Item from './item';

class HomeRPayments extends Component {

  static propTypes = {
    isLoading: PropTypes.bool,
    filter: immutablePropTypes.map.isRequired,
    list: immutablePropTypes.list.isRequired,
    linkToAll: PropTypes.string.isRequired,
    onFilterChange: PropTypes.func.isRequired,
    onItemPrintClick: PropTypes.func.isRequired,
    onItemRepeatClick: PropTypes.func.isRequired
  };

  state = {
    isFilterOpen: false
  };

  handleOpenFilter = () => {
    this.setState({ isFilterOpen: true });
  };

  handleCloseFilter = () => {
    this.setState({ isFilterOpen: false });
  };

  renderHeaderTitle = () => (
    <h2>Исходящие платежи</h2>
  );

  renderHeaderControls = () => {
    const { filter } = this.props;

    const settingsElementClassName = classnames(`${COMPONENT_STYLE_NAME}__card-header-controls-item`,
      filter.get('isChanged') && 's-changed'
    );

    return (
      <div className={`${COMPONENT_STYLE_NAME}__card-header-controls`}>
        <div className={settingsElementClassName}>
          <Icon
            type="settings"
            size="16"
            title="Фильтр"
            onClick={this.handleOpenFilter} locator="indexPaymentsButtonSettingsOpen"
          />
        </div>
      </div>
    );
  };

  render() {
    const {
      isLoading,
      list,
      filter,
      linkToAll,
      onFilterChange,
      onItemPrintClick,
      onItemRepeatClick
    } = this.props;
    const { isFilterOpen } = this.state;

    return (
      <Card>
        <div className={`${COMPONENT_STYLE_NAME}__r-payments`} data-loc="indexPayments">
          <div className={`${COMPONENT_STYLE_NAME}__r-payments-header`}>
            <CardHeader
              title={this.renderHeaderTitle()}
              controls={this.renderHeaderControls()}
            />
          </div>
          <div
            className={`${COMPONENT_STYLE_NAME}__card-filters`}
            ref={(element) => { this.filterElement = element; }}
          >
            {isFilterOpen &&
            <Filter
              items={filter.get('statuses')}
              openerElement={this.filterElement}
              onClose={this.handleCloseFilter}
              onChange={onFilterChange}
            />
            }
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__r-payments-content`}>
            <CardContent>
              <div className={`${COMPONENT_STYLE_NAME}__r-payments-list`}>
                <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-content`}>
                  {isLoading && <Loader centered />}
                  {!isLoading && !list.size &&
                    <CardNotFound>
                      <p>Платежи в выбранных статусах отсутствуют</p>
                    </CardNotFound>
                  }
                  {!isLoading && !!list.size &&
                    <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-items`}>
                      {list.map((item, key) => (
                        <Item
                          key={key}
                          id={item.get('id')}
                          link={item.get('link')}
                          status={item.get('status')}
                          description={item.get('description')}
                          amount={item.get('amount')}
                          receiver={item.get('receiver')}
                          onPrintClick={onItemPrintClick}
                          onRepeatClick={onItemRepeatClick}
                        />
                      ))}
                    </div>
                  }
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-footer`}>
                  <Link className="m-dashed-link" to={linkToAll}>
                    Все платежи
                  </Link>
                </div>
              </div>
            </CardContent>
          </div>
        </div>
      </Card>
    );
  }
}

export default HomeRPayments;
