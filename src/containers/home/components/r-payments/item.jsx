import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import Icon from 'components/ui-components/icon';
import { getDocStatusClassNameMode } from 'utils';
import { COMPONENT_STYLE_NAME } from '../../constants';

class HomeRPaymentsItem extends Component {

  static propTypes = {
    id: PropTypes.string.isRequired,
    link: PropTypes.string,
    status: PropTypes.string,
    description: PropTypes.string,
    amount: PropTypes.string,
    receiver: PropTypes.string,
    onPrintClick: PropTypes.func.isRequired,
    onRepeatClick: PropTypes.func.isRequired
  };

  handlePrintClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.props.onPrintClick(this.props.id);
  };

  handleRepeatClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.props.onRepeatClick(this.props.id);
  };


  render() {
    const { link, status, description, amount, receiver } = this.props;

    const statusClassName = classnames(
      `${COMPONENT_STYLE_NAME}__r-payments-list-item-status`,
      getDocStatusClassNameMode(status)
    );

    return (
      <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item`}>
        <Link to={link}>
          <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-row`}>
            <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-cell m-main`}>
              <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-description`}>
                {description}
              </div>
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-cell m-extra`}>
              <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-amount`}>
                {amount}
              </div>
            </div>
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-row`}>
            <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-cell m-main`}>
              <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-receiver`}>
                {receiver}
              </div>
            </div>
            <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-cell m-extra`}>
              <div className={statusClassName}>
                {status}
              </div>
              <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-operations`}>
                <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-operations-item`}>
                  <Icon type="print" size="16" title="Распечатать" onClick={this.handlePrintClick} />
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__r-payments-list-item-operations-item`}>
                  <Icon type="repeat" size="16" title="Повторить" onClick={this.handleRepeatClick} />
                </div>
              </div>
            </div>
          </div>
        </Link>
      </div>
    );
  }
}

export default HomeRPaymentsItem;
