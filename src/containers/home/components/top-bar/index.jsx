import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import { ROUTER_ALIASES } from 'constants';
import { COMPONENT_STYLE_NAME } from '../../constants';

const HomeTopBar = (props) => {
  const { infoCounts } = props;

  return (
    <div className={`${COMPONENT_STYLE_NAME}__top-bar`}>
      <div className={`${COMPONENT_STYLE_NAME}__top-bar-left`}>
        <div className={`${COMPONENT_STYLE_NAME}__top-bar-operations`}>
          <div className={`${COMPONENT_STYLE_NAME}__top-bar-operations-items`}>
            <Link
              className={`${COMPONENT_STYLE_NAME}__top-bar-operations-item`}
              to={ROUTER_ALIASES.R_PAYMENTS.NEW}
            >
              Создать новый платеж
            </Link>
            <Link
              className={`${COMPONENT_STYLE_NAME}__top-bar-operations-item`}
              to={ROUTER_ALIASES.OUTGOING_MESSAGES.NEW}
            >
              Написать письмо в банк
            </Link>
          </div>
        </div>
      </div>
      <div className={`${COMPONENT_STYLE_NAME}__top-bar-right`}>
        <div className={`${COMPONENT_STYLE_NAME}__top-bar-info`} data-loc="indexTopBarInfo">
          <div className={`${COMPONENT_STYLE_NAME}__top-bar-info-title`}>Платежи</div>
          <div className={`${COMPONENT_STYLE_NAME}__top-bar-info-items`}>
            {infoCounts.map((item, i) => (
              <div className={`${COMPONENT_STYLE_NAME}__top-bar-info-item`} key={i}>
                <div className={`${COMPONENT_STYLE_NAME}__top-bar-info-item-title`}>
                  <Link to={item.get('link')}>
                    {item.get('title')}
                  </Link>
                </div>
                <div
                  className={
                    classnames(
                      `${COMPONENT_STYLE_NAME}__top-bar-info-item-count`,
                      item.get('isAttention') ? 'm-attention' : ''
                    )
                  }
                >
                  {item.get('value')}
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__top-bar-info-item-date`}>
                  с {item.get('date')}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

HomeTopBar.propTypes = {
  infoCounts: ImmutablePropTypes.list.isRequired,
};


export default HomeTopBar;
