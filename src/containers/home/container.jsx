import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Loader from '@rbo/components/lib/loader/Loader';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';
import TopBar from './components/top-bar';
import Accounts from './components/accounts';
import RPayments from './components/r-payments';
import IncomingMessages from './components/incomming-messages';

class HomeContainer extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),

    isDataLoading: PropTypes.bool,
    isDataError: PropTypes.bool,
    infoCounts: ImmutablePropTypes.list.isRequired,
    accounts: ImmutablePropTypes.map.isRequired,
    rPayments: ImmutablePropTypes.map.isRequired,
    incomingMessages: ImmutablePropTypes.map.isRequired,

    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    accountsPreviewViewsChangeAction: PropTypes.func.isRequired,
    accountsPreviewCompactListSelectItemAction: PropTypes.func.isRequired,
    accountsPreviewCorpTabsChangeAction: PropTypes.func.isRequired,
    accountsPreviewCorpItemPrintAction: PropTypes.func.isRequired,
    accountsPreviewCorpItemExportAction: PropTypes.func.isRequired,
    accountsPreviewCorpSettingsColumnsChangeAction: PropTypes.func.isRequired,
    accountsPreviewCorpSettingsSummaryShowedChangeAction: PropTypes.func.isRequired,
    accountsSettingsFavoritesChangeAction: PropTypes.func.isRequired,
    accountsRefreshAction: PropTypes.func.isRequired,
    rPaymentsFilterChangeAction: PropTypes.func.isRequired,
    rPaymentsItemPrintAction: PropTypes.func.isRequired,
    rPaymentsItemRepeatAction: PropTypes.func.isRequired,
    incomingMessagesFilterChangeAction: PropTypes.func.isRequired,
    incomingMessagesItemDownloadAttachmentAction: PropTypes.func.isRequired,
  };

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.mountAction(params);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search)
        && nextProps.history.action !== 'REPLACE') {
      this.props.mountAction(nextProps.match.params);
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  render() {
    const {
      isDataLoading,
      isDataError,
      infoCounts,
      accounts,
      accountsPreviewViewsChangeAction,
      accountsPreviewCompactListSelectItemAction,
      accountsPreviewCorpTabsChangeAction,
      accountsPreviewCorpItemPrintAction,
      accountsPreviewCorpItemExportAction,
      accountsPreviewCorpSettingsColumnsChangeAction,
      accountsPreviewCorpSettingsSummaryShowedChangeAction,
      accountsSettingsFavoritesChangeAction,
      accountsRefreshAction,
      rPayments,
      rPaymentsFilterChangeAction,
      rPaymentsItemPrintAction,
      rPaymentsItemRepeatAction,
      incomingMessages,
      incomingMessagesFilterChangeAction,
      incomingMessagesItemDownloadAttachmentAction
    } = this.props;

    return (
      <div className={COMPONENT_STYLE_NAME}>
        {isDataLoading && <Loader centered />}
        {!isDataLoading && !isDataError &&
        <div className={`${COMPONENT_STYLE_NAME}__inner`}>

          <div className={`${COMPONENT_STYLE_NAME}__header`}>
            <TopBar infoCounts={infoCounts} />
          </div>
          <div className={`${COMPONENT_STYLE_NAME}__content`}>
            <div className={`${COMPONENT_STYLE_NAME}__grid`}>
              <div className={`${COMPONENT_STYLE_NAME}__grid-row`}>
                <div className={`${COMPONENT_STYLE_NAME}__grid-cell m-2`}>
                  <Accounts
                    isCompactMovementsLoading={accounts.get('isCompactMovementsLoading')}
                    isCorpListLoading={accounts.get('isCorpListLoading')}
                    views={accounts.get('views')}
                    onPreviewViewToggleItemClick={accountsPreviewViewsChangeAction}
                    compactList={accounts.get('compactList')}
                    compactMovements={accounts.get('compactMovements')}
                    linkToAllCompactMovements={accounts.get('linkToAllCompactMovements')}
                    onPreviewCompactListItemClick={accountsPreviewCompactListSelectItemAction}
                    corpTabs={accounts.get('corpTabs')}
                    corpList={accounts.get('corpList')}
                    corpColumns={accounts.get('corpColumns')}
                    isCorpSummaryShowed={accounts.get('isCorpSummaryShowed')}
                    onPreviewCorpTabClick={accountsPreviewCorpTabsChangeAction}
                    onPreviewCorpItemPrintClick={accountsPreviewCorpItemPrintAction}
                    onPreviewCorpItemExportClick={accountsPreviewCorpItemExportAction}
                    onPreviewCorpSettingsColumnsChange={accountsPreviewCorpSettingsColumnsChangeAction}
                    onPreviewCorpSettingsSummaryShowedChange={accountsPreviewCorpSettingsSummaryShowedChangeAction}
                    settings={accounts.get('settings')}
                    onSettingsFavoritesChange={accountsSettingsFavoritesChangeAction}
                    onSettingsClose={accountsRefreshAction}
                  />
                </div>
              </div>
              <div className={`${COMPONENT_STYLE_NAME}__grid-row`}>
                <div className={`${COMPONENT_STYLE_NAME}__grid-cell m-1`}>
                  <RPayments
                    isLoading={rPayments.get('isLoading')}
                    filter={rPayments.get('filter')}
                    list={rPayments.get('list')}
                    linkToAll={rPayments.get('linkToAll')}
                    onFilterChange={rPaymentsFilterChangeAction}
                    onItemPrintClick={rPaymentsItemPrintAction}
                    onItemRepeatClick={rPaymentsItemRepeatAction}
                  />
                </div>
                <div className={`${COMPONENT_STYLE_NAME}__grid-cell m-1`}>
                  <IncomingMessages
                    isLoading={incomingMessages.get('isLoading')}
                    filter={incomingMessages.get('filter')}
                    list={incomingMessages.get('list')}
                    linkToAll={incomingMessages.get('linkToAll')}
                    onFilterChange={incomingMessagesFilterChangeAction}
                    onItemAttachmentClick={incomingMessagesItemDownloadAttachmentAction}
                  />
                </div>
              </div>
            </div>
          </div>

        </div>
        }
      </div>
    );
  }
}

export default HomeContainer;
