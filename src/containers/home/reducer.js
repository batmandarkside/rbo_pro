import { fromJS } from 'immutable';
import moment from 'moment-timezone';
import { getLastOperationDay } from 'utils';
import { DOC_STATUSES } from 'constants';
import { ACTIONS } from './constants';

const initialInfoCounts = {
  declined: {
    title: 'отвергнутые',
    statuses: [DOC_STATUSES.DECLINED, DOC_STATUSES.INVALID_SIGN, DOC_STATUSES.INVALID_PROPS],
    lastStateChangeDateFrom: getLastOperationDay(),
    value: ''
  },
  toSend: {
    title: 'к отправке',
    statuses: [DOC_STATUSES.SIGNED],
    dateFrom: moment().subtract(10, 'days'),
    value: ''
  },
  toSign: {
    title: 'на подпись',
    statuses: [DOC_STATUSES.CREATED, DOC_STATUSES.IMPORTED, DOC_STATUSES.PARTLY_SIGNED],
    dateFrom: moment().subtract(10, 'days'),
    value: ''
  }
};

const initialAccounts = {
  isCompactMovementsLoading: false,
  isCorpListLoading: false,
  isCorpSummaryShowed: true,
  views: [
    {
      id: 'compact',
      isActive: false
    },
    {
      id: 'corp',
      isActive: true
    }
  ],
  corpTabs: [
    {
      id: 'lastOperationDay',
      value: getLastOperationDay().format(),
      title: getLastOperationDay().format('dddd (D MMMM)'),
      isActive: true
    },
    {
      id: 'today',
      value: moment({ hour: 0, minute: 0, seconds: 0 }).format(),
      title: 'Сегодня',
      isActive: false
    }
  ],
  corpColumns: [
    {
      id: 'account',
      name: 'Номер счёта',
      isVisible: true
    },
    {
      id: 'accountCurrency',
      name: 'Валюта',
      isVisible: true
    },
    {
      id: 'opBalance',
      name: 'Входящий остаток',
      isVisible: true,
      modeClassName: 'text-align-right'
    },
    {
      id: 'turnoverD',
      name: 'Списание',
      isVisible: true,
      modeClassName: 'text-align-right'
    },
    {
      id: 'turnoverC',
      name: 'Зачисление',
      isVisible: true,
      modeClassName: 'text-align-right'
    },
    {
      id: 'closingBalance',
      name: 'Исходящий остаток',
      isVisible: true,
      modeClassName: 'text-align-right',
    },
    {
      id: 'availBalance',
      name: 'Доступно',
      isVisible: true,
      modeClassName: 'text-align-right'
    }
  ],

  list: [],
  favorites: null,
  movements: [],
  statements: []
};

const initialRPayments = {
  isLoading: false,
  filter: {
    statuses: [
      { value: DOC_STATUSES.CREATED },
      { value: DOC_STATUSES.IMPORTED },
      { value: DOC_STATUSES.INVALID },
      { value: DOC_STATUSES.PARTLY_SIGNED },
      { value: DOC_STATUSES.SIGNED },
      { value: DOC_STATUSES.DELIVERED },
      { value: DOC_STATUSES.INVALID_SIGN },
      { value: DOC_STATUSES.INVALID_PROPS },
      { value: DOC_STATUSES.ACCEPTED },
      { value: DOC_STATUSES.DELAYED },
      { value: DOC_STATUSES.RECALLED },
      { value: DOC_STATUSES.QUEUED },
      { value: DOC_STATUSES.DECLINED },
      { value: DOC_STATUSES.EXECUTED }
    ]
  },
  list: []
};

const initialIncomingMessages = {
  isLoading: false,
  filter: {
    items: [
      {
        value: 'all',
        title: 'Все',
        selected: true
      },
      {
        value: 'isMustRead',
        title: 'Непрочитанные',
        selected: false
      }
    ]
  },
  list: []
};

const initialState = fromJS({
  isMounted: false,
  isDataLoading: true,
  isDataError: false,
  infoCounts: initialInfoCounts,
  accounts: initialAccounts,
  rPayments: initialRPayments,
  incomingMessages: initialIncomingMessages
});

export const mount = () => initialState.merge({
  isMounted: true
});

export const unmount = () => initialState.set('isMounted', false);

export const getDataRequest = (state, { settings }) => {
  const settingsRPaymentsFilterSelectedStatuses =
    (settings && settings.rPaymentsFilterSelectedStatuses) || null;
  const rPaymentsFilterStatuses = initialState.getIn(['rPayments', 'filter', 'statuses'])
    .map(item => item.set(
      'selected',
      !settingsRPaymentsFilterSelectedStatuses
      || !!settingsRPaymentsFilterSelectedStatuses.find(v => item.get('value') === v)
    ));

  const settingsIncomingMessagesFilterValue =
    (settings && settings.incomingMessagesFilterValue) || 'all';
  const incomingMessagesFilterItems = initialState.getIn(['incomingMessages', 'filter', 'items'])
    .map(item => item.set(
      'selected',
      item.get('value') === settingsIncomingMessagesFilterValue
    ));

  const accountsFavorites = (settings && settings.accountsFavorites) || null;

  const settingsAccountsPreviewSelectedView =
    (settings && settings.accountsPreviewSelectedView) || 'corp';
  const accountsViews = initialState.getIn(['accounts', 'views'])
    .map(item => item.set(
      'isActive',
      item.get('id') === settingsAccountsPreviewSelectedView
    ));

  const settingsAccountsPreviewCorpSelectedTab =
    (settings && settings.accountsPreviewCorpSelectedTab) || 'lastOperationDay';
  const accountsCorpTabs = initialState.getIn(['accounts', 'corpTabs'])
    .map(item => item.set(
      'isActive',
      item.get('id') === settingsAccountsPreviewCorpSelectedTab
    ));

  const settingsAccountsPreviewCorpSelectedColumns =
    (settings && settings.accountsPreviewCorpSelectedColumns) || null;
  const accountsCorpColumns = initialState.getIn(['accounts', 'corpColumns'])
    .map(item => item.set(
      'isVisible',
      !settingsAccountsPreviewCorpSelectedColumns
      || !!settingsAccountsPreviewCorpSelectedColumns.find(v => item.get('id') === v)
    ));

  const isCorpSummaryShowed =
    settings && settings.accountsPreviewCorpSummaryShowed !== undefined
      ? settings.accountsPreviewCorpSummaryShowed
      : true;

  return state.merge({
    isDataLoading: true,
    accounts: initialState.get('accounts').merge({
      isCorpSummaryShowed,
      views: accountsViews,
      corpTabs: accountsCorpTabs,
      corpColumns: accountsCorpColumns,
      favorites: accountsFavorites
    }),
    rPayments: initialState.get('rPayments').merge({
      filter: {
        statuses: rPaymentsFilterStatuses
      }
    }),
    incomingMessages: initialState.get('incomingMessages').merge({
      filter: {
        items: incomingMessagesFilterItems
      }
    })
  });
};

export const getDataSuccess = (state, { infoCounts, accounts, movements, statements, rPayments, incomingMessages }) =>
  state.merge({
    isDataLoading: false,
    infoCounts,
    accounts: state.get('accounts').merge({ list: accounts, movements, statements }),
    rPayments: state.get('rPayments').merge({ list: rPayments }),
    incomingMessages: state.get('incomingMessages').merge({ list: incomingMessages })
  });

export const getDataFail = state => state.merge({
  isDataError: true,
  isDataLoading: false
});

export const accountsPreviewViewsChange = (state, { id }) => state
  .setIn(['accounts', 'views'], state.getIn(['accounts', 'views'])
    .map(item => item.set('isActive', item.get('id') === id)));

export const accountsPreviewCompactListSelectItem = (state, { id }) => state
  .setIn(['accounts', 'list'], state.getIn(['accounts', 'list'])
    .map(item => item.set('isSelected', item.get('id') === id)));

export const accountsPreviewCompactGetMovementsRequired = state => state
  .setIn(['accounts', 'isCompactMovementsLoading'], true);

export const accountsPreviewCompactGetMovementsSuccess = (state, { movements }) => state
  .setIn(['accounts', 'movements'], fromJS(movements))
  .setIn(['accounts', 'isCompactMovementsLoading'], false);

export const accountsPreviewCompactGetMovementsFail = state => state
  .setIn(['accounts', 'isCompactMovementsLoading'], false);

export const accountsPreviewCorpTabsChange = (state, { tabId }) => state
  .setIn(['accounts', 'corpTabs'], state.getIn(['accounts', 'corpTabs'])
    .map(item => item.set('isActive', item.get('id') === tabId)));

export const accountsPreviewCorpGetStatementsRequest = state => state.setIn(['accounts', 'isCorpListLoading'], true);

export const accountsPreviewCorpGetStatementsSuccess = (state, { statements }) => state
  .setIn(['accounts', 'statements'], fromJS(statements))
  .setIn(['accounts', 'isCorpListLoading'], false);

export const accountsPreviewCorpGetStatementsFail = state => state.setIn(['accounts', 'isCorpListLoading'], false);

export const accountsPreviewCorpSettingsColumnsChange = (state, { columns }) => state
  .setIn(['accounts', 'corpColumns'], columns);

export const accountsPreviewCorpSettingsSummaryShowedChange = (state, { isSummaryShowed }) => state
  .setIn(['accounts', 'isCorpSummaryShowed'], isSummaryShowed);

export const accountsSettingsFavoritesChange = (state, { favorites }) => state
  .setIn(['accounts', 'favorites'], favorites);

export const rPaymentsFilterChange = (state, { statuses }) => state
  .setIn(['rPayments', 'filter', 'statuses'], statuses);

export const rPaymentsGetListRequest = state => state.setIn(['rPayments', 'isLoading'], true);

export const rPaymentsGetListSuccess = (state, { rPayments }) => state
  .setIn(['rPayments', 'list'], fromJS(rPayments))
  .setIn(['rPayments', 'isLoading'], false);

export const rPaymentsGetListFail = state => state.setIn(['rPayments', 'isLoading'], false);

export const incomingMessagesFilterChange = (state, { items }) => state
  .setIn(['incomingMessages', 'filter', 'items'], items);

export const incomingMessagesGetListRequest = state => state.setIn(['incomingMessages', 'isLoading'], true);

export const incomingMessagesGetListSuccess = (state, { incomingMessages }) => state
  .setIn(['incomingMessages', 'list'], fromJS(incomingMessages))
  .setIn(['incomingMessages', 'isLoading'], false);

export const incomingMessagesGetListFail = state => state.setIn(['incomingMessages', 'isLoading'], false);

function homeContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.HOME_CONTAINER_MOUNT:
      return mount();

    case ACTIONS.HOME_CONTAINER_UNMOUNT:
      return unmount();

    case ACTIONS.HOME_CONTAINER_GET_DATA_REQUEST:
      return getDataRequest(state, action.payload);

    case ACTIONS.HOME_CONTAINER_GET_DATA_SUCCESS:
      return getDataSuccess(state, action.payload);

    case ACTIONS.HOME_CONTAINER_GET_DATA_FAIL:
      return getDataFail(state);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_VIEWS_CHANGE:
      return accountsPreviewViewsChange(state, action.payload);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_LIST_SELECT_ITEM:
      return accountsPreviewCompactListSelectItem(state, action.payload);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_GET_MOVEMENTS_REQUIRED:
      return accountsPreviewCompactGetMovementsRequired(state);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_GET_MOVEMENTS_SUCCESS:
      return accountsPreviewCompactGetMovementsSuccess(state, action.payload);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_GET_MOVEMENTS_FAIL:
      return accountsPreviewCompactGetMovementsFail(state);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_TABS_CHANGE:
      return accountsPreviewCorpTabsChange(state, action.payload);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_REQUEST:
      return accountsPreviewCorpGetStatementsRequest(state);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_SUCCESS:
      return accountsPreviewCorpGetStatementsSuccess(state, action.payload);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_FAIL:
      return accountsPreviewCorpGetStatementsFail(state);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_SETTINGS_COLUMNS_CHANGE:
      return accountsPreviewCorpSettingsColumnsChange(state, action.payload);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_SETTINGS_SUMMARY_SHOWED_CHANGE:
      return accountsPreviewCorpSettingsSummaryShowedChange(state, action.payload);

    case ACTIONS.HOME_CONTAINER_ACCOUNTS_SETTINGS_FAVORITES_CHANGE:
      return accountsSettingsFavoritesChange(state, action.payload);

    case ACTIONS.HOME_CONTAINER_R_PAYMENTS_FILTER_CHANGE:
      return rPaymentsFilterChange(state, action.payload);

    case ACTIONS.HOME_CONTAINER_R_PAYMENTS_GET_LIST_REQUEST:
      return rPaymentsGetListRequest(state);

    case ACTIONS.HOME_CONTAINER_R_PAYMENTS_GET_LIST_SUCCESS:
      return rPaymentsGetListSuccess(state, action.payload);

    case ACTIONS.HOME_CONTAINER_R_PAYMENTS_GET_LIST_FAIL:
      return rPaymentsGetListFail(state);

    case ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_FILTER_CHANGE:
      return incomingMessagesFilterChange(state, action.payload);

    case ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_GET_LIST_REQUEST:
      return incomingMessagesGetListRequest(state);

    case ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_GET_LIST_SUCCESS:
      return incomingMessagesGetListSuccess(state, action.payload);

    case ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_GET_LIST_FAIL:
      return incomingMessagesGetListFail(state);

    default:
      return state;
  }
}

export default homeContainerReducer;
