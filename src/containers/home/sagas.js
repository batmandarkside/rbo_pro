import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import { push } from 'react-router-redux';
import { ROUTER_ALIASES } from 'constants';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import {
  getAccounts as dalGetAccounts,
  getMovements as dalGetMovements,
} from 'dal/accounts/sagas';
import {
  getStatements as dalGetStatements,
  printStatements as dalAccountsPreviewCorpItemPrint,
  exportStatementsTo1C as dalAccountsPreviewCorpItemExport,
} from 'dal/statements/sagas';
import {
  getCount as dalGetRPaymentsCounts,
  getPayments as dalGetRPayments,
} from 'dal/r-payments/sagas';
import {
  print as dalRPaymentsItemPrint
} from 'dal/documents/sagas';
import {
  getIncomingMessages as dalGetIncomingMessages,
  getIncomingMessageAttachments as dalGetIncomingMessagesItemAttachments,
  downloadIncomingMessageAttachment as dalDownloadIncomingMessagesItemAttachment
} from 'dal/messages/sagas';
import { ACTIONS, LOCAL_REDUCER, SETTINGS_ALIAS } from './constants';

const profileSettingsSelector = state => state.getIn(['profile', 'settings']);
const infoCountsSelector = state => state.getIn([LOCAL_REDUCER, 'infoCounts']);
const accountsSelector = state => state.getIn([LOCAL_REDUCER, 'accounts']);
const rPaymentsSelector = state => state.getIn([LOCAL_REDUCER, 'rPayments']);
const incomingMessagesSelector = state => state.getIn([LOCAL_REDUCER, 'incomingMessages']);

function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

function* getSettings() {
  const profileSettings = yield select(profileSettingsSelector);
  return (profileSettings && profileSettings.get(SETTINGS_ALIAS))
    ? profileSettings.get(SETTINGS_ALIAS).toJS()
    : null;
}

function* saveSettings(localSettings) {
  try {
    const profileSettings = yield select(profileSettingsSelector);
    const params = { settings: profileSettings.set(SETTINGS_ALIAS, localSettings).toJS() };
    yield call(dalSaveSettings, { payload: params });
  } catch (error) {
    yield call(errorNotification);
  }
}

function* getStatements() {
  try {
    const accounts = yield select(accountsSelector);
    const favoritesAccounts = accounts.get('favorites');
    const day = accounts.get('corpTabs').find(item => item.get('isActive')).get('value');

    const params = {
      offset: 0,
      offsetStep: 31,
      sort: 'receiptDate',
      desc: true,
      account: favoritesAccounts && favoritesAccounts.size ? favoritesAccounts.toJS() : null,
      from: day,
      to: day,
      zero: 1
    };

    if (favoritesAccounts && !favoritesAccounts.size) return [];

    return yield call(dalGetStatements, { payload: params });
  } catch (error) {
    throw error;
  }
}

function* getRPayments() {
  try {
    const rPayments = yield select(rPaymentsSelector);
    const statuses = rPayments.getIn(['filter', 'statuses']);
    const selectedStatuses = statuses.filter(item => item.get('selected'));
    const params = {
      offset: 0,
      offsetStep: 10,
      sort: 'date',
      desc: true,
      status: selectedStatuses.size !== statuses.size
        ? selectedStatuses.map(item => item.get('value')).toJS()
        : null
    };
    if (!selectedStatuses.size) return [];
    return yield call(dalGetRPayments, { payload: params });
  } catch (error) {
    throw error;
  }
}

function* getIncomingMessages() {
  try {
    const incomingMessages = yield select(incomingMessagesSelector);
    const filterItems = incomingMessages.getIn(['filter', 'items']);
    const selectedFilterItem = filterItems.find(item => item.get('selected'));
    const params = {
      offset: 0,
      offsetStep: 10,
      sort: selectedFilterItem.get('value') === 'all' ? 'read' : 'date',
      desc: true,
      isRead: selectedFilterItem.get('value') === 'all' ? '1' : '0'
    };
    return yield call(dalGetIncomingMessages, { payload: params });
  } catch (error) {
    throw error;
  }
}

function* mountSaga() {
  const settings = yield call(getSettings);
  yield put({ type: ACTIONS.HOME_CONTAINER_GET_DATA_REQUEST, payload: { settings } });
}

function* getDataSaga() {
  try {
    const infoCounts = yield select(infoCountsSelector);
    const [
      infoCountDeclined,
      infoCountToSend,
      infoCountToSign,
      accounts,
      statements,
      rPayments,
      incomingMessages
    ] = yield all([
      dalGetRPaymentsCounts({
        payload: {
          statuses: infoCounts.getIn(['declined', 'statuses']).toJS(),
          lastStateChangeDateFrom: infoCounts.getIn(['declined', 'lastStateChangeDateFrom']).format()
        }
      }),
      dalGetRPaymentsCounts({
        payload: {
          statuses: infoCounts.getIn(['toSend', 'statuses']).toJS(),
          dateFrom: infoCounts.getIn(['toSend', 'dateFrom']).format()
        }
      }),
      dalGetRPaymentsCounts({
        payload: {
          statuses: infoCounts.getIn(['toSign', 'statuses']).toJS(),
          dateFrom: infoCounts.getIn(['toSign', 'dateFrom']).format()
        }
      }),
      dalGetAccounts(),
      getStatements(),
      getRPayments(),
      getIncomingMessages()
    ]);

    const settings = yield call(getSettings);
    const accountsFavorites = (settings && settings.accountsFavorites) || null;
    const selectedAccountId = accountsFavorites
      ? accountsFavorites.length && accountsFavorites[0]
      : accounts.length && accounts[0].id;

    const movements = selectedAccountId
      ? yield call(dalGetMovements, { payload: { accId: selectedAccountId, sort: 'operDate', desc: true, offset: 0, offsetStep: 31 } })
      : [];

    yield put({
      type: ACTIONS.HOME_CONTAINER_GET_DATA_SUCCESS,
      payload: {
        infoCounts: infoCounts
          .setIn(['declined', 'value'], infoCountDeclined)
          .setIn(['toSend', 'value'], infoCountToSend)
          .setIn(['toSign', 'value'], infoCountToSign),
        accounts: accounts.map(account => ({
          ...account,
          isSelected: account.id === selectedAccountId
        })),
        movements,
        statements,
        rPayments,
        incomingMessages
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.HOME_CONTAINER_GET_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно получить данные'
    });
  }
}

function* accountsPreviewViewChangeSaga(action) {
  const { payload: { id } } = action;
  const profileSettings = yield select(profileSettingsSelector);
  const localSettings = profileSettings.get(SETTINGS_ALIAS) || fromJS({});
  const newLocalSettings = localSettings.merge({ accountsPreviewSelectedView: id });
  yield saveSettings(newLocalSettings);
}

function* accountsPreviewCompactListSelectItemSaga(action) {
  const { payload: { id } } = action;
  yield put({ type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_GET_MOVEMENTS_REQUIRED, payload: { accId: id } });
}

function* accountsPreviewCompactGetMovementsSaga(action) {
  const { payload: { accId } } = action;
  try {
    const movements = accId
      ? yield call(dalGetMovements, { payload: { accId, sort: 'operDate', desc: true, offset: 0, offsetStep: 31 } })
      : [];
    yield put({
      type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_GET_MOVEMENTS_SUCCESS,
      payload: { movements }
    });
  } catch (error) {
    yield put({ type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_GET_MOVEMENTS_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно получить данные'
    });
  }
}

function* accountsPreviewCorpTabsChangeSaga(action) {
  const { payload: { tabId } } = action;
  yield put({ type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_REQUEST });

  const profileSettings = yield select(profileSettingsSelector);
  const localSettings = profileSettings.get(SETTINGS_ALIAS) || fromJS({});
  const newLocalSettings = localSettings.merge({ accountsPreviewCorpSelectedTab: tabId });
  yield saveSettings(newLocalSettings);
}

function* accountsPreviewCorpGetStatementsSaga() {
  try {
    const statements = yield call(getStatements);

    yield put({
      type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_SUCCESS,
      payload: { statements }
    });
  } catch (error) {
    yield put({ type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно получить данные'
    });
  }
}

function* accountsPreviewCorpSettingsColumnsChangeSaga(action) {
  const { payload: { columns } } = action;

  const accountsPreviewCorpSelectedColumns = columns
    .filter(item => item.get('isVisible'))
    .map(item => item.get('id'));

  const profileSettings = yield select(profileSettingsSelector);
  const localSettings = profileSettings.get(SETTINGS_ALIAS) || fromJS({});
  const newLocalSettings = localSettings.merge({ accountsPreviewCorpSelectedColumns });
  yield saveSettings(newLocalSettings);
}

function* accountsPreviewCorpSettingsSummaryShowedChangeSaga(action) {
  const { payload: { isSummaryShowed } } = action;

  const accountsPreviewCorpSummaryShowed = isSummaryShowed;

  const profileSettings = yield select(profileSettingsSelector);
  const localSettings = profileSettings.get(SETTINGS_ALIAS) || fromJS({});
  const newLocalSettings = localSettings.merge({ accountsPreviewCorpSummaryShowed });
  yield saveSettings(newLocalSettings);
}

export function* accountsPreviewCorpItemPrintSaga(action) {
  const { payload } = action;
  try {
    const result = yield call(dalAccountsPreviewCorpItemPrint, { payload: { id: payload } });
    yield put({ type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_PRINT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* accountsPreviewCorpItemExportSaga(action) {
  const { payload } = action;
  try {
    const result = yield call(dalAccountsPreviewCorpItemExport, { payload: { id: payload } });
    yield put({ type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_EXPORT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_EXPORT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

function* accountsSettingsFavoritesChangeSaga(action) {
  const { payload: { favorites } } = action;

  const profileSettings = yield select(profileSettingsSelector);
  const localSettings = profileSettings.get(SETTINGS_ALIAS) || fromJS({});
  const newLocalSettings = localSettings.merge({ accountsFavorites: favorites });
  yield saveSettings(newLocalSettings);
}

function* accountsRefreshSaga() {
  const accounts = yield select(accountsSelector);
  const favoritesAccounts = accounts.get('favorites');
  if (favoritesAccounts) {
    yield put({
      type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_LIST_SELECT_ITEM,
      payload: { id: favoritesAccounts.size ? favoritesAccounts.get(0) : null }
    });
  }
  yield put({ type: ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_REQUEST });
}

function* rPaymentsFilterChangeSaga(action) {
  const { payload: { statuses } } = action;
  yield put({ type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_GET_LIST_REQUEST });

  const rPaymentsFilterSelectedStatuses = statuses
    .filter(item => item.get('selected'))
    .map(item => item.get('value'));

  const profileSettings = yield select(profileSettingsSelector);
  const localSettings = profileSettings.get(SETTINGS_ALIAS) || fromJS({});
  const newLocalSettings = localSettings.merge({ rPaymentsFilterSelectedStatuses });
  yield saveSettings(newLocalSettings);
}

function* rPaymentsGetListSaga() {
  try {
    const rPayments = yield call(getRPayments);

    yield put({
      type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_GET_LIST_SUCCESS,
      payload: { rPayments }
    });
  } catch (error) {
    yield put({ type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_GET_LIST_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно получить данные'
    });
  }
}

function* rPaymentsItemPrintSaga(action) {
  const { payload: { docId } } = action;
  const rPayments = yield select(rPaymentsSelector);
  const rPaymentsItemData = rPayments.get('list').find(item => item.get('recordID') === docId);
  const docNumber = rPaymentsItemData.get('docNumber');
  const docDate = rPaymentsItemData.get('docDate');
  try {
    yield call(dalRPaymentsItemPrint, { payload: { docTitle: 'payment_order', docId, docNumber, docDate } });
    yield put({ type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_ITEM_PRINT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.HOME_CONTAINER_R_PAYMENTS_ITEM_PRINT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно распечатать документ'
    });
  }
}

function* rPaymentsItemRepeatSaga(action) {
  const { payload: { docId } } = action;
  yield put(push({ pathname: ROUTER_ALIASES.R_PAYMENTS.NEW, search: `?type=repeat&itemId=${docId}` }));
}

function* incomingMessagesFilterChangeSaga(action) {
  const { payload: { items } } = action;
  yield put({ type: ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_GET_LIST_REQUEST });

  const incomingMessagesFilterValue = items
    .find(item => item.get('selected'))
    .get('value');

  const profileSettings = yield select(profileSettingsSelector);
  const localSettings = profileSettings.get(SETTINGS_ALIAS) || fromJS({});
  const newLocalSettings = localSettings.merge({ incomingMessagesFilterValue });
  yield saveSettings(newLocalSettings);
}

function* incomingMessagesGetListSaga() {
  try {
    const incomingMessages = yield call(getIncomingMessages);

    yield put({
      type: ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_GET_LIST_SUCCESS,
      payload: { incomingMessages }
    });
  } catch (error) {
    yield put({ type: ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_GET_LIST_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно получить данные'
    });
  }
}

function* incomingMessagesItemDownloadAttachmentSaga(action) {
  const { payload: { docId } } = action;
  try {
    const attachments = yield call(dalGetIncomingMessagesItemAttachments, { payload: { id: docId } });
    yield all(
      attachments.map(attachment => call(
        dalDownloadIncomingMessagesItemAttachment,
        { payload: { fileId: attachment.id, fileName: attachment.name } }
      ))
    );
    yield put({ type: ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_ITEM_DOWNLOAD_ATTACHMENT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_ITEM_DOWNLOAD_ATTACHMENT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно скачать файлы'
    });
  }
}

export default function* saga() {
  yield takeLatest(ACTIONS.HOME_CONTAINER_MOUNT, mountSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_GET_DATA_REQUEST, getDataSaga);

  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_VIEWS_CHANGE, accountsPreviewViewChangeSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_LIST_SELECT_ITEM, accountsPreviewCompactListSelectItemSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_COMPACT_GET_MOVEMENTS_REQUIRED, accountsPreviewCompactGetMovementsSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_TABS_CHANGE, accountsPreviewCorpTabsChangeSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_GET_STATEMENTS_REQUEST, accountsPreviewCorpGetStatementsSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_SETTINGS_COLUMNS_CHANGE, accountsPreviewCorpSettingsColumnsChangeSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_SETTINGS_SUMMARY_SHOWED_CHANGE, accountsPreviewCorpSettingsSummaryShowedChangeSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_PRINT_REQUEST, accountsPreviewCorpItemPrintSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_PREVIEW_CORP_ITEM_EXPORT_REQUEST, accountsPreviewCorpItemExportSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_SETTINGS_FAVORITES_CHANGE, accountsSettingsFavoritesChangeSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_ACCOUNTS_REFRESH, accountsRefreshSaga);

  yield takeLatest(ACTIONS.HOME_CONTAINER_R_PAYMENTS_FILTER_CHANGE, rPaymentsFilterChangeSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_R_PAYMENTS_GET_LIST_REQUEST, rPaymentsGetListSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_R_PAYMENTS_ITEM_PRINT_REQUEST, rPaymentsItemPrintSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_R_PAYMENTS_ITEM_REPEAT, rPaymentsItemRepeatSaga);

  yield takeLatest(ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_FILTER_CHANGE, incomingMessagesFilterChangeSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_GET_LIST_REQUEST, incomingMessagesGetListSaga);
  yield takeLatest(ACTIONS.HOME_CONTAINER_INCOMING_MESSAGES_ITEM_DOWNLOAD_ATTACHMENT_REQUEST, incomingMessagesItemDownloadAttachmentSaga);
}
