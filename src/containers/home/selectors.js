import { createSelector } from 'reselect';
import moment from 'moment-timezone';
import numeral from 'numeral';
import { fromJS } from 'immutable';
import { getFormattedAccNum } from 'utils';
import {
  getLinkForInfoCount,
  getLinkForAllMovements,
  getLinkForStatementsItem,
  getLinkForAllRPayments,
  getLinkForRPaymentsItem,
  getLinkForAllIncomingMessages,
  getLinkForIncomingMessagesItem
} from './utils';
import { LOCAL_REDUCER } from './constants';

const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
const isDataErrorSelector = state => state.getIn([LOCAL_REDUCER, 'isDataError']);
const isDataLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isDataLoading']);
const infoCountsSelector = state => state.getIn([LOCAL_REDUCER, 'infoCounts']);
const accountsSelector = state => state.getIn([LOCAL_REDUCER, 'accounts']);
const rPaymentsSelector = state => state.getIn([LOCAL_REDUCER, 'rPayments']);
const incomingMessagesSelector = state => state.getIn([LOCAL_REDUCER, 'incomingMessages']);

const infoCountsCreatedSelector = createSelector(
  infoCountsSelector,
  (infoCounts) => {
    const result = Object.keys(infoCounts.toJS()).map((key) => {
      const value = parseInt(infoCounts.getIn([key, 'value']), 10) || 0;
      const date = infoCounts.getIn([key, 'dateFrom']) || infoCounts.getIn([key, 'lastStateChangeDateFrom']);
      return {
        id: key,
        value,
        link: getLinkForInfoCount(infoCounts.get(key)),
        isAttention: key === 'declined' && !!parseInt(infoCounts.getIn([key, 'value']), 10),
        title: infoCounts.getIn([key, 'title']),
        date: date ? moment(date).format('DD.MM.YYYY') : null
      };
    });

    return fromJS(result);
  }
);

const accountsCreatedSelector = createSelector(
  isDataLoadingSelector,
  accountsSelector,
  organizationsSelector,
  (isDataLoading, accounts, organizations) => {
    if (isDataLoading) return fromJS({});

    const list = accounts.get('list');
    const favorites = accounts.get('favorites') || list.map(account => account.get('id'));
    const compactList = favorites.map((item) => {
      const account = list.find(a => a.get('id') === item);
      return fromJS({
        id: account.get('id'),
        isSelected: account.get('isSelected'),
        isBlocked: account.get('accType') === '0',
        isClosed: account.get('accStatus') === '0',
        accNum: getFormattedAccNum(account.get('accNum')),
        name: ` / ${account.get('currCodeIso')}${account.get('name') ? ` (${account.get('name')})` : ''}`,
        organization: account.get('orgName'),
        currentBalance: numeral(account.get('currentBalance')).format('0,0.00'),
        balanceDate: moment(account.get('balanceDate') && account.get('balanceDate')).format('D MMMM YYYY'),
        balance: numeral(account.get('rest')).format('0,0.00'),
      });
    });

    const selectedAccount = list.find(item => item.get('isSelected'));
    const linkToAllCompactMovements = getLinkForAllMovements(selectedAccount);

    const movements = accounts.get('movements');
    const compactMovements = movements.map(item => item.get('operDate')).toOrderedSet().toList()
      .map(operDate => fromJS({
        title: moment(operDate).format('D MMMM YYYY'),
        items: movements
          .filter(item => item.get('operDate') === operDate)
          .sort((a, b) => (a.get('credit') - a.get('debet')) > (b.get('credit') - b.get('debet')))
          .map(item => fromJS({
            isCredit: !!item.get('credit'),
            credit: numeral(item.get('credit')).format('+0,0.00'),
            debit: numeral(-item.get('debet')).format('+0,0.00'),
            name: item.get('corrName'),
            purpose: item.get('paymentPurpose'),
            account: `Счет № ${getFormattedAccNum(item.get('corrAccount'))}`,
            number: `№ ${item.get('operNumber')}`,
            inn: `ИНН ${item.get('inn')}`
          }))
      }));

    const statements = accounts.get('statements');
    const corpList = statements.map(item => item.get('orgId')).toOrderedSet().toList()
      .map((orgId) => {
        const groupStatements = statements.filter(item => item.get('orgId') === orgId);
        return fromJS({
          title: organizations.find(o => o.get('id') === orgId).get('shortName'),
          blocks: groupStatements.map(item => item.get('natCurrency')).toOrderedSet().toList()
            .map((natCurrency) => {
              const blockItems = groupStatements.filter(item => item.get('natCurrency') === natCurrency);
              const opBalance = blockItems.reduce((result, item) => (result + item.get('opBalance', 0)), 0);
              const turnoverD = blockItems.reduce((result, item) => (result + item.get('turnoverD', 0)), 0);
              const turnoverC = blockItems.reduce((result, item) => (result + item.get('turnoverC', 0)), 0);
              const closingBalance = blockItems.reduce((result, item) => (result + item.get('closingBalance', 0)), 0);
              const availBalance = blockItems.reduce((result, item) => (result + item.get('availBalance', 0)), 0);
              return fromJS({
                items: blockItems.map(item => fromJS({
                  id: item.get('recordID'),
                  link: getLinkForStatementsItem(item),
                  isBlocked: item.get('accountStatus') === 'BLOCKED',
                  isClosed: item.get('accountStatus') === 'CLOSED',
                  data: {
                    account: getFormattedAccNum(item.get('account')),
                    accountCurrency: item.get('natCurrency'),
                    opBalance: item.get('opBalance') ? numeral(item.get('opBalance')).format('0,0.00') : '—',
                    turnoverD: numeral(item.get('turnoverD')).format('0,0.00'),
                    turnoverC: numeral(item.get('turnoverC')).format('0,0.00'),
                    closingBalance: item.get('closingBalance') ? numeral(item.get('closingBalance')).format('0,0.00') : '—',
                    availBalance: item.get('availBalance') ? numeral(item.get('availBalance')).format('0,0.00') : '—'
                  }
                })),
                summary: {
                  account: 'Итого:',
                  opBalance: opBalance ? numeral(opBalance).format('0,0.00') : '—',
                  turnoverD: numeral(turnoverD).format('0,0.00'),
                  turnoverC: numeral(turnoverC).format('0,0.00'),
                  closingBalance: closingBalance ? numeral(closingBalance).format('0,0.00') : '—',
                  availBalance: availBalance ? numeral(availBalance).format('0,0.00') : '—'
                }
              });
            })
        });
      });

    const settingsFavorites = favorites.map((item) => {
      const account = list.find(a => a.get('id') === item);
      return fromJS({
        id: account.get('id'),
        accNum: getFormattedAccNum(account.get('accNum')),
        name: ` / ${account.get('currCodeIso')}${account.get('name') ? ` (${account.get('name')})` : ''}`,
        organization: account.get('orgName')
      });
    });

    const settingsList = list.map(item => fromJS({
      id: item.get('id'),
      isSelected: !!favorites.find(a => a === item.get('id')),
      accNum: getFormattedAccNum(item.get('accNum')),
      organization: item.get('orgName')
    }));

    return fromJS({
      views: accounts.get('views'),
      isCompactMovementsLoading: accounts.get('isCompactMovementsLoading'),
      isCorpListLoading: accounts.get('isCorpListLoading'),
      isCorpSummaryShowed: accounts.get('isCorpSummaryShowed'),
      compactList,
      compactMovements,
      linkToAllCompactMovements,
      corpTabs: accounts.get('corpTabs'),
      corpList,
      corpColumns: accounts.get('corpColumns'),
      settings: {
        favorites: settingsFavorites,
        list: settingsList
      }
    });
  }
);

const rPaymentsCreatedSelector = createSelector(
  rPaymentsSelector,
  (rPayments) => {
    const linkToAll = getLinkForAllRPayments(rPayments);
    const list = rPayments.get('list').map(item => fromJS({
      id: item.get('recordID'),
      link: getLinkForRPaymentsItem(item),
      status: item.get('status'),
      description: `${item.get('docNumber') 
        ? `№${item.get('docNumber')} ` 
        : ''} ${item.get('docDate') 
        ? `от ${moment(item.get('docDate')).format('D MMMM YYYY')}` 
        : ''}`,
      amount: `${item.get('amount') ? numeral(item.get('amount')).format('0,0.00') : '0.00'} RUB`,
      receiver: item.get('receiverName')
    }));
    const rPaymentsFilterStatuses = rPayments.getIn(['filter', 'statuses']);
    const rPaymentsFilterSelectedStatuses = rPaymentsFilterStatuses.filter(item => item.get('selected'));
    const isFilterChanged = rPaymentsFilterStatuses.size !== rPaymentsFilterSelectedStatuses.size;

    return rPayments.merge({
      linkToAll,
      list,
      filter: rPayments.get('filter').set('isChanged', isFilterChanged)
    });
  }
);

const incomingMessagesCreatedSelector = createSelector(
  incomingMessagesSelector,
  organizationsSelector,
  (incomingMessages, organizations) => {
    const linkToAll = getLinkForAllIncomingMessages();
    const list = incomingMessages.get('list').map((item) => {
      const receivers = item.get('receivers');
      const receiver = organizations.find(o => o.get('id') === receivers.get(0));
      const orgShortName = receivers.size > 1 ? 'Несколько получателей' : receiver && receiver.get('shortName');
      return fromJS({
        id: item.get('docId'),
        link: getLinkForIncomingMessagesItem(item),
        isRead: item.get('read') === '1',
        isHasAttachment: item.get('attachExists') === '1',
        topic: item.get('topic'),
        date: item.get('date') ? moment(item.get('date')).format('DD.MM.YYYY') : '',
        receiver: orgShortName
      });
    });
    const incomingMessagesFilterItems = incomingMessages.getIn(['filter', 'items']);
    const incomingMessagesFilterSelectedItem = incomingMessagesFilterItems.find(item => item.get('selected'));
    const isFilterChanged = incomingMessagesFilterSelectedItem.get('value') !== 'all';

    return incomingMessages.merge({
      linkToAll,
      list,
      filter: incomingMessages.get('filter').set('isChanged', isFilterChanged)
    });
  }
);

const mapStateToProps = state => ({
  isDataError: isDataErrorSelector(state),
  isDataLoading: isDataLoadingSelector(state),
  infoCounts: infoCountsCreatedSelector(state),
  accounts: accountsCreatedSelector(state),
  rPayments: rPaymentsCreatedSelector(state),
  incomingMessages: incomingMessagesCreatedSelector(state)
});

export default mapStateToProps;
