import qs from 'qs';
import moment from 'moment-timezone';
import { ROUTER_ALIASES } from 'constants';

export const getLinkForInfoCount = (count) => {
  const filter = {
    conditions: [
      {
        id: 'status',
        params: count.get('statuses').toJS()
      },
      {
        id: count.get('dateFrom') ? 'date' : 'lastStateChangeDate',
        params: {
          dateFrom: moment(count.get('dateFrom') || count.get('lastStateChangeDateFrom')).format(),
        }
      }
    ]
  };
  return `${ROUTER_ALIASES.R_PAYMENTS.INDEX}?${qs.stringify({ filter })}`;
};

export const getLinkForAllMovements = (selectedAccount) => {
  const filter = selectedAccount
    ? {
      conditions: [
        {
          id: 'account',
          params: selectedAccount.get('accNum')
        },
      ]
    }
    : null;
  return `${ROUTER_ALIASES.STATEMENTS.MOVEMENTS}${filter ? `?${qs.stringify({ filter })}` : ''}`;
};

export const getLinkForStatementsItem = (item) => {
  const params = {
    account: item.get('account'),
    date: item.get('stateDate'),
    final: item.get('stateType') ? 0 : 1
  };
  return `${ROUTER_ALIASES.STATEMENTS.ITEM.replace('{:id}', item.get('recordID'))}?${qs.stringify(params)}`;
};

export const getLinkForAllRPayments = (rPayments) => {
  const filterStatuses = rPayments.getIn(['filter', 'statuses']);
  const selectedFilterStatuses = filterStatuses.filter(item => item.get('selected'));
  const filter = selectedFilterStatuses.size && selectedFilterStatuses.size !== filterStatuses.size
    ? {
      conditions: [
        {
          id: 'status',
          params: selectedFilterStatuses.map(item => item.get('value')).toJS()
        },
      ]
    }
    : null;
  return `${ROUTER_ALIASES.R_PAYMENTS.INDEX}${filter ? `?${qs.stringify({ filter })}` : ''}`;
};

export const getLinkForRPaymentsItem = item =>
  ROUTER_ALIASES.R_PAYMENTS.ITEM.replace('{:id}', item.get('recordID'));

export const getLinkForAllIncomingMessages = () => ROUTER_ALIASES.INCOMING_MESSAGES.INDEX;

export const getLinkForIncomingMessagesItem = item =>
  ROUTER_ALIASES.INCOMING_MESSAGES.ITEM.replace('{:id}', item.get('docId'));
