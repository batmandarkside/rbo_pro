import { ACTIONS } from './constants';

/**
 *    action загрузки контейнера
 */
export const mountAction = params => ({
  type: ACTIONS.INCOMING_MESSAGE_CONTAINER_MOUNT,
  payload: { params }
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.INCOMING_MESSAGE_CONTAINER_UNMOUNT
});

/**
 *    action выполнения операции
 *    @param {string} operationId
 */
export const operationAction = operationId => ({
  type: ACTIONS.INCOMING_MESSAGE_CONTAINER_OPERATION,
  payload: { operationId }
});

/**
 *    action скачивание прикрепленного файла
 */
export const downloadAttachmentAction = fileId => ({
  type: ACTIONS.INCOMING_MESSAGE_CONTAINER_DOWNLOAD_ATTACHMENT_REQUEST,
  payload: { fileId }
});
