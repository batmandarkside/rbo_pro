import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Loader from '@rbo/components/lib/loader/Loader';
import RboDocument, {
  RboDocumentPanel,
  RboDocumentOperations,
  RboDocumentContent,
  RboDocumentMain,
  RboDocumentHead,
  RboDocumentHeadStatus,
  RboDocumentBody,
  RboDocumentInfo,
  RboDocumentExtra
} from 'components/ui-components/rbo-document-compact';
import RboForm from 'components/ui-components/rbo-form-compact';
import { SectionAttachments } from './sections';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';

class IncomingMessageContainer extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),

    isDataError: PropTypes.bool,
    isDataLoading: PropTypes.bool,

    documentValues: PropTypes.instanceOf(Map).isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    documentAttachmentsTotalSize: PropTypes.string.isRequired,

    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    downloadAttachmentAction: PropTypes.func.isRequired
  };

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.mountAction(params);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search)
        && nextProps.history.action !== 'REPLACE') {
      this.props.mountAction(nextProps.match.params);
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  render() {
    const {
      isDataError,
      isDataLoading,
      documentValues,
      operations,
      operationAction,
      downloadAttachmentAction,
      documentAttachmentsTotalSize
    } = this.props;

    return (
      <div className={COMPONENT_STYLE_NAME}>
        <div className={`${COMPONENT_STYLE_NAME}__inner`}>
          {isDataLoading && <Loader centered />}
          {!isDataLoading && !isDataError &&
          <RboDocument>
            <RboDocumentPanel>
              <RboDocumentOperations
                operations={operations}
                operationAction={operationAction}
              />
            </RboDocumentPanel>
            <RboDocumentContent>
              <RboDocumentMain>
                <RboDocumentHead title={documentValues.get('title')}>
                  <RboDocumentHeadStatus
                    date={documentValues.get('docDate')}
                  />
                </RboDocumentHead>
                <RboDocumentBody>
                  <RboDocumentInfo items={documentValues.get('mainInfo')} />
                  {documentValues.get('attaches') && !!documentValues.get('attaches').size &&
                  <RboForm>
                    <SectionAttachments
                      documentValues={documentValues}
                      onFileDownload={downloadAttachmentAction}
                      attachmentsTotalSize={documentAttachmentsTotalSize}
                    />
                  </RboForm>
                  }
                </RboDocumentBody>
              </RboDocumentMain>
              <RboDocumentExtra />
            </RboDocumentContent>
          </RboDocument>
          }
        </div>
      </div>
    );
  }
}

export default IncomingMessageContainer;
