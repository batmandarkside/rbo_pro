import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

const initialDictionaries = {
  organizations: {
    items: []
  }
};

const initialOperations = [
  {
    id: 'back',
    title: 'Назад',
    icon: 'back',
    disabled: false,
    progress: false
  }
];

const initialState = fromJS({
  isMounted: false,
  isDataError: false,
  isDataLoading: true,
  documentData: {
    attaches: []
  },
  documentDictionaries: initialDictionaries,
  operations: initialOperations
});

export const mount = () => initialState.set('isMounted', true);

export const unmount = () => initialState.set('isMounted', false);

export const getDataRequest = state => state.merge({
  isDataLoading: true
});

export const getDataSuccess = (state, { documentData, documentDictionaries }) => state.merge({
  isDataLoading: false,
  documentData,
  documentDictionaries
});

export const getDataFail = state => state.merge({
  isDataError: true,
  isDataLoading: false
});

function IncomingMessageContainerReducer(state = initialState, action) {
  const isMounted = state.get('isMounted');

  switch (action.type) {
    case ACTIONS.INCOMING_MESSAGE_CONTAINER_MOUNT:
      return mount();

    case ACTIONS.INCOMING_MESSAGE_CONTAINER_UNMOUNT:
      return unmount();

    case ACTIONS.INCOMING_MESSAGE_CONTAINER_GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.INCOMING_MESSAGE_CONTAINER_GET_DATA_SUCCESS:
      return isMounted ? getDataSuccess(state, action.payload) : state;

    case ACTIONS.INCOMING_MESSAGE_CONTAINER_GET_DATA_FAIL:
      return isMounted ? getDataFail(state) : state;

    default:
      return state;
  }
}

export default IncomingMessageContainerReducer;
