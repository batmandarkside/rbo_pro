import { call, put, select, takeLatest } from 'redux-saga/effects';
import { goBack, replace } from 'react-router-redux';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import {
  getIncomingMessageData as dalGetData,
  getIncomingMessageAttachments as dalGetAttachments,
  downloadIncomingMessageAttachment as dalDownloadAttachment,
} from 'dal/messages/sagas';
import { ACTIONS, LOCAL_REDUCER, LOCAL_ROUTER_ALIASES } from './constants';

export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);

export function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

export function* mountSaga(action) {
  const { payload: { params: { id } } } = action;
  yield put({ type: ACTIONS.INCOMING_MESSAGE_CONTAINER_GET_DATA_REQUEST, payload: { id } });
}

export function* getDataSaga(action) {
  const { payload: { id } } = action;

  try {
    const organizations = yield select(organizationsSelector);
    const documentData = yield call(dalGetData, { payload: { id } });

    documentData.attaches = parseInt(documentData.attachExists, 10)
      ? yield call(dalGetAttachments, { payload: { id } })
      : [];

    yield put({
      type: ACTIONS.INCOMING_MESSAGE_CONTAINER_GET_DATA_SUCCESS,
      payload: {
        documentData,
        documentDictionaries: {
          organizations: { items: organizations }
        }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.INCOMING_MESSAGE_CONTAINER_GET_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Документ не найден.',
      message: 'Неверно указан идентификатор документа.'
    });
  }
}

export function* operationSaga(action) {
  const { payload: { operationId } } = action;
  switch (operationId) {
    case 'back':
      yield put({ type: ACTIONS.INCOMING_MESSAGE_CONTAINER_OPERATION_BACK });
      break;
    default:
      break;
  }
}

export function* downloadAttachmentSaga(action) {
  const { payload: { fileId } } = action;
  const documentData = yield select(documentDataSelector);
  try {
    const fileName = documentData.get('attaches').find(attachment => attachment.get('id') === fileId).get('name');
    yield call(dalDownloadAttachment, { payload: { fileId, fileName } });
    yield put({ type: ACTIONS.INCOMING_MESSAGE_CONTAINER_DOWNLOAD_ATTACHMENT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.INCOMING_MESSAGE_CONTAINER_DOWNLOAD_ATTACHMENT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно скачать файл'
    });
  }
}

export function* operationBackSaga() {
  const location = yield select(routingLocationSelector);
  const prevLocation = location.get('prevLocation');
  if (prevLocation && prevLocation.get('pathname') !== location.get('pathname')) yield put(goBack());
  else yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
}

export default function* saga() {
  yield takeLatest(ACTIONS.INCOMING_MESSAGE_CONTAINER_MOUNT, mountSaga);
  yield takeLatest(ACTIONS.INCOMING_MESSAGE_CONTAINER_GET_DATA_REQUEST, getDataSaga);

  yield takeLatest(ACTIONS.INCOMING_MESSAGE_CONTAINER_OPERATION, operationSaga);
  yield takeLatest(ACTIONS.INCOMING_MESSAGE_CONTAINER_OPERATION_BACK, operationBackSaga);

  yield takeLatest(ACTIONS.INCOMING_MESSAGE_CONTAINER_DOWNLOAD_ATTACHMENT_REQUEST, downloadAttachmentSaga);
}
