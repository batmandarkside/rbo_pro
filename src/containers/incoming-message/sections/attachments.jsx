import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldFiles
} from 'components/ui-components/rbo-form-compact';

const IncomingMessageSectionAttachments = (props) => {
  const {
    documentValues,
    attachmentsTotalSize,
    onFileDownload
  } = props;

  const sectionNote = attachmentsTotalSize ? `Общий объем: ${attachmentsTotalSize}` : '';

  return (
    <RboFormSection
      id="attachments"
      title="Вложения"
      note={sectionNote}
    >
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldFiles
                  id="attaches"
                  values={documentValues.get('attaches')}
                  onFileDownload={onFileDownload}
                  isDisabled
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

IncomingMessageSectionAttachments.propTypes = {
  documentValues: ImmutablePropTypes.map.isRequired,
  attachmentsTotalSize: PropTypes.string,
  onFileDownload: PropTypes.func.isRequired
};

export default IncomingMessageSectionAttachments;
