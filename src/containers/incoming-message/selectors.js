import { createSelector } from 'reselect';
import { Map } from 'immutable';
import moment from 'moment-timezone';
import {
  getMimeTypeTitleByMimeType,
  getFormattedFileSize,
  getFormattedFilesTotalSize
} from 'utils';
import { LOCAL_REDUCER } from './constants';
import {
  getMainInfo
} from './utils';

const isDataErrorSelector = state => state.getIn([LOCAL_REDUCER, 'isDataError']);
const isDataLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isDataLoading']);
const isEditableSelector = state => state.getIn([LOCAL_REDUCER, 'isEditable']);
const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);

const documentValuesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  isEditableSelector,
  (documentData, documentDictionaries) => {
    const docNumber = documentData.get('docNumber') || '';
    const docDate = documentData.get('date') ? moment(documentData.get('date')).format('DD.MM.YYYY') : '';
    const title = `Письмо из банка${docNumber ? ` № ${docNumber}` : ''}`;
    return Map({
      id: documentData.get('recordID'),
      title,
      docDate,
      mainInfo: getMainInfo(documentData, documentDictionaries),
      attaches: documentData.get('attaches')
        .map((attachment) => {
          const mimeTypeTitle = getMimeTypeTitleByMimeType(attachment.get('mimeType'));
          const size = attachment.get('size') ? getFormattedFileSize(attachment.get('size')) : '';
          const created = attachment.get('date') ? `создан: ${moment(attachment.get('date')).format('DD.MM.YYYY')}` : '';
          const describe = `${mimeTypeTitle}${size ? `, ${size}` : ''}${created ? `, ${created}` : ''}`;
          return attachment.merge({ describe, isDownloadable: true });
        })
    });
  }
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  operations => operations.filter((operation) => {
    const operationId = operation.get('id');
    switch (operationId) {
      case 'back':
        return true;
      default:
        return false;
    }
  })
);

const documentAttachmentsTotalSizeCreatedSelector = createSelector(
  documentDataSelector,
  documentData => getFormattedFilesTotalSize(documentData.get('attaches').map(attach => attach.get('size')))
);

const mapStateToProps = state => ({
  isDataError: isDataErrorSelector(state),
  isDataLoading: isDataLoadingSelector(state),
  isEditable: isEditableSelector(state),
  documentValues: documentValuesCreatedSelector(state),
  operations: operationsCreatedSelector(state),
  documentAttachmentsTotalSize: documentAttachmentsTotalSizeCreatedSelector(state)
});

export default mapStateToProps;
