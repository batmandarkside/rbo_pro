import moment from 'moment-timezone';
import { getFormattedAccNum } from 'utils';

export const getMainInfo = (documentData, documentDictionaries) => {
  const result = [];
  result.push({ label: 'Номер', value: documentData.get('docNumber') || '—' });
  result.push({
    label: 'Дата',
    value: documentData.get('date') ? moment(documentData.get('date')).format('DD.MM.YYYY') : '—'
  });
  result.push({
    label: 'Счет',
    value: getFormattedAccNum(documentData.get('account')) || '—'
  });
  result.push({ label: 'От кого', value: documentData.get('sender') || '—' });
  const receivers = documentData.get('receivers');
  if (receivers) {
    let receiver;
    if (receivers.size > 1) {
      receiver = 'Несколько получателей';
    } else {
      const organization = documentDictionaries.getIn(['organizations', 'items'])
        .find(o => o.get('id') === receivers.get(0));
      receiver = organization ? organization.get('shortName') : '';
    }
    result.push({ label: 'Кому', value: receiver || '—' });
  }
  result.push({
    label: 'Тип письма',
    value: documentData.get('messageType') || '—'
  });
  result.push({
    label: 'Тема',
    value: documentData.get('topic') || '—'
  });
  result.push({
    label: 'Сообщение',
    value: documentData.get('message') || '—'
  });

  return result;
};
