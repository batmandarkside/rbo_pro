import { fromJS } from 'immutable';
import {
  initialFiltersConditions,
  initialFilters,
  initialPages,
  initialState,
  getSectionIndexSuccess,
  getSettingsSuccess,
  getQueryParams,
  changeSection,
  reloadPagesRequest,
  changeFilters,
  getDictionaryForFilterFail,
  saveSettings,
  getListRequest,
  getListSuccess,
  exportListRequest,
  exportListFinish,
  unmountRequest,
  resizeColumns,
  changeColumn,
  changeGrouping,
  changeSorting,
  changePagination
} from '../reducer';

describe('IncomingMessagesContainer Reducer', () => {
  const state = initialState;

  it('getSectionIndexSuccess', () => {
    const reducer = getSectionIndexSuccess(state, 2);

    const selectedSectionId = initialState.get('sections').find((section, key) => key === 2).get('id');

    expect(reducer).toEqual(
      initialState.merge({
        sections: state.get('sections').map((section, key) => section.set('selected', key === 2)),
        filtersConditions: fromJS(initialFiltersConditions)
          .filter(item => item.get('sections').find(section => section === selectedSectionId))
          .map(item => item.remove('sections')),
        filters: fromJS(initialFilters)
          .filter(item => item.get('sections').find(section => section === selectedSectionId))
      })
    );
  });

  it('getSettingsSuccess', () => {
    const settings = {
      visible: ['date'],
      sorting: { id: 'date', direction: 1 },
      grouped: 'date',
      pagination: 60
    };

    const reducer = getSettingsSuccess(state, settings);

    expect(reducer).toEqual(
      initialState.merge({
        settings: fromJS(settings)
      })
    );
  });

  it('getQueryParams', () => {
    const params = {
      pageIndex: 0,
      filter: null
    };

    const reducer = getQueryParams(state, params);

    expect(reducer).toEqual(
      initialState.merge({
        isListUpdateRequired: true
      })
    );
  });

  it('changeSection', () => {
    const reducer = changeSection(state, 1);

    expect(reducer).toEqual(
      initialState
    );
  });

  it('reloadPagesRequest', () => {
    const reducer = reloadPagesRequest(state);

    expect(reducer).toEqual(
      initialState.merge({
        isReloadRequired: false
      })
    );
  });

  it('changeFilters', () => {
    const filters = initialFilters;

    const reducer = changeFilters(state, { filters, isReloadRequired: false });

    expect(reducer).toEqual(
      initialState.merge({
        filters,
        isReloadRequired: false
      })
    );
  });

  it('getDictionaryForFilterFail', () => {
    const reducer = getDictionaryForFilterFail(state);

    expect(reducer).toEqual(state);
  });

  it('saveSettings', () => {
    const settings = {
      visible: ['number'],
      sorting: { id: 'number', direction: 1 },
      grouped: 'number',
      pagination: 60
    };

    const reducer = saveSettings(state, settings);

    expect(reducer).toEqual(
      initialState.merge({
        settings: fromJS(settings)
      })
    );
  });

  it('getListRequest', () => {
    const reducer = getListRequest(state);

    expect(reducer).toEqual(
      initialState.merge({
        list: fromJS([]),
        isListLoading: true,
        isListUpdateRequired: false
      })
    );
  });

  it('getListSuccess', () => {
    const list = [
      {
        status: 'Ошибка контроля',
        amount: 1,
        receiverName: 'Получатель',
        receiverAccount: '40125810864561024956',
        paymentPurpose: 'Назначение платежа',
        payerAccount: '40702810600001489171',
        recordID: '211a8444-d16a-4d55-b14e-6eade8ff8f7a',
        docDate: '2017-01-01T00:00:00+0300',
        docNumber: '1',
        customerBankRecordID: 'a7468808-5048-4d99-b9ad-0557e06b5410',
        payerName: 'Плательщик',
        payerBankBIC: '044525700',
        bankMessage: 'Сообщение из банка',
        paymentKind: '',
        receiverINN: 'ИНН получателя',
        allowedSmActions: {
          sign: false,
          fromArchive: false,
          delete: true,
          recall: false,
          save: true,
          archive: true,
          send: false
        },
        signed: false,
        linkedDocs: [],
        notes: 'Заметки',
        lastChangeStateDate: '2017-01-01T00:00:00+0300'
      }
    ];

    const reducer = getListSuccess(state, list);

    expect(reducer).toEqual(
      initialState.merge({
        list,
        isListLoading: false
      })
    );
  });

  it('exportListRequest', () => {
    const reducer = exportListRequest(state);

    expect(reducer).toEqual(
      initialState.merge({
        isExportListLoading: true
      })
    );
  });

  it('exportListFinish', () => {
    const reducer = exportListFinish(state);

    expect(reducer).toEqual(
      initialState.merge({
        isExportListLoading: false
      })
    );
  });

  it('unmountRequest', () => {
    const reducer = unmountRequest(state);

    expect(reducer).toEqual(
      initialState.merge({
        list: fromJS([]),
        pages: fromJS(initialPages),
      })
    );
  });

  it('resizeColumns', () => {
    const columns = fromJS([
      {
        id: 'number',
        width: 200,
        title: 'Номер',
        canGrouped: false,
        sorting: -1
      },
      {
        id: 'date',
        width: 92,
        title: 'Дата',
        canGrouped: true
      }
    ]);

    const reducer = resizeColumns(state, columns);

    expect(reducer).toEqual(
      initialState.merge({
        columns
      })
    );
  });

  it('changeColumn', () => {
    const columns = fromJS([
      {
        id: 'number',
        width: 100,
        title: 'Номер',
        canGrouped: false,
        sorting: -1
      },
      {
        id: 'date',
        width: 92,
        title: 'Дата',
        canGrouped: true
      }
    ]);

    const reducer = changeColumn(state, columns);

    expect(reducer).toEqual(
      initialState.merge({
        columns
      })
    );
  });

  it('changeGrouping', () => {
    const columns = fromJS([
      {
        id: 'number',
        width: 100,
        title: 'Номер',
        canGrouped: false,
        sorting: -1
      },
      {
        id: 'date',
        width: 92,
        title: 'Дата',
        canGrouped: true
      }
    ]);

    const reducer = changeGrouping(state, columns);

    expect(reducer).toEqual(
      initialState.merge({
        columns,
        isListUpdateRequired: true
      })
    );
  });

  it('changeSorting', () => {
    const columns = fromJS([
      {
        id: 'number',
        width: 100,
        title: 'Номер',
        canGrouped: false,
        sorting: -1
      },
      {
        id: 'date',
        width: 92,
        title: 'Дата',
        canGrouped: true
      }
    ]);

    const reducer = changeSorting(state, columns);

    expect(reducer).toEqual(
      initialState.merge({
        columns,
        isListUpdateRequired: true
      })
    );
  });

  it('changePagination', () => {
    const reducer = changePagination(state);

    expect(reducer).toEqual(
      initialState.merge({
        pages: fromJS(initialPages),
        isReloadRequired: true,
        isListUpdateRequired: true
      })
    );
  });
});
