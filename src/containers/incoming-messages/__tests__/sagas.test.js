import { put, call, select } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import { push } from 'react-router-redux';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction,
} from 'dal/notification/actions';
import {
  getIncomingMessages as dalGetList,
  exportIncomingMessagesListToExcel as dalExportIncomingMessagesListToExcel,
} from 'dal/messages/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import {
  ACTIONS,
  SETTINGS_ALIAS,
  LOCAL_REDUCER
} from '../constants';
import {
  initialSections,
  initialSettings,
  initialColumns,
  initialPages,
  initialFilters
} from '../reducer';
import {
  routingLocationSelector,
  profileSettingsSelector,
  localSectionsSelector,
  localSettingsSelector,
  localFiltersSelector,
  localPagesSelector,

  getSectionIndex,
  getSettings,
  getQueryParams,
  changeSection,
  changePage,
  reloadPages,
  changeFilters,
  getDictionaryForFilters,
  saveSettings,
  getList,
  exportList,
  changeSettingsWidth,
  changeSettingsColumns,
  changeSettingsPagination,
  newMessage,
  routeToItem,
  errorNotification
} from '../sagas';

describe('IncomingMessagesContainer Sagas', () => {
  const state = fromJS({
    routing: {
      locationBeforeTransitions: {
        pathname: '/messages',
        query: {
          page: 1
        },
        currentLocation: '/messages'
      }
    },
    profile: {
      settings: {
        [SETTINGS_ALIAS]: {
          incoming: initialSettings
        }
      }
    },
    [LOCAL_REDUCER]: {
      sections: initialSections,
      settings: initialSettings,
      pages: initialPages,
      filters: initialFilters
    }
  });

  describe('getSectionIndex', () => {
    it('success', () => {
      const generator = getSectionIndex();

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: 0 })
      );

      expect(
        generator.next(put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: 0 })).value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_SETTINGS_REQUEST })
      );
    });
  });

  describe('getSettings', () => {
    it('success', () => {
      const generator = getSettings();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_SETTINGS_SUCCESS, payload: initialSettings })
      );
    });
  });

  describe('getQueryParams', () => {
    it('success', () => {
      const generator = getQueryParams();

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_SUCCESS,
          payload: { pageIndex: 0 }
        })
      );
    });
  });

  describe('changeSection', () => {
    it('success', () => {
      const generator = changeSection({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_SECTION, payload: 'outgoing' });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        put(push({ pathname: '/messages/outgoing', search: null }))
      );
    });
  });

  describe('changePage', () => {
    it('success', () => {
      const generator = changePage({ type: ACTIONS.PAYMENTS_CONTAINER_CHANGE_PAGE, payload: 1 });

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );
    });
  });

  describe('reloadPages', () => {
    it('success', () => {
      const generator = reloadPages();
      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        put(push({
          pathname: '/messages',
          search: '?page=1'
        }))
      );
    });
  });

  describe('changeFilters', () => {
    it('success', () => {
      const generator = changeFilters({
        type: ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_FILTERS,
        payload: {
          filters: fromJS([
            {
              id: '1',
              title: 'Title 1',
              isPreset: true,
              savedConditions: fromJS(['1', '2'])
            },
            {
              id: '2',
              title: 'Title 2',
              isPreset: false,
              savedConditions: fromJS(['3', '4'])
            }
          ]),
          isSaveSettingRequired: true
        }
      });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            filters: [
              {
                id: '2',
                title: 'Title 2',
                isPreset: false,
                savedConditions: ['3', '4']

              }
            ]
          })
        })
      );
    });
  });

  describe('getDictionaryForFilters', () => {
    it('success', () => {
      const generator = getDictionaryForFilters({
        type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST,
        payload: { conditionId: 'account', value: '1234' }
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetAccounts, { payload: { accNum: '1234' } })
      );

      expect(
        generator.next(['1', '2', '3']).value
      ).toEqual(
        put({
          type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
          payload: { conditionId: 'account', items: ['1', '2', '3'] }
        })
      );
    });

    it('failure', () => {
      const generator = getDictionaryForFilters({
        type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST,
        payload: { conditionId: 'account', value: '1234' }
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetAccounts, { payload: { accNum: '1234' } })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({
          type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
          error: 'Произошла ошибка на сервере'
        })
      );
    });
  });

  describe('saveSettings', () => {
    it('success', () => {
      const generator = saveSettings({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: fromJS(initialSettings) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        call(dalSaveSettings, {
          payload: {
            settings: {
              [SETTINGS_ALIAS]: {
                incoming: initialSettings
              }
            }
          }
        })
      );

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_SUCCESS, payload: fromJS(initialSettings) })
      );
    });

    it('failure', () => {
      const generator = saveSettings({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: fromJS(initialSettings) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        call(dalSaveSettings, {
          payload: {
            settings: {
              [SETTINGS_ALIAS]: {
                incoming: initialSettings
              }
            }
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('getList', () => {
    it('success', () => {
      const generator = getList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localPagesSelector)
      );

      expect(
        generator.next(localPagesSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalGetList, {
          payload: {
            offset: 0,
            offsetStep: 31,
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            sender: null,
            topic: null,
            text: null,
            receiver: null,
            isRead: null,
            mustRead: null,
            account: null
          }
        })
      );

      expect(
        generator.next(['1', '2', '3']).value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_LIST_SUCCESS, payload: ['1', '2', '3'] })
      );
    });

    it('failure', () => {
      const generator = getList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localPagesSelector)
      );

      expect(
        generator.next(localPagesSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalGetList, {
          payload: {
            offset: 0,
            offsetStep: 31,
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            sender: null,
            topic: null,
            text: null,
            receiver: null,
            isRead: null,
            mustRead: null,
            account: null
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_LIST_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('exportList', () => {
    it('success', () => {
      const generator = exportList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalExportIncomingMessagesListToExcel, {
          payload: {
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            sender: null,
            topic: null,
            text: null,
            receiver: null,
            isRead: null,
            mustRead: null,
            account: null,
            visibleColumns: ['date', 'attach', 'read', 'title', 'receiver',
              'sender', 'account', 'mustRead']
          }
        })
      );

      expect(
        generator.next([1]).value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_SUCCESS, payload: [1] })
      );
    });

    it('failure', () => {
      const generator = exportList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalExportIncomingMessagesListToExcel, {
          payload: {
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            sender: null,
            topic: null,
            text: null,
            receiver: null,
            isRead: null,
            mustRead: null,
            account: null,
            visibleColumns: ['date', 'attach', 'read', 'title', 'receiver',
              'sender', 'account', 'mustRead']
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('changeSettingsWidth', () => {
    it('success', () => {
      const generator = changeSettingsWidth({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_RESIZE_COLUMNS, payload: fromJS(initialColumns).map(column => column.set('width', 300)) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            width: {
              date: 300,
              attach: 300,
              read: 300,
              title: 300,
              receiver: 300,
              sender: 300,
              account: 300,
              mustRead: 300
            }
          })
        })
      );
    });
  });

  describe('changeSettingsColumns', () => {
    it('success', () => {
      const generator = changeSettingsColumns({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_COLUMN, payload: fromJS(initialColumns).update(0, column => column.set('sorting', 1)) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            visible: [],
            sorting: { id: 'date', direction: 1 }
          })
        })
      );
    });
  });

  describe('changeSettingsPagination', () => {
    it('success', () => {
      const generator = changeSettingsPagination({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_PAGINATION, payload: 60 });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            pagination: 60
          })
        })
      );
    });
  });

  describe('newMessage', () => {
    it('success', () => {
      const generator = newMessage({
        type: ACTIONS.INCOMING_MESSAGES_CONTAINER_NEW_MESSAGE
      });
      expect(
        generator.next().value
      ).toEqual(
        put(push('/messages/outgoing/new'))
      );
    });
  });

  describe('routeToItem', () => {
    it('success', () => {
      const generator = routeToItem({ type: ACTIONS.INCOMING_MESSAGES_CONTAINER_ROUTE_TO_ITEM, payload: 'itemId' });
      expect(
        generator.next().value
      ).toEqual(
        put(push('/messages/itemId'))
      );
    });
  });

  describe('errorNotification', () => {
    it('success', () => {
      const generator = errorNotification({
        type: ACTIONS.INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL,
        error: 'Произошла ошибка на сервере'
      });

      expect(
        generator.next().value
      ).toEqual(
        put(dalAddOrdinaryNotificationAction({
          type: 'error',
          title: 'Произошла ошибка на сервере'
        }))
      );
    });
  });
});
