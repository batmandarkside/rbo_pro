import { fromJS } from 'immutable';
import { LOCAL_REDUCER } from './../constants';
import {
  columnsCreatedSelector,
  filtersCreatedSelector,
  listCreatedSelector,
  paginationOptionsCreatedSelector
} from '../selectors';

describe('IncomingMessagesContainer Selectors', () => {
  const state = fromJS({
    organizations: {
      orgs: [
        {
          intAddress: 'AKADEMIKA ANOKHINA STR 38 BLD 1',
          actualAddress: '105062, г Москва, пер Подсосенский, 23',
          shortName: 'ООО АРТ БАЗАР (сокр)',
          inn: '7729580257',
          city: '',
          name: 'Общество с ограниченной ответственностью АРТ БАЗАР',
          kpp: '772901001',
          juridicalAddress: '119602, Москва г, Академика Анохина ул, 38, 1',
          area: 'Москва г',
          ogrn: '1077758447073',
          country: 'Российская Федерация',
          intName: 'Art Bazaar',
          id: 'f4027a46-7997-4adf-9478-92d05610221c',
          lawType: 'ООО'
        }
      ]
    },
    [LOCAL_REDUCER]: {
      isExportListLoading: false,
      isReloadRequired: false,
      settings: {
        visible: [
          'date',
          'attach',
          'read',
          'title',
          'receiver',
          'sender',
          'account',
          'mustRead'
        ],
        sorting: {
          id: 'date',
          direction: -1
        },
        grouped: null,
        pagination: 30,
        width: {
          date: 92,
          attach: 110,
          read: 110,
          title: 460,
          receiver: 240,
          sender: 240,
          account: 196,
          mustRead: 110
        }
      },
      paginationOptions: [
        30,
        60,
        120,
        240
      ],
      pages: {
        size: 1,
        selected: 0,
        visible: 1
      },
      sections: [
        {
          id: 'incoming',
          title: 'Входящие',
          route: '',
          api: '',
          selected: true
        },
        {
          id: 'outgoing',
          title: 'Исходящие',
          route: 'outgoing',
          selected: false
        },
        {
          id: 'archive',
          title: 'Архив',
          route: 'outgoing/archive',
          selected: false
        },
        {
          id: 'trash',
          title: 'Удаленные',
          route: 'outgoing/trash',
          selected: false
        }
      ],
      isListUpdateRequired: false,
      filtersConditions: [
        {
          id: 'date',
          title: 'Дата',
          type: 'datesRange'
        },
        {
          id: 'isRead',
          title: 'Признак прочтения',
          type: 'select',
          values: [
            'Прочитано',
            'Не прочитано'
          ]
        },
        {
          id: 'topic',
          title: 'Тема',
          maxLength: 200,
          type: 'string'
        },
        {
          id: 'receiver',
          title: 'Кому',
          maxLength: 200,
          type: 'string'
        },
        {
          id: 'sender',
          title: 'От кого',
          maxLength: 200,
          type: 'string'
        },
        {
          id: 'account',
          title: 'Счет',
          type: 'dictionary',
          values: {
            discrete: true,
            labelKey: 'accNum'
          }
        },
        {
          id: 'mustRead',
          title: 'Признак важности',
          type: 'select',
          values: [
            'Важные',
            'Прочие'
          ]
        },
        {
          id: 'text',
          title: 'Сообщение',
          maxLength: 200,
          type: 'string'
        }
      ],
      isListLoading: false,
      filters: [
        {
          title: 'Непрочитанные',
          isPreset: true,
          savedConditions: [
            {
              id: 'isRead',
              params: 'Не прочитано'
            }
          ],
          sections: [
            'incoming'
          ]
        },
        {
          title: 'Важные',
          isPreset: true,
          savedConditions: [
            {
              id: 'mustRead',
              params: 'Важные'
            }
          ],
          sections: [
            'incoming'
          ]
        }
      ],
      columns: [
        {
          id: 'date',
          title: 'Дата',
          canGrouped: true
        },
        {
          id: 'attach',
          title: 'Вложение',
          canGrouped: false
        },
        {
          id: 'read',
          title: 'Прочитано',
          canGrouped: true
        },
        {
          id: 'title',
          title: 'Тема',
          canGrouped: true
        },
        {
          id: 'receiver',
          title: 'Кому',
          canGrouped: true
        },
        {
          id: 'sender',
          title: 'От кого',
          canGrouped: false
        },
        {
          id: 'account',
          title: 'Счет',
          canGrouped: false
        },
        {
          id: 'mustRead',
          title: 'Важность',
          canGrouped: true
        }
      ],
      list: [
        {
          attachExists: '0',
          messageType: 'Досыл уточненных реквизитов',
          receivers: [
            'f4027a46-7997-4adf-9478-92d05610221c'
          ],
          message: 'Чекбокс "Обязательно для прочтения" отмечен.',
          account: '40702810400004409387',
          date: '2017-04-18T00:00:00+0300',
          topic: 'Обязательное для прочтения письмо с аттачем',
          docId: '07635a5f-7c8a-4062-a651-1575a9442bde',
          sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
          read: '1',
          mustRead: '1'
        }]
    }
  });

  it('columnsCreatedSelector', () => {
    const selector = columnsCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'date',
          title: 'Дата',
          canGrouped: true,
          grouped: false,
          sorting: -1,
          visible: true,
          width: 92
        },
        {
          id: 'attach',
          title: 'Вложение',
          canGrouped: false,
          grouped: false,
          sorting: 0,
          visible: true,
          width: 110
        },
        {
          id: 'read',
          title: 'Прочитано',
          canGrouped: true,
          grouped: false,
          sorting: 0,
          visible: true,
          width: 110
        },
        {
          id: 'title',
          title: 'Тема',
          canGrouped: true,
          grouped: false,
          sorting: 0,
          visible: true,
          width: 460
        },
        {
          id: 'receiver',
          title: 'Кому',
          canGrouped: true,
          grouped: false,
          sorting: 0,
          visible: true,
          width: 240
        },
        {
          id: 'sender',
          title: 'От кого',
          canGrouped: false,
          grouped: false,
          sorting: 0,
          visible: true,
          width: 240
        },
        {
          id: 'account',
          title: 'Счет',
          canGrouped: false,
          grouped: false,
          sorting: 0,
          visible: true,
          width: 196
        },
        {
          id: 'mustRead',
          title: 'Важность',
          canGrouped: true,
          grouped: false,
          sorting: 0,
          visible: true,
          width: 110
        }
      ]
    );
  });

  it('filtersCreatedSelector', () => {
    const selector = filtersCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          title: 'Непрочитанные',
          isPreset: true,
          savedConditions: [
            {
              id: 'isRead',
              params: 'Не прочитано'
            }
          ],
          conditions: [
            {
              id: 'isRead',
              params: 'Не прочитано',
              value: 'Не прочитано'
            }
          ],
          sections: [
            'incoming'
          ]
        },
        {
          title: 'Важные',
          isPreset: true,
          savedConditions: [
            {
              id: 'mustRead',
              params: 'Важные'
            }
          ],
          conditions: [
            {
              id: 'mustRead',
              params: 'Важные',
              value: 'Важные'
            }
          ],
          sections: [
            'incoming'
          ]
        }
      ]
    );
  });

  it('listCreatedSelector', () => {
    const selector = listCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'notGrouped',
          items: [
            {
              id: '07635a5f-7c8a-4062-a651-1575a9442bde',
              account: '40702.810.4.00004409387',
              attach: false,
              date: '18.04.2017',
              mustRead: true,
              read: true,
              receiver: 'ООО АРТ БАЗАР (сокр)',
              sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
              title: 'Обязательное для прочтения письмо с аттачем'
            },
          ]
        }
      ]
    );
  });

  it('paginationOptionsCreatedSelector', () => {
    const selector = paginationOptionsCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          value: 30,
          title: 30,
          selected: true
        },
        {
          value: 60,
          title: 60,
          selected: false
        },
        {
          value: 120,
          title: 120,
          selected: false
        },
        {
          value: 240,
          title: 240,
          selected: false
        }
      ]
    );
  });
});
