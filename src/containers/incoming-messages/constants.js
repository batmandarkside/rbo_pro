import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  INCOMING_MESSAGES_CONTAINER_GET_SECTION_INDEX_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS: null,
  INCOMING_MESSAGES_CONTAINER_GET_SETTINGS_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_GET_SETTINGS_SUCCESS: null,
  INCOMING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_SUCCESS: null,
  INCOMING_MESSAGES_CONTAINER_CHANGE_SECTION: null,
  INCOMING_MESSAGES_CONTAINER_CHANGE_PAGE: null,
  INCOMING_MESSAGES_CONTAINER_RELOAD_PAGES: null,
  INCOMING_MESSAGES_CONTAINER_CHANGE_FILTERS: null,
  INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS: null,
  INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL: null,
  INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_SUCCESS: null,
  INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_FAIL: null,
  INCOMING_MESSAGES_CONTAINER_GET_LIST_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_GET_LIST_SUCCESS: null,
  INCOMING_MESSAGES_CONTAINER_GET_LIST_FAIL: null,
  INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_SUCCESS: null,
  INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL: null,
  INCOMING_MESSAGES_CONTAINER_UNMOUNT_REQUEST: null,
  INCOMING_MESSAGES_CONTAINER_RESIZE_COLUMNS: null,
  INCOMING_MESSAGES_CONTAINER_CHANGE_COLUMN: null,
  INCOMING_MESSAGES_CONTAINER_CHANGE_GROUPING: null,
  INCOMING_MESSAGES_CONTAINER_CHANGE_SORTING: null,
  INCOMING_MESSAGES_CONTAINER_CHANGE_PAGINATION: null,
  INCOMING_MESSAGES_CONTAINER_NEW_MESSAGE: null,
  INCOMING_MESSAGES_CONTAINER_ROUTE_TO_ITEM: null
});

export const LOCAL_REDUCER = 'IncomingMessagesContainerReducer';

export const LOCAL_ROUTER_ALIAS = '/messages';

export const SETTINGS_ALIAS = 'IncomingMessagesContainer';

export const VALIDATE_SETTINGS = settings => !!settings &&
  !!settings.visible &&
  !!settings.sorting &&
  !!settings.pagination &&
  !!settings.width &&
  !!settings.width.date &&
  !!settings.width.attach &&
  !!settings.width.read &&
  !!settings.width.title &&
  !!settings.width.receiver &&
  !!settings.width.sender &&
  !!settings.width.account &&
  !!settings.width.mustRead;

export const LOCATORS = {
  CONTAINER: 'incomingMessagesContainer',
  SPREADSHEET: 'incomingMessagesContainerSpreadsheet',

  TOOLBAR: 'incomingMessagesContainerSpreadsheetToolbar',
  TOOLBAR_BUTTONS: 'incomingMessagesContainerSpreadsheetToolbarButtons',
  TOOLBAR_BUTTON_REFRESH_LIST: 'incomingMessagesContainerSpreadsheetToolbarButtonRefreshList',
  TOOLBAR_BUTTON_NEW_MESSAGE: 'incomingMessagesContainerSpreadsheetToolbarButtonNewMessage',
  TOOLBAR_TABS: 'incomingMessagesContainerSpreadsheetToolbarTabs',

  FILTERS: 'incomingMessagesContainerSpreadsheetFilters',
  FILTERS_SELECT: 'incomingMessagesContainerSpreadsheetFiltersSelect',
  FILTERS_ADD: 'incomingMessagesContainerSpreadsheetFiltersAdd',
  FILTERS_REMOVE: 'incomingMessagesContainerSpreadsheetFiltersRemove',
  FILTERS_OPERATIONS: 'incomingMessagesContainerSpreadsheetFiltersOperations',
  FILTERS_OPERATION_SAVE: 'incomingMessagesContainerSpreadsheetFiltersOperationSave',
  FILTERS_OPERATION_SAVE_AS: 'incomingMessagesContainerSpreadsheetFiltersOperationSaveAs',
  FILTERS_OPERATION_SAVE_AS_POPUP: 'incomingMessagesContainerSpreadsheetFiltersOperationSaveAsPopup',
  FILTERS_OPERATION_CLEAR: 'incomingMessagesContainerSpreadsheetFiltersOperationClear',
  FILTERS_CONDITIONS: 'incomingMessagesContainerSpreadsheetFiltersConditions',
  FILTERS_CONDITION: 'incomingMessagesContainerSpreadsheetFiltersCondition',
  FILTERS_CONDITION_POPUP: 'incomingMessagesContainerSpreadsheetFiltersConditionPopup',
  FILTERS_CONDITION_REMOVE: 'incomingMessagesContainerSpreadsheetFiltersConditionRemove',
  FILTERS_ADD_CONDITION: 'incomingMessagesContainerSpreadsheetFiltersAddCondition',
  FILTERS_ADD_CONDITION_POPUP: 'incomingMessagesContainerSpreadsheetFiltersAddConditionPopup',
  FILTERS_CLOSE: 'incomingMessagesContainerSpreadsheetFiltersClose',

  TABLE: 'incomingMessagesContainerSpreadsheetTable',
  TABLE_HEAD: 'incomingMessagesContainerSpreadsheetTableHead',
  TABLE_BODY: 'incomingMessagesContainerSpreadsheetTableBody',
  TABLE_SETTINGS: 'incomingMessagesContainerSpreadsheetTableSettings',
  TABLE_SETTINGS_TOGGLE: 'incomingMessagesContainerSpreadsheetTableSettingsToggle',
  TABLE_SETTINGS_TOGGLE_BUTTON: 'incomingMessagesContainerSpreadsheetTableSettingsToggleButton',
  TABLE_SETTINGS_POPUP: 'incomingMessagesContainerSpreadsheetTableSettingsPopup',

  EXPORT: 'incomingMessagesContainerSpreadsheetExport',
  PAGINATION: 'incomingMessagesContainerSpreadsheetPagination'
};
