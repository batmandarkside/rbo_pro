import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Icon from 'components/ui-components/icon';
import Spreadsheet, {
  Toolbar,
  Filters,
  Table,
  Export,
  Pagination
} from 'components/ui-components/spreadsheet';
import { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import { LOCATORS } from './constants';
import './style.css';

class IncomingMessagesContainer extends Component {

  static propTypes = {
    isExportListLoading: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,

    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    location: PropTypes.object.isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,
    sections: PropTypes.instanceOf(List).isRequired,

    getSectionIndexAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changeSectionAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getDictionaryForFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    exportListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeGroupingAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
    newMessageAction: PropTypes.func.isRequired,
    routeToItemAction: PropTypes.func.isRequired,
    tabs: PropTypes.instanceOf(List).isRequired
  };

  componentWillMount() {
    this.props.getSectionIndexAction();
    this.props.getQueryParamsAction();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getSectionIndexAction();
      this.props.getQueryParamsAction();
    } else if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  filterDictionaryFormFieldOptionRenderer = ({ conditionId, option, highlight }) => {
    switch (conditionId) {
      case 'account':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.account}
            describe={option && option.orgName}
            extra={option && option.availableBalance}
            highlight={{
              value: highlight,
              inTitle: option && option.account
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  renderItemAlignRightElement = (value) => {
    const style = { textAlign: 'right' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemAlignCenterElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemAttachElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>
        {value &&
        <Icon
          type="attachment"
          size="16"
        />
        }
      </div>
    );
  };

  renderItemBoolElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>
        <Icon
          type={value ? 'bool-true' : 'bool-false'}
          size="16"
        />
      </div>
    );
  };

  render() {
    const {
      isExportListLoading,
      isListLoading,
      columns,
      filtersConditions,
      filters,
      list,
      pages,
      paginationOptions,
      sections,
      exportListAction,
      getListAction,
      changeColumnsAction,
      changeGroupingAction,
      changeFiltersAction,
      getDictionaryForFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSectionAction,
      changeSortingAction,
      newMessageAction,
      resizeColumnsAction,
      routeToItemAction,
      tabs
    } = this.props;

    return (
      <div className="b-incoming-messages-container" data-loc={LOCATORS.CONTAINER}>
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                  newMessage: LOCATORS.TOOLBAR_BUTTON_NEW_MESSAGE,
                },
                tabs: LOCATORS.TOOLBAR_TABS
              }}
              title="Письма"
              buttons={[
                {
                  id: 'refreshList',
                  isProgress: isListLoading,
                  type: 'refresh',
                  title: 'Обновить',
                  onClick: getListAction
                },
                {
                  id: 'newMessage',
                  type: 'simple',
                  title: 'Написать новое письмо',
                  onClick: newMessageAction
                }
              ]}
              tabs={{
                items: tabs,
                selected: sections.findKey(section => section.get('selected')),
                onChange: changeSectionAction
              }}
            />
            <Filters
              dataLocs={{
                main: LOCATORS.FILTERS,
                select: LOCATORS.FILTERS_SELECT,
                add: LOCATORS.FILTERS_ADD,
                remove: LOCATORS.FILTERS_REMOVE,
                operations: LOCATORS.FILTERS_OPERATIONS,
                operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                conditions: LOCATORS.FILTERS_CONDITIONS,
                condition: LOCATORS.FILTERS_CONDITION,
                conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                close: LOCATORS.FILTERS_CLOSE
              }}
              conditions={filtersConditions}
              filters={filters}
              changeFiltersAction={changeFiltersAction}
              dictionaryFormFieldOptionRenderer={this.filterDictionaryFormFieldOptionRenderer}
              onConditionDictionaryFormInputChange={getDictionaryForFiltersAction}
            />
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                date: this.renderItemAlignRightElement,
                attach: this.renderItemAttachElement,
                read: this.renderItemBoolElement,
                mustRead: this.renderItemBoolElement,
              }}
              renderHeadCells={{
                date: this.renderItemAlignRightElement,
                attach: this.renderItemAlignCenterElement,
                read: this.renderItemAlignCenterElement,
                mustRead: this.renderItemAlignCenterElement,
              }}
              settings={{
                iconTitle: 'Настройка таблицы писем',
                changeColumnsAction,
                showGrouping: true,
                changeGroupingAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              sorting={{
                changeSortingAction
              }}
              resize={{
                resizeColumnsAction
              }}
              onItemClick={routeToItemAction}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {!isListLoading && !!list.size &&
              <Export
                dataLoc={LOCATORS.EXPORT}
                label="Выгрузить список в Excel"
                onClick={exportListAction}
                isLoading={isExportListLoading}
              />
            }
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
      </div>
    );
  }
}

export default IncomingMessagesContainer;
