import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const initialSections = [
  {
    id: 'incoming',
    title: 'Входящие',
    route: '/messages',
    selected: true
  },
  {
    id: 'outgoing',
    title: 'Исходящие',
    route: '/messages/outgoing'
  },
  {
    id: 'archive',
    title: 'Архив',
    route: '/messages/outgoing/archive'
  },
  {
    id: 'trash',
    title: 'Удаленные',
    route: '/messages/outgoing/trash'
  }
];

export const initialFiltersConditions = [
  {
    id: 'date',
    title: 'Дата',
    type: 'datesRange',
    sections: ['incoming']
  },
  {
    id: 'isRead',
    title: 'Признак прочтения',
    type: 'select',
    values: [
      'Прочитано',
      'Не прочитано'
    ],
    sections: ['incoming']
  },
  {
    id: 'topic',
    title: 'Тема',
    type: 'string',
    maxLength: 100,
    sections: ['incoming']
  },
  {
    id: 'sender',
    title: 'От кого',
    type: 'string',
    maxLength: 250,
    sections: ['incoming']
  },
  {
    id: 'account',
    title: 'Счет',
    type: 'search',
    inputType: 'Account',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['incoming']
  },
  {
    id: 'mustRead',
    title: 'Признак важности',
    type: 'select',
    values: [
      'Важные',
      'Прочие'
    ],
    sections: ['incoming']
  },
  {
    id: 'text',
    title: 'Сообщение',
    type: 'string',
    maxLength: 100,
    sections: ['incoming']
  }
];

export const initialFilters = [
  {
    id: 'incomingMessagesContainerToRead',
    title: 'Непрочитанные',
    isPreset: true,
    savedConditions: [
      {
        id: 'isRead',
        params: 'Не прочитано'
      }
    ],
    sections: ['incoming']
  },
  {
    id: 'incomingMessagesContainerMustRead',
    title: 'Важные',
    isPreset: true,
    savedConditions: [
      {
        id: 'mustRead',
        params: 'Важные'
      }
    ],
    sections: ['incoming']
  }
];

export const initialColumns = [
  {
    id: 'date',
    title: 'Дата',
    canGrouped: true
  },
  {
    id: 'attach',
    title: 'Вложение',
    canGrouped: false
  },
  {
    id: 'read',
    title: 'Прочитано',
    canGrouped: true
  },
  {
    id: 'title',
    title: 'Тема',
    canGrouped: true
  },
  {
    id: 'receiver',
    title: 'Кому',
    canGrouped: true
  },
  {
    id: 'sender',
    title: 'От кого',
    canGrouped: false
  },
  {
    id: 'account',
    title: 'Счет',
    canGrouped: false
  },
  {
    id: 'mustRead',
    title: 'Важность',
    canGrouped: true
  }
];

export const initialSettings = {
  visible: ['date', 'attach', 'read', 'title', 'receiver',
    'sender', 'account', 'mustRead'],
  sorting: { id: 'date', direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    date: 92,
    attach: 110,
    read: 110,
    title: 460,
    receiver: 240,
    sender: 240,
    account: 196,
    mustRead: 110
  }
};

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialState = fromJS({
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: [],
  filters: [],
  list: []
});

export const getSectionIndexSuccess = (state, sectionIndex) => {
  const selectedSectionId = state.get('sections').find((section, key) => key === sectionIndex).get('id');
  return state.merge({
    sections: state.get('sections').map((section, key) => section.set('selected', key === sectionIndex)),
    filtersConditions: fromJS(initialFiltersConditions)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections')),
    filters: fromJS(initialFilters)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections'))
  });
};

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters;
  if (filter) {
    if (state.get('filters').find(item => item.get('id') === filter.id)) {
      filters = state.get('filters').map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = state.get('filters').push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  } else {
    filters = state.get('filters').map(item => item.set('selected', false));
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const changeSection = (state, sectionIndex) => (
  state.getIn(['sections', sectionIndex, 'selected']) ?
    state :
    state.merge({
      columns: fromJS(initialColumns),
      settings: fromJS(initialSettings),
      pages: fromJS(initialPages),
      paginationOptions: fromJS(initialPaginationOptions)
    })
);

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], true)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list);
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

function IncomingMessagesContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS:
      return getSectionIndexSuccess(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_SECTION:
      return changeSection(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.INCOMING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.INCOMING_MESSAGES_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    default:
      return state;
  }
}

export default IncomingMessagesContainerReducer;
