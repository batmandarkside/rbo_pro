import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  MOVEMENTS_CONTAINER_GET_SECTION_INDEX_REQUEST: null,
  MOVEMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS: null,
  MOVEMENTS_CONTAINER_GET_SETTINGS_REQUEST: null,
  MOVEMENTS_CONTAINER_GET_SETTINGS_SUCCESS: null,
  MOVEMENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST: null,
  MOVEMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS: null,
  MOVEMENTS_CONTAINER_CHANGE_SECTION: null,
  MOVEMENTS_CONTAINER_CHANGE_PAGE: null,
  MOVEMENTS_CONTAINER_RELOAD_PAGES: null,
  MOVEMENTS_CONTAINER_CHANGE_FILTERS: null,
  MOVEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST: null,
  MOVEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS: null,
  MOVEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL: null,
  MOVEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST: null,
  MOVEMENTS_CONTAINER_SAVE_SETTINGS_SUCCESS: null,
  MOVEMENTS_CONTAINER_SAVE_SETTINGS_FAIL: null,
  MOVEMENTS_CONTAINER_GET_LIST_REQUEST: null,
  MOVEMENTS_CONTAINER_GET_LIST_SUCCESS: null,
  MOVEMENTS_CONTAINER_GET_LIST_FAIL: null,
  MOVEMENTS_CONTAINER_EXPORT_LIST_REQUEST: null,
  MOVEMENTS_CONTAINER_EXPORT_LIST_SUCCESS: null,
  MOVEMENTS_CONTAINER_EXPORT_LIST_FAIL: null,
  MOVEMENTS_CONTAINER_UNMOUNT_REQUEST: null,
  MOVEMENTS_CONTAINER_RESIZE_COLUMNS: null,
  MOVEMENTS_CONTAINER_CHANGE_COLUMN: null,
  MOVEMENTS_CONTAINER_CHANGE_GROUPING: null,
  MOVEMENTS_CONTAINER_CHANGE_SORTING: null,
  MOVEMENTS_CONTAINER_CHANGE_PAGINATION: null,
  MOVEMENTS_CONTAINER_CHANGE_SELECT: null,

  MOVEMENTS_CONTAINER_OPERATION: null,
  MOVEMENTS_CONTAINER_OPERATION_PRINT_REQUEST: null,
  MOVEMENTS_CONTAINER_OPERATION_PRINT_SUCCESS: null,
  MOVEMENTS_CONTAINER_OPERATION_PRINT_FAIL: null,
});

export const LOCAL_REDUCER = 'MovementsContainerReducer';

export const LOCAL_ROUTER_ALIAS = '/statements';

export const SETTINGS_ALIAS = 'MovementsContainer';

export const VALIDATE_SETTINGS = settings => !!settings &&
  !!settings.visible &&
  !!settings.sorting &&
  !!settings.pagination &&
  !!settings.width &&
  !!settings.width.account &&
  !!settings.width.final &&
  !!settings.width.stmtDate &&
  !!settings.width.operDate &&
  !!settings.width.valDate &&
  !!settings.width.operType &&
  !!settings.width.operNumber &&
  !!settings.width.debit &&
  !!settings.width.credit &&
  !!settings.width.currCodeIso &&
  !!settings.width.corrName &&
  !!settings.width.inn &&
  !!settings.width.corrAccount &&
  !!settings.width.corrBankName &&
  !!settings.width['debit-credit'] &&
  !!settings.width.paymentPurpose;

export const LOCATORS = {
  CONTAINER: 'movementsContainer',
  SPREADSHEET: 'movementsContainerSpreadsheet',

  TOOLBAR: 'movementsContainerSpreadsheetToolbar',
  TOOLBAR_BUTTONS: 'movementsContainerSpreadsheetToolbarButtons',
  TOOLBAR_BUTTON_REFRESH_LIST: 'movementsContainerSpreadsheetToolbarButtonRefreshList',
  TOOLBAR_TABS: 'movementsContainerSpreadsheetToolbarTabs',

  FILTERS: 'movementsContainerSpreadsheetFilters',
  FILTERS_SELECT: 'movementsContainerSpreadsheetFiltersSelect',
  FILTERS_ADD: 'movementsContainerSpreadsheetFiltersAdd',
  FILTERS_REMOVE: 'movementsContainerSpreadsheetFiltersRemove',
  FILTERS_OPERATIONS: 'movementsContainerSpreadsheetFiltersOperations',
  FILTERS_OPERATION_SAVE: 'movementsContainerSpreadsheetFiltersOperationSave',
  FILTERS_OPERATION_SAVE_AS: 'movementsContainerSpreadsheetFiltersOperationSaveAs',
  FILTERS_OPERATION_SAVE_AS_POPUP: 'movementsContainerSpreadsheetFiltersOperationSaveAsPopup',
  FILTERS_OPERATION_CLEAR: 'movementsContainerSpreadsheetFiltersOperationClear',
  FILTERS_CONDITIONS: 'movementsContainerSpreadsheetFiltersConditions',
  FILTERS_CONDITION: 'movementsContainerSpreadsheetFiltersCondition',
  FILTERS_CONDITION_POPUP: 'movementsContainerSpreadsheetFiltersConditionPopup',
  FILTERS_CONDITION_REMOVE: 'movementsContainerSpreadsheetFiltersConditionRemove',
  FILTERS_ADD_CONDITION: 'movementsContainerSpreadsheetFiltersAddCondition',
  FILTERS_ADD_CONDITION_POPUP: 'movementsContainerSpreadsheetFiltersAddConditionPopup',
  FILTERS_CLOSE: 'movementsContainerSpreadsheetFiltersClose',

  OPERATIONS: 'movementsContainerSpreadsheetOperations',

  TABLE: 'movementsContainerSpreadsheetTable',
  TABLE_HEAD: 'movementsContainerSpreadsheetTableHead',
  TABLE_BODY: 'movementsContainerSpreadsheetTableBody',
  TABLE_SETTINGS: 'movementsContainerSpreadsheetTableSettings',
  TABLE_SETTINGS_TOGGLE: 'movementsContainerSpreadsheetTableSettingsToggle',
  TABLE_SETTINGS_TOGGLE_BUTTON: 'movementsContainerSpreadsheetTableSettingsToggleButton',
  TABLE_SETTINGS_POPUP: 'movementsContainerSpreadsheetTableSettingsPopup',

  SELECTION: 'movementsContainerSpreadsheetSelection',
  EXPORT: 'movementsContainerSpreadsheetExport',
  PAGINATION: 'movementsContainerSpreadsheetPagination'
};
