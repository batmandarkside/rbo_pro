import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const initialSections = [
  {
    id: 'statements',
    title: 'Выписки',
    route: '/statements'
  },
  {
    id: 'movements',
    title: 'Операции',
    route: '/statements/movements',
    selected: true,
  }
];

export const initialFiltersConditions = [
  {
    id: 'account',
    title: 'Собственный счет',
    type: 'search',
    inputType: 'Account',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['movements']
  },
  {
    id: 'isFinal',
    title: 'Итоговая',
    type: 'select',
    values: [
      'Итоговая',
      'Внутридневная'
    ],
    sections: ['movements']
  },
  {
    id: 'stmtDate',
    title: 'Дата выписки',
    type: 'datesRange',
    sections: ['movements']
  },
  {
    id: 'operNumber',
    title: 'Номер операции',
    type: 'string',
    maxLength: 64,
    sections: ['movements']
  },
  {
    id: 'currCodeIso',
    title: 'Валюта счета',
    type: 'search',
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['movements']
  },
  {
    id: 'corrName',
    title: 'Контрагент',
    type: 'string',
    maxLength: 160,
    sections: ['movements']
  },
  {
    id: 'inn',
    title: 'ИНН контрагента',
    type: 'string',
    maxLength: 12,
    sections: ['movements']
  },
  {
    id: 'corrAccount',
    title: 'Счет контрагента',
    type: 'string',
    inputType: 'Account',
    sections: ['movements']
  },
  {
    id: 'operType',
    title: 'Дебет/Кредит',
    type: 'select',
    values: [
      'Дебет',
      'Кредит'
    ],
    sections: ['movements']
  },
  {
    id: 'purpose',
    title: 'Назначение платежа',
    type: 'string',
    maxLength: 210,
    sections: ['movements']
  },
  {
    id: 'sum',
    title: 'Сумма операции',
    type: 'amountsRange',
    sections: ['movements']
  }
];

export const initialFilters = [
  {
    id: 'movementsContainerPresetCredit',
    title: 'Поступления',
    isPreset: true,
    savedConditions: [
      {
        id: 'operType',
        params: 'Кредит'
      }
    ],
    sections: ['movements']
  },
  {
    id: 'movementsContainerPresetDebit',
    title: 'Списания',
    isPreset: true,
    savedConditions: [
      {
        id: 'operType',
        params: 'Дебет'
      }
    ],
    sections: ['movements']
  }
];

export const initialColumns = [
  {
    id: 'account',
    title: 'Собственный счет',
    canGrouped: true
  },
  {
    id: 'final',
    title: 'Итоговая',
    canGrouped: false
  },
  {
    id: 'stmtDate',
    title: 'Дата выписки',
    canGrouped: true
  },
  {
    id: 'operDate',
    title: 'Дата операции',
    canGrouped: true
  },
  {
    id: 'valDate',
    title: 'Дата валютирования',
    canGrouped: false
  },
  {
    id: 'operType',
    title: 'Вид операции',
    canGrouped: true
  },
  {
    id: 'operNumber',
    title: 'Номер операции',
    canGrouped: false
  },
  {
    id: 'debit',
    title: 'Дебет',
    canGrouped: false
  },
  {
    id: 'credit',
    title: 'Кредит',
    canGrouped: false
  },
  {
    id: 'currCodeIso',
    title: 'Валюта счета',
    canGrouped: false
  },
  {
    id: 'corrName',
    title: 'Контрагент',
    canGrouped: true
  },
  {
    id: 'inn',
    title: 'ИНН контрагента',
    canGrouped: false
  },
  {
    id: 'corrAccount',
    title: 'Счет контрагента',
    canGrouped: true
  },
  {
    id: 'corrBankName',
    title: 'Наименование банка контрагента',
    canGrouped: false
  },
  {
    id: 'debit-credit',
    title: 'Дебет/Кредит',
    canGrouped: true
  },
  {
    id: 'paymentPurpose',
    title: 'Назначение платежа',
    canGrouped: false
  }
];

export const initialOperations = [
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false,
    grouped: true
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialSettings = {
  visible: [
    'account',
    'final',
    'operDate',
    'debit',
    'credit',
    'currCodeIso',
    'corrName',
    'paymentPurpose'
  ],
  sorting: { id: 'stmtDate', direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    account: 196,
    final: 104,
    stmtDate: 136,
    operDate: 136,
    valDate: 136,
    operType: 136,
    operNumber: 146,
    debit: 280,
    credit: 280,
    currCodeIso: 96,
    corrName: 280,
    inn: 152,
    corrAccount: 196,
    corrBankName: 280,
    'debit-credit': 132,
    paymentPurpose: 320
  }
};

export const initialState = fromJS({
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: [],
  filters: [],
  list: [],
  operations: initialOperations,
  selectedItems: []
});

export const getSectionIndexSuccess = (state, sectionIndex) => {
  const selectedSectionId = state.get('sections').find((section, key) => key === sectionIndex).get('id');
  return state.merge({
    sections: state.get('sections').map((section, key) => section.set('selected', key === sectionIndex)),
    filtersConditions: fromJS(initialFiltersConditions)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections')),
    filters: fromJS(initialFilters)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections'))
  });
};

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters;
  if (filter) {
    if (state.get('filters').find(item => item.get('id') === filter.id)) {
      filters = state.get('filters').map(item => (
        item.get('id') === filter.id ?
          item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          })
        :
        item
      ));
    } else {
      filters = state.get('filters').push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  } else {
    filters = state.get('filters').map(item => item.set('selected', false));
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const changeSection = (state, sectionIndex) => (
  state.getIn(['sections', sectionIndex, 'selected']) ?
    state :
    state.merge({
      columns: fromJS(initialColumns),
      settings: fromJS(initialSettings),
      pages: fromJS(initialPages),
      paginationOptions: fromJS(initialPaginationOptions)
    })
);

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], true)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  selectedItems: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list);
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([])
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

export const operationFinish = (state, operationId, isListUpdateRequired) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

function MovementsContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOVEMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS:
      return getSectionIndexSuccess(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_CHANGE_SECTION:
      return changeSection(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.MOVEMENTS_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.MOVEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.MOVEMENTS_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.MOVEMENTS_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.MOVEMENTS_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.MOVEMENTS_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.MOVEMENTS_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, action.payload);

    case ACTIONS.MOVEMENTS_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationRequest(state, 'print');

    case ACTIONS.MOVEMENTS_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.MOVEMENTS_CONTAINER_OPERATION_PRINT_FAIL:
      return operationFinish(state, 'print', false);

    default:
      return state;
  }
}

export default MovementsContainerReducer;
