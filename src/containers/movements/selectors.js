import { createSelector } from 'reselect';
import { List, Map, OrderedMap } from 'immutable';
import numeral from 'numeral';
import moment from 'moment-timezone';
import { getFormattedAccNum } from 'utils';
import { LOCAL_REDUCER } from './constants';

const isExportListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isExportListLoading']);
const isListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isListLoading']);
const isListUpdateRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isListUpdateRequired']);
const isReloadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isReloadRequired']);

const columnsSelector = state => state.getIn([LOCAL_REDUCER, 'columns']);
const filtersConditionsSelector = state => state.getIn([LOCAL_REDUCER, 'filtersConditions']);
const filtersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
const listSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
const sectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
const settingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
const pagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
const paginationOptionsSelector = state => state.getIn([LOCAL_REDUCER, 'paginationOptions']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);
const selectedItemsSelector = state => state.getIn([LOCAL_REDUCER, 'selectedItems']);

const mapListItemOperations = (operations, isFinal) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'print':
          return isFinal;
        default:
          return true;
      }
    })), OrderedMap()
  ).toJS()
);

const mapDebitCredit = (debit, credit) => {
  if (debit) return 'Дебет';
  if (credit) return 'Кредит';
  return null;
};

const mapListItem = (item, selected, operations) => Map({
  id: item.get('recordID'),
  account: getFormattedAccNum(item.get('account')),
  final: item.get('isFinal'),
  stmtDate: item.get('stmtDate') ?
    moment(item.get('stmtDate')).format('DD.MM.YYYY') :
    null,
  operDate: item.get('operDate') ?
    moment(item.get('operDate')).format('DD.MM.YYYY') :
    null,
  valDate: item.get('valDate') ?
    moment(item.get('valDate')).format('DD.MM.YYYY') :
    null,
  operType: item.get('operType'),
  operNumber: item.get('operNumber'),
  debit: !isNaN(item.get('debet')) ?
    numeral(item.get('debet')).format('0,0.00') :
    null,
  credit: !isNaN(item.get('credit')) ?
    numeral(item.get('credit')).format('0,0.00') :
    null,
  currCodeIso: item.get('currCodeIso'),
  corrName: item.get('corrName'),
  inn: item.get('inn'),
  corrAccount: getFormattedAccNum(item.get('corrAccount')),
  corrBankName: item.get('corrBankName'),
  'debit-credit': mapDebitCredit(item.get('debet'), item.get('credit')),
  paymentPurpose: item.get('paymentPurpose'),
  selected,
  operations: mapListItemOperations(operations, !!item.get('isFinal'))
});

const mapFilterConditionDictionaryItems = condition => condition.setIn(
  ['values', 'items'],
  condition.getIn(['values', 'items'], List()).map((item) => {
    switch (condition.get('id')) {
      case 'account':
        return Map({
          accNum: item.get('accNum'),
          availableBalance: !isNaN(item.get('availableBalance')) ?
            numeral(item.get('availableBalance')).format('0,0.00')
            :
            null,
          orgName: item.get('orgName'),
          account: getFormattedAccNum(item.get('accNum')),
          title: item.get('accNum'),
          value: item.get('id')
        });
      case 'currCodeIso':
        return Map({
          isoCode: item.get('isoCode'),
          name: item.get('name'),
          label: `${item.get('isoCode')} ${item.get('name')}`,
          title: item.get('isoCode'),
          value: item.get('id')
        });
      default:
        return null;
    }
  })
);

const mapFilterConditionParamsToValue = (params, condition) => {
  if (!params) return '...';
  switch (condition.get('type')) {
    case 'datesRange':
      return `${params.get('dateFrom') ?
        `с ${moment(params.get('dateFrom')).format('DD.MM.YYYY')}` : ''} ${params.get('dateTo') ?
        `по ${moment(params.get('dateTo')).format('DD.MM.YYYY')}` : ''}`;
    case 'amountsRange':
      return `${params.get('amountFrom') ? 
        `от ${numeral(params.get('amountFrom')).format('0,0.00')}` : ''} ${params.get('amountTo') ?
        `до ${numeral(params.get('amountTo')).format('0,0.00')}` : ''}`;
    case 'bool':
      return '';
    case 'multipleSelect':
      return params.join(', ');
    case 'search':
    case 'string':
      switch (condition.get('inputType')) {
        case 'Account':
          return getFormattedAccNum(params);
        default:
          return params;
      }
    default:
      return params;
  }
};

const mapFilterConditions = (conditions, savedConditions, filtersConditions) => (
  (conditions && conditions.size ? conditions : savedConditions).map(condition =>
    condition.merge({
      params: condition.get('params'),
      value: mapFilterConditionParamsToValue(
        condition.get('params'),
        filtersConditions.find(filtersCondition => filtersCondition.get('id') === condition.get('id'))
      )
    })
  )
);

export const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) => columns.map(item => item.merge({
    visible: !!settings.get('visible').find(visible => visible === item.get('id')),
    sorting: settings.getIn(['sorting', 'id']) === item.get('id') ? settings.getIn(['sorting', 'direction']) : 0,
    grouped: settings.get('grouped') === item.get('id'),
    width: settings.getIn(['width', item.get('id')])
  }))
);

export const filtersConditionsCreatedSelector = createSelector(
  filtersConditionsSelector,
  filtersConditions => filtersConditions.map(
    condition => (condition.get('type') === 'suggest' || condition.get('type') === 'search'
      ? mapFilterConditionDictionaryItems(condition)
      : condition)
  )
);

export const filtersCreatedSelector = createSelector(
  filtersSelector,
  filtersConditionsSelector,
  (filters, filtersConditions) => filters
    .map(filter => filter.merge({
      conditions: mapFilterConditions(filter.get('conditions'), filter.get('savedConditions'), filtersConditions)
    }))
    .sort((a, b) => {
      if (a.get('isPreset') && b.get('isPreset')) return 0;
      if (a.get('isPreset') && !b.get('isPreset')) return -1;
      if (!a.get('isPreset') && b.get('isPreset')) return 1;
      return a.get('title').localeCompare(b.get('title'));
    })
);

const getGroupTitle = (group, grouped) => {
  switch (grouped) {
    case 'stateType':
      return group ? 'Итоговая' : 'Внутридневная';
    default:
      return group;
  }
};

export const listCreatedSelector = createSelector(
  listSelector,
  selectedItemsSelector,
  settingsSelector,
  operationsSelector,
  (list, selectedItems, settings, operations) => {
    if (!list.size) return list;
    const mappedList = list.map(item =>
      mapListItem(
        item,
        !!selectedItems.filter(selectedItem => selectedItem === item.get('recordID')).size,
        operations
      ));
    const grouped = settings.get('grouped');
    if (!grouped) {
      return List([Map({
        id: 'notGrouped',
        selected: mappedList.filter(item => item.get('selected')).size === mappedList.size,
        items: mappedList
      })]);
    }
    return mappedList.map(item => item.get(grouped)).toOrderedSet().toList().map((group, i) => {
      const items = mappedList.filter(item => item.get(grouped) === group);
      return Map({
        id: `${grouped}_${i}`,
        title: getGroupTitle(group, grouped),
        selected: items.filter(item => item.get('selected')).size === items.size,
        items
      });
    });
  }
);

export const paginationOptionsCreatedSelector = createSelector(
  paginationOptionsSelector,
  settingsSelector,
  (paginationOptions, settings) => paginationOptions.map(item => ({
    value: item,
    title: item,
    selected: settings.get('pagination') === item
  }))
);

export const panelOperationsCreatedSelector = createSelector(
  operationsSelector,
  selectedItemsSelector,
  listSelector,
  (operations, selectedItems, list) => operations.filter((operation) => {
    const itemsWithOperations = selectedItems.map(item => Map({
      id: item,
      operations: mapListItemOperations(
        operations,
        list.find(listItem => listItem.get('recordID') === item).get('isFinal')
      ),
    }));
    return (
      itemsWithOperations.size ===
      itemsWithOperations.filter(item => !!item.get('operations')[operation.get('id')]).size
      ) &&
      (selectedItems.size === 1 || operation.get('grouped'));
  })
);

export const tabCreateSelector = createSelector(
  sectionsSelector,
  sections => sections.map(section => section.merge({
    isActive: section.get('selected')
  }))
);

const mapStateToProps = state => ({
  isListLoading: isListLoadingSelector(state),
  isExportListLoading: isExportListLoadingSelector(state),
  isListUpdateRequired: isListUpdateRequiredSelector(state),
  isReloadRequired: isReloadRequiredSelector(state),
  columns: columnsCreatedSelector(state),
  filtersConditions: filtersConditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  list: listCreatedSelector(state),
  operations: operationsSelector(state),
  pages: pagesSelector(state),
  paginationOptions: paginationOptionsCreatedSelector(state),
  panelOperations: panelOperationsCreatedSelector(state),
  sections: sectionsSelector(state),
  selectedItems: selectedItemsSelector(state),
  settings: settingsSelector(state),
  tabs: tabCreateSelector(state)
});

export default mapStateToProps;
