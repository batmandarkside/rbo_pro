/* eslint-disable react/jsx-filename-extension */
import React            from 'react';
import { expect }       from 'chai';
import { mount }        from 'enzyme';
import NoFindRouter  from '../index';

describe('<NoFindRouter /> tests', () => {
  it('Renders <NoFindRouter /> ', () => {
    const wrapper = mount(<NoFindRouter  />);
    expect(wrapper.find('h1').text()).to.contain('Такой страницы не существует!');
  });
});
