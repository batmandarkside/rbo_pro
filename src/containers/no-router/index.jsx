import React from 'react';
import './style.css';

const NoFindRouter = () => (
  <div className="b-no-router-layout">
    <h1>Такой страницы не существует!</h1>
  </div>
);

export default NoFindRouter;
