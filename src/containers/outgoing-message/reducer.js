import { fromJS } from 'immutable';
import { DOC_STATUSES } from 'constants';
import { getIsDocEditable } from 'utils';
import { ACTIONS } from './constants';

export const initialDocumentTabs = [
  {
    id: 'main',
    title: 'Основные поля',
    isActive: true
  },
  {
    id: 'info',
    title: 'Информация из банка',
    isActive: false
  }
];

export const initialFormSections = {
  primary: {
    title: 'Общая информация',
    isVisible: true,
    isCollapsible: false,
    order: 0
  },
  attachment: {
    title: 'Вложение',
    isVisible: true,
    isCollapsible: false,
    order: 1
  }
};

export const initialDocumentDictionaries = {
  organizations: {
    items: []
  },
  account: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  topic: {
    items: [],
    searchValue: '',
    isSearching: false
  }
};

export const initialOperations = [
  {
    id: 'back',
    title: 'Назад',
    icon: 'back',
    disabled: false,
    progress: false
  },
  {
    id: 'save',
    title: 'Сохранить',
    icon: 'save',
    disabled: false,
    progress: false
  },
  {
    id: 'signAndSend',
    title: 'Подписать и отправить',
    icon: 'sign-and-send',
    disabled: false,
    progress: false
  },
  {
    id: 'sign',
    title: 'Подписать',
    icon: 'sign',
    disabled: false,
    progress: false
  },
  {
    id: 'unarchive',
    title: 'Вернуть из архива',
    icon: 'unarchive',
    disabled: false,
    progress: false
  },
  {
    id: 'repeat',
    title: 'Повторить',
    icon: 'repeat',
    disabled: false,
    progress: false
  },
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false
  },
  {
    id: 'send',
    title: 'Отправить',
    icon: 'send',
    disabled: false,
    progress: false
  },
  {
    id: 'history',
    title: 'История статустов',
    icon: 'history',
    disabled: false,
    progress: false
  },
  {
    id: 'archive',
    title: 'Переместить в архив',
    icon: 'archive',
    disabled: false,
    progress: false
  },
  {
    id: 'checkSign',
    title: 'Проверить подпись',
    icon: 'check-sign',
    disabled: false,
    progress: false
  }
];

export const initialDocumentHistory = {
  isVisible: false,
  items: []
};

export const initialDocumentSignatures = {
  isVisible: false,
  items: []
};

export const initialState = fromJS({
  isMounted: false,
  isDataError: false,
  isDataLoading: true,
  isEditable: false,
  documentTabs: initialDocumentTabs,
  documentHistory: initialDocumentHistory,
  documentSignatures: initialDocumentSignatures,
  documentData: {
    attaches: []
  },
  documentErrors: [],
  documentDictionaries: initialDocumentDictionaries,
  formSections: initialFormSections,
  activeFormField: null,
  operations: initialOperations
});

export const mount = () => initialState.set('isMounted', true);

export const unmount = () => initialState.set('isMounted', false);

export const getDataRequest = state => state.merge({
  isDataLoading: true
});

export const getDataSuccess = (state, { documentData, documentErrors, documentDictionaries }) => {
  const documentDataImmutable = fromJS(documentData);
  const documentErrorsImmutable = fromJS(documentErrors).get('errors', fromJS([]));
  const documentDictionariesImmutable = fromJS(documentDictionaries);
  const status = documentDataImmutable.get('status', DOC_STATUSES.NEW);
  const isEditable = getIsDocEditable(status, !!documentDataImmutable.getIn(['allowedSmActions', 'fromArchive']));

  const firstSenderAccount = documentDictionariesImmutable.getIn(['account', 'items', 0]);
  const isChooseFirstPayerAccount = status === DOC_STATUSES.NEW
    && !documentDataImmutable.get('account')
    && documentDictionariesImmutable.getIn(['account', 'items']).size === 1;

  return state.mergeDeep({
    isDataLoading: false,
    isEditable,
    documentData: documentDataImmutable.merge({
      status,
      account: isChooseFirstPayerAccount ?
        firstSenderAccount.get('accNum') :
        documentDataImmutable.get('account', ''),
    }),
    documentErrors: documentErrorsImmutable,
    documentDictionaries: documentDictionariesImmutable
  });
};

export const getDataFail = state => state.merge({
  isDataError: true,
  isDataLoading: false
});

export const saveDataSuccess = (state, { recordID, status, allowedSmActions, errors }) => state.merge({
  documentData: state.get('documentData').merge({
    docId: recordID,
    status,
    allowedSmActions
  }),
  documentErrors: errors
});

export const saveDataFail = (state, { error }) => state.merge({
  documentErrors: error.name === 'RBO Controls Error' ? error.stack : state.get('documentErrors')
});

export const fieldFocus = (state, { fieldId }) => state.set('activeFormField', fieldId);

export const fieldBlur = (state, { fieldId }) => {
  switch (fieldId) {
    default:
      return state.set('activeFormField', null);
  }
};

export const sectionCollapseToggle = (state, { sectionId, isCollapsed }) =>
  state.setIn(['formSections', sectionId, 'isCollapsed'], isCollapsed);

const changeDataAccount = (state, { account }) => state.mergeDeep({
  documentData: {
    account: account ? account.get('accNum') : null,
  }
});

const changeDataTopic = (state, { topic }) => state.mergeDeep({
  documentData: {
    topic: topic ? topic.get('topic') : null,
    receiver: topic ? topic.get('receiver') : null,
  }
});

const changeAttaches = (state, { attaches }) => state.merge({
  documentData: state.get('documentData').merge({
    attaches: attaches.map(item => item.delete('describe'))
  })
});

export const changeData = (state, { fieldId, fieldValue }) => {
  const selectedDocumentDictionaryItems = state.getIn(['documentDictionaries', fieldId, 'items']);
  let selectedDictionaryItem;
  switch (fieldId) {
    case 'account':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataAccount(state, { account: selectedDictionaryItem });
    case 'topic':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataTopic(state, { topic: selectedDictionaryItem });
    case 'attaches':
      return changeAttaches(state, { attaches: fieldValue });
    default:
      return state.setIn(['documentData', fieldId], fieldValue);
  }
};

export const dictionarySearchAccount = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'account', 'searchValue'], dictionarySearchValue);

export const dictionarySearchTopic = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'topic', 'searchValue'], dictionarySearchValue);

export const changeTab = (state, { tabId }) => state.merge({
  documentTabs: state.get('documentTabs').map(tab => tab.set('isActive', tab.get('id') === tabId))
});

export const operationSaveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSaveFinish = state => state.mergeDeep({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSignAndSendRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSignAndSendSigningSuccess = (state, { documentData }) => state.merge({
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignAndSendSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignAndSendFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSignRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSignSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationPrintRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'print' ? operation.set('progress', true) : operation))
});

export const operationPrintFinish = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'print' ? operation.set('progress', false) : operation))
});

export const operationRemoveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationRemoveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  documentErrors: fromJS([]),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationRemoveFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSendRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSendSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSendFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationArchiveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'archive':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationArchiveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'archive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationArchiveFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'archive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationUnarchiveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'unarchive':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationUnarchiveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'unarchive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationUnarchiveFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'unarchive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationHistoryRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', true) : operation))
});

export const operationHistorySuccess = (state, payload) => state.merge({
  documentHistory: {
    isVisible: true,
    items: payload
  },
  documentSignatures: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});

export const operationHistoryFail = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});

export const operationCheckSignRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', true) : operation))
});

export const operationCheckSignSuccess = (state, payload) => state.merge({
  documentSignatures: {
    isVisible: true,
    items: payload
  },
  documentHistory: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', false) : operation))
});

export const operationCheckSignFail = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', false) : operation))
});

export const operationCloseDocumentTop = state => state.merge({
  documentHistory: {
    isVisible: false,
    items: []
  },
  documentSignatures: {
    isVisible: false,
    items: []
  }
});

function OutgoingMessageContainerReducer(state = initialState, action) {
  const isMounted = state.get('isMounted');

  switch (action.type) {
    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_MOUNT:
      return mount();

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_UNMOUNT:
      return unmount();

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_GET_DATA_SUCCESS:
      return isMounted ? getDataSuccess(state, action.payload) : state;

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_GET_DATA_FAIL:
      return isMounted ? getDataFail(state) : state;

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_SUCCESS:
      return saveDataSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_FAIL:
      return saveDataFail(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_FIELD_FOCUS:
      return fieldFocus(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_FIELD_BLUR:
      return fieldBlur(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_SECTION_COLLAPSE_TOGGLE:
      return sectionCollapseToggle(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_CHANGE_DATA:
      return changeData(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_DICTIONARY_SEARCH_ACCOUNT:
      return dictionarySearchAccount(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_DICTIONARY_SEARCH_TOPIC:
      return dictionarySearchTopic(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_CHANGE_TAB:
      return changeTab(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_REQUEST:
      return operationSaveRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_SUCCESS:
    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_FAIL:
      return operationSaveFinish(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST:
      return operationSignAndSendRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_SUCCESS:
      return operationSignAndSendSigningSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS:
      return operationSignAndSendSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_CANCEL:
    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL:
      return operationSignAndSendFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_REQUEST:
      return operationSignRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_SUCCESS:
      return operationSignSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_CANCEL:
    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_FAIL:
      return operationSignFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationPrintRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_PRINT_FAIL:
      return operationPrintFinish(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_REQUEST:
      return operationRemoveRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationRemoveSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_CANCEL:
    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationRemoveFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_REQUEST:
      return operationSendRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_SUCCESS:
      return operationSendSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_FAIL:
      return operationSendFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_ARCHIVE_REQUEST:
      return operationArchiveRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_ARCHIVE_SUCCESS:
      return operationArchiveSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_ARCHIVE_FAIL:
      return operationArchiveFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_UNARCHIVE_REQUEST:
      return operationUnarchiveRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_UNARCHIVE_SUCCESS:
      return operationUnarchiveSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_UNARCHIVE_FAIL:
      return operationUnarchiveFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_HISTORY_REQUEST:
      return operationHistoryRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_HISTORY_SUCCESS:
      return operationHistorySuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_HISTORY_FAIL:
      return operationHistoryFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_CHECK_SIGN_REQUEST:
      return operationCheckSignRequest(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS:
      return operationCheckSignSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_CHECK_SIGN_FAIL:
      return operationCheckSignFail(state);

    case ACTIONS.OUTGOING_MESSAGE_CONTAINER_CLOSE_DOCUMENT_TOP:
      return operationCloseDocumentTop(state);

    default:
      return state;
  }
}

export default OutgoingMessageContainerReducer;
