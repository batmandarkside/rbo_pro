import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import qs from 'qs';
import { goBack, push, replace } from 'react-router-redux';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import { signDocAddAndTrySigningAction as dalSignAction } from 'dal/sign/actions';
import {
  getOutgoingMessageData as dalGetData,
  getOutgoingMessageDefaultData as dalGetDefaultData,
  getOutgoingMessageCopyData as dalGetCopyData,
  getOutgoingMessageAttachments as dalGetAttachments,
  getOutgoingMessageAttachmentData as dalGetAttachmentData,
  saveOutgoingMessageData as dalSaveData,
  downloadOutgoingMessageAttachment as dalDownloadAttachment
} from 'dal/messages/sagas';
import {
  getReceiverTopics as dalGetTopics,
} from 'dal/dictionaries/sagas';
import {
  getAccounts as dalGetAccounts
} from 'dal/accounts/sagas';
import {
  print as dalOperationPrint,
  archive as dalOperationArchive,
  unarchive as dalOperationUnarchive,
  remove as dalOperationRemove,
  sendToBank as dalOperationSend,
  stateHistoryById as dalOperationHistory,
  signsCheckById as dalOperationSignsCheck
} from 'dal/documents/sagas';
import {
  printSignature as dalSignaturePrint,
  downloadSignature as dalSignatureDownload
} from 'dal/sign/sagas/operations-saga';
import { DOC_STATUSES } from 'constants';
import { getIsDocNeedGetErrors } from 'utils';
import { ACTIONS, LOCAL_REDUCER, LOCAL_ROUTER_ALIASES } from './constants';

export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
export const docIdSelector = state => state.getIn([LOCAL_REDUCER, 'documentData', 'docId']);
export const documentSignaturesSelector = state => state.getIn([LOCAL_REDUCER, 'documentSignatures']);

export function* successNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'success',
    title: title || 'Операция выполнена успешно',
    message: message || ''
  }));
}

export function* warningNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'warning',
    title: title || '',
    message: message || ''
  }));
}

export function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

export function* confirmNotification(params) {
  const { level, title, message, success, cancel, autoDismiss } = params;
  yield put(dalNotificationAction({
    type: 'confirm',
    level: level || 'success',
    title: title || '',
    message: message || '',
    autoDismiss: typeof autoDismiss === 'number' ? autoDismiss : 20,
    success,
    cancel
  }));
}

export function* refreshData() {
  const id = yield select(docIdSelector);
  try {
    return yield call(dalGetData, { payload: { id } });
  } catch (error) {
    throw error;
  }
}

export function* mountSaga(action) {
  const { payload: { params: { id } } } = action;
  yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_GET_DATA_REQUEST, payload: { id } });
}

export function* getDataSaga(action) {
  const { payload: { id } } = action;
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const type = query.type;

  let dalGetDataGenerator;
  let dalGetDataParams;
  if (id === 'new' && type && type === 'repeat' && query.itemId) {
    dalGetDataGenerator = dalGetCopyData;
    dalGetDataParams = { payload: { id: query.itemId } };
  } else if (id === 'new') {
    dalGetDataGenerator = dalGetDefaultData;
    dalGetDataParams = {};
  } else {
    dalGetDataGenerator = dalGetData;
    dalGetDataParams = { payload: { id } };
  }

  try {
    const organizations = yield select(organizationsSelector);
    const [
      documentData,
      account,
      topic,
    ] = yield all([
      dalGetDataGenerator(dalGetDataParams),
      dalGetAccounts({ payload: {} }),
      dalGetTopics()
    ]);

    const attaches = documentData.attachExists
      ? yield call(dalGetAttachments, { payload: { id } })
      : [];

    const attachmentsData = yield all(
      attaches.map(attach => call(dalGetAttachmentData, { payload: { fileId: attach.id } }))
    );

    documentData.attaches = attaches.map((attach, index) => ({
      ...attach,
      content: attachmentsData[index]
    }));

    const documentErrors = (getIsDocNeedGetErrors(documentData.status))
      ? yield call(dalSaveData, { payload: documentData })
      : {};

    yield put({
      type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_GET_DATA_SUCCESS,
      payload: {
        documentData,
        documentErrors,
        documentDictionaries: {
          organizations: { items: organizations },
          account: { items: account },
          topic: { items: topic }
        }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_GET_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Документ не найден.',
      message: 'Неверно указан идентификатор документа.'
    });
  }
}

export function* saveDataSaga(action) {
  const { channels } = action;
  const documentData = yield select(documentDataSelector);
  const allowedSave = documentData.getIn(['allowedSmActions', 'save']);
  const status = documentData.get('status');
  if (status === DOC_STATUSES.NEW || allowedSave) {
    try {
      const result = yield call(dalSaveData, { payload: documentData.toJS() });
      if (status === DOC_STATUSES.NEW) yield put(replace(LOCAL_ROUTER_ALIASES.MESSAGE.replace('{:id}', result.recordID)));
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_SUCCESS, payload: result, channels });
    } catch (error) {
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_FAIL, payload: { error }, channels });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'doNotAllowed' } });
  }
}

export function* saveDataSuccessSaga(action) {
  const { payload: { errors }, channels } = action;
  if (errors && errors.length) {
    if (errors.filter(error => error.level === '2').length) {
      yield put({ type: channels.success, payload: { status: 'haveErrors' } });
    } else if (errors.filter(error => error.level === '1').length) {
      yield put({ type: channels.success, payload: { status: 'haveWarnings' } });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'success' } });
  }
}

export function* saveDataFailSaga(action) {
  const { payload: { error }, channels } = action;
  yield put({ type: channels.fail });
  yield call(errorNotification, error.name === 'RBO Controls Error'
    ? {
      title: 'Документ не может быть сохранен.',
      message: 'Для сохранения и подтверждения документа требуется исправить ошибки. ' +
      'При наличии блокирующих ошибок документ не может быть сохранен.'
    }
    : {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно сохранить документ'
    }
  );
}

export function* dictionarySearchSaga(action) {
  const { payload: { dictionaryId, dictionarySearchValue } } = action;
  switch (dictionaryId) {
    case 'account':
      yield put({
        type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_DICTIONARY_SEARCH_ACCOUNT,
        payload: { dictionarySearchValue }
      });
      break;
    case 'topic':
      yield put({
        type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_DICTIONARY_SEARCH_TOPIC,
        payload: { dictionarySearchValue }
      });
      break;
    default:
      break;
  }
}

export function* operationSaga(action) {
  const { payload: { operationId } } = action;
  switch (operationId) {
    case 'back':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_BACK });
      break;
    case 'save':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_REQUEST });
      break;
    case 'signAndSend':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST });
      break;
    case 'sign':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_REQUEST });
      break;
    case 'send':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_REQUEST });
      break;
    case 'repeat':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REPEAT });
      break;
    case 'print':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_PRINT_REQUEST });
      break;
    case 'remove':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_REQUEST });
      break;
    case 'archive':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_ARCHIVE_REQUEST });
      break;
    case 'unarchive':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_UNARCHIVE_REQUEST });
      break;
    case 'history':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_HISTORY_REQUEST });
      break;
    case 'checkSign':
      yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_CHECK_SIGN_REQUEST });
      break;
    default:
      break;
  }
}

export function* operationBackSaga() {
  const location = yield select(routingLocationSelector);
  const prevLocation = location.get('prevLocation');
  if (prevLocation && prevLocation.get('pathname') !== location.get('pathname')) yield put(goBack());
  else yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
}

export function* operationSaveSaga() {
  yield put({
    type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_SUCCESS,
      fail: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_FAIL
    }
  });
}

export function* operationSaveSuccessSaga(action) {
  const { payload: { status } } = action;
  switch (status) {
    case 'haveErrors':
      yield call(errorNotification, {
        title: 'Документ сохранен с ошибками.',
        message: 'Для подтверждения документа требуется исправить ошибки.'
      });
      break;
    case 'haveWarnings':
      yield call(warningNotification, {
        title: 'Документ сохранен с предупреждениями.',
        message: 'Перед подтверждением документа обратите внимание на предупреждения из банка.'
      });
      break;
    default:
      yield call(confirmNotification, {
        level: 'success',
        title: 'Документ успешно сохранен.',
        success: {
          label: 'Подписать',
          action: { type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_REQUEST }
        }
      });
  }
}

export function* operationSignAndSendSaga() {
  yield put({
    type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_CONTINUE,
      fail: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL
    }
  });
}

export function* operationSignAndSendContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      channel: {
        cancel: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_CANCEL,
        success: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_SUCCESS,
        fail: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_FAIL
      }
    }));
  }
}

export function* operationSignAndSendSigningDalCancelSaga() {
  yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_CANCEL });
}

export function* operationSignAndSendSigningDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_SUCCESS,
      payload: { documentData: result }
    });
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SENDING_REQUEST });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationSignAndSendSigningDalFailSaga() {
  yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
}

export function* operationSignAndSendSendingRequestSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationSend, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно подписан и отправлен в банк.' });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно отправить документ в банк'
    });
  }
}

export function* operationSignSaga() {
  yield put({
    type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_CONTINUE,
      fail: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_FAIL
    }
  });
}

export function* operationSignContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      channel: {
        cancel: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_DAL_CANCEL,
        success: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_DAL_SUCCESS,
        fail: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_DAL_FAIL
      }
    }));
  }
}

export function* operationSignDalCancelSaga() {
  yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_CANCEL });
}

export function* operationSignDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_SUCCESS, payload: { documentData: result } });
    yield call(confirmNotification, {
      level: 'success',
      title: 'Документ успешно подписан.',
      success: {
        label: 'Отправить',
        action: { type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_REQUEST }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationSignDalFailSaga() {
  yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_FAIL });
}

export function* operationRepeatSaga() {
  const docId = yield select(docIdSelector);
  yield put(push({ pathname: LOCAL_ROUTER_ALIASES.MESSAGE_NEW, search: `?type=repeat&itemId=${docId}` }));
}

export function* operationPrintSaga() {
  const documentData = yield select(documentDataSelector);
  const docId = yield select(docIdSelector);
  const docNumber = documentData.get('docNumber');
  const docDate = documentData.get('docDate');
  try {
    yield call(dalOperationPrint, { payload: { docTitle: 'message_to_bank', docId, docNumber, docDate } });
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_PRINT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_PRINT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно распечатать документ'
    });
  }
}

export function* operationRemoveSaga() {
  yield call(confirmNotification, {
    level: 'error',
    title: 'Удаление документа',
    message: 'Вы действительно хотите удалить документ?',
    autoDismiss: 0,
    success: {
      label: 'Подтвердить',
      action: { type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_CONFIRM }
    },
    cancel: {
      label: 'Отмена',
      action: { type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_CANCEL }
    }
  });
}

export function* operationRemoveConfirmSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationRemove, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно удален.' });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно удалить документ'
    });
  }
}

export function* operationSendSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationSend, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно отправлен в банк.' });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно отправить документ в банк'
    });
  }
}

export function* operationArchiveSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationArchive, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ перемещен в архив.' });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_ARCHIVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно переместить документ в архив'
    });
  }
}

export function* operationUnarchiveSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationUnarchive, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_UNARCHIVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ возвращен из архива.' });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_UNARCHIVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно вернуть документ из архива'
    });
  }
}

export function* operationHistorySaga() {
  const docId = yield select(docIdSelector);
  try {
    const result = yield call(dalOperationHistory, docId);
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_HISTORY_SUCCESS, payload: result });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_HISTORY_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить историю статустов'
    });
  }
}

export function* operationCheckSignSaga() {
  const docId = yield select(docIdSelector);
  try {
    const result = yield call(dalOperationSignsCheck, docId);
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS, payload: result });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_CHECK_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно проверить подписи'
    });
  }
}

export function* signaturePrintSaga(action) {
  const docId = yield select(docIdSelector);
  const documentSignatures = yield select(documentSignaturesSelector);
  const { payload: { signId } } = action;
  const signature = documentSignatures.get('items').find(item => item.get('signId') === signId);
  const signDate = signature.get('signDateTime');
  try {
    yield call(dalSignaturePrint, { payload: { docId, signId, signDate } });
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SIGNATURE_PRINT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SIGNATURE_PRINT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно распечатать подпись'
    });
  }
}

export function* signatureDownloadSaga(action) {
  const docId = yield select(docIdSelector);
  const documentSignatures = yield select(documentSignaturesSelector);
  const { payload: { signId } } = action;
  const signature = documentSignatures.get('items').find(item => item.get('signId') === signId);
  const signDate = signature.get('signDateTime');
  try {
    yield call(dalSignatureDownload, { payload: { docId, signId, signDate } });
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SIGNATURE_DOWNLOAD_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_SIGNATURE_DOWNLOAD_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно скачать подпись'
    });
  }
}

export function* downloadAttachmentSaga(action) {
  const { payload: { fileId } } = action;
  const documentData = yield select(documentDataSelector);
  try {
    const fileName = documentData.get('attaches').find(attachment => attachment.get('id') === fileId).get('name');
    yield call(dalDownloadAttachment, { payload: { fileId, fileName } });
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_DOWNLOAD_ATTACHMENT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGE_CONTAINER_DOWNLOAD_ATTACHMENT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно скачать файл'
    });
  }
}

export default function* saga() {
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_MOUNT, mountSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_GET_DATA_REQUEST, getDataSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_REQUEST, saveDataSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_SUCCESS, saveDataSuccessSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_SAVE_DATA_FAIL, saveDataFailSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_DICTIONARY_SEARCH, dictionarySearchSaga);

  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION, operationSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_BACK, operationBackSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_REQUEST, operationSaveSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST, operationSignAndSendSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_CONTINUE, operationSignAndSendContinueSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_CANCEL, operationSignAndSendSigningDalCancelSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_SUCCESS, operationSignAndSendSigningDalSuccessSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_FAIL, operationSignAndSendSigningDalFailSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_AND_SEND_SENDING_REQUEST, operationSignAndSendSendingRequestSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_REQUEST, operationSignSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_CONTINUE, operationSignContinueSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_DAL_CANCEL, operationSignDalCancelSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_DAL_SUCCESS, operationSignDalSuccessSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SIGN_DAL_FAIL, operationSignDalFailSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REPEAT, operationRepeatSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_PRINT_REQUEST, operationPrintSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemoveSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_SEND_REQUEST, operationSendSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_ARCHIVE_REQUEST, operationArchiveSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_UNARCHIVE_REQUEST, operationUnarchiveSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_HISTORY_REQUEST, operationHistorySaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_OPERATION_CHECK_SIGN_REQUEST, operationCheckSignSaga);

  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_SIGNATURE_PRINT_REQUEST, signaturePrintSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_SIGNATURE_DOWNLOAD_REQUEST, signatureDownloadSaga);

  yield takeLatest(ACTIONS.OUTGOING_MESSAGE_CONTAINER_DOWNLOAD_ATTACHMENT_REQUEST, downloadAttachmentSaga);
}
