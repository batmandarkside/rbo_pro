import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldFiles
} from 'components/ui-components/rbo-form-compact';

const OutgoingMessageSectionAttachment = (props) => {
  const {
    isEditable,
    documentData,
    formWarnings,
    formErrors,
    attachmentsTotalSize,
    onFieldFocus,
    onFieldBlur,
    onFieldChange,
    onFileDownload
  } = props;

  const sectionNote = attachmentsTotalSize ? `Общий объем: ${attachmentsTotalSize}` : '';

  return (
    <RboFormSection
      id="attachment"
      title="Вложение"
      note={sectionNote}
    >
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldFiles
                  id="attaches"
                  values={documentData.get('attaches')}
                  isWarning={formWarnings.get('attaches')}
                  isError={formErrors.get('attaches')}
                  maxFileSize={10485760}
                  maxFilesQty={1}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                  onFileDownload={onFileDownload}
                  isDisabled={!isEditable}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

OutgoingMessageSectionAttachment.propTypes = {
  isEditable: PropTypes.bool,
  documentData: ImmutablePropTypes.map.isRequired,
  formWarnings: ImmutablePropTypes.map.isRequired,
  formErrors: ImmutablePropTypes.map.isRequired,
  attachmentsTotalSize: PropTypes.string,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  onFileDownload: PropTypes.func.isRequired
};

export default OutgoingMessageSectionAttachment;
