import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldSearch,
  RboFormFieldTextarea,
  RboFormFieldValue
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

const OutgoingMessageSectionPrimary = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange
  } = props;

  return (
    <RboFormSection id="primary">
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldSearch
                  id="account"
                  label="Счет отправителя"
                  inputType="Account"
                  value={documentData.get('account')}
                  searchValue={documentDictionaries.getIn(['account', 'searchValue'])}
                  options={documentDictionaries.getIn(['account', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('account')}
                  isError={formErrors.get('account')}
                  isSearching={documentDictionaries.getIn(['account', 'isSearching'])}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="account"
                  label="Счет отправителя"
                  value={documentValues.get('account')}
                  isWarning={formWarnings.get('account')}
                  isError={formErrors.get('account')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="25%">
                <RboFormFieldValue
                  id="docNumber"
                  label="Номер"
                  value={documentValues.get('docNumber')}
                  isWarning={formWarnings.get('docNumber')}
                  isError={formErrors.get('docNumber')}
                />
              </RboFormCell>
              <RboFormCell size="35%">
                <RboFormFieldValue
                  id="docDate"
                  label="Дата"
                  value={documentValues.get('docDate')}
                  isWarning={formWarnings.get('docDate')}
                  isError={formErrors.get('docDate')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id="branchShortName"
                  label="Кому"
                  value={documentValues.get('branchShortName')}
                  isWarning={formWarnings.get('branchShortName')}
                  isError={formErrors.get('branchShortName')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id="orgName"
                  label="От кого"
                  value={documentValues.get('orgName')}
                  isWarning={formWarnings.get('orgName')}
                  isError={formErrors.get('orgName')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldSearch
                  id="topic"
                  label="Тема сообщения"
                  inputType="Text"
                  value={documentData.get('topic')}
                  searchValue={documentDictionaries.getIn(['topic', 'searchValue'])}
                  options={documentDictionaries.getIn(['topic', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('topic')}
                  isError={formErrors.get('topic')}
                  isSearching={documentDictionaries.getIn(['topic', 'isSearching'])}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="topic"
                  label="Тема сообщения"
                  value={documentValues.get('topic')}
                  isWarning={formWarnings.get('topic')}
                  isError={formErrors.get('topic')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id="receiver"
                  label="Получатель"
                  value={documentValues.get('receiver')}
                  isWarning={formWarnings.get('receiver')}
                  isError={formErrors.get('receiver')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="message"
                  label="Сообщение"
                  value={documentData.get('message')}
                  isWarning={formWarnings.get('message')}
                  isError={formErrors.get('message')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="message"
                  label="Сообщение"
                  value={documentValues.get('message')}
                  isWarning={formWarnings.get('message')}
                  isError={formErrors.get('message')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

OutgoingMessageSectionPrimary.propTypes = {
  isEditable: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default OutgoingMessageSectionPrimary;
