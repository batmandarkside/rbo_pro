import { createSelector } from 'reselect';
import { Map } from 'immutable';
import moment from 'moment-timezone';
import numeral from 'numeral';
import { DOC_STATUSES, SIGN_VALIDATION_STATUSES } from 'constants';
import {
  regExpEscape,
  getDocStatusClassNameMode,
  getFormattedAccNum,
  getDocFormErrors,
  getMappingDocSectionsAndErrors,
  getSignType,
  getMimeTypeTitleByMimeType,
  getFormattedFileSize,
  getFormattedFilesTotalSize
} from 'utils';
import { LOCAL_REDUCER } from './constants';
import {
  documentFieldsMap,
  documentSectionsFieldsMap,
  documentErrorsMap,
  getBankInfo
} from './utils';

const isDataErrorSelector = state => state.getIn([LOCAL_REDUCER, 'isDataError']);
const isDataLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isDataLoading']);
const isEditableSelector = state => state.getIn([LOCAL_REDUCER, 'isEditable']);
const documentTabsSelector = state => state.getIn([LOCAL_REDUCER, 'documentTabs']);
const documentHistorySelector = state => state.getIn([LOCAL_REDUCER, 'documentHistory']);
const documentSignaturesSelector = state => state.getIn([LOCAL_REDUCER, 'documentSignatures']);
const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
const documentErrorsSelector = state => state.getIn([LOCAL_REDUCER, 'documentErrors']);
const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
const formSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'formSections']);
const activeFormFieldSelector = state => state.getIn([LOCAL_REDUCER, 'activeFormField']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);

const getDictionaryId = (value, paramName, dictionary, idName) => {
  const selectedItem = dictionary.get('items').find(item => item.get(paramName) === value);
  return selectedItem ? selectedItem.get(idName) : null;
};

const documentHistoryCreatedSelector = createSelector(
  documentHistorySelector,
  documentHistory => documentHistory.set('items', documentHistory.get('items')
    .reverse()
    .map(item => Map({
      date: item.get('date') ? moment(item.get('date')).format('DD.MM.YYYY HH:mm:ss') : '',
      user: item.get('user'),
      endState: item.get('endState'),
      statusClassName: getDocStatusClassNameMode(item.get('endState'))
    }))
  )
);

const documentSignaturesCreatedSelector = createSelector(
  documentSignaturesSelector,
  documentSignatures => documentSignatures.set('items', documentSignatures.get('items').map(item => Map({
    signId: item.get('signId'),
    signDateTime: item.get('signDateTime') ? moment(item.get('signDateTime')).format('DD.MM.YYYY HH:mm:ss') : '',
    userLogin: item.get('userLogin'),
    userName: item.get('userName'),
    userPosition: item.get('userPosition'),
    signType: getSignType(item.get('signType')),
    signValid: item.get('signValid') ? SIGN_VALIDATION_STATUSES.VALID : SIGN_VALIDATION_STATUSES.INVALID,
    signHash: item.get('signHash')
  })))
);

const documentValuesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => {
    const docNumber = documentData.get('docNumber') || '';
    const docDate = documentData.get('docDate') ? moment(documentData.get('docDate')).format('DD.MM.YYYY') : '';
    const title = `Письмо в банк${docNumber ? ` № ${docNumber}` : ''}`;
    const selectedAccount = documentDictionaries.getIn(['account', 'items'])
      .find(item => item.get('accNum') === documentData.get('account'));
    const branchShortName = selectedAccount && selectedAccount.get('branchName');

    return Map({
      title,
      status: documentData.get('status', ''),
      account: getFormattedAccNum(documentData.get('account', '')),
      docNumber,
      docDate,
      branchShortName,
      orgName: documentData.get('orgName'),
      topic: documentData.get('topic'),
      receiver: documentData.get('receiver'),
      message: documentData.get('message'),

      bankInfo: getBankInfo(documentData)
    });
  }
);

const documentDataCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => Map({
    account: getDictionaryId(
      documentData.get('account'), 'accNum',
      documentDictionaries.get('account'), 'id'
    ),
    topic: getDictionaryId(
      documentData.get('topic'), 'topic',
      documentDictionaries.get('topic'), 'id'
    ),
    message: documentData.get('message'),
    attaches: documentData.get('attaches')
      .map((attachment) => {
        const mimeTypeTitle = getMimeTypeTitleByMimeType(attachment.get('mimeType'));
        const size = attachment.get('size') ? getFormattedFileSize(attachment.get('size')) : '';
        const created = attachment.get('date') ? `создан: ${moment(attachment.get('date')).format('DD.MM.YYYY')}` : '';
        const describe = `${mimeTypeTitle}${size ? `, ${size}` : ''}${created ? `, ${created}` : ''}`;
        const isDownloadable = !!attachment.get('content');
        return attachment.merge({ describe, isDownloadable });
      })
  })
);

const documentErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  activeFormFieldSelector,
  (documentErrors, activeFormField) => documentErrors
    .map(error => error.merge({
      fieldId: documentErrorsMap[error.get('id')] && documentErrorsMap[error.get('id')][0],
      fieldsIds: documentErrorsMap[error.get('id')],
      isActive: documentErrorsMap[error.get('id')] &&
        !!documentErrorsMap[error.get('id')].find(i => i === activeFormField)
    }))
    .filter(error => !!error.get('fieldId'))
    .sort((a, b) => documentFieldsMap.indexOf(a.get('fieldId')) - documentFieldsMap.indexOf(b.get('fieldId')))
);

const documentStructureCreatedSelector = createSelector(
  documentTabsSelector,
  formSectionsSelector,
  documentErrorsCreatedSelector,
  activeFormFieldSelector,
  (documentTabs, formSections, documentErrors, activeFormField) => documentTabs.map((tab) => {
    switch (tab.get('id')) {
      case 'main':
        return tab.set('sections',
          getMappingDocSectionsAndErrors(formSections, documentSectionsFieldsMap, documentErrors, activeFormField));
      default:
        return tab;
    }
  })
);

const documentDictionariesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => documentDictionaries.map((dictionary, key) => {
    let searchRegExp;
    const items = dictionary.get('items');
    const searchValue = regExpEscape(dictionary.get('searchValue'));
    switch (key) {
      case 'account':
        searchRegExp = new RegExp(`^${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('accNum').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('accNum'),
              account: getFormattedAccNum(item.get('accNum')),
              availableBalance: !isNaN(item.get('availableBalance')) ?
                numeral(item.get('availableBalance')).format('0,0.00') :
                null,
              orgName: item.get('orgName')
            }))
        });
      case 'topic':
        searchRegExp = new RegExp(`(^|\\s)${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('topic').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('topic'),
              receiver: item.get('receiver')
            }))
        });
      default:
        return dictionary;
    }
  })
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['1'])
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['2', '3'])
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  documentDataSelector,
  (operations, documentData) => {
    const status = documentData.get('status');
    const allowedSmActions = documentData.get('allowedSmActions', Map());
    return operations.filter((operation) => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'back':
          return true;
        case 'save':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save');
        case 'signAndSend':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save') || allowedSmActions.get('sign');
        case 'sign':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('sign');
        case 'unarchive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('fromArchive');
        case 'repeat':
          return status !== DOC_STATUSES.NEW;
        case 'print':
          return status !== DOC_STATUSES.NEW;
        case 'remove':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('delete');
        case 'send':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('send');
        case 'history':
          return status !== DOC_STATUSES.NEW;
        case 'archive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('archive');
        case 'checkSign':
          return !![
            DOC_STATUSES.DELIVERED,
            DOC_STATUSES.PROCESSED,
            DOC_STATUSES.EXECUTED,
            DOC_STATUSES.DECLINED,
            DOC_STATUSES.RECALLED,
            DOC_STATUSES.INVALID_PROPS,
            DOC_STATUSES.SIGNED,
            DOC_STATUSES.ACCEPTED,
            DOC_STATUSES.PARTLY_SIGNED,
            DOC_STATUSES.INVALID_SIGN
          ].find(v => v === status);
        case 'saveAsTemplate':
          return status !== DOC_STATUSES.NEW;
        default:
          return false;
      }
    });
  }
);

const documentAttachmentsTotalSizeCreatedSelector = createSelector(
  documentDataSelector,
  documentData => getFormattedFilesTotalSize(documentData.get('attaches').map(attach => attach.get('size')))
);

const mapStateToProps = state => ({
  isDataError: isDataErrorSelector(state),
  isDataLoading: isDataLoadingSelector(state),
  isEditable: isEditableSelector(state),
  documentTabs: documentTabsSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentHistory: documentHistoryCreatedSelector(state),
  documentSignatures: documentSignaturesCreatedSelector(state),
  documentValues: documentValuesCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentDictionaries: documentDictionariesCreatedSelector(state),
  formSections: formSectionsSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  operations: operationsCreatedSelector(state),
  documentAttachmentsTotalSize: documentAttachmentsTotalSizeCreatedSelector(state)
});

export default mapStateToProps;
