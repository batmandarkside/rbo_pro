export const documentFieldsMap = [
  'account',
  'docNumber',
  'docDate',
  'branchShortName',
  'orgName',
  'topic',
  'receiver',
  'message',
  'attaches'
];

export const documentSectionsFieldsMap = {
  primary: [
    'account',
    'docNumber',
    'docDate',
    'branchShortName',
    'orgName',
    'topic',
    'receiver',
    'message'
  ],
  attachment: [
    'attaches'
  ]
};

export const documentErrorsMap = {
  '1.1': ['account'],
  '1.10.1': ['docNumber'],
  '1.10.2': ['docNumber'],
  '1.11': ['message', 'receiver'],
  '1.12': ['docDate'],
  '1.14': ['account'],
  '1.2': ['account'],
  '1.3': ['account'],
  '1.3.1': ['account'],
  '1.4': ['message'],
  '1.5': ['message'],
  '1.6': ['topic', 'receiver'],
  '1.7': ['topic'],
  '1.7.1': ['topic'],
  '1.8': ['message'],
  '1.9': ['docNumber'],
  '2.1': ['attaches'],
  '2.2': ['attaches'],
  '2.3': ['attaches'],
  '2.4': ['attaches'],
  '3.1': ['account'],
  '4.1': ['account']
};

export const getBankInfo = documentData => (
  !documentData.get('bankMessage')
    ? null
    : [
      { label: 'Сообщение из Банка', value: documentData.get('bankMessage') }
    ]
);
