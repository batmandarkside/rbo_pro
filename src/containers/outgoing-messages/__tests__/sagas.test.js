import { put, call, select } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import { push } from 'react-router-redux';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction,
  addConfirmNotification as dalAddConfirmNotification
} from 'dal/notification/actions';
import {
  getOutgoingMessages as dalGetList,
  exportOutgoingMessagesListToExcel as dalExportOutgoingMessagesListToExcel,
  archiveMessages as dalOperationArchive,
  unarchiveMessages as dalOperationUnarchive,
  removeMessages as dalOperationRemove
} from 'dal/messages/sagas';
import { signDocAddAndTrySigningAction } from 'dal/sign/actions';
import {
  documentSendToBankSaga,
  print as dalOperationPrint
} from 'dal/documents/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import { ACTIONS, SETTINGS_ALIAS, LOCAL_REDUCER } from './../constants';
import { initialSettings, initialPages, initialColumns, initialSections, initialFilters } from './../reducer';
import {
  routingLocationSelector,
  profileSettingsSelector,
  localSectionsSelector,
  localSettingsSelector,
  localFiltersSelector,
  localPagesSelector,
  localOperationInProgressIdSelector,

  getSectionIndex,
  getSettings,
  getQueryParams,
  changeSection,
  changePage,
  reloadPages,
  changeFilters,
  getDictionaryForFilters,
  saveSettings,
  getList,
  exportList,
  changeSettingsWidth,
  changeSettingsColumns,
  changeSettingsPagination,
  newMessage,
  routeToItem,
  operation,
  operationPrint,
  operationArchive,
  operationUnarchive,
  operationRemove,
  operationRemoveConfirm,
  operationSign,
  operationSend,
  operationRepeat,
  successNotification,
  errorNotification
} from '../sagas';

describe('OutgoingMessagesContainer Sagas', () => {
  const state = fromJS({
    routing: {
      locationBeforeTransitions: {
        pathname: '/messages/outgoing',
        query: {
          page: 1
        },
        currentLocation: '/messages/outgoing'
      }
    },
    profile: {
      settings: {
        [SETTINGS_ALIAS]: {
          outgoing: initialSettings
        }
      }
    },
    [LOCAL_REDUCER]: {
      settings: initialSettings,
      pages: initialPages,
      sections: initialSections,
      filters: initialFilters
    }
  });

  describe('getSectionIndex', () => {
    it('success', () => {
      const generator = getSectionIndex();

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: 1 })
      );

      expect(
        generator.next(put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: 0 })).value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SETTINGS_REQUEST })
      );
    });
  });

  describe('getSettings', () => {
    it('success', () => {
      const generator = getSettings();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SETTINGS_SUCCESS, payload: initialSettings })
      );
    });
  });

  describe('getQueryParams', () => {
    it('success', () => {
      const generator = getQueryParams();

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_SUCCESS,
          payload: { pageIndex: 0 }
        })
      );
    });
  });

  describe('changeSection', () => {
    it('success', () => {
      const generator = changeSection({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_SECTION, payload: 'archive' });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        put(push({ pathname: '/messages/outgoing/archive', search: null }))
      );
    });
  });

  describe('changePage', () => {
    it('success', () => {
      const generator = changePage({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_PAGE, payload: 1 });

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );
    });
  });

  describe('reloadPages', () => {
    it('success', () => {
      const generator = reloadPages();
      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        put(push({
          pathname: '/messages/outgoing',
          search: '?page=1'
        }))
      );
    });
  });

  describe('changeFilters', () => {
    it('success', () => {
      const generator = changeFilters({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_FILTERS,
        payload: {
          filters: fromJS([
            {
              id: '1',
              title: 'Title 1',
              isPreset: true,
              savedConditions: fromJS(['1', '2'])
            },
            {
              id: '2',
              title: 'Title 2',
              isPreset: false,
              savedConditions: fromJS(['3', '4'])
            }
          ]),
          isSaveSettingRequired: true
        }
      });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            filters: [
              {
                id: '2',
                title: 'Title 2',
                isPreset: false,
                savedConditions: ['3', '4']
              }
            ]
          })
        })
      );
    });
  });

  describe('getDictionaryForFilters', () => {
    it('success', () => {
      const generator = getDictionaryForFilters({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST,
        payload: { conditionId: 'account', value: '1234' }
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetAccounts, { payload: { accNum: '1234' } })
      );

      expect(
        generator.next(['1', '2', '3']).value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
          payload: { conditionId: 'account', items: ['1', '2', '3'] }
        })
      );
    });

    it('failure', () => {
      const generator = getDictionaryForFilters({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST,
        payload: { conditionId: 'account', value: '1234' }
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetAccounts, { payload: { accNum: '1234' } })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
          error: 'Произошла ошибка на сервере'
        })
      );
    });
  });

  describe('saveSettings', () => {
    it('success', () => {
      const generator = saveSettings({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
        payload: fromJS(initialSettings)
      });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        call(dalSaveSettings, {
          payload: {
            settings: {
              [SETTINGS_ALIAS]: {
                outgoing: initialSettings
              }
            }
          }
        })
      );

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_SUCCESS, payload: fromJS(initialSettings) })
      );
    });

    it('failure', () => {
      const generator = saveSettings({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: fromJS(initialSettings) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        call(dalSaveSettings, {
          payload: {
            settings: {
              [SETTINGS_ALIAS]: {
                outgoing: initialSettings
              }
            }
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('getList', () => {
    it('success', () => {
      const generator = getList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localPagesSelector)
      );

      expect(
        generator.next(localPagesSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalGetList, {
          payload: {
            section: '',
            offset: 0,
            offsetStep: 31,
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            topic: null,
            receiver: null,
            sender: null,
            text: null,
            account: null,
            status: null
          }
        })
      );

      expect(
        generator.next([1]).value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_SUCCESS, payload: [1] })
      );
    });

    it('failure', () => {
      const generator = getList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localPagesSelector)
      );

      expect(
        generator.next(localPagesSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalGetList, {
          payload: {
            section: '',
            offset: 0,
            offsetStep: 31,
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            topic: null,
            receiver: null,
            sender: null,
            text: null,
            account: null,
            status: null
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('exportList', () => {
    it('success', () => {
      const generator = exportList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalExportOutgoingMessagesListToExcel, {
          payload: {
            section: '',
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            topic: null,
            receiver: null,
            sender: null,
            text: null,
            account: null,
            status: null,
            visibleColumns: ['number', 'attach', 'date', 'status', 'receiver', 'topic', 'account', 'orgName']
          }
        })
      );

      expect(
        generator.next([1]).value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_SUCCESS, payload: [1] })
      );
    });

    it('failure', () => {
      const generator = exportList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalExportOutgoingMessagesListToExcel, {
          payload: {
            section: '',
            sort: 'date',
            desc: true,
            from: null,
            to: null,
            topic: null,
            receiver: null,
            sender: null,
            text: null,
            account: null,
            status: null,
            visibleColumns: ['number', 'attach', 'date', 'status', 'receiver', 'topic', 'account', 'orgName']
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('changeSettingsWidth', () => {
    it('success', () => {
      const generator = changeSettingsWidth({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_RESIZE_COLUMNS, payload: fromJS(initialColumns).map(column => column.set('width', 300)) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            width: {
              number: 300,
              attach: 300,
              date: 300,
              status: 300,
              receiver: 300,
              topic: 300,
              account: 300,
              orgName: 300
            }
          })
        })
      );
    });
  });

  describe('changeSettingsColumns', () => {
    it('success', () => {
      const generator = changeSettingsColumns({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_COLUMN, payload: fromJS(initialColumns).update(0, column => column.set('sorting', 1)) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            visible: [],
            sorting: { id: 'number', direction: 1 }
          })
        })
      );
    });
  });

  describe('changeSettingsPagination', () => {
    it('success', () => {
      const generator = changeSettingsPagination({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_PAGINATION, payload: 60 });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            pagination: 60
          })
        })
      );
    });
  });

  describe('newMessage', () => {
    it('success', () => {
      const generator = newMessage({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_NEW_MESSAGE
      });
      expect(
        generator.next().value
      ).toEqual(
        put(push({
          pathname: '/messages/outgoing/new',
          search: '?'
        }))
      );
    });
  });

  describe('routeToItem', () => {
    it('success', () => {
      const generator = routeToItem({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_ROUTE_TO_ITEM, payload: 'itemId' });
      expect(
        generator.next().value
      ).toEqual(
        put(push('/messages/outgoing/itemId'))
      );
    });
  });

  describe('operation', () => {
    it('success', () => {
      const generator = operation({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION,
        payload: { operationId: 'print', items: ['0', '1', '2'] }
      });

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_REQUEST, payload: ['0', '1', '2'] })
      );
    });
  });

  describe('operationPrint', () => {
    it('success', () => {
      const generator = operationPrint({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_REQUEST,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationPrint, {
          payload: { docTitle: 'messages_to_bank', docId: ['0', '1', '2'] }
        })
      );

      expect(
        generator.next(['0', '1', '2']).value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_SUCCESS, payload: ['0', '1', '2'] })
      );
    });

    it('failure', () => {
      const generator = operationPrint({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_REQUEST,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationPrint, {
          payload: { docTitle: 'messages_to_bank', docId: ['0', '1', '2'] }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('operationArchive', () => {
    it('success', () => {
      const generator = operationArchive({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_REQUEST,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationArchive, ['0', '1', '2'])
      );

      expect(
        generator.next().value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
          payload: 'Письма помещены в архив'
        })
      );
    });

    it('failure', () => {
      const generator = operationArchive({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_REQUEST,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationArchive, ['0', '1', '2'])
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_FAIL,
          error: 'Произошла ошибка на сервере'
        })
      );
    });
  });

  describe('operationUnarchive', () => {
    it('success', () => {
      const generator = operationUnarchive({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_REQUEST,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationUnarchive, ['0', '1', '2'])
      );

      expect(
        generator.next().value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_SUCCESS,
          payload: 'Письма возвращены из архива'
        })
      );
    });

    it('failure', () => {
      const generator = operationUnarchive({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_REQUEST,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationUnarchive, ['0', '1', '2'])
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_FAIL,
          error: 'Произошла ошибка на сервере'
        })
      );
    });
  });

  describe('operationRemove', () => {
    it('success', () => {
      const generator = operationRemove({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_REQUEST,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        put(dalAddConfirmNotification({
          title: 'Удаление писем',
          text: 'Вы действительно хотите удалить письма?',
          labelSuccess: 'Подтвердить',
          labelCancel: 'Отмена',
          uid: '0',
          actionData: ['0', '1', '2']
        }))
      );
    });
  });

  describe('operationRemoveConfirm', () => {
    it('success', () => {
      const generator = operationRemoveConfirm({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_CONFIRM,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationRemove, ['0', '1', '2'])
      );

      expect(
        generator.next().value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_SUCCESS,
          payload: 'Письма успешно удалены'
        })
      );
    });

    it('failure', () => {
      const generator = operationRemoveConfirm({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_CONFIRM,
        payload: ['0', '1', '2']
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalOperationRemove, ['0', '1', '2'])
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({
          type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_FAIL,
          error: 'Произошла ошибка на сервере'
        })
      );
    });
  });

  describe('operationSign', () => {
    it('success', () => {
      const generator = operationSign({
        payload: {
          docIds: ['0']
        }
      });

      const payloadWithChannel = {
        docIds: ['0'],
        // уникальный канал подписи для контейнера
        channel: {
          success: ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_SUCCESS,
          cancel: ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_CANCEL,
          fail: ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_FAIL,
        }
      };

      expect(
        generator.next().value
      ).toEqual(
        put(signDocAddAndTrySigningAction(payloadWithChannel))
      );
    });
  });

  describe('operationSend', () => {
    it('success', () => {
      const generator = operationSend({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_REQUEST,
        payload: {
          docIds: ['0']
        }
      });

      expect(
        generator.next().value
      ).toEqual(
        select(localOperationInProgressIdSelector)
      );

      expect(
        generator.next().value
      ).toEqual(
        call(documentSendToBankSaga, { payload: { docId: ['0'] } })
      );
    });
  });

  describe('operationRepeat', () => {
    it('success', () => {
      const generator = operationRepeat({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REPEAT,
        payload: ['0']
      });

      expect(
        generator.next(['0']).value
      ).toEqual(
        put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_NEW_MESSAGE, payload: { type: 'repeat', itemId: '0' } })
      );
    });
  });

  describe('successNotification', () => {
    it('success', () => {
      const generator = successNotification({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
        payload: 'Документы успешно перемещены в архив'
      });

      expect(
        generator.next().value
      ).toEqual(
        put(dalAddOrdinaryNotificationAction({
          type: 'success',
          title: 'Документы успешно перемещены в архив'
        }))
      );
    });
  });

  describe('errorNotification', () => {
    it('success', () => {
      const generator = errorNotification({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_FAIL,
        error: 'Произошла ошибка на сервере'
      });

      expect(
        generator.next().value
      ).toEqual(
        put(dalAddOrdinaryNotificationAction({
          type: 'error',
          title: 'Произошла ошибка на сервере'
        }))
      );
    });
  });
});
