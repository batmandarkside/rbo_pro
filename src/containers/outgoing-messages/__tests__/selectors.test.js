import { fromJS } from 'immutable';
import { LOCAL_REDUCER } from './../constants';
import {
  columnsCreatedSelector,
  filtersCreatedSelector,
  listCreatedSelector,
  paginationOptionsCreatedSelector,
  panelOperationsCreatedSelector
} from './../selectors';

describe('OutgoingMessagesContainer Selectors', () => {
  const state = fromJS({
    organizations: {
      orgs: [
        {
          intAddress: 'AKADEMIKA ANOKHINA STR 38 BLD 1',
          actualAddress: '105062, г Москва, пер Подсосенский, 23',
          shortName: 'ООО АРТ БАЗАР (сокр)',
          inn: '7729580257',
          city: '',
          name: 'Общество с ограниченной ответственностью АРТ БАЗАР',
          kpp: '772901001',
          juridicalAddress: '119602, Москва г, Академика Анохина ул, 38, 1',
          area: 'Москва г',
          ogrn: '1077758447073',
          country: 'Российская Федерация',
          intName: 'Art Bazaar',
          id: 'f4027a46-7997-4adf-9478-92d05610221c',
          lawType: 'ООО'
        }
      ]
    },
    [LOCAL_REDUCER]: {
      isExportListLoading: false,
      isReloadRequired: false,
      settings: {
        visible: [
          'number',
          'attach',
          'date',
          'status',
          'receiver',
          'topic',
          'account',
          'orgName'
        ],
        sorting: {
          id: 'date',
          direction: -1
        },
        grouped: null,
        pagination: 30,
        width: {
          number: 92,
          attach: 110,
          date: 92,
          status: 192,
          receiver: 240,
          topic: 460,
          account: 196,
          orgName: 240
        }
      },
      paginationOptions: [
        30,
        60,
        120,
        240
      ],
      pages: {
        size: 1,
        selected: 0,
        visible: 1
      },
      selectedItems: [],
      operations: [
        {
          id: 'signAndSend',
          title: 'Подписать и отправить',
          icon: 'sign-and-send',
          disabled: false,
          progress: false,
          grouped: true
        },
        {
          id: 'sign',
          title: 'Подписать',
          icon: 'sign',
          disabled: false,
          progress: false,
          grouped: true
        }
      ],
      sections: [
        {
          id: 'incoming',
          title: 'Входящие',
          route: '/messages',
          selected: false
        },
        {
          id: 'outgoing',
          title: 'Исходящие',
          route: '/messages/outgoing',
          api: '',
          selected: true
        },
        {
          id: 'archive',
          title: 'Архив',
          route: '/messages/outgoing/archive',
          api: 'archived',
          selected: false
        },
        {
          id: 'trash',
          title: 'Удаленные',
          route: '/messages/outgoing/trash',
          api: 'deleted',
          selected: false
        }
      ],
      isListUpdateRequired: false,
      filtersConditions: [
        {
          id: 'date',
          title: 'Дата',
          type: 'datesRange'
        },
        {
          id: 'status',
          title: 'Статус',
          type: 'multipleSelect',
          values: [
            'Исполнен',
            'Отказан АБС',
            'Создан',
            'Ошибка контроля',
            'Частично подписан',
            'Подписан',
            'Доставлен',
            'Принят',
            'Отозван',
            'ЭП/АСП неверна',
            'Ошибка реквизитов',
            'Принят АБС',
            'Отвергнут Банком',
            'Отложен'
          ]
        },
        {
          id: 'receiver',
          title: 'Получатель',
          type: 'dictionary',
          values: {
            discrete: false,
            labelKey: 'receiver'
          }
        },
        {
          id: 'topic',
          title: 'Тема',
          type: 'dictionary',
          values: {
            discrete: true,
            labelKey: 'topic'
          }
        },
        {
          id: 'account',
          title: 'Счет',
          type: 'dictionary',
          values: {
            discrete: true,
            labelKey: 'accNum'
          }
        },
        {
          id: 'sender',
          title: 'От кого',
          type: 'dictionary',
          values: {
            discrete: true,
            labelKey: 'shortName'
          }
        },
        {
          id: 'text',
          title: 'Сообщение',
          type: 'string',
          maxLength: 100
        }
      ],
      isListLoading: false,
      filters: [
        {
          id: 'outgoingMessagesContainerPresetToSign',
          title: 'На подпись',
          isPreset: true,
          savedConditions: [
            {
              id: 'status',
              params: [
                'Создан',
                'Частично подписан'
              ]
            }
          ],
          selected: false
        }
      ],
      columns: [
        {
          id: 'number',
          title: 'Номер',
          canGrouped: false
        },
        {
          id: 'date',
          title: 'Дата',
          canGrouped: true
        }
      ],
      list: [
        {
          docNumber: '20',
          attachExists: false,
          docDate: '2016-10-28T00:00:00+0300',
          account: '40702810400004409387',
          topic: 'Дополнение по зарплатной ведомости',
          receiver: 'Вниманию группы проверки зарплатных файлов',
          orgName: 'ООО АРТ БАЗАР (сокр)',
          status: 'Подписан',
          docId: 'cf5bb78b-55b1-4764-8e8b-4ad5a8ce5df1',
          allowedSmActions: {
            sign: false
          }
        }
      ]
    }
  });

  it('columnsCreatedSelector', () => {
    const selector = columnsCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'number',
          width: 92,
          title: 'Номер',
          canGrouped: false,
          visible: true,
          sorting: 0,
          grouped: false
        },
        {
          id: 'date',
          width: 92,
          title: 'Дата',
          canGrouped: true,
          visible: true,
          sorting: -1,
          grouped: false
        }
      ]
    );
  });

  it('filtersCreatedSelector', () => {
    const selector = filtersCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'outgoingMessagesContainerPresetToSign',
          title: 'На подпись',
          isPreset: true,
          savedConditions: [
            {
              id: 'status',
              params: [
                'Создан',
                'Частично подписан'
              ]
            }
          ],
          conditions: [
            {
              id: 'status',
              params: [
                'Создан',
                'Частично подписан'
              ],
              value: 'Создан, Частично подписан'
            }
          ],
          selected: false
        }
      ]
    );
  });

  it('listCreatedSelector', () => {
    const selector = listCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'notGrouped',
          selected: false,
          items: [
            {
              id: 'cf5bb78b-55b1-4764-8e8b-4ad5a8ce5df1',
              number: '20',
              attach: false,
              date: '28.10.2016',
              status: 'Подписан',
              receiver: 'Вниманию группы проверки зарплатных файлов',
              topic: 'Дополнение по зарплатной ведомости',
              account: '40702.810.4.00004409387',
              orgName: 'ООО АРТ БАЗАР (сокр)',
              selected: false,
              operations: {
                sign: false,
                signAndSend: false
              }
            }
          ]
        }
      ]
    );
  });

  it('paginationOptionsCreatedSelector', () => {
    const selector = paginationOptionsCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          value: 30,
          title: 30,
          selected: true
        },
        {
          value: 60,
          title: 60,
          selected: false
        },
        {
          value: 120,
          title: 120,
          selected: false
        },
        {
          value: 240,
          title: 240,
          selected: false
        }
      ]
    );
  });

  it('panelOperationsCreatedSelector', () => {
    const selector = panelOperationsCreatedSelector(state);

    expect(selector.toJS()).toEqual([
      {
        id: 'signAndSend',
        title: 'Подписать и отправить',
        icon: 'sign-and-send',
        disabled: false,
        progress: false,
        grouped: true
      },
      {
        id: 'sign',
        title: 'Подписать',
        icon: 'sign',
        disabled: false,
        progress: false,
        grouped: true
      }
    ]);
  });
});
