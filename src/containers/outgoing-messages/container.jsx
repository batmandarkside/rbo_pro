import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import classnames from 'classnames';
import { getDocStatusClassNameMode } from 'utils';
import Icon from 'components/ui-components/icon';
import Spreadsheet, {
  Toolbar,
  Filters,
  Operations,
  Table,
  Selection,
  Export,
  Pagination
} from 'components/ui-components/spreadsheet';
import { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import ConfirmNotification from 'components/notification/confirm';
import { LOCATORS } from './constants';
import './style.css';

class OutgoingMessagesContainer extends Component {

  static propTypes = {
    isExportListLoading: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,

    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    location: PropTypes.object.isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,
    panelOperations: PropTypes.instanceOf(List).isRequired,
    sections: PropTypes.instanceOf(List).isRequired,
    selectedItems: PropTypes.instanceOf(List).isRequired,

    getSectionIndexAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changeSectionAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getDictionaryForFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    exportListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeGroupingAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
    changeSelectAction: PropTypes.func.isRequired,
    newMessageAction: PropTypes.func.isRequired,
    routeToItemAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    operationRemoveConfirmAction: PropTypes.func.isRequired,

    getFiltersStaticDictionariesAction: PropTypes.func.isRequired,
    tabs: PropTypes.instanceOf(List).isRequired
  };

  componentWillMount() {
    this.props.getSectionIndexAction();
    this.props.getQueryParamsAction();
    this.props.getFiltersStaticDictionariesAction();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getSectionIndexAction();
      this.props.getQueryParamsAction();
    } else if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  filterDictionaryFormFieldOptionRenderer = ({ conditionId, option, highlight }) => {
    switch (conditionId) {
      case 'account':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.account}
            describe={option && option.orgName}
            extra={option && option.availableBalance}
            highlight={{
              value: highlight,
              inTitle: option && option.account
            }}
          />
        );
      case 'receiver':
      case 'topic':
      case 'sender':
        return (
          <RboFormFieldPopupOptionInner
            label={option && option.title}
            highlight={{
              value: highlight,
              inLabel: option && option.title
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  renderItemAlignRightElement = (value) => {
    const style = { textAlign: 'right' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemAlignCenterElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemAttachElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>
        {value &&
        <Icon
          type="attachment"
          size="16"
        />
        }
      </div>
    );
  };

  renderItemStatusElement = (status) => {
    const elementClassName = classnames(
      'b-outgoing-messages-container__item-status',
      getDocStatusClassNameMode(status)
    );

    return (
      <div className={elementClassName}>
        {status}
      </div>
    );
  };

  render() {
    const {
      isExportListLoading,
      isListLoading,
      columns,
      filtersConditions,
      filters,
      list,
      operations,
      pages,
      paginationOptions,
      panelOperations,
      sections,
      selectedItems,
      exportListAction,
      getListAction,
      changeColumnsAction,
      changeGroupingAction,
      changeFiltersAction,
      getDictionaryForFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSectionAction,
      changeSelectAction,
      changeSortingAction,
      newMessageAction,
      operationAction,
      operationRemoveConfirmAction,
      resizeColumnsAction,
      routeToItemAction,
      tabs
    } = this.props;

    return (
      <div className="b-outgoing-messages-container" data-loc={LOCATORS.CONTAINER}>
        <ConfirmNotification onClickOK={operationRemoveConfirmAction} />
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                  newMessage: LOCATORS.TOOLBAR_BUTTON_NEW_MESSAGE
                },
                tabs: LOCATORS.TOOLBAR_TABS
              }}
              title="Письма"
              buttons={[
                {
                  id: 'refreshList',
                  isProgress: isListLoading,
                  type: 'refresh',
                  title: 'Обновить',
                  onClick: getListAction
                },
                {
                  id: 'newMessage',
                  type: 'simple',
                  title: 'Написать новое письмо',
                  onClick: newMessageAction
                }
              ]}
              tabs={{
                items: tabs,
                selected: sections.findKey(section => section.get('selected')),
                onChange: changeSectionAction
              }}
            />
            {selectedItems.size ?
              <Operations
                dataLoc={LOCATORS.OPERATIONS}
                operations={panelOperations}
                operationAction={operationAction}
                selectedItems={selectedItems}
              />
              :
              <Filters
                dataLocs={{
                  main: LOCATORS.FILTERS,
                  select: LOCATORS.FILTERS_SELECT,
                  add: LOCATORS.FILTERS_ADD,
                  remove: LOCATORS.FILTERS_REMOVE,
                  operations: LOCATORS.FILTERS_OPERATIONS,
                  operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                  operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                  operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                  operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                  conditions: LOCATORS.FILTERS_CONDITIONS,
                  condition: LOCATORS.FILTERS_CONDITION,
                  conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                  conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                  addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                  addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                  close: LOCATORS.FILTERS_CLOSE
                }}
                conditions={filtersConditions}
                filters={filters}
                changeFiltersAction={changeFiltersAction}
                dictionaryFormFieldOptionRenderer={this.filterDictionaryFormFieldOptionRenderer}
                onConditionDictionaryFormInputChange={getDictionaryForFiltersAction}
              />
            }
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                date: this.renderItemAlignRightElement,
                attach: this.renderItemAttachElement,
                status: this.renderItemStatusElement
              }}
              renderHeadCells={{
                date: this.renderItemAlignRightElement,
                attach: this.renderItemAlignCenterElement
              }}
              settings={{
                iconTitle: 'Настройка таблицы писем',
                changeColumnsAction,
                showGrouping: true,
                changeGroupingAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              sorting={{
                changeSortingAction
              }}
              select={{
                selectItemTitle: 'Выделить письмо',
                selectGroupTitle: 'Выделить группу писем',
                selectAllTitle: 'Выделить все письма',
                changeSelectAction
              }}
              resize={{
                resizeColumnsAction
              }}
              onItemClick={routeToItemAction}
              operations={operations}
              operationAction={operationAction}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {!isListLoading && !!selectedItems.size &&
            <Selection
              dataLoc={LOCATORS.SELECTION}
              main={{
                label: 'Выбрано:',
                value: selectedItems.size
              }}
            />
            }
            {!isListLoading && !!list.size && !selectedItems.size &&
              <Export
                dataLoc={LOCATORS.EXPORT}
                label="Выгрузить список в Excel"
                onClick={exportListAction}
                isLoading={isExportListLoading}
              />
            }
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
      </div>
    );
  }
}

export default OutgoingMessagesContainer;
