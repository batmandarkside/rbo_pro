import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const initialSections = [
  {
    id: 'incoming',
    title: 'Входящие',
    route: '/messages',
  },
  {
    id: 'outgoing',
    title: 'Исходящие',
    route: '/messages/outgoing',
    api: '',
    selected: true
  },
  {
    id: 'archive',
    title: 'Архив',
    route: '/messages/outgoing/archive',
    api: 'archived'
  },
  {
    id: 'trash',
    title: 'Удаленные',
    route: '/messages/outgoing/trash',
    api: 'deleted'
  }
];

export const initialFiltersConditions = [
  {
    id: 'date',
    title: 'Дата',
    type: 'datesRange',
    sections: ['outgoing', 'archive', 'trash']
  },
  {
    id: 'status',
    title: 'Статус',
    type: 'multipleSelect',
    values: [
      'Исполнен',
      'Отказан АБС',
      'Создан',
      'Ошибка контроля',
      'Частично подписан',
      'Подписан',
      'Доставлен',
      'Принят',
      'Выгружен',
      'Отозван',
      'ЭП/АСП неверна',
      'Ошибка реквизитов',
      'Принят АБС',
      'Отвергнут Банком',
      'Отложен'
    ],
    sections: ['outgoing', 'archive']
  },
  {
    id: 'receiver',
    title: 'Получатель',
    type: 'search',
    inputType: 'Text',
    maxLength: 200,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['outgoing', 'archive', 'trash']
  },
  {
    id: 'topic',
    title: 'Тема',
    type: 'search',
    inputType: 'Text',
    maxLength: 250,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['outgoing', 'archive', 'trash']
  },
  {
    id: 'account',
    title: 'Счет',
    type: 'search',
    inputType: 'Account',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['outgoing', 'archive', 'trash']
  },
  {
    id: 'sender',
    title: 'От кого',
    type: 'search',
    inputType: 'Text',
    maxLength: 250,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['outgoing', 'archive', 'trash']
  },
  {
    id: 'text',
    title: 'Сообщение',
    type: 'string',
    maxLength: 100,
    sections: ['outgoing', 'archive', 'trash']
  }
];

export const initialFilters = [
  {
    id: 'outgoingMessagesContainerPresetToSign',
    title: 'На подпись',
    isPreset: true,
    savedConditions: [
      {
        id: 'status',
        params: ['Создан', 'Частично подписан']
      }
    ],
    sections: ['outgoing', 'archive']
  },
  {
    id: 'outgoingMessagesContainerPresetRejected',
    title: 'Отвергнутые',
    isPreset: true,
    savedConditions: [
      {
        id: 'status',
        params: ['Отказан АБС', 'ЭП/АСП неверна', 'Ошибка реквизитов', 'Отвергнут Банком']
      }
    ],
    sections: ['outgoing', 'archive']
  },
];

export const initialColumns = [
  {
    id: 'number',
    title: 'Номер',
    canGrouped: false
  },
  {
    id: 'attach',
    title: 'Вложение',
    canGrouped: false
  },
  {
    id: 'date',
    title: 'Дата',
    canGrouped: true
  },
  {
    id: 'status',
    title: 'Статус',
    canGrouped: true
  },
  {
    id: 'receiver',
    title: 'Получатель',
    canGrouped: true
  },
  {
    id: 'topic',
    title: 'Тема',
    canGrouped: true
  },
  {
    id: 'account',
    title: 'Счет',
    canGrouped: true
  },
  {
    id: 'orgName',
    title: 'От кого',
    canGrouped: true
  }
];

export const initialOperations = [
  {
    id: 'signAndSend',
    title: 'Подписать и отправить',
    icon: 'sign-and-send',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'sign',
    title: 'Подписать',
    icon: 'sign',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'unarchive',
    title: 'Вернуть из архива',
    icon: 'unarchive',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'repeat',
    title: 'Повторить',
    icon: 'repeat',
    disabled: false,
    progress: false,
    grouped: false
  },
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'send',
    title: 'Отправить',
    icon: 'send',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'archive',
    title: 'Переместить в архив',
    icon: 'archive',
    disabled: false,
    progress: false,
    grouped: true
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialSettings = {
  visible: ['number', 'attach', 'date', 'status', 'receiver', 'topic', 'account', 'orgName'],
  sorting: { id: 'date', direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    number: 92,
    attach: 110,
    date: 92,
    status: 192,
    receiver: 240,
    topic: 460,
    account: 196,
    orgName: 240
  }
};

export const initialState = fromJS({
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: [],
  filters: [],
  list: [],
  operations: initialOperations,
  selectedItems: []
});

export const getSectionIndexSuccess = (state, sectionIndex) => {
  const selectedSectionId = state.get('sections').find((section, key) => key === sectionIndex).get('id');
  return state.merge({
    sections: state.get('sections').map((section, key) => section.set('selected', key === sectionIndex)),
    filtersConditions: fromJS(initialFiltersConditions)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections')),
    filters: fromJS(initialFilters)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections'))
  });
};

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters;
  if (filter) {
    if (state.get('filters').find(item => item.get('id') === filter.id)) {
      filters = state.get('filters').map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = state.get('filters').push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  } else {
    filters = state.get('filters').map(item => item.set('selected', false));
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const changeSection = (state, sectionIndex) => (
  state.getIn(['sections', sectionIndex, 'selected']) ?
    state :
    state.merge({
      columns: fromJS(initialColumns),
      settings: fromJS(initialSettings),
      pages: fromJS(initialPages),
      paginationOptions: fromJS(initialPaginationOptions)
    })
);

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const isSearching = conditionId !== 'receiver' && conditionId !== 'topic';
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], isSearching)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  selectedItems: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list);
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([])
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

export const operationFinish = (state, operationId, isListUpdateRequired) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

export const getFiltersStaticDictionariesSuccess = (state, filtersStaticDictionaries) => {
  const receiverDictionary = fromJS(filtersStaticDictionaries.receiver);
  const topicDictionary = fromJS(filtersStaticDictionaries.topic);
  const senderDictionary = fromJS(filtersStaticDictionaries.sender);
  return state.set('filtersConditions', state.get('filtersConditions').map((condition) => {
    switch (condition.get('id')) {
      case 'receiver':
        return condition.setIn(['values', 'items'],
          receiverDictionary
            .map(item => item.get('receiver')).toOrderedSet().toList()
            .map(receiver => receiverDictionary.find(item => item.get('receiver') === receiver))
      );
      case 'sender':
        return condition.setIn(['values', 'items'], senderDictionary);
      case 'topic':
        return condition.setIn(['values', 'items'], topicDictionary);
      default:
        return condition;
    }
  }));
};

function OutgoingMessagesContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS:
      return getSectionIndexSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_SECTION:
      return changeSection(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, action.payload);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationRequest(state, 'print');

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_FAIL:
      return operationFinish(state, 'print', false);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_REQUEST:
      return operationRequest(state, 'archive');

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_SUCCESS:
      return operationFinish(state, 'archive', true);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_FAIL:
      return operationFinish(state, 'archive', false);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_REQUEST:
      return operationRequest(state, 'unarchive');

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_SUCCESS:
      return operationFinish(state, 'unarchive', true);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_FAIL:
      return operationFinish(state, 'unarchive', false);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_CONFIRM:
      return operationRequest(state, 'remove');

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationFinish(state, 'remove', true);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationFinish(state, 'remove', false);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_REQUEST:
      return operationRequest(state, 'sign');

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_SUCCESS:
    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_FAIL:
      return operationFinish(state, 'sign', true);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST:
      return operationRequest(state, 'signAndSend');

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS:
    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL:
      return operationFinish(state, 'signAndSend', true);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_REQUEST:
      return operationRequest(state, 'send');

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_SUCCESS:
    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_FAIL:
      return operationFinish(state, 'send', true);

    case ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_SUCCESS:
      return getFiltersStaticDictionariesSuccess(state, action.payload);

    default:
      return state;
  }
}

export default OutgoingMessagesContainerReducer;
