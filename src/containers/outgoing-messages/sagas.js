import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction,
  addConfirmNotification as dalAddConfirmNotification
} from 'dal/notification/actions';
import {
  getOutgoingMessages as dalGetList,
  exportOutgoingMessagesListToExcel as dalExportOutgoingMessagesListToExcel,
  archiveMessages as dalOperationArchive,
  unarchiveMessages as dalOperationUnarchive,
  removeMessages as dalOperationRemove
} from 'dal/messages/sagas';
import {
  warningSignDocSaga,
  successSignDocSaga
} from 'dal/sign/sagas/notifications-saga';
import { signDocAddAndTrySigningAction } from 'dal/sign/actions';
import {
  print as dalOperationPrint,
  documentSendToBankSaga
} from 'dal/documents/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import { getReceiverTopics as dalGetReceiverTopics } from 'dal/dictionaries/sagas';
import {
  ACTIONS,
  LOCAL_ROUTER_ALIAS,
  LOCAL_REDUCER,
  SETTINGS_ALIAS,
  VALIDATE_SETTINGS
} from './constants';

const getParamsForDalOperation = ({ sections, settings, pages, filters, type }) => {
  const section = sections.find(item => item.get('selected')).get('api');
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const visibleColumns = settings.get('visible').toJS();
  const selectedFilter = filters.find(filter => filter.get('selected'));
  const conditions = selectedFilter && selectedFilter.get('conditions');
  const conditionDate = conditions && conditions.find(c => c.get('id') === 'date');
  const from = (conditionDate && conditionDate.getIn(['params', 'dateFrom'])) || null;
  const to = (conditionDate && conditionDate.getIn(['params', 'dateTo'])) || null;
  const conditionTopic = conditions && conditions.find(c => c.get('id') === 'topic');
  const topic = (conditionTopic && conditionTopic.get('params')) || null;
  const conditionReceiver = conditions && conditions.find(c => c.get('id') === 'receiver');
  const receiver = (conditionReceiver && conditionReceiver.get('params')) || null;
  const conditionSender = conditions && conditions.find(c => c.get('id') === 'sender');
  const sender = (conditionSender && conditionSender.get('params')) || null;
  const conditionText = conditions && conditions.find(c => c.get('id') === 'text');
  const text = (conditionText && conditionText.get('params')) || null;
  const conditionAccount = conditions && conditions.find(c => c.get('id') === 'account');
  const account = (conditionAccount && conditionAccount.get('params')) || null;
  const conditionStatus = conditions && conditions.find(c => c.get('id') === 'status');
  const status = (conditionStatus && conditionStatus.get('params')) ? conditionStatus.get('params').toJS() : null;

  const params = {
    section,
    sort,
    desc,
    from,
    to,
    topic,
    receiver,
    sender,
    text,
    account,
    status
  };

  switch (type) {
    case 'list':
      params.offset = pages.get('selected') * settings.get('pagination');
      params.offsetStep = settings.get('pagination') + 1;
      break;
    case 'export':
      params.visibleColumns = visibleColumns;
      break;
    default:
      break;
  }

  return params;
};

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);
export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);

export const compileForSignSelector = state => state.getIn(['sign', 'saveAfterTrySignGroupDocuments', 'compileForSign']);
export const compileNotSignSelector = state => state.getIn(['sign', 'saveAfterTrySignGroupDocuments', 'compileNotSign']);
export const realitySignSelector = state => state.getIn(['sign', 'saveRealitySignGroupDocuments']);
export const saveDocIdForFutureSignSelector = state => state.getIn(['sign', 'saveDocIdForFutureSign']);

export const localOperationInProgressIdSelector = (state) => {
  const operations = state.getIn([LOCAL_REDUCER, 'operations']);
  const operationInProgress = operations && operations.find(item => item.get('progress'));
  return operationInProgress && operationInProgress.get('id');
};

export function* getSectionIndex() {
  const location = yield select(routingLocationSelector);
  const sections = yield select(localSectionsSelector);
  const pathname = location.get('pathname');
  let sectionIndex = 0;
  sections.forEach((section, key) => {
    if (pathname.match(new RegExp(section.get('route'), 'gi'))) {
      sectionIndex = key;
    }
  });
  yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: sectionIndex });
  yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SETTINGS_REQUEST });
}

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload: { pageIndex, filter } });
}

export function* changeSection(action) {
  const { payload } = action;
  const sections = yield select(localSectionsSelector);
  const activeSection = sections.find(section => section.get('id') === payload);
  yield put(push({ pathname: activeSection.get('route'), search: null }));
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;

  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* getDictionaryForFilters(action) {
  try {
    const { payload } = action;
    let result = null;
    switch (payload.conditionId) {
      case 'account':
        result = yield call(dalGetAccounts, { payload: { accNum: payload.value } });
        break;
      default:
        break;
    }
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
      payload: { conditionId: payload.conditionId, items: result } });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getList() {
  try {
    const sections = yield select(localSectionsSelector);
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ sections, settings, pages, filters, type: 'list' });
    const result = yield call(dalGetList, { payload: params });
    yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* exportList() {
  try {
    const sections = yield select(localSectionsSelector);
    const settings = yield select(localSettingsSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ sections, settings, pages: null, filters, type: 'export' });
    const result = yield call(dalExportOutgoingMessagesListToExcel, { payload: params });
    yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* newMessage(action) {
  const { payload } = action;
  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/outgoing/new`,
    search: `?${qs.stringify(payload)}`
  }));
}

export function* routeToItem(action) {
  const { payload } = action;
  yield put(push(`${LOCAL_ROUTER_ALIAS}/outgoing/${payload}`));
}

export function* operation(action) {
  const { payload: { operationId, items } } = action;
  switch (operationId) {
    case 'print':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_REQUEST, payload: items });
      break;
    case 'archive':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_REQUEST, payload: items });
      break;
    case 'unarchive':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_REQUEST, payload: items });
      break;
    case 'remove':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_REQUEST, payload: items });
      break;
    case 'sign':
      yield put({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'signAndSend':
      yield put({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'send':
      yield put({
        type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'repeat':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REPEAT, payload: items });
      break;
    default:
      break;
  }
}

export function* operationPrint(action) {
  const { payload } = action;
  try {
    const result = yield call(dalOperationPrint, { payload: { docTitle: 'messages_to_bank', docId: payload } });
    yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationArchive(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalOperationArchive, payload);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
      payload: `Письм${many ? 'а' : 'о'} помещен${many ? 'ы' : 'о'} в архив`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationUnarchive(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalOperationUnarchive, payload);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_SUCCESS,
      payload: `Письм${many ? 'а' : 'о'} возвращен${many ? 'ы' : 'о'} из архива`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationRemove(action) {
  const { payload } = action;
  const many = payload.length > 1;
  yield put(dalAddConfirmNotification({
    title: `Удаление пис${many ? 'ем' : 'ьма'}`,
    text: `Вы действительно хотите удалить письм${many ? 'а' : 'о'}?`,
    labelSuccess: 'Подтвердить',
    labelCancel: 'Отмена',
    uid: payload[0],
    actionData: payload
  }));
}

export function* operationRemoveConfirm(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalOperationRemove, payload);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: `Письм${many ? 'а' : 'о'} успешно удален${many ? 'ы' : 'о'}`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationSign({ payload }) {
  const payloadWithChannel = {
    ...payload,

    // уникальный канал подписи для контейнера
    channel: {
      success: ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_SUCCESS,
      cancel: ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_CANCEL,
      fail: ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_FAIL,
    }
  };
  yield put(signDocAddAndTrySigningAction(payloadWithChannel));
}

export function* operationSignSuccess(action) {
  const { payload: { resultData } } = action;
  const operationInProgressId = yield select(localOperationInProgressIdSelector);
  const docIds = resultData.map(r => r.docId);

  switch (operationInProgressId) {
    case 'sign':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_SUCCESS });
      break;
    case 'signAndSend':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS });
      yield call(operationSend, { payload: { docIds } });
      break;
    default:
      break;
  }
}

export function* operationSignFail(action) {
  /*
   проверка на ACTIONS_DAL_SIGN.SIGN_MODAL_VISIBILITY payload.show === true;
   игнорируем если сага вызвана при открытии модального окна
  */
  if (action && action.payload && action.payload.show) return;

  const operationInProgressId = yield select(localOperationInProgressIdSelector);
  switch (operationInProgressId) {
    case 'sign':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_FAIL });
      break;
    case 'signAndSend':
      yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
      break;
    default:
      break;
  }
}

export function* operationSend(action) {
  const { payload: { docIds } } = action;
  const operationInProgressId = yield select(localOperationInProgressIdSelector);
  try {
    yield call(documentSendToBankSaga, { payload: { docId: docIds } });

    switch (operationInProgressId) {
      case 'signAndSend':
        yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS });
        break;
      case 'send':
        yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_SUCCESS });
        break;
      default:
        break;
    }
  } catch (error) {
    switch (operationInProgressId) {
      case 'signAndSend':
        yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
        break;
      case 'send':
        yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_FAIL });
        break;
      default:
        break;
    }
  }
}

export function* operationRepeat({ payload }) {
  yield put({
    type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_NEW_MESSAGE,
    payload: { type: 'repeat', itemId: payload[0] }
  });
}

export function* successNotification(action) {
  const { payload } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'success', title: payload }));
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}

export function* getFiltersStaticDictionaries() {
  try {
    const receiverTopics = yield call(dalGetReceiverTopics);
    const sender = yield select(organizationsSelector);
    yield put({
      type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_SUCCESS,
      payload: {
        sender,
        receiver: receiverTopics,
        topic: receiverTopics
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_FAIL });
  }
}


/**
 * логика нотификаций после успешной подписи документа
 * зависит от того групповая или это подпись или одного документа
 * есть ли не подписанные документы в группе
 */
export function* notificationSignSaga() {
  // const operationInProgressId = yield select(localOperationInProgressIdSelector);
  const compileForSign = yield select(compileForSignSelector);
  const compileNotSign = yield select(compileNotSignSelector);
  const realitySign = yield select(realitySignSelector);
  const saveDocIdForFutureSign = yield select(saveDocIdForFutureSignSelector);
  const isGroupSign = saveDocIdForFutureSign.size > 1;

  // @todo все комментарии оставить
  // @todo на доработке

  // console.log(realitySign && realitySign.toJS(), 'realitySignrealitySign');
  // console.log(compileForSign && compileForSign.toJS(), 'realitySignrealitySign');
  // console.log(compileNotSign && compileNotSign.toJS(), 'realitySignrealitySign');

  // ГРУППОВЫЕ ОПЕРАЦИИ
  if (isGroupSign) {
    // if (operationInProgressId === 'sign') { }// нажали на кнопку подписать
    if (compileNotSign.size && realitySign.size) { // при условии что хоть один из группы документов подписан
      yield warningSignDocSaga(
        compileForSign.size,
        realitySign.size,
        compileNotSign.size,
      );
    }

    if (!compileNotSign.size && realitySign.size) { // все выбранные документы подписаны
      yield successSignDocSaga(
        compileForSign.size,
        realitySign.size,
        compileNotSign.size,
      );
    }
    // if (operationInProgressId === 'signAndSend') { } // нажали на кнопку ( подписать и отправить )
  }

  // НЕ ГРУППОВЫЕ ОПЕРАЦИИ
  if (!isGroupSign) {
    // const signedDocument = realitySign.first();
    // const docType = signedDocument.get('docType');

    yield put(dalAddOrdinaryNotificationAction({
      type: 'success',
      message: 'Документ успешно подписан'
    }));

    /* if (operationInProgressId === 'sign') { // нажали на кнопку подписать
     if (ALLOWED_DOCUMENT_SEND_TO_BANK_STATUS.includes(docType)) { // документ ДОПУСКАЕТ отправку в банк
     yield put(dalAddOrdinaryNotificationAction({
     type: 'success',
     message: 'Документ успешно подписан'
     }));
     } else {
     yield put(dalAddOrdinaryNotificationAction({ // документ НЕ допускает отправку в банк
     type: 'success',
     message: 'Документ успешно подписан'
     }));
     }
     }

     if (operationInProgressId === 'signAndSend') { // нажали на кнопку ( подписать и отправить )
     yield put(dalAddOrdinaryNotificationAction({
     type: 'success',
     message: 'Документ успешно подписан'
     }));
     } */
  }
}


export default function* saga() {
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SECTION_INDEX_REQUEST, getSectionIndex);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_SECTION, changeSection);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST, getDictionaryForFilters);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_REQUEST, exportList);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_EXPORT_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_GROUPING, changeSettingsColumns);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_NEW_MESSAGE, newMessage);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_ROUTE_TO_ITEM, routeToItem);

  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION, operation);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_REQUEST, operationPrint);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_PRINT_FAIL, errorNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_REQUEST, operationArchive);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_ARCHIVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_REQUEST, operationUnarchive);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_UNARCHIVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemove);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirm);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REMOVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SEND_REQUEST, operationSend);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_REPEAT, operationRepeat);

  // подпись
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_REQUEST, operationSign);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST, operationSign);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_SUCCESS, operationSignSuccess);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_FAIL, operationSignFail);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CHANNEL_SIGN_DOCUMENT_CANCEL, operationSignFail);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_SUCCESS, notificationSignSaga);
  yield takeLatest(ACTIONS.OUTGOING_MESSAGES_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS, notificationSignSaga);

  yield takeLatest(
    ACTIONS.OUTGOING_MESSAGES_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_REQUEST,
    getFiltersStaticDictionaries
  );
}
