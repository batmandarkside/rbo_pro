import { ACTIONS } from './constants';

/**
 *    action загрузки контейнера
 */
export const mountAction = params => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_MOUNT,
  payload: { params }
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_UNMOUNT
});

/**
 *    action переключения табов
 *    @param {string} tabId
 */
export const changeTabAction = tabId => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_CHANGE_TAB,
  payload: { tabId }
});

/**
 *    action focus на поле формы
 *    @param {string} fieldId
 */
export const fieldFocusAction = fieldId => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_FIELD_FOCUS,
  payload: { fieldId }
});

/**
 *    action blur на поле формы
 *    @param {string} fieldId
 */
export const fieldBlurAction = fieldId => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_FIELD_BLUR,
  payload: { fieldId }
});

/**
 *    action изменения данных
 *    @param {string} fieldId
 *    @param {string} fieldValue
 */
export const changeDataAction = (fieldId, fieldValue, dictionaryValue) => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA,
  payload: { fieldId, fieldValue, dictionaryValue }
});

/**
 *    action поиска по справочникам
 *    @param {string} dictionaryId
 *    @param {string} dictionarySearchValue
 */
export const dictionarySearchAction = (dictionaryId, dictionarySearchValue) => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH,
  payload: { dictionaryId, dictionarySearchValue }
});

/**
 *    action сворачивания / разворачивания секции формы
 *    @param {string} sectionId
 *    @param {boolean} isCollapsed
 */
export const sectionCollapseToggleAction = (sectionId, isCollapsed) => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_SECTION_COLLAPSE_TOGGLE,
  payload: { sectionId, isCollapsed }
});

/**
 *    action выполнения операции
 *    @param {string} operationId
 */
export const operationAction = operationId => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION,
  payload: { operationId }
});

/**
 *    action выполнения операции сохранения шаблона
 *    @param {string} templateName
 */
export const operationSaveAsTemplateContinueAction = templateName => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_CONTINUE,
  payload: { templateName }
});

/**
 *    action перехода к отзыву
 *    @param {string} recallId
 */
export const routeToRecallAction = recallId => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_ROUTE_TO_RECALL,
  payload: { recallId }
});

/**
 *    action сохранения получателя в справочник контрагентов
 */
export const saveReceiverAction = () => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_RECEIVER_REQUEST
});

/**
 *    action закрытия верхней панели документа
 */
export const closeDocumentTopAction = () => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_CLOSE_DOCUMENT_TOP
});

/**
 *    action печати подписи документа
 */
export const signaturePrintAction = signId => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_PRINT_REQUEST,
  payload: { signId }
});

/**
 *    action скачивание подписи документа
 */
export const signatureDownloadAction = signId => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_REQUEST,
  payload: { signId }
});

/**
 * action переключения типа платежа бюджет/не бюджет
 */

export const toggleBudgetAction = () => ({
  type: ACTIONS.R_PAYMENT_CONTAINER_PAYMENT_TYPE_TOGGLE
});
