import { fromJS } from 'immutable';
import { DOC_STATUSES } from 'constants';
import { getIsDocEditable } from 'utils';
import { ACTIONS } from './constants';
import {
  vatRateDependenceOnVatCalculationRuleMap,
  getVatRate,
  getVatSum,
  payerInnDependenceOnDrawerStatusMap,
  getCurrencyOperationType,
  getFormatPurpose,
  getFormatPayerName
} from './utils';

export const initialDocumentTabs = [
  {
    id: 'main',
    title: 'Основные поля',
    isActive: true
  },
  {
    id: 'notes',
    title: 'Заметки',
    isActive: false
  },
  {
    id: 'info',
    title: 'Информация из банка',
    isActive: false
  }
];

export const initialFormSections = {
  primary: {
    title: 'Сумма и назначение платежа',
    isVisible: true,
    isCollapsible: false,
    order: 0
  },
  payer: {
    title: 'Плательщик',
    isVisible: true,
    isCollapsible: true,
    isCollapsed: true,
    order: 1
  },
  receiver: {
    title: 'Получатель',
    isVisible: true,
    isCollapsible: true,
    order: 2
  },
  internalPrimary: {
    title: 'Сумма и назначение платежа',
    isVisible: false,
    isCollapsible: false,
    order: 3
  },
  internalPayer: {
    title: 'Со своего счета',
    isVisible: false,
    isCollapsible: true,
    isCollapsed: true,
    order: 4
  },
  internalReceiver: {
    title: 'На свой счет',
    isVisible: false,
    isCollapsible: true,
    isCollapsed: true,
    order: 5
  },
  internalPurpose: {
    title: 'Общая информация',
    isVisible: false,
    isCollapsible: true,
    isCollapsed: true,
    order: 6
  },
  purpose: {
    title: 'Общая информация',
    isVisible: true,
    isCollapsible: true,
    isCollapsed: false,
    order: 7
  },
  budget: {
    title: 'Платеж в бюджет',
    isVisible: true,
    isCollapsible: false,
    order: 8
  }
};

export const initialDocumentDictionaries = {
  organizations: {
    items: []
  },
  paymentType: {
    items: [
      {
        value: 'partner',
        title: 'Контрагенту'
      },
      {
        value: 'budget',
        title: 'В бюджет'
      },
      {
        value: 'internal',
        title: 'Между своими счетами'
      }
    ]
  },
  paymentKind: {
    items: []
  },
  payerAccount: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  customerKPP: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  payerBankBIC: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  receiver: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  receiverAccount: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  receiverINN: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  receiverName: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  internalPayerAccount: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  internalReceiverAccount: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  paymentPriority: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  receiverBankBIC: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  taxOrCustoms: {
    items: [
      {
        value: 'tax',
        title: 'Налог. период',
        apiValue: 'Налог. период'
      },
      {
        value: 'customs',
        title: 'Код тамож. органа',
        apiValue: 'Код тамож. органа'
      }
    ]
  },
  drawerStatus: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  cbc: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  payReason: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  taxPeriodDay1: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  chargeType: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  vatCalculationRule: {
    items: []
  },
  currencyOperationType: {
    items: [],
    searchValue: '',
    isSearching: false
  },
  templates: {
    items: [],
    searchValue: '',
    isSearching: false
  }
};

export const initialOperations = [
  {
    id: 'back',
    title: 'Назад',
    icon: 'back',
    disabled: false,
    progress: false
  },
  {
    id: 'save',
    title: 'Сохранить',
    icon: 'save',
    disabled: false,
    progress: false
  },
  {
    id: 'signAndSend',
    title: 'Подписать и отправить',
    icon: 'sign-and-send',
    disabled: false,
    progress: false
  },
  {
    id: 'sign',
    title: 'Подписать',
    icon: 'sign',
    disabled: false,
    progress: false
  },
  {
    id: 'unarchive',
    title: 'Вернуть из архива',
    icon: 'unarchive',
    disabled: false,
    progress: false
  },
  {
    id: 'repeat',
    title: 'Повторить',
    icon: 'repeat',
    disabled: false,
    progress: false
  },
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false
  },
  {
    id: 'recall',
    title: 'Отозвать',
    icon: 'recall',
    disabled: false,
    progress: false
  },
  {
    id: 'send',
    title: 'Отправить',
    icon: 'send',
    disabled: false,
    progress: false
  },
  {
    id: 'history',
    title: 'История статустов',
    icon: 'history',
    disabled: false,
    progress: false
  },
  {
    id: 'archive',
    title: 'Переместить в архив',
    icon: 'archive',
    disabled: false,
    progress: false
  },
  {
    id: 'visa',
    title: 'Виза',
    icon: 'visa',
    disabled: false,
    progress: false
  },
  {
    id: 'checkSign',
    title: 'Проверить подпись',
    icon: 'check-sign',
    disabled: false,
    progress: false
  },
  {
    id: 'saveAsTemplate',
    title: 'Сохранить как шаблон',
    icon: 'save-as-template',
    disabled: false,
    progress: false
  }
];

export const initialDocumentHistory = {
  isVisible: false,
  items: []
};

export const initialDocumentSignatures = {
  isVisible: false,
  items: []
};

export const initialState = fromJS({
  isMounted: false,
  isDataError: false,
  isDataLoading: true,
  isEditable: false,
  documentTabs: initialDocumentTabs,
  documentHistory: initialDocumentHistory,
  documentSignatures: initialDocumentSignatures,
  documentData: {},
  documentErrors: [],
  documentDictionaries: initialDocumentDictionaries,
  formSections: initialFormSections,
  activeFormField: null,
  operations: initialOperations
});

export const mount = () => initialState.set('isMounted', true);

export const unmount = () => initialState.set('isMounted', false);

export const getDataRequest = state => state.merge({
  isDataLoading: true
});

export const getDataSuccess = (state, { documentData, documentErrors, documentDictionaries }) => {
  const documentDataImmutable = fromJS(documentData);
  const documentErrorsImmutable = fromJS(documentErrors).get('errors', fromJS([]));
  const documentDictionariesImmutable = fromJS(documentDictionaries);
  const status = documentDataImmutable.get('status', DOC_STATUSES.NEW);
  const isEditable = getIsDocEditable(status, !!documentDataImmutable.getIn(['allowedSmActions', 'fromArchive']));
  const isInternal = !!documentDictionariesImmutable.getIn(['internalReceiverAccount', 'items'])
    .find(item => item.get('accNum') === documentDataImmutable.get('receiverAccount'));
  const isBudget = !!documentDataImmutable.get('drawerStatus');
  let paymentType = documentDataImmutable.get('paymentType', 'partner');
  if (isInternal) paymentType = 'internal';
  else if (isBudget) paymentType = 'budget';

  const firstPayerAccount = documentDictionariesImmutable.getIn(['payerAccount', 'items', 0]);
  const firstPayerBankBIC = documentDictionariesImmutable.getIn(['payerBankBIC', 'items', 0]);
  const isChooseFirstPayerAccount = status === DOC_STATUSES.NEW
    && !documentDataImmutable.get('payerAccount')
    && documentDictionariesImmutable.getIn(['payerAccount', 'items']).size === 1;

  return state.mergeDeep({
    isDataLoading: false,
    isEditable,
    documentData: documentDataImmutable.merge({
      status,
      paymentType,

      payerAccount: isChooseFirstPayerAccount ?
        firstPayerAccount.get('accNum') :
        documentDataImmutable.get('payerAccount', ''),
      payerBankBIC: isChooseFirstPayerAccount
        ? firstPayerBankBIC.get('bic') :
        documentDataImmutable.get('payerBankBIC', ''),
      payerBankCorrAccount: isChooseFirstPayerAccount
        ? firstPayerBankBIC.get('corrAccount')
        : documentDataImmutable.get('payerBankCorrAccount', ''),
      payerBankName: isChooseFirstPayerAccount
        ? firstPayerBankBIC.get('bankName') :
        documentDataImmutable.get('payerBankName', ''),
      payerBankCity: isChooseFirstPayerAccount
        ? firstPayerBankBIC.get('locationName')
        : documentDataImmutable.get('payerBankCity', ''),
      payerBankSettlementType: isChooseFirstPayerAccount
        ? firstPayerBankBIC.get('locationType')
        : documentDataImmutable.get('payerBankSettlementType', ''),

      receiverINN: status === DOC_STATUSES.NEW && paymentType === 'internal'
        ? documentDataImmutable.get('payerINN')
        : documentDataImmutable.get('receiverINN'),
      receiverKPP: status === DOC_STATUSES.NEW && paymentType === 'internal'
        ? documentDataImmutable.get('payerKPP')
        : documentDataImmutable.get('receiverKPP'),
      receiverName: status === DOC_STATUSES.NEW && paymentType === 'internal'
        ? documentDataImmutable.get('payerName')
        : documentDataImmutable.get('receiverName'),

      drawerStatus: status === DOC_STATUSES.NEW && paymentType === 'budget'
        ? documentDataImmutable.get('drawerStatus') || documentDictionariesImmutable.getIn(['drawerStatus', 'items', 0, 'status'])
        : documentDataImmutable.get('drawerStatus'),
      taxOrCustoms: status === DOC_STATUSES.NEW && paymentType === 'budget'
        ? documentDataImmutable.get('taxOrCustoms') || state.getIn(['documentDictionaries', 'taxOrCustoms', 'items', 0, 'apiValue'])
        : documentDataImmutable.get('taxOrCustoms'),
      operationType: documentDataImmutable.get('operationType')
    }),
    documentErrors: documentErrorsImmutable,
    documentDictionaries: documentDictionariesImmutable,
    formSections: {
      primary: {
        isVisible: paymentType !== 'internal'
      },
      payer: {
        isVisible: paymentType !== 'internal',
        isCollapsed: paymentType !== 'budget'
      },
      receiver: {
        isVisible: paymentType !== 'internal'
      },
      purpose: {
        isVisible: paymentType !== 'internal'
      },
      internalPrimary: {
        isVisible: paymentType === 'internal'
      },
      internalPayer: {
        isVisible: paymentType === 'internal',
        isCollapsible: !documentErrorsImmutable.size,
        isCollapsed: !documentErrorsImmutable.size
      },
      internalReceiver: {
        isVisible: paymentType === 'internal',
        isCollapsible: !documentErrorsImmutable.size,
        isCollapsed: !documentErrorsImmutable.size
      },
      internalPurpose: {
        isVisible: paymentType === 'internal'
      },
      budget: {
        isVisible: paymentType === 'budget'
      }
    }
  });
};

export const getDataFail = state => state.merge({
  isDataError: true,
  isDataLoading: false
});

export const saveDataSuccess = (state, { recordID, status, allowedSmActions, errors }) => state.merge({
  documentData: state.get('documentData').merge({
    recordID,
    status,
    allowedSmActions
  }),
  documentErrors: errors,
  formSections: state.get('formSections')
    .mergeDeep({
      internalPayer: {
        isCollapsible: !errors.length,
        isCollapsed: errors.length ? false : state.getIn(['formSections', 'internalPayer', 'isCollapsed'])
      },
      internalReceiver: {
        isCollapsible: !errors.length,
        isCollapsed: errors.length ? false : state.getIn(['formSections', 'internalReceiver', 'isCollapsed'])
      },
      internalPurpose: {
        isCollapsible: !errors.length,
        isCollapsed: errors.length ? false : state.getIn(['formSections', 'internalPurpose', 'isCollapsed'])
      }
    })
});

export const saveDataFail = (state, { error }) => state.merge({
  documentErrors: error.name === 'RBO Controls Error' ? error.stack : state.get('documentErrors'),
  formSections: state.get('formSections')
    .mergeDeep({
      internalPayer: {
        isCollapsible: false,
        isCollapsed: false
      },
      internalReceiver: {
        isCollapsible: false,
        isCollapsed: false
      },
      internalPurpose: {
        isCollapsible: false,
        isCollapsed: false
      }
    })
});

export const fieldFocus = (state, { fieldId }) => state.set('activeFormField', fieldId);

const fieldBlurVatRate = (state) => {
  const amount = state.getIn(['documentData', 'amount']);
  const vatCalculationRule = state.getIn(['documentData', 'vatCalculationRule']);
  if (!vatRateDependenceOnVatCalculationRuleMap.find(v => v === vatCalculationRule) ||
    state.getIn(['documentData', 'vatRate'])) {
    return state;
  }
  const vatRate = getVatRate({
    amount,
    vatCalculationRule,
    vatRate: state.getIn(['documentData', 'vatRate']),
    vatSum: state.getIn(['documentData', 'vatSum'])
  });
  const vatSum = getVatSum({
    amount,
    vatCalculationRule,
    vatRate,
    vatSum: state.getIn(['documentData', 'vatSum'])
  });
  const paymentPurpose = getFormatPurpose({
    paymentPurpose: state.getIn(['documentData', 'paymentPurpose']) || '',
    vatCalculationRule,
    vatRate,
    vatSum,
    currencyOperationType: state.getIn(['documentData', 'currencyOperationType'])
  });
  return state.mergeDeep({
    documentData: {
      vatRate,
      vatSum,
      paymentPurpose
    },
    activeFormField: null
  });
};

export const fieldBlur = (state, { fieldId }) => {
  switch (fieldId) {
    case 'vatRate':
      return fieldBlurVatRate(state);
    default:
      return state.set('activeFormField', null);
  }
};

export const sectionCollapseToggle = (state, { sectionId, isCollapsed }) =>
  state.setIn(['formSections', sectionId, 'isCollapsed'], isCollapsed);

const changeDataAmount = (state, { fieldValue }) => {
  const vatRate = getVatRate({
    amount: fieldValue,
    vatCalculationRule: state.getIn(['documentData', 'vatCalculationRule']),
    vatRate: state.getIn(['documentData', 'vatRate']),
    vatSum: state.getIn(['documentData', 'vatSum'])
  });
  const vatSum = getVatSum({
    amount: fieldValue,
    vatCalculationRule: state.getIn(['documentData', 'vatCalculationRule']),
    vatRate,
    vatSum: state.getIn(['documentData', 'vatSum'])
  });
  return state.mergeDeep({
    documentData: {
      amount: fieldValue,
      vatRate,
      vatSum
    }
  });
};

const changeDataDrawerStatus = (state, { status }) => {
  const statusValue = status && status.get('status');
  const setPayerINNDefaultValue = !payerInnDependenceOnDrawerStatusMap.find(v => v === statusValue);
  const payerINNDefaultValue = state
    .getIn(['documentDictionaries', 'organizations', 'items'])
    .find(item => item.get('id') === state.getIn(['documentData', 'customerBankRecordID'])).get('inn');
  return state.mergeDeep({
    documentData: {
      drawerStatus: statusValue ? status.get('status') : null,
      payerINN: setPayerINNDefaultValue ? payerINNDefaultValue : state.getIn(['documentData', 'payerINN']),
      cbc: statusValue ? state.getIn(['documentData', 'cbc']) : null,
      ocato: statusValue ? state.getIn(['documentData', 'ocato']) : null,
      payReason: statusValue ? state.getIn(['documentData', 'payReason']) : null,
      taxOrCustoms: statusValue
        ? (
          state.getIn(['documentData', 'taxOrCustoms']) ||
          state.getIn(['documentDictionaries', 'taxOrCustoms', 'items', 0, 'apiValue'])
        )
        : null,
      taxPeriodDay1: statusValue ? state.getIn(['documentData', 'taxPeriodDay1']) : null,
      taxPeriodMonth: statusValue ? state.getIn(['documentData', 'taxPeriodMonth']) : null,
      taxPeriodYear: statusValue ? state.getIn(['documentData', 'taxPeriodYear']) : null,
      taxPeriodDay2: statusValue ? state.getIn(['documentData', 'taxPeriodDay2']) : null,
      taxDocNumber: statusValue ? state.getIn(['documentData', 'taxDocNumber']) : null,
      docDateDay: statusValue ? state.getIn(['documentData', 'docDateDay']) : null,
      docDateMonth: statusValue ? state.getIn(['documentData', 'docDateMonth']) : null,
      docDateYear: statusValue ? state.getIn(['documentData', 'docDateYear']) : null,
      chargeType: statusValue ? state.getIn(['documentData', 'chargeType']) : null
    }
  });
};

export const paymentTypeToggle = (state) => {
  const documentData = state.get('documentData');
  const paymentType = documentData.get('paymentType', 'partner');
  const showBudget = paymentType !== 'budget';
  const formSections = state.get('formSections');
  const drawerStatus = state.getIn(['documentData', 'drawerStatus']);

  const setPayerINNDefaultValue = !payerInnDependenceOnDrawerStatusMap.find(v => v === drawerStatus);
  const payerINNDefaultValue = state
    .getIn(['documentDictionaries', 'organizations', 'items'])
    .find(item => item.get('id') === state.getIn(['documentData', 'customerBankRecordID'])).get('inn');

  return state.mergeDeep({
    documentData: {
      paymentType: showBudget ? 'budget' : 'partner',
      drawerStatus: showBudget ? drawerStatus : null,
      payerINN: setPayerINNDefaultValue ? payerINNDefaultValue : state.getIn(['documentData', 'payerINN']),
      cbc: drawerStatus ? state.getIn(['documentData', 'cbc']) : null,
      ocato: drawerStatus ? state.getIn(['documentData', 'ocato']) : null,
      payReason: drawerStatus ? state.getIn(['documentData', 'payReason']) : null,
      taxOrCustoms: drawerStatus
        ? (
          state.getIn(['documentData', 'taxOrCustoms']) ||
          state.getIn(['documentDictionaries', 'taxOrCustoms', 'items', 0, 'apiValue'])
        )
        : null,
      taxPeriodDay1: drawerStatus ? state.getIn(['documentData', 'taxPeriodDay1']) : null,
      taxPeriodMonth: drawerStatus ? state.getIn(['documentData', 'taxPeriodMonth']) : null,
      taxPeriodYear: drawerStatus ? state.getIn(['documentData', 'taxPeriodYear']) : null,
      taxPeriodDay2: drawerStatus ? state.getIn(['documentData', 'taxPeriodDay2']) : null,
      taxDocNumber: drawerStatus ? state.getIn(['documentData', 'taxDocNumber']) : null,
      docDateDay: drawerStatus ? state.getIn(['documentData', 'docDateDay']) : null,
      docDateMonth: drawerStatus ? state.getIn(['documentData', 'docDateMonth']) : null,
      docDateYear: drawerStatus ? state.getIn(['documentData', 'docDateYear']) : null,
      chargeType: drawerStatus ? state.getIn(['documentData', 'chargeType']) : null
    },
    formSections: formSections.merge({
      budget: {
        isVisible: showBudget
      }
    })
  });
};

const changeDataPayerAccount = (state, { account }) => {
  const currencyOperationType = getCurrencyOperationType({
    currencyOperationType: state.getIn(['documentData', 'currencyOperationType']),
    payerAccount: account ? account.get('accNum') : '',
    receiverAccount: state.getIn(['documentData', 'receiverAccount'])
  });

  return state.mergeDeep({
    documentData: {
      payerAccount: account ? account.get('accNum') : null,
      payerName: account ? account.get('orgName') : state.getIn(['documentData', 'payerName']),
      payerBankBIC: account ? account.get('branchBic') : null,
      payerBankCorrAccount: account ? account.get('branchCorrAcc') : null,
      payerBankName: account ? account.get('branchName') : null,
      currencyOperationType
    },
    documentDictionaries: {
      currencyOperationType: {
        searchValue: currencyOperationType
          ? state.getIn(['documentDictionaries', 'currencyOperationType', 'searchValue'])
          : null
      }
    }
  });
};

const changeDataVatCalculationRule = (state, { fieldValue }) => {
  const vatRate = getVatRate({
    amount: state.getIn(['documentData', 'amount']),
    vatCalculationRule: fieldValue,
    vatRate: state.getIn(['documentData', 'vatRate']),
    vatSum: state.getIn(['documentData', 'vatSum'])
  });
  const vatSum = getVatSum({
    amount: state.getIn(['documentData', 'amount']),
    vatCalculationRule: fieldValue,
    vatRate,
    vatSum: state.getIn(['documentData', 'vatSum'])
  });
  return state.mergeDeep({
    documentData: {
      vatCalculationRule: fieldValue,
      vatRate,
      vatSum
    }
  });
};

const changeDataInternalPayerAccount = (state, { account }) => {
  const internalReceiverAccount = state
    .getIn(['documentDictionaries', 'internalReceiverAccount', 'items'])
    .find(item => item.get('accNum') === state.getIn(['documentData', 'receiverAccount']));

  const prepearedState = state.mergeDeep({
    documentData: {
      payerAccount: account ? account.get('accNum') : null,
      payerName: account ? account.get('orgName') : state.getIn(['documentData', 'payerName']),
      payerBankBIC: account ? account.get('branchBic') : null,
      payerBankCorrAccount: account ? account.get('branchCorrAcc') : null,
      payerBankName: account ? account.get('branchName') : null,
    }
  });

  if (account && account.get('orgName') && internalReceiverAccount &&
    account.get('orgName') === internalReceiverAccount.get('orgName')) {
    return changeDataVatCalculationRule(prepearedState, { fieldValue: 'НДС не облаг.' });
  }
  return prepearedState;
};

const changeDataPayerBankBIC = (state, { bic }) => state.mergeDeep({
  documentData: {
    payerBankBIC: bic ? bic.get('bic') : null,
    payerBankCorrAccount: bic ? bic.get('corrAccount') : null,
    payerBankName: bic ? bic.get('bankName') : null,
    payerBankCity: bic ? bic.get('locationName') : null,
    payerBankSettlementType: bic ? bic.get('locationType') : null
  }
});

const changeDataReceiver = (state, { receiver }) => {
  const currencyOperationType = getCurrencyOperationType({
    currencyOperationType: state.getIn(['documentData', 'currencyOperationType']),
    payerAccount: state.getIn(['documentData', 'payerAccount']),
    receiverAccount: receiver ? receiver.get('account') : state.getIn(['documentData', 'receiverAccount'])
  });

  return state.mergeDeep({
    documentData: {
      receiver: receiver || null,
      receiverINN: receiver ? receiver.get('receiverINN') : state.getIn(['documentData', 'receiverINN']),
      receiverKPP: receiver ? receiver.get('receiverKPP') : state.getIn(['documentData', 'receiverKPP']),
      receiverAccount: receiver ? receiver.get('account') : state.getIn(['documentData', 'receiverAccount']),
      receiverName: receiver ? receiver.get('receiverName') : state.getIn(['documentData', 'receiverName']),
      receiverBankBIC: receiver ? receiver.get('receiverBankBIC') : state.getIn(['documentData', 'receiverBankBIC']),
      receiverBankName: receiver ? receiver.get('receiverBankName') : state.getIn(['documentData', 'receiverBankName']),
      receiverBankCorrAccount: receiver ? receiver.get('receiverBankCorrAccount') :
        state.getIn(['documentData', 'receiverBankCorrAccount']),
      receiverBankSettlementType: receiver ? receiver.get('receiverBankSettlementType') :
        state.getIn(['documentData', 'receiverBankSettlementType']),
      receiverBankCity: receiver ? receiver.get('receiverBankCity') : state.getIn(['documentData', 'receiverBankCity']),
      currencyOperationType
    },
    documentDictionaries: {
      receiver: { searchValue: '' },
      currencyOperationType: {
        searchValue: currencyOperationType
          ? state.getIn(['documentDictionaries', 'currencyOperationType', 'searchValue'])
          : null
      }
    }
  });
};

const changeDataReceiverAccount = (state, { account }) => {
  const currencyOperationType = getCurrencyOperationType({
    currencyOperationType: state.getIn(['documentData', 'currencyOperationType']),
    payerAccount: state.getIn(['documentData', 'payerAccount']),
    receiverAccount: account
  });

  return state.mergeDeep({
    documentData: {
      receiverAccount: account,
      currencyOperationType
    },
    documentDictionaries: {
      currencyOperationType: {
        searchValue: currencyOperationType
          ? state.getIn(['documentDictionaries', 'currencyOperationType', 'searchValue'])
          : null
      }
    }
  });
};

const changeDataInternalReceiverAccount = (state, { account }) => {
  const internalPayerAccount = state
    .getIn(['documentDictionaries', 'internalPayerAccount', 'items'])
    .find(item => item.get('accNum') === state.getIn(['documentData', 'payerAccount']));

  const prepearedState = state.mergeDeep({
    documentData: {
      receiverAccount: account ? account.get('accNum') : null,
      receiverName: account ? account.get('orgName') : state.getIn(['documentData', 'receiverName']),
      receiverBankBIC: account ? account.get('branchBic') : null,
      receiverBankCorrAccount: account ? account.get('branchCorrAcc') : null,
      receiverBankName: account ? account.get('branchName') : null
    }
  });

  if (account && account.get('orgName') &&
    internalPayerAccount && account.get('orgName') === internalPayerAccount.get('orgName')) {
    return changeDataVatCalculationRule(prepearedState, { fieldValue: 'НДС не облаг.' });
  }
  return prepearedState;
};

const changeDataReceiverBankBIC = (state, { bic }) => state.mergeDeep({
  documentData: {
    receiverBankBIC: bic ? bic.get('bic') : null,
    receiverBankCorrAccount: bic ? bic.get('corrAccount') : null,
    receiverBankName: bic ? bic.get('bankName') : null,
    receiverBankCity: bic ? bic.get('locationName') : null,
    receiverBankSettlementType: bic ? bic.get('locationType') : null
  }
});

const changeDataVatRate = (state, { fieldValue }) => {
  const vatSum = getVatSum({
    amount: state.getIn(['documentData', 'amount']),
    vatCalculationRule: state.getIn(['documentData', 'vatCalculationRule']),
    vatRate: fieldValue,
    vatSum: state.getIn(['documentData', 'vatSum'])
  });
  return state.mergeDeep({
    documentData: {
      vatRate: fieldValue,
      vatSum
    }
  });
};

const changeDataVatSum = (state, { fieldValue }) => {
  const vatRate = getVatRate({
    amount: state.getIn(['documentData', 'amount']),
    vatCalculationRule: state.getIn(['documentData', 'vatCalculationRule']),
    vatRate: state.getIn(['documentData', 'vatRate']),
    vatSum: fieldValue
  });
  return state.mergeDeep({
    documentData: {
      vatRate,
      vatSum: fieldValue
    }
  });
};

const changeDataTaxOrCustoms = (state, { taxOrCustoms }) => {
  const taxOrCustomsId = taxOrCustoms ? taxOrCustoms.get('id') : null;
  const taxOrCustomsValue = taxOrCustoms ? taxOrCustoms.get('apiValue') : null;
  return state.mergeDeep({
    documentData: {
      taxOrCustoms: taxOrCustomsValue || null,
      taxPeriodDay1: taxOrCustomsId === 'tax' ? state.getIn(['documentData', 'taxPeriodDay1']) : null,
      taxPeriodMonth: taxOrCustomsId === 'tax' ? state.getIn(['documentData', 'taxPeriodMonth']) : null,
      taxPeriodYear: taxOrCustomsId === 'tax' ? state.getIn(['documentData', 'taxPeriodYear']) : null,
      taxPeriodDay2: taxOrCustomsId === 'customs' ? state.getIn(['documentData', 'taxPeriodDay2']) : null,
    }
  });
};

export const changeData = (state, { fieldId, fieldValue, dictionaryValue }) => {
  const selectedDocumentDictionaryItems = state.getIn(['documentDictionaries', fieldId, 'items']);
  let selectedDictionaryItem;
  switch (fieldId) {
    case 'paymentKind':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('code') === fieldValue);
      return state.setIn(['documentData', fieldId],
        selectedDictionaryItem ? selectedDictionaryItem.get('description').toUpperCase() : null);
    case 'amount':
      return changeDataAmount(state, { fieldValue });
    case 'drawerStatus':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataDrawerStatus(state, { status: selectedDictionaryItem });
    case 'payerAccount':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataPayerAccount(state, { account: selectedDictionaryItem });
    case 'internalPayerAccount':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataInternalPayerAccount(state, { account: selectedDictionaryItem });
    case 'payerBankBIC':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataPayerBankBIC(state, { bic: selectedDictionaryItem });
    case 'receiver':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataReceiver(state, { receiver: selectedDictionaryItem });
    case 'receiverAccount':
      if (dictionaryValue) {
        selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === dictionaryValue);
        return changeDataReceiver(state, { receiver: selectedDictionaryItem });
      }
      return changeDataReceiverAccount(state, { account: fieldValue });
    case 'receiverINN':
      if (dictionaryValue) {
        selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === dictionaryValue);
        return changeDataReceiver(state, { receiver: selectedDictionaryItem });
      }
      return state.setIn(['documentData', 'receiverINN'], fieldValue);
    case 'receiverName':
      if (dictionaryValue) {
        selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === dictionaryValue);
        return changeDataReceiver(state, { receiver: selectedDictionaryItem });
      }
      return state.setIn(['documentData', 'receiverName'], fieldValue);
    case 'internalReceiverAccount':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataInternalReceiverAccount(state, { account: selectedDictionaryItem });
    case 'paymentPriority':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return state.setIn(['documentData', fieldId],
        selectedDictionaryItem ? selectedDictionaryItem.get('code') : null);
    case 'receiverBankBIC':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return changeDataReceiverBankBIC(state, { bic: selectedDictionaryItem });
    case 'vatCalculationRule':
      return changeDataVatCalculationRule(state, { fieldValue });
    case 'vatRate':
      return changeDataVatRate(state, { fieldValue });
    case 'vatSum':
      return changeDataVatSum(state, { fieldValue });
    case 'taxOrCustoms':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('value') === fieldValue);
      return changeDataTaxOrCustoms(state, { taxOrCustoms: selectedDictionaryItem });
    case 'payReason':
    case 'chargeType':
      selectedDictionaryItem = selectedDocumentDictionaryItems.find(item => item.get('id') === fieldValue);
      return state.setIn(['documentData', fieldId],
        selectedDictionaryItem ? selectedDictionaryItem.get('param') : null);
    case 'taxPeriodDay1':
      return state.setIn(['documentData', fieldId], fieldValue.toUpperCase());
    default:
      return state.setIn(['documentData', fieldId], fieldValue);
  }
};

export const dictionarySearchDrawerStatus = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'drawerStatus', 'searchValue'], dictionarySearchValue);

export const dictionarySearchPayerAccount = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'payerAccount', 'searchValue'], dictionarySearchValue);

export const dictionarySearchCustomerKPP = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'customerKPP', 'searchValue'], dictionarySearchValue);

export const dictionarySearchPayerBankBICRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'payerBankBIC', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'payerBankBIC', 'isSearching'], true);

export const dictionarySearchPayerBankBICSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'payerBankBIC', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'payerBankBIC', 'isSearching'], false);

export const dictionarySearchPayerBankBICFail = state =>
  state.setIn(['documentDictionaries', 'payerBankBIC', 'isSearching'], false);

export const dictionarySearchReceiverRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'receiver', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'receiver', 'isSearching'], true);

export const dictionarySearchReceiverSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'receiver', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'receiver', 'isSearching'], false);

export const dictionarySearchReceiverFail = state =>
  state.setIn(['documentDictionaries', 'receiver', 'isSearching'], false);

export const dictionarySearchReceiverAccountRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'receiverAccount', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'receiverAccount', 'isSearching'], true);

export const dictionarySearchReceiverAccountSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'receiverAccount', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'receiverAccount', 'isSearching'], false);

export const dictionarySearchReceiverAccountFail = state =>
  state.setIn(['documentDictionaries', 'receiverAccount', 'isSearching'], false);

export const dictionarySearchReceiverINNRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'receiverINN', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'receiverINN', 'isSearching'], true);

export const dictionarySearchReceiverINNSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'receiverINN', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'receiverINN', 'isSearching'], false);

export const dictionarySearchReceiverINNFail = state =>
  state.setIn(['documentDictionaries', 'receiverINN', 'isSearching'], false);

export const dictionarySearchReceiverNameRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'receiverName', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'receiverName', 'isSearching'], true);

export const dictionarySearchReceiverNameSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'receiverName', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'receiverName', 'isSearching'], false);

export const dictionarySearchReceiverNameFail = state =>
  state.setIn(['documentDictionaries', 'receiverName', 'isSearching'], false);

export const dictionarySearchInternalPayerAccount = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'internalPayerAccount', 'searchValue'], dictionarySearchValue);

export const dictionarySearchInternalReceiverAccount = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'internalReceiverAccount', 'searchValue'], dictionarySearchValue);

export const dictionarySearchReceiverBankBICRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'receiverBankBIC', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'receiverBankBIC', 'isSearching'], true);

export const dictionarySearchReceiverBankBICSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'receiverBankBIC', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'receiverBankBIC', 'isSearching'], false);

export const dictionarySearchReceiverBankBICFail = state =>
  state.setIn(['documentDictionaries', 'receiverBankBIC', 'isSearching'], false);

export const dictionarySearchPaymentPriority = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'paymentPriority', 'searchValue'], dictionarySearchValue);

export const dictionarySearchCurrencyOperationTypeRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'currencyOperationType', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'currencyOperationType', 'isSearching'], true);

export const dictionarySearchCBCRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'cbc', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'cbc', 'isSearching'], true);

export const dictionarySearchCBCSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'cbc', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'cbc', 'isSearching'], false);

export const dictionarySearchCBCFail = state =>
  state.setIn(['documentDictionaries', 'cbc', 'isSearching'], false);

export const dictionarySearchPayReason = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'payReason', 'searchValue'], dictionarySearchValue);

export const dictionarySearchTaxPeriodDay1 = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'taxPeriodDay1', 'searchValue'], dictionarySearchValue);

export const dictionarySearchChargeType = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'chargeType', 'searchValue'], dictionarySearchValue);

export const dictionarySearchTemplatesRequest = (state, { dictionarySearchValue }) =>
  state.setIn(['documentDictionaries', 'templates', 'searchValue'], dictionarySearchValue)
    .setIn(['documentDictionaries', 'templates', 'isSearching'], true);

export const dictionarySearchTemplatesSuccess = (state, templates) =>
  state.setIn(['documentDictionaries', 'templates', 'items'], fromJS(templates))
    .setIn(['documentDictionaries', 'templates', 'isSearching'], false);

export const dictionarySearchTemplatesFail = state =>
  state.setIn(['documentDictionaries', 'templates', 'isSearching'], false);

export const dictionarySearchCurrencyOperationTypeSuccess = (state, payload) =>
  state.setIn(['documentDictionaries', 'currencyOperationType', 'items'], fromJS(payload))
    .setIn(['documentDictionaries', 'currencyOperationType', 'isSearching'], false);

export const dictionarySearchCurrencyOperationTypeFail = state =>
  state.setIn(['documentDictionaries', 'currencyOperationType', 'isSearching'], false);

export const formatPurpose = state => state.setIn(
  ['documentData', 'paymentPurpose'],
  getFormatPurpose({
    paymentPurpose: state.getIn(['documentData', 'paymentPurpose']) || '',
    vatCalculationRule: state.getIn(['documentData', 'vatCalculationRule']),
    vatRate: state.getIn(['documentData', 'vatRate']) || 0,
    vatSum: state.getIn(['documentData', 'vatSum']) || 0,
    currencyOperationType: state.getIn(['documentData', 'currencyOperationType'])
  })
);

export const formatPayerName = state => state.setIn(
  ['documentData', 'payerName'],
  getFormatPayerName({
    payerName: state.getIn(['documentData', 'payerName']) || '',
    amount: state.getIn(['documentData', 'amount']) || 0,
    receiverAccount: state.getIn(['documentData', 'receiverAccount']) || '',
    paymentPurpose: state.getIn(['documentData', 'paymentPurpose']) || '',
    organizationId: state.getIn(['documentData', 'customerBankRecordID']),
    organizations: state.getIn(['documentDictionaries', 'organizations', 'items'])
  })
);

export const changeTab = (state, { tabId }) => state.merge({
  documentTabs: state.get('documentTabs').map(tab => tab.set('isActive', tab.get('id') === tabId))
});

export const operationSaveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSaveFinish = state => state.mergeDeep({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSignAndSendRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSignAndSendSigningSuccess = (state, { documentData }) => state.merge({
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignAndSendSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignAndSendFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSignRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSignSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationPrintRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'print' ? operation.set('progress', true) : operation))
});

export const operationPrintFinish = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'print' ? operation.set('progress', false) : operation))
});

export const operationRemoveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationRemoveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  documentErrors: fromJS([]),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationRemoveFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSendRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSendSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSendFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationArchiveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'archive':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationArchiveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'archive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationArchiveFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'archive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationUnarchiveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'unarchive':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationUnarchiveSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'unarchive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationUnarchiveFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'unarchive':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationVisaRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'visa':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationVisaSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'visa':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationVisaFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'visa':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSaveAsTemplateContinue = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'saveAsTemplate':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSaveAsTemplateFinish = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'saveAsTemplate':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationHistoryRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', true) : operation))
});

export const operationHistorySuccess = (state, payload) => state.merge({
  documentHistory: {
    isVisible: true,
    items: payload
  },
  documentSignatures: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});

export const operationHistoryFail = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});

export const operationCheckSignRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', true) : operation))
});

export const operationCheckSignSuccess = (state, payload) => state.merge({
  documentSignatures: {
    isVisible: true,
    items: payload
  },
  documentHistory: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', false) : operation))
});

export const operationCheckSignFail = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', false) : operation))
});

export const operationCloseDocumentTop = state => state.merge({
  documentHistory: {
    isVisible: false,
    items: []
  },
  documentSignatures: {
    isVisible: false,
    items: []
  }
});

function RPaymentContainerReducer(state = initialState, action) {
  const isMounted = state.get('isMounted');

  switch (action.type) {
    case ACTIONS.R_PAYMENT_CONTAINER_MOUNT:
      return mount();

    case ACTIONS.R_PAYMENT_CONTAINER_UNMOUNT:
      return unmount();

    case ACTIONS.R_PAYMENT_CONTAINER_GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_GET_DATA_SUCCESS:
      return isMounted ? getDataSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_GET_DATA_FAIL:
      return isMounted ? getDataFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_SUCCESS:
      return saveDataSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_FAIL:
      return saveDataFail(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_FIELD_FOCUS:
      return fieldFocus(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_FIELD_BLUR:
      return fieldBlur(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_SECTION_COLLAPSE_TOGGLE:
      return sectionCollapseToggle(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA:
      return changeData(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_DRAWER_STATUS:
      return dictionarySearchDrawerStatus(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_ACCOUNT:
      return dictionarySearchPayerAccount(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CUSTOMER_KPP:
      return dictionarySearchCustomerKPP(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_REQUEST:
      return dictionarySearchPayerBankBICRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_SUCCESS:
      return isMounted ? dictionarySearchPayerBankBICSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_FAIL:
      return isMounted ? dictionarySearchPayerBankBICFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_REQUEST:
      return dictionarySearchReceiverRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_SUCCESS:
      return isMounted ? dictionarySearchReceiverSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_FAIL:
      return isMounted ? dictionarySearchReceiverFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_REQUEST:
      return dictionarySearchReceiverAccountRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_SUCCESS:
      return isMounted ? dictionarySearchReceiverAccountSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_FAIL:
      return isMounted ? dictionarySearchReceiverAccountFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_REQUEST:
      return dictionarySearchReceiverINNRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_SUCCESS:
      return isMounted ? dictionarySearchReceiverINNSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_FAIL:
      return isMounted ? dictionarySearchReceiverINNFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_REQUEST:
      return dictionarySearchReceiverNameRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_SUCCESS:
      return isMounted ? dictionarySearchReceiverNameSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_FAIL:
      return isMounted ? dictionarySearchReceiverNameFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_INTERNAL_PAYER_ACCOUNT:
      return dictionarySearchInternalPayerAccount(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_INTERNAL_RECEIVER_ACCOUNT:
      return dictionarySearchInternalReceiverAccount(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST:
      return dictionarySearchReceiverBankBICRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_SUCCESS:
      return isMounted ? dictionarySearchReceiverBankBICSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_FAIL:
      return isMounted ? dictionarySearchReceiverBankBICFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYMENT_PRIORITY:
      return dictionarySearchPaymentPriority(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_REQUEST:
      return dictionarySearchCurrencyOperationTypeRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_SUCCESS:
      return isMounted ? dictionarySearchCurrencyOperationTypeSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_FAIL:
      return isMounted ? dictionarySearchCurrencyOperationTypeFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CBC_REQUEST:
      return dictionarySearchCBCRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CBC_SUCCESS:
      return isMounted ? dictionarySearchCBCSuccess(state, action.payload) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CBC_FAIL:
      return isMounted ? dictionarySearchCBCFail(state) : state;

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAY_REASON:
      return dictionarySearchPayReason(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_TAX_PERIOD_DAY1:
      return dictionarySearchTaxPeriodDay1(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CHARGE_TYPE:
      return dictionarySearchChargeType(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_TEMPLATES_REQUEST:
      return dictionarySearchTemplatesRequest(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_TEMPLATES_SUCCESS:
      return dictionarySearchTemplatesSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_TEMPLATES_FAIL:
      return dictionarySearchTemplatesFail(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PURPOSE:
      return formatPurpose(state);

    case ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PAYER_NAME:
      return formatPayerName(state);

    case ACTIONS.R_PAYMENT_CONTAINER_CHANGE_TAB:
      return changeTab(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_REQUEST:
      return operationSaveRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_SUCCESS:
    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_FAIL:
      return operationSaveFinish(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST:
      return operationSignAndSendRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_SUCCESS:
      return operationSignAndSendSigningSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS:
      return operationSignAndSendSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_CANCEL:
    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL:
      return operationSignAndSendFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_REQUEST:
      return operationSignRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_SUCCESS:
      return operationSignSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_CANCEL:
    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL:
      return operationSignFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationPrintRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_PRINT_FAIL:
      return operationPrintFinish(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_REQUEST:
      return operationRemoveRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationRemoveSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_CANCEL:
    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationRemoveFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST:
      return operationSendRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_SUCCESS:
      return operationSendSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_FAIL:
      return operationSendFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_ARCHIVE_REQUEST:
      return operationArchiveRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_ARCHIVE_SUCCESS:
      return operationArchiveSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_ARCHIVE_FAIL:
      return operationArchiveFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_REQUEST:
      return operationUnarchiveRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_SUCCESS:
      return operationUnarchiveSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_FAIL:
      return operationUnarchiveFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_REQUEST:
      return operationVisaRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_SUCCESS:
      return operationVisaSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_CANCEL:
    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_FAIL:
      return operationVisaFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_CONTINUE:
      return operationSaveAsTemplateContinue(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_SUCCESS:
    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_FAIL:
      return operationSaveAsTemplateFinish(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_HISTORY_REQUEST:
      return operationHistoryRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_HISTORY_SUCCESS:
      return operationHistorySuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_HISTORY_FAIL:
      return operationHistoryFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_REQUEST:
      return operationCheckSignRequest(state);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS:
      return operationCheckSignSuccess(state, action.payload);

    case ACTIONS.R_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_FAIL:
      return operationCheckSignFail(state);

    case ACTIONS.R_PAYMENT_CONTAINER_CLOSE_DOCUMENT_TOP:
      return operationCloseDocumentTop(state);

    case ACTIONS.R_PAYMENT_CONTAINER_PAYMENT_TYPE_TOGGLE:
      return paymentTypeToggle(state);

    default:
      return state;
  }
}

export default RPaymentContainerReducer;
