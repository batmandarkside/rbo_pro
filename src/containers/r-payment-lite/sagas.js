import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import qs from 'qs';
import { goBack, push, replace } from 'react-router-redux';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import { signDocAddAndTrySigningAction as dalSignAction } from 'dal/sign/actions';
import {
  showTemplateModalAction as dalShowTemplateModalAction,
  saveAsTemplateFailAction as dalSaveAsTemplateFailAction
}  from 'dal/template/actions';
import {
  getPaymentData as dalGetData,
  savePaymentData as dalSaveData,
  getPaymentDefaultData as dalGetDefaultData,
  getPaymentCopyData as dalGetCopyData,
  getPaymentFromTemplateData as dalGetFromTemplateData,
  getPaymentErrors as dalGetErrors,
  savePaymentAsTemplate as dalOperationSaveAsTemplate,
  getTemplates as dalGetTemplates,
} from 'dal/r-payments/sagas';
import {
  getPaymentKinds as dalGetPaymentKinds,
  getCustomerKpps as dalGetCustomerKpps,
  getPaymentPriorities as dalGetPaymentPriorities,
  getBIC as dalGetBIC,
  getDrawerStatuses as dalGetDrawerStatuses,
  getCBC as dalGetCBC,
  getPayReasons as dalGetPayReasons,
  getTaxPeriods as dalGetTaxPeriods,
  getChargeTypes as dalGetChargeTypes,
  getVatCalculationRules as dalGetVatCalculationRules,
  getCurrencyOperationTypes as dalGetCurrencyOperationTypes
} from 'dal/dictionaries/sagas';
import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import {
  getCorrespondents as dalGetCorrespondents,
  saveCorrespondentData as dalSaveReceiver
} from 'dal/correspondents/sagas';
import {
  print as dalOperationPrint,
  archive as dalOperationArchive,
  unarchive as dalOperationUnarchive,
  remove as dalOperationRemove,
  sendToBank as dalOperationSend,
  stateHistoryById as dalOperationHistory,
  signsCheckById as dalOperationSignsCheck
} from 'dal/documents/sagas';
import {
  printSignature as dalSignaturePrint,
  downloadSignature as dalSignatureDownload
} from 'dal/sign/sagas/operations-saga';
import { DOC_STATUSES } from 'constants';
import { getIsDocNeedGetErrors, smoothScrollTo } from 'utils';
import { ACTIONS, LOCAL_REDUCER, LOCAL_ROUTER_ALIASES } from './constants';

export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
export const docIdSelector = state => state.getIn([LOCAL_REDUCER, 'documentData', 'recordID']);
export const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
export const documentSignaturesSelector = state => state.getIn([LOCAL_REDUCER, 'documentSignatures']);

export function* successNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'success',
    title: title || 'Операция выполнена успешно',
    message: message || ''
  }));
}

export function* warningNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'warning',
    title: title || '',
    message: message || ''
  }));
}

export function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

export function* confirmNotification(params) {
  const { level, title, message, success, cancel, autoDismiss } = params;
  yield put(dalNotificationAction({
    type: 'confirm',
    level: level || 'success',
    title: title || '',
    message: message || '',
    autoDismiss: typeof autoDismiss === 'number' ? autoDismiss : 20,
    success,
    cancel
  }));
}

export function* refreshData() {
  const id = yield select(docIdSelector);
  try {
    return yield call(dalGetData, { payload: { id } });
  } catch (error) {
    throw error;
  }
}

export function* mountSaga(action) {
  const { payload: { params: { id } } } = action;
  yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_GET_DATA_REQUEST, payload: { id } });
}

export function* getDataSaga(action) {
  const { payload: { id } } = action;
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const type = query.type;
  const documentDictionaries = yield select(documentDictionariesSelector);
  const types = documentDictionaries.getIn(['paymentType', 'items']);

  let dalGetDataGenerator;
  let dalGetDataParams;
  if (id === 'new' && type && type === 'repeat' && query.itemId) {
    dalGetDataGenerator = dalGetCopyData;
    dalGetDataParams = { payload: { id: query.itemId } };
  } else if (id === 'new' && type && type === 'template' && query.templateId) {
    dalGetDataGenerator = dalGetFromTemplateData;
    dalGetDataParams = { payload: { templateId: query.templateId } };
  } else if (id === 'new') {
    dalGetDataGenerator = dalGetDefaultData;
    dalGetDataParams = {};
  } else {
    dalGetDataGenerator = dalGetData;
    dalGetDataParams = { payload: { id } };
  }

  try {
    const organizations = yield select(organizationsSelector);
    const documentData = yield call(dalGetDataGenerator, dalGetDataParams);
    const userAccounts = yield call(dalGetAccounts, { payload: { currIsoCode: 'RUR', status: 'OPEN', type: 1 } });
    const payerBankBICCode = documentData.payerBankBIC ||
      (userAccounts && userAccounts.length && userAccounts[0].branchBic);
    const [
      paymentKind,
      customerKPP,
      paymentPriority,
      payerBankBIC,
      receiverBankBIC,
      drawerStatus,
      payReason,
      taxPeriodDay1,
      chargeType,
      vatCalculationRule,
      templates,
      currencyOperationTypes
    ] = yield all([
      dalGetPaymentKinds(),
      dalGetCustomerKpps(),
      dalGetPaymentPriorities(),
      dalGetBIC({ payload: { code: payerBankBICCode || '' } }),
      dalGetBIC({ payload: { code: documentData.receiverBankBIC || '' } }),
      dalGetDrawerStatuses(),
      dalGetPayReasons(),
      dalGetTaxPeriods(),
      dalGetChargeTypes(),
      dalGetVatCalculationRules(),
      dalGetTemplates({ payload: {} }),
      dalGetCurrencyOperationTypes({ payload: { code: '' } })
    ]);

    if (id === 'new' && type && !!types.find(item => type === item.get('value'))) documentData.paymentType = type;

    const documentErrors = (getIsDocNeedGetErrors(documentData.status))
      ? yield call(dalGetErrors, { payload: documentData })
      : {};

    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_GET_DATA_SUCCESS,
      payload: {
        documentData,
        documentErrors,
        documentDictionaries: {
          organizations: { items: organizations },
          paymentKind: { items: paymentKind },
          customerKPP: { items: customerKPP },
          payerAccount: { items: userAccounts },
          internalPayerAccount: { items: userAccounts },
          internalReceiverAccount: { items: userAccounts },
          paymentPriority: { items: paymentPriority },
          payerBankBIC: { items: payerBankBIC },
          receiverBankBIC: { items: receiverBankBIC },
          drawerStatus: { items: drawerStatus },
          payReason: { items: payReason },
          taxPeriodDay1: { items: taxPeriodDay1 },
          chargeType: { items: chargeType },
          vatCalculationRule: { items: vatCalculationRule },
          templates: { items: templates },
          currencyOperationType: { items: currencyOperationTypes }
        }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_GET_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Документ не найден.',
      message: 'Неверно указан идентификатор документа.'
    });
  }
}

export function* saveDataSaga(action) {
  const { channels } = action;
  const documentData = yield select(documentDataSelector);
  const allowedSave = documentData.getIn(['allowedSmActions', 'save']);
  const status = documentData.get('status');
  if (status === DOC_STATUSES.NEW || allowedSave) {
    try {
      const result = yield call(dalSaveData, { payload: documentData.toJS() });
      if (status === DOC_STATUSES.NEW) yield put(replace(LOCAL_ROUTER_ALIASES.R_PAYMENT.replace('{:id}', result.recordID)));
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_SUCCESS, payload: result, channels });
    } catch (error) {
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_FAIL, payload: { error }, channels });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'doNotAllowed' } });
  }
}

export function* saveDataSuccessSaga(action) {
  const { payload: { errors }, channels } = action;
  if (errors && errors.length) {
    if (errors.filter(error => error.level === '2').length) {
      yield put({ type: channels.success, payload: { status: 'haveErrors' } });
    } else if (errors.filter(error => error.level === '1').length) {
      yield put({ type: channels.success, payload: { status: 'haveWarnings' } });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'success' } });
  }
}

export function* saveDataFailSaga(action) {
  const { payload: { error }, channels } = action;
  yield put({ type: channels.fail });
  yield call(errorNotification, error.name === 'RBO Controls Error'
    ? {
      title: 'Документ не может быть сохранен.',
      message: 'Для сохранения и подтверждения документа требуется исправить ошибки. ' +
      'При наличии блокирующих ошибок документ не может быть сохранен.'
    }
    : {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно сохранить документ'
    }
  );
}

export function* changeDataSaga(action) {
  const { payload: { fieldId, fieldValue } } = action;
  switch (fieldId) {
    case 'payerAccount':
    case 'internalPayerAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA_PAYER_ACCOUNT,
        payload: { fieldValue }
      });
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PAYER_NAME });
      break;
    case 'receiver':
    case 'receiverINN':
    case 'receiverName':
    case 'internalReceiverAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA_RECEIVER,
        payload: { fieldValue }
      });
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PAYER_NAME });
      break;
    case 'amount':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PAYER_NAME });
      break;
    case 'receiverAccount':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PAYER_NAME });
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA_RECEIVER,
        payload: { fieldValue }
      });
      break;
    case 'vatCalculationRule':
    case 'vatRate':
    case 'vatSum':
    case 'currencyOperationType':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PURPOSE });
      break;
    case 'paymentPurpose':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PAYER_NAME });
      break;
    case 'templatesSelect':
      if (fieldValue) {
        yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_TEMPLATE_SELECT, payload: fieldValue });
      }
      break;
    default:
      break;
  }
}

export function* changeDataPayerAccountSaga(action) {
  const { payload: { fieldValue } } = action;
  if (fieldValue !== null) {
    const documentData = yield select(documentDataSelector);
    const payerBankBIC = documentData.get('payerBankBIC');
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_REQUEST,
      payload: { dictionarySearchValue: payerBankBIC, updateDataFlag: true }
    });
  }
}

export function* changeDataReceiverSaga(action) {
  const { payload: { fieldValue } } = action;
  if (fieldValue !== null) {
    const documentData = yield select(documentDataSelector);
    const receiverBankBIC = documentData.get('receiverBankBIC');
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST,
      payload: { dictionarySearchValue: receiverBankBIC, updateDataFlag: true }
    });
  }
}

export function* dictionarySearchSaga(action) {
  const { payload: { dictionaryId, dictionarySearchValue } } = action;
  switch (dictionaryId) {
    case 'drawerStatus':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_DRAWER_STATUS,
        payload: { dictionarySearchValue }
      });
      break;
    case 'payerAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_ACCOUNT,
        payload: { dictionarySearchValue }
      });
      break;
    case 'payerKPP':
    case 'receiverKPP':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CUSTOMER_KPP,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiver':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverINN':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverName':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'internalPayerAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_INTERNAL_PAYER_ACCOUNT,
        payload: { dictionarySearchValue }
      });
      break;
    case 'internalReceiverAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_INTERNAL_RECEIVER_ACCOUNT,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverBankBIC':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'paymentPriority':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYMENT_PRIORITY,
        payload: { dictionarySearchValue }
      });
      break;
    case 'currencyOperationType':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'cbc':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CBC_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'payReason':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAY_REASON,
        payload: { dictionarySearchValue }
      });
      break;
    case 'taxPeriodDay1':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_TAX_PERIOD_DAY1,
        payload: { dictionarySearchValue }
      });
      break;
    case 'chargeType':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CHARGE_TYPE,
        payload: { dictionarySearchValue }
      });
      break;
    case 'templatesSelect':
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_TEMPLATES_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    default:
      break;
  }
}

export function* dictionaryTemplatesSearchSaga({ payload }) {
  const search = payload.dictionarySearchValue;

  try {
    const result = yield call(dalGetTemplates, { payload: { search } });
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_TEMPLATES_SUCCESS, payload: result });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_TEMPLATES_FAIL });
  }
}

export function* dictionarySearchPayerBankBICSaga(action) {
  const { payload: { dictionarySearchValue, updateDataFlag } } = action;
  try {
    const result = yield call(dalGetBIC, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_SUCCESS,
      payload: result
    });
    if (updateDataFlag) {
      const selected = result.find(item => item.bic === dictionarySearchValue);
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA,
        payload: { fieldId: 'payerBankBIC', fieldValue: selected ? selected.id : '' }
      });
    }
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник БИК'
    });
  }
}

export function* dictionarySearchReceiverSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}

export function* dictionarySearchReceiverAccountSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}

export function* dictionarySearchReceiverINNSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}

export function* dictionarySearchReceiverNameSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}

export function* dictionarySearchReceiverBankBICSaga(action) {
  const { payload: { dictionarySearchValue, updateDataFlag } } = action;
  try {
    const result = yield call(dalGetBIC, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_SUCCESS,
      payload: result
    });
    if (updateDataFlag) {
      const selected = result.find(item => item.bic === dictionarySearchValue);
      yield put({
        type: ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA,
        payload: { fieldId: 'receiverBankBIC', fieldValue: selected ? selected.id : '' }
      });
    }
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник БИК'
    });
  }
}

export function* dictionarySearchCBCSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCBC, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CBC_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CBC_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник КБК'
    });
  }
}

export function* dictionarySearchCurrencyOperationTypeSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCurrencyOperationTypes, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник кодов вида валютной операции'
    });
  }
}

export function* saveReceiverSaga() {
  const documentData = yield select(documentDataSelector);
  const data = {
    account: documentData.get('receiverAccount'),
    receiverINN: documentData.get('receiverINN'),
    receiverKPP: documentData.get('receiverKPP'),
    receiverName: documentData.get('receiverName'),
    receiverBankBIC: documentData.get('receiverBankBIC')
  };
  try {
    yield call(dalSaveReceiver, { payload: data });
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_RECEIVER_SUCCESS });
    yield call(successNotification, { title: 'Контрагент успешно добавлен в справочник' });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_RECEIVER_FAIL });
    yield call(errorNotification, error.name === 'RBO Controls Error'
      ? error
      : {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно сохранить получателя в справочнике контрагентов'
      }
    );
  }
}

export function* routeToRecallSaga(action) {
  const docId = yield select(docIdSelector);
  const { payload: { recallId } } = action;
  yield put(push(LOCAL_ROUTER_ALIASES.R_PAYMENT_RECALL.replace('{:id}', docId).replace('{:recallId}', recallId)));
}

export function* operationSaga(action) {
  const { payload: { operationId } } = action;
  switch (operationId) {
    case 'back':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_BACK });
      break;
    case 'save':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_REQUEST });
      break;
    case 'signAndSend':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST });
      break;
    case 'sign':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_REQUEST });
      break;
    case 'send':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST });
      break;
    case 'repeat':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REPEAT });
      break;
    case 'print':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_PRINT_REQUEST });
      break;
    case 'remove':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_REQUEST });
      break;
    case 'recall':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_RECALL });
      break;
    case 'archive':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_ARCHIVE_REQUEST });
      break;
    case 'unarchive':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_REQUEST });
      break;
    case 'visa':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_REQUEST });
      break;
    case 'saveAsTemplate':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_REQUEST });
      break;
    case 'history':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_HISTORY_REQUEST });
      break;
    case 'checkSign':
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_REQUEST });
      break;
    default:
      break;
  }
}

export function* operationBackSaga() {
  const location = yield select(routingLocationSelector);
  const prevLocation = location.get('prevLocation');
  if (prevLocation && prevLocation.get('pathname') !== location.get('pathname')) yield put(goBack());
  else yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
}

export function* operationSaveSaga() {
  yield put({
    type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_SUCCESS,
      fail: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_FAIL
    }
  });
}

export function* operationSaveSuccessSaga(action) {
  const { payload: { status } } = action;
  switch (status) {
    case 'haveErrors':
      yield call(errorNotification, {
        title: 'Документ сохранен с ошибками.',
        message: 'Для подтверждения документа требуется исправить ошибки.'
      });
      break;
    case 'haveWarnings':
      yield call(warningNotification, {
        title: 'Документ сохранен с предупреждениями.',
        message: 'Перед подтверждением документа обратите внимание на предупреждения из банка.'
      });
      break;
    default:
      yield call(confirmNotification, {
        level: 'success',
        title: 'Документ успешно сохранен.',
        success: {
          label: 'Подписать',
          action: { type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_REQUEST }
        }
      });
  }
}

export function* operationSignAndSendSaga() {
  yield put({
    type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_CONTINUE,
      fail: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL
    }
  });
}

export function* operationSignAndSendContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      channel: {
        cancel: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_CANCEL,
        success: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_SUCCESS,
        fail: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_FAIL
      }
    }));
  }
}

export function* operationSignAndSendSigningDalCancelSaga() {
  yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_CANCEL });
}

export function* operationSignAndSendSigningDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_SUCCESS,
      payload: { documentData: result }
    });
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SENDING_REQUEST });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationSignAndSendSigningDalFailSaga() {
  yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
}

export function* operationSignAndSendSendingRequestSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationSend, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно подписан и отправлен в банк.' });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
    yield call(errorNotification, error.name === 'RBO Error' ? error
      : {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно отправить документ в банк'
      }
    );
  }
}

export function* operationSignSaga() {
  yield put({
    type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_CONTINUE,
      fail: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL
    }
  });
}

export function* operationSignContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      channel: {
        cancel: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_CANCEL,
        success: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_SUCCESS,
        fail: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_FAIL
      }
    }));
  }
}

export function* operationSignDalCancelSaga() {
  yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_CANCEL });
}

export function* operationSignDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_SUCCESS, payload: { documentData: result } });
    yield call(confirmNotification, {
      level: 'success',
      title: 'Документ успешно подписан.',
      success: {
        label: 'Отправить',
        action: { type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationSignDalFailSaga() {
  yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_FAIL });
}

export function* operationRepeatSaga() {
  const docId = yield select(docIdSelector);
  yield put(push({ pathname: LOCAL_ROUTER_ALIASES.R_PAYMENT_NEW, search: `?type=repeat&itemId=${docId}` }));
}

export function* operationPrintSaga() {
  const documentData = yield select(documentDataSelector);
  const docId = yield select(docIdSelector);
  const docNumber = documentData.get('docNumber');
  const docDate = documentData.get('docDate');
  try {
    yield call(dalOperationPrint, { payload: { docTitle: 'payment_order', docId, docNumber, docDate } });
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_PRINT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_PRINT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно распечатать документ'
    });
  }
}

export function* operationRemoveSaga() {
  yield call(confirmNotification, {
    level: 'error',
    title: 'Удаление документа',
    message: 'Вы действительно хотите удалить документ?',
    autoDismiss: 0,
    success: {
      label: 'Подтвердить',
      action: { type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_CONFIRM }
    },
    cancel: {
      label: 'Отмена',
      action: { type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_CANCEL }
    }
  });
}

export function* operationRemoveConfirmSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationRemove, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно удален.' });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно удалить документ'
    });
  }
}

export function* operationRecallSaga() {
  const docId = yield select(docIdSelector);
  yield put(push(LOCAL_ROUTER_ALIASES.R_PAYMENT_RECALL_NEW.replace('{:id}', docId)));
}

export function* operationSendSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationSend, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ успешно отправлен в банк.' });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_FAIL });
    yield call(errorNotification, error.name === 'RBO Error' ? error
      : {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно отправить документ в банк'
      }
    );
  }
}

export function* operationArchiveSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationArchive, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ перемещен в архив.' });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_ARCHIVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно переместить документ в архив'
    });
  }
}

export function* operationUnarchiveSaga() {
  const docId = yield select(docIdSelector);
  try {
    yield call(dalOperationUnarchive, { payload: { docId } });
    const result = yield call(refreshData);
    yield put({
      type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_SUCCESS,
      payload: { documentData: result }
    });
    yield call(successNotification, { title: 'Документ возвращен из архива.' });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно вернуть документ из архива'
    });
  }
}

export function* operationVisaSaga() {
  yield put({
    type: ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_CONTINUE,
      fail: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_FAIL
    }
  });
}

export function* operationVisaContinueSaga(action) {
  const itemId = yield select(docIdSelector);
  const { payload: { status } } = action;
  if (status === 'haveErrors') {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_FAIL });
    yield call(errorNotification, {
      title: 'Документ сохранен с ошибками.',
      message: 'Для подтверждения документа требуется исправить ошибки.'
    });
  } else {
    yield put(dalSignAction({
      docIds: [itemId],
      visa: true,
      channel: {
        cancel: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_DAL_CANCEL,
        success: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_DAL_SUCCESS,
        fail: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_DAL_FAIL
      }
    }));
  }
}

export function* operationVisaDalCancelSaga() {
  yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_CANCEL });
}

export function* operationVisaDalSuccessSaga() {
  try {
    const result = yield call(refreshData);
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_SUCCESS, payload: { documentData: result } });
    yield call(confirmNotification, {
      level: 'success',
      title: 'Документ успешно подписан.',
      success: {
        label: 'Отправить',
        action: { type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить данные документа'
    });
  }
}

export function* operationVisaDalFailSaga() {
  yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_FAIL });
}

export function* operationSaveAsTemplateSaga() {
  yield put(dalShowTemplateModalAction(true));
}

export function* operationSaveAsTemplateContinueSaga(action) {
  const docId = yield select(docIdSelector);
  const { payload: { templateName } } = action;
  try {
    const result = yield call(dalOperationSaveAsTemplate, { payload: { docId, name: templateName } });
    yield put(dalShowTemplateModalAction(false));
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_SUCCESS, payload: result });
    yield call(successNotification, {
      title: 'Шаблон успешно сохранен.',
      message: `Шаблон сохранен с именем: ${templateName}`
    });
  } catch (error) {
    if (error.stack) yield put(dalSaveAsTemplateFailAction(error.stack));
    else {
      put(dalShowTemplateModalAction(false));
      yield call(errorNotification, {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно сохранить документ как шаблон'
      });
    }
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_FAIL, payload: { error } });
  }
}

export function* operationHistorySaga() {
  const docId = yield select(docIdSelector);
  try {
    const result = yield call(dalOperationHistory, docId);
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_HISTORY_SUCCESS, payload: result });
    smoothScrollTo(0);
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_HISTORY_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить историю статустов'
    });
  }
}

export function* operationCheckSignSaga() {
  const docId = yield select(docIdSelector);
  try {
    const result = yield call(dalOperationSignsCheck, docId);
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS, payload: result });
    smoothScrollTo(0);
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно проверить подписи'
    });
  }
}

export function* signaturePrintSaga(action) {
  const docId = yield select(docIdSelector);
  const documentSignatures = yield select(documentSignaturesSelector);
  const { payload: { signId } } = action;
  const signature = documentSignatures.get('items').find(item => item.get('signId') === signId);
  const signDate = signature.get('signDateTime');
  try {
    yield call(dalSignaturePrint, { payload: { docId, signId, signDate } });
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_PRINT_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_PRINT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно распечатать подпись'
    });
  }
}

export function* signatureDownloadSaga(action) {
  const docId = yield select(docIdSelector);
  const documentSignatures = yield select(documentSignaturesSelector);
  const { payload: { signId } } = action;
  const signature = documentSignatures.get('items').find(item => item.get('signId') === signId);
  const signDate = signature.get('signDateTime');
  try {
    yield call(dalSignatureDownload, { payload: { docId, signId, signDate } });
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно скачать подпись'
    });
  }
}

export function* recuperationFromTemplateSaga({ payload }) {
  yield put(push(LOCAL_ROUTER_ALIASES.R_PAYMENT_TEMPLATE.replace('{:templateId}', payload)));
}

export default function* saga() {
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_MOUNT, mountSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_GET_DATA_REQUEST, getDataSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_REQUEST, saveDataSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_SUCCESS, saveDataSuccessSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_SAVE_DATA_FAIL, saveDataFailSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA, changeDataSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA_PAYER_ACCOUNT, changeDataPayerAccountSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_CHANGE_DATA_RECEIVER, changeDataReceiverSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH, dictionarySearchSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_REQUEST, dictionarySearchPayerBankBICSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_REQUEST, dictionarySearchReceiverSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_REQUEST, dictionarySearchReceiverAccountSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_REQUEST, dictionarySearchReceiverINNSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_REQUEST, dictionarySearchReceiverNameSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST, dictionarySearchReceiverBankBICSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CBC_REQUEST, dictionarySearchCBCSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_REQUEST, dictionarySearchCurrencyOperationTypeSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_DICTIONARY_TEMPLATES_REQUEST, dictionaryTemplatesSearchSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_TEMPLATE_SELECT, recuperationFromTemplateSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_SAVE_RECEIVER_REQUEST, saveReceiverSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_ROUTE_TO_RECALL, routeToRecallSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION, operationSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_BACK, operationBackSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_REQUEST, operationSaveSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST, operationSignAndSendSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_CONTINUE, operationSignAndSendContinueSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_CANCEL, operationSignAndSendSigningDalCancelSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_SUCCESS, operationSignAndSendSigningDalSuccessSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_FAIL, operationSignAndSendSigningDalFailSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_AND_SEND_SENDING_REQUEST, operationSignAndSendSendingRequestSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_REQUEST, operationSignSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_CONTINUE, operationSignContinueSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_CANCEL, operationSignDalCancelSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_SUCCESS, operationSignDalSuccessSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SIGN_DAL_FAIL, operationSignDalFailSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REPEAT, operationRepeatSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_PRINT_REQUEST, operationPrintSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemoveSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_RECALL, operationRecallSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SEND_REQUEST, operationSendSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_ARCHIVE_REQUEST, operationArchiveSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_UNARCHIVE_REQUEST, operationUnarchiveSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_REQUEST, operationVisaSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_CONTINUE, operationVisaContinueSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_DAL_CANCEL, operationVisaDalCancelSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_DAL_SUCCESS, operationVisaDalSuccessSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_VISA_DAL_FAIL, operationVisaDalFailSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_REQUEST, operationSaveAsTemplateSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_SAVE_AS_TEMPLATE_CONTINUE, operationSaveAsTemplateContinueSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_HISTORY_REQUEST, operationHistorySaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_OPERATION_CHECK_SIGN_REQUEST, operationCheckSignSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_PRINT_REQUEST, signaturePrintSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_CONTAINER_SIGNATURE_DOWNLOAD_REQUEST, signatureDownloadSaga);
}
