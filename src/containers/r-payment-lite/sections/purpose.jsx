import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldDropdown,
  RboFormFieldSearch,
  RboFormFieldValue,
  RboFormCollapse,
  RboFormFieldDate,
  RboFormFieldText,
  RboFormFieldCheckbox
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

const RPaymentSectionPurpose = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange,
    onCollapseToggle,
    isCollapsed,
    isCollapsible,
    toggleBudgetAction
  } = props;


  const handleCollapseToggle = () => {
    onCollapseToggle({ sectionId: 'purpose', isCollapsed: !isCollapsed });
  };

  return (
    <RboFormSection
      id="purpose"
      title="Общая информация"
      isCollapsible={isCollapsible}
      isCollapsed={isCollapsed}
    >
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell size="25%">
                {isEditable &&
                <RboFormFieldText
                  id="docNumber"
                  label="Номер"
                  value={documentData.get('docNumber')}
                  maxLength={7}
                  isWarning={formWarnings.get('docNumber')}
                  isError={formErrors.get('docNumber')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="docNumber"
                  label="Номер"
                  value={documentValues.get('docNumber')}
                  isWarning={formWarnings.get('docNumber')}
                  isError={formErrors.get('docNumber')}
                />
                }
              </RboFormCell>
              <RboFormCell size="35%">
                {isEditable &&
                <RboFormFieldDate
                  id="docDate"
                  label="Дата"
                  value={documentData.get('docDate')}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('docDate')}
                  isError={formErrors.get('docDate')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="docDate"
                  label="Дата"
                  value={documentValues.get('docDate')}
                  isWarning={formWarnings.get('docDate')}
                  isError={formErrors.get('docDate')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell size="50%">
                <RboFormCollapse
                  title="Скрыть детали"
                  collapsedTitle="Показать детали"
                  pseudoLabel
                  isCollapsed={isCollapsed}
                  onCollapseToggle={handleCollapseToggle}
                />
              </RboFormCell>
              <RboFormCell size="50%">
                <RboFormFieldCheckbox
                  id="isBudget"
                  title="Платеж в бюджет"
                  pseudoLabel
                  value={documentData.get('paymentType') === 'budget'}
                  onChange={toggleBudgetAction}
                  isDisabled={!isEditable}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="33%">
                {isEditable &&
                <RboFormFieldDropdown
                  id="paymentKind"
                  label="Вид платежа"
                  value={documentData.get('paymentKind')}
                  options={documentDictionaries.getIn(['paymentKind', 'items']).toJS()}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('paymentKind')}
                  isError={formErrors.get('paymentKind')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="paymentKind"
                  label="Вид платежа"
                  value={documentValues.get('paymentKind')}
                  isWarning={formWarnings.get('paymentKind')}
                  isError={formErrors.get('paymentKind')}
                />
                }
              </RboFormCell>
              <RboFormCell size="33%">
                <RboFormFieldValue
                  id="operationType"
                  label="Вид операции"
                  inputType="Digital"
                  value={documentData.get('operationType')}
                />
              </RboFormCell>
              <RboFormCell size="30%">
                {isEditable &&
                <RboFormFieldSearch
                  id="paymentPriority"
                  label="Очер. платежа"
                  value={documentData.get('paymentPriority')}
                  inputType="Text"
                  maxLength={1}
                  options={documentDictionaries.getIn(['paymentPriority', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupStyle={{ width: 524 }}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('paymentPriority')}
                  isError={formErrors.get('paymentPriority')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="paymentPriority"
                  label="Очер. платежа"
                  value={documentValues.get('paymentPriority')}
                  isWarning={formWarnings.get('paymentPriority')}
                  isError={formErrors.get('paymentPriority')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};


RPaymentSectionPurpose.propTypes = {
  isEditable: PropTypes.bool,
  isCollapsed: PropTypes.bool,
  isCollapsible: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  onCollapseToggle: PropTypes.func.isRequired,
  toggleBudgetAction: PropTypes.func.isRequired
};

export default RPaymentSectionPurpose;
