import React, { Component } from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldButton,
  RboFormFieldSuggest,
  RboFormFieldSearch,
  RboFormFieldText,
  RboFormFieldValue,
  RboFormCollapse
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

class RPaymentSectionReceiver extends Component {

  static propTypes = {
    isEditable: PropTypes.bool,
    isCollapsed: PropTypes.bool,
    isCollapsible: PropTypes.bool,
    documentValues: PropTypes.instanceOf(Map).isRequired,
    documentData: PropTypes.instanceOf(Map).isRequired,
    documentDictionaries: PropTypes.instanceOf(Map).isRequired,
    formWarnings: PropTypes.instanceOf(Map).isRequired,
    formErrors: PropTypes.instanceOf(Map).isRequired,
    formFieldOptionRenderer: PropTypes.func.isRequired,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    onFieldChange: PropTypes.func.isRequired,
    onFieldSearch: PropTypes.func.isRequired,
    saveReceiverAction: PropTypes.func.isRequired,
    onCollapseToggle: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isSearchActive: false
    };
  }

  setReceiverSearchFocus = () => {
    const { isSearchActive } = this.state;
    if (isSearchActive) document.querySelector('#receiver').focus();
  };

  setReceiverINNFocus = () => {
    const { isSearchActive } = this.state;
    if (!isSearchActive) document.querySelector('#receiverINN').focus();
  };

  handleReceiverChange = ({ fieldId, fieldValue }) => {
    this.props.onFieldChange({ fieldId, fieldValue });
    if (fieldValue) {
      this.setState({ isSearchActive: false });
      this.props.onFieldBlur({ fieldId });
      setTimeout(this.setReceiverINNFocus, 0);
    }
  };

  handleReceiverBlur = ({ fieldId }) => {
    this.setState({ isSearchActive: false });
    this.props.onFieldBlur({ fieldId });
  };

  toggleSearchActive = () => {
    const { isSearchActive } = this.state;
    this.setState({ isSearchActive: !isSearchActive });
    setTimeout(this.setReceiverSearchFocus, 0);
  };

  handleCollapseToggle = () => {
    const { onCollapseToggle, isCollapsed } = this.props;
    onCollapseToggle({ sectionId: 'receiver', isCollapsed: !isCollapsed });
  };

  render() {
    const {
      isEditable,
      documentValues,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formFieldOptionRenderer,
      onFieldFocus,
      onFieldBlur,
      onFieldChange,
      onFieldSearch,
      saveReceiverAction,
      isCollapsed,
      isCollapsible
    } = this.props;

    return (
      <RboFormSection
        id="receiver"
        title="Получатель"
        isCollapsible={isCollapsible}
        isCollapsed={isCollapsed}
      >
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormRowset isMain>
              <RboFormRow>
                <RboFormCell>
                  {isEditable &&
                  <RboFormFieldSuggest
                    id="receiverAccount"
                    label="Счет №"
                    inputType="Account"
                    value={documentData.get('receiverAccount')}
                    searchValue={documentDictionaries.getIn(['receiverAccount', 'searchValue'])}
                    options={documentDictionaries.getIn(['receiverAccount', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isWarning={formWarnings.get('receiverAccount')}
                    isError={formErrors.get('receiverAccount')}
                    isSearching={documentDictionaries.getIn(['receiverAccount', 'isSearching'])}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverAccount"
                    label="Счет №"
                    value={documentValues.get('receiverAccount')}
                    isWarning={formWarnings.get('receiverAccount')}
                    isError={formErrors.get('receiverAccount')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
          <RboFormBlock>
            <RboFormRowset isMain>
              <RboFormRow>
                <RboFormCell size="40%">
                  <RboFormCollapse
                    title="Скрыть детали"
                    collapsedTitle="Показать детали"
                    pseudoLabel
                    isCollapsed={isCollapsed}
                    onCollapseToggle={this.handleCollapseToggle}
                  />
                </RboFormCell>
                {isEditable &&
                <RboFormCell size="60%">
                  <RboFormFieldButton
                    id="saveReceiver"
                    pseudoLabel
                    align="right"
                    icon="dictionary-add"
                    title="Добавить в справочник"
                    isDisabled={!documentData.get('receiverName') || !documentData.get('receiverBankBIC')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onClick={saveReceiverAction}
                  />
                </RboFormCell>
                }
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
        </RboFormBlockset>
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell size="50%">
                  {isEditable &&
                  <RboFormFieldSuggest
                    id="receiverINN"
                    label="ИНН/КИО"
                    inputType="Digital"
                    value={documentData.get('receiverINN')}
                    searchValue={documentDictionaries.getIn(['receiverINN', 'searchValue'])}
                    options={documentDictionaries.getIn(['receiverINN', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isWarning={formWarnings.get('receiverINN')}
                    isError={formErrors.get('receiverINN')}
                    isSearching={documentDictionaries.getIn(['receiverINN', 'isSearching'])}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverINN"
                    label="ИНН/КИО"
                    value={documentValues.get('receiverINN')}
                    isWarning={formWarnings.get('receiverINN')}
                    isError={formErrors.get('receiverINN')}
                  />
                  }
                </RboFormCell>
                <RboFormCell size="50%">
                  {isEditable &&
                  <RboFormFieldText
                    id="receiverKPP"
                    label="КПП"
                    value={documentData.get('receiverKPP')}
                    maxLength={9}
                    isWarning={formWarnings.get('receiverKPP')}
                    isError={formErrors.get('receiverKPP')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverKPP"
                    label="КПП"
                    value={documentValues.get('receiverKPP')}
                    isWarning={formWarnings.get('receiverKPP')}
                    isError={formErrors.get('receiverKPP')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell size="1/3">
                  {isEditable &&
                  <RboFormFieldSearch
                    id="receiverBankBIC"
                    label="БИК"
                    value={documentData.get('receiverBankBIC')}
                    searchValue={documentDictionaries.getIn(['receiverBankBIC', 'searchValue'])}
                    options={documentDictionaries.getIn(['receiverBankBIC', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    popupStyle={{ width: 362 }}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isWarning={formWarnings.get('receiverBankBIC')}
                    isError={formErrors.get('receiverBankBIC')}
                    isSearching={documentDictionaries.getIn(['receiverBankBIC', 'isSearching'])}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverBankBIC"
                    label="БИК"
                    value={documentValues.get('receiverBankBIC')}
                    isWarning={formWarnings.get('receiverBankBIC')}
                    isError={formErrors.get('receiverBankBIC')}
                  />
                  }
                </RboFormCell>
                <RboFormCell size="2/3">
                  <RboFormFieldValue
                    id="receiverBankCorrAccount"
                    label="Кор. счет"
                    value={documentValues.get('receiverBankCorrAccount')}
                    isWarning={formWarnings.get('receiverBankCorrAccount')}
                    isError={formErrors.get('receiverBankCorrAccount')}
                  />
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
        </RboFormBlockset>
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell>
                  {isEditable &&
                  <RboFormFieldSuggest
                    id="receiverName"
                    label="Наименование получателя"
                    inputType="Textarea"
                    value={documentData.get('receiverName')}
                    searchValue={documentDictionaries.getIn(['receiverName', 'searchValue'])}
                    options={documentDictionaries.getIn(['receiverName', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isWarning={formWarnings.get('receiverName')}
                    isError={formErrors.get('receiverName')}
                    isSearching={documentDictionaries.getIn(['receiverName', 'isSearching'])}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverName"
                    label="Наименование получателя"
                    value={documentValues.get('receiverName')}
                    isWarning={formWarnings.get('receiverName')}
                    isError={formErrors.get('receiverName')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell>
                  <RboFormFieldValue
                    id="receiverBankName"
                    label="Банк получателя"
                    value={documentValues.get('receiverBankName')}
                    isWarning={formWarnings.get('receiverBankName')}
                    isError={formErrors.get('receiverBankName')}
                  />
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
        </RboFormBlockset>
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell>
                  {isEditable &&
                  <RboFormFieldText
                    id="uip"
                    label="Код/УИП"
                    value={documentData.get('uip')}
                    maxLength={25}
                    isWarning={formWarnings.get('uip')}
                    isError={formErrors.get('uip')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="uip"
                    label="Код/УИП"
                    value={documentValues.get('uip')}
                    isWarning={formWarnings.get('uip')}
                    isError={formErrors.get('uip')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
        </RboFormBlockset>
      </RboFormSection>
    );
  }
}

export default RPaymentSectionReceiver;
