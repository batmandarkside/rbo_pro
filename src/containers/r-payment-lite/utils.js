import moment from 'moment-timezone';
import numeral from 'numeral';

export const documentFieldsMap = [
  'docNumber',
  'docDate',
  'paymentKind',
  'amount',
  'drawerStatus',

  'payerINN',
  'payerKPP',
  'payerName',
  'payerAccount',
  'payerBankName',
  'payerBankBIC',
  'payerBankCorrAccount',

  'receiverINN',
  'receiverKPP',
  'receiverName',
  'receiverAccount',
  'paymentPriority',
  'uip',
  'receiverBankName',
  'receiverBankBIC',
  'receiverBankCorrAccount',

  'paymentPurpose',
  'vatCalculationRule',
  'vatRate',
  'vatSum',
  'currencyOperationType',

  'cbc',
  'ocato',
  'payReason',
  'taxOrCustoms',
  'taxPeriodDay1',
  'taxPeriodMonth',
  'taxPeriodYear',
  'taxPeriodDay2',
  'taxDocNumber',
  'docDateDay',
  'docDateMonth',
  'docDateYear',
  'chargeType'
];

export const documentSectionsFieldsMap = {
  primary: [
    'amount',
    'vatCalculationRule',
    'vatRate',
    'vatSum',
    'paymentPurpose',
    'currencyOperationType'
  ],
  internalPrimary: [
    'amount',
    'vatCalculationRule',
    'vatRate',
    'vatSum',
    'paymentPurpose',
    'currencyOperationType'
  ],
  payer: [
    'payerAccount',
    'payerINN',
    'payerKPP',
    'payerName',
    'payerBankName',
    'payerBankBIC',
    'payerBankCorrAccount'
  ],
  internalPayer: [
    'internalPayerAccount',
    'payerINN',
    'payerKPP',
    'payerName',
    'payerBankName',
    'payerBankBIC',
    'payerBankCorrAccount'
  ],
  receiver: [
    'receiverAccount',
    'receiverINN',
    'receiverKPP',
    'receiverName',
    'uip',
    'receiverBankName',
    'receiverBankBIC',
    'receiverBankCorrAccount',
    'toggleSearch',
    'receiver',
    'saveReceiver'
  ],
  internalReceiver: [
    'internalReceiverAccount',
    'receiverINN',
    'receiverKPP',
    'receiverName',
    'uip',
    'receiverBankName',
    'receiverBankBIC',
    'receiverBankCorrAccount',
  ],
  purpose: [
    'docNumber',
    'docDate',
    'paymentKind',
    'operationType',
    'paymentPriority'
  ],
  internalPurpose: [
    'docNumber',
    'docDate',
    'paymentKind',
    'operationType',
    'paymentPriority'
  ],
  budget: [
    'drawerStatus',
    'cbc',
    'ocato',
    'payReason',
    'taxOrCustoms',
    'taxPeriodDay1',
    'taxPeriodMonth',
    'taxPeriodYear',
    'taxPeriodDay2',
    'taxDocNumber',
    'docDateDay',
    'docDateMonth',
    'docDateYear',
    'chargeType'
  ]
};

export const documentErrorsMap = {
  '1': ['templateName'],
  '2': ['templateName'],
  '1.1.1': ['docNumber'],
  '1.1.2': ['docNumber'],
  '1.1.4': ['docNumber'],
  '1.1.5': ['docNumber'],
  '1.1.6': ['docNumber'],
  '1.1.7': ['docNumber'],
  'r1.1.7': ['docNumber'],
  '1.1.8': ['internalReceiverAccount', 'receiverBankBIC'],
  '1.1.9': ['internalReceiverAccount', 'receiverBankBIC'],
  '1.2.1': ['docDate'],
  '1.2.2': ['docDate'],
  '1.2.3': ['docDate'],
  '1.3.1': ['amount'],
  '1.3.5': ['amount'],
  '1.5.1': ['paymentPurpose'],
  '1.5.2.2': ['paymentPurpose'],
  '1.5.3': ['paymentPurpose'],
  '1.5.5': ['paymentPurpose'],
  '1.5.6': ['currencyOperationType', 'paymentPurpose'],
  '1.5.7': ['paymentPurpose'],
  '1.5.10' : ['currencyOperationType', 'paymentPurpose'],
  '1.6.1': ['operationType'],
  '1.6.2': ['operationType'],
  '1.7.1': ['paymentPriority'],
  '1.7.2': ['paymentPriority'],
  '1.7.3': ['paymentPriority'],
  '1.7.4': ['paymentKind'],
  '1.7.5': ['paymentKind'],
  '1.7.6': ['paymentKind'],
  '1.7.7': ['paymentKind'],
  '1.7.8': ['paymentKind'],
  '1.7.9': ['paymentKind'],
  '1.8.1': ['vatCalculationRule'],
  '1.8.2': ['vatCalculationRule', 'vatRate', 'vatSum'],
  '1.8.3': ['vatCalculationRule'],
  '1.8.4': ['vatRate'],
  '1.8.5': ['vatSum'],
  '2.1.1': ['payerAccount', 'internalPayerAccount'],
  '2.1.2': ['payerAccount', 'internalPayerAccount'],
  '2.1.2.1': ['payerAccount', 'internalPayerAccount'],
  '2.1.3': ['payerAccount', 'internalPayerAccount'],
  '2.1.4': ['payerAccount', 'internalPayerAccount'],
  '2.1.5': ['payerAccount', 'internalPayerAccount'],
  '2.1.6': ['payerAccount', 'internalPayerAccount'],
  '2.2.1': ['payerAccount', 'internalPayerAccount', 'payerBankBIC'],
  '2.2.2': ['payerAccount', 'internalPayerAccount', 'payerBankBIC'],
  '2.2.3': ['payerAccount', 'internalPayerAccount', 'payerBankBIC'],
  '2.2.4': ['payerAccount', 'internalPayerAccount', 'payerBankBIC'],
  '2.3.1': ['payerName'],
  '2.3.2': ['payerName'],
  '2.3.3': ['payerName'],
  'r.2.3.3': ['payerName'],
  '2.3.4': ['payerName'],
  '2.3.5': ['payerName'],
  '2.4.1': ['payerAccount', 'internalPayerAccount', 'payerINN'],
  '2.4.2': ['payerAccount', 'internalPayerAccount', 'payerINN'],
  '2.4.2.1': ['payerAccount', 'internalPayerAccount', 'payerINN'],
  '2.4.2.2': ['payerAccount', 'internalPayerAccount', 'payerINN'],
  '2.4.2.3': ['payerAccount', 'internalPayerAccount', 'payerINN'],
  '2.4.3': ['payerAccount', 'internalPayerAccount', 'payerINN'],
  '2.4.4': ['payerKPP'],
  '2.4.4.1': ['payerKPP'],
  '4.2': ['payerKPP'],
  '4.2.1': ['payerKPP'],
  '2.4.5': ['payerAccount', 'internalPayerAccount', 'payerINN'],
  '2.4.6': ['receiverKPP'],
  '3.1.1.1': ['receiverAccount', 'internalReceiverAccount'],
  '3.1.1.2': ['receiverAccount', 'internalReceiverAccount'],
  '3.1.2': ['receiverAccount', 'internalReceiverAccount'],
  '3.1.3': ['receiverAccount', 'internalReceiverAccount'],
  '3.1.4': ['receiverAccount', 'internalReceiverAccount'],
  '3.1.5': ['receiverAccount', 'internalReceiverAccount'],
  '3.1.6': ['receiverAccount', 'internalReceiverAccount'],
  '3.1.7': ['drawerStatus'],
  '3.2.1': ['internalReceiverAccount', 'receiverBankBIC'],
  '3.2.2': ['internalReceiverAccount', 'receiverBankBIC'],
  '3.2.3': ['internalReceiverAccount', 'receiverBankBIC'],
  '3.2.4': ['internalReceiverAccount', 'receiverBankBIC'],
  '3.3.1': ['internalReceiverAccount', 'receiverINN'],
  '3.3.1.1': ['internalReceiverAccount', 'receiverINN'],
  '3.3.2': ['internalReceiverAccount', 'receiverINN'],
  '3.3.2.1': ['internalReceiverAccount', 'receiverINN'],
  '3.3.2.2': ['internalReceiverAccount', 'receiverINN'],
  '3.3.2.3': ['internalReceiverAccount', 'receiverINN'],
  'r3.3.2.4' : ['internalReceiverAccount', 'receiverINN'],
  'r3.3.2.5' : ['internalReceiverAccount', 'receiverINN'],
  'r3.3.2.6' : ['internalReceiverAccount', 'receiverINN'],
  'r3.3.2.7' : ['internalReceiverAccount', 'receiverINN'],
  '3.3.3': ['receiverName'],
  '3.3.4': ['receiverName'],
  '3.3.5': ['receiverName'],
  '3.3.6': ['receiverName'],
  '3.3.7': ['receiverName'],
  '3.4.1': ['paymentPurpose'],
  '3.4.2': ['docDate'],
  '3.4.3': ['currencyOperationType'],
  '4.4.1': ['cbc'],
  '4.4.2': ['cbc'],
  '4.4.3': ['cbc'],
  '4.4.4': ['cbc'],
  '4.5.1': ['ocato'],
  '4.5.2': ['ocato'],
  '4.5.3': ['ocato'],
  '4.6': ['payReason'],
  '4.6.2': ['payReason'],
  '4.8.1': ['taxDocNumber'],
  '4.8.2': ['taxDocNumber'],
  '4.8.3': ['taxDocNumber'],
  '4.12.1': ['drawerStatus'],
  '4.12.3': ['cbc'],
  '4.12.4': ['payReason'],
  'r4.12.5.1': ['taxPeriodMonth', 'taxPeriodYear'],
  'r4.12.5.2': ['taxPeriodMonth', 'taxPeriodYear'],
  'r4.12.5.3': ['taxPeriodMonth', 'taxPeriodYear'],
  'r4.12.5.4': ['taxPeriodMonth', 'taxPeriodYear'],
  'r4.12.5.5': ['taxPeriodMonth', 'taxPeriodYear'],
  'r4.12.5.6': ['taxPeriodDay1', 'taxPeriodMonth', 'taxPeriodYear'],
  'r4.12.5.7': ['taxPeriodDay2'],
  'r9.12': ['payerINN', 'payerKPP'],
  'r9.13': ['payerINN', 'payerKPP'],
  'r9.14': ['payerINN', 'payerKPP'],
  'r10.6': ['taxDocNumber'],
  'r10.7': ['taxDocNumber'],
  '4.12.7' : ['docDateDay', 'docDateMonth', 'docDateYear'],
  '4.12.8' : ['chargeType'],
  '4.12.10': ['paymentKind'],
  '4.12.11': ['payReason'],
  '4.12.14': ['paymentKind'],
  '4.13' : ['uip'],
  '4.14' : ['uip'],
  '4.15' : ['uip'],
  '4.16' : ['uip'],
  'r4.5.3' : ['ocato'],
  'r4.5.4' : ['ocato'],
  'r4.5.5' : ['ocato'],
  'r4.12.3.1': ['cbc'],
  'r4.13.1': ['cbc', 'uip'],
  'r4.15.1': ['uip'],
  'r4.15.2': ['uip'],
  'r4.16.1': ['uip'],
  'r4.16.2': ['uip'],
  'r11.3.2': ['taxDocNumber'],
  'r11.3.3': ['taxDocNumber'],
  '4.1': ['drawerStatus'],
  '4.3': ['receiverKPP'],
  '4.3.1': ['receiverKPP'],
  '4.5': ['ocato'],
  '4.7': ['taxPeriodDay1', 'taxPeriodMonth', 'taxPeriodYear', 'taxPeriodDay2'],
  '4.9': ['docDateDay'],
  '4.10': ['chargeType'],
  '5.1': ['amount', 'docNumber', 'payerAccount', 'internalPayerAccount', 'receiverAccount', 'internalReceiverAccount', 'docDate'],
  '6.1': ['receiverAccount', 'internalReceiverAccount', 'receiverINN', 'receiverBankBIC'],
  '7.2.1': ['docNumber'],
  '7.2.2': ['payerAccount', 'internalPayerAccount'],
  '7.4.1': ['docNumber'],
  '7.4.2': ['currencyOperationType'],
  '7.1': ['amount'],
  '7.3': ['docNumber'],
  '7.5': ['amount'],
  '8.1': ['internalReceiverAccount', 'receiverBankBIC', 'receiverBankName', 'receiverBankSettlementType', 'receiverBankCity'],
  '8.2': ['internalReceiverAccount', 'receiverBankBIC', 'receiverBankName', 'receiverBankSettlementType', 'receiverBankCity'],
  '8.3': ['internalReceiverAccount', 'receiverBankBIC', 'receiverBankName', 'receiverBankSettlementType', 'receiverBankCity'],
  '8.4': ['internalReceiverAccount', 'receiverBankBIC', 'receiverBankName', 'receiverBankSettlementType', 'receiverBankCity'],
  '8.5': ['internalReceiverAccount', 'receiverBankBIC', 'receiverBankName', 'receiverBankSettlementType', 'receiverBankCity'],
  '8.6': ['payerAccount', 'internalPayerAccount'],
  '8.7': ['payerAccount', 'internalPayerAccount', 'payerBankName', 'payerBankSettlementType', 'payerBankCity'],
  '9.1': ['ocato'],
  '9.2': ['taxPeriodDay2'],
  '9.3': ['payReason'],
  '9.4': ['taxPeriodDay1'],
  '9.5': ['taxPeriodDay1', 'taxPeriodMonth', 'taxPeriodYear'],
  '9.6': ['taxDocNumber'],
  '9.7': ['taxDocNumber'],
  '9.8': ['taxDocNumber'],
  '9.9': ['docDateDay', 'docDateMonth', 'docDateYear'],
  '9.10' : ['payReason'],
  '9.11' : ['ocato'],
  'r9.15' : ['payerINN', 'payerKPP'],
  '10.1' : ['payReason'],
  '10.2' : ['taxOrCustoms', 'taxPeriodDay2'],
  '10.3' : ['taxDocNumber'],
  '10.4' : ['taxDocNumber'],
  '10.5' : ['docDateDay', 'docDateMonth', 'docDateYear'],
  '11.1' : ['payReason'],
  '11.2' : ['taxPeriodDay1', 'taxPeriodMonth', 'taxPeriodYear', 'taxPeriodDay2'],
  '11.3' : ['taxDocNumber'],
  '11.3.1' : ['taxDocNumber'],
  '11.4' : ['docDateDay', 'docDateMonth', 'docDateYear'],
  'mobile.3' : ['drawerStatus'],
  'mobile.2' : ['taxPeriodDay1', 'taxPeriodDay2', 'taxPeriodMonth', 'taxPeriodYear'],
  'mobile.1' : ['payerAccount', 'internalPayerAccount']
};

export const getBankInfo = documentData => (
  !documentData.get('bankAcceptDate') &&
  !documentData.get('operationDate') &&
  !documentData.get('bankMessage') &&
  !documentData.get('bankMessageAuthor')
    ? null
    : [
      {
        label: 'Поступило в Банк плательщика',
        value: documentData.get('bankAcceptDate') ? moment(documentData.get('bankAcceptDate')).format('DD.MM.YYYY') : ''
      },
      {
        label: 'Списано со счета плательщика',
        value: documentData.get('operationDate') ? moment(documentData.get('operationDate')).format('DD.MM.YYYY') : ''
      },
      { label: 'Сообщение из Банка', value: documentData.get('bankMessage') },
      { label: 'Ответственный исполнитель', value: documentData.get('bankMessageAuthor') }
    ]
);

export const payerInnDependenceOnDrawerStatusMap =
  ['01', '02', '03', '05', '09', '10', '11', '12', '13', '19', '20', '22', '25', '26'];

export const getPayerInnIsDisabled = (value, dictionary) => {
  const dictionaryItem = dictionary.find(item => item.get('value') === value);
  const title = dictionaryItem && dictionaryItem.get('title');
  return !payerInnDependenceOnDrawerStatusMap.find(v => v === title);
};

export const vatRateDependenceOnVatCalculationRuleMap = ['Расчет по % (1)', 'Расчет по % (2)', 'Ручной ввод'];

export const getVatRateIsDisabled = (value, dictionary) => {
  const dictionaryItem = dictionary.find(item => item.get('value') === value);
  const title = dictionaryItem && dictionaryItem.get('title');
  return !vatRateDependenceOnVatCalculationRuleMap.find(v => v === title);
};

export const vatSumDependenceOnVatCalculationRuleMap = ['Ввод НДС'];

export const getVatSumIsDisabled = (value, dictionary) => {
  const dictionaryItem = dictionary.find(item => item.get('value') === value);
  const title = dictionaryItem && dictionaryItem.get('title');
  return !vatSumDependenceOnVatCalculationRuleMap.find(v => v === title);
};

export const getVatRate = ({ amount, vatCalculationRule, vatRate, vatSum }) => {
  switch (vatCalculationRule) {
    case 'НДС не облаг.':
      return 0.00;
    case 'Расчет по % (1)':
    case 'Расчет по % (2)':
    case 'Ручной ввод':
      return vatRate || 18.00;
    case 'Ввод НДС':
      return amount >= vatSum ? parseFloat((vatSum / amount * 100).toFixed(2)) : 100;
    default:
      return vatRate;
  }
};

export const getVatSum = ({ amount, vatCalculationRule, vatRate, vatSum }) => {
  switch (vatCalculationRule) {
    case 'НДС не облаг.':
      return 0.00;
    case 'Расчет по % (1)':
    case 'Расчет по % (2)':
      return parseFloat((amount / (100 + vatRate) * vatRate).toFixed(2));
    case 'Ручной ввод':
      return parseFloat((amount / 100 * vatRate).toFixed(2));
    case 'Ввод НДС':
    default:
      return vatSum;
  }
};

export const currencyOperationTypeDependenceOnAccountNumStart = [
  '30111',
  '30122',
  '30123',
  '30230',
  '30231',
  '316',
  '314',
  '40803',
  '40804',
  '40805',
  '40806',
  '40807',
  '40809',
  '40812',
  '40813',
  '40814',
  '40815',
  '40818',
  '40820',
  '425',
  '426',
  '440'
];

export const getCurrencyOperationType = ({ currencyOperationType, payerAccount, receiverAccount }) => {
  const searchPayerAccountException = !!(payerAccount && payerAccount.search(new RegExp('^30109', 'ig')) >= 0);
  if (searchPayerAccountException) return '';

  const searchPayerAccountConjunction = payerAccount && currencyOperationTypeDependenceOnAccountNumStart
      .find(v => payerAccount.search(new RegExp(`^${v}`, 'ig')) >= 0);
  const searchReceiverAccountConjunction = receiverAccount && currencyOperationTypeDependenceOnAccountNumStart
      .find(v => receiverAccount.search(new RegExp(`^${v}`, 'ig')) >= 0);
  if (!searchPayerAccountConjunction && !searchReceiverAccountConjunction) return '';

  return currencyOperationType;
};

export const getCurrencyOperationTypeIsDisabled = (
  { payerAccountValue, payerAccountDictionary, receiverAccountValue }
) => {
  const dictionaryItem = payerAccountDictionary.find(item => item.get('value') === payerAccountValue);
  const payerAccount = dictionaryItem && dictionaryItem.get('title');

  const searchPayerAccountException = !!(payerAccount && payerAccount.search(new RegExp('^30109', 'ig')) >= 0);
  if (searchPayerAccountException) return false;

  const searchPayerAccountConjunction = payerAccount && currencyOperationTypeDependenceOnAccountNumStart
    .find(v => payerAccount.search(new RegExp(`^${v}`, 'ig')) >= 0);
  const searchReceiverAccountConjunction = receiverAccountValue && currencyOperationTypeDependenceOnAccountNumStart
    .find(v => receiverAccountValue.search(new RegExp(`^${v}`, 'ig')) >= 0);
  return !(searchPayerAccountConjunction || searchReceiverAccountConjunction);
};

export const getFormatPurpose = ({ paymentPurpose, vatCalculationRule, vatRate, vatSum, currencyOperationType }) => {
  const currencyOperationTypeValue = currencyOperationType ? `{VO${currencyOperationType}}` : '';

  let vatValue;
  switch (vatCalculationRule) {
    case 'НДС не облаг.':
      vatValue = 'НДС не облагается';
      break;
    case 'Расчет по % (1)':
      vatValue = `В том числе НДС ${numeral(vatRate).format('0,0.00')}% - ${numeral(vatSum).format('0,0.00')}`;
      break;
    case 'Расчет по % (2)':
    case 'Ввод НДС':
    case 'Ручной ввод':
    default:
      vatValue = `В том числе НДС ${numeral(vatSum).format('0,0.00')}`;
  }

  let clearPurpose = paymentPurpose;
  [
    new RegExp('В\\s+том\\s+числе\\s+НДС\\s*[0-9 ]+(:?.\\d*)?\\s*%\\s*-\\s*(\\-?)[0-9 ]+(.?[\\.,][0-9 ]*)', 'gi'),
    new RegExp('В\\s+том\\s+числе\\s+НДС\\s*(\\-?)(:?.[0-9. ]*)?\\s*%', 'gi'),
    new RegExp('В\\s+том\\s+числе\\s+НДС\\s*(\\-?)[0-9 ]+(:?[\\.,][0-9 ]*)?', 'gi'),
    new RegExp('В\\s+том\\s+числе\\s+НДС\\s*', 'gi'),
    new RegExp('НДС\\s+не\\s+облагается', 'gi'),
    new RegExp('НДС\\s+не\\s+облаг.', 'gi'),
    new RegExp('{VO\\d+}', 'gi')
  ].forEach((regExp) => {
    clearPurpose = clearPurpose.replace(regExp, '');
  });

  clearPurpose = clearPurpose.replace(/\n+/gi, '\n').replace(/^\n/i, '').replace(/\n$/i, '');

  return `${currencyOperationTypeValue ? `${currencyOperationTypeValue}\n` : ''}${clearPurpose}\n${vatValue}`;
};

const getAddressValue = ({ organizationId, organizations }) => {
  const organization = organizations.find(item => item.get('id') === organizationId);
  const actualAddress = organization ? organization.get('actualAddress') : '';
  const juridicalAddress = organization ? organization.get('juridicalAddress') : '';
  return actualAddress || juridicalAddress;
};

export const getFormatPayerName = (
  { payerName, amount, receiverAccount, paymentPurpose, organizationId, organizations }
) => {
  let addressValue = '';
  const amountRule = amount > 15000;
  const receiverAccountRule = /^30111/.test(receiverAccount);
  const paymentPurposeRule = /30111\d{15}/gi.test(paymentPurpose) || /30111\.\d{3}.\d.\d{11}/gi.test(paymentPurpose);
  if (amountRule && (receiverAccountRule || paymentPurposeRule)) {
    addressValue = `//${getAddressValue({ organizationId, organizations })}//`;
  }

  let clearPayerName = payerName;
  [
    new RegExp('//.*//', 'gi')
  ].forEach((regExp) => {
    clearPayerName = clearPayerName.replace(regExp, '');
  });

  return `${clearPayerName}\n${addressValue}`
    .replace(/\n+/gi, '\n').replace(/^\n/i, '').replace(/\n$/i, '');
};
