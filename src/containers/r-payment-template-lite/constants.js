import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  R_PAYMENT_TEMPLATE_CONTAINER_MOUNT: null,
  R_PAYMENT_TEMPLATE_CONTAINER_UNMOUNT: null,
  R_PAYMENT_TEMPLATE_CONTAINER_GET_DATA_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_GET_DATA_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_GET_DATA_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_VALIDATE_DATA_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_VALIDATE_DATA_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_VALIDATE_DATA_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_SECTION_COLLAPSE_TOGGLE: null,
  R_PAYMENT_TEMPLATE_CONTAINER_FIELD_FOCUS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_FIELD_BLUR: null,

  R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_TAB: null,
  R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA: null,
  R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA_PAYER_ACCOUNT: null,
  R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA_RECEIVER: null,

  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_DRAWER_STATUS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_ACCOUNT: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CUSTOMER_KPP: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_INTERNAL_PAYER_ACCOUNT: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_INTERNAL_RECEIVER_ACCOUNT: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYMENT_PRIORITY: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CBC_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CBC_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CBC_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAY_REASON: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_TAX_PERIOD_DAY1: null,
  R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CHARGE_TYPE: null,

  R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PURPOSE: null,
  R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PAYER_NAME: null,

  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION: null,

  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_BACK: null,

  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_FAIL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT_FAIL: null,

  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_CONFIRM: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_CANCEL: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_FAIL: null,

  R_PAYMENT_TEMPLATE_CONTAINER_SAVE_RECEIVER_REQUEST: null,
  R_PAYMENT_TEMPLATE_CONTAINER_SAVE_RECEIVER_SUCCESS: null,
  R_PAYMENT_TEMPLATE_CONTAINER_SAVE_RECEIVER_FAIL: null,

  R_PAYMENT_TEMPLATE_CONTAINER_PAYMENT_TYPE_TOGGLE: null
});

export const LOCAL_REDUCER = 'RPaymentTemplateContainerReducer';

export const LOCAL_ROUTER_ALIASES = {
  LIST: '/r-payments',
  R_PAYMENT: '/r-payments/{:id}',
  R_PAYMENT_TEMPLATE: '/r-payments/new?type=template&templateId={:templateId}'
};

export const COMPONENT_STYLE_NAME = 'b-r-payment-template-container';
