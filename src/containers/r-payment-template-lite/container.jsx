import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Loader from '@rbo/components/lib/loader/Loader';
import { getFormattedAccNum, getScrollTopByDataId, smoothScrollTo } from 'utils';
import RboDocument, {
  RboDocumentPanel,
  RboDocumentOperations,
  RboDocumentContent,
  RboDocumentMain,
  RboDocumentHead,
  RboDocumentHeadStatus,
  RboDocumentHeadTabs,
  RboDocumentBody,
  RboDocumentNotes,
  RboDocumentExtra,
  RboDocumentExtraSection,
  RboDocumentStructure,
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import RboForm, { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import { getFieldElementSelectorByIds } from 'components/ui-components/rbo-form-compact/utils';
import { COMPONENT_STYLE_NAME } from './constants';
import {
  SectionPrimary,
  SectionInternalPrimary,
  SectionPayer,
  SectionInternalPayer,
  SectionReceiver,
  SectionInternalReceiver,
  SectionPurpose,
  SectionInternalPurpose,
  SectionBudget
} from './sections';
import './style.css';

class RPaymentContainer extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),
    isDataError: PropTypes.bool,
    isDataLoading: PropTypes.bool,
    isEditable: PropTypes.bool.isRequired,

    documentTabs: PropTypes.instanceOf(List).isRequired,
    documentStructure: PropTypes.instanceOf(List).isRequired,
    documentValues: PropTypes.instanceOf(Map).isRequired,
    documentData: PropTypes.instanceOf(Map).isRequired,
    documentDictionaries: PropTypes.instanceOf(Map).isRequired,
    formSections: PropTypes.instanceOf(Map).isRequired,
    formWarnings: PropTypes.instanceOf(Map).isRequired,
    formErrors: PropTypes.instanceOf(Map).isRequired,
    operations: PropTypes.instanceOf(List).isRequired,

    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    changeTabAction: PropTypes.func.isRequired,
    fieldFocusAction: PropTypes.func.isRequired,
    fieldBlurAction: PropTypes.func.isRequired,
    changeDataAction: PropTypes.func.isRequired,
    dictionarySearchAction: PropTypes.func.isRequired,
    sectionCollapseToggleAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    saveReceiverAction: PropTypes.func.isRequired,
    toggleBudgetAction: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired
  };

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.mountAction(params);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search)
        && nextProps.history.action !== 'REPLACE') {
      this.props.mountAction(nextProps.match.params);
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  handleTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleFormSectionCollapseToggle = ({ sectionId, isCollapsed }) => {
    this.props.sectionCollapseToggleAction(sectionId, isCollapsed);
  };

  handleSearch = ({ fieldId, fieldSearchValue }) => {
    this.props.dictionarySearchAction(fieldId, fieldSearchValue);
  };

  handleFocus = ({ fieldId }) => {
    this.props.fieldFocusAction(fieldId);
  };

  handleBlur = ({ fieldId }) => {
    this.props.fieldBlurAction(fieldId);
  };

  handleChange = ({ fieldId, fieldValue, dictionaryValue }) => {
    this.props.changeDataAction(fieldId, fieldValue, dictionaryValue);
  };

  handleStructureTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleStructureSectionClick = ({ tabId, sectionId }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    setTimeout(() => {
      const fieldId = parentTab.get('sections').find(section => section.get('id') === sectionId).get('firstFieldId');
      smoothScrollTo(
        getScrollTopByDataId(sectionId, DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR),
        () => {
          document.querySelector(`#${fieldId}`) &&
          setTimeout(() => { document.querySelector(`#${fieldId}`).focus(); }, 0);
        }
      );
    }, 0);
  };

  handleStructureErrorClick = ({ tabId, fieldsIds }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    const fieldElementSelector = getFieldElementSelectorByIds(fieldsIds);
    fieldElementSelector && setTimeout(() => { fieldElementSelector.focus(); }, 0);
  };

  formFieldOptionRenderer = (fieldId, option, highlight) => {
    switch (fieldId) {
      case 'payerAccount':
      case 'internalPayerAccount':
      case 'internalReceiverAccount':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.account}
            describe={option && option.orgName}
            extra={option && option.availableBalance}
            highlight={{
              value: highlight,
              inTitle: option && option.account
            }}
          />
        );
      case 'receiverAccount':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.account}
            describe={option && option.name}
            extra={option && option.inn}
            highlight={{
              value: highlight,
              inTitle: option && option.account
            }}
          />
        );
      case 'receiverINN':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.inn}
            describe={option && option.name}
            highlight={{
              value: highlight,
              inTitle: option && option.inn
            }}
          />
        );
      case 'receiverName':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.name}
            describe={option && option.account}
            extra={option && option.inn}
            highlight={{
              value: highlight,
              inTitle: option && option.name
            }}
          />
        );
      case 'payerKPP':
      case 'receiverKPP':
        return (
          <RboFormFieldPopupOptionInner
            isInline
            title={option && option.title}
            highlight={{
              value: highlight,
              inTitle: option && option.title
            }}
          />
        );
      case 'receiver':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.account}
            describe={option && option.name}
            extra={option && option.inn}
            highlight={{
              value: highlight,
              inTitle: option && option.account,
              inTitleValue: getFormattedAccNum(highlight),
              inDescribe: option && option.name,
              inExtra: option && option.inn
            }}
          />
        );
      case 'receiverBankBIC':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.title}
            describe={option && option.bankName}
            extra={option && option.corrAccount}
            highlight={{
              value: highlight,
              inTitle: option && option.title
            }}
          />
        );
      case 'paymentPriority':
        return (
          <RboFormFieldPopupOptionInner
            isInline
            title={option && option.title}
            describe={option && option.description}
          />
        );
      case 'chargeType':
        return (
          <RboFormFieldPopupOptionInner
            isInline
            title={option && option.title}
            describe={option && option.comment}
          />
        );
      case 'cbc':
      case 'currencyOperationType':
        return (
          <RboFormFieldPopupOptionInner
            isInline
            title={option && option.title}
            describe={option && option.description}
            highlight={{
              value: highlight,
              inTitle: option && option.title
            }}
          />
        );
      case 'drawerStatus':
      case 'taxPeriodDay1':
      case 'payReason':
        return (
          <RboFormFieldPopupOptionInner
            isInline
            title={option && option.title}
            describe={option && option.comment}
            highlight={{
              value: highlight,
              inTitle: option && option.title
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  render() {
    const {
      isDataError,
      isDataLoading,
      isEditable,
      documentTabs,
      documentValues,
      documentData,
      documentStructure,
      documentDictionaries,
      formSections,
      formWarnings,
      formErrors,
      operations,
      operationAction,
      saveReceiverAction,
      toggleBudgetAction,
      title
    } = this.props;

    const sectionsProps = {
      isEditable,
      documentValues,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formFieldOptionRenderer: this.formFieldOptionRenderer,
      onCollapseToggle: this.handleFormSectionCollapseToggle,
      onFieldFocus: this.handleFocus,
      onFieldBlur: this.handleBlur,
      onFieldChange: this.handleChange,
      onFieldSearch: this.handleSearch,
      toggleBudgetAction
    };

    return (
      <div className={COMPONENT_STYLE_NAME}>
        <div className={`${COMPONENT_STYLE_NAME}__inner`}>
          {isDataLoading && <Loader centered />}
          {!isDataLoading && !isDataError &&
          <RboDocument>
            <RboDocumentPanel>
              <RboDocumentOperations
                operations={operations}
                operationAction={operationAction}
              />
            </RboDocumentPanel>
            <RboDocumentContent>
              <RboDocumentMain>
                <RboDocumentHead title={title} isTemplate>
                  <RboDocumentHeadStatus
                    describe="Для документа: Платежное поручение"
                  />
                  <RboDocumentHeadTabs
                    tabs={documentTabs}
                    onTabClick={this.handleTabClick}
                  />
                </RboDocumentHead>
                <RboDocumentBody>
                  {!!documentStructure.find(tab => tab.get('id') === 'main').get('isActive') &&
                  <RboForm initialFocusFieldId="amount">
                    {formSections.getIn(['primary', 'isVisible']) &&
                    <SectionPrimary
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['primary', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['primary', 'isCollapsed'])}
                    />
                    }
                    {formSections.getIn(['payer', 'isVisible']) &&
                    <SectionPayer
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['payer', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['payer', 'isCollapsed'])}
                    />
                    }
                    {formSections.getIn(['receiver', 'isVisible']) &&
                    <SectionReceiver
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['receiver', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['receiver', 'isCollapsed'])}
                      saveReceiverAction={saveReceiverAction}
                    />
                    }
                    {formSections.getIn(['internalPrimary', 'isVisible']) &&
                    <SectionInternalPrimary
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['internalPrimary', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['internalPrimary', 'isCollapsed'])}
                    />
                    }
                    {formSections.getIn(['internalPayer', 'isVisible']) &&
                    <SectionInternalPayer
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['internalPayer', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['internalPayer', 'isCollapsed'])}
                    />
                    }
                    {formSections.getIn(['internalReceiver', 'isVisible']) &&
                    <SectionInternalReceiver
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['internalReceiver', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['internalReceiver', 'isCollapsed'])}
                    />
                    }
                    {formSections.getIn(['purpose', 'isVisible']) &&
                    <SectionPurpose
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['purpose', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['purpose', 'isCollapsed'])}
                    />
                    }
                    {formSections.getIn(['internalPurpose', 'isVisible']) &&
                    <SectionInternalPurpose
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['internalPurpose', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['internalPurpose', 'isCollapsed'])}
                    />
                    }
                    {formSections.getIn(['budget', 'isVisible']) &&
                    <SectionBudget
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['budget', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['budget', 'isCollapsed'])}
                    />
                    }
                  </RboForm>
                  }
                  {!!documentStructure.find(tab => tab.get('id') === 'notes').get('isActive') &&
                  <RboDocumentNotes
                    isEditable={isEditable}
                    fieldId="docNote"
                    value={documentData.get('docNote')}
                    onChange={this.handleChange}
                  />
                  }
                </RboDocumentBody>
              </RboDocumentMain>
              <RboDocumentExtra>
                <RboDocumentExtraSection title="Структура документа">
                  <RboDocumentStructure
                    structure={documentStructure}
                    onTabClick={this.handleStructureTabClick}
                    onSectionClick={this.handleStructureSectionClick}
                    onErrorClick={this.handleStructureErrorClick}
                    isErrorClickable={isEditable}
                  />
                </RboDocumentExtraSection>
              </RboDocumentExtra>
            </RboDocumentContent>
          </RboDocument>
          }
        </div>
      </div>
    );
  }
}

export default RPaymentContainer;
