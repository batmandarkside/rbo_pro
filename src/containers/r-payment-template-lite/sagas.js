import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import qs from 'qs';
import { goBack, replace, push } from 'react-router-redux';
import { addOrdinaryNotificationAction as dalNotificationAction }  from 'dal/notification/actions';
import {
  getTemplateErrors as dalGetTemplateErrors,
  saveTemplateData as dalSaveTemplate,
  getTemplateData as dalFetchSingleRPayTemplate,
  removeTemplate as dalRemoveTemplate
} from 'dal/r-payments/sagas';
import {
  getPaymentKinds as dalGetPaymentKinds,
  getCustomerKpps as dalGetCustomerKpps,
  getPaymentPriorities as dalGetPaymentPriorities,
  getBIC as dalGetBIC,
  getDrawerStatuses as dalGetDrawerStatuses,
  getCBC as dalGetCBC,
  getPayReasons as dalGetPayReasons,
  getTaxPeriods as dalGetTaxPeriods,
  getChargeTypes as dalGetChargeTypes,
  getVatCalculationRules as dalGetVatCalculationRules,
  getCurrencyOperationTypes as dalGetCurrencyOperationTypes
} from 'dal/dictionaries/sagas';
import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import {
  getCorrespondents as dalGetCorrespondents,
  saveCorrespondentData as dalSaveReceiver
} from 'dal/correspondents/sagas';
import { getIsDocNeedGetErrors } from 'utils';
import { ACTIONS, LOCAL_REDUCER, LOCAL_ROUTER_ALIASES } from './constants';

export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
export const docIdSelector = state => state.getIn([LOCAL_REDUCER, 'documentData', 'recordID']);
export const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
export const templateNameSelector = state => state.getIn([LOCAL_REDUCER, 'documentData', 'templateName']);

export function* successNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'success',
    title: title || 'Операция выполнена успешно',
    message: message || ''
  }));
}

export function* warningNotification(params) {
  const { title, message } = params;
  yield put(dalNotificationAction({
    type: 'warning',
    title: title || '',
    message: message || ''
  }));
}

export function* errorNotification(params) {
  const { name, title, message, stack } = params;
  let notificationMessage = '';
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => { notificationMessage += `${text}\n`; });
  } else notificationMessage = message;
  yield put(dalNotificationAction({
    type: 'error',
    title: title || 'Произошла ошибка на сервере',
    message: notificationMessage || ''
  }));
}

export function* confirmNotification(params) {
  const { level, title, message, success, cancel, autoDismiss } = params;
  yield put(dalNotificationAction({
    type: 'confirm',
    level: level || 'success',
    title: title || '',
    message: message || '',
    autoDismiss: typeof autoDismiss === 'number' ? autoDismiss : 20,
    success,
    cancel
  }));
}

export function* mountSaga(action) {
  const { payload: { params: { id } } } = action;
  yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_GET_DATA_REQUEST, payload: { id } });
}

export function* getDataSaga(action) {
  const { payload: { id } } = action;
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const type = query.type;
  const documentDictionaries = yield select(documentDictionariesSelector);
  const types = documentDictionaries.getIn(['paymentType', 'items']);

  try {
    const organizations = yield select(organizationsSelector);
    const documentData = yield call(dalFetchSingleRPayTemplate, { payload: { id } });

    const payerCity =  ` ${documentData.payerBankSettlementType || ''}. ${documentData.payerBankCity || ''}`;
    const receiverCity =  ` ${documentData.receiverBankSettlementType || ''}. ${documentData.receiverBankCity || ''}`;
    documentData.payerBankName = documentData.payerBankName && documentData.payerBankName.replace(payerCity, '');
    documentData.receiverBankName = documentData.receiverBankName && documentData.receiverBankName.replace(receiverCity, '');

    const userAccounts = yield call(dalGetAccounts, { payload: { currIsoCode: 'RUR', status: 'OPEN', type: 1 } });
    const payerBankBICCode = documentData.payerBankBIC ||
      (userAccounts && userAccounts.length && userAccounts[0].branchBic);
    const [
      paymentKind,
      customerKPP,
      paymentPriority,
      payerBankBIC,
      receiverBankBIC,
      drawerStatus,
      payReason,
      taxPeriodDay1,
      chargeType,
      vatCalculationRule,
      currencyOperationTypes
    ] = yield all([
      dalGetPaymentKinds(),
      dalGetCustomerKpps(),
      dalGetPaymentPriorities(),
      dalGetBIC({ payload: { code: payerBankBICCode || '' } }),
      dalGetBIC({ payload: { code: documentData.receiverBankBIC || '' } }),
      dalGetDrawerStatuses(),
      dalGetPayReasons(),
      dalGetTaxPeriods(),
      dalGetChargeTypes(),
      dalGetVatCalculationRules(),
      dalGetCurrencyOperationTypes({ payload: { code: '' } })
    ]);

    if (id === 'new' && type && !!types.find(item => type === item.get('value'))) documentData.paymentType = type;

    const documentErrors = (getIsDocNeedGetErrors(documentData.status))
      ? yield call(dalGetTemplateErrors, { payload: documentData })
      : {};

    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_GET_DATA_SUCCESS,
      payload: {
        documentData,
        documentErrors,
        documentDictionaries: {
          organizations: { items: organizations },
          paymentKind: { items: paymentKind },
          customerKPP: { items: customerKPP },
          payerAccount: { items: userAccounts },
          internalPayerAccount: { items: userAccounts },
          internalReceiverAccount: { items: userAccounts },
          paymentPriority: { items: paymentPriority },
          payerBankBIC: { items: payerBankBIC },
          receiverBankBIC: { items: receiverBankBIC },
          drawerStatus: { items: drawerStatus },
          payReason: { items: payReason },
          taxPeriodDay1: { items: taxPeriodDay1 },
          chargeType: { items: chargeType },
          vatCalculationRule: { items: vatCalculationRule },
          currencyOperationType: { items: currencyOperationTypes }
        }
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_GET_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Документ не найден.',
      message: 'Неверно указан идентификатор шаблона.'
    });
  }
}

export function* validateDataSaga() {
  const documentData = yield select(documentDataSelector);
  try {
    const documentErrors = yield call(dalGetTemplateErrors, { payload: documentData.toJS() });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_VALIDATE_DATA_SUCCESS,
      payload: { documentErrors }
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_VALIDATE_DATA_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно проверить данные документа.'
    });
  }
}

export function* saveDataSaga(action) {
  const { channels } = action;
  const documentData = yield select(documentDataSelector);
  try {
    const result = yield call(dalSaveTemplate, { payload: documentData.toJS() });
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_SUCCESS, payload: result, channels });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_FAIL, payload: { error }, channels });
  }
}

export function* saveDataSuccessSaga(action) {
  const { payload: { errors }, channels } = action;
  if (errors && errors.length) {
    if (errors.filter(error => error.level === '2').length) {
      yield put({ type: channels.success, payload: { status: 'haveErrors' } });
    } else if (errors.filter(error => error.level === '1').length) {
      yield put({ type: channels.success, payload: { status: 'haveWarnings' } });
    }
  } else {
    yield put({ type: channels.success, payload: { status: 'success' } });
  }
}

export function* saveDataFailSaga(action) {
  const { payload: { error }, channels } = action;

  yield put({ type: channels.fail });
  yield call(errorNotification, error.name === 'RBO Controls Error'
    ? {
      title: 'Шаблон не может быть сохранен.',
      message: 'Для сохранения шаблона требуется исправить ошибки. ' +
      'При наличии блокирующих ошибок шаблон не может быть сохранен.'
    }
    : {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно сохранить документ'
    }
  );
}

export function* changeDataSaga(action) {
  const { payload: { fieldId, fieldValue } } = action;
  switch (fieldId) {
    case 'payerAccount':
    case 'internalPayerAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA_PAYER_ACCOUNT,
        payload: { fieldValue }
      });
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_CONTAINER_FORMAT_PAYER_NAME });
      break;
    case 'receiver':
    case 'receiverINN':
    case 'receiverName':
    case 'internalReceiverAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA_RECEIVER,
        payload: { fieldValue }
      });
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PAYER_NAME });
      break;
    case 'amount':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PAYER_NAME });
      break;
    case 'receiverAccount':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PURPOSE });
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PAYER_NAME });
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA_RECEIVER,
        payload: { fieldValue }
      });
      break;
    case 'vatCalculationRule':
    case 'vatRate':
    case 'vatSum':
    case 'currencyOperationType':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PURPOSE });
      break;
    case 'paymentPurpose':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FORMAT_PAYER_NAME });
      break;
    default:
      break;
  }
}

export function* changeDataPayerAccountSaga(action) {
  const { payload: { fieldValue } } = action;
  if (fieldValue !== null) {
    const documentData = yield select(documentDataSelector);
    const payerBankBIC = documentData.get('payerBankBIC');
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_REQUEST,
      payload: { dictionarySearchValue: payerBankBIC, updateDataFlag: true }
    });
  }
}

export function* changeDataReceiverSaga(action) {
  const { payload: { fieldValue } } = action;
  if (fieldValue !== null) {
    const documentData = yield select(documentDataSelector);
    const receiverBankBIC = documentData.get('receiverBankBIC');
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST,
      payload: { dictionarySearchValue: receiverBankBIC, updateDataFlag: true }
    });
  }
}

export function* dictionarySearchSaga(action) {
  const { payload: { dictionaryId, dictionarySearchValue } } = action;
  switch (dictionaryId) {
    case 'drawerStatus':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_DRAWER_STATUS,
        payload: { dictionarySearchValue }
      });
      break;
    case 'payerAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_ACCOUNT,
        payload: { dictionarySearchValue }
      });
      break;
    case 'payerKPP':
    case 'receiverKPP':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CUSTOMER_KPP,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiver':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverINN':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverName':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'internalPayerAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_INTERNAL_PAYER_ACCOUNT,
        payload: { dictionarySearchValue }
      });
      break;
    case 'internalReceiverAccount':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_INTERNAL_RECEIVER_ACCOUNT,
        payload: { dictionarySearchValue }
      });
      break;
    case 'receiverBankBIC':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'paymentPriority':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYMENT_PRIORITY,
        payload: { dictionarySearchValue }
      });
      break;
    case 'currencyOperationType':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'cbc':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CBC_REQUEST,
        payload: { dictionarySearchValue }
      });
      break;
    case 'payReason':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAY_REASON,
        payload: { dictionarySearchValue }
      });
      break;
    case 'taxPeriodDay1':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_TAX_PERIOD_DAY1,
        payload: { dictionarySearchValue }
      });
      break;
    case 'chargeType':
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CHARGE_TYPE,
        payload: { dictionarySearchValue }
      });
      break;
    default:
      break;
  }
}

export function* dictionarySearchPayerBankBICSaga(action) {
  const { payload: { dictionarySearchValue, updateDataFlag } } = action;
  try {
    const result = yield call(dalGetBIC, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_SUCCESS,
      payload: result
    });
    if (updateDataFlag) {
      const selected = result.find(item => item.bic === dictionarySearchValue);
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA,
        payload: { fieldId: 'payerBankBIC', fieldValue: selected ? selected.id : '' }
      });
    }
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник БИК'
    });
  }
}

export function* dictionarySearchReceiverSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}

export function* dictionarySearchReceiverAccountSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}

export function* dictionarySearchReceiverINNSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}

export function* dictionarySearchReceiverNameSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCorrespondents, { payload: { requisite: dictionarySearchValue, offsetStep: 10 } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник корреспондентов'
    });
  }
}


export function* dictionarySearchReceiverBankBICSaga(action) {
  const { payload: { dictionarySearchValue, updateDataFlag } } = action;
  try {
    const result = yield call(dalGetBIC, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_SUCCESS,
      payload: result
    });
    if (updateDataFlag) {
      const selected = result.find(item => item.bic === dictionarySearchValue);
      yield put({
        type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA,
        payload: { fieldId: 'receiverBankBIC', fieldValue: selected ? selected.id : '' }
      });
    }
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник БИК'
    });
  }
}

export function* dictionarySearchCBCSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCBC, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CBC_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CBC_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник КБК'
    });
  }
}

export function* dictionarySearchCurrencyOperationTypeSaga(action) {
  const { payload: { dictionarySearchValue } } = action;
  try {
    const result = yield call(dalGetCurrencyOperationTypes, { payload: { code: dictionarySearchValue } });
    yield put({
      type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно загрузить справочник кодов вида валютной операции'
    });
  }
}

export function* saveReceiverSaga() {
  const documentData = yield select(documentDataSelector);
  const data = {
    account: documentData.get('receiverAccount'),
    receiverINN: documentData.get('receiverINN'),
    receiverKPP: documentData.get('receiverKPP'),
    receiverName: documentData.get('receiverName'),
    receiverBankBIC: documentData.get('receiverBankBIC')
  };
  try {
    yield call(dalSaveReceiver, { payload: data });
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_RECEIVER_SUCCESS });
    yield call(successNotification, { title: 'Получатель добавлен в справочник.' });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_RECEIVER_FAIL });
    yield call(errorNotification, error.name === 'RBO Controls Error'
      ? error
      : {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно сохранить получателя в справочнике контрагентов'
      }
    );
  }
}

export function* operationSaga(action) {
  const { payload: { operationId } } = action;
  switch (operationId) {
    case 'back':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_BACK });
      break;
    case 'createDocument':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT });
      break;
    case 'save':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_REQUEST });
      break;
    case 'remove':
      yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_REQUEST });
      break;
    default:
      break;
  }
}

export function* operationBackSaga() {
  const location = yield select(routingLocationSelector);
  const prevLocation = location.get('prevLocation');
  if (prevLocation && prevLocation.get('pathname') !== location.get('pathname')) yield put(goBack());
  else yield put(replace(LOCAL_ROUTER_ALIASES.LIST));
}

export function* operationSaveSaga() {
  yield put({
    type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_SUCCESS,
      fail: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_FAIL
    }
  });
}

export function* operationSaveSuccessSaga(action) {
  const { payload: { status } } = action;
  const templateName = yield select(templateNameSelector);

  switch (status) {
    case 'haveErrors':
      yield call(errorNotification, {
        title: 'Шаблон сохранен с ошибками',
        message: 'Для дальнейшего корректного сохранения и подтверждения документа требуется исправить ошибки'
      });
      break;
    case 'haveWarnings':
      yield call(warningNotification, {
        title: 'Шаблон сохранен с предупреждениями.'
      });
      break;
    default:
      yield call(successNotification, {
        title: 'Шаблон  успешно сохранен',
        message: `Шаблон сохранен с именем: ${templateName}`
      });
  }
}

export function* operationRemoveSaga() {
  yield call(confirmNotification, {
    level: 'error',
    message: 'Вы действительно хотите удалить шаблон?',
    autoDismiss: 0,
    success: {
      label: 'Подтвердить',
      action: { type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_CONFIRM }
    },
    cancel: {
      label: 'Отмена',
      action: { type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_CANCEL }
    }
  });
}

export function* operationRemoveConfirmSaga() {
  const docId = yield select(docIdSelector);
  const templateName = yield select(templateNameSelector);
  try {
    yield call(dalRemoveTemplate, { payload: docId });
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_BACK });
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_SUCCESS });
    yield call(successNotification, {
      title: 'Шаблон успешно удален',
      message: `Шаблон с именем ${templateName} удален`
    });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_FAIL });
    yield call(errorNotification, {
      title: 'Произошла ошибка на сервере.',
      message: 'Невозможно удалить шаблон'
    });
  }
}

export function* operationCreateDocument() {
  yield put({
    type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_REQUEST,
    channels: {
      success: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT_SUCCESS,
      fail: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT_FAIL
    }
  });
}

export function* createDocumentSaga() {
  const docId = yield select(docIdSelector);
  yield put(push(LOCAL_ROUTER_ALIASES.R_PAYMENT_TEMPLATE.replace('{:templateId}', docId)));
}

export default function* saga() {
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_MOUNT, mountSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_GET_DATA_REQUEST, getDataSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_VALIDATE_DATA_REQUEST, validateDataSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_REQUEST, saveDataSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_SUCCESS, saveDataSuccessSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_DATA_FAIL, saveDataFailSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA, changeDataSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA_PAYER_ACCOUNT, changeDataPayerAccountSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA_RECEIVER, changeDataReceiverSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH, dictionarySearchSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_PAYER_BANK_BIC_REQUEST, dictionarySearchPayerBankBICSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_REQUEST, dictionarySearchReceiverSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_ACCOUNT_REQUEST, dictionarySearchReceiverAccountSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_INN_REQUEST, dictionarySearchReceiverINNSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_NAME_REQUEST, dictionarySearchReceiverNameSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_RECEIVER_BANK_BIC_REQUEST, dictionarySearchReceiverBankBICSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CBC_REQUEST, dictionarySearchCBCSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH_CURRENCY_OPERATION_TYPE_REQUEST, dictionarySearchCurrencyOperationTypeSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_RECEIVER_REQUEST, saveReceiverSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION, operationSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_BACK, operationBackSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_REQUEST, operationSaveSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT, operationCreateDocument);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_CREATE_DOCUMENT_SUCCESS, createDocumentSaga);

  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemoveSaga);
  yield takeLatest(ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
}
