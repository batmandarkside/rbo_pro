import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldSearch,
  RboFormFieldTextarea,
  RboFormFieldValue,
  RboFormCollapse,
  RboFormFieldText
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

const RPaymentSectionInternalReceiver = (props) => {
  const {
    isCollapsible,
    isCollapsed,
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldChange,
    onFieldSearch,
    onCollapseToggle
  } = props;

  const handleCollapseToggle = () => {
    onCollapseToggle({ sectionId: 'internalReceiver', isCollapsed: !isCollapsed });
  };

  return (
    <RboFormSection
      id="internalReceiver"
      title="На свой счет"
      isCollapsible={isCollapsible}
      isCollapsed={isCollapsed}
    >
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldSearch
                  id="internalReceiverAccount"
                  label="Счет №"
                  inputType="Account"
                  value={documentData.get('receiverAccount')}
                  searchValue={documentDictionaries.getIn(['internalReceiverAccount', 'searchValue'])}
                  options={documentDictionaries.getIn(['internalReceiverAccount', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('receiverAccount')}
                  isError={formErrors.get('receiverAccount')}
                  isSearching={documentDictionaries.getIn(['internalReceiverAccount', 'isSearching'])}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="internalReceiverAccount"
                  label="Счет зачисления"
                  value={documentValues.get('receiverAccount')}
                  isWarning={formWarnings.get('receiverAccount')}
                  isError={formErrors.get('receiverAccount')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                <RboFormCollapse
                  title="Скрыть детали"
                  collapsedTitle="Показать детали"
                  pseudoLabel
                  isCollapsed={isCollapsed}
                  onCollapseToggle={handleCollapseToggle}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="50%">
                <RboFormFieldValue
                  id="receiverINN"
                  label="ИНН/КИО"
                  value={documentValues.get('receiverINN')}
                  isWarning={formWarnings.get('receiverINN')}
                  isError={formErrors.get('receiverINN')}
                />
              </RboFormCell>
              <RboFormCell size="50%">
                {isEditable &&
                <RboFormFieldText
                  id="receiverKPP"
                  label="КПП"
                  value={documentData.get('receiverKPP')}
                  maxLength={9}
                  isWarning={formWarnings.get('receiverKPP')}
                  isError={formErrors.get('receiverKPP')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="receiverKPP"
                  label="КПП"
                  value={documentValues.get('receiverKPP')}
                  isWarning={formWarnings.get('receiverKPP')}
                  isError={formErrors.get('receiverKPP')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="1/3">
                <RboFormFieldValue
                  id="receiverBankBIC"
                  label="БИК"
                  value={documentValues.get('receiverBankBIC')}
                  isWarning={formWarnings.get('receiverBankBIC')}
                  isError={formErrors.get('receiverBankBIC')}
                />
              </RboFormCell>
              <RboFormCell size="2/3">
                <RboFormFieldValue
                  id="receiverBankCorrAccount"
                  label="Кор. счет"
                  value={documentValues.get('receiverBankCorrAccount')}
                  isWarning={formWarnings.get('receiverBankCorrAccount')}
                  isError={formErrors.get('receiverBankCorrAccount')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="receiverName"
                  label="Наименование получателя"
                  value={documentData.get('receiverName')}
                  maxLength={160}
                  isWarning={formWarnings.get('receiverName')}
                  isError={formErrors.get('receiverName')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="receiverName"
                  label="Наименование получателя"
                  value={documentValues.get('receiverName')}
                  isWarning={formWarnings.get('receiverName')}
                  isError={formErrors.get('receiverName')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id="receiverBankName"
                  label="Банк получателя"
                  value={documentValues.get('receiverBankName')}
                  isWarning={formWarnings.get('receiverBankName')}
                  isError={formErrors.get('receiverBankName')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldText
                  id="uip"
                  label="Код/УИП"
                  value={documentData.get('uip')}
                  maxLength={25}
                  isWarning={formWarnings.get('uip')}
                  isError={formErrors.get('uip')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="uip"
                  label="Код/УИП"
                  value={documentValues.get('uip')}
                  isWarning={formWarnings.get('uip')}
                  isError={formErrors.get('uip')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

RPaymentSectionInternalReceiver.propTypes = {
  isCollapsible: PropTypes.bool,
  isCollapsed: PropTypes.bool,
  isEditable: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onCollapseToggle: PropTypes.func.isRequired
};

export default RPaymentSectionInternalReceiver;
