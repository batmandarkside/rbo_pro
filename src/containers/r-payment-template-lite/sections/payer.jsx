import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldDigital,
  RboFormFieldSearch,
  RboFormFieldSuggest,
  RboFormFieldTextarea,
  RboFormFieldValue,
  RboFormCollapse
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import { getPayerInnIsDisabled } from '../utils';

const RPaymentSectionPayer = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldChange,
    onFieldSearch,
    onCollapseToggle,
    isCollapsed,
    isCollapsible
  } = props;

  const handleCollapseToggle = () => {
    onCollapseToggle({ sectionId: 'payer', isCollapsed: !isCollapsed });
  };

  return (
    <RboFormSection
      id="payer"
      title="Плательщик"
      isCollapsible={isCollapsible}
      isCollapsed={isCollapsed}
    >
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldSearch
                  id="payerAccount"
                  label="Счет №"
                  inputType="Account"
                  value={documentData.get('payerAccount')}
                  searchValue={documentDictionaries.getIn(['payerAccount', 'searchValue'])}
                  options={documentDictionaries.getIn(['payerAccount', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('payerAccount')}
                  isError={formErrors.get('payerAccount')}
                  isSearching={documentDictionaries.getIn(['payerAccount', 'isSearching'])}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="payerAccount"
                  label="Счет №"
                  value={documentValues.get('payerAccount')}
                  isWarning={formWarnings.get('payerAccount')}
                  isError={formErrors.get('payerAccount')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset isMain>
            <RboFormRow>
              <RboFormCell>
                <RboFormCollapse
                  title="Скрыть детали"
                  collapsedTitle="Показать детали"
                  pseudoLabel
                  isCollapsed={isCollapsed}
                  onCollapseToggle={handleCollapseToggle}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="50%">
                {isEditable &&
                <RboFormFieldDigital
                  id="payerINN"
                  label="ИНН/КИО"
                  value={documentData.get('payerINN')}
                  maxLength={12}
                  isDisabled={getPayerInnIsDisabled(
                    documentData.get('drawerStatus'),
                    documentDictionaries.getIn(['drawerStatus', 'items'])
                  )}
                  isWarning={formWarnings.get('payerINN')}
                  isError={formErrors.get('payerINN')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="payerINN"
                  label="ИНН/КИО"
                  value={documentValues.get('payerINN')}
                  isWarning={formWarnings.get('payerINN')}
                  isError={formErrors.get('payerINN')}
                />
                }
              </RboFormCell>
              <RboFormCell size="50%">
                {isEditable &&
                <RboFormFieldSuggest
                  id="payerKPP"
                  label="КПП"
                  inputType="Text"
                  value={documentData.get('payerKPP')}
                  options={documentDictionaries.getIn(['customerKPP', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  maxLength={9}
                  isWarning={formWarnings.get('payerKPP')}
                  isError={formErrors.get('payerKPP')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="payerKPP"
                  label="КПП"
                  value={documentValues.get('payerKPP')}
                  isWarning={formWarnings.get('payerKPP')}
                  isError={formErrors.get('payerKPP')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="1/3">
                <RboFormFieldValue
                  id="payerBankBIC"
                  label="БИК"
                  value={documentValues.get('payerBankBIC')}
                  isWarning={formWarnings.get('payerBankBIC')}
                  isError={formErrors.get('payerBankBIC')}
                />
              </RboFormCell>
              <RboFormCell size="2/3">
                <RboFormFieldValue
                  id="payerBankCorrAccount"
                  label="Кор. счет"
                  value={documentValues.get('payerBankCorrAccount')}
                  isWarning={formWarnings.get('payerBankCorrAccount')}
                  isError={formErrors.get('payerBankCorrAccount')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="payerName"
                  label="Наименование плательщика"
                  value={documentData.get('payerName')}
                  maxLength={160}
                  isWarning={formWarnings.get('payerName')}
                  isError={formErrors.get('payerName')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="payerName"
                  label="Наименование плательщика"
                  value={documentValues.get('payerName')}
                  isWarning={formWarnings.get('payerName')}
                  isError={formErrors.get('payerName')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldValue
                  id="payerBankName"
                  label="Банк плательщика"
                  value={documentValues.get('payerBankName')}
                  isWarning={formWarnings.get('payerBankName')}
                  isError={formErrors.get('payerBankName')}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

RPaymentSectionPayer.propTypes = {
  isEditable: PropTypes.bool,
  isCollapsed: PropTypes.bool,
  isCollapsible: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  onCollapseToggle: PropTypes.func.isRequired
};

export default RPaymentSectionPayer;
