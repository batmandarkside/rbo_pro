import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldDecimalNumber,
  RboFormFieldDropdown,
  RboFormFieldSearch,
  RboFormFieldValue,
  RboFormFieldTextarea,
  RboFormFieldText
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import { getVatRateIsDisabled, getVatSumIsDisabled, getCurrencyOperationTypeIsDisabled } from '../utils';

const RPaymentSectionPrimary = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange
  } = props;

  return (
    <RboFormSection
      id="primary"
      title="Сумма и назначение платежа"
    >
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldText
                  id="templateName"
                  label="Наименование шаблона"
                  value={documentData.get('templateName')}
                  maxLength={255}
                  isWarning={formWarnings.get('templateName')}
                  isError={formErrors.get('templateName')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="50%">
                {isEditable &&
                <RboFormFieldDecimalNumber
                  id="amount"
                  label="Сумма"
                  value={documentData.get('amount')}
                  maxLength={13}
                  isShowZero
                  isWarning={formWarnings.get('amount')}
                  isError={formErrors.get('amount')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="amount"
                  label="Сумма"
                  value={documentValues.get('amount')}
                  textAlign="right"
                  isWarning={formWarnings.get('amount')}
                  isError={formErrors.get('amount')}
                />
                }
              </RboFormCell>
              <RboFormCell size="50%">
                <RboFormFieldDropdown
                  id="vatCalculationRule"
                  label="Способ расчета НДС"
                  value={documentData.get('vatCalculationRule')}
                  options={documentDictionaries.getIn(['vatCalculationRule', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  isWarning={formWarnings.get('vatCalculationRule')}
                  isError={formErrors.get('vatCalculationRule')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="50%">
                <RboFormFieldDecimalNumber
                  id="vatRate"
                  label="%"
                  value={documentData.get('vatRate')}
                  maxLength={4}
                  isShowZero
                  isDisabled={getVatRateIsDisabled(
                    documentData.get('vatCalculationRule'),
                    documentDictionaries.getIn(['vatCalculationRule', 'items'])
                  )}
                  isWarning={formWarnings.get('vatRate')}
                  isError={formErrors.get('vatRate')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
              </RboFormCell>
              <RboFormCell size="50%">
                <RboFormFieldDecimalNumber
                  id="vatSum"
                  label="НДС"
                  value={documentData.get('vatSum')}
                  maxLength={16}
                  isShowZero
                  isDisabled={getVatSumIsDisabled(
                    documentData.get('vatCalculationRule'),
                    documentDictionaries.getIn(['vatCalculationRule', 'items'])
                  )}
                  isWarning={formWarnings.get('vatSum')}
                  isError={formErrors.get('vatSum')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="paymentPurpose"
                  label="Назначение платежа"
                  value={documentData.get('paymentPurpose')}
                  height={3}
                  isWarning={formWarnings.get('paymentPurpose')}
                  isError={formErrors.get('paymentPurpose')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="paymentPurpose"
                  label="Назначение платежа"
                  value={documentValues.get('paymentPurpose')}
                  isWarning={formWarnings.get('paymentPurpose')}
                  isError={formErrors.get('paymentPurpose')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldSearch
                  id="currencyOperationType"
                  label="Код вида валютной операции"
                  inputType="Digital"
                  value={documentData.get('currencyOperationType')}
                  maxLength={5}
                  searchValue={documentDictionaries.getIn(['currencyOperationType', 'searchValue'])}
                  options={documentDictionaries.getIn(['currencyOperationType', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupStyle={{ width: 524 }}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isDisabled={getCurrencyOperationTypeIsDisabled(
                    {
                      payerAccountValue: documentData.get('payerAccount'),
                      payerAccountDictionary: documentDictionaries.getIn(['payerAccount', 'items']),
                      receiverAccountValue: documentData.get('receiverAccount')
                    }
                  )}
                  isWarning={formWarnings.get('currencyOperationType')}
                  isError={formErrors.get('currencyOperationType')}
                  isSearching={documentDictionaries.getIn(['currencyOperationType', 'isSearching'])}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

RPaymentSectionPrimary.propTypes = {
  isEditable: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default RPaymentSectionPrimary;
