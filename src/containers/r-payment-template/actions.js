import { ACTIONS } from './constants';

/**
 *    action загрузки контейнера
 */
export const mountAction = params => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_MOUNT,
  payload: { params }
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_UNMOUNT
});

/**
 *    action переключения табов
 *    @param {string} tabId
 */
export const changeTabAction = tabId => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_TAB,
  payload: { tabId }
});

/**
 *    action focus на поле формы
 *    @param {string} fieldId
 */
export const fieldFocusAction = fieldId => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FIELD_FOCUS,
  payload: { fieldId }
});

/**
 *    action blur на поле формы
 *    @param {string} fieldId
 */
export const fieldBlurAction = fieldId => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_FIELD_BLUR,
  payload: { fieldId }
});

/**
 *    action изменения данных
 *    @param {string} fieldId
 *    @param {string} fieldValue
 */
export const changeDataAction = (fieldId, fieldValue) => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_CHANGE_DATA,
  payload: { fieldId, fieldValue }
});

/**
 *    action проверки данных
 */
export const validateDataAction = () => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_VALIDATE_DATA_REQUEST
});

/**
 *    action поиска по справочникам
 *    @param {string} dictionaryId
 *    @param {string} dictionarySearchValue
 */
export const dictionarySearchAction = (dictionaryId, dictionarySearchValue) => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_DICTIONARY_SEARCH,
  payload: { dictionaryId, dictionarySearchValue }
});

/**
 *    action сворачивания / разворачивания секции формы
 *    @param {string} sectionId
 *    @param {boolean} isCollapsed
 */
export const sectionCollapseToggleAction = (sectionId, isCollapsed) => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SECTION_COLLAPSE_TOGGLE,
  payload: { sectionId, isCollapsed }
});

/**
 *    action выполнения операции
 *    @param {string} operationId
 */
export const operationAction = operationId => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_OPERATION,
  payload: { operationId }
});

/**
 *    action сохранения получателя в справочник контрагентов
 */
export const saveReceiverAction = () => ({
  type: ACTIONS.R_PAYMENT_TEMPLATE_CONTAINER_SAVE_RECEIVER_REQUEST
});
