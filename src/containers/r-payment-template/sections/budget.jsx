import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldset,
  RboFormFieldDigital,
  RboFormFieldDropdown,
  RboFormFieldSearch,
  RboFormFieldSuggest,
  RboFormFieldText,
  RboFormFieldValue,
  RboFormAutoTransitions
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

const RPaymentSectionBudget = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange
  } = props;

  return (
    <RboFormSection
      id="budget"
      title="Бюджетный платеж"
    >
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="50%">
                {isEditable &&
                <RboFormFieldSuggest
                  id="cbc"
                  label="104 КБК"
                  inputType="Text"
                  value={documentData.get('cbc')}
                  options={documentDictionaries.getIn(['cbc', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupStyle={{ width: 524 }}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  maxLength={20}
                  isDisabled={!documentData.get('drawerStatus')}
                  isWarning={formWarnings.get('cbc')}
                  isError={formErrors.get('cbc')}
                  isSearching={documentDictionaries.getIn(['cbc', 'isSearching'])}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="cbc"
                  label="104 КБК"
                  value={documentValues.get('cbc')}
                  isWarning={formWarnings.get('cbc')}
                  isError={formErrors.get('cbc')}
                />
                }
              </RboFormCell>
              <RboFormCell size="26%">
                {isEditable &&
                <RboFormFieldText
                  id="ocato"
                  label="105 ОКТМО"
                  value={documentData.get('ocato')}
                  maxLength={11}
                  isDisabled={!documentData.get('drawerStatus')}
                  isWarning={formWarnings.get('ocato')}
                  isError={formErrors.get('ocato')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="ocato"
                  label="105 ОКТМО"
                  value={documentValues.get('ocato')}
                  isWarning={formWarnings.get('ocato')}
                  isError={formErrors.get('ocato')}
                />
                }
              </RboFormCell>
              <RboFormCell size="24%">
                {isEditable &&
                <RboFormFieldSearch
                  id="payReason"
                  label="106 Пок. основания"
                  value={documentData.get('payReason')}
                  maxLength={2}
                  options={documentDictionaries.getIn(['payReason', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupStyle={{ left: 'auto', right: 0, width: 502 }}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isDisabled={!documentData.get('drawerStatus')}
                  isWarning={formWarnings.get('payReason')}
                  isError={formErrors.get('payReason')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="payReason"
                  label="106 Пок. основания"
                  value={documentValues.get('payReason')}
                  isWarning={formWarnings.get('payReason')}
                  isError={formErrors.get('payReason')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldset
                  label="107 Налог. период / Код тамож. органа"
                >
                  <RboFormFieldDropdown
                    id="taxOrCustoms"
                    size="50%"
                    value={documentData.get('taxOrCustoms')}
                    options={documentDictionaries.getIn(['taxOrCustoms', 'items']).toJS()}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isDisabled={!documentData.get('drawerStatus')}
                    isWarning={formWarnings.get('taxOrCustoms')}
                    isError={formErrors.get('taxOrCustoms')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  {documentData.get('taxOrCustoms') === 'tax' &&
                  <RboFormAutoTransitions>
                    <RboFormFieldSuggest
                      id="taxPeriodDay1"
                      size="16%"
                      inputType="Text"
                      maxLength={2}
                      value={documentData.get('taxPeriodDay1')}
                      options={documentDictionaries.getIn(['taxPeriodDay1', 'items']).toJS()}
                      optionRenderer={formFieldOptionRenderer}
                      popupStyle={{ width: 200 }}
                      popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                      isDisabled={!documentData.get('drawerStatus')}
                      isWarning={formWarnings.get('taxPeriodDay1')}
                      isError={formErrors.get('taxPeriodDay1')}
                      isSearching={documentDictionaries.getIn(['taxPeriodDay1', 'isSearching'])}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      onSearch={onFieldSearch}
                      onChange={onFieldChange}
                    />
                    <RboFormFieldDigital
                      id="taxPeriodMonth"
                      size="14.5%"
                      value={documentData.get('taxPeriodMonth')}
                      maxLength={2}
                      isWarning={formWarnings.get('taxPeriodMonth')}
                      isError={formErrors.get('taxPeriodMonth')}
                      isDisabled={!documentData.get('drawerStatus')}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      onChange={onFieldChange}
                    />
                    <RboFormFieldDigital
                      id="taxPeriodYear"
                      size="19.5%"
                      value={documentData.get('taxPeriodYear')}
                      maxLength={4}
                      isWarning={formWarnings.get('taxPeriodYear')}
                      isError={formErrors.get('taxPeriodYear')}
                      isDisabled={!documentData.get('drawerStatus')}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      onChange={onFieldChange}
                    />
                  </RboFormAutoTransitions>
                  }
                  {documentData.get('taxOrCustoms') === 'customs' &&
                  <RboFormFieldText
                    id="taxPeriodDay2"
                    size="50%"
                    value={documentData.get('taxPeriodDay2')}
                    maxLength={8}
                    isWarning={formWarnings.get('taxPeriodDay2')}
                    isError={formErrors.get('taxPeriodDay2')}
                    isDisabled={!documentData.get('drawerStatus')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                </RboFormFieldset>
                }
                {!isEditable && documentData.get('taxOrCustoms') === 'tax' &&
                <RboFormFieldValue
                  id="TaxPeriod"
                  label="107 Налог. период"
                  value={documentValues.get('taxPeriod')}
                  isWarning={formWarnings.get('taxPeriodDay1') || formWarnings.get('taxPeriodMonth') || formWarnings.get('taxPeriodYear')}
                  isError={formErrors.get('taxPeriodDay1') || formErrors.get('taxPeriodMonth') || formErrors.get('taxPeriodYear')}
                />
                }
                {!isEditable && documentData.get('taxOrCustoms') === 'customs' &&
                <RboFormFieldValue
                  id="taxPeriodDay2"
                  label="107 Код тамож. органа"
                  value={documentValues.get('taxPeriodDay2')}
                  isWarning={formWarnings.get('taxPeriodDay2')}
                  isError={formErrors.get('taxPeriodDay2')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="34%">
                {isEditable &&
                <RboFormFieldText
                  id="taxDocNumber"
                  label="108 Пок. номера"
                  value={documentData.get('taxDocNumber')}
                  maxLength={15}
                  isDisabled={!documentData.get('drawerStatus')}
                  isWarning={formWarnings.get('taxDocNumber')}
                  isError={formErrors.get('taxDocNumber')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="taxDocNumber"
                  label="108 Пок. номера"
                  value={documentValues.get('taxDocNumber')}
                  isWarning={formWarnings.get('taxDocNumber')}
                  isError={formErrors.get('taxDocNumber')}
                />
                }
              </RboFormCell>
              <RboFormCell size="40%">
                {isEditable &&
                <RboFormFieldset
                  label="109 Пок. даты докум."
                >
                  <RboFormAutoTransitions>
                    <RboFormFieldDigital
                      id="docDateDay"
                      size="30%"
                      value={documentData.get('docDateDay')}
                      maxLength={2}
                      isWarning={formWarnings.get('docDateDay')}
                      isError={formErrors.get('docDateDay')}
                      isDisabled={!documentData.get('drawerStatus')}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      onChange={onFieldChange}
                    />
                    <RboFormFieldDigital
                      id="docDateMonth"
                      size="30%"
                      value={documentData.get('docDateMonth')}
                      maxLength={2}
                      isWarning={formWarnings.get('docDateMonth')}
                      isError={formErrors.get('docDateMonth')}
                      isDisabled={!documentData.get('drawerStatus')}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      onChange={onFieldChange}
                    />
                    <RboFormFieldDigital
                      id="docDateYear"
                      size="40%"
                      value={documentData.get('docDateYear')}
                      maxLength={4}
                      isWarning={formWarnings.get('docDateYear')}
                      isError={formErrors.get('docDateYear')}
                      isDisabled={!documentData.get('drawerStatus')}
                      onFocus={onFieldFocus}
                      onBlur={onFieldBlur}
                      onChange={onFieldChange}
                    />
                  </RboFormAutoTransitions>
                </RboFormFieldset>
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="taxDocDate"
                  label="109 Пок. даты докум."
                  value={documentValues.get('taxDocDate')}
                  isEWarning={formWarnings.get('docDateDay') || formWarnings.get('docDateMonth') || formWarnings.get('docDateYear')}
                  isError={formErrors.get('docDateDay') || formErrors.get('docDateMonth') || formErrors.get('docDateYear')}
                />
                }
              </RboFormCell>
              <RboFormCell size="26%">
                {isEditable &&
                <RboFormFieldSearch
                  id="chargeType"
                  label="110 Пок. типа"
                  value={documentData.get('chargeType')}
                  maxLength={2}
                  options={documentDictionaries.getIn(['chargeType', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupStyle={{ left: 'auto', right: 0, width: 200 }}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isDisabled={!documentData.get('drawerStatus')}
                  isWarning={formWarnings.get('chargeType')}
                  isError={formErrors.get('chargeType')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="chargeType"
                  label="110 Пок. типа"
                  value={documentValues.get('chargeType')}
                  isWarning={formWarnings.get('chargeType')}
                  isError={formErrors.get('chargeType')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

RPaymentSectionBudget.propTypes = {
  isEditable: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default RPaymentSectionBudget;
