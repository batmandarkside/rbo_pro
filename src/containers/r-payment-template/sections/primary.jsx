import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldDate,
  RboFormFieldDecimalNumber,
  RboFormFieldDropdown,
  RboFormFieldSearch,
  RboFormFieldText,
  RboFormFieldValue,
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

const RPaymentSectionPrimary = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formFieldOptionRenderer,
    onFieldFocus,
    onFieldBlur,
    onFieldSearch,
    onFieldChange
  } = props;

  return (
    <RboFormSection id="primary">
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                <RboFormFieldText
                  id="templateName"
                  label="Наименование шаблона"
                  value={documentData.get('templateName')}
                  maxLength={255}
                  isWarning={formWarnings.get('templateName')}
                  isError={formErrors.get('templateName')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
      <RboFormBlockset>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="25%">
                {isEditable &&
                <RboFormFieldText
                  id="docNumber"
                  label="Номер"
                  value={documentData.get('docNumber')}
                  maxLength={7}
                  isWarning={formWarnings.get('docNumber')}
                  isError={formErrors.get('docNumber')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="docNumber"
                  label="Номер"
                  value={documentValues.get('docNumber')}
                  isWarning={formWarnings.get('docNumber')}
                  isError={formErrors.get('docNumber')}
                />
                }
              </RboFormCell>
              <RboFormCell size="35%">
                {isEditable &&
                <RboFormFieldDate
                  id="docDate"
                  label="Дата"
                  value={documentData.get('docDate')}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('docDate')}
                  isError={formErrors.get('docDate')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="docDate"
                  label="Дата"
                  value={documentValues.get('docDate')}
                  isWarning={formWarnings.get('docDate')}
                  isError={formErrors.get('docDate')}
                />
                }
              </RboFormCell>
              <RboFormCell size="40%">
                {isEditable &&
                <RboFormFieldDropdown
                  id="paymentKind"
                  label="Вид платежа"
                  value={documentData.get('paymentKind')}
                  options={documentDictionaries.getIn(['paymentKind', 'items']).toJS()}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('paymentKind')}
                  isError={formErrors.get('paymentKind')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="paymentKind"
                  label="Вид платежа"
                  value={documentValues.get('paymentKind')}
                  isWarning={formWarnings.get('paymentKind')}
                  isError={formErrors.get('paymentKind')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
        <RboFormBlock>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell size="60%">
                {isEditable &&
                <RboFormFieldDecimalNumber
                  id="amount"
                  label="Сумма"
                  value={documentData.get('amount')}
                  maxLength={13}
                  isShowZero
                  isWarning={formWarnings.get('amount')}
                  isError={formErrors.get('amount')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="amount"
                  label="Сумма"
                  value={documentValues.get('amount')}
                  textAlign="right"
                  isWarning={formWarnings.get('amount')}
                  isError={formErrors.get('amount')}
                />
                }
              </RboFormCell>
              <RboFormCell size="40%">
                {isEditable &&
                <RboFormFieldSearch
                  id="drawerStatus"
                  label="101 Пок. статуса"
                  value={documentData.get('drawerStatus')}
                  maxLength={2}
                  options={documentDictionaries.getIn(['drawerStatus', 'items']).toJS()}
                  optionRenderer={formFieldOptionRenderer}
                  popupStyle={{ left: 'auto', right: 0, width: 524 }}
                  popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                  isWarning={formWarnings.get('drawerStatus')}
                  isError={formErrors.get('drawerStatus')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onSearch={onFieldSearch}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="drawerStatus"
                  label="101 Пок. статуса"
                  value={documentValues.get('drawerStatus')}
                  isWarning={formWarnings.get('drawerStatus')}
                  isError={formErrors.get('drawerStatus')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

RPaymentSectionPrimary.propTypes = {
  isEditable: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  documentDictionaries: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  formFieldOptionRenderer: PropTypes.func.isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldSearch: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default RPaymentSectionPrimary;
