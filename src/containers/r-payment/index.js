import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import mapStateToProps from './selectors';
import * as Actions from './actions';
import saga from './sagas';
import reducer from './reducer';
import RPaymentContainer from './container';

const withConnect = connect(mapStateToProps, Actions);
const withReducer = injectReducer({ key: 'RPaymentContainerReducer', reducer });
const withSaga = injectSaga({ key: 'rPaymentContainerSagas', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RPaymentContainer);
