import React, { Component } from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldAccount,
  RboFormFieldButton,
  RboFormFieldDigital,
  RboFormFieldSearch,
  RboFormFieldText,
  RboFormFieldTextarea,
  RboFormFieldValue
} from 'components/ui-components/rbo-form-compact';
import {
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';

class RPaymentSectionReceiver extends Component {

  static propTypes = {
    isEditable: PropTypes.bool,
    documentValues: PropTypes.instanceOf(Map).isRequired,
    documentData: PropTypes.instanceOf(Map).isRequired,
    documentDictionaries: PropTypes.instanceOf(Map).isRequired,
    formWarnings: PropTypes.instanceOf(Map).isRequired,
    formErrors: PropTypes.instanceOf(Map).isRequired,
    formFieldOptionRenderer: PropTypes.func.isRequired,
    onFieldFocus: PropTypes.func.isRequired,
    onFieldBlur: PropTypes.func.isRequired,
    onFieldChange: PropTypes.func.isRequired,
    onFieldSearch: PropTypes.func.isRequired,
    saveReceiverAction: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isSearchActive: false
    };
  }

  setReceiverSearchFocus = () => {
    const { isSearchActive } = this.state;
    if (isSearchActive) document.querySelector('#receiver').focus();
  };

  setReceiverINNFocus = () => {
    const { isSearchActive } = this.state;
    if (!isSearchActive) document.querySelector('#receiverINN').focus();
  };

  handleReceiverChange = ({ fieldId, fieldValue }) => {
    this.props.onFieldChange({ fieldId, fieldValue });
    if (fieldValue) {
      this.setState({ isSearchActive: false });
      this.props.onFieldBlur({ fieldId });
      setTimeout(this.setReceiverINNFocus, 0);
    }
  };

  handleReceiverBlur = ({ fieldId }) => {
    this.setState({ isSearchActive: false });
    this.props.onFieldBlur({ fieldId });
  };

  toggleSearchActive = () => {
    const { isSearchActive } = this.state;
    this.setState({ isSearchActive: !isSearchActive });
    setTimeout(this.setReceiverSearchFocus, 0);
  };

  render() {
    const {
      isEditable,
      documentValues,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formFieldOptionRenderer,
      onFieldFocus,
      onFieldBlur,
      onFieldChange,
      onFieldSearch,
      saveReceiverAction
    } = this.props;
    const { isSearchActive } = this.state;

    return (
      <RboFormSection
        id="receiver"
        title="Получатель"
      >
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormRowset>
              {isSearchActive &&
              <RboFormRow>
                <RboFormCell>
                  <RboFormFieldSearch
                    id="receiver"
                    label="Номер счета, ИНН или наименование получателя"
                    searchValue={documentDictionaries.getIn(['receiver', 'searchValue'])}
                    options={documentDictionaries.getIn(['receiver', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isSearching={documentDictionaries.getIn(['receiver', 'isSearching'])}
                    onFocus={onFieldFocus}
                    onBlur={this.handleReceiverBlur}
                    onSearch={onFieldSearch}
                    onChange={this.handleReceiverChange}
                  />
                </RboFormCell>
              </RboFormRow>
              }
              {!isSearchActive &&
              <RboFormRow>
                {isEditable &&
                <RboFormCell size="16%">
                  <RboFormFieldButton
                    id="toggleSearch"
                    pseudoLabel
                    icon="dictionary"
                    iconTitle="Поиск в справочнике контрагентов"
                    isDisabled={isSearchActive}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onClick={this.toggleSearchActive}
                  />
                </RboFormCell>
                }
                <RboFormCell size={isEditable ? '42%' : '50%'}>
                  {isEditable &&
                  <RboFormFieldDigital
                    id="receiverINN"
                    label="ИНН/КИО"
                    value={documentData.get('receiverINN')}
                    maxLength={12}
                    isWarning={formWarnings.get('receiverINN')}
                    isError={formErrors.get('receiverINN')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverINN"
                    label="ИНН/КИО"
                    value={documentValues.get('receiverINN')}
                    isWarning={formWarnings.get('receiverINN')}
                    isError={formErrors.get('receiverINN')}
                  />
                  }
                </RboFormCell>
                <RboFormCell size={isEditable ? '42%' : '50%'}>
                  {isEditable &&
                  <RboFormFieldText
                    id="receiverKPP"
                    label="КПП"
                    value={documentData.get('receiverKPP')}
                    maxLength={9}
                    isWarning={formWarnings.get('receiverKPP')}
                    isError={formErrors.get('receiverKPP')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverKPP"
                    label="КПП"
                    value={documentValues.get('receiverKPP')}
                    isWarning={formWarnings.get('receiverKPP')}
                    isError={formErrors.get('receiverKPP')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
              }
            </RboFormRowset>
          </RboFormBlock>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                {isEditable &&
                <RboFormCell>
                  <RboFormFieldButton
                    id="saveReceiver"
                    pseudoLabel
                    align="right"
                    icon="dictionary-add"
                    title="Добавить в справочник"
                    isDisabled={!documentData.get('receiverName') || !documentData.get('receiverBankBIC')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onClick={saveReceiverAction}
                  />
                </RboFormCell>
                }
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
        </RboFormBlockset>
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell>
                  {isEditable &&
                  <RboFormFieldTextarea
                    id="receiverName"
                    label="Наименование получателя"
                    value={documentData.get('receiverName')}
                    maxLength={160}
                    isWarning={formWarnings.get('receiverName')}
                    isError={formErrors.get('receiverName')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverName"
                    label="Наименование получателя"
                    value={documentValues.get('receiverName')}
                    isWarning={formWarnings.get('receiverName')}
                    isError={formErrors.get('receiverName')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell>
                  {isEditable &&
                  <RboFormFieldAccount
                    id="receiverAccount"
                    label="Счет №"
                    value={documentData.get('receiverAccount')}
                    isWarning={formWarnings.get('receiverAccount')}
                    isError={formErrors.get('receiverAccount')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverAccount"
                    label="Счет №"
                    value={documentValues.get('receiverAccount')}
                    isWarning={formWarnings.get('receiverAccount')}
                    isError={formErrors.get('receiverAccount')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell size="1/3">
                  {isEditable &&
                  <RboFormFieldSearch
                    id="paymentPriority"
                    label="Очер. платежа"
                    value={documentData.get('paymentPriority')}
                    inputType="Text"
                    maxLength={1}
                    options={documentDictionaries.getIn(['paymentPriority', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    popupStyle={{ width: 524 }}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isWarning={formWarnings.get('paymentPriority')}
                    isError={formErrors.get('paymentPriority')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="paymentPriority"
                    label="Очер. платежа"
                    value={documentValues.get('paymentPriority')}
                    isWarning={formWarnings.get('paymentPriority')}
                    isError={formErrors.get('paymentPriority')}
                  />
                  }
                </RboFormCell>
                <RboFormCell size="2/3">
                  {isEditable &&
                  <RboFormFieldText
                    id="uip"
                    label="Код/УИП"
                    value={documentData.get('uip')}
                    maxLength={25}
                    isWarning={formWarnings.get('uip')}
                    isError={formErrors.get('uip')}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="uip"
                    label="Код/УИП"
                    value={documentValues.get('uip')}
                    isWarning={formWarnings.get('uip')}
                    isError={formErrors.get('uip')}
                  />
                  }
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
        </RboFormBlockset>
        <RboFormBlockset>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell>
                  <RboFormFieldValue
                    id="receiverBankName"
                    label="Банк получателя"
                    value={documentValues.get('receiverBankName')}
                    isWarning={formWarnings.get('receiverBankName')}
                    isError={formErrors.get('receiverBankName')}
                  />
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
          <RboFormBlock>
            <RboFormRowset>
              <RboFormRow>
                <RboFormCell size="1/3">
                  {isEditable &&
                  <RboFormFieldSearch
                    id="receiverBankBIC"
                    label="БИК"
                    value={documentData.get('receiverBankBIC')}
                    searchValue={documentDictionaries.getIn(['receiverBankBIC', 'searchValue'])}
                    options={documentDictionaries.getIn(['receiverBankBIC', 'items']).toJS()}
                    optionRenderer={formFieldOptionRenderer}
                    popupStyle={{ width: 362 }}
                    popupContainerElementSelector={DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR}
                    isWarning={formWarnings.get('receiverBankBIC')}
                    isError={formErrors.get('receiverBankBIC')}
                    isSearching={documentDictionaries.getIn(['receiverBankBIC', 'isSearching'])}
                    onFocus={onFieldFocus}
                    onBlur={onFieldBlur}
                    onSearch={onFieldSearch}
                    onChange={onFieldChange}
                  />
                  }
                  {!isEditable &&
                  <RboFormFieldValue
                    id="receiverBankBIC"
                    label="БИК"
                    value={documentValues.get('receiverBankBIC')}
                    isWarning={formWarnings.get('receiverBankBIC')}
                    isError={formErrors.get('receiverBankBIC')}
                  />
                  }
                </RboFormCell>
                <RboFormCell size="2/3">
                  <RboFormFieldValue
                    id="receiverBankCorrAccount"
                    label="Кор. счет"
                    value={documentValues.get('receiverBankCorrAccount')}
                    isWarning={formWarnings.get('receiverBankCorrAccount')}
                    isError={formErrors.get('receiverBankCorrAccount')}
                  />
                </RboFormCell>
              </RboFormRow>
            </RboFormRowset>
          </RboFormBlock>
        </RboFormBlockset>
      </RboFormSection>
    );
  }
}

export default RPaymentSectionReceiver;
