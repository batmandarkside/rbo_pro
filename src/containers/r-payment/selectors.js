import { createSelector } from 'reselect';
import { Map, List } from 'immutable';
import moment from 'moment-timezone';
import numeral from 'numeral';
import { DOC_STATUSES, SIGN_VALIDATION_STATUSES } from 'constants';
import {
  regExpEscape,
  getDocStatusClassNameMode,
  getFormattedAccNum,
  getDocFormErrors,
  getMappingDocSectionsAndErrors,
  getSignType
} from 'utils';
import { LOCAL_REDUCER } from './constants';
import {
  documentFieldsMap,
  documentSectionsFieldsMap,
  documentErrorsMap,
  getBankInfo,
} from './utils';

const isDataErrorSelector = state => state.getIn([LOCAL_REDUCER, 'isDataError']);
const isDataLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isDataLoading']);
const isEditableSelector = state => state.getIn([LOCAL_REDUCER, 'isEditable']);
const documentTabsSelector = state => state.getIn([LOCAL_REDUCER, 'documentTabs']);
const documentHistorySelector = state => state.getIn([LOCAL_REDUCER, 'documentHistory']);
const documentSignaturesSelector = state => state.getIn([LOCAL_REDUCER, 'documentSignatures']);
const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
const documentErrorsSelector = state => state.getIn([LOCAL_REDUCER, 'documentErrors']);
const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
const formSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'formSections']);
const activeFormFieldSelector = state => state.getIn([LOCAL_REDUCER, 'activeFormField']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);

const getDictionaryId = (value, paramName, dictionary, idName) => {
  const selectedItem = dictionary.get('items').find(item => item.get(paramName) === value);
  return selectedItem ? selectedItem.get(idName) : null;
};

const documentHistoryCreatedSelector = createSelector(
  documentHistorySelector,
  documentHistory => documentHistory.set('items', documentHistory.get('items')
    .reverse()
    .map(item => Map({
      date: item.get('date') ? moment(item.get('date')).format('DD.MM.YYYY HH:mm:ss') : '',
      user: item.get('user'),
      endState: item.get('endState'),
      statusClassName: getDocStatusClassNameMode(item.get('endState'))
    }))
  )
);

const documentSignaturesCreatedSelector = createSelector(
  documentSignaturesSelector,
  documentSignatures => documentSignatures.set('items', documentSignatures.get('items').map(item => Map({
    signId: item.get('signId'),
    signDateTime: item.get('signDateTime') ? moment(item.get('signDateTime')).format('DD.MM.YYYY HH:mm:ss') : '',
    userLogin: item.get('userLogin'),
    userName: item.get('userName'),
    userPosition: item.get('userPosition'),
    signType: getSignType(item.get('signType')),
    signValid: item.get('signValid') ? SIGN_VALIDATION_STATUSES.VALID : SIGN_VALIDATION_STATUSES.INVALID,
    signHash: item.get('signHash')
  })))
);

const documentValuesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => {
    const docNumber = documentData.get('docNumber') || '';
    const docDate = documentData.get('docDate') ? moment(documentData.get('docDate')).format('DD.MM.YYYY') : '';
    const title = `Платежное поручение${docNumber ? ` № ${docNumber}` : ''}`;

    const taxPeriod = documentData.get('taxPeriodDay1')
      ? `${documentData.get('taxPeriodDay1')}${documentData.get('taxPeriodMonth') ?
        `.${documentData.get('taxPeriodMonth')}` : ''}${documentData.get('taxPeriodYear') ?
        `.${documentData.get('taxPeriodYear')}` : ''}`
      : '';
    const taxDocDateValue = `${documentData.get('docDateDay', '')}${documentData.get('docDateMonth') 
      ? ` ${documentData.get('docDateMonth')}` : ''}${documentData.get('docDateYear') 
      ? ` ${documentData.get('docDateYear')}` : ''}`;

    return Map({
      title,
      status: documentData.get('status', ''),
      paymentType: documentData.get('paymentType')
        ? documentDictionaries.getIn(['paymentType', 'items'])
          .find(item => item.get('value') === documentData.get('paymentType')).get('title')
        : '',

      docNumber,
      docDate,
      paymentKind: documentData.get('paymentKind', '').toLowerCase() || '—',
      amount: !isNaN(documentData.get('amount'))
        ? numeral(documentData.get('amount')).format('0,0.00')
        : '0.00',
      drawerStatus: documentData.get('drawerStatus', ''),

      payerINN: documentData.get('payerINN', ''),
      payerKPP: documentData.get('payerKPP', ''),
      payerName: documentData.get('payerName', ''),
      payerAccount: getFormattedAccNum(documentData.get('payerAccount', '')),
      payerBankBIC: documentData.get('payerBankBIC', ''),
      payerBankCorrAccount: getFormattedAccNum(documentData.get('payerBankCorrAccount', '')),
      payerBankName: documentData.get('payerBankName')
        ? `${documentData.get('payerBankName')} ${documentData.get('payerBankSettlementType') ? 
          `${documentData.get('payerBankSettlementType')}.` : ''} ${documentData.get('payerBankCity', '')}`
        : '',

      receiverINN: documentData.get('receiverINN', ''),
      receiverKPP: documentData.get('receiverKPP', ''),
      receiverName: documentData.get('receiverName', ''),
      receiverAccount: getFormattedAccNum(documentData.get('receiverAccount', '')),
      paymentPriority: documentData.get('paymentPriority', ''),
      uip: documentData.get('uip', ''),
      receiverBankBIC: documentData.get('receiverBankBIC', ''),
      receiverBankCorrAccount: getFormattedAccNum(documentData.get('receiverBankCorrAccount', '')),
      receiverBankName: documentData.get('receiverBankName')
        ? `${documentData.get('receiverBankName')} ${documentData.get('receiverBankSettlementType') ?
          `${documentData.get('receiverBankSettlementType')}.` : ''} ${documentData.get('receiverBankCity', '')}`
        : '',

      paymentPurpose: documentData.get('paymentPurpose', ''),
      vatCalculationRule: documentData.get('vatCalculationRule', ''),
      vatRate: !isNaN(documentData.get('vatRate'))
        ? numeral(documentData.get('vatRate')).format('0,0.00')
        : '0.00',
      vatSum: !isNaN(documentData.get('vatSum'))
        ? numeral(documentData.get('vatSum')).format('0,0.00')
        : '0.00',
      currencyOperationType: documentData.get('currencyOperationType')
        ? documentDictionaries.getIn(['currencyOperationType', 'items'])
          .find(type => type.get('code') === documentData.get('currencyOperationType'))
        : '',

      cbc: documentData.get('cbc', ''),
      ocato: documentData.get('ocato', ''),
      payReason: documentData.get('payReason', ''),
      taxOrCustoms: documentData.get('taxOrCustoms')
        ? documentDictionaries.getIn(['taxOrCustoms', 'items'])
          .find(item => item.get('apiValue') === documentData.get('taxOrCustoms')).get('title')
        : '',
      taxPeriod,
      taxPeriodDay2: documentData.get('taxPeriodDay2', ''),
      taxDocNumber: documentData.get('taxDocNumber', ''),
      taxDocDate: taxDocDateValue,
      chargeType: documentData.get('chargeType', ''),

      docNote: documentData.get('docNote', ''),

      bankInfo: getBankInfo(documentData)
    });
  }
);

const documentDataCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => {
    const paymentType = documentData.get('paymentType');
    const payerAccount = getDictionaryId(
      documentData.get('payerAccount'), 'accNum',
      documentDictionaries.get((paymentType !== 'internal') ? 'payerAccount' : 'internalPayerAccount'), 'id'
    );
    const receiverAccount = (paymentType !== 'internal') ? documentData.get('receiverAccount') :
      getDictionaryId(
        documentData.get('receiverAccount'), 'accNum',
        documentDictionaries.get('internalReceiverAccount'), 'id'
      );

    return Map({
      id: documentData.get('recordID'),
      status: documentData.get('status'),
      paymentType,

      docNumber: documentData.get('docNumber'),
      docDate: documentData.get('docDate'),
      paymentKind: getDictionaryId(
        documentData.get('paymentKind') ? documentData.get('paymentKind').toLowerCase() : '', 'description',
        documentDictionaries.get('paymentKind'), 'code'
      ),
      amount: documentData.get('amount') || 0,
      drawerStatus: getDictionaryId(
        documentData.get('drawerStatus'), 'status',
        documentDictionaries.get('drawerStatus'), 'id'
      ),

      payerINN: documentData.get('payerINN'),
      payerKPP: documentData.get('payerKPP'),
      payerName: documentData.get('payerName'),
      payerAccount,
      payerBankBIC: getDictionaryId(
        documentData.get('payerBankBIC'), 'bic',
        documentDictionaries.get('payerBankBIC'), 'id'
      ),
      payerBankCorrAccount: getFormattedAccNum(documentData.get('payerBankCorrAccount')),
      payerBankName: documentData.get('payerBankName') ?
        `${documentData.get('payerBankName')} ${documentData.get('payerBankSettlementType') ?
          `${documentData.get('payerBankSettlementType')}.` : ''} ${documentData.get('payerBankCity', '')}`
        : '',

      receiverAccount,
      receiverINN: documentData.get('receiverINN'),
      receiverKPP: documentData.get('receiverKPP'),
      receiverName: documentData.get('receiverName'),
      paymentPriority: getDictionaryId(
        documentData.get('paymentPriority'), 'code',
        documentDictionaries.get('paymentPriority'), 'id'
      ),
      uip: documentData.get('uip'),
      receiverBankBIC: getDictionaryId(
        documentData.get('receiverBankBIC'), 'bic',
        documentDictionaries.get('receiverBankBIC'), 'id'
      ),
      receiverBankCorrAccount: getFormattedAccNum(documentData.get('receiverBankCorrAccount')),
      receiverBankName: documentData.get('receiverBankName') ?
        `${documentData.get('receiverBankName')} ${documentData.get('receiverBankSettlementType') ?
          `${documentData.get('receiverBankSettlementType')}.` : ''} ${documentData.get('receiverBankCity', '')}`
        : '',

      paymentPurpose: documentData.get('paymentPurpose'),
      vatCalculationRule: documentData.get('vatCalculationRule'),
      vatRate: documentData.get('vatRate') || 0,
      vatSum: documentData.get('vatSum') || 0,
      currencyOperationType: documentData.get('currencyOperationType'),

      cbc: documentData.get('cbc'),
      ocato: documentData.get('ocato'),
      payReason: getDictionaryId(
        documentData.get('payReason'), 'param',
        documentDictionaries.get('payReason'), 'id'
      ),
      taxOrCustoms: getDictionaryId(
        documentData.get('taxOrCustoms'), 'apiValue',
        documentDictionaries.get('taxOrCustoms'), 'value'
      ),
      taxPeriodDay1: documentData.get('taxPeriodDay1'),
      taxPeriodMonth: documentData.get('taxPeriodMonth'),
      taxPeriodYear: documentData.get('taxPeriodYear'),
      taxPeriodDay2: documentData.get('taxPeriodDay2'),
      taxDocNumber: documentData.get('taxDocNumber'),
      docDateDay: documentData.get('docDateDay'),
      docDateMonth: documentData.get('docDateMonth'),
      docDateYear: documentData.get('docDateYear'),
      chargeType: getDictionaryId(
        documentData.get('chargeType'), 'param',
        documentDictionaries.get('chargeType'), 'id'
      ),

      docNote: documentData.get('docNote', '')
    });
  }
);

const documentRecallsCreatedSelector = createSelector(
  documentDataSelector,
  documentData => documentData.get('linkedDocs', List([]))
    .filter(doc => doc.get('docType') === 'Recall')
    .sort((a, b) => {
      // сортируем по дате, если дата одинаковая (хранится только число, месяц, год) то сортируем по номеру
      const dateA = moment(a.get('docDate'));
      const dateB = moment(b.get('docDate'));
      const different = dateA.diff(dateB);

      if (!different) {
        const numberA = a.get('docNumber');
        const numberB = b.get('docNumber');

        return parseInt(numberA, 10) < parseInt(numberB, 10) ? 1 : -1;
      }

      return (different < 0) ? 1 : -1;
    })
    .map(doc => Map({
      id: doc.get('recordID'),
      title: `Отзыв № ${doc.get('docNumber')} от ${moment(doc.get('docDate')).format('DD.MM.YYYY')}`
    }))
);

const documentErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  activeFormFieldSelector,
  (documentErrors, activeFormField) => documentErrors
    .map(error => error.merge({
      fieldId: documentErrorsMap[error.get('id')] && documentErrorsMap[error.get('id')][0],
      fieldsIds: documentErrorsMap[error.get('id')],
      isActive: documentErrorsMap[error.get('id')] &&
        !!documentErrorsMap[error.get('id')].find(i => i === activeFormField)
    }))
    .filter(error => !!error.get('fieldId'))
    .sort((a, b) => documentFieldsMap.indexOf(a.get('fieldId')) - documentFieldsMap.indexOf(b.get('fieldId')))
);

const documentStructureCreatedSelector = createSelector(
  documentTabsSelector,
  formSectionsSelector,
  documentErrorsCreatedSelector,
  activeFormFieldSelector,
  (documentTabs, formSections, documentErrors, activeFormField) => documentTabs.map((tab) => {
    switch (tab.get('id')) {
      case 'main':
        return tab.set('sections',
          getMappingDocSectionsAndErrors(formSections, documentSectionsFieldsMap, documentErrors, activeFormField));
      default:
        return tab;
    }
  })
);

const documentDictionariesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  (documentData, documentDictionaries) => documentDictionaries.map((dictionary, key) => {
    let searchRegExp;
    const items = dictionary.get('items');
    const searchValue = regExpEscape(dictionary.get('searchValue'));
    switch (key) {
      case 'paymentKind':
        return dictionary.merge({
          items: items.map(item => Map({
            value: item.get('code'),
            title: item.get('description')
          }))
        });
      case 'drawerStatus':
        searchRegExp = new RegExp(`^${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('status').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('status'),
              comment: item.get('comment')
            }))
        });
      case 'payerAccount':
        searchRegExp = new RegExp(`^${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('accNum').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('accNum'),
              account: getFormattedAccNum(item.get('accNum')),
              availableBalance: !isNaN(item.get('availableBalance')) ?
                numeral(item.get('availableBalance')).format('0,0.00') :
                null,
              orgName: item.get('orgName')
            }))
        });
      case 'customerKPP':
        searchRegExp = new RegExp(`^${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('value').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('value'),
              title: item.get('value')
            }))
        });
      case 'receiver':
        return dictionary.merge({
          items: items.map(item => Map({
            value: item.get('id'),
            title: item.get('account'),
            name: item.get('receiverName'),
            account: getFormattedAccNum(item.get('account')),
            inn: item.get('receiverINN')
          }))
        });
      case 'internalPayerAccount':
        searchRegExp = new RegExp(`^${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('accNum') !== documentData.get('receiverAccount'))
            .filter(item => item.get('accNum').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('accNum'),
              account: getFormattedAccNum(item.get('accNum')),
              availableBalance: !isNaN(item.get('availableBalance')) ?
                numeral(item.get('availableBalance')).format('0,0.00') :
                null,
              orgName: item.get('orgName')
            }))
        });
      case 'internalReceiverAccount':
        searchRegExp = new RegExp(`^${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('accNum') !== documentData.get('payerAccount'))
            .filter(item => item.get('accNum').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('accNum'),
              account: getFormattedAccNum(item.get('accNum')),
              availableBalance: !isNaN(item.get('availableBalance')) ?
                numeral(item.get('availableBalance')).format('0,0.00') :
                null,
              orgName: item.get('orgName')
            }))
        });
      case 'receiverBankBIC':
        return dictionary.merge({
          items: items.map(item => Map({
            value: item.get('id'),
            title: item.get('bic'),
            bankName: item.get('bankName'),
            corrAccount: getFormattedAccNum(item.get('corrAccount'))
          }))
        });
      case 'paymentPriority':
        searchRegExp = new RegExp(`^${searchValue || ''}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('code').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('code'),
              description: item.get('description')
            }))
        });
      case 'cbc':
        return dictionary.merge({
          items: items.map(item => Map({
            value: item.get('id'),
            title: item.get('code'),
            description: item.get('description')
          }))
        });
      case 'vatCalculationRule':
        return dictionary.merge({
          items: items.map(item => Map({
            value: item.get('rule'),
            title: item.get('rule')
          }))
        });
      case 'currencyOperationType':
        return dictionary.merge({
          items: items.map(item => Map({
            value: item.get('code'),
            title: item.get('code'),
            description: item.get('description')
          }))
        });
      case 'payReason':
      case 'taxPeriodDay1':
      case 'chargeType':
        searchRegExp = new RegExp(`^${searchValue}`, 'ig');
        return dictionary.merge({
          items: items.filter(item => item.get('param').search(searchRegExp) >= 0)
            .map(item => Map({
              value: item.get('id'),
              title: item.get('param'),
              comment: item.get('comment')
            }))
        });
      case 'templates':
        return dictionary.merge({
          items: items.map(item => Map({
            value: item.get('recordID'),
            recordID: item.get('recordID'),
            title: item.get('name'),
            label: item.get('name'),
            correspondentName: item.get('correspondentName')
          }))
        });
      default:
        return dictionary;
    }
  })
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['1'])
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['2', '3'])
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  documentDataSelector,
  (operations, documentData) => {
    const status = documentData.get('status');
    const allowedSmActions = documentData.get('allowedSmActions', Map());
    return operations.filter((operation) => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'back':
          return true;
        case 'save':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save');
        case 'signAndSend':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save') || allowedSmActions.get('sign');
        case 'sign':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('sign');
        case 'unarchive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('fromArchive');
        case 'repeat':
          return status !== DOC_STATUSES.NEW;
        case 'print':
          return status !== DOC_STATUSES.NEW;
        case 'remove':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('delete');
        case 'recall':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('recall');
        case 'send':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('send');
        case 'history':
          return status !== DOC_STATUSES.NEW;
        case 'archive':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('archive');
        case 'visa':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('sign');
        case 'checkSign':
          return !![
            DOC_STATUSES.DELIVERED,
            DOC_STATUSES.PROCESSED,
            DOC_STATUSES.EXECUTED,
            DOC_STATUSES.DECLINED,
            DOC_STATUSES.RECALLED,
            DOC_STATUSES.INVALID_PROPS,
            DOC_STATUSES.SIGNED,
            DOC_STATUSES.ACCEPTED,
            DOC_STATUSES.PARTLY_SIGNED,
            DOC_STATUSES.INVALID_SIGN
          ].find(v => v === status);
        case 'saveAsTemplate':
          return status !== DOC_STATUSES.NEW;
        default:
          return false;
      }
    });
  }
);

const mapStateToProps = state => ({
  isDataError: isDataErrorSelector(state),
  isDataLoading: isDataLoadingSelector(state),
  isEditable: isEditableSelector(state),
  documentTabs: documentTabsSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentHistory: documentHistoryCreatedSelector(state),
  documentSignatures: documentSignaturesCreatedSelector(state),
  documentValues: documentValuesCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentRecalls: documentRecallsCreatedSelector(state),
  documentDictionaries: documentDictionariesCreatedSelector(state),
  formSections: formSectionsSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  operations: operationsCreatedSelector(state)
});

export default mapStateToProps;
