/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { List, Map } from 'immutable';
import Spreadsheet, {
  Toolbar, Filters, Operations, Table, Selection, Export, Pagination
} from 'components/ui-components/spreadsheet';
import RPaymentsContainer from '../container';

describe('PaymentsContainer', () => {
  it('render', () => {
    const context = { router: { isActive: a => a === '/rpay' } };

    const props = {
      isExportListLoading: false,
      isListLoading: false,
      isListUpdateRequired: false,
      isReloadRequired: false,
      isTemplatesLoading: false,
      isTemplatesLoadRequired: false,

      columns: List(),
      filtersConditions: List([
        {
          id: 'status',
          title: 'Статус',
          type: 'multipleSelect',
          values: [
            'Статус 1',
            'Статус 2'
          ]
        }
      ]),
      filters: List([
        {
          title: 'Test Filter',
          isPreset: true,
          savedConditions: [
            {
              id: 'status',
              params: ['Статус 1']
            }
          ]
        }
      ]),
      list: List(),
      location: {},
      operations: List(),
      pages: Map(),
      paginationOptions: List(),
      panelOperations: List(),
      sections: List([Map({ id: 'id', selected: true })]),
      selectedItems: List(),
      selectedItemsSumAmount: '0 руб.',
      templates: List(),

      getSectionIndexAction: jest.fn(),
      getQueryParamsAction: jest.fn(),
      changeSectionAction: jest.fn(),
      changePageAction: jest.fn(),
      reloadPagesAction: jest.fn(),
      changeFiltersAction: jest.fn(),
      getDictionaryForFiltersAction: jest.fn(),
      getListAction: jest.fn(),
      exportListAction: jest.fn(),
      unmountAction: jest.fn(),
      resizeColumnsAction: jest.fn(),
      changeColumnsAction: jest.fn(),
      changeGroupingAction: jest.fn(),
      changeSortingAction: jest.fn(),
      changePaginationAction: jest.fn(),
      changeSelectAction: jest.fn(),
      newPaymentAction: jest.fn(),
      routeToItemAction: jest.fn(),
      routeToItemRecallAction: jest.fn(),
      operationAction: jest.fn(),
      operationRemoveConfirmAction: jest.fn(),
      recallOperationAction: jest.fn(),
      getTemplatesAction: jest.fn(),
      templateEditAction: jest.fn(),
      importPaymentsAction: jest.fn(),
      saveAsTemplateAction: jest.fn()
    };

    const wrapper = shallow(
      <RPaymentsContainer
        {...props}
      />,
      { context }
    );
    expect(wrapper.find('.b-r-payments-container')).to.have.length(1);
    expect(wrapper.find(Spreadsheet)).to.have.length(1);
    expect(wrapper.find(Toolbar)).to.have.length(1);
    expect(wrapper.find(Filters)).to.have.length(1);
    expect(wrapper.find(Operations)).to.have.length(0);
    expect(wrapper.find(Table)).to.have.length(1);
    expect(wrapper.find(Selection)).to.have.length(0);
    expect(wrapper.find(Export)).to.have.length(0);
    expect(wrapper.find(Pagination)).to.have.length(0);
    wrapper.unmount();
  });
});
