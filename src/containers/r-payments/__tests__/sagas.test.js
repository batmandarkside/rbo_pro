import { put, call, select } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import { push } from 'react-router-redux';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction,
} from 'dal/notification/actions';
import {
  exportPaymentsListToExcel as dalExportPaymentsListToExcel,
  getTemplates as dalGetTemplates
} from 'dal/r-payments/sagas';
import { signDocAddAndTrySigningAction } from 'dal/sign/actions';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import { ACTIONS, SETTINGS_ALIAS, LOCAL_REDUCER } from './../constants';
import { initialSettings, initialPages, initialColumns, initialSections, initialFilters } from './../reducer';
import {
  routingLocationSelector,
  profileSettingsSelector,
  localSectionsSelector,
  localSettingsSelector,
  localFiltersSelector,

  getSectionIndex,
  getSettings,
  changeSection,
  changePage,
  reloadPages,
  changeFilters,
  getDictionaryForFilters,
  saveSettings,
  exportList,
  changeSettingsWidth,
  changeSettingsColumns,
  changeSettingsPagination,
  newPayment,
  routeToItem,
  routeToItemRecall,
  operation,
  signDocument,
  operationRepeat,
  recallOperation,
  getTemplates,
  templateEdit,
  successNotification,
  errorNotification
} from './../sagas';

describe('PaymentsContainer Sagas', () => {
  const state = fromJS({
    routing: {
      locationBeforeTransitions: {
        pathname: '/rpay',
        query: {
          page: 1
        },
        currentLocation: '/rpay'
      }
    },
    profile: {
      settings: {
        [SETTINGS_ALIAS]: {
          working: initialSettings
        }
      }
    },
    [LOCAL_REDUCER]: {
      settings: initialSettings,
      pages: initialPages,
      sections: initialSections,
      filters: initialFilters
    }
  });

  describe('getSectionIndex', () => {
    it('success', () => {
      const generator = getSectionIndex();

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: 0 })
      );

      expect(
        generator.next(put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: 0 })).value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_SETTINGS_REQUEST })
      );
    });
  });

  describe('getSettings', () => {
    it('success', () => {
      const generator = getSettings();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_SETTINGS_SUCCESS, payload: initialSettings })
      );
    });
  });

  describe('changeSection', () => {
    it('success', () => {
      const generator = changeSection({ type: ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_SECTION, payload: 'archive' });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        put(push({ pathname: '/r-payments/archive', search: null }))
      );
    });
  });

  describe('changePage', () => {
    it('success', () => {
      const generator = changePage({ type: ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_PAGE, payload: 1 });

      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );
    });
  });

  describe('reloadPages', () => {
    it('success', () => {
      const generator = reloadPages();
      expect(
        generator.next().value
      ).toEqual(
        select(routingLocationSelector)
      );

      expect(
        generator.next(routingLocationSelector(state)).value
      ).toEqual(
        put(push({
          pathname: '/rpay',
          search: '?page=1'
        }))
      );
    });
  });

  describe('changeFilters', () => {
    it('success', () => {
      const generator = changeFilters({
        type: ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_FILTERS,
        payload: {
          filters: fromJS([
            {
              id: '1',
              title: 'Title 1',
              isPreset: true,
              savedConditions: fromJS(['1', '2'])
            },
            {
              id: '2',
              title: 'Title 2',
              isPreset: false,
              savedConditions: fromJS(['3', '4'])
            }
          ]),
          isSaveSettingRequired: true
        }
      });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            filters: [
              {
                id: '2',
                title: 'Title 2',
                isPreset: false,
                savedConditions: ['3', '4']
              }
            ]
          })
        })
      );
    });
  });

  describe('getDictionaryForFilters', () => {
    it('success', () => {
      const generator = getDictionaryForFilters({
        type: ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST,
        payload: { conditionId: 'payerAccount', value: '1234' }
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetAccounts, { payload: { accNum: '1234', currIsoCode: 'RUR' } })
      );

      expect(
        generator.next(['1', '2', '3']).value
      ).toEqual(
        put({
          type: ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
          payload: { conditionId: 'payerAccount', items: ['1', '2', '3'] }
        })
      );
    });

    it('failure', () => {
      const generator = getDictionaryForFilters({
        type: ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST,
        payload: { conditionId: 'payerAccount', value: '1234' }
      });

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetAccounts, { payload: { accNum: '1234', currIsoCode: 'RUR' } })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({
          type: ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
          error: 'Произошла ошибка на сервере'
        })
      );
    });
  });

  describe('saveSettings', () => {
    it('success', () => {
      const generator = saveSettings({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: fromJS(initialSettings) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        call(dalSaveSettings, {
          payload: {
            settings: {
              [SETTINGS_ALIAS]: {
                working: initialSettings
              }
            }
          }
        })
      );

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_SUCCESS, payload: fromJS(initialSettings) })
      );
    });

    it('failure', () => {
      const generator = saveSettings({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: fromJS(initialSettings) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(profileSettingsSelector)
      );

      expect(
        generator.next(profileSettingsSelector(state)).value
      ).toEqual(
        call(dalSaveSettings, {
          payload: {
            settings: {
              [SETTINGS_ALIAS]: {
                working: initialSettings
              }
            }
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('exportList', () => {
    it('success', () => {
      const generator = exportList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalExportPaymentsListToExcel, {
          payload: {
            section: '',
            sort: 'date',
            desc: true,
            dateFrom: null,
            dateTo: null,
            status: null,
            sumMin: null,
            sumMax: null,
            payerName: null,
            account: null,
            receiver: null,
            receiverAccount: null,
            receiverINN: null,
            paymentPurpose: null,
            notes: null,
            withRecall: null,
            importId: null,
            lastStateChangeDateFrom: null,
            lastStateChangeDateTo: null,
            visibleColumns: ['number', 'date', 'sum', 'status', 'bankMessage',
              'receiver', 'receiverAccount', 'paymentPurpose', 'payerAccount', 'payer']
          }
        })
      );

      expect(
        generator.next([1]).value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_SUCCESS, payload: [1] })
      );
    });

    it('failure', () => {
      const generator = exportList();

      expect(
        generator.next().value
      ).toEqual(
        select(localSectionsSelector)
      );

      expect(
        generator.next(localSectionsSelector(state)).value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        select(localFiltersSelector)
      );

      expect(
        generator.next(localFiltersSelector(state)).value
      ).toEqual(
        call(dalExportPaymentsListToExcel, {
          payload: {
            section: '',
            sort: 'date',
            desc: true,
            dateFrom: null,
            dateTo: null,
            status: null,
            sumMin: null,
            sumMax: null,
            payerName: null,
            account: null,
            receiver: null,
            receiverAccount: null,
            receiverINN: null,
            paymentPurpose: null,
            notes: null,
            withRecall: null,
            importId: null,
            lastStateChangeDateFrom: null,
            lastStateChangeDateTo: null,
            visibleColumns: ['number', 'date', 'sum', 'status', 'bankMessage',
              'receiver', 'receiverAccount', 'paymentPurpose', 'payerAccount', 'payer']
          }
        })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('changeSettingsWidth', () => {
    it('success', () => {
      const generator = changeSettingsWidth({ type: ACTIONS.R_PAYMENTS_CONTAINER_RESIZE_COLUMNS, payload: fromJS(initialColumns).map(column => column.set('width', 300)) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            width: {
              number: 300,
              date: 300,
              sum: 300,
              status: 300,
              bankMessage: 300,
              lastChangeStateDate: 300,
              receiver: 300,
              receiverAccount: 300,
              paymentPurpose: 300,
              payerAccount: 300,
              payer: 300,
              inn: 300,
              payerBankBIC: 300,
              note: 300
            }
          })
        })
      );
    });
  });

  describe('changeSettingsColumns', () => {
    it('success', () => {
      const generator = changeSettingsColumns({ type: ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_COLUMN, payload: fromJS(initialColumns).update(0, column => column.set('sorting', 1)) });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            visible: [],
            sorting: { id: 'number', direction: 1 }
          })
        })
      );
    });
  });

  describe('changeSettingsPagination', () => {
    it('success', () => {
      const generator = changeSettingsPagination({ type: ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_PAGINATION, payload: 60 });

      expect(
        generator.next().value
      ).toEqual(
        select(localSettingsSelector)
      );

      expect(
        generator.next(localSettingsSelector(state)).value
      ).toEqual(
        put({
          type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST,
          payload: fromJS(initialSettings).merge({
            pagination: 60
          })
        })
      );
    });
  });

  describe('newPayment', () => {
    it('success', () => {
      const generator = newPayment({
        type: ACTIONS.R_PAYMENTS_CONTAINER_NEW_PAYMENT,
        payload: { type: 'template', templateId: 'templateId' }
      });
      expect(
        generator.next().value
      ).toEqual(
        put(push({
          pathname: '/r-payments/new',
          search: '?type=template&templateId=templateId'
        }))
      );
    });
  });

  describe('routeToItem', () => {
    it('success', () => {
      const generator = routeToItem({ type: ACTIONS.R_PAYMENTS_CONTAINER_ROUTE_TO_ITEM, payload: 'itemId' });
      expect(
        generator.next().value
      ).toEqual(
        put(push('/r-payments/itemId'))
      );
    });
  });

  describe('routeToItemRecall', () => {
    it('success', () => {
      const generator = routeToItemRecall({
        type: ACTIONS.R_PAYMENTS_CONTAINER_ROUTE_TO_ITEM_RECALL,
        payload: { itemId: 'itemId', recallId: 'recallId' }
      });
      expect(
        generator.next().value
      ).toEqual(
        put(push({
          pathname: '/r-payments/itemId/recalls/recallId'
        }))
      );
    });
  });

  describe('operation', () => {
    it('success', () => {
      const generator = operation({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION,
        payload: { operationId: 'print', items: ['0', '1', '2'] }
      });

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_REQUEST, payload: ['0', '1', '2'] })
      );
    });
  });

  describe('operationSign', () => {
    it('success', () => {
      const generator = signDocument({
        payload: {
          docIds: ['0']
        }
      });

      const payloadWithChannel = {
        docIds: ['0'],
        // уникальный канал подписи для контейнера
        channel: {
          success: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_SUCCESS,
          cancel: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_CANCEL,
          fail: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_FAIL,
        }
      };

      expect(
        generator.next().value
      ).toEqual(
        put(signDocAddAndTrySigningAction(payloadWithChannel))
      );
    });
  });

  describe('operationRepeat', () => {
    it('success', () => {
      const generator = operationRepeat({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REPEAT,
        payload: ['0']
      });

      expect(
        generator.next(['0']).value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_NEW_PAYMENT, payload: { type: 'repeat', itemId: '0' } })
      );
    });
  });

  describe('recallOperation', () => {
    it('success', () => {
      const generator = recallOperation({
        type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION,
        payload: { operationId: 'print', recallId: 'recallId' }
      });

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_REQUEST, payload: 'recallId' })
      );
    });
  });

  describe('getTemplates', () => {
    it('success', () => {
      const generator = getTemplates();

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetTemplates, { payload: {} })
      );

      expect(
        generator.next([1]).value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_SUCCESS, payload: [1] })
      );
    });

    it('failure', () => {
      const generator = getTemplates();

      expect(
        generator.next().value
      ).toEqual(
        call(dalGetTemplates, { payload: {} })
      );

      expect(
        generator.throw('Test error').value
      ).toEqual(
        put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_FAIL, error: 'Произошла ошибка на сервере' })
      );
    });
  });

  describe('templateEdit', () => {
    it('success', () => {
      const generator = templateEdit({
        type: ACTIONS.R_PAYMENTS_CONTAINER_TEMPLATE_EDIT,
        payload: 'templateId'
      });
      expect(
        generator.next().value
      ).toEqual(
        put(push('/r-payments/templates/templateId'))
      );
    });
  });

  describe('successNotification', () => {
    it('success', () => {
      const generator = successNotification({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
        payload: 'Документы успешно перемещены в архив'
      });

      expect(
        generator.next().value
      ).toEqual(
        put(dalAddOrdinaryNotificationAction({
          type: 'success',
          title: 'Документы успешно перемещены в архив'
        }))
      );
    });
  });

  describe('errorNotification', () => {
    it('success', () => {
      const generator = errorNotification({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_FAIL,
        error: 'Произошла ошибка на сервере'
      });

      expect(
        generator.next().value
      ).toEqual(
        put(dalAddOrdinaryNotificationAction({
          type: 'error',
          title: 'Произошла ошибка на сервере'
        }))
      );
    });
  });
});
