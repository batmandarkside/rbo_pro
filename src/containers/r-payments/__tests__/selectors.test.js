import { fromJS } from 'immutable';
import { LOCAL_REDUCER } from './../constants';
import {
  columnsCreatedSelector,
  filtersConditionsCreatedSelector,
  filtersCreatedSelector,
  listCreatedSelector,
  paginationOptionsCreatedSelector,
  panelOperationsCreatedSelector,
  selectedItemsSumCreatedSelector,
  templatesCreatedSelector
} from './../selectors';

describe('PaymentsContainer Selectors', () => {
  const state = fromJS({
    rpay: {
    },
    [LOCAL_REDUCER]: {
      list: [
        {
          status: 'Ошибка контроля',
          amount: 1,
          receiverName: 'Получатель',
          receiverAccount: '40125810864561024956',
          paymentPurpose: 'Назначение платежа',
          payerAccount: '40702810600001489171',
          recordID: '211a8444-d16a-4d55-b14e-6eade8ff8f7a',
          docDate: '2017-01-01T00:00:00+0300',
          docNumber: '1',
          customerBankRecordID: 'a7468808-5048-4d99-b9ad-0557e06b5410',
          payerName: 'Плательщик',
          payerBankBIC: '044525700',
          bankMessage: 'Сообщение из банка',
          paymentKind: '',
          receiverINN: 'ИНН получателя',
          allowedSmActions: {
            sign: false,
            fromArchive: false,
            delete: true,
            recall: false,
            save: true,
            archive: true,
            send: false
          },
          signed: false,
          linkedDocs: [],
          notes: 'Заметки',
          lastChangeStateDate: '2017-01-01T00:00:00+0300'
        }
      ],
      selectedItems: [],
      columns: [
        {
          id: 'number',
          title: 'Номер',
          canGrouped: false
        },
        {
          id: 'date',
          title: 'Дата',
          canGrouped: true
        }
      ],
      filtersConditions: [
        {
          id: 'status',
          title: 'Статус',
          type: 'multipleSelect',
          values: [
            'Статус 1',
            'Статус 2'
          ]
        }
      ],
      filters: [
        {
          title: 'Test Filter',
          isPreset: true,
          savedConditions: [
            {
              id: 'status',
              params: ['Статус 1']
            }
          ]
        }
      ],
      settings: {
        visible: ['date'],
        sorting: { id: 'date', direction: -1 },
        grouped: null,
        pagination: 30,
        width: {
          number: 100,
          date: 92
        }
      },
      operations: [
        {
          id: 'signAndSend',
          title: 'Подписать и отправить',
          icon: 'sign-and-send',
          groped: false
        },
        {
          id: 'sign',
          title: 'Подписать',
          icon: 'sign',
          groped: false
        },
        {
          id: 'repeat',
          title: 'Повторить',
          icon: 'repeat',
          groped: false
        },
        {
          id: 'print',
          title: 'Распечатать',
          icon: 'print',
          groped: true
        },
        {
          id: 'remove',
          title: 'Удалить',
          icon: 'remove',
          groped: true
        },
        {
          id: 'recall',
          title: 'Отозвать',
          icon: 'recall',
          groped: false
        },
        {
          id: 'send',
          title: 'Отправить',
          icon: 'send',
          groped: true
        },
        {
          id: 'archive',
          title: 'Переместить в архив',
          icon: 'archive',
          groped: true
        },
        {
          id: 'visa',
          title: 'Виза',
          icon: 'visa',
          groped: false
        },
        {
          id: 'saveAsTemplate',
          title: 'Сохранить как шаблон',
          icon: 'save-as-template',
          groped: false
        }
      ],
      isListLoading: false,
      isExportListLoading: false,
      isListUpdateRequired: false,
      isReloadRequired: false,
      isTemplatesLoadRequired: false,
      isTemplatesLoading: false,
      paginationOptions: [30, 60],
      pages: { size: 1, selected: 0, visible: 1 },
      templates: [
        {
          recordID: 'e15e9fc5-933b-442a-8fa6-23f4e0d1310c',
          name: 'РПП - шаблончик',
          correspondentName: 'получатель платежа'
        },
        {
          recordID: 'a15e9fc5-933b-442a-8fa6-23f4e0d1310c',
          name: 'РПП - шаблончик',
          correspondentName: 'получатель платежа'
        }
      ]
    }
  });

  it('columnsCreatedSelector', () => {
    const selector = columnsCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'number',
          width: 100,
          title: 'Номер',
          canGrouped: false,
          visible: false,
          sorting: 0,
          grouped: false,
        },
        {
          id: 'date',
          width: 92,
          title: 'Дата',
          canGrouped: true,
          visible: true,
          sorting: -1,
          grouped: false,
        }
      ]
    );
  });

  it('filtersConditionsCreatedSelector', () => {
    const selector = filtersConditionsCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'status',
          title: 'Статус',
          type: 'multipleSelect',
          values: [
            'Статус 1',
            'Статус 2'
          ]
        }
      ]
    );
  });

  it('filtersCreatedSelector', () => {
    const selector = filtersCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          title: 'Test Filter',
          isPreset: true,
          savedConditions: [
            {
              id: 'status',
              params: ['Статус 1']
            }
          ],
          conditions: [
            {
              id: 'status',
              params: ['Статус 1'],
              value: 'Статус 1'
            }
          ]
        }
      ]
    );
  });

  it('listCreatedSelector', () => {
    const selector = listCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'notGrouped',
          selected: false,
          items: [
            {
              id: '211a8444-d16a-4d55-b14e-6eade8ff8f7a',
              number: '1',
              date: '01.01.2017',
              sum: '1.00',
              status: 'Ошибка контроля',
              recalls: [],
              bankMessage: 'Сообщение из банка',
              lastChangeStateDate: '01.01.2017 00:00:00',
              receiver: 'Получатель',
              receiverAccount: '40125.810.8.64561024956',
              paymentPurpose: 'Назначение платежа',
              payerAccount: '40702.810.6.00001489171',
              payer: 'Плательщик',
              inn: 'ИНН получателя',
              payerBankBIC: '044525700',
              note: 'Заметки',
              selected: false,
              operations: {
                signAndSend: false,
                sign: false,
                repeat: true,
                print: true,
                remove: true,
                recall: false,
                send: false,
                archive: true,
                visa: false,
                saveAsTemplate: true
              }
            }
          ]
        }
      ]
    );
  });

  it('paginationOptionsCreatedSelector', () => {
    const selector = paginationOptionsCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          value: 30,
          title: 30,
          selected: true
        },
        {
          value: 60,
          title: 60,
          selected: false
        }
      ]
    );
  });

  it('panelOperationsCreatedSelector', () => {
    const selector = panelOperationsCreatedSelector(state);

    expect(selector.toJS()).toEqual([]);
  });

  it('selectedItemsSumCreatedSelector', () => {
    const selector = selectedItemsSumCreatedSelector(state);

    expect(selector).toEqual('0.00 руб.');
  });

  it('templatesCreatedSelector', () => {
    const selector = templatesCreatedSelector(state);

    expect(selector.toJS()).toEqual(
      [
        {
          id: 'e15e9fc5-933b-442a-8fa6-23f4e0d1310c',
          title: 'РПП - шаблончик',
          corr: 'получатель платежа'
        },
        {
          id: 'a15e9fc5-933b-442a-8fa6-23f4e0d1310c',
          title: 'РПП - шаблончик',
          corr: 'получатель платежа'
        }
      ]
    );
  });
});
