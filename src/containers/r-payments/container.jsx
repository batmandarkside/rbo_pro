import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import TemplatesSelectItem from 'components/templates-select-item';
import Spreadsheet, {
  Toolbar,
  Filters,
  Operations,
  Table,
  Selection,
  Export,
  Pagination
} from 'components/ui-components/spreadsheet';
import { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import ConfirmNotification from 'components/notification/confirm';
import TemplateModal from 'components/template-modal';
import PaymentsItemStatus from './payments-item-status';
import Import1c from './import-1c';
import { LOCATORS } from './constants';
import './style.css';

class RPaymentsContainer extends Component {

  static propTypes = {
    isExportListLoading: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,
    isTemplatesLoading: PropTypes.bool.isRequired,
    isTemplatesLoadRequired: PropTypes.bool.isRequired,

    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    location: PropTypes.object.isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,
    panelOperations: PropTypes.instanceOf(List).isRequired,
    sections: PropTypes.instanceOf(List).isRequired,
    selectedItems: PropTypes.instanceOf(List).isRequired,
    selectedItemsSumAmount: PropTypes.string.isRequired,
    templates: PropTypes.instanceOf(List).isRequired,

    getSectionIndexAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changeSectionAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getDictionaryForFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    exportListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeGroupingAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
    changeSelectAction: PropTypes.func.isRequired,
    newPaymentAction: PropTypes.func.isRequired,
    routeToItemAction: PropTypes.func.isRequired,
    routeToItemRecallAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    operationRemoveConfirmAction: PropTypes.func.isRequired,
    recallOperationAction: PropTypes.func.isRequired,
    getTemplatesAction: PropTypes.func.isRequired,
    templateEditAction: PropTypes.func.isRequired,
    import1cShowPopupAction: PropTypes.func.isRequired,
    saveAsTemplateAction: PropTypes.func.isRequired,
    tabs: PropTypes.instanceOf(List).isRequired
  };

  componentWillMount() {
    this.props.getSectionIndexAction();
    this.props.getQueryParamsAction();
    this.props.getTemplatesAction();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getSectionIndexAction();
      this.props.getQueryParamsAction();
    } else if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }

    if (this.props.isTemplatesLoadRequired) {
      this.props.getTemplatesAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  filterDictionaryFormFieldOptionRenderer = ({ conditionId, option, highlight }) => {
    switch (conditionId) {
      case 'payerAccount':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.accNum}
            describe={option && option.orgName}
            extra={option && option.availableBalance}
            highlight={{
              value: highlight,
              inTitle: option && option.accNum
            }}
          />
        );
      case 'receiverAccount':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.accNum}
            describe={option && option.orgName}
            extra={option && option.inn}
            highlight={{
              value: highlight,
              inTitle: option && option.accNum
            }}
          />
        );
      case 'receiverInn':
        return (
          <RboFormFieldPopupOptionInner
            label={option && option.inn}
            describe={option && option.orgName}
            highlight={{
              value: highlight,
              inLabel: option && option.inn
            }}
          />
        );
      case 'import':
        return (
          <RboFormFieldPopupOptionInner
            label={option && option.date}
            describe={option && option.fileName}
            extra={option && option.type}
            highlight={{
              value: highlight,
              inLabel: option && option.date,
              inDescribe: option && option.fileName,
              inExtra: option && option.type
            }}
          />
        );
      case 'receiver':
        return (
          <RboFormFieldPopupOptionInner
            label={option && option.title}
            highlight={{
              value: highlight,
              inLabel: option && option.title
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  renderTemplateSelectItemElement = (id) => {
    const { templates, templateEditAction } = this.props;

    const item = templates.find(template => template.get('id') === id);

    return (
      <TemplatesSelectItem
        item={item}
        onEditClick={templateEditAction}
      />
    );
  };

  renderItemAlignRightElement = (value) => {
    const style = { textAlign: 'right' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemStatusElement = (status, id) => {
    const { routeToItemRecallAction, recallOperationAction, operations } = this.props;
    let renderedItem = null;
    this.props.list.forEach((group) => {
      group.get('items').forEach((item) => {
        if (item.get('id') === id) renderedItem = item;
      });
    });

    return (
      <PaymentsItemStatus
        item={renderedItem}
        onRecallClickAction={routeToItemRecallAction}
        recallOperationAction={recallOperationAction}
        operations={operations}
      />
    );
  };

  render() {
    const {
      isExportListLoading,
      isListLoading,
      isTemplatesLoading,
      columns,
      filtersConditions,
      filters,
      list,
      operations,
      pages,
      paginationOptions,
      panelOperations,
      sections,
      selectedItems,
      selectedItemsSumAmount,
      templates,
      exportListAction,
      getListAction,
      changeColumnsAction,
      changeGroupingAction,
      changeFiltersAction,
      getDictionaryForFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSectionAction,
      changeSelectAction,
      changeSortingAction,
      import1cShowPopupAction,
      newPaymentAction,
      operationAction,
      operationRemoveConfirmAction,
      resizeColumnsAction,
      routeToItemAction,
      saveAsTemplateAction,
      tabs
    } = this.props;

    return (
      <div className="b-r-payments-container" data-loc={LOCATORS.CONTAINER}>
        <ConfirmNotification onClickOK={operationRemoveConfirmAction} />
        <TemplateModal onSubmitAction={saveAsTemplateAction} />
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                  newPayment: LOCATORS.TOOLBAR_BUTTON_NEW_PAYMENT,
                  importPayments: LOCATORS.TOOLBAR_BUTTON_IMPORT_PAYMENTS
                },
                tabs: LOCATORS.TOOLBAR_TABS
              }}
              title="Платежные поручения"
              buttons={[
                {
                  id: 'refreshList',
                  isProgress: isListLoading,
                  type: 'refresh',
                  title: 'Обновить',
                  onClick: getListAction
                },
                {
                  id: 'newPayment',
                  isDisabled: isTemplatesLoading,
                  type: 'multiple',
                  title: 'Создать новый платеж',
                  onClick: newPaymentAction,
                  items: [
                    {
                      isDefault: true,
                      id: 'partner',
                      type: 'simple',
                      title: 'Платеж контрагенту'
                    },
                    {
                      id: 'internal',
                      type: 'simple',
                      title: 'Перевод между своими счетами'
                    },
                    {
                      id: 'budget',
                      type: 'simple',
                      title: 'Платеж в бюджет'
                    },
                    {
                      id: 'template',
                      type: 'select',
                      title: 'Создать на основе шаблона:',
                      renderItemElement: this.renderTemplateSelectItemElement,
                      items: templates.map(template => ({ id: template.get('id') })).toJS()
                    }
                  ]
                },
                {
                  id: 'importPayments',
                  type: 'simple',
                  title: 'Импорт',
                  onClick: import1cShowPopupAction
                }
              ]}
              tabs={{
                items: tabs,
                selected: sections.findKey(section => section.get('selected')),
                onChange: changeSectionAction
              }}
            />
            {selectedItems.size ?
              <Operations
                dataLoc={LOCATORS.OPERATIONS}
                operations={panelOperations}
                operationAction={operationAction}
                selectedItems={selectedItems}
              />
              :
              <Filters
                dataLocs={{
                  main: LOCATORS.FILTERS,
                  select: LOCATORS.FILTERS_SELECT,
                  add: LOCATORS.FILTERS_ADD,
                  remove: LOCATORS.FILTERS_REMOVE,
                  operations: LOCATORS.FILTERS_OPERATIONS,
                  operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                  operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                  operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                  operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                  conditions: LOCATORS.FILTERS_CONDITIONS,
                  condition: LOCATORS.FILTERS_CONDITION,
                  conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                  conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                  addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                  addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                  close: LOCATORS.FILTERS_CLOSE
                }}
                conditions={filtersConditions}
                filters={filters}
                changeFiltersAction={changeFiltersAction}
                dictionaryFormFieldOptionRenderer={this.filterDictionaryFormFieldOptionRenderer}
                onConditionDictionaryFormInputChange={getDictionaryForFiltersAction}
              />
            }
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                date: this.renderItemAlignRightElement,
                sum: this.renderItemAlignRightElement,
                status: this.renderItemStatusElement,
                lastChangeStateDate: this.renderItemAlignRightElement
              }}
              renderHeadCells={{
                date: this.renderItemAlignRightElement,
                sum: this.renderItemAlignRightElement,
                lastChangeStateDate: this.renderItemAlignRightElement
              }}
              settings={{
                iconTitle: 'Настройка таблицы платежей',
                changeColumnsAction,
                showGrouping: true,
                changeGroupingAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              sorting={{
                changeSortingAction
              }}
              select={{
                selectItemTitle: 'Выделить платеж',
                selectGroupTitle: 'Выделить группу платежей',
                selectAllTitle: 'Выделить все платежи',
                changeSelectAction
              }}
              resize={{
                resizeColumnsAction
              }}
              onItemClick={routeToItemAction}
              operations={operations}
              operationAction={operationAction}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {!isListLoading && !!selectedItems.size &&
            <Selection
              dataLoc={LOCATORS.SELECTION}
              main={{
                label: 'Выбрано:',
                value: selectedItems.size
              }}
              extra={{
                label: 'Сумма:',
                value: selectedItemsSumAmount
              }}
            />
            }
            {!isListLoading && !!list.size && !selectedItems.size &&
              <Export
                dataLoc={LOCATORS.EXPORT}
                label="Выгрузить список в Excel"
                onClick={exportListAction}
                isLoading={isExportListLoading}
              />
            }
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
        <Import1c />
      </div>
    );
  }
}

export default RPaymentsContainer;
