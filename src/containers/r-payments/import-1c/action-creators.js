import ActionTypes from './constants/actions';

export function importFrom1CStartAction() {
  return {
    type: ActionTypes.IMPORT_FROM_1C_START
  };
}

export function importFrom1CSuccessAction(taskId) {
  return {
    type: ActionTypes.IMPORT_FROM_1C_SUCCESS,
    payload: { taskId }
  };
}

export function importFrom1CDoneAction() {
  return { type: ActionTypes.IMPORT_FROM_1C_DONE };
}

export function importFrom1CNotImportedAction(errors) {
  return {
    type: ActionTypes.IMPORT_FROM_1C_SET_NOT_IMPORTED,
    payload: { errors }
  };
}

export function importFrom1CUpdateProgressAction({ data }) {
  return {
    type: ActionTypes.IMPORT_FROM_1C_UPDATE_PROGRESS,
    payload: data
  };
}

export function setSettingsAction(settings) {
  return {
    type: ActionTypes.IMPORT_FROM_1C_SET_SETTINGS,
    payload: { settings }
  };
}

export function importResetAction() {
  return {
    type: ActionTypes.IMPORT_FROM_1C_RESET
  };
}

// TODO: для перехода на импорт с журнала
// export function setTaskIdAction() {
//   return {
//     type: ActionTypes.IMPORT_FROM_1C_SET_TASK_ID
//   };
// }

export function importFrom1CUploadAction(fileName, content) {
  return {
    type: ActionTypes.IMPORT_FROM_1C_UPLOAD,
    payload: { fileName, content }
  };
}

export function showPopupAction() {
  return { type: ActionTypes.IMPORT_FROM_1C_SHOW };
}

export function hidePopupAction() {
  return { type: ActionTypes.IMPORT_FROM_1C_HIDE };
}

export function routeToPaymentsAction(status) {
  return {
    type: ActionTypes.IMPORT_FROM_1C_ROUTE_TO_PAYMENTS,
    payload: { status }
  };
}
