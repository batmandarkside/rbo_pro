import React, { Component } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import Chart from '@rbo/components/lib/charts/Chart';

const defaultConfig = {
  chart: {
    type: 'pie',
    height: 180,
    width: 200,
  },
  credits: {
    enabled: false,
  },
  title: {
    text: '0',
    verticalAlign: 'middle',
    align: 'center',
    y: 5,
  },
  tooltip: {
    enabled: false
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: false,
      },
      innerSize: '80%',
      size: 150,
      states: {
        hover: {
          enabled: false,
        },
      },
    }
  },
  series: [{
    name: 'Brands',
    colorByPoint: true,
  }]
};

const propTypes = {
  total: PropTypes.number,
  success: PropTypes.number,
  fail: PropTypes.number,
  warning: PropTypes.number,
  progress: PropTypes.number,
  isImportFinished: PropTypes.bool,
};

const defaultProps = {
  total: 0,
  success: 0,
  fail: 0,
  warning: 0,
  progress: 0
};

class ImportFrom1CProgressPie extends Component {
  config = defaultConfig;

  render = () => {
    const { success, fail, warning, total, progress, isImportFinished } = this.props;
    this.config = R.mergeWith(R.merge, defaultConfig, { title: { text: progress.toString() } });
    this.data = [{
      name: 'Processed',
      // eslint-disable-next-line
      y: isImportFinished ? (progress === 0 && fail !== 100 ? 100 : 0) : progress,
    }, {
      name: 'Remain',
      y: isImportFinished ? 0 : total - progress,
    }, {
      name: 'Success',
      y: isImportFinished ? success : 0,
    }, {
      name: 'Warning',
      y: isImportFinished ? warning : 0,
    }, {
      name: 'Fail',
      // eslint-disable-next-line
      y: isImportFinished ? (fail === 0 && total === 0 ? 100 : fail) : 0,
    }];

    return <Chart config={this.config} data={this.data} />;
  }
}

ImportFrom1CProgressPie.propTypes = propTypes;
ImportFrom1CProgressPie.defaultProps = defaultProps;

export default ImportFrom1CProgressPie;
