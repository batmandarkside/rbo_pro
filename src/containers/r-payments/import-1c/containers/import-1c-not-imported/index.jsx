import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { AutoSizer, Column, Table } from 'react-virtualized';

import './react-virtualized-table.css';

const ImportFrom1CNotImported = ({ progress, tasks }) => {
  const errors = _(tasks)
    .map(item => ({ orderIndex: parseInt(item.orderIndex, 10), message: item.message }))
    .sortBy('orderIndex')
    .value();
  const rowCount = errors.length;
  const rowHeight = 36;
  const height = 290;

  const getRowHeight = ({ index }) => {
    const widthRow = 500;
    const fontSize = 16;
    const sizeLetter = fontSize / 2;
    const content = errors[index].message;
    const sizeCont = content.length;

    return (Math.ceil((sizeCont * sizeLetter) / widthRow) * fontSize) + fontSize;
  };

  return (
    <div>
      <p className="bold">Документов не импортировано: {progress.errors}</p>
      <AutoSizer disableHeight>
        {({ width }) => (
          <Table
            height={height}
            headerHeight={rowHeight}
            rowCount={rowCount}
            rowHeight={getRowHeight}
            rowGetter={({ index }) => errors[index]}
            width={width}
          >
            <Column
              width={90}
              label="№ в файле"
              dataKey="orderIndex"
            />
            <Column
              width={200}
              flexGrow={1}
              label="Сообщение"
              dataKey="message"
            />
          </Table>
        )}
      </AutoSizer>
    </div>
  );
};

ImportFrom1CNotImported.propTypes = {
  progress: PropTypes.object,
  tasks: PropTypes.array
};

export default connect(
  state => ({
    progress: state.getIn(['import1c', 'progress']).toJS(),
    tasks: state.getIn(['import1c', 'tasks']).toJS()
  })
)(ImportFrom1CNotImported);
