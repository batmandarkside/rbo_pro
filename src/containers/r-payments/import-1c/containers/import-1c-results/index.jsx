import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import R from 'ramda';
import moment from 'moment';
import numeral from 'numeral';
import Grid from '@rbo/components/lib/grid/Grid';
import Column from '@rbo/components/lib/grid/Column';
import Dropzone from '@rbo/components/lib/dropzone/Dropzone';
import Loader from '@rbo/components/lib/loader/Loader';
import Link from '@rbo/components/lib/link/Link';
import Button from '@rbo/components/lib/button/Button';
import * as importFrom1CActions from '../../action-creators';
import { getBinaryData } from '../../helpers/fileUpload';
import ImportFrom1CProgressPie from '../../components/import-1c-progress-pie';


const ImportResult = ({ status, count, ...rest }) => (
  <div key={status} {...rest}>
    <div className="key">{status}:</div>
    <div className="value">{count}</div>
  </div>
);

ImportResult.propTypes = {
  status: PropTypes.node,
  count: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

class ImportResults extends Component {
  static propTypes = {
    taskId: PropTypes.string,
    progress: PropTypes.object,

    routeToPaymentsAction: PropTypes.func.isRequired,
    importFrom1CUploadAction: PropTypes.func.isRequired,
    importResetAction: PropTypes.func.isRequired,
    hidePopupAction: PropTypes.func.isRequired,
    goToNotImported: PropTypes.func,
  };

  goToImportedDocs = (_status) => {
    let status = _status;

    // EL-9869 нужно при "Импортирован" открывать ещё и "Создан"
    if (status === 'Импортирован') status += ',Создан';
    this.props.routeToPaymentsAction(status);

    this.props.importResetAction();
    this.props.hidePopupAction();
  };

  handleFileUpload = (acceptedFiles) => {
    getBinaryData({
      file: acceptedFiles[0],
      onComplete: (data) => {
        this.props.importFrom1CUploadAction(acceptedFiles[0].name, data);
      }
    });
  };

  render() {
    const { taskId, progress, importResetAction, goToNotImported } = this.props;

    if (!taskId) {
      return (
        <Dropzone onDrop={this.handleFileUpload}>
          <div className="absolute absolute-centered align-center font-small">
            <Link dashed>Выберите файл</Link> или перетащите в эту область
          </div>
        </Dropzone>
      );
    }

    if (!progress) {
      return (
        <div className="import-loader">
          <Loader />
        </div>
      );
    }

    const {
      started,
      finished,
      failed,
      totalDocs = 0,
      success = 0,
      warnings = 0,
      errors = 0,
      status,
      statuses,
      sum = 0,
      fileName
    } = progress;

    const isFinished = finished !== '';

    return (
      <Grid className="import-results font-small">
        <Column width={4}>
          { failed ? <ImportFrom1CProgressPie
            total={0}
            success={0}
            warning={0}
            fail={100}
            progress={0}
            isImportFinished
          /> : <ImportFrom1CProgressPie
            total={totalDocs}
            success={success}
            warning={warnings}
            fail={errors}
            progress={success + warnings + errors}
            isImportFinished={isFinished}
          />
        }
          { failed ? <div className="align-center error">Недопустимый формат файла</div> : <div className="align-center">{status}</div>}
        </Column>

        <Column width={8} className="details">
          <div><span className="file-name">{fileName}</span></div>
          <div>
            Начало: {started ? moment.parseZone(started).format('DD.MM.YYYY HH:mm:ss') : '-'} &middot;
            завершен: {finished ? moment.parseZone(finished).format('DD.MM.YYYY HH:mm:ss') : '-'}
          </div>
          <div className="font-medium mt3 warm-grey">Результат импорта файла</div>
          <ImportResult status="Импортировано на сумму" count={`${numeral(sum).format('0,0.00')} руб.`} />
          <ImportResult status="Успешно" count={`${success} из ${totalDocs}`} className="indented" />
          <ImportResult status="С ошибками" count={warnings} className="indented" />
          <ImportResult
            status={
              //eslint-disable-next-line
              errors ?
                (isFinished ?
                  <Link error bold dashed onClick={goToNotImported}>Не импортировано</Link> :
                  <span className="error">Не импортировано</span>) :
                'Не импортировано'
            }
            count={errors}
          />
          <div className="font-medium mt3 warm-grey">Актуальные статусы документов</div>
          {!R.isEmpty(statuses) && R.map(({ status: _status, count }) => (
            <ImportResult
              key={_status}
              status={<a onClick={() => this.goToImportedDocs(_status)}>{_status}</a>}
              count={count}
            />)
          )(statuses)}
        </Column>

        <div className="actions">
          {isFinished || failed ? <Link dashed onClick={() => { importResetAction(); }}>Импортировать новый</Link> : <div />}
          <Button
            primary
            className="right"
            disabled={!isFinished || failed || (success + warnings === 0)}
            onClick={() => this.goToImportedDocs()}
          >
            Перейти к импортированным документам
          </Button>
        </div>
      </Grid>
    );
  }
}

export default connect(
  (state) => {
    const immutableProgress = state.getIn(['import1c', 'progress']);
    const progress = immutableProgress && immutableProgress.toJS(); // должен вернуть null || {}

    return {
      taskId: state.getIn(['import1c', 'taskId']),
      errorsCount: state.getIn(['import1c', 'errorsCount']),
      progress
    };
  },
  { ...importFrom1CActions }
)(ImportResults);
