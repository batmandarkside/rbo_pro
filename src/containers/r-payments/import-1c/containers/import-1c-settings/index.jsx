import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Form from '@rbo/components/lib/form/Form';
import Field from '@rbo/components/lib/form/Field';
import Checkbox from '@rbo/components/lib/form/Checkbox';
import Input from '@rbo/components/lib/form/Input';
import * as importActions from '../../action-creators';

class ImportFrom1CSettings extends Component {
  static propTypes = {
    settings: PropTypes.shape({
      payerNameFromFile: PropTypes.bool.isRequired,
      paymentKindFromFile: PropTypes.bool.isRequired,
      paymentPurposeIgnoreNewLines: PropTypes.bool.isRequired,
      shouldCorrectSymbols: PropTypes.bool.isRequired,
      correctionRules: PropTypes.string.isRequired,
    }).isRequired,
    setSettingsAction: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      correctionRules: props.settings.correctionRules,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.settings.shouldCorrectSymbols === true && prevProps.settings.shouldCorrectSymbols === false) {
      this.inputCorrectionRules.focus();
    }
  }

  updateBoolSettings = (event, { name, checked }) => {
    this.props.setSettingsAction({ [name]: checked });
  }

  updateStringSettings = ({ target }) => {
    this.setState({ correctionRules: target.value });

    clearTimeout(this.correctionRulesTimeout);
    this.correctionRulesTimeout = setTimeout(() => {
      this.props.setSettingsAction({ [target.name]: target.value });
    }, 1000);
  }

  render() {
    const { settings } = this.props;
    const { correctionRules } = this.state;

    return (
      <Form>
        <Field>
          <Checkbox
            id="importFrom1C_settings_payerNameFromFile"
            label="Заполнять наименование плательщика из файла импорта"
            name="payerNameFromFile"
            checked={settings.payerNameFromFile}
            onChange={this.updateBoolSettings}
          />
        </Field>
        <Field>
          <Checkbox
            id="importFrom1C_settings_paymentKindFromFile"
            label="Заполнять вид платежа из файла импорта"
            name="paymentKindFromFile"
            checked={settings.paymentKindFromFile}
            onChange={this.updateBoolSettings}
          />
        </Field>
        <Field>
          <Checkbox
            id="importFrom1C_settings_paymentPurposeIgnoreNewLines"
            label="Игнорировать перенос строки в поле Назначение платежа"
            name="paymentPurposeIgnoreNewLines"
            checked={settings.paymentPurposeIgnoreNewLines}
            onChange={this.updateBoolSettings}
          />
        </Field>
        <Field>
          <Checkbox
            id="importFrom1C_settings_shouldCorrectSymbols"
            label="Корректировать символы"
            name="shouldCorrectSymbols"
            checked={settings.shouldCorrectSymbols}
            onChange={this.updateBoolSettings}
          />
        </Field>
        <div className="correctionrules">
          <Field>
            <Input
              id="importFrom1C_settings_correctionRules"
              name="correctionRules"
              disabled={!settings.shouldCorrectSymbols}
              value={correctionRules}
              onChange={this.updateStringSettings}
              placeholder="Правила корректировки символов"
              ref={ref => (this.inputCorrectionRules = ref)}
            />
          </Field>
        </div>
      </Form>);
  }
}

export default connect(
  state => ({ settings: state.getIn(['profile', 'settings', 'import1C']).toJS() }),
  { setSettingsAction: importActions.setSettingsAction }
)(ImportFrom1CSettings);
