export function getBinaryData({ file, onComplete, onProgress }) {
  const reader = new FileReader();

  reader.onloadend = (readerEvt) => {
    const binaryString = btoa(ab2str(readerEvt.target.result));
    if (onComplete) {
      onComplete(binaryString);
    }
  };

  reader.onprogress = (event) => {
    if (event.lengthComputable && onProgress) {
      const percentLoaded = Math.round((event.loaded / event.total) * 100);
      onProgress(percentLoaded);
    }
  };

  reader.onabort = () => {
    console.error('File read cancelled');
  };

  reader.onerror = (event) => {
    console.error(`Файл не может быть прочитан! код ${event.target.error.code}`);
  };

  reader.readAsArrayBuffer(file);
}

export function ab2str(buf) {
  let result = '';
  if (buf) {
    const bytes = new Uint8Array(buf);
    for (let i = 0; i < bytes.byteLength; i += 1) {
      result += String.fromCharCode(bytes[i]);
    }
  }
  return result;
}
