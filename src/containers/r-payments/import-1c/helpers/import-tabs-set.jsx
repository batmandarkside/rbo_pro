import React from 'react';
import ImportFrom1CResults from '../containers/import-1c-results';
import ImportFrom1CSettings from '../containers/import-1c-settings';
import ImportFrom1CNotImported from '../containers/import-1c-not-imported';

export default function getImportTabsSet(taskId, progress, component) {
  const tabs = [{
    title: 'Загрузка',
    component: (
      <ImportFrom1CResults
        onDrop={component.handleFileUpload}
        onImportStart={component.props.onImportStart}
        goToNotImported={() => component.toggleTab(1)}
      />
    )
  }];

  if (progress && progress.finished !== '' && progress.errors > 0) {
    tabs.push({
      title: 'Неимпортированные',
      component: (<ImportFrom1CNotImported />)
    });
  }

  if (!progress) {
    tabs.push({
      title: 'Настройки',
      component: (<ImportFrom1CSettings />)
    });
  }

  return tabs;
}
