import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from '@rbo/components/lib/modal/Modal';
import Icon from '@rbo/components/lib/icon/Icon';
import Header from '@rbo/components/lib/header/Header';
import NavMenu from '@rbo/components/lib/navMenu/NavMenu';

import * as importActions from './action-creators';

import getImportTabsSet from './helpers/import-tabs-set';

import './style.css';

class ImportFrom1CModal extends Component {
  static propTypes = {
    taskId: PropTypes.string,
    progress: PropTypes.object,
    hidePopupAction: PropTypes.func.isRequired,
    // eslint-disable-next-line
    routeToPaymentsAction: PropTypes.func.isRequired,
    // eslint-disable-next-line
    onImportStart: PropTypes.func.isRequired,
    importResetAction: PropTypes.func.isRequired,
    isModalOpen: PropTypes.bool.isRequired
  };

  static defaultProps = {
    onImportStart: () => {}
  };

  state = { openTab: 0 };

  closeModalAction = () => {
    if (this.props.progress && (this.props.progress.failed || this.props.progress.finished !== '')) {
      // резетим только если импорт завершен
      this.props.importResetAction();
    }
    this.props.hidePopupAction();
    this.setState({ openTab: 0 });
  }

  toggleTab = (index) => {
    this.setState({ openTab: index });
  };

  render() {
    const { openTab } = this.state;
    const { isModalOpen, progress, taskId } = this.props;

    const tabsSet = getImportTabsSet(taskId, progress, this);

    return (
      <Modal
        isOpen={isModalOpen}
        onOverlayClick={this.closeModalAction}
        className="import-from-1c-modal"
      >
        <div className="flex justify-between mb3 modal-header">
          <Header h={2}>Импорт из 1С</Header>
          <div className="nav-menu">
            <NavMenu
              index={openTab}
              items={tabsSet.map(tab => tab.title)}
              onChange={this.toggleTab}
            />
          </div>
        </div>
        <div
          className="crossModal"
          onClick={this.closeModalAction}
        >
          <Icon of="fat cross" size="mini" />
        </div>
        {tabsSet.map(tab => tab.component)[openTab]}
      </Modal>
    );
  }
}

export default connect((state) => {
  const progress = state.getIn(['import1c', 'progress']);
  return {
    progress: progress && progress.toJS(),
    taskId: state.getIn(['import1c', 'taskId']),
    isModalOpen: state.getIn(['import1c', 'isVisible'])
  };
}, {
  ...importActions
})(ImportFrom1CModal);
