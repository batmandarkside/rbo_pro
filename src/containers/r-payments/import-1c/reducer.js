import { fromJS } from 'immutable';
import ActionTypes from './constants/actions';

const defaults = {
  taskId: null,
  tasks: [],
  errorsCount: 0,
  progress: null,
};

const initialState = fromJS({
  ...defaults,
  isVisible: false,
  isFetching: false,
  settings: {
    payerNameFromFile: false,
    paymentKindFromFile: true,
    paymentPurposeIgnoreNewLines: false,
    shouldCorrectSymbols: false,
    correctionRules: '',
  }
});

export default function importFrom1CReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ActionTypes.IMPORT_FROM_1C_SHOW:
      return state.merge({ isVisible: true });
    case ActionTypes.IMPORT_FROM_1C_HIDE:
      return state.merge({ isVisible: false });
    case ActionTypes.IMPORT_FROM_1C_START:
      return state.merge({
        isFetching: true,
      });
    case ActionTypes.IMPORT_FROM_1C_SUCCESS:
      return state.merge({
        isFetching: false,
        taskId: payload.taskId,
      });
    case ActionTypes.IMPORT_FROM_1C_UPDATE_PROGRESS: // results -> progress
      return state.merge({
        progress: payload,
      });
    case ActionTypes.IMPORT_FROM_1C_RESET:
      return state.merge(defaults);

    case ActionTypes.IMPORT_FROM_1C_SET_SETTINGS:
      return state.mergeDeep({
        settings: payload.settings,
      });
    // case ActionTypes.IMPORT_FROM_1C_SET_TASK_ID:
    //   return state.merge({
    //     taskId: payload
    //   });
    case ActionTypes.IMPORT_FROM_1C_SET_NOT_IMPORTED:
      return state.merge({
        tasks: state.get('tasks').concat(payload.errors)
      });

    default:
      return state;
  }
}
