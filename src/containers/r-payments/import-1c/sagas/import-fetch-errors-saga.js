import { select, call, put } from 'redux-saga/effects';
import Api from 'app/api';

import { importFrom1CNotImportedAction } from '../action-creators';

const MAX_FETCH_ERRORS = 240; // ограничение с серверной стороны на количество загружаемых ошибок

export default function* importFetchErrorsSaga() {
  const {
    taskId,
    progress: { errors }
  } = yield select(state => state.get('import1c').toJS());

  if (errors === 0) return;

  let requestedErrors = 0;

  while (requestedErrors < errors) {
    const restOfErrors = errors - requestedErrors;
    const requestSize = restOfErrors > MAX_FETCH_ERRORS ? MAX_FETCH_ERRORS : restOfErrors;

    try {
      const response = yield call(Api.imports.findErrorsImport, {
        docId: taskId,
        offset: requestedErrors,
        offsetStep: requestSize
      });
      requestedErrors += requestSize;

      yield put(importFrom1CNotImportedAction(response.data));
    } catch (err) {
      console.error('Error [getErrorTask]: Import 1C - server response bad answer', err);
      return;
    }
  }
}
