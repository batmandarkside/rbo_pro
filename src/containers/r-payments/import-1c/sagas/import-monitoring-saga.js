import { select, put, call } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import Api from 'app/api';

import { importFrom1CUpdateProgressAction, importFrom1CDoneAction } from '../action-creators';

const PING_PROGRESS_INTERVAL_MS = 1000;

export default function* importMonitoringSaga() {
  // eslint-disable-next-line
  while (true) {
    yield delay(PING_PROGRESS_INTERVAL_MS);

    const taskId = yield select(state => state.getIn(['import1c', 'taskId']));

    try {
      const response = yield call(Api.imports.findOneById, taskId);
      yield put(importFrom1CUpdateProgressAction(response));
    } catch (err) {
      yield put(importFrom1CUpdateProgressAction({
        finished: 'Ошибка загрузки файла с сервера!',
        started: 'Ошибка загрузки файла с сервера!',
        state: 'Ошибка загрузки файла с сервера!',
        status: 'Ошибка загрузки файла с сервера!',
        total: 0,
        fileName: 'Ошибка загрузки файла с сервера!',
        importType: '',
        log: '',
        sum: 0.00,
        errors: 0,
        success: 0,
        warnings: 0,
        failed: false,
        tasks: [],
        statuses: [],
      }));
      console.error('Error [importFrom1CUpdateProgress]: Import 1C - server response bad answer ', err);
      return;
    }

    const progress = yield select(state => state.getIn(['import1c', 'progress']).toJS());

    const isFinished = progress.finished !== '';
    const resultsCount = progress.errors + progress.warnings + progress.success;

    if ((isFinished && resultsCount > 0) || progress.failed) { // импорт завершен
      yield put(importFrom1CDoneAction());
      return;
    }
  }
}
