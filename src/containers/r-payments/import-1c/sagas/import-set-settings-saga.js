import { call, select } from 'redux-saga/effects';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';

const profileSettingsSelector = state => state.getIn(['profile', 'settings']);

export default function* importSetSettingsSaga({ payload }) {
  const { settings } = payload;

  const currentSettings = yield select(profileSettingsSelector);

  const newSettings = currentSettings.mergeDeep({
    import1C: settings
  });

  const newSettingsPure = newSettings.toJS();

  yield call(dalSaveSettings, { payload: { settings: newSettingsPure } });
}
