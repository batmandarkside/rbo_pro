import { takeLatest, put, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';

import ACTIONS from '../constants/actions';
import { LOCAL_ROUTER_ALIAS } from '../../constants';


import importMonitoringSaga from './import-monitoring-saga';
import uploadImportFileSaga from './upload-import-file-saga';
import importFetchErrorsSaga from './import-fetch-errors-saga';
import importSetSettingsSaga from './import-set-settings-saga';

const importSelector = state => state.get('import1c');

function* routeToPaymentsSaga({ payload }) {
  const immutableImportData = yield select(importSelector);
  const importData = immutableImportData.toJS();

  const {
    taskId,
  } = importData;

  const filter = {
    conditions: [
      {
        id: 'import',
        params: taskId
      }
    ],
    isPreset: true
  };

  const { status } = payload;
  if (status) {
    filter.conditions.push({
      id: 'status',
      params: status.split(',')
    });
  }

  yield put(push(`${LOCAL_ROUTER_ALIAS}?${qs.stringify({ filter })}`));
}

export default function* import1cSaga() {
  yield takeLatest(ACTIONS.IMPORT_FROM_1C_UPLOAD, uploadImportFileSaga);

  // после выгрузки файла импорта на сервер запускаем мониторинг прогресса импорта на сервере
  yield takeLatest(ACTIONS.IMPORT_FROM_1C_SUCCESS, importMonitoringSaga);

  // после обработки файлов импорта на сервере запрашиваем все ошибки импорта
  yield takeLatest(ACTIONS.IMPORT_FROM_1C_DONE, importFetchErrorsSaga);

  yield takeLatest(ACTIONS.IMPORT_FROM_1C_SET_SETTINGS, importSetSettingsSaga);

  yield takeLatest(ACTIONS.IMPORT_FROM_1C_ROUTE_TO_PAYMENTS, routeToPaymentsSaga);
}
