import { select, put, call } from 'redux-saga/effects';
import Api from 'app/api';
import R from 'ramda';
import { importFrom1CStartAction, importFrom1CSuccessAction } from '../action-creators';

const importSettingsSelector = state => state.getIn(['profile', 'settings', 'import1C']).toJS();

export default function* uploadImportFileSaga({ payload }) {
  yield put(importFrom1CStartAction());

  const importSettings = yield select(importSettingsSelector);

  // TODO: выпилить Ramda
  const settings = R.pipe(
    R.when(R.propEq('shouldCorrectSymbols', false), R.assoc('correctionRules', '')),
    R.pick(['payerNameFromFile', 'paymentKindFromFile', 'paymentPurposeIgnoreNewLines', 'correctionRules'])
  )(importSettings);

  try {
    const { fileName, content } = payload;
    const { data: { taskId } } = yield call(Api.imports.createTask, { fileName, content, settings });

    // выгружаем файл, получаем в ответ taskId -> сохраняем в redux store
    yield put(importFrom1CSuccessAction(taskId));
  } catch (err) {
    console.error('Error [importFrom1CSuccess]: Import 1C - server response bad answer', err);
    throw err;
  }
}
