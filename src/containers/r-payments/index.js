import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import mapStateToProps from './selectors';
import * as LocalActions from './actions';
import RPaymentsContainer from './container';
import saga from './sagas';
import reducer from './reducer';
import reducer1C from './import-1c/reducer';
import { showPopupAction as import1cShowPopupAction } from './import-1c/action-creators';

const withConnect = connect(
  mapStateToProps,
  {
    ...LocalActions,
    import1cShowPopupAction
  }
);
const withReducer = injectReducer({ key: 'RPaymentsContainerReducer', reducer });
const withReducer1C = injectReducer({ key: 'import1c', reducer: reducer1C });
const withSaga = injectSaga({ key: 'rPaymentsContainerSagas', saga });

export default compose(
  withReducer,
  withReducer1C,
  withSaga,
  withConnect,
)(RPaymentsContainer);
