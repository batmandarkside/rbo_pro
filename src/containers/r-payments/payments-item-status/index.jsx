import React from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import classnames from 'classnames';
import { getDocStatusClassNameMode } from 'utils';
import Recalls from './recalls';
import './style.css';

const PaymentsItemStatus = (props) => {
  const { item, onRecallClickAction, recallOperationAction, operations } = props;
  const titleClassName = classnames('b-payments-item-status__title', getDocStatusClassNameMode(item.get('status')));

  return (
    <div className="b-payments-item-status">
      {!!item.get('recalls').size &&
      <Recalls
        status={item.get('status')}
        recalls={item.get('recalls')}
        onRecallClickAction={onRecallClickAction}
        recallOperationAction={recallOperationAction}
        operations={operations}
      />
      }
      <span className={titleClassName}>
        {item.get('status')}
      </span>
    </div>
  );
};

PaymentsItemStatus.propTypes = {
  item: PropTypes.instanceOf(Map).isRequired,
  onRecallClickAction: PropTypes.func.isRequired,
  recallOperationAction: PropTypes.func.isRequired,
  operations: PropTypes.instanceOf(List).isRequired
};

export default PaymentsItemStatus;
