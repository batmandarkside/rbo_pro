import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'ui-components/icon';

const PaymentsItemStatusRecallOperation = (props) => {
  const { id, recallId, title, icon, onClick } = props;

  const handleOnClick = (e) => {
    e.preventDefault();
    onClick(id, recallId);
  };

  return (
    <div className="b-payments-item-status__recall-item-operation">
      <Icon type={icon} size="16" title={title} onClick={handleOnClick} />
    </div>
  );
};

PaymentsItemStatusRecallOperation.propTypes = {
  id: PropTypes.string.isRequired,
  recallId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default PaymentsItemStatusRecallOperation;
