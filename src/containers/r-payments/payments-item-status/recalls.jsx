import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import classnames from 'classnames';
import PopupBox from 'ui-components/popup-box';
import Recall from './recall';

class PaymentsItemStatusRecalls extends Component {
  static propTypes = {
    status: PropTypes.string.isRequired,
    recalls: PropTypes.instanceOf(List),
    onRecallClickAction: PropTypes.func.isRequired,
    recallOperationAction: PropTypes.func.isRequired,
    operations: PropTypes.instanceOf(List).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      popupOpen: false
    };
  }

  getFinalStatus = (status) => {
    switch (status) {
      case 'ЭП/АСП неверна':
      case 'Ошибка реквизитов':
      case 'Отказан АБС':
      case 'Отозван':
      case 'Отвергнут Банком':
      case 'Исполнен':
        return true;
      default:
        return false;
    }
  };

  handleOnClick = (e) => {
    e.stopPropagation();
    this.handlePopupOpen();
  };

  handlePopupOpen = () => {
    this.setState({
      popupOpen: true
    });
  };

  handlePopupClose = () => {
    this.setState({
      popupOpen: false
    });
  };

  handleOnMouseLeave = (e) => {
    if (!e.relatedTarget || !e.relatedTarget.closest || !e.relatedTarget.closest('.b-popup-box')) {
      this.handlePopupClose();
    }
  };

  handleOnRecallClick = (item) => {
    this.props.onRecallClickAction(item.get('parentId'), item.get('id'));
    this.handlePopupClose();
  };

  handleOnRecallOperationClick = (operationId, recallId) => {
    this.props.recallOperationAction(operationId, recallId);
    this.handlePopupClose();
  };

  calculateLabel = (recalls, finalStatus) => {
    const quantity = recalls.size;
    const quantityString = quantity.toString();
    const quantityLastDigit = parseInt(quantityString[quantityString.length - 1], 10);

    let label = '';
    if (quantity === 1) {
      label += 'Отзыв';
      if (!finalStatus) label += `: ${recalls.get(0).get('status')}`;
    } else if (quantity > 1) {
      label += `${quantity} `;
      label += 'отзыв';
      if ((quantity > 5 && quantity < 21) || quantityLastDigit > 4) label += 'ов';
      else if (quantityLastDigit > 1) label += 'a';
    }
    return label;
  };

  render() {
    const { status, recalls, operations } = this.props;
    const { popupOpen } = this.state;
    const finalStatus = this.getFinalStatus(status);
    const label = this.calculateLabel(recalls, finalStatus);
    const toggleClassName = classnames('b-payments-item-status__recall-toggle', finalStatus && 'm-final');

    return (
      <div className="b-payments-item-status__recall" onMouseLeave={this.handleOnMouseLeave}>
        <span
          ref={(element) => { this.popupOpenerElement = element; }}
          className={toggleClassName}
          onClick={this.handleOnClick}
        >
          {label}
        </span>
        {popupOpen &&
        <PopupBox
          openerElement={this.popupOpenerElement}
          onClose={this.handlePopupClose}
          closeOnMouseLeave
        >
          <div className="b-payments-item-status__recall-popup">
            <div className="b-payments-item-status__recall-items">
              {recalls.map((item, key) => (
                <Recall
                  key={key}
                  item={item}
                  onClick={this.handleOnRecallClick}
                  onOperationClick={this.handleOnRecallOperationClick}
                  operations={operations}
                />
              ))}
            </div>
          </div>
        </PopupBox>
        }
      </div>
    );
  }
}

export default PaymentsItemStatusRecalls;
