import { fromJS } from 'immutable';
import moment from 'moment-timezone';
import { ACTIONS, LOCAL_ROUTER_ALIAS } from './constants';

export const initialSections = [
  {
    id: 'working',
    title: 'Рабочие документы',
    route: LOCAL_ROUTER_ALIAS,
    api: '',
    selected: true
  },
  {
    id: 'archive',
    title: 'Архив',
    route: `${LOCAL_ROUTER_ALIAS}/archive`,
    api: 'archived'
  },
  {
    id: 'trash',
    title: 'Удаленные',
    route: `${LOCAL_ROUTER_ALIAS}/trash`,
    api: 'deleted'
  }
];

export const initialFiltersConditions = [
  {
    id: 'date',
    title: 'Дата',
    type: 'datesRange',
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'status',
    title: 'Статус',
    type: 'multipleSelect',
    values: [
      'Создан',
      'Импортирован',
      'Ошибка контроля',
      'Частично подписан',
      'Подписан',
      'Доставлен',
      'ЭП/АСП неверна',
      'Ошибка реквизитов',
      'Принят',
      'Отложен',
      'Отозван',
      'Помещен в очередь',
      'Отвергнут Банком',
      'Исполнен'
    ],
    sections: ['working', 'archive']
  },
  {
    id: 'sum',
    title: 'Сумма',
    type: 'amountsRange',
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'payerName',
    title: 'Плательщик',
    type: 'string',
    maxLength: 160,
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'payerAccount',
    title: 'Счет плательщика',
    type: 'search',
    inputType: 'Account',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'receiver',
    title: 'Получатель',
    type: 'suggest',
    inputType: 'Text',
    maxLength: 160,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'receiverAccount',
    title: 'Счет получателя',
    type: 'suggest',
    inputType: 'Account',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'receiverInn',
    title: 'ИНН получателя',
    type: 'suggest',
    inputType: 'Digital',
    maxLength: 12,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'paymentPurpose',
    title: 'Назначение платежа',
    type: 'string',
    maxLength: 210,
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'notes',
    title: 'Заметки',
    type: 'string',
    maxLength: 100,
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'withRecall',
    title: 'Отзыв',
    type: 'select',
    values: [
      'С отзывом',
      'Без отзыва'
    ],
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'import',
    title: 'Импорт',
    type: 'search',
    inputType: 'Text',
    useOptionValue: true,
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'lastStateChangeDate',
    title: 'Изменено',
    type: 'datesRange',
    sections: ['working', 'archive', 'trash']
  }
];

export const initialFilters = [
  {
    id: 'paymentsContainerPresetToday',
    title: 'За сегодня',
    isPreset: true,
    savedConditions: [
      {
        id: 'date',
        params: {
          dateFrom: moment({ hour: 0, minute: 0, seconds: 0 }).format(),
          dateTo: moment({ hour: 0, minute: 0, seconds: 0 }).format()
        }
      }
    ],
    sections: ['working', 'archive', 'trash']
  },
  {
    id: 'paymentsContainerPresetToSign',
    title: 'На подпись',
    isPreset: true,
    savedConditions: [
      {
        id: 'status',
        params: ['Создан', 'Импортирован', 'Частично подписан']
      }
    ],
    sections: ['working', 'archive']
  },
  {
    id: 'paymentsContainerPresetRejected',
    title: 'Отвергнутые',
    isPreset: true,
    savedConditions: [
      {
        id: 'status',
        params: ['ЭП/АСП неверна', 'Ошибка реквизитов', 'Отвергнут Банком']
      }
    ],
    sections: ['working', 'archive']
  }
];

export const initialColumns = [
  {
    id: 'number',
    title: 'Номер',
    canGrouped: false
  },
  {
    id: 'date',
    title: 'Дата',
    canGrouped: true
  },
  {
    id: 'sum',
    title: 'Сумма',
    canGrouped: false
  },
  {
    id: 'status',
    title: 'Статус',
    canGrouped: true
  },
  {
    id: 'bankMessage',
    title: 'Сообщение из банка',
    canGrouped: false
  },
  {
    id: 'lastChangeStateDate',
    title: 'Изменено',
    canGrouped: false
  },
  {
    id: 'receiver',
    title: 'Получатель',
    canGrouped: true
  },
  {
    id: 'receiverAccount',
    title: 'Счет получателя',
    canGrouped: true
  },
  {
    id: 'paymentPurpose',
    title: 'Назначение платежа',
    canGrouped: false
  },
  {
    id: 'payerAccount',
    title: 'Счет плательщика',
    canGrouped: true
  },
  {
    id: 'payer',
    title: 'Плательщик',
    canGrouped: true
  },
  {
    id: 'inn',
    title: 'ИНН получателя',
    canGrouped: false
  },
  {
    id: 'payerBankBIC',
    title: 'БИК банка плательщика',
    canGrouped: false
  },
  {
    id: 'note',
    title: 'Заметки',
    canGrouped: false
  }
];

export const initialOperations = [
  {
    id: 'signAndSend',
    title: 'Подписать и отправить',
    icon: 'sign-and-send',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'sign',
    title: 'Подписать',
    icon: 'sign',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'unarchive',
    title: 'Вернуть из архива',
    icon: 'unarchive',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'repeat',
    title: 'Повторить',
    icon: 'repeat',
    disabled: false,
    progress: false,
    grouped: false
  },
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'recall',
    title: 'Отозвать',
    icon: 'recall',
    disabled: false,
    progress: false,
    grouped: false
  },
  {
    id: 'send',
    title: 'Отправить',
    icon: 'send',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'archive',
    title: 'Переместить в архив',
    icon: 'archive',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'visa',
    title: 'Виза',
    icon: 'visa',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'saveAsTemplate',
    title: 'Сохранить как шаблон',
    icon: 'save-as-template',
    disabled: false,
    progress: false,
    grouped: false
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialSettings = {
  visible: ['number', 'date', 'sum', 'status', 'bankMessage',
    'receiver', 'receiverAccount', 'paymentPurpose', 'payerAccount', 'payer'],
  sorting: { id: 'date', direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    number: 100,
    date: 92,
    sum: 160,
    status: 192,
    bankMessage: 460,
    lastChangeStateDate: 152,
    receiver: 240,
    receiverAccount: 196,
    paymentPurpose: 360,
    payerAccount: 196,
    payer: 240,
    inn: 148,
    payerBankBIC: 188,
    note: 360
  }
};

export const initialState = fromJS({
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  isTemplatesLoading: false,
  isTemplatesLoadRequired: true,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: [],
  filters: [],
  list: [],
  operations: initialOperations,
  selectedItems: [],
  templates: []
});

export const getSectionIndexSuccess = (state, sectionIndex) => {
  const selectedSectionId = state.get('sections').find((section, key) => key === sectionIndex).get('id');
  return state.merge({
    sections: state.get('sections').map((section, key) => section.set('selected', key === sectionIndex)),
    filtersConditions: fromJS(initialFiltersConditions)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections')),
    filters: fromJS(initialFilters)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections'))
  });
};

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getFiltersStaticDictionariesSuccess = (state, filtersStaticDictionaries) => {
  const importsDictionary = fromJS(filtersStaticDictionaries.imports);
  return state.set('filtersConditions', state.get('filtersConditions').map((condition) => {
    switch (condition.get('id')) {
      case 'import':
        return condition.setIn(['values', 'items'], importsDictionary);
      default:
        return condition;
    }
  }));
};

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters = state.get('filters').map(item => item.set('selected', false));
  if (filter) {
    if (filters.find(item => item.get('id') === filter.id)) {
      filters = filters.map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = filters.push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const changeSection = (state, sectionIndex) => (
  state.getIn(['sections', sectionIndex, 'selected']) ?
    state :
    state.merge({
      columns: fromJS(initialColumns),
      settings: fromJS(initialSettings),
      pages: fromJS(initialPages),
      paginationOptions: fromJS(initialPaginationOptions)
    })
);

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const isSearching = conditionId !== 'import';
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], isSearching)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  selectedItems: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list);
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([])
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

/**
 * установить операцию в статус - активна ( в прогрессе )
 * @param state
 * @param operationId
 */
export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

/**
 * оброс активной операции и опциональный релоад скроллера
 * @param state
 * @param operationId
 * @param isListUpdateRequired
 */
export const operationFinish = (state, operationId, isListUpdateRequired = false) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

export const scrollerReload = state => state.merge({
  isListUpdateRequired: true
});

export const getTemplatesRequest = state => state.merge({
  isTemplatesLoading: true,
  isTemplatesLoadRequired: false
});

export const getTemplatesSuccess = (state, templates) => state.merge({
  isTemplatesLoading: false,
  templates
});

export const getTemplatesFail = state => state.merge({
  isTemplatesLoading: false
});

function RPaymentsContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.R_PAYMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS:
      return getSectionIndexSuccess(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_SUCCESS:
      return getFiltersStaticDictionariesSuccess(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_SECTION:
      return changeSection(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationRequest(state, 'print');

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_FAIL:
      return operationFinish(state, 'print', false);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_REQUEST:
      return operationRequest(state, 'archive');

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_SUCCESS:
      return operationFinish(state, 'archive', true);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_FAIL:
      return operationFinish(state, 'archive', false);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_REQUEST:
      return operationRequest(state, 'unarchive');

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_SUCCESS:
      return operationFinish(state, 'unarchive', true);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_FAIL:
      return operationFinish(state, 'unarchive', false);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_CONFIRM:
      return operationRequest(state, 'remove');

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_SUCCESS:
      return operationFinish(state, 'remove', true);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationFinish(state, 'remove', false);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_REQUEST:
      return operationRequest(state, 'sign');

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_SUCCESS:
      return operationFinish(state, 'sign');

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_CANCEL:
    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_FAIL:
      return operationFinish(state, 'sign', false);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST:
      return operationRequest(state, 'signAndSend');

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS:
      return operationFinish(state, 'signAndSend');

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_FAIL:
    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_CANCEL:
      return operationFinish(state, 'signAndSend', false);
      
    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_VISA_REQUEST:
      return operationRequest(state, 'visa');

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_SUCCESS:
      return operationFinish(state, 'visa');

    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_FAIL:
    case ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_CANCEL:
      return operationFinish(state, 'visa', false);

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_REQUEST:
      return operationRequest(state, 'send');

    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_SUCCESS:
    case ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_FAIL:
      return operationFinish(state, 'send', true);

    case ACTIONS.R_PAYMENTS_CONTAINER_SCROLLER_RELOAD:
    case ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_SUCCESS:
    case ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SUCCESS:
      return scrollerReload(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_REQUEST:
      return getTemplatesRequest(state);

    case ACTIONS.R_PAYMENTS_CONTAINER_TEMPLATES_REFRESH:
      return state.set('isTemplatesLoadRequired', true);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_SUCCESS:
      return getTemplatesSuccess(state, action.payload);

    case ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_FAIL:
      return getTemplatesFail(state);

    default:
      return state;
  }
}

export default RPaymentsContainerReducer;
