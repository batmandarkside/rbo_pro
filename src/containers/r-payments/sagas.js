import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import moment from 'moment-timezone';
import qs from 'qs';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction,
  addConfirmNotification as dalAddConfirmNotification
} from 'dal/notification/actions';
import {
  showTemplateModalAction as dalShowTemplateModalAction,
  saveAsTemplateFailAction as dalSaveAsTemplateFailAction
}  from 'dal/template/actions';
import {
  getPayments as dalGetList,
  exportPaymentsListToExcel as dalExportPaymentsListToExcel,
  getTemplates as dalGetTemplates,
  getImports as dalGetImports,
  savePaymentAsTemplate as dalSaveAsTemplate
} from 'dal/r-payments/sagas';

import {
  warningSignDocSaga,
  successSignDocSaga
} from 'dal/sign/sagas/notifications-saga';
import { signDocAddAndTrySigningAction } from 'dal/sign/actions';
import {
  archive as dalArchivePayments,
  unarchive as dalUnarchivePayments,
  remove as dalRemovePayments,
  print as dalPrintPayments,
  documentSendToBankSaga
} from 'dal/documents/sagas';

import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';

import {
  getCorrespondents as dalGetCorrespondents
} from 'dal/correspondents/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import {
  ACTIONS,
  LOCAL_ROUTER_ALIAS,
  LOCAL_REDUCER,
  SETTINGS_ALIAS,
  VALIDATE_SETTINGS
} from './constants';
import import1cSaga from './import-1c/sagas';

import { calculateAmount } from './util';

const getParamsForDalOperation = ({ sections, settings, pages, filters, type }) => {
  const section = sections.find(item => item.get('selected')).get('api');
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const visibleColumns = settings.get('visible').toJS();
  const selectedFilter = filters.find(filter => filter.get('selected'));
  const conditions = selectedFilter && selectedFilter.get('conditions');
  const conditionDate = conditions && conditions.find(c => c.get('id') === 'date');
  const dateFrom = (conditionDate && conditionDate.getIn(['params', 'dateFrom'])) || null;
  const dateTo = (conditionDate && conditionDate.getIn(['params', 'dateTo'])) || null;
  const conditionStatus = conditions && conditions.find(c => c.get('id') === 'status');
  const status = (conditionStatus && conditionStatus.get('params')) ? conditionStatus.get('params').toJS() : null;
  const conditionSum = conditions && conditions.find(c => c.get('id') === 'sum');
  const sumMin = (conditionSum && conditionSum.getIn(['params', 'amountFrom'])) || null;
  const sumMax = (conditionSum && conditionSum.getIn(['params', 'amountTo'])) || null;
  const conditionPayerName = conditions && conditions.find(c => c.get('id') === 'payerName');
  const payerName = (conditionPayerName && conditionPayerName.get('params')) || null;
  const conditionAccount = conditions && conditions.find(c => c.get('id') === 'payerAccount');
  const account = (conditionAccount && conditionAccount.get('params')) || null;
  const conditionReceiver = conditions && conditions.find(c => c.get('id') === 'receiver');
  const receiver = (conditionReceiver && conditionReceiver.get('params')) || null;
  const conditionReceiverAccount = conditions && conditions.find(c => c.get('id') === 'receiverAccount');
  const receiverAccount = (conditionReceiverAccount && conditionReceiverAccount.get('params')) || null;
  const conditionReceiverInn = conditions && conditions.find(c => c.get('id') === 'receiverInn');
  const receiverINN = (conditionReceiverInn && conditionReceiverInn.get('params')) || null;
  const conditionPaymentPurpose = conditions && conditions.find(c => c.get('id') === 'paymentPurpose');
  const paymentPurpose = (conditionPaymentPurpose && conditionPaymentPurpose.get('params')) || null;
  const conditionNotes = conditions && conditions.find(c => c.get('id') === 'notes');
  const notes = (conditionNotes && conditionNotes.get('params')) || null;
  const conditionWithRecall = conditions && conditions.find(c => c.get('id') === 'withRecall');
  const withRecall = (conditionWithRecall && conditionWithRecall.get('params')) || null;
  const conditionImport = conditions && conditions.find(c => c.get('id') === 'import');
  const importId = (conditionImport && conditionImport.get('params')) || null;
  const conditionlastStateChangeDate = conditions && conditions.find(c => c.get('id') === 'lastStateChangeDate');
  const lastStateChangeDateFrom =
    (conditionlastStateChangeDate && conditionlastStateChangeDate.getIn(['params', 'dateFrom'])) || null;
  const lastStateChangeDateTo =
    (conditionlastStateChangeDate && conditionlastStateChangeDate.getIn(['params', 'dateTo'])) || null;

  const params = {
    section,
    sort,
    desc,
    dateFrom,
    dateTo,
    status,
    sumMin,
    sumMax,
    payerName,
    account,
    receiver,
    receiverAccount,
    receiverINN,
    paymentPurpose,
    notes,
    withRecall: withRecall ? (withRecall === 'С отзывом') : null,
    importId,
    lastStateChangeDateFrom,
    lastStateChangeDateTo
  };

  switch (type) {
    case 'list':
      params.offset = pages.get('selected') * settings.get('pagination');
      params.offsetStep = settings.get('pagination') + 1;
      break;
    case 'export':
      params.visibleColumns = visibleColumns;
      break;
    default:
      break;
  }

  return params;
};

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);

// sign
export const compileForSignSelector = state => state.getIn(['sign', 'saveAfterTrySignGroupDocuments', 'compileForSign']);
export const compileNotSignSelector = state => state.getIn(['sign', 'saveAfterTrySignGroupDocuments', 'compileNotSign']);
export const realitySignSelector = state => state.getIn(['sign', 'saveRealitySignGroupDocuments']);
export const saveDocIdForFutureSignSelector = state => state.getIn(['sign', 'saveDocIdForFutureSign']);

export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const rpaysListSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
export const localOperationInProgressIdSelector = (state) => {
  const operations = state.getIn([LOCAL_REDUCER, 'operations']);
  const operationInProgress = operations && operations.find(item => item.get('progress'));
  return operationInProgress && operationInProgress.get('id');
};

export const docIdToTemplateSelector = state => state.getIn(['template', 'docIdToTemplate']);

export function* getSectionIndex() {
  const location = yield select(routingLocationSelector);
  const sections = yield select(localSectionsSelector);
  const pathname = location.get('pathname', '');
  let sectionIndex = 0;
  sections.forEach((section, key) => {
    if (pathname.match(new RegExp(section.get('route'), 'gi'))) {
      sectionIndex = key;
    }
  });
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: sectionIndex });
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_SETTINGS_REQUEST });
}

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_REQUEST, payload: { pageIndex, filter } });
}

export function* getFiltersStaticDictionaries(action) {
  const { payload } = action;
  try {
    const result = yield call(dalGetImports, { payload: { offsetStep: 241 } });
    const imports = result.map(item => ({
      ...item,
      label: `${item.finished ? `от ${moment(item.finished).format('DD.MM.YYYY HH:mm')}` : ''} из ${item.fileName}`
    }));
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_SUCCESS, payload: { imports } });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_FAIL });
  }
}

export function* changeSection(action) {
  const { payload } = action;
  const sections = yield select(localSectionsSelector);
  const activeSection = sections.find(section => section.get('id') === payload);
  yield put(push({ pathname: activeSection.get('route'), search: null }));
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;

  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* getDictionaryForFilters({ payload }) {
  try {
    const { conditionId, value = '' } = payload;
    let result;
    switch (payload.conditionId) {
      case 'payerAccount':
        result = yield call(dalGetAccounts, { payload: { accNum: value, currIsoCode: 'RUR' } });
        break;
      case 'receiver':
      case 'receiverInn':
        result = yield call(dalGetCorrespondents, { payload: { requisite: value } });
        break;
      case 'receiverAccount':
        result = yield call(dalGetCorrespondents, { payload: { receiverAccount: value } });
        break;
      default:
        result = null;
        break;
    }
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
      payload: {
        conditionId,
        items: result
      }
    });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getList() {
  try {
    const sections = yield select(localSectionsSelector);
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ sections, settings, pages, filters, type: 'list' });
    const result = yield call(dalGetList, { payload: params });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* exportList() {
  try {
    const sections = yield select(localSectionsSelector);
    const settings = yield select(localSettingsSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ sections, settings, pages: null, filters, type: 'export' });
    const result = yield call(dalExportPaymentsListToExcel, { payload: params });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* newPayment(action) {
  const { payload } = action;
  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/new`,
    search: `?${qs.stringify(payload)}`
  }));
}

export function* routeToItem(action) {
  const { payload } = action;
  yield put(push(`${LOCAL_ROUTER_ALIAS}/${payload}`));
}

export function* routeToItemRecall(action) {
  const { payload } = action;
  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/${payload.itemId}/recalls/${payload.recallId}`,
    // search: `?recall=${payload.recallId}`
  }));
}

export function* operation(action) {
  const { payload: { operationId, items } } = action;

  switch (operationId) {
    case 'print':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_REQUEST, payload: items });
      break;
    case 'archive':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_REQUEST, payload: items });
      break;
    case 'unarchive':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_REQUEST, payload: items });
      break;
    case 'remove':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_REQUEST, payload: items });
      break;
    case 'sign':
      yield put({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'signAndSend':
      yield put({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'visa':
      yield put({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_VISA_REQUEST,
        payload: {
          docIds: items,
          visa: true
        }
      });
      break;
    case 'send':
      yield put({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_REQUEST,
        payload: {
          docIds: items
        }
      });
      break;
    case 'repeat':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REPEAT, payload: items });
      break;
    case 'recall':
      yield put({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_RECALL,
        payload: {
          id: items[0]
        }
      });
      break;
    case 'saveAsTemplate':
      yield put(dalShowTemplateModalAction(true, items[0]));
      break;
    default:
      break;
  }
}

export function* operationRecall(action) {
  const { id } = action.payload;

  yield put(push(`r-payments/${id}/recalls/new`));
}

export function* operationPrint(action) {
  const { payload } = action;
  try {
    const result = yield call(dalPrintPayments, { payload: { docTitle: 'payment_orders', docId: payload } });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationArchive(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalArchivePayments, { payload: { docId: payload } });
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_SUCCESS,
      payload: `Документ${many ? 'ы' : ''} помещен${many ? 'ы' : ''} в архив`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationUnarchive(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalUnarchivePayments, { payload: { docId: payload } });
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_SUCCESS,
      payload: `Документ${many ? 'ы' : ''} возвращен${many ? 'ы' : ''} из архива`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationRemove(action) {
  const { payload } = action;
  const many = payload.length > 1;
  yield put(dalAddConfirmNotification({
    title: `Удаление документ${many ? 'ов' : 'а'}`,
    text: `Вы действительно хотите удалить документ${many ? 'ы' : ''}?`,
    labelSuccess: 'Подтвердить',
    labelCancel: 'Отмена',
    uid: payload[0],
    actionData: payload
  }));
}

export function* operationRemoveConfirm(action) {
  const { payload } = action;
  const many = payload.length > 1;
  try {
    yield call(dalRemovePayments, { payload: { docId: payload } });
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_SUCCESS,
      payload: `Документ${many ? 'ы' : ''} успешно удален${many ? 'ы' : ''}`
    });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

// ---------- SIGN ----------------------
export function* signDocument({ payload }) {
  const payloadWithChannel = {
    ...payload,

    // уникальный канал подписи для контейнера
    channel: {
      success: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_SUCCESS,
      cancel: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_CANCEL,
      fail: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(payloadWithChannel));
}

export function* signAndSend({ payload }) {
  const payloadWithChannel = {
    ...payload,

    // уникальный канал подписи для контейнера
    channel: {
      success: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS,
      cancel: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_CANCEL,
      fail: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(payloadWithChannel));
}

export function* signVisa({ payload }) {
  const payloadWithChannel = {
    ...payload,

    // уникальный канал подписи для контейнера
    channel: {
      success: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_SUCCESS,
      cancel: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_CANCEL,
      fail: ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(payloadWithChannel));
}
// \\---------- SIGN ----------------------


// ---------- SEND ----------------------
export function* operationSend(action) {
  const { payload: { docIds } } = action;
  try {
    yield call(documentSendToBankSaga, { payload: { docId: docIds } });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_SUCCESS });
  } catch (error) {
    put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_FAIL });
  }
}

export function* operationSendAfterSign(action) {
  const { payload: { resultData } } = action;
  const docIds = resultData.map(d => d.docId);
  try {
    yield call(documentSendToBankSaga, { payload: { docId: docIds } });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL });
  }
}

// \\---------- SEND ----------------------

export function* operationSaveAsTemplate(action) {
  const docId = yield select(docIdToTemplateSelector);
  const { payload } = action;
  try {
    yield call(dalSaveAsTemplate, { payload: { docId, name: payload } });
    yield put(dalShowTemplateModalAction(false));
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_TEMPLATES_REFRESH });
    yield put(dalAddOrdinaryNotificationAction({
      type: 'success',
      title: 'Шаблон успешно сохранен',
      message: `Шаблон сохранен с именем: ${payload}`
    }));
  } catch (error) {
    if (error.stack) yield put(dalSaveAsTemplateFailAction(error.stack));
    else {
      put(dalShowTemplateModalAction(false));
      yield call(errorNotification, {
        title: 'Произошла ошибка на сервере.',
        message: 'Невозможно сохранить документ как шаблон'
      });
    }
  }
}

export function* operationRepeat({ payload }) {
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_NEW_PAYMENT, payload: { type: 'repeat', itemId: payload[0] } });
}

export function* recallOperation(action) {
  const { payload: { operationId, recallId } } = action;
  switch (operationId) {
    case 'print':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_REQUEST, payload: recallId });
      break;
    case 'sign':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_REQUEST, payload: recallId });
      break;
    case 'signAndSend':
      yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_REQUEST, payload: recallId });
      break;
    case 'send':
      yield put({
        type: ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_REQUEST,
        payload: {
          docIds: [recallId]
        }
      });
      break;

    default:
      break;
  }
}

export function* recallOperationPrint(action) {
  const { payload } = action;
  try {
    const result = yield call(dalPrintPayments, { payload: { docTitle: 'payment_order_recall', docId: payload } });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* recallOperationSign(action) {
  const { payload } = action;

  const params = {
    docIds: [payload],
    channel: {
      success: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_SUCCESS,
      cancel: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_CANCEL,
      fail: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(params));
}

export function* recallOperationSignSuccess() {
  yield put(successNotification({ payload: 'Отзыв успешно подписан' }));
}

export function* recallOperationSignAndSend(action) {
  const { payload } = action;

  const params = {
    docIds: [payload],
    channel: {
      success: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SIGNED,
      cancel: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_CANCEL,
      fail: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_FAIL,
    }
  };

  yield put(signDocAddAndTrySigningAction(params));
}

export function* recallOperationSignAndSendSigned(action) {
  const { payload: { resultData } } = action;
  const docIds = resultData.map(r => r.docId);
  try {
    yield call(documentSendToBankSaga, { payload: { docId: docIds } });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SUCCESS });
  } catch (error) {
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_FAIL });
  }
}

export function* getTemplates() {
  try {
    const result = yield call(dalGetTemplates, { payload: {} });
    yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* templateEdit(action) {
  const { payload } = action;
  yield put(push(`${LOCAL_ROUTER_ALIAS}/templates/${payload}`));
}

export function* successNotification(action) {
  const { payload } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'success', title: payload }));
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}


/**
 * логика нотификаций после успешной подписи документа
 * зависит от того групповая или это подпись или одного документа
 * есть ли не подписанные документы в группе
 */
export function* notificationSignSaga() {
  // const operationInProgressId = yield select(localOperationInProgressIdSelector);
  const compileForSign = yield select(compileForSignSelector);
  const compileNotSign = yield select(compileNotSignSelector);
  const realitySign = yield select(realitySignSelector);
  const saveDocIdForFutureSign = yield select(saveDocIdForFutureSignSelector);
  const rpays = yield select(rpaysListSelector);
  const isGroupSign = saveDocIdForFutureSign.size > 1;
  const countAmount = calculateAmount(realitySign.map(r => r.get('docId')), rpays);

  // перезагружаем скроллер
  yield put({ type: ACTIONS.R_PAYMENTS_CONTAINER_SCROLLER_RELOAD });

  // @todo все комментарии оставить
  // @todo на доработке

  // console.log(realitySign && realitySign.toJS(), 'realitySignrealitySign');
  // console.log(compileForSign && compileForSign.toJS(), 'realitySignrealitySign');
  // console.log(compileNotSign && compileNotSign.toJS(), 'realitySignrealitySign');

  // ГРУППОВЫЕ ОПЕРАЦИИ
  if (isGroupSign) {
    // if (operationInProgressId === 'sign') { }// нажали на кнопку подписать
    if (compileNotSign.size && realitySign.size) { // при условии что хоть один из группы документов подписан
      yield warningSignDocSaga(
        compileForSign.size,
        realitySign.size,
        compileNotSign.size,
        countAmount
      );
    }

    if (!compileNotSign.size && realitySign.size) { // все выбранные документы подписаны
      yield successSignDocSaga(
        compileForSign.size,
        realitySign.size,
        countAmount
      );
    }
    // if (operationInProgressId === 'signAndSend') { } // нажали на кнопку ( подписать и отправить )
  }

  // НЕ ГРУППОВЫЕ ОПЕРАЦИИ
  if (!isGroupSign) {
    // const signedDocument = realitySign.first();
    // const docType = signedDocument.get('docType');

    yield put(dalAddOrdinaryNotificationAction({
      type: 'success',
      message: 'Документ успешно подписан'
    }));

    /* if (operationInProgressId === 'sign') { // нажали на кнопку подписать
      if (ALLOWED_DOCUMENT_SEND_TO_BANK_STATUS.includes(docType)) { // документ ДОПУСКАЕТ отправку в банк
        yield put(dalAddOrdinaryNotificationAction({
          type: 'success',
          message: 'Документ успешно подписан'
        }));
      } else {
        yield put(dalAddOrdinaryNotificationAction({ // документ НЕ допускает отправку в банк
          type: 'success',
          message: 'Документ успешно подписан'
        }));
      }
    }

    if (operationInProgressId === 'signAndSend') { // нажали на кнопку ( подписать и отправить )
      yield put(dalAddOrdinaryNotificationAction({
        type: 'success',
        message: 'Документ успешно подписан'
      }));
    } */
  }
}


export default function* saga() {
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_SECTION_INDEX_REQUEST, getSectionIndex);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_FILTERS_STATIC_DICTIONARIES_REQUEST, getFiltersStaticDictionaries);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_SECTION, changeSection);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST, getDictionaryForFilters);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_REQUEST, exportList);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_EXPORT_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_GROUPING, changeSettingsColumns);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_NEW_PAYMENT, newPayment);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_ROUTE_TO_ITEM, routeToItem);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_ROUTE_TO_ITEM_RECALL, routeToItemRecall);

  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION, operation);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_REQUEST, operationPrint);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_PRINT_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_REQUEST, operationArchive);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_ARCHIVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_REQUEST, operationUnarchive);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_UNARCHIVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_REQUEST, operationRemove);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_CONFIRM, operationRemoveConfirm);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_SUCCESS, successNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REMOVE_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_REPEAT, operationRepeat);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_RECALL, operationRecall);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SAVE_AS_TEMPLATE, operationSaveAsTemplate);

  // подпись
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_REQUEST, signDocument);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST, signAndSend);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_VISA_REQUEST, signVisa);

  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_DOCUMENT_SUCCESS, notificationSignSaga);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_VISA_SUCCESS, notificationSignSaga);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS, notificationSignSaga);

  // отправить в банк
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_OPERATION_SEND_REQUEST, operationSend);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_CHANNEL_SIGN_AND_SEND_SUCCESS, operationSendAfterSign);

  // отзыв
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION, recallOperation);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_REQUEST, recallOperationPrint);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_PRINT_FAIL, errorNotification);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_REQUEST, recallOperationSign);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_SUCCESS, recallOperationSignSuccess);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_REQUEST, recallOperationSignAndSend);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_RECALL_OPERATION_SIGN_AND_SEND_SIGNED, recallOperationSignAndSendSigned);

  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_GET_TEMPLATES_REQUEST, getTemplates);
  yield takeLatest(ACTIONS.R_PAYMENTS_CONTAINER_TEMPLATE_EDIT, templateEdit);

  yield import1cSaga();
}
