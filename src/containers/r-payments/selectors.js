import { createSelector } from 'reselect';
import { List, Map, OrderedMap } from 'immutable';
import numeral from 'numeral';
import moment from 'moment-timezone';
import { regExpEscape, getFormattedAccNum } from 'utils';
import { LOCAL_REDUCER } from './constants';
import { calculateAmount } from './util';

const isExportListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isExportListLoading']);
const isListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isListLoading']);
const isListUpdateRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isListUpdateRequired']);
const isReloadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isReloadRequired']);
const isTemplatesLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isTemplatesLoading']);
const isTemplatesLoadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isTemplatesLoadRequired']);

const columnsSelector = state => state.getIn([LOCAL_REDUCER, 'columns']);
const filtersConditionsSelector = state => state.getIn([LOCAL_REDUCER, 'filtersConditions']);
const filtersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
const listSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);
const sectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
const selectedItemsSelector = state => state.getIn([LOCAL_REDUCER, 'selectedItems']);
const settingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
const pagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
const paginationOptionsSelector = state => state.getIn([LOCAL_REDUCER, 'paginationOptions']);
const templatesSelector = state => state.getIn([LOCAL_REDUCER, 'templates']);

const mapListItemOperations = (allowedSmActions, operations) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'repeat':
        case 'print':
        case 'saveAsTemplate':
          return true;
        case 'signAndSend':
        case 'visa':
          return allowedSmActions.get('sign');
        case 'remove':
          return allowedSmActions.get('delete');
        case 'unarchive':
          return allowedSmActions.get('fromArchive');
        default:
          return allowedSmActions.get(operationId);
      }
    })), OrderedMap()
  ).toJS()
);

const mapListRecallOperations = (allowedSmActions, operations) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'print':
          return true;
        case 'signAndSend':
          return allowedSmActions.get('sign');
        default:
          return allowedSmActions.get(operationId);
      }
    })), OrderedMap()
  ).toJS()
);

const mapListItem = (item, selected, operations) => Map({
  id: item.get('recordID'),
  number: item.get('docNumber'),
  date: item.get('docDate') ? moment(item.get('docDate')).format('DD.MM.YYYY') : null,
  sum: !isNaN(item.get('amount')) ? numeral(item.get('amount')).format('0,0.00') : null,
  status: item.get('status'),
  recalls: item.get('linkedDocs')
    .filter(doc => doc.get('docType') === 'Recall')
    .sort((a, b) => {
      const aNumber = parseInt(a.get('docNumber'), 10);
      const bNumber = parseInt(b.get('docNumber'), 10);
      if (aNumber < bNumber) { return 1; }
      if (aNumber > bNumber) { return -1; }
      return 0;
    })
    .map(recall => Map({
      id: recall.get('recordID'),
      parentId: recall.get('linkedRecordID'),
      number: recall.get('docNumber'),
      date: moment(recall.get('docDate')).format('DD.MM.YYYY'),
      status: recall.get('status'),
      reason: recall.get('reason'),
      operations: mapListRecallOperations(recall.get('allowedSmActions'), operations)
    })),
  bankMessage: item.get('bankMessage'),
  lastChangeStateDate: item.get('lastChangeStateDate') ?
    moment(item.get('lastChangeStateDate')).format('DD.MM.YYYY HH:mm:ss') :
    null,
  receiver: item.get('receiverName'),
  receiverAccount: getFormattedAccNum(item.get('receiverAccount')),
  paymentPurpose: item.get('paymentPurpose'),
  payerAccount: getFormattedAccNum(item.get('payerAccount')),
  payer: item.get('payerName'),
  inn: item.get('receiverINN'),
  payerBankBIC: item.get('payerBankBIC'),
  note: item.get('notes'),
  selected,
  operations: mapListItemOperations(item.get('allowedSmActions'), operations)
});

const mapFilterConditionDictionaryItems = (condition) => {
  let searchRegExp;
  const items = condition.getIn(['values', 'items']);
  const searchValue = regExpEscape(condition.getIn(['values', 'searchValue'], ''));

  switch (condition.get('id')) {
    case 'payerAccount':
      return condition.setIn(
        ['values', 'items'],
        items
          .map(item => Map({
            accNum: getFormattedAccNum(item.get('accNum')),
            availableBalance: !isNaN(item.get('availableBalance'))
              ? numeral(item.get('availableBalance')).format('0,0.00')
              : null,
            orgName: item.get('orgName'),
            title: item.get('accNum'),
            value: item.get('id')
          }))
      );
    case 'receiver':
      return condition.setIn(
        ['values', 'items'],
        items
          .map(item => Map({
            orgName: item.get('receiverName'),
            label: item.get('receiverName'),
            title: item.get('receiverName'),
            value: item.get('id')
          }))
      );
    case 'receiverAccount':
      return condition.setIn(
        ['values', 'items'],
        items
          .map(item => Map({
            accNum: getFormattedAccNum(item.get('account')),
            inn: item.get('receiverINN'),
            orgName: item.get('receiverName'),
            title: item.get('account'),
            value: item.get('id')
          }))
      );
    case 'receiverInn':
      return condition.setIn(
        ['values', 'items'],
        items
          .map(item => Map({
            inn: item.get('receiverINN'),
            orgName: item.get('receiverName'),
            label: item.get('receiverINN'),
            title: item.get('receiverINN'),
            value: item.get('id')
          }))
      );
    case 'import':
      searchRegExp = new RegExp(`(^|\\s)${searchValue}`, 'ig');
      return condition.setIn(
        ['values', 'items'],
        items
          .filter(item => item.get('label').search(searchRegExp) >= 0)
          .map(item => Map({
            id: item.get('id'),
            date: item.get('finished') ? moment(item.get('finished')).format('DD.MM.YYYY HH:mm') : null,
            fileName: item.get('fileName'),
            type: item.get('importType'),
            label: item.get('label'),
            title: item.get('label'),
            value: item.get('id')
          }))
      );
    default:
      return condition;
  }
};

const mapFilterConditionParamsToValue = (params, condition) => {
  if (!params) return '...';
  let dictionaryItem;
  switch (condition.get('type')) {
    case 'datesRange':
      return `${params.get('dateFrom') ?
        `с ${moment(params.get('dateFrom')).format('DD.MM.YYYY')}` : ''} ${params.get('dateTo') ?
        `по ${moment(params.get('dateTo')).format('DD.MM.YYYY')}` : ''}`;
    case 'amountsRange':
      return `${params.get('amountFrom') ? 
        `от ${numeral(params.get('amountFrom')).format('0,0.00')}` : ''} ${params.get('amountTo') ?
        `до ${numeral(params.get('amountTo')).format('0,0.00')}` : ''}`;
    case 'bool':
      return '';
    case 'multipleSelect':
      return params.join(', ');
    case 'search':
    case 'suggest':
      switch (condition.get('id')) {
        case 'payerAccount':
        case 'receiverAccount':
          return getFormattedAccNum(params);
        case 'import':
          dictionaryItem = condition.getIn(['values', 'items']).find(item => item.get('id') === params);
          return dictionaryItem ? dictionaryItem.get('label') : '';
        default:
          return params;
      }
    default:
      return params;
  }
};

const mapFilterConditions = (conditions, savedConditions, filtersConditions) => (
  (conditions && conditions.size ? conditions : savedConditions).map(condition =>
    condition.merge({
      params: condition.get('params'),
      value: mapFilterConditionParamsToValue(
        condition.get('params'),
        filtersConditions.find(filtersCondition => filtersCondition.get('id') === condition.get('id'))
      )
    })
  )
);

export const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) => columns.map(item => item.merge({
    visible: !!settings.get('visible').find(visible => visible === item.get('id')),
    sorting: settings.getIn(['sorting', 'id']) === item.get('id') ? settings.getIn(['sorting', 'direction']) : 0,
    grouped: settings.get('grouped') === item.get('id'),
    width: settings.getIn(['width', item.get('id')])
  }))
);

export const filtersConditionsCreatedSelector = createSelector(
  filtersConditionsSelector,
  filtersConditions => filtersConditions.map(
    condition => (condition.get('type') === 'suggest' || condition.get('type') === 'search'
      ? mapFilterConditionDictionaryItems(condition)
      : condition)
  )
);

export const filtersCreatedSelector = createSelector(
  filtersSelector,
  filtersConditionsSelector,
  (filters, filtersConditions) => filters
    .map(filter => filter.merge({
      conditions: mapFilterConditions(filter.get('conditions'), filter.get('savedConditions'), filtersConditions),
    }))
    .sort((a, b) => {
      if (a.get('isPreset') && b.get('isPreset')) return 0;
      if (a.get('isPreset') && !b.get('isPreset')) return -1;
      if (!a.get('isPreset') && b.get('isPreset')) return 1;
      return a.get('title').localeCompare(b.get('title'));
    })
);

export const listCreatedSelector = createSelector(
  listSelector,
  selectedItemsSelector,
  settingsSelector,
  operationsSelector,
  (list, selectedItems, settings, operations) => {
    if (!list.size) return list;
    const mappedList = list.map(item =>
      mapListItem(
        item,
        !!selectedItems.filter(selectedItem => selectedItem === item.get('recordID')).size,
        operations
      ));
    const grouped = settings.get('grouped');
    if (!grouped) {
      return List([Map({
        id: 'notGrouped',
        selected: mappedList.filter(item => item.get('selected')).size === mappedList.size,
        items: mappedList
      })]);
    }
    return mappedList.map(item => item.get(grouped)).toOrderedSet().toList().map((group, i) => {
      const items = mappedList.filter(item => item.get(grouped) === group);
      return Map({
        id: `${grouped}_${i}`,
        title: group,
        selected: items.filter(item => item.get('selected')).size === items.size,
        items
      });
    });
  }
);

export const paginationOptionsCreatedSelector = createSelector(
  paginationOptionsSelector,
  settingsSelector,
  (paginationOptions, settings) => paginationOptions.map(item => ({
    value: item,
    title: item,
    selected: settings.get('pagination') === item
  }))
);

export const panelOperationsCreatedSelector = createSelector(
  operationsSelector,
  selectedItemsSelector,
  listSelector,
  (operations, selectedItems, list) => operations.filter((operation) => {
    const itemsWithOperations = selectedItems.map(item => Map({
      id: item,
      operations: mapListItemOperations(
        list.find(listItem => listItem.get('recordID') === item).get('allowedSmActions'),
        operations
      )
    }));
    return (
      itemsWithOperations.size ===
      itemsWithOperations.filter(item => !!item.get('operations')[operation.get('id')]).size
      ) &&
      (selectedItems.size === 1 || operation.get('grouped'));
  })
);

export const selectedItemsSumCreatedSelector = createSelector(
  selectedItemsSelector,
  listSelector,
  (selectedItems, list) =>
    calculateAmount(selectedItems, list)
);

export const templatesCreatedSelector = createSelector(
  templatesSelector,
  templates => templates.map(item => Map({
    id: item.get('recordID'),
    title: item.get('name'),
    corr: item.get('correspondentName')
  }))
);

export const tabCreateSelector = createSelector(
  sectionsSelector,
  sections => sections.map(section => section.merge({
    isActive: section.get('selected')
  }))
);

const mapStateToProps = state => ({
  isListLoading: isListLoadingSelector(state),
  isExportListLoading: isExportListLoadingSelector(state),
  isListUpdateRequired: isListUpdateRequiredSelector(state),
  isReloadRequired: isReloadRequiredSelector(state),
  isTemplatesLoading: isTemplatesLoadingSelector(state),
  isTemplatesLoadRequired: isTemplatesLoadRequiredSelector(state),
  columns: columnsCreatedSelector(state),
  filtersConditions: filtersConditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  list: listCreatedSelector(state),
  operations: operationsSelector(state),
  pages: pagesSelector(state),
  paginationOptions: paginationOptionsCreatedSelector(state),
  panelOperations: panelOperationsCreatedSelector(state),
  sections: sectionsSelector(state),
  selectedItems: selectedItemsSelector(state),
  selectedItemsSumAmount: selectedItemsSumCreatedSelector(state),
  settings: settingsSelector(state),
  templates: templatesCreatedSelector(state),
  tabs: tabCreateSelector(state)
});

export default mapStateToProps;
