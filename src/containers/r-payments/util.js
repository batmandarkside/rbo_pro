import numeral from 'numeral';

/**
 *
 * @param selectedItems
 * @param rpays
 * @returns {string}
 */
export function calculateAmount(selectedItems, rpays) {
  let selectedSum = 0;
  selectedItems.forEach((id) => {
    const amount = rpays
      .find(dalListItem => dalListItem.get('recordID') === id)
      .get('amount');
    if (!isNaN(amount)) selectedSum += amount;
  });
  return `${numeral(selectedSum).format('0,0.00')} руб.`;
}
