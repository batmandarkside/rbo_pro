import { ACTIONS } from './constants';

/**
 *    action загрузки контейнера
 */
export const mountAction = params => ({
  type: ACTIONS.RECALL_CONTAINER_MOUNT,
  payload: { params }
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.RECALL_CONTAINER_UNMOUNT
});

/**
 *    action переключения табов
 *    @param {string} tabId
 */
export const changeTabAction = tabId => ({
  type: ACTIONS.RECALL_CONTAINER_CHANGE_TAB,
  payload: { tabId }
});

/**
 *    action focus на поле формы
 *    @param {string} fieldId
 */
export const fieldFocusAction = fieldId => ({
  type: ACTIONS.RECALL_CONTAINER_FIELD_FOCUS,
  payload: { fieldId }
});

/**
 *    action blur на поле формы
 *    @param {string} fieldId
 */
export const fieldBlurAction = fieldId => ({
  type: ACTIONS.RECALL_CONTAINER_FIELD_BLUR,
  payload: { fieldId }
});

/**
 *    action изменения данных
 *    @param {string} fieldId
 *    @param {string} fieldValue
 */
export const changeDataAction = (fieldId, fieldValue) => ({
  type: ACTIONS.RECALL_CONTAINER_CHANGE_DATA,
  payload: { fieldId, fieldValue }
});

/**
 *    action сворачивания / разворачивания секции формы
 *    @param {string} sectionId
 *    @param {boolean} isCollapsed
 */
export const sectionCollapseToggleAction = (sectionId, isCollapsed) => ({
  type: ACTIONS.RECALL_CONTAINER_SECTION_COLLAPSE_TOGGLE,
  payload: { sectionId, isCollapsed }
});

/**
 *    action выполнения операции
 *    @param {string} operationId
 */
export const operationAction = operationId => ({
  type: ACTIONS.RECALL_CONTAINER_OPERATION,
  payload: { operationId }
});

/**
 *    action закрытия верхней панели документа
 */
export const closeDocumentTopAction = () => ({
  type: ACTIONS.RECALL_CONTAINER_CLOSE_DOCUMENT_TOP
});

/**
 *    action печати подписи документа
 */
export const signaturePrintAction = signId => ({
  type: ACTIONS.RECALL_CONTAINER_SIGNATURE_PRINT_REQUEST,
  payload: { signId }
});

/**
 *    action скачивание подписи документа
 */
export const signatureDownloadAction = signId => ({
  type: ACTIONS.RECALL_CONTAINER_SIGNATURE_DOWNLOAD_REQUEST,
  payload: { signId }
});
