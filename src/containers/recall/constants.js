import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  RECALL_CONTAINER_MOUNT: null,
  RECALL_CONTAINER_UNMOUNT: null,
  RECALL_CONTAINER_GET_DATA_REQUEST: null,
  RECALL_CONTAINER_GET_DATA_SUCCESS: null,
  RECALL_CONTAINER_GET_DATA_FAIL: null,
  RECALL_CONTAINER_SAVE_DATA_REQUEST: null,
  RECALL_CONTAINER_SAVE_DATA_SUCCESS: null,
  RECALL_CONTAINER_SAVE_DATA_FAIL: null,
  RECALL_CONTAINER_SECTION_COLLAPSE_TOGGLE: null,
  RECALL_CONTAINER_FIELD_FOCUS: null,
  RECALL_CONTAINER_FIELD_BLUR: null,

  RECALL_CONTAINER_CHANGE_TAB: null,
  RECALL_CONTAINER_CHANGE_DATA: null,

  RECALL_CONTAINER_OPERATION: null,
  RECALL_CONTAINER_OPERATION_BACK: null,
  RECALL_CONTAINER_OPERATION_SAVE_REQUEST: null,
  RECALL_CONTAINER_OPERATION_SAVE_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_SAVE_FAIL: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_CONTINUE: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_CANCEL: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_DAL_FAIL: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SENDING_REQUEST: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_CANCEL: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL: null,
  RECALL_CONTAINER_OPERATION_SIGN_REQUEST: null,
  RECALL_CONTAINER_OPERATION_SIGN_CONTINUE: null,
  RECALL_CONTAINER_OPERATION_SIGN_DAL_CANCEL: null,
  RECALL_CONTAINER_OPERATION_SIGN_DAL_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_SIGN_DAL_FAIL: null,
  RECALL_CONTAINER_OPERATION_SIGN_CANCEL: null,
  RECALL_CONTAINER_OPERATION_SIGN_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_SIGN_FAIL: null,
  RECALL_CONTAINER_OPERATION_SEND_REQUEST: null,
  RECALL_CONTAINER_OPERATION_SEND_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_SEND_FAIL: null,
  RECALL_CONTAINER_OPERATION_PRINT_REQUEST: null,
  RECALL_CONTAINER_OPERATION_PRINT_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_PRINT_FAIL: null,
  RECALL_CONTAINER_OPERATION_REMOVE_REQUEST: null,
  RECALL_CONTAINER_OPERATION_REMOVE_CONFIRM: null,
  RECALL_CONTAINER_OPERATION_REMOVE_CANCEL: null,
  RECALL_CONTAINER_OPERATION_REMOVE_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_REMOVE_FAIL: null,
  RECALL_CONTAINER_OPERATION_HISTORY_REQUEST: null,
  RECALL_CONTAINER_OPERATION_HISTORY_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_HISTORY_FAIL: null,
  RECALL_CONTAINER_OPERATION_CHECK_SIGN_REQUEST: null,
  RECALL_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS: null,
  RECALL_CONTAINER_OPERATION_CHECK_SIGN_FAIL: null,

  RECALL_CONTAINER_CLOSE_DOCUMENT_TOP: null,

  RECALL_CONTAINER_SIGNATURE_PRINT_REQUEST: null,
  RECALL_CONTAINER_SIGNATURE_PRINT_SUCCESS: null,
  RECALL_CONTAINER_SIGNATURE_PRINT_FAIL: null,

  RECALL_CONTAINER_SIGNATURE_DOWNLOAD_REQUEST: null,
  RECALL_CONTAINER_SIGNATURE_DOWNLOAD_SUCCESS: null,
  RECALL_CONTAINER_SIGNATURE_DOWNLOAD_FAIL: null
});

export const LOCAL_REDUCER = 'RecallContainerReducer';

export const LOCAL_ROUTER_ALIASES = {
  PARENT_DOC: '/{:parentDocType}/{:parentDocId}',
  RECALL: '/{:parentDocType}/{:parentDocId}/recalls/{:id}',
  RECALL_NEW: '/{:parentDocType}/{:parentDocId}/recalls/new'
};

export const COMPONENT_STYLE_NAME = 'b-recall-container';
