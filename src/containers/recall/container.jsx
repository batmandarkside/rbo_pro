import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Loader from '@rbo/components/lib/loader/Loader';
import { getDocStatusClassNameMode, getScrollTopByDataId, smoothScrollTo } from 'utils';
import RboDocument, {
  RboDocumentPanel,
  RboDocumentOperations,
  RboDocumentContent,
  RboDocumentMain,
  RboDocumentTop,
  RboDocumentTopHistory,
  RboDocumentTopSignatures,
  RboDocumentHead,
  RboDocumentHeadStatus,
  RboDocumentHeadTabs,
  RboDocumentBody,
  RboDocumentInfo,
  RboDocumentExtra,
  RboDocumentExtraSection,
  RboDocumentStructure,
  COMPONENT_CONTENT_ELEMENT_SELECTOR as DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR
} from 'components/ui-components/rbo-document-compact';
import RboForm from 'components/ui-components/rbo-form-compact';
import { getFieldElementSelectorByIds } from 'components/ui-components/rbo-form-compact/utils';
import { SectionReason } from './sections';
import { COMPONENT_STYLE_NAME } from './constants';
import './style.css';

class RecallContainer extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),

    isDataError: PropTypes.bool,
    isDataLoading: PropTypes.bool,
    isEditable: PropTypes.bool.isRequired,

    documentTabs: PropTypes.instanceOf(List).isRequired,
    documentStructure: PropTypes.instanceOf(List).isRequired,
    documentHistory: PropTypes.instanceOf(Map).isRequired,
    documentSignatures: PropTypes.instanceOf(Map).isRequired,
    documentValues: PropTypes.instanceOf(Map).isRequired,
    documentData: PropTypes.instanceOf(Map).isRequired,
    documentDictionaries: PropTypes.instanceOf(Map).isRequired,
    formSections: PropTypes.instanceOf(Map).isRequired,
    formWarnings: PropTypes.instanceOf(Map).isRequired,
    formErrors: PropTypes.instanceOf(Map).isRequired,
    operations: PropTypes.instanceOf(List).isRequired,

    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    changeTabAction: PropTypes.func.isRequired,
    fieldFocusAction: PropTypes.func.isRequired,
    fieldBlurAction: PropTypes.func.isRequired,
    changeDataAction: PropTypes.func.isRequired,
    sectionCollapseToggleAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    closeDocumentTopAction: PropTypes.func.isRequired,
    signaturePrintAction: PropTypes.func.isRequired,
    signatureDownloadAction: PropTypes.func.isRequired
  };

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.mountAction(params);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search)
        && nextProps.history.action !== 'REPLACE') {
      this.props.mountAction(nextProps.match.params);
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  handleTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleFormSectionCollapseToggle = ({ sectionId, isCollapsed }) => {
    this.props.sectionCollapseToggleAction(sectionId, isCollapsed);
  };

  handleFocus = ({ fieldId }) => {
    this.props.fieldFocusAction(fieldId);
  };

  handleBlur = ({ fieldId }) => {
    this.props.fieldBlurAction(fieldId);
  };

  handleChange = ({ fieldId, fieldValue }) => {
    this.props.changeDataAction(fieldId, fieldValue);
  };

  handleStructureTabClick = ({ tabId }) => {
    this.props.changeTabAction(tabId);
  };

  handleStructureSectionClick = ({ tabId, sectionId }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    setTimeout(() => {
      const fieldId = parentTab.get('sections').find(section => section.get('id') === sectionId).get('firstFieldId');
      smoothScrollTo(
        getScrollTopByDataId(sectionId, DOCUMENT_COMPONENT_CONTENT_ELEMENT_SELECTOR),
        () => {
          document.querySelector(`#${fieldId}`) &&
          setTimeout(() => { document.querySelector(`#${fieldId}`).focus(); }, 0);
        }
      );
    }, 0);
  };

  handleStructureErrorClick = ({ tabId, fieldsIds }) => {
    const parentTab = this.props.documentStructure.find(tab => tab.get('id') === tabId);
    if (!parentTab.get('isActive')) this.props.changeTabAction(tabId);
    const fieldElementSelector = getFieldElementSelectorByIds(fieldsIds);
    fieldElementSelector && setTimeout(() => { fieldElementSelector.focus(); }, 0);
  };

  render() {
    const {
      isDataError,
      isDataLoading,
      isEditable,
      documentTabs,
      documentHistory,
      documentSignatures,
      documentValues,
      documentData,
      documentStructure,
      documentDictionaries,
      formSections,
      formWarnings,
      formErrors,
      operations,
      operationAction,
      closeDocumentTopAction,
      signaturePrintAction,
      signatureDownloadAction
    } = this.props;

    const sectionsProps = {
      isEditable,
      documentValues,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      onCollapseToggle: this.handleFormSectionCollapseToggle,
      onFieldFocus: this.handleFocus,
      onFieldBlur: this.handleBlur,
      onFieldChange: this.handleChange,
    };

    return (
      <div className={COMPONENT_STYLE_NAME}>
        <div className={`${COMPONENT_STYLE_NAME}__inner`}>
          {isDataLoading && <Loader centered />}
          {!isDataLoading && !isDataError &&
          <RboDocument>
            <RboDocumentPanel>
              <RboDocumentOperations
                operations={operations}
                operationAction={operationAction}
              />
            </RboDocumentPanel>
            <RboDocumentContent>
              <RboDocumentMain>
                {(documentHistory.get('isVisible') || documentSignatures.get('isVisible')) &&
                <RboDocumentTop onCloseClick={closeDocumentTopAction}>
                  {documentHistory.get('isVisible') &&
                  <RboDocumentTopHistory
                    items={documentHistory.get('items')}
                  />
                  }
                  {documentSignatures.get('isVisible') &&
                  <RboDocumentTopSignatures
                    items={documentSignatures.get('items')}
                    onSignaturePrintClick={signaturePrintAction}
                    onSignatureDownloadClick={signatureDownloadAction}
                  />
                  }
                </RboDocumentTop>
                }
                <RboDocumentHead title={documentValues.get('title')}>
                  <RboDocumentHeadStatus
                    statusClassName={getDocStatusClassNameMode(documentData.get('status'))}
                    status={documentData.get('status')}
                  />
                  <RboDocumentHeadTabs
                    tabs={documentTabs}
                    onTabClick={this.handleTabClick}
                  />
                </RboDocumentHead>
                <RboDocumentBody>
                  {!!documentStructure.find(tab => tab.get('id') === 'main').get('isActive') &&
                  <RboDocumentInfo items={documentValues.get('mainInfo')} />
                  }
                  {!!documentStructure.find(tab => tab.get('id') === 'main').get('isActive') &&
                  <RboForm initialFocusFieldId="reason">
                    {formSections.getIn(['reason', 'isVisible']) &&
                    <SectionReason
                      {...sectionsProps}
                      isCollapsible={formSections.getIn(['reason', 'isCollapsible'])}
                      isCollapsed={formSections.getIn(['reason', 'isCollapsed'])}
                    />
                    }
                  </RboForm>
                  }
                  {!!documentStructure.find(tab => tab.get('id') === 'info').get('isActive') &&
                  <RboDocumentInfo items={documentValues.get('bankInfo')} />
                  }
                </RboDocumentBody>
              </RboDocumentMain>
              <RboDocumentExtra>
                <RboDocumentExtraSection title="Структура документа">
                  <RboDocumentStructure
                    structure={documentStructure}
                    onTabClick={this.handleStructureTabClick}
                    onSectionClick={this.handleStructureSectionClick}
                    onErrorClick={this.handleStructureErrorClick}
                    isErrorClickable={isEditable}
                  />
                </RboDocumentExtraSection>
              </RboDocumentExtra>
            </RboDocumentContent>
          </RboDocument>
          }
        </div>
      </div>
    );
  }
}

export default RecallContainer;
