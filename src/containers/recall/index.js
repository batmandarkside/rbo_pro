import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import mapStateToProps from './selectors';
import * as Actions from './actions';
import saga from './sagas';
import reducer from './reducer';
import RecallContainer from './container';


const withConnect = connect(mapStateToProps, Actions);
const withReducer = injectReducer({ key: 'RecallContainerReducer', reducer });
const withSaga = injectSaga({ key: 'recallContainerSagas', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RecallContainer);
