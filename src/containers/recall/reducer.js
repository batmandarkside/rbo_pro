import { fromJS } from 'immutable';
import { DOC_STATUSES } from 'constants';
import { getIsDocEditable } from 'utils';
import { ACTIONS } from './constants';
import { createDocInfo  } from './utils';

const initialDocumentTabs = [
  {
    id: 'main',
    title: 'Основные поля',
    isActive: true
  },
  {
    id: 'info',
    title: 'Информация из банка',
    isActive: false
  }
];

const initialFormSections = {
  reason: {
    title: 'Причина отзыва',
    isVisible: true,
    isCollapsible: false,
    order: 0
  }
};

const initialDictionaries = {
  organizations: {
    items: []
  }
};

const initialOperations = [
  {
    id: 'back',
    title: 'Назад',
    icon: 'back',
    disabled: false,
    progress: false
  },
  {
    id: 'save',
    title: 'Сохранить',
    icon: 'save',
    disabled: false,
    progress: false
  },
  {
    id: 'signAndSend',
    title: 'Подписать и отправить',
    icon: 'sign-and-send',
    disabled: false,
    progress: false
  },
  {
    id: 'sign',
    title: 'Подписать',
    icon: 'sign',
    disabled: false,
    progress: false
  },
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false
  },
  {
    id: 'remove',
    title: 'Удалить',
    icon: 'remove',
    disabled: false,
    progress: false
  },
  {
    id: 'send',
    title: 'Отправить',
    icon: 'send',
    disabled: false,
    progress: false
  },
  {
    id: 'history',
    title: 'История статустов',
    icon: 'history',
    disabled: false,
    progress: false
  },
  {
    id: 'checkSign',
    title: 'Проверить подпись',
    icon: 'check-sign',
    disabled: false,
    progress: false
  }
];

export const initialDocumentHistory = {
  isVisible: false,
  items: []
};

export const initialDocumentSignatures = {
  isVisible: false,
  items: []
};

const initialState = fromJS({
  isMounted: false,
  isDataError: false,
  isDataLoading: true,
  isEditable: false,
  documentTabs: initialDocumentTabs,
  documentHistory: initialDocumentHistory,
  documentSignatures: initialDocumentSignatures,
  documentData: {},
  documentErrors: [],
  documentDictionaries: initialDictionaries,
  formSections: initialFormSections,
  activeFormField: null,
  operations: initialOperations
});

export const mount = () => initialState.set('isMounted', true);

export const unmount = () => initialState.set('isMounted', false);

export const getDataRequest = state => state.merge({
  isDataLoading: true
});

export const getDataSuccess = (state, { documentData, documentErrors, documentDictionaries }) => {
  const status = documentData.status || DOC_STATUSES.NEW;
  const docInfo = documentData.docInfo || createDocInfo(documentData.parentDoc);
  const isEditable = getIsDocEditable(status);

  return state.mergeDeep({
    isDataLoading: false,
    isEditable,
    documentData: { ...documentData, status, docInfo },
    documentErrors: documentErrors && documentErrors.errors,
    documentDictionaries
  });
};

export const getDataFail = state => state.merge({
  isDataError: true,
  isDataLoading: false
});

export const saveDataSuccess = (state, { recordID, status, allowedSmActions, errors }) => state.merge({
  documentData: state.get('documentData').merge({
    recordID,
    status,
    allowedSmActions
  }),
  documentErrors: errors
});

export const saveDataFail = (state, { error }) => state.merge({
  documentErrors: error.name === 'RBO Controls Error' ? fromJS(error.stack) : state.get('documentErrors')
});

export const fieldFocus = (state, { fieldId }) => state.set('activeFormField', fieldId);

export const fieldBlur = state => state.set('activeFormField', null);

export const sectionCollapseToggle = (state, { sectionId, isCollapsed }) =>
  state.setIn(['formSections', sectionId, 'isCollapsed'], isCollapsed);

export const changeData = (state, { fieldId, fieldValue }) => state.setIn(['documentData', fieldId], fieldValue);

export const changeTab = (state, { tabId }) => state.merge({
  documentTabs: state.get('documentTabs').map(tab => tab.set('isActive', tab.get('id') === tabId))
});

export const operationSaveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSaveFinish = state => state.mergeDeep({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'save':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSignAndSendRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSignAndSendSigningSuccess = (state, { documentData }) => state.merge({
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignAndSendSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignAndSendFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'signAndSend':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSignRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSignSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSignFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'sign':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationPrintRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'print' ? operation.set('progress', true) : operation))
});

export const operationPrintFinish = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'print' ? operation.set('progress', false) : operation))
});

export const operationRemoveRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationRemoveFinish = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'remove':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationSendRequest = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', true);
      default:
        return operation.set('disabled', true);
    }
  })
});

export const operationSendSuccess = (state, { documentData }) => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  }),
  documentData: state.get('documentData').merge(documentData),
  isEditable: getIsDocEditable(documentData.status,
    documentData.allowedSmActions && documentData.allowedSmActions.fromArchive)
});

export const operationSendFail = state => state.merge({
  operations: state.get('operations').map((operation) => {
    switch (operation.get('id')) {
      case 'send':
        return operation.set('progress', false);
      default:
        return operation.set('disabled', false);
    }
  })
});

export const operationHistoryRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', true) : operation))
});

export const operationHistorySuccess = (state, payload) => state.merge({
  documentHistory: {
    isVisible: true,
    items: payload
  },
  documentSignatures: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});

export const operationHistoryFail = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'history' ? operation.set('progress', false) : operation))
});

export const operationCheckSignRequest = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', true) : operation))
});

export const operationCheckSignSuccess = (state, payload) => state.merge({
  documentSignatures: {
    isVisible: true,
    items: payload
  },
  documentHistory: {
    isVisible: false,
    items: []
  },
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', false) : operation))
});

export const operationCheckSignFail = state => state.merge({
  operations: state.get('operations')
    .map(operation => (operation.get('id') === 'checkSign' ? operation.set('progress', false) : operation))
});

export const operationCloseDocumentTop = state => state.merge({
  documentHistory: {
    isVisible: false,
    items: []
  },
  documentSignatures: {
    isVisible: false,
    items: []
  }
});

function RecallContainerReducer(state = initialState, action) {
  const isMounted = state.get('isMounted');

  switch (action.type) {
    case ACTIONS.RECALL_CONTAINER_MOUNT:
      return mount();

    case ACTIONS.RECALL_CONTAINER_UNMOUNT:
      return unmount();

    case ACTIONS.RECALL_CONTAINER_GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.RECALL_CONTAINER_GET_DATA_SUCCESS:
      return isMounted ? getDataSuccess(state, action.payload) : state;

    case ACTIONS.RECALL_CONTAINER_GET_DATA_FAIL:
      return isMounted ? getDataFail(state) : state;

    case ACTIONS.RECALL_CONTAINER_SAVE_DATA_SUCCESS:
      return saveDataSuccess(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_SAVE_DATA_FAIL:
      return saveDataFail(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_FIELD_FOCUS:
      return fieldFocus(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_FIELD_BLUR:
      return fieldBlur(state);

    case ACTIONS.RECALL_CONTAINER_SECTION_COLLAPSE_TOGGLE:
      return sectionCollapseToggle(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_CHANGE_DATA:
      return changeData(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_CHANGE_TAB:
      return changeTab(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SAVE_REQUEST:
      return operationSaveRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SAVE_SUCCESS:
    case ACTIONS.RECALL_CONTAINER_OPERATION_SAVE_FAIL:
      return operationSaveFinish(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_REQUEST:
      return operationSignAndSendRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SIGNING_SUCCESS:
      return operationSignAndSendSigningSuccess(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_SUCCESS:
      return operationSignAndSendSuccess(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_CANCEL:
    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_AND_SEND_FAIL:
      return operationSignAndSendFail(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_REQUEST:
      return operationSignRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_SUCCESS:
      return operationSignSuccess(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_CANCEL:
    case ACTIONS.RECALL_CONTAINER_OPERATION_SIGN_FAIL:
      return operationSignFail(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationPrintRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.RECALL_CONTAINER_OPERATION_PRINT_FAIL:
      return operationPrintFinish(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_REMOVE_REQUEST:
      return operationRemoveRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_REMOVE_SUCCESS:
    case ACTIONS.RECALL_CONTAINER_OPERATION_REMOVE_CANCEL:
    case ACTIONS.RECALL_CONTAINER_OPERATION_REMOVE_FAIL:
      return operationRemoveFinish(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SEND_REQUEST:
      return operationSendRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SEND_SUCCESS:
      return operationSendSuccess(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_OPERATION_SEND_FAIL:
      return operationSendFail(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_HISTORY_REQUEST:
      return operationHistoryRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_HISTORY_SUCCESS:
      return operationHistorySuccess(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_OPERATION_HISTORY_FAIL:
      return operationHistoryFail(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_CHECK_SIGN_REQUEST:
      return operationCheckSignRequest(state);

    case ACTIONS.RECALL_CONTAINER_OPERATION_CHECK_SIGN_SUCCESS:
      return operationCheckSignSuccess(state, action.payload);

    case ACTIONS.RECALL_CONTAINER_OPERATION_CHECK_SIGN_FAIL:
      return operationCheckSignFail(state);

    case ACTIONS.RECALL_CONTAINER_CLOSE_DOCUMENT_TOP:
      return operationCloseDocumentTop(state);

    default:
      return state;
  }
}

export default RecallContainerReducer;
