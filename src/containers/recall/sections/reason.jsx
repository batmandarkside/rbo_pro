import React from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import {
  RboFormSection,
  RboFormBlockset,
  RboFormBlock,
  RboFormRowset,
  RboFormRow,
  RboFormCell,
  RboFormFieldTextarea,
  RboFormFieldValue
} from 'components/ui-components/rbo-form-compact';

const RecallSectionReason = (props) => {
  const {
    isEditable,
    documentValues,
    documentData,
    formWarnings,
    formErrors,
    onFieldFocus,
    onFieldBlur,
    onFieldChange
  } = props;

  return (
    <RboFormSection
      id="reason"
      title="Причина отзыва"
    >
      <RboFormBlockset>
        <RboFormBlock isWide>
          <RboFormRowset>
            <RboFormRow>
              <RboFormCell>
                {isEditable &&
                <RboFormFieldTextarea
                  id="reason"
                  value={documentData.get('reason')}
                  isWarning={formWarnings.get('reason')}
                  isError={formErrors.get('reason')}
                  onFocus={onFieldFocus}
                  onBlur={onFieldBlur}
                  onChange={onFieldChange}
                />
                }
                {!isEditable &&
                <RboFormFieldValue
                  id="reason"
                  value={documentValues.get('reason')}
                  isWarning={formWarnings.get('reason')}
                  isError={formErrors.get('reason')}
                />
                }
              </RboFormCell>
            </RboFormRow>
          </RboFormRowset>
        </RboFormBlock>
      </RboFormBlockset>
    </RboFormSection>
  );
};

RecallSectionReason.propTypes = {
  isEditable: PropTypes.bool,
  documentValues: PropTypes.instanceOf(Map).isRequired,
  documentData: PropTypes.instanceOf(Map).isRequired,
  formWarnings: PropTypes.instanceOf(Map).isRequired,
  formErrors: PropTypes.instanceOf(Map).isRequired,
  onFieldFocus: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired
};

export default RecallSectionReason;
