import { createSelector } from 'reselect';
import { Map } from 'immutable';
import moment from 'moment-timezone';
import { DOC_STATUSES, SIGN_VALIDATION_STATUSES } from 'constants';
import {
  getDocStatusClassNameMode,
  getDocFormErrors,
  getMappingDocSectionsAndErrors,
  getSignType
} from 'utils';
import { LOCAL_REDUCER } from './constants';
import {
  documentFieldsMap,
  documentSectionsFieldsMap,
  documentErrorsMap,
  getMainInfo,
  getBankInfo
} from './utils';

const isDataErrorSelector = state => state.getIn([LOCAL_REDUCER, 'isDataError']);
const isDataLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isDataLoading']);
const isEditableSelector = state => state.getIn([LOCAL_REDUCER, 'isEditable']);
const documentTabsSelector = state => state.getIn([LOCAL_REDUCER, 'documentTabs']);
const documentHistorySelector = state => state.getIn([LOCAL_REDUCER, 'documentHistory']);
const documentSignaturesSelector = state => state.getIn([LOCAL_REDUCER, 'documentSignatures']);
const documentDataSelector = state => state.getIn([LOCAL_REDUCER, 'documentData']);
const documentErrorsSelector = state => state.getIn([LOCAL_REDUCER, 'documentErrors']);
const documentDictionariesSelector = state => state.getIn([LOCAL_REDUCER, 'documentDictionaries']);
const formSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'formSections']);
const activeFormFieldSelector = state => state.getIn([LOCAL_REDUCER, 'activeFormField']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);

const documentHistoryCreatedSelector = createSelector(
  documentHistorySelector,
  documentHistory => documentHistory.set('items', documentHistory.get('items')
    .reverse()
    .map(item => Map({
      date: item.get('date') ? moment(item.get('date')).format('DD.MM.YYYY HH:mm:ss') : '',
      user: item.get('user'),
      endState: item.get('endState'),
      statusClassName: getDocStatusClassNameMode(item.get('endState'))
    }))
  )
);

const documentSignaturesCreatedSelector = createSelector(
  documentSignaturesSelector,
  documentSignatures => documentSignatures.set('items', documentSignatures.get('items').map(item => Map({
    signId: item.get('signId'),
    signDateTime: item.get('signDateTime') ? moment(item.get('signDateTime')).format('DD.MM.YYYY HH:mm:ss') : '',
    userLogin: item.get('userLogin'),
    userName: item.get('userName'),
    userPosition: item.get('userPosition'),
    signType: getSignType(item.get('signType')),
    signValid: item.get('signValid') ? SIGN_VALIDATION_STATUSES.VALID : SIGN_VALIDATION_STATUSES.INVALID,
    signHash: item.get('signHash')
  })))
);

const documentValuesCreatedSelector = createSelector(
  documentDataSelector,
  documentDictionariesSelector,
  isEditableSelector,
  (documentData, documentDictionaries) => {
    const docNumber = documentData.get('docNumber') || '';
    const docDate = documentData.get('docDate') ? moment(documentData.get('docDate')).format('DD.MM.YYYY') : '';
    const title = `Отзыв${docNumber ? ` № ${docNumber}` : ''}${docDate ? ` от ${docDate}` : ''}`;
    return Map({
      id: documentData.get('recordID'),
      title,
      status: documentData.get('status'),
      reason: documentData.get('reason', ''),
      mainInfo: getMainInfo(documentData, documentDictionaries),
      bankInfo: getBankInfo(documentData)
    });
  }
);

const documentDataCreatedSelector = createSelector(
  documentDataSelector,
  documentData => Map({
    id: documentData.get('recordID'),
    status: documentData.get('status'),
    docNumber: documentData.get('docNumber'),
    docDate: documentData.get('docDate'),
    reason: documentData.get('reason', '')
  })
);

const documentErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  activeFormFieldSelector,
  (documentErrors, activeFormField) => documentErrors
    .map(error => error.merge({
      fieldId: documentErrorsMap[error.get('id')] && documentErrorsMap[error.get('id')][0],
      fieldsIds: documentErrorsMap[error.get('id')],
      isActive: documentErrorsMap[error.get('id')] &&
        !!documentErrorsMap[error.get('id')].find(i => i === activeFormField)
    }))
    .filter(error => !!error.get('fieldId'))
    .sort((a, b) => documentFieldsMap.indexOf(a.get('fieldId')) - documentFieldsMap.indexOf(b.get('fieldId')))
);

const documentStructureCreatedSelector = createSelector(
  documentTabsSelector,
  formSectionsSelector,
  documentErrorsCreatedSelector,
  activeFormFieldSelector,
  (documentTabs, formSections, documentErrors, activeFormField) => documentTabs.map((tab) => {
    switch (tab.get('id')) {
      case 'main':
        return tab.set('sections',
          getMappingDocSectionsAndErrors(formSections, documentSectionsFieldsMap, documentErrors, activeFormField));
      default:
        return tab;
    }
  })
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['1'])
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => getDocFormErrors(documentErrors, documentErrorsMap, ['2', '3'])
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  documentDataSelector,
  (operations, documentData) => {
    const status = documentData.get('status');
    const allowedSmActions = documentData.get('allowedSmActions', Map());
    return operations.filter((operation) => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'back':
          return true;
        case 'save':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save');
        case 'signAndSend':
          return status === DOC_STATUSES.NEW || allowedSmActions.get('save') || allowedSmActions.get('sign');
        case 'sign':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('sign');
        case 'print':
          return status !== DOC_STATUSES.NEW;
        case 'remove':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('delete');
        case 'send':
          return status !== DOC_STATUSES.NEW && allowedSmActions.get('send');
        case 'history':
          return status !== DOC_STATUSES.NEW;
        case 'checkSign':
          return !![
            DOC_STATUSES.DELIVERED,
            DOC_STATUSES.PROCESSED,
            DOC_STATUSES.EXECUTED,
            DOC_STATUSES.DECLINED,
            DOC_STATUSES.RECALLED,
            DOC_STATUSES.INVALID_PROPS,
            DOC_STATUSES.SIGNED,
            DOC_STATUSES.ACCEPTED,
            DOC_STATUSES.UNLOADED,
            DOC_STATUSES.PARTLY_SIGNED,
            DOC_STATUSES.INVALID_SIGN
          ].find(v => v === status);
        default:
          return false;
      }
    });
  }
);

const mapStateToProps = state => ({
  isDataError: isDataErrorSelector(state),
  isDataLoading: isDataLoadingSelector(state),
  isEditable: isEditableSelector(state),
  documentTabs: documentTabsSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentHistory: documentHistoryCreatedSelector(state),
  documentSignatures: documentSignaturesCreatedSelector(state),
  documentValues: documentValuesCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentDictionaries: documentDictionariesSelector(state),
  formSections: formSectionsSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  operations: operationsCreatedSelector(state)
});

export default mapStateToProps;
