import moment from 'moment-timezone';
import numeral from 'numeral';

export const documentFieldsMap = ['reason'];

export const documentSectionsFieldsMap = {
  reason: ['reason', 'docNumber', 'docDate', 'docInfo']
};

export const documentErrorsMap = {
  '1.1.1': ['reason', 'docNumber'],
  '1.1.2': ['reason', 'docNumber'],
  '1.1.3': ['reason', 'docNumber'],
  '1.2.1': ['reason', 'docDate'],
  '1.3.1': ['reason', 'docInfo'],
  '2.1': ['reason', 'docInfo'],
  '2.2': ['reason', 'docInfo'],
  '2.3': ['reason', 'reason'],
  '2.3.1': ['reason', 'reason'],
  '2.4': ['reason', 'docInfo'],
  '2.6': ['reason', 'docInfo'],
  '4.1': ['reason', 'docDate'],
  '12.1': ['reason', 'docDate']
};

export const getBankInfo = documentData => (
  !documentData.get('acceptDate') &&
  !documentData.get('bankMessage') &&
  !documentData.get('bankAuthor')
    ? null
    : [
      {
        label: 'Дата обработки',
        value: documentData.get('acceptDate') ? moment(documentData.get('acceptDate')).format('DD.MM.YYYY') : ''
      },
      { label: 'Сообщение', value: documentData.get('bankMessage') },
      { label: 'Ответственный исполнитель', value: documentData.get('bankAuthor') }
    ]
);

export const createDocInfo = (data) => {
  if (!data) return '';
  const docDate = data.docDate ? moment(data.docDate).format('DD.MM.YYYY') : '';
  const docAmount = !isNaN(data.docAmount) ? numeral(data.docAmount).format('0,0.00') : '0.00';
  return `${data.docName}:\nНомер ${data.docNumber} от ${docDate}\nНа сумму ${docAmount} руб.\nCо счета № ${data.payerAccount}\nНа счет № ${data.receiverAccount}`;
};

export const getMainInfo = (documentData, documentDictionaries) => {
  const result = [];
  result.push({ label: 'Отзываемый документ', value: documentData.get('docInfo') || '—' });
  const organization = documentDictionaries.getIn(['organizations', 'items'])
    .find(item => item.get('id') === documentData.getIn(['parentDoc', 'orgId']));
  if (organization) {
    result.push({ label: 'Организация', value: organization.get('shortName') || '—' });
    result.push({ label: 'ИНН', value: organization.get('inn') || '—' });
  }
  return result;
};
