import { ACTIONS } from './constants';

// test
export const resetNotifyAction = params => ({
  type: ACTIONS.CRYPTO_RESET_NOTIFY_REQUEST,
  payload: params
});

/**
 * default values
 * @param params
 */
export const getValuesUlkAction = params => ({
  type: ACTIONS.CRYPTO_PRO_GET_ULK_REQUEST,
  payload: params || {}
});


export const getCertListAction = params => ({
  type: ACTIONS.CRYPTO_PRO_GET_CERT_LIST_REQUEST,
  payload: params
});

export const changeUlkAction = value => ({
  type: ACTIONS.CRYPTO_PRO_CHANGE_ULK,
  payload: value
});

export const createNewCertAction = params => ({
  type: ACTIONS.CRYPTO_CREATE_NEW_CERT_REQUEST,
  payload: params
});

/**
 * показать модальное окно на перевыпуск активного сертификата
 * @param item
 */
export const reissueCertStartAction = item => ({
  type: ACTIONS.CRYPTO_PRO_REISSUE_CERT_START,
  payload: item
});


export const reissueCertFinishAction = () => ({
  type: ACTIONS.CRYPTO_PRO_REISSUE_CERT_FINISH
});


export const reissueCertAction = id => ({
  type: ACTIONS.CRYPTO_PRO_REISSUE_REQUEST,
  payload: { id }
});

/**
 * изменить url для перевыпуска сертификата
 * @param item
 */
export const changeUrlForReissueCertAction = item => ({
  type: ACTIONS.CRYPTO_CHANGE_URL_FOR_REISSUE_CERT_REQUEST,
  payload: item
});

export const printNewCertAction = id => ({
  type: ACTIONS.CRYPTO_OPERATION_PRINT_REQUEST,
  payload: {
    id
  }
});

export const onDownloadCertAction = id => ({
  type: ACTIONS.CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_REQUEST,
  payload: {
    id
  }
});

/**
 *
 * @param {string} id
 */
export const deleteCertAction = (id, requestType) => ({
  type: ACTIONS.CRYPTO_OPERATION_DELETE_REQUEST,
  payload: {
    id,
    requestType
  }
});

/**
 *
 * @param id
 * @param requestType
 */
export const sendToBankCertAction = (id, requestType)  => ({
  type: ACTIONS.CRYPTO_OPERATION_SEND_REQUEST,
  payload: {
    id,
    requestType
  }
});


export const sendToBankNewCertAction = certData => ({
  type: ACTIONS.CRYPTO_SEND_TO_BANK_NEW_CERT_REQUEST,
  payload: certData
});

/**
 * подпись сертификата старым средством подписи
 * нужно для старых сертификатов созданных в эльбрусе
 * @param item
 * @param item
 * @param id
 */
export const signCertAction = (item, id) => ({
  type: ACTIONS.CRYPTO_OPERATION_SIGN_OLD_CERT_REQUEST,
  payload: {
    item,
    id
  }
});

export const createPKCS10RequestAction = params => ({
  type: ACTIONS.CRYPTO_CREATE_PKCS10_REQUEST,
  payload: params
});

export const resetFormCertAction = () => ({
  type: ACTIONS.CRYPTO_PRO_RESET_FORM
});


export const sortCertAction = dateValue => ({
  type: ACTIONS.CRYPTO_PRO_FILTERS_SORT,
  payload: {
    dateValue
  }
});

export const changeStatusAction = value => ({
  type: ACTIONS.CRYPTO_PRO_FILTERS_CHANGE_STATUS,
  payload: {
    value
  }
});


export const changeSortValueAction = value => ({
  type: ACTIONS.CRYPTO_PRO_FILTERS_CHANGE_SORT_VALUE,
  payload: {
    value
  }
});

export const clearStatusAction = () => ({
  type: ACTIONS.CRYPTO_PRO_FILTERS_CLEAR_STATUS
});
