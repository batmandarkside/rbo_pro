import keyMirror from 'keymirror';
import { COLORS } from 'assets/styles/style-vars.js';


export const ACTIONS = keyMirror({
  CRYPTO_RESET_NOTIFY_REQUEST: null, // test
  CRYPTO_PRO_GET_ULK_REQUEST: null,
  CRYPTO_PRO_GET_ULK_SUCCESS: null,
  CRYPTO_PRO_GET_ULK_FAIL: null,

  CRYPTO_PRO_GET_ULK_FOR_REGENERATE_REQUEST: null,
  CRYPTO_PRO_GET_ULK_FOR_REGENERATE_SUCCESS: null,
  CRYPTO_PRO_GET_ULK_FOR_REGENERATE_FAIL: null,

  CRYPTO_PRO_CHANGE_ULK: null,
  CRYPTO_PRO_CHANGE_FIELDS_ERROR: null,

  CRYPTO_CREATE_NEW_CERT_REQUEST: null,
  CRYPTO_CREATE_NEW_CERT_SUCCESS: null,
  CRYPTO_CREATE_NEW_CERT_FAIL: null,

  CRYPTO_OPERATION_DELETE_REQUEST: null,
  CRYPTO_OPERATION_DELETE_SUCCESS: null,
  CRYPTO_OPERATION_DELETE_FAIL: null,

  CRYPTO_OPERATION_REGENERATE_REQUEST: null,
  CRYPTO_OPERATION_REGENERATE_SUCCESS: null,
  CRYPTO_OPERATION_REGENERATE_FAIL: null,

  CRYPTO_PRO_REISSUE_REQUEST: null,
  CRYPTO_PRO_REISSUE_SUCCESS: null,
  CRYPTO_PRO_REISSUE_FAIL: null,

  CRYPTO_CHANGE_URL_FOR_REISSUE_CERT_REQUEST: null,

  CRYPTO_CHANGE_URL_FOR_REISSUE_CERT: null,

  CRYPTO_OPERATION_SEND_REQUEST: null,
  CRYPTO_OPERATION_SEND_SUCCESS: null,
  CRYPTO_OPERATION_SEND_FAIL: null,

  CRYPTO_OPERATION_PRINT_REQUEST: null,
  CRYPTO_OPERATION_PRINT_SUCCESS: null,
  CRYPTO_OPERATION_PRINT_FAIL: null,

  CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_REQUEST: null,
  CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_SUCCESS: null,
  CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_FAIL: null,

  CRYPTO_SEND_TO_BANK_NEW_CERT_REQUEST: null,
  CRYPTO_SEND_TO_BANK_NEW_CERT_SUCCESS: null,
  CRYPTO_SEND_TO_BANK_NEW_CERT_FAIL: null,

  CRYPTO_OPERATION_SIGN_OLD_CERT_REQUEST: null,
  CRYPTO_OPERATION_SIGN_OLD_CERT_SUCCESS: null,
  CRYPTO_OPERATION_SIGN_OLD_CERT_FAIL: null,

  CRYPTO_PRO_GET_CERT_LIST_REQUEST: null,
  CRYPTO_PRO_GET_CERT_LIST_SUCCESS: null,
  CRYPTO_PRO_GET_CERT_LIST_FAIL: null,

  CRYPTO_PRO_CHANGE_CRYPTO_PRO_NAME_VALUES: null,
  CRYPTO_PRO_RESET_FORM: null,

  CRYPTO_PRO_FILTERS_CHANGE_STATUS: null,
  CRYPTO_PRO_FILTERS_CLEAR_STATUS: null,
  CRYPTO_PRO_FILTERS_SORT: null,
  CRYPTO_PRO_FILTERS_CHANGE_SORT_VALUE: null,

  CRYPTO_CREATE_PKCS10_REQUEST: null,
  CRYPTO_CREATE_PKCS10_SUCCESS: null,
  CRYPTO_CREATE_PKCS10_FAIL: null,

  // смена url
  CRYPTO_PRO_REISSUE_CERT_START: null,
  CRYPTO_PRO_REISSUE_CERT_FINISH: null,

  // запуск саги, получение данных, подпись
  CRYPTO_REISSUE_START: null,
  CRYPTO_REISSUE_STOP: null
});

export const DATA_LOC = keyMirror({
  DATA_LOC_STATUS_FILTER: null,
  DATA_LOC_DETAIL_MODAL_CERT: null,
  DATA_LOC_MODAL_NEW_CERT: null,
  DATA_LOC_MODAL_REGENERATE_CERT: null,
  DATA_LOC_INSTRUCTION: null,
  DATA_LOC_SORT: null,
  DATA_LOC_BUTTON_CREATE_NEW_CERT: null,
  DATA_LOC_OPERATION_REPEAT: null,
  DATA_LOC_OPERATION_REMOVE: null,
  DATA_LOC_OPERATION_SEND: null,
  DATA_LOC_OPERATION_PRINT: null,
  DATA_LOC_OPERATION_DOWNLOAD: null,
  DATA_LOC_OPERATION_SIGN_OLD_CERT: null
});

export const FORM_NAME_NEW_CERT = 'createNewCert';
export const FIELD_REG_NUM = 'RequestNumber';
export const FIELD_DATE = 'RequestDate';
export const FIELD_ULK = 'ULK';
export const FIELD_CRYPTO_NAME = 'CryptoProfileName';
export const FIELD_EMAIL = 'Email';
export const FIELD_STATUS = 'Status';
export const FIELD_CITY = 'City';
export const FIELD_COUNTRY = 'Country';
export const FIELD_SUBDIVISION = 'Dep';
export const FIELD_ORGANIZATION = 'OrgName';

export const FILTER_SORT_NEW = 'beginNew';
export const FILTER_SORT_OLD = 'beginOld';
export const FILTER_SORT_START_DATE = 'dateBegin';
export const FILTER_SORT_REQUEST_DATE = 'requestDate';

export const ADDITIONAL_STATUS_EXPIRING = 'expiring';
export const ADDITIONAL_STATUS_NEW_REG_REGEN = 'newReqRegen';
export const ADDITIONAL_STATUS_REISSUE = 'reissue';

export const ENTITY_STATUS_REG_REGEN_ONLY = 'reqRegenOnly';
export const ENTITY_STATUS_REG_NEW_ONLY = 'reqNewOnly';
export const ENTITY_STATUS_REG_WITH_CERT = 'reqWithCert';
export const ENTITY_STATUS_CERT_ONLY = 'certOnly';


export const STATUSES_LABEL = {
  active: 'Активен',
  new: 'Создан',
  signed: 'Подписан',
  sendToBank: 'Запрос отправлен в Банк',
  invalidProps: 'Ошибка реквизитов',
  declinedByBank: 'Отвергнут банком',
  invalidSign: 'Подпись неверна',
  inactive: 'Неактивен',
  reissued: 'Перевыпущен',
  expired: 'Истек',
  exported: 'Экспортирован',
  delivered: 'Доставлен',
  deleted: 'Удален',
};

export const STATUSES_COLOR = {
  active: COLORS.colorTrueGreen,
  new: COLORS.colorBlack,
  signed: COLORS.colorBlack,
  sendToBank: COLORS.colorBlack,
  invalidProps: COLORS.colorTomatoRed,
  declinedByBank: COLORS.colorTomatoRed,
  invalidSign: COLORS.colorTomatoRed,
  inactive: COLORS.colorBlack,
  reissued: COLORS.colorBlack,
  expired: COLORS.colorTomatoRed,
  exported: COLORS.colorBlack,
  delivered: COLORS.colorBlack,
  activeExpired: COLORS.colorTangerine,
  deleted: COLORS.colorTomatoRed,
};

export const ALLOW_OPACITY_STATUSES_COLOR = [
  'new',
  'signed',
  'sendToBank',
  'inactive',
  'reissued'
];

export const ALLOW_SHOW_CERT_DETAIL = [
  'expired',
  'reissued',
  'active',
  'inactive'
];


export const ALLOW_SHOW_REQUEST_DETAIL = [
  ENTITY_STATUS_REG_WITH_CERT
];
