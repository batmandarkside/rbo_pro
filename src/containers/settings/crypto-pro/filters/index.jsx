import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'lodash';
import { List, fromJS } from 'immutable';
import { connect } from 'react-redux';
import { InstructionInstallText } from 'components/sign/instruction-install-text';
import CryptoProService from 'services/cryptopro/cryptopro';
import { mapStateToProps } from '../selectors';
import * as LocalActions from '../actions';
import {
  Wraper,
  Plus,
  FiltersBlock,
  StatusValueCrear,
  DropdownBoxExtend,
  StatusValue,
  ButtonCreateNewCert,
  InstructionInstallWrapper,
  EmptyState,
  // ButtonTest
} from './style.js';

import {
  DATA_LOC,
  FILTER_SORT_NEW,
  FILTER_SORT_OLD
} from '../constants';

class Filters extends PureComponent {
  static propTypes = {
    certListFiltered: PropTypes.instanceOf(List).isRequired,
    changeStatusAction: PropTypes.func.isRequired,
    sortCertAction: PropTypes.func.isRequired,
    onHandleCreateCert: PropTypes.func.isRequired,
    clearStatusAction: PropTypes.func.isRequired,
    changeSortValueAction: PropTypes.func.isRequired,
    resetNotifyAction: PropTypes.func, // test
    statusValues: PropTypes.instanceOf(List).isRequired,
    statusValue: PropTypes.object,
    sortValue: PropTypes.object,
    loadCertListLoad: PropTypes.bool,
    sortValues: PropTypes.instanceOf(List).isRequired
  }

  state = {
    isOpenPopupBox: false,
    canCreateCryptoProCert: undefined
  }

  componentDidMount() {
    setTimeout(() => {
      CryptoProService.pluginInfo()
        .then((info) => {
          if (info && info.enabled) {
            this.setState({ canCreateCryptoProCert: true });
          } else {
            this.setState({ canCreateCryptoProCert: false });
          }
        })
        .catch(() => this.setState({ canCreateCryptoProCert: false }));
    }, 1000);
  }

  /**
   * reissue = перевыпустить сертификат
   * Перевыпустить можно только активный сертификат, поэтому меняем статус на active
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    const { reissue, statusValues } = nextProps;
    if (reissue && statusValues.size && !this.changeStatusByUrl) {
      this.changeStatusByUrl = true;
      this.changeStatusAsReissue(statusValues);
    }
  }

  onChangeStatus = (evt, value) => {
    this.props.changeStatusAction(fromJS(value));
  }

  onClearStatus = (evt, value) => {
    evt.stopPropagation();
    this.props.clearStatusAction(value);
  }

  onSortCertAsc = debounce(() => {
    this.props.sortCertAction(FILTER_SORT_NEW);
  }, 200)

  onSortCertDesc = debounce(() => {
    this.props.sortCertAction(FILTER_SORT_OLD);
  }, 200)

  /**
   * направление сортировки
   * @param evt
   * @param value
   */
  onChangeSortDirection = (evt, value) => {
    const v = fromJS(value);
    if (value && v.get('sortDirection') === FILTER_SORT_NEW) {
      this.onSortCertAsc();
    } else if (value && v.get('sortDirection') === FILTER_SORT_OLD) {
      this.onSortCertDesc();
    }
    this.props.changeSortValueAction(v);
  }
  
  // test
  onResetNotification = () => {  
    this.props.resetNotifyAction({
      notificationsCert: {      
        hideNotificationDate: null,
        hideNotifyReissueDate: null,
        hideCertActiveIds: [],
      },
    });
  }

  changeStatusByUrl = false

  /**
   * находим активный статус в списке фильтра статусов
   * @param statusValues
   */
  changeStatusAsReissue = (statusValues) => {
    const { changeStatusAction } = this.props;
    const foundActiveStatus = statusValues.find(s => s.get('mainStatus') === 'active');
    foundActiveStatus && changeStatusAction(foundActiveStatus);
  }

  valueRenderer = (itemDropDown) => {
    const { title } = itemDropDown;
    return (
      <StatusValue>
        {title}
        <StatusValueCrear
          type="close"
          size="16"
          onClick={this.onClearStatus}
        />
      </StatusValue>
    );
  }

  render() {
    const {
      statusValues,
      statusValue,
      onHandleCreateCert,
      sortValue,
      sortValues,
      certListFiltered,
      loadCertListLoad
    } = this.props;
    const { canCreateCryptoProCert } = this.state;

    return (
      <FiltersBlock>
        {canCreateCryptoProCert === false && (
          <InstructionInstallWrapper>
            <InstructionInstallText data-loc={DATA_LOC.DATA_LOC_INSTRUCTION} />
          </InstructionInstallWrapper>
        )}
        <Wraper>
          {!!certListFiltered.size && (
            [
              <DropdownBoxExtend
                key={DATA_LOC.DATA_LOC_STATUS_FILTER}
                isGetFullValue
                maxHeight={350}
                value={!statusValue.size ? null : statusValue.toJS()}
                options={statusValues.toJS()}
                valueRenderer={this.valueRenderer}
                placeholder="Статус"
                onChange={this.onChangeStatus}
                data-loc={DATA_LOC.DATA_LOC_STATUS_FILTER}
                testLocator={DATA_LOC.DATA_LOC_STATUS_FILTER}
              />,
              <DropdownBoxExtend
                key={DATA_LOC.DATA_LOC_SORT}
                isGetFullValue
                maxHeight={350}
                value={sortValue.toJS()}
                options={sortValues.toJS()}
                onChange={this.onChangeSortDirection}
                canCreateCryptoProCert={canCreateCryptoProCert}
                data-loc={DATA_LOC.DATA_LOC_SORT}
                testLocator={DATA_LOC.DATA_LOC_SORT}
              />
            ]
          )}

          {(!certListFiltered.size && !loadCertListLoad) && (
            <EmptyState>У Вас не создано ни одного сертификата</EmptyState>
          )}
          {canCreateCryptoProCert && (
            <ButtonCreateNewCert onClick={onHandleCreateCert} data-loc={DATA_LOC.DATA_LOC_BUTTON_CREATE_NEW_CERT}>
              <Plus /> Запросить сертификат
            </ButtonCreateNewCert>
          )}
          {/* test */}
          {/* <ButtonTest onClick={this.onResetNotification}>
              reset
          </ButtonTest> */}
        </Wraper>
      </FiltersBlock>
    );
  }
}

export default connect(
  mapStateToProps,
  {
    ...LocalActions
  }
)(Filters);
