import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as NotificationAction  from 'dal/notification/actions';
import * as ProfileAction       from 'dal/profile/actions';
import { connect }              from 'react-redux';
import { Map } from 'immutable';
import * as LocalAction         from './actions';
import { mapStateToProps }      from './selectors';
import NewCert from './new-cert';
import ReasonForRegenerate from './reason-regenerate';
import CertList from './user-certifications/list';
import Filters from './filters';
import { DATA_LOC } from './constants';
import {
  Layout,
  ModalExtend
} from './style.js';

class CryptoProSettings extends Component {
  static propTypes = {
    queryPage: PropTypes.shape({
      create: PropTypes.string,
      reissue: PropTypes.string
    }),
    resetFormCertAction: PropTypes.func,
    itemModalRegenerateCertReason: PropTypes.instanceOf(Map).isRequired
  }

  state = {
    showModal: false
  }

  onHandleCreateCert = () => {
    this.setState({ showModal: true });
  }

  onCloseModal = () => {
    const { resetFormCertAction } = this.props;
    this.setState({
      showModal: false
    }, () => {
      resetFormCertAction();
    });
  }
  
  render() {
    const { showModal } = this.state;
    const {
      itemModalRegenerateCertReason,
      queryPage
    } = this.props;
    return (
      <Layout>
        <Filters
          reissue={queryPage && queryPage.reissue}
          onHandleCreateCert={this.onHandleCreateCert}
        />
        <CertList reissueId={queryPage && queryPage.reissue} />

        {(showModal || (queryPage && queryPage.create)) && (
          <ModalExtend
            stopClose
            onClose={this.onCloseModal}
            testLocator={DATA_LOC.DATA_LOC_MODAL_NEW_CERT}
          >
            <NewCert {...this.props} />
          </ModalExtend>
        )}

        {(itemModalRegenerateCertReason.size > 0 && (queryPage && queryPage.reissue)) && (
          <ReasonForRegenerate {...this.props} testLocator={DATA_LOC.DATA_LOC_MODAL_REGENERATE_CERT} />
        )}
      </Layout>
    );
  }
}

export default connect(
  mapStateToProps,
  {
    ...NotificationAction,
    ...LocalAction,
    ...ProfileAction
  }
)(CryptoProSettings);

