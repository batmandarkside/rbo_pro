import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { reduxForm } from 'redux-form/immutable';
import {
  FORM_NAME_NEW_CERT,
} from '../constants';

import StepForm from './step-form';
import StepPrint from './step-print';

class NewCert extends Component {
  
  static propTypes = {
    ulcValues: PropTypes.instanceOf(List),
    cpsValues: PropTypes.instanceOf(List),
    error: PropTypes.string,
    submittingForm: PropTypes.bool,
    sendingToBank: PropTypes.bool,
    printingNewCert: PropTypes.bool,
    getValuesUlkAction: PropTypes.func,
    createNewCertAction: PropTypes.func,
    printNewCertAction: PropTypes.func,
    changeUlkAction: PropTypes.func,
    createdNewCertData: PropTypes.instanceOf(Map),
    newCertStatuses: ImmutablePropTypes.contains({
      cryptoPluginWork: PropTypes.bool,
      sendToBank: PropTypes.bool
    }),
    sendToBankNewCertAction: PropTypes.func.isRequired,
    isHaveDefaultCityName: PropTypes.bool.isRequired
  }

  componentDidMount() {
    const { getValuesUlkAction } = this.props;
    getValuesUlkAction();
  }
  
  /**
   *
   * @param evt
   * @private
   */
  handleSubmit = (evt) => {
    evt.preventDefault();
    const { createNewCertAction } = this.props;
    createNewCertAction();
  };

  handlePrint = () => {
    const { printNewCertAction, createdNewCertData } = this.props;
    const recordID = createdNewCertData.getIn(['newCert', 'recordID']);
    printNewCertAction(recordID);
  }

  handleSend = () => {
    const { sendToBankNewCertAction } = this.props;
    sendToBankNewCertAction();
  }

  render() {
    const {
      newCertStatuses,
      createdNewCertData,
      ulcValues,
      cpsValues,
      submittingForm,
      changeUlkAction,
      error,
      isHaveDefaultCityName,
      sendingToBank,
      printingNewCert,
      ...rest
    } = this.props;

    return (
      <div>
        {!newCertStatuses.get('cryptoPluginWork') && (
          <StepForm
            {...rest}
            error={error}
            changeUlkAction={changeUlkAction}
            ulcValues={ulcValues}
            cpsValues={cpsValues}
            newCertStatuses={newCertStatuses}
            submittingForm={submittingForm}
            isHaveDefaultCityName={isHaveDefaultCityName}
            handleSubmit={this.handleSubmit}
          />
        )}
        {newCertStatuses.get('cryptoPluginWork') && (
          <StepPrint
            newCertStatuses={newCertStatuses}
            createdNewCertData={createdNewCertData}
            handlePrint={this.handlePrint}
            handleSend={this.handleSend}
            sendingToBank={sendingToBank}
            printingNewCert={printingNewCert}
          />
        )}
      </div>
    );
  }
}


export default reduxForm({
  form: FORM_NAME_NEW_CERT
})(NewCert);

