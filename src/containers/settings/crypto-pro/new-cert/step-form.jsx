import React from 'react';
import PropTypes from 'prop-types';
import { List, fromJS } from 'immutable';
import {
  Field
} from 'redux-form/immutable';
import { normalizeAllMaxLength } from 'utils';
import {
  FieldTextInline,
  FieldInput,
  FieldDropDown
} from '../../fields';
import {
  FIELD_REG_NUM,
  FIELD_DATE,
  FIELD_ULK,
  FIELD_STATUS,
  FIELD_CRYPTO_NAME,
  FIELD_EMAIL,
  FIELD_CITY
} from '../constants';
import {
  Gray,
  Black,
  Body,
  Footer,
  WorkCryptoPro,
  LoadingText,
  LoaderExtend,
  ButtonExtend
} from './style.js';

import {
  DirectionColumn,
  Row,
  RowFlex,
  InfoError,
} from '../style.js';

import {
  Header,
  H2
} from '../../style.js';

const StepForm = (props) => {
  const {
    error,
    submittingForm,
    handleSubmit,
    ulcValues,
    cpsValues,
    changeUlkAction,
    isHaveDefaultCityName
  } = props;

  function onChangeOlk(value) {
    value.preventDefault();
    changeUlkAction(fromJS(value));
  }

  return (
    <form
      onSubmit={handleSubmit}
      className="b-forms"
    >
      <Header>
        <H2>
          Запрос на новый сертификат №
          <Field name={FIELD_REG_NUM} component={FieldTextInline} testLocator={FIELD_REG_NUM} />
        </H2>
        <div>
          <Gray>От:</Gray>
          <Black> <Field name={FIELD_DATE} component={FieldTextInline} testLocator={FIELD_DATE} /> </Black>
          <Gray> · Статус: </Gray>
          <Black>
            <Field name={FIELD_STATUS} component={FieldTextInline} testLocator={FIELD_STATUS} />
          </Black>
        </div>
      </Header>
      <Body>
        {error && <InfoError>{error}</InfoError>}
        <DirectionColumn>
          <Row>
            <Field
              name={FIELD_ULK}
              component={FieldDropDown}
              onChange={onChangeOlk}
              options={ulcValues}
              testLocator={FIELD_ULK}
              label="Организация"
            />
          </Row>
          <Row>
            <Field
              name={FIELD_CRYPTO_NAME}
              component={FieldDropDown}
              options={cpsValues}
              testLocator={FIELD_CRYPTO_NAME}
              label="Средство подписи"
            />
          </Row>
          <Row>
            <Field
              name={FIELD_EMAIL}
              autoFocus
              normalize={value => normalizeAllMaxLength(value, 56)}
              component={FieldInput}
              type="text"
              label="Email"
            />
          </Row>
          <Row>
            <Field
              name={FIELD_CITY}
              component={FieldInput}
              normalize={value => normalizeAllMaxLength(value, 50)}
              disabled={isHaveDefaultCityName}
              type="text"
              label="Город"
            />
          </Row>
          {/* <Row>
            <Field
              name={FIELD_COUNTRY}
              component={FieldText}
              testLocator={FIELD_COUNTRY}
              label="Страна"
            />
          </Row>
          <Row>
            <Field
              name={FIELD_SUBDIVISION}
              component={FieldText}
              testLocator={FIELD_ORGANIZATION}
              label="Подразделение"
            />
          </Row>
           */}
        </DirectionColumn>
      </Body>
      <Footer>
        <RowFlex>
          <ButtonExtend
            primary
            size="large"
            className="button"
            disabled={submittingForm}
            type="submit"
            name="createNewCertSubmit"
            data-loc="createNewCertSubmit"
          >
            Создать и отправить
          </ButtonExtend>
          <WorkCryptoPro>
            {submittingForm && <LoaderExtend /> }
            {submittingForm && <LoadingText>Идет работа в КриптоПРО</LoadingText>}
          </WorkCryptoPro>
        </RowFlex>
      </Footer>
    </form>
  );
};

StepForm.propTypes = {
  submittingForm: PropTypes.bool,
  error: PropTypes.string,
  handleSubmit: PropTypes.func,
  changeUlkAction: PropTypes.func,
  isHaveDefaultCityName: PropTypes.bool,
  ulcValues: PropTypes.instanceOf(List),
  cpsValues: PropTypes.instanceOf(List)
};

export default StepForm;

