import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { connect } from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { mapStateToProps } from '../selectors';
import * as LocalAction from '../actions';

import {
  ReissueContainer,
  WorkCryptoPro,
  LoadingText,
  LoaderExtend,
} from './style.js';

import StepPrint from './step-print';

class ReasonForRegenerate extends Component {
  static propTypes = {
    submittingForm: PropTypes.bool,
    printingNewCert: PropTypes.bool,
    createdNewCertData: PropTypes.instanceOf(Map),
    newCertStatuses: ImmutablePropTypes.contains({
      cryptoPluginWork: PropTypes.bool,
      sendToBank: PropTypes.bool
    }),
    printNewCertAction: PropTypes.func,
    reissueCertAction: PropTypes.func.isRequired,
    itemModalRegenerateCertReason: PropTypes.instanceOf(Map).isRequired,
  }

  componentDidMount() {
    const { reissueCertAction, itemModalRegenerateCertReason } = this.props;
    reissueCertAction(itemModalRegenerateCertReason.getIn(['cert', 'id']));
  }

  handlePrint = () => {
    const { printNewCertAction, createdNewCertData } = this.props;
    const recordID = createdNewCertData.getIn(['newCert', 'recordID']);
    printNewCertAction(recordID);
  }

  render() {
    const {
      newCertStatuses,
      submittingForm,
      createdNewCertData,
      printingNewCert,
    } = this.props;

    return (
      <ReissueContainer>
        {submittingForm && (
          <WorkCryptoPro>
            <LoaderExtend />
            <LoadingText>Идет работа в КриптоПРО</LoadingText>
          </WorkCryptoPro>
        )}
        {newCertStatuses.get('cryptoPluginWork') && (
          <StepPrint
            newCertStatuses={newCertStatuses}
            createdNewCertData={createdNewCertData}
            handlePrint={this.handlePrint}
            handleSend={this.handleSend}
            printingNewCert={printingNewCert}
          />
        )}
      </ReissueContainer>
    );
  }
}


export default connect(
  mapStateToProps,
  { ...LocalAction }
)(ReasonForRegenerate);

