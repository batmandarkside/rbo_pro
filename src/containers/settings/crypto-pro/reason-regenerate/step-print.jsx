import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import moment from 'moment-timezone';
import {
  Gray,
  Black,
  Body,
  Footer,
  IconForButton,
  ButtonExtend,
  ButtonWithIconPrint,
  WorkCryptoPro,
  LoaderExtend
} from './style.js';

import {
  Header,
  H2
} from '../../style.js';

import {
  CollLabel,
  CollDescription,
  RowFlex,
  Info,
  DirectionColumn
} from '../style.js';

const StepPrint = ({ createdNewCertData, newCertStatuses, handlePrint, handleSend, sendingToBank, printingNewCert }) => {
  const {
    reqDate,
    ulk: {
      email,
      cityName,
      orgName
    },
    cryptoProfileName,
    certState
  } = createdNewCertData.toJS();

  return (
    <div>
      <Header>
        <H2> Сертификат </H2>
        <div>
          <Gray>От:</Gray> <Black>{moment(reqDate).format('DD.MM.YYYY')}</Black>
          <Gray> · Статус:</Gray> <Black>{certState}</Black>
        </div>
      </Header>
      <Body>
        {(newCertStatuses.get('cryptoPluginWork') && newCertStatuses.get('sendToBank')) && (
          <Info>
            Запрос отправлен. Распечатайте его, заверьте печатью и подписью и предоставьте в отделение банка.
            После предъявления бумажного экземпляра сертификат будет активирован в течение рабочего дня.
          </Info>
        )}

        {(newCertStatuses.get('cryptoPluginWork') && !newCertStatuses.get('sendToBank')) && (
          <Info>Запрос создан, необходимо отправить его в банк.</Info>
        )}

        <DirectionColumn>
          <RowFlex>
            <CollLabel>Организация</CollLabel>
            <CollDescription>{orgName}</CollDescription>
          </RowFlex>
          <RowFlex>
            <CollLabel>Средство подписи</CollLabel>
            <CollDescription>{cryptoProfileName}</CollDescription>
          </RowFlex>
          <RowFlex>
            <CollLabel>Email</CollLabel>
            <CollDescription>{email}</CollDescription>
          </RowFlex>
          <RowFlex>
            <CollLabel>Город</CollLabel>
            <CollDescription>{cityName}</CollDescription>
          </RowFlex>
        </DirectionColumn>
      </Body>
      <Footer>
        <RowFlex>
          {newCertStatuses.get('cryptoPluginWork') && newCertStatuses.get('sendToBank') && (
            <ButtonWithIconPrint
              primary
              size="large"
              className="button"
              data-loc="printNewCertSubmit"
              onClick={handlePrint}
            >
              <IconForButton
                type="print"
                size="16"
                title="Распечатать"
                onClick={handlePrint}
              />
              Распечатать
            </ButtonWithIconPrint>
          )}

          {newCertStatuses.get('cryptoPluginWork') && !newCertStatuses.get('sendToBank') &&  (
            <ButtonExtend
              primary
              size="large"
              disabled={sendingToBank}
              data-loc="sendNewCertSubmit"
              onClick={handleSend}
            >
              Отправить в банк
            </ButtonExtend>
          )}
          <WorkCryptoPro>
            {(sendingToBank || printingNewCert) && <LoaderExtend /> }
          </WorkCryptoPro>
        </RowFlex>
      </Footer>
    </div>
  );
};

StepPrint.propTypes = {
  newCertStatuses: ImmutablePropTypes.contains({
    cryptoPluginWork: PropTypes.bool,
    sendToBank: PropTypes.bool
  }),
  createdNewCertData: ImmutablePropTypes.contains({
    ulk: ImmutablePropTypes.contains({
      cityName: PropTypes.string,
      country: PropTypes.string,
      orgName: PropTypes.string,
      email: PropTypes.string,
    }),
    cryptoProfileName: PropTypes.string
  }),
  handlePrint: PropTypes.func,
  handleSend: PropTypes.func,
  sendingToBank: PropTypes.bool,
  printingNewCert: PropTypes.bool,
};

export default StepPrint;

