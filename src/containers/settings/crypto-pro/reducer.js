import { fromJS } from 'immutable';
import { flowRight } from 'lodash';
import moment from 'moment-timezone';
import {
  ACTIONS,
  FILTER_SORT_START_DATE,
  FILTER_SORT_REQUEST_DATE,
  FILTER_SORT_NEW,
  FILTER_SORT_OLD,
  ENTITY_STATUS_REG_REGEN_ONLY,
  ENTITY_STATUS_REG_NEW_ONLY,
  ENTITY_STATUS_CERT_ONLY,
  ENTITY_STATUS_REG_WITH_CERT,
  STATUSES_LABEL
} from './constants';

import { prepareForDropBox } from './util';

/**
 *
 * @param state
 * @param payload
 */
export function createdNewCert(state, payload) {
  const dataNewCert = payload.get('dataNewCert');
  const newCert = payload.get('resultNewCertData');
  const createdNewCertData = dataNewCert.merge({
    newCert,
    certState: 'Создан'
  });

  return state.merge({
    submittingForm: false,
    newCertStatuses: {
      cryptoPluginWork: true,
      sendToBank: false,
    },
    certState: dataNewCert.get('certState'),
    createdNewCertData
  });
}

/**
 * @todo по требованиям проверка должна быть на сервере и additionalStatus === expiring
 * @param cert
 * @returns {*}
 */
export const normalizeCertItemExpired = (cert) => {
  if (cert.get('mainStatus') === 'active') {
    const now = moment.now();
    const dateEnd = moment(cert.get('dateEnd'), 'YYYY-MM-DD');
    const countDays = dateEnd.diff(now, 'days');
    return cert.set('additionalStatus', countDays <= 31 ? 'expiring' :  '');
  }
  return cert;
};

/**
 *
 * @param cert
 */
export const translateStatus = cert => (
  cert.set('translateStatus', STATUSES_LABEL[cert.get('mainStatus')])
);

/**
 *
 * @param certList
 */
export const normalizeCertItem = certList => (
  certList.map(cert =>
    flowRight([
      translateStatus,
      normalizeCertItemOperations,
      normalizeCertDetailImportantMessage,
      // normalizeCertItemExpired
    ])(cert)
  )
);

/**
 * свойтва для показа иконок и кнопок операций
 * @param cert
 */
export function normalizeCertItemOperations(cert) {
  const status = cert.get('mainStatus');
  const type = cert.get('entityType');
  const operations = {
    // перевыпуск
    repeat: (status === 'active') &&
      (type === ENTITY_STATUS_REG_WITH_CERT || type === ENTITY_STATUS_CERT_ONLY),

    // удалить
    remove: status === 'new' && (type === ENTITY_STATUS_REG_NEW_ONLY ||
      type === ENTITY_STATUS_REG_REGEN_ONLY),

    // отправить в банк
    send: (status === 'new' && type === ENTITY_STATUS_REG_NEW_ONLY) ||
    (status === 'signed' && type === ENTITY_STATUS_REG_REGEN_ONLY),

    // подписать
    sign: status === 'new' && type === ENTITY_STATUS_REG_REGEN_ONLY,
    print: (status === 'sendToBank'  && type === ENTITY_STATUS_REG_NEW_ONLY),
    download: (status === 'active'  && (type === ENTITY_STATUS_REG_WITH_CERT || type === ENTITY_STATUS_CERT_ONLY))
  };
  return cert.merge({ operations });
}

/**
 * Отображение нотификации во всплывающем окне ( детальная информация по сертификату )
 * @link https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=103363272
 * @param certList
 */
export function normalizeCertDetailImportantMessage(cert) {
  const status = cert.get('mainStatus');
  const type = cert.get('entityType');
  let importantMessage = '';

  if (status === 'new' && type === ENTITY_STATUS_REG_NEW_ONLY) {
    importantMessage = ['Запрос создан, необходимо отправить его в банк.'];
  }

  if (status === 'new' && type === ENTITY_STATUS_REG_REGEN_ONLY) {
    importantMessage = ['Запрос создан, необходимо подписать его и отправить в банк.'];
  }

  if (status === 'signed' && type === ENTITY_STATUS_REG_REGEN_ONLY) {
    importantMessage = ['Запрос создан и подписан, необходимо отправить его в банк.'];
  }

  if (status === 'sendToBank' && (type === ENTITY_STATUS_REG_REGEN_ONLY || type === ENTITY_STATUS_REG_NEW_ONLY)) {
    importantMessage = [
      'Запрос отправлен в банк. Распечатайте его, заверьте печатью и подписью и предоставьте в отделение банка.',
      'После предъявления бумажного экземпляра сертификат будет активирован в течение рабочего дня.'
    ];
  }

  if (
    (status === 'invalidProps' || status === 'declinedByBank' || status === 'invalidSign') &&
    (type === ENTITY_STATUS_REG_REGEN_ONLY || type === ENTITY_STATUS_REG_NEW_ONLY)) {
    importantMessage = ['Запрос не обработан. Проверьте данные и повторите попытку.'];
  }

  if (status === 'inactive' &&
    (type === ENTITY_STATUS_CERT_ONLY || type === ENTITY_STATUS_REG_WITH_CERT)) {
    importantMessage = [
      `Сертификат не активен с ${moment(cert.get('certStatusChangeDate')).format('DD.MM.YYYY')}`,
      cert.get('certStatusChangeReason') ? `Причина: ${cert.get('certStatusChangeReason')}` : ''
    ];
  }
  return cert.merge({ importantMessage });
}

/**
 *
 * @param date1
 * @param date2
 * @returns {number}
 */
const sortByDateAsc = (date1, date2) => {
  if (date1 < date2) return 1;
  if (date1 > date2) return -1;
  return 0;
};

/**
 *
 * @param date1
 * @param date2
 * @returns {number}
 */
const sortByDateDesc = (date1, date2) => {
  if (date1 > date2) return 1;
  if (date1 < date2) return -1;
  return 0;
};


/**
 *
 * @param list
 * @param sortDirection
 */
const sortCertList = (list, sortDirection) => (
  list.sort((a, b) => {
    const c = moment(a.get(FILTER_SORT_START_DATE) || a.get(FILTER_SORT_REQUEST_DATE));
    const d = moment(b.get(FILTER_SORT_START_DATE) || b.get(FILTER_SORT_REQUEST_DATE));
    if (!c.isValid() || !d.isValid()) return -1;

    return sortDirection === FILTER_SORT_NEW
      ? sortByDateAsc(c, d)
      : sortByDateDesc(c, d);
  })
);


/**
 * фильтруем список по статусу
 * @param state
 */
export const filterByStatusCert = (state) => {
  const mainStatus = state.getIn(['filters', 'statusValue', 'mainStatus']);

  if (!mainStatus) {
    return state;
  }

  let certListFilteredState = state
    .update('certListFiltered', () => (
      sortCertList(
        state.get('certListRaw').filter(c => c.get('mainStatus') === mainStatus),
        state.getIn(['filters', 'sortDirection'])
      )
    ));

  /**
   * если по такому фильтру сертификатов больше нет, то сбрасываем фильтр
   * и возвращаем полный список
   */
  if (!certListFilteredState.get('certListFiltered').size) {
    certListFilteredState = certListFilteredState.setIn(['filters', 'statusValue'], fromJS([]));
    certListFilteredState = certListFilteredState
      .update('certListFiltered', () => state.get('certListRaw'));
  }

  return certListFilteredState;
};


/**
 *
 * @param state
 */
export const clearFilterStatusCert = state => (
  state
    .setIn(['filters', 'statusValue'], fromJS({}))
    .set('certListFiltered', sortCertList(state.get('certListRaw'), state.getIn(['filters', 'sortDirection'])))
);

/**
 *
 * @param state
 * @param sortDirection
 */
export const sortCert = (state, sortDirection) => (
  state
    .setIn(['filters', 'sortDirection'], sortDirection)
    .update('certListFiltered', certListFiltered => sortCertList(certListFiltered, sortDirection))
);

/**
 *
 * @param state
 * @param payload
 */
export const prepareCertList = (state, payload) => {
  let newState = state;
  const list = payload.get('list');
  const reissue = payload.get('reissue');
  const normalizeList = normalizeCertItem(list);
  if (reissue) {
    const reissueItem = normalizeList.find(i => i.getIn(['cert', 'id']) === reissue);
    newState = reissueItem
      ? newState.setIn(['filters', 'statusValue'], prepareForDropBox(reissueItem, ['cert', 'id'], 'translateStatus'))
      : newState;
  }
  newState = newState
    .set('certListRaw', normalizeList)
    .set('certListFiltered', sortCertList(normalizeList, state.getIn(['filters', 'sortDirection'])))
    .set('loadCertListLoad', false);

  return filterByStatusCert(newState);
};

/**
 *
 * @param state
 * @returns {*}
 */
export const sendToBankNewCertSuccess = (state) => {
  const cryptoPluginWork = state.getIn(['newCertStatuses', 'cryptoPluginWork']);
  return state
    .setIn(['createdNewCertData', 'certState'], 'отправлен в банк')
    .setIn(['newCertStatuses', 'sendToBank'], cryptoPluginWork)
    .set('sendingToBank', false);
};


/**
 * typeId у сертификата есть несколько типов id - getIn(['cert', 'id']), requestId, recordID
 *
 * @param state
 * @param id
 * @param typeId
 */
export const setActivePendingItem = (state, id, typeId = ['cert', 'id']) => {
  const certListFiltered = state.get('certListFiltered');
  const found = certListFiltered.find(cert => cert.getIn(typeId) === id);
  if (!found) {
    return state;
  }
  const foundIntex = certListFiltered.findIndex(cert => cert.getIn(typeId) === id);
  const newCertList = certListFiltered.update(foundIntex, () => found.set('pending', true));
  return state.set('certListFiltered', newCertList);
};

export const setFinishPendingItem = state => (
  state.update('certListFiltered', certListFiltered =>
    certListFiltered.map(c => c.set('pending', false))
  )
);


const resetData = state => state.merge({
  cpsValues: [],
  submittingForm: false,
  itemModalRegenerateCertReason: {},
  ulcProfiles: {
    cryptoServiceConf: {},
    requestInfo: {},
    ulkList: [],
  },
  createdNewCertData: {},
  newCertStatuses: {
    cryptoPluginWork: false,
    sendToBank: false,
  }
});

const initialState = fromJS({
  ulcProfiles: {
    cryptoServiceConf: {},
    requestInfo: {},
    ulkList: [],
  },
  cpsValues: [],
  certListRaw: [],
  certListFiltered: [],
  country: 'Россия',
  newCertStatuses: {
    cryptoPluginWork: false,
    sendToBank: false,
  },
  createdNewCertData: {},
  statusValues: [],
  sortValues: [{
    value: 0,
    title: 'Сначала новые',
    sortDirection: FILTER_SORT_NEW
  }, {
    value: 0,
    title: 'Сначала старые',
    sortDirection: FILTER_SORT_OLD
  }],
  submittingForm: false,
  loadCertListLoad: false,
  sendingToBank: false,
  printingNewCert: false,
  itemModalRegenerateCertReason: {},
  filters: {
    statusValue: {},
    sortValue: {
      value: 0,
      title: 'Сначала новые',
      sortDirection: FILTER_SORT_NEW
    },
    sortDirection: FILTER_SORT_NEW
  }
});

export default function cryptoProSettingsReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.CRYPTO_REISSUE_START:
    case ACTIONS.CRYPTO_CREATE_NEW_CERT_REQUEST:
      return state.set('submittingForm', true);
    case ACTIONS.CRYPTO_REISSUE_STOP:
    case ACTIONS.CRYPTO_CREATE_NEW_CERT_FAIL:
      return state.set('submittingForm', false);
    case ACTIONS.CRYPTO_CREATE_NEW_CERT_SUCCESS:
      return createdNewCert(state, fromJS(action.payload));

    case ACTIONS.CRYPTO_SEND_TO_BANK_NEW_CERT_REQUEST:
      return state.set('sendingToBank', true);
    case ACTIONS.CRYPTO_SEND_TO_BANK_NEW_CERT_FAIL:
      return state.set('sendingToBank', false);
    case ACTIONS.CRYPTO_SEND_TO_BANK_NEW_CERT_SUCCESS:
      return sendToBankNewCertSuccess(state);

    // выставляем в статус pending сертификат из стиска
    // pending with requestId
    case ACTIONS.CRYPTO_OPERATION_DELETE_REQUEST:
    case ACTIONS.CRYPTO_OPERATION_SEND_REQUEST:
    case ACTIONS.CRYPTO_OPERATION_SIGN_OLD_CERT_REQUEST:
      return setActivePendingItem(state, action.payload.id, ['requestId']);
    case ACTIONS.CRYPTO_OPERATION_DELETE_SUCCESS:
    case ACTIONS.CRYPTO_OPERATION_DELETE_FAIL:
    case ACTIONS.CRYPTO_OPERATION_SEND_SUCCESS:
    case ACTIONS.CRYPTO_OPERATION_SEND_FAIL:
    case ACTIONS.CRYPTO_OPERATION_SIGN_OLD_CERT_SUCCESS:
    case ACTIONS.CRYPTO_OPERATION_SIGN_OLD_CERT_FAIL:
      return setFinishPendingItem(state);

    case ACTIONS.CRYPTO_OPERATION_PRINT_REQUEST:
      return setActivePendingItem(state, action.payload.id, ['requestId'])
        .set('printingNewCert', true);

    case ACTIONS.CRYPTO_OPERATION_PRINT_SUCCESS:
    case ACTIONS.CRYPTO_OPERATION_PRINT_FAIL:
      return setFinishPendingItem(state).set('printingNewCert', false);

    // pending with ['cert', 'id']
    case ACTIONS.CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_REQUEST:
      return setActivePendingItem(state, action.payload.id, ['cert', 'id']);
    case ACTIONS.CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_SUCCESS:
    case ACTIONS.CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_FAIL:
      return setFinishPendingItem(state);

    case ACTIONS.CRYPTO_PRO_FILTERS_CHANGE_STATUS:
      return filterByStatusCert(
        state.setIn(['filters', 'statusValue'], action.payload.value)
      );

    case ACTIONS.CRYPTO_PRO_FILTERS_CHANGE_SORT_VALUE:
      return state.setIn(['filters', 'sortValue'], action.payload.value);

    case ACTIONS.CRYPTO_PRO_FILTERS_CLEAR_STATUS:
      return clearFilterStatusCert(state);
    case ACTIONS.CRYPTO_PRO_FILTERS_SORT:
      return sortCert(state, action.payload.dateValue);

    case ACTIONS.CRYPTO_PRO_GET_CERT_LIST_REQUEST:
      return state.set('loadCertListLoad', true);
    case ACTIONS.CRYPTO_PRO_GET_CERT_LIST_FAIL:
      return state.set('loadCertListLoad', false);
    case ACTIONS.CRYPTO_PRO_GET_CERT_LIST_SUCCESS:
      return prepareCertList(state, fromJS(action.payload));

    case ACTIONS.CRYPTO_PRO_GET_ULK_SUCCESS:
      return state.merge({ ulcProfiles: fromJS(action.payload) });

    // выставляем CryptoProfiles из массива ulkList
    case ACTIONS.CRYPTO_PRO_CHANGE_CRYPTO_PRO_NAME_VALUES:
      return state.set('cpsValues', fromJS(action.payload));

    case ACTIONS.CRYPTO_PRO_REISSUE_CERT_START:
      return state.set('itemModalRegenerateCertReason', action.payload);
    case ACTIONS.CRYPTO_PRO_REISSUE_CERT_FINISH:
      return state.set('itemModalRegenerateCertReason', fromJS({}));
    case ACTIONS.CRYPTO_PRO_REISSUE_SUCCESS:
    case ACTIONS.CRYPTO_PRO_RESET_FORM:
    case ACTIONS.CRYPTO_PRO_REISSUE_FAIL:
      return resetData(state);
    default:
      return state;
  }
}
