import { put, all } from 'redux-saga/effects';
import { change }  from 'redux-form/immutable';
import { prepareCryptoProfilesValue } from '../util';
import {
  ACTIONS,
  FORM_NAME_NEW_CERT,
  FIELD_ULK,
  FIELD_CRYPTO_NAME,
  FIELD_SUBDIVISION,
  FIELD_CITY
} from '../constants';

/**
 *
 * @param payload
 */
export function* changeUlkSaga({ payload }) {
  try {
    const cps = payload.get('cps');
    const cpsValue = cps.map(c => prepareCryptoProfilesValue(c));

    yield all([
      put(
        change(
          FORM_NAME_NEW_CERT,
          FIELD_ULK,
          payload,
          false,
          true
        )
      ),
      put(
        change(
          FORM_NAME_NEW_CERT,
          FIELD_CRYPTO_NAME,
          cpsValue.first(),
          false,
          true
        )
      ),
      put(
        change(
          FORM_NAME_NEW_CERT,
          FIELD_SUBDIVISION,
          payload.get('branchName'),
          false,
          true
        )
      ),
      put(
        change(
          FORM_NAME_NEW_CERT,
          FIELD_CITY,
          payload.get('cityName'),
          false,
          true
        )
      ),
      put({
        type: ACTIONS.CRYPTO_PRO_CHANGE_CRYPTO_PRO_NAME_VALUES,
        payload: cpsValue
      })
    ]);
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_PRO_CHANGE_FIELDS_ERROR, error: error.message });
  }
}
