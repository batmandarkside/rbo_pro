import { put, select, all, call } from 'redux-saga/effects';
import { isArray } from 'lodash';
import { required }  from 'utils/validation';
import { saveCryptoKeyAndEncodedData as dalSaveCryptoKeyAndEncodedData } from 'dal/sign/sagas/save-crypto-key-encoded-data';
import { addOrdinaryNotificationAction }  from 'dal/notification/actions';
import { prepareConfForNotification }  from 'utils/errors-util';
import moment from 'moment-timezone';
import {
  startSubmit,
  stopSubmit,
  getFormValues
} from 'redux-form/immutable';
import {
  ACTIONS,
  FORM_NAME_NEW_CERT,
  FIELD_CITY,
  FIELD_EMAIL
} from '../constants';

import {
  COMBINE_REDUCERS,
  CRYPTO_PRO_REDUCER
} from '../../constants';

import { getCertListAction } from '../actions';
import { createPKCS10RequestSaga } from './create-pkcs10-request';
import { sendToBankCert } from './send-to-bank-cert-saga';

const cryptoServiceConfSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'ulcProfiles', 'cryptoServiceConf']);

/**
 * Валидация обязательных полей перед созданием сертификата
 * @param email
 * @param city
 * @returns {Promise}
 */
export function validateEmailAndCity(email, city) {
  const errors = {};
  return new Promise((resolve, reject) => {
    errors[FIELD_CITY] = required(city);
    errors[FIELD_EMAIL] = required(email);
    if (!errors[FIELD_CITY] && !errors[FIELD_EMAIL]) {
      errors[FIELD_EMAIL] = !/^[A-ZА-Я0-9._%+-]+@[A-ZА-Я0-9.-]+\.[A-ZА-Я]{2,4}$/i.test(email.trim());
      if (errors[FIELD_EMAIL]) {
        reject({
          message: 'Заполните email по маске - email@domain.com',
          errors
        });
      }
      resolve();
    } else {
      reject({
        message: 'Заполните обязательные поля',
        errors
      });
    }
  });
}


/**
 * создание пары ключей и отправка сейтификата в банк
 */
export function* createNewCertSaga() {
  const formValues = yield select(getFormValues(FORM_NAME_NEW_CERT));
  const cryptoServiceConf = yield select(cryptoServiceConfSelector);
  const formValuesRaw = formValues && formValues.toJS ? formValues.toJS() : {};
  const ulk = formValuesRaw.ULK;
  const CryptoProfile = formValuesRaw.CryptoProfileName;

  try {
    const dataNewCert = {
      reqDate: moment(formValuesRaw.RequestDate, 'DD.MM.YYYY').format(),
      reqNum: formValuesRaw.RequestNumber,
      ulk: {
        name: ulk.name,
        id: ulk.id,
        email: formValuesRaw.Email && formValuesRaw.Email.trim(),
        cityName: formValuesRaw.City && formValuesRaw.City.trim(),
        orgINN: ulk.orgINN,
        branchName: ulk.branchName,
        branchId: ulk.branchId,
        orgName: ulk.orgName
      },
      // прокидываем дальше по action
      cryptoProfile: {
        id: CryptoProfile.id,
        name: CryptoProfile.name
      },
      cryptoProfileId: CryptoProfile.id,
      country: formValuesRaw.Country,
      reqType: 'CryptoProReqNew',
      reqData: '',
      containerId: ''
    };

    yield all([
      put(stopSubmit(FORM_NAME_NEW_CERT, {})),
      put(startSubmit(FORM_NAME_NEW_CERT))
    ]);

    yield validateEmailAndCity(formValuesRaw[FIELD_EMAIL], formValuesRaw[FIELD_CITY]);

    // вызываем cryptoPro для создании подписи
    // console.log(dataNewCert, "dataNewCert")
    const createPKCS10Result = yield call(createPKCS10RequestSaga, {
      payload: dataNewCert,
      cryptoServiceConf
    });

    // кодируем данные в base64
    dataNewCert.reqData = btoa(createPKCS10Result.pkcs10);
    dataNewCert.containerId = createPKCS10Result.containerName;

    // отправляем открытый ключ на сервер для валидации
    // получаем recordID для отправки в банк
    const resultNewCert = yield call(dalSaveCryptoKeyAndEncodedData, { payload: dataNewCert });  

    if (resultNewCert.errors.length) {
      throw resultNewCert.errors;
    }

    // событие об успешной работе припто плагина и создания пары ключей
    yield put({
      type: ACTIONS.CRYPTO_CREATE_NEW_CERT_SUCCESS,
      payload: {
        dataNewCert,
        resultNewCertData: resultNewCert
      }
    });

    // отправка в банк
    yield call(sendToBankCert, { payload: { docId: resultNewCert.recordID } });

    yield all([
      put(stopSubmit(FORM_NAME_NEW_CERT, {})),
      put(getCertListAction()),
    ]);
  } catch (error) {
    if (error && isArray(error)) {
      yield put(addOrdinaryNotificationAction({
        level: 'warning',
        type: 'messagesList',
        title: 'Не удалось перевыпустить сертификат',
        collectionMessages: error
      }));
    } else if ((error && error.errors) && (error.errors[FIELD_CITY] || error.errors[FIELD_EMAIL])) {
      yield put(
        stopSubmit(FORM_NAME_NEW_CERT, {
          [FIELD_CITY]: error.errors && error.errors[FIELD_CITY],
          [FIELD_EMAIL]: error.errors && error.errors[FIELD_EMAIL],
          _error: error.message
        })
      );
    } else {
      yield put(addOrdinaryNotificationAction(prepareConfForNotification(error)));
    }

    yield put({ type: ACTIONS.CRYPTO_CREATE_NEW_CERT_FAIL, payload: error });
  }
}
