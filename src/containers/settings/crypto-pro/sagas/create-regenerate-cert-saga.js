import { put, select, all, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { isArray } from 'lodash';
import { addOrdinaryNotificationAction }  from 'dal/notification/actions';
import { saveCryptoKeyAndEncodedData as dalSaveCryptoKeyAndEncodedData } from 'dal/sign/sagas/save-crypto-key-encoded-data';
import { getUlkValuesForRegenerateCertSaga as dalUlkValuesForRegenerateCertSaga } from 'dal/sign/sagas/get-ulk-values-saga';
import { signCertSaga as dalSignCertSaga } from 'dal/sign/sagas/sign-saga';
import { prepareConfForNotification }  from 'utils/errors-util';
import moment from 'moment-timezone';

import { ACTIONS } from '../constants';
import {
  COMBINE_REDUCERS,
  CRYPTO_PRO_REDUCER
} from '../../constants';

import { getCertListAction } from '../actions';
import { sendToBankCert } from './send-to-bank-cert-saga';
import { createPKCS10RequestSaga } from './create-pkcs10-request';


const certListSelector = state => state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'certListRaw']);

export function* changeUrlReissueSaga({ payload }) {
  const reissue = payload.getIn(['cert', 'id']);
  yield put(push({
    pathname: '/settings/crypto-pro',
    query: { reissue }
  }));
  yield put({
    type: ACTIONS.CRYPTO_PRO_REISSUE_CERT_START,
    payload
  });
}

export function* resetUrlReissueSaga() {
  yield put(push({ pathname: '/settings/crypto-pro' }));
  yield put({ type: ACTIONS.CRYPTO_PRO_REISSUE_CERT_FINISH });
}

export function* getUlkValuesForRegenerateCertSaga(id) {
  const certList = yield select(certListSelector);
  const cert = certList.find(c => c.getIn(['cert', 'id']) === id);
  try {
    yield put({ type: ACTIONS.CRYPTO_PRO_GET_ULK_FOR_REGENERATE_REQUEST });
    const resultCertData = yield call(dalUlkValuesForRegenerateCertSaga, {
      cert
    });
    yield put({
      type: ACTIONS.CRYPTO_PRO_GET_ULK_FOR_REGENERATE_SUCCESS,
      payload: resultCertData
    });
    return resultCertData;
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_PRO_GET_ULK_FOR_REGENERATE_FAIL });
    throw error;
  }
}

export function* createRegenerateCertSaga({ payload }) {
  const { id } = payload;
  try {
    yield put({ type: ACTIONS.CRYPTO_REISSUE_START });
    const { ulkList, requestInfo, cryptoServiceConf } = yield call(getUlkValuesForRegenerateCertSaga, id);

    const { cryptoProfiles, ...ulkRest } = ulkList[0];
    const cryptoProfile = cryptoProfiles[0];
    const signAuthorities = cryptoProfile.signAuthorities[0];

    const dataRepeatCert = {
      reqDate: moment(requestInfo.reqDate, 'YYYY-MM-DD').format(),
      reqNum: requestInfo.reqNum,
      ulk: {
        ...ulkRest
      },
      cryptoProfile:  {
        id: cryptoProfile.id,
        name: cryptoProfile.name
      },
      cryptoProfileId: cryptoProfile.id,
      country: 'RU',
      reqType: 'CryptoProReqRegen',
      reqData: '',
      containerId: ''
    };

    // создаем новую пару
    const createPKCS10Result = yield call(createPKCS10RequestSaga, {
      payload: dataRepeatCert,
      cryptoServiceConf
    });

    // кодируем данные в base64
    dataRepeatCert.reqData = btoa(createPKCS10Result.pkcs10);
    dataRepeatCert.containerId = createPKCS10Result.containerName;

    // сохраняем новый документ
    // и получаем recordID документа для дальнейшей подписи и отправки в банк
    const resultNewCert = yield call(dalSaveCryptoKeyAndEncodedData, { payload: dataRepeatCert });
  
    if (resultNewCert.errors.length) {
      throw resultNewCert.errors;
    }

    // пытаемся получить digest и подписать
    yield call(dalSignCertSaga, {
      payload: {
        docId: resultNewCert.recordID,
        signAuthId: signAuthorities.id || ''
      }
    });

    // отправка в банк
    yield call(sendToBankCert, {
      payload: {
        docId: resultNewCert.recordID,
        reqType: 'CryptoProReqRegen'
      }
    });

    yield call(resetUrlReissueSaga);
    yield all([
      put({ type: ACTIONS.CRYPTO_REISSUE_STOP }),
      put({ type: ACTIONS.CRYPTO_PRO_REISSUE_SUCCESS }),
      put(getCertListAction())
    ]);
  } catch (error) {
    if (error && isArray(error)) {
      yield put(addOrdinaryNotificationAction({
        level: 'warning',
        type: 'messagesList',
        title: 'Не удалось перевыпустить сертификат',
        collectionMessages: error
      }));
    } else {
      yield put(addOrdinaryNotificationAction(prepareConfForNotification(error)));
    }
    yield call(resetUrlReissueSaga);
    yield put({ type: ACTIONS.CRYPTO_PRO_REISSUE_FAIL, error: error.message });
  }
}
