import { put, call, select } from 'redux-saga/effects';
import { getCertListSaga as dalGetCertListSaga } from 'dal/sign/sagas/get-cert-list-saga';
import { ACTIONS } from '../constants';

export function* getCertListSaga() {
  const reissue = yield select(state =>
    state.getIn(['routing', 'locationBeforeTransitions', 'query', 'reissue']));
  try {
    const result = yield call(dalGetCertListSaga);
    yield put({
      type: ACTIONS.CRYPTO_PRO_GET_CERT_LIST_SUCCESS,
      payload: {
        list: result,
        reissue
      }
    });
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_PRO_GET_CERT_LIST_FAIL });
  }
}
