import { put, call } from 'redux-saga/effects';
import { getUlkValuesSaga as dalGetUlkValuesSaga } from 'dal/sign/sagas/get-ulk-values-saga';
import { ACTIONS } from '../constants';

export function* getUlkValuesSaga(action) {
  try {
    const result = yield call(dalGetUlkValuesSaga, action);
    yield put({
      type: ACTIONS.CRYPTO_PRO_GET_ULK_SUCCESS,
      payload: result
    });
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_PRO_GET_ULK_FAIL });
  }
}
