import { takeLatest, select, call } from 'redux-saga/effects';
import { updateSettings }  from 'dal/profile/sagas';
import { ACTIONS } from '../constants';

import { getUlkValuesSaga } from './get-ulk-values-saga';
import { createNewCertSaga } from './create-cert-saga';
import { createRegenerateCertSaga, changeUrlReissueSaga } from './create-regenerate-cert-saga';
import { formCertInit } from './init-form-cert-saga';
import { changeUlkSaga } from './change-ulk-saga';
import { resetFormSaga } from './reset-form-saga';
import { getCertListSaga } from './get-cert-list-saga';
import { signOldCertSaga } from './sign-old-cert-saga';
import {
  deleteCertSaga,
  sendToBankCertSaga,
  printCertSaga,
  downloadActiveCertSaga,
} from './operation-cert-saga';

export const settingsSelector = state => state.getIn(['profile', 'settings']);

// test
export function* updateSettingsForTest(action) {
  const { payload } = action;
  const settings = yield select(settingsSelector);
  yield call(updateSettings, {
    payload: {
      settings: {
        ...settings.toJS(),
        ...payload
      }
    }
  });    
}


export default function* cryptoProContainerSagas() {
  yield takeLatest(ACTIONS.CRYPTO_CREATE_NEW_CERT_REQUEST, createNewCertSaga);

  yield takeLatest(ACTIONS.CRYPTO_PRO_GET_ULK_REQUEST, getUlkValuesSaga);
  yield takeLatest(ACTIONS.CRYPTO_PRO_REISSUE_REQUEST, createRegenerateCertSaga);
  yield takeLatest(ACTIONS.CRYPTO_PRO_GET_ULK_SUCCESS, formCertInit);
  yield takeLatest(ACTIONS.CRYPTO_PRO_CHANGE_ULK, changeUlkSaga);
  yield takeLatest(ACTIONS.CRYPTO_PRO_RESET_FORM, resetFormSaga);
  yield takeLatest(ACTIONS.CRYPTO_PRO_GET_CERT_LIST_REQUEST, getCertListSaga);

  // operation
  yield takeLatest(ACTIONS.CRYPTO_OPERATION_DELETE_REQUEST, deleteCertSaga);
  yield takeLatest(ACTIONS.CRYPTO_OPERATION_PRINT_REQUEST, printCertSaga);
  yield takeLatest(ACTIONS.CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_REQUEST, downloadActiveCertSaga);
  yield takeLatest(ACTIONS.CRYPTO_OPERATION_SEND_REQUEST, sendToBankCertSaga);
  yield takeLatest(ACTIONS.CRYPTO_OPERATION_SIGN_OLD_CERT_REQUEST, signOldCertSaga);
  yield takeLatest(ACTIONS.CRYPTO_CHANGE_URL_FOR_REISSUE_CERT_REQUEST, changeUrlReissueSaga);

  yield takeLatest(ACTIONS.CRYPTO_RESET_NOTIFY_REQUEST, updateSettingsForTest);
}
