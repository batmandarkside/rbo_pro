import moment from 'moment-timezone';
import { put, all, select } from 'redux-saga/effects';
import { initialize } from 'redux-form/immutable';
import { fromJS } from 'immutable';
import {
  prepareUlkValue,
  prepareCryptoProfilesValue
} from '../util';

import {
  ACTIONS,
  FIELD_REG_NUM,
  FIELD_DATE,
  FORM_NAME_NEW_CERT,
  FIELD_ULK,
  FIELD_CITY,
  FIELD_STATUS,
  FIELD_CRYPTO_NAME,
  FIELD_COUNTRY,
  FIELD_SUBDIVISION,
  FIELD_ORGANIZATION
} from '../constants';

import {
  COMBINE_REDUCERS,
  CRYPTO_PRO_REDUCER
} from '../../constants';

const countrySelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'country']);

export function* formCertInit({ payload }) {
  const ulkValues = fromJS(payload);
  const country = yield select(countrySelector);
  const reqDate = ulkValues.getIn(['requestInfo', 'reqDate']);
  const reqNum = ulkValues.getIn(['requestInfo', 'reqNum']);

  const firstUlk = ulkValues.get('ulkList').first();
  const firstCryptoProfiles = firstUlk.get('cryptoProfiles').first();

  const ulkValue = prepareUlkValue(firstUlk);
  const cryptoProfilesValue = prepareCryptoProfilesValue(firstCryptoProfiles);

  yield all([
    put(
      initialize(
        FORM_NAME_NEW_CERT,
        {
          [FIELD_REG_NUM]: reqNum,
          [FIELD_DATE]: moment(reqDate || Date()).format('DD.MM.YYYY'),
          [FIELD_STATUS]: 'Новый',
          [FIELD_ULK]: ulkValue,
          [FIELD_CRYPTO_NAME]: cryptoProfilesValue,
          [FIELD_CITY]: firstUlk.get('cityName'),
          [FIELD_COUNTRY]: country,
          [FIELD_ORGANIZATION]: firstUlk.get('orgName'),
          [FIELD_SUBDIVISION]: firstUlk.get('branchName'),
        }
      )
    ),
    put({
      type: ACTIONS.CRYPTO_PRO_CHANGE_CRYPTO_PRO_NAME_VALUES,
      payload: firstUlk.get('cryptoProfiles').map(c => prepareCryptoProfilesValue(c))
    })
  ]);
}
