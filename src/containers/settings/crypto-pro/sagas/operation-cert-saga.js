import { put, call, all } from 'redux-saga/effects';
import { 
  deleteCertSaga as dalDeleteCertSaga,
  printCertSaga as dalPrintCertSaga,
  downloadActiveCertSaga as dalDownloadActiveCertSaga,
 } from 'dal/sign/sagas/operations-saga';
import { addOrdinaryNotificationAction }  from 'dal/notification/actions';
import { prepareConfForNotification }  from 'utils/errors-util';
import { sendToBankCert } from './send-to-bank-cert-saga';

import { ACTIONS } from '../constants';

import {
  getCertListAction
} from '../actions';

/**
 *
 * @param payload
 */
export function* deleteCertSaga({ payload }) {
  const { id } = payload;
  try {
    yield call(dalDeleteCertSaga, { payload });    
    yield all([
      put(getCertListAction()),
      put({
        type: ACTIONS.CRYPTO_OPERATION_DELETE_SUCCESS,
        payload: id
      })
    ]);
  } catch (error) {
    yield put(addOrdinaryNotificationAction(prepareConfForNotification(error)));
    yield put({ type: ACTIONS.CRYPTO_OPERATION_DELETE_FAIL, error: error.message });
  }
}

/**
 *
 * @param payload
 */
export function* printCertSaga({ payload }) {
  const { id } = payload;
  try {
    yield call(dalPrintCertSaga, { docId: id });
    yield put({
      type: ACTIONS.CRYPTO_OPERATION_PRINT_SUCCESS,
      payload: id
    });
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_OPERATION_PRINT_FAIL, error: error.message });
  }
}

export function* downloadActiveCertSaga({ payload }) {
  const { id } = payload;
  try {
    yield call(dalDownloadActiveCertSaga, { certId: id });
    yield put({
      type: ACTIONS.CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_SUCCESS,
      payload: id
    });
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_FAIL, error: error.message });
  }
}

export function* sendToBankCertSaga({ payload }) {
  const { id, requestType } = payload;
  try {
    yield call(sendToBankCert, {
      payload: {
        docId: id,
        reqType: requestType === 'new' ? 'CryptoProReqNew' : 'CryptoProReqRegen'
      }
    });
    yield all([
      put(getCertListAction()),
      put({
        type: ACTIONS.CRYPTO_OPERATION_SEND_SUCCESS,
        payload: id
      })
    ]);
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_OPERATION_SEND_FAIL, error: error.message });
  }
}
