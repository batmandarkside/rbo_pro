import { put, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { reset } from 'redux-form/immutable';
import {
  FORM_NAME_NEW_CERT
} from '../constants';


export function* resetFormSaga() {
  yield all([
    put(reset(FORM_NAME_NEW_CERT)),
    put(push({ pathname: '/settings/crypto-pro' }))
  ]);
}
