import { call, put } from 'redux-saga/effects';
import { sendToBankCert as dalSendToBankCert } from 'dal/sign/sagas/send-to-bank-cert-saga';
import { ACTIONS } from '../constants';

/**
 *
 * @param payload
 * @returns {*|{}}
 */
export function* sendToBankCert({ payload }) {
  try {
    yield put({ type: ACTIONS.CRYPTO_SEND_TO_BANK_NEW_CERT_REQUEST });
    const data = yield call(dalSendToBankCert, { payload });
    yield put({
      type: ACTIONS.CRYPTO_SEND_TO_BANK_NEW_CERT_SUCCESS,
      payload: {
        data
      }
    });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.CRYPTO_SEND_TO_BANK_NEW_CERT_FAIL });
    throw error;
  }
}
