import { put, all, call } from 'redux-saga/effects';
import { isArray } from 'lodash';
import { addOrdinaryNotificationAction }  from 'dal/notification/actions';
import { signCertSaga as dalSignCertSaga } from 'dal/sign/sagas/sign-saga';
import { prepareConfForNotification }  from 'utils/errors-util';
import {
  ACTIONS
} from '../constants';

import { getCertListAction } from '../actions';
import { sendToBankCert } from './send-to-bank-cert-saga';
/**
 * подпись сертификата старым средством подписи
 * нужно для старых сертификатов созданных в эльбрусе
 * @param payload
 */
export function* signOldCertSaga({ payload }) {
  const item = payload.item;
  const cryptoProfile = item.get('cryptoProfile');
  const signAuthoritiesId = cryptoProfile.getIn(['signAuthorities', 0, 'id']);

  try {
    yield call(dalSignCertSaga, {
      payload: {
        docId: item.get('requestId'),
        signAuthId: signAuthoritiesId || ''
      }
    });

    // отправка в банк
    yield call(sendToBankCert, {
      payload: {
        docId: item.get('requestId'),
        reqType: 'CryptoProReqRegen'
      }
    });

    yield all([
      put(getCertListAction()),
      put({
        type: ACTIONS.CRYPTO_OPERATION_SIGN_OLD_CERT_SUCCESS,
        payload: item.get('id')
      })
    ]);
  } catch (error) {
    if (error && isArray(error)) {
      yield put(addOrdinaryNotificationAction({
        level: 'warning',
        type: 'messagesList',
        title: 'Не удалось подписать сертификат',
        collectionMessages: error
      }));
    } else {
      yield put(addOrdinaryNotificationAction(prepareConfForNotification(error)));
    }
    yield  put({ type: ACTIONS.CRYPTO_OPERATION_SIGN_OLD_CERT_FAIL, error: error.message });
  }
}
