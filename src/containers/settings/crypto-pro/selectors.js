import { createSelector } from 'reselect';
import { Map, List, OrderedMap } from 'immutable';
import {
  getFormValues,
  getFormInitialValues
} from 'redux-form/immutable';
import {
  prepareForDropBox,
  prepareUlkValue,
  prepareCryptoProfilesValue
} from './util';

import {
  COMBINE_REDUCERS,
  CRYPTO_PRO_REDUCER
} from '../constants';

import {
  FORM_NAME_NEW_CERT,
  FIELD_CITY
} from './constants';

const profileSelector = state => state.get('profile');
const formValuesSelector = state => getFormValues(FORM_NAME_NEW_CERT)(state);
const formInitialValuesSelector = state => getFormInitialValues(FORM_NAME_NEW_CERT)(state);

const ulcProfilesSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'ulcProfiles']);

// @todo доделать
// берем первый по дефолту
const cpsProfilesSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'cpsValues']);

const submittingFormSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'submittingForm']);

const newCertStatusesSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'newCertStatuses']);

const createdNewCertDataSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'createdNewCertData']);

const ulcCurrentSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'ulcCurrent']);

const loadCertListLoadSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'loadCertListLoad']);

const certListSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'certListRaw']);

const certListFilteredSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'certListFiltered']);

const sendingToBankSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'sendingToBank']);

const printingNewCertSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'printingNewCert']);

const statusValueSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'filters', 'statusValue']);

const sortValuesSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'sortValues']);

const itemModalRegenerateCertReasonSelector = state =>
  state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'itemModalRegenerateCertReason']);

const locationBeforeTransitionsSelector = state =>
  state.getIn(['routing', 'locationBeforeTransitions']);

export const ulcValuesSelector = createSelector(
  ulcProfilesSelector,
  ulcProfiles => ulcProfiles.get('ulkList').map(ulc => prepareUlkValue(ulc))
);

export const cpsValuesSelector = createSelector(
  cpsProfilesSelector,
  (cpsProfiles) => {
    const copyCpsProfiles = cpsProfiles || List();
    return copyCpsProfiles.map(cps => prepareCryptoProfilesValue(cps));
  }
);

export const findReissueItemSelector = createSelector(
  locationBeforeTransitionsSelector,
  certListFilteredSelector,
  (router, certListFiltered) =>
    certListFiltered.find(c => c.getIn(['cert', 'id']) === router.getIn(['query', 'reissue']))
);


/**
 * статусы для фильтров
 * @type {Reselect.Selector<TInput, TOutput>}
 */
export const statusValuesSelector = createSelector(
  certListSelector,
  (certList) => {
    const certListCopy = certList.size ? certList : List([]);
    let createOrderedMapStatuses = OrderedMap({});
    if (!certListCopy.size) return certListCopy;

    certListCopy.forEach((cert) => {
      if (!createOrderedMapStatuses.has(cert.get('mainStatus'))) {
        createOrderedMapStatuses = createOrderedMapStatuses.set(cert.get('mainStatus'), cert);
      }
    });

    return createOrderedMapStatuses
      .toList()
      .map(c => prepareForDropBox(c, ['cert', 'id'], ['translateStatus']));
  }
);

export const defaultCityNameSelector = createSelector(
  formInitialValuesSelector,
  (formValues) => {
    const values = formValues || Map({});
    const cityName = values.get(FIELD_CITY, false);
    return Boolean(cityName);
  }
);

export const statusCertSelector = createSelector(
  formValuesSelector,
  (formValues) => {
    const values = formValues || Map({});
    return  values.get(FIELD_CITY, '');
  }
);

export const mapStateToProps = state => ({
  profile: profileSelector(state),
  submittingForm: submittingFormSelector(state),
  statusCert: statusCertSelector(state),
  newCertStatuses: newCertStatusesSelector(state),
  createdNewCertData: createdNewCertDataSelector(state),
  ulcCurrent: ulcCurrentSelector(state),
  ulcValues: ulcValuesSelector(state),
  cpsValues: cpsValuesSelector(state),
  certListFiltered: certListFilteredSelector(state),
  findReissueItem: findReissueItemSelector(state),
  loadCertListLoad: loadCertListLoadSelector(state),
  statusValues: statusValuesSelector(state),
  statusValue: statusValueSelector(state),
  sortValue: state.getIn([COMBINE_REDUCERS, CRYPTO_PRO_REDUCER, 'filters', 'sortValue']),
  sortValues: sortValuesSelector(state),
  isHaveDefaultCityName: defaultCityNameSelector(state),
  sendingToBank: sendingToBankSelector(state),
  printingNewCert: printingNewCertSelector(state),
  itemModalRegenerateCertReason: itemModalRegenerateCertReasonSelector(state),
});
