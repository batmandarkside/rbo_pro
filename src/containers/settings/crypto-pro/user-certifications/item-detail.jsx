import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { is } from 'immutable';
import ImmutablePropTypes from 'react-immutable-proptypes';
import moment from 'moment-timezone';
import OperationsButton from './operation-button';

import {
  STATUSES_LABEL,
  ADDITIONAL_STATUS_EXPIRING,
  ADDITIONAL_STATUS_NEW_REG_REGEN,
  ADDITIONAL_STATUS_REISSUE,
  ALLOW_SHOW_CERT_DETAIL,
  ALLOW_SHOW_REQUEST_DETAIL,
  DATA_LOC
} from '../constants';

import {
  Header,
  H2
} from '../../style.js';
import {
  Gray,
  Black,
  BodyDetail,
  HeaderStatusDetail,
  StatusLabelDetail,
  IconAdditionalStatus,
  FooterDetail,
  CertDetail,
  RequestDetail,
  DetailRequestToggler
} from './style';

import {
  ModalExtend,
  CollLabel,
  CollDescription,
  RowFlex,
  Info
} from '../style.js';

class CertItemDetail extends Component {
  static propTypes = {
    item: ImmutablePropTypes.contains({
      ulk: ImmutablePropTypes.contains({
        branchName: PropTypes.string,
        cityName: PropTypes.string,
        country: PropTypes.string,
        email: PropTypes.string,
        id: PropTypes.string,
        name: PropTypes.string,
        orgINN: PropTypes.string,
        orgName: PropTypes.string,
      }),
      requestId: PropTypes.string,
      cert: ImmutablePropTypes.contains({
        cryptoProfile: ImmutablePropTypes.contains({
          id: PropTypes.string,
          name: PropTypes.string
        }),
        dateBegin: PropTypes.string,
        dateEnd: PropTypes.string,
        id: PropTypes.string,
        issuer: PropTypes.string,
        serialNumber: PropTypes.string,
        statusChangeDate: PropTypes.string,
        statusChangeReason: PropTypes.string
      }),
      requestNumber: PropTypes.string,
      requestDate: PropTypes.string,
      mainStatus: PropTypes.string,
      additionalStatus: PropTypes.string,
      operDelete: PropTypes.string,
      operSign: PropTypes.string,
      operSend: PropTypes.string
    }).isRequired,
    closeModalItem: PropTypes.func.isRequired,
    onDeleteCert: PropTypes.func.isRequired,
    onSendToBankCert: PropTypes.func.isRequired,
    onPrintCertAction: PropTypes.func.isRequired,
    onDownloadCertAction: PropTypes.func.isRequired,
    onSignOldCertAction: PropTypes.func.isRequired,
    onCreateNewReissueCert: PropTypes.func.isRequired,
  }

  state = {
    isShowDetail: false
  }

  shouldComponentUpdate(nextProps, nextState) {
    const item = !is(nextProps.item, this.props.item);
    const isShowDetail = nextState.isShowDetail !== this.state.isShowDetail;

    return item || isShowDetail;
  }

  toggleRequestDetail = () =>
    this.setState({ isShowDetail: !this.state.isShowDetail })

  render() {
    const {
      onDeleteCert,
      onSendToBankCert,
      closeModalItem,
      onPrintCertAction,
      onSignOldCertAction,
      onDownloadCertAction,
      onCreateNewReissueCert,
      item
    } = this.props;

    const { isShowDetail } = this.state;

    // показываем CertDetail только для определенных статусов сертификата
    const isShowCertDetail = ALLOW_SHOW_CERT_DETAIL.includes(item.get('mainStatus'));
    const isShowRequestDetail = ALLOW_SHOW_REQUEST_DETAIL.includes(item.get('entityType'));

    return (
      <ModalExtend
        className="modal-create-cert"
        classNameContent="modal-create-cert-content"
        onClose={closeModalItem}
        data-loc={DATA_LOC.DATA_LOC_DETAIL_MODAL_CERT}
      >
        <Header>
          <H2>Сертификат</H2>
          <HeaderStatusDetail>
            {item.get('requestDate') && (
              <span>
                <Gray>От:</Gray> <Black>{moment(item.get('requestDate')).format('DD.MM.YYYY')}</Black> ·
              </span>
            )}
            <Gray> Статус:</Gray>
            <StatusLabelDetail
              status={item.get('mainStatus')}
              additionalStatus={item.get('additionalStatus')}
              data-loc={`ITEM-STATUS-DETAIL-${item.get('mainStatus')}`}
            >
              {STATUSES_LABEL[item.get('mainStatus')]}
              {item.get('additionalStatus') === ADDITIONAL_STATUS_EXPIRING && (
                <IconAdditionalStatus type="clock" size="16" />
              )}
              {item.get('additionalStatus') === ADDITIONAL_STATUS_NEW_REG_REGEN && (
                <IconAdditionalStatus type="context-round" size="16" />
              )}
              {item.get('additionalStatus') === ADDITIONAL_STATUS_REISSUE && (
                <IconAdditionalStatus type="repeat" size="16" />
              )}
            </StatusLabelDetail>
          </HeaderStatusDetail>
        </Header>
        <BodyDetail>
          {item.get('importantMessage') && (
            <Info>{item.get('importantMessage').map((m, i) => <div key={i}>{m}</div>)}</Info>
          )}
          {isShowCertDetail && (
            <CertDetail>
              <RowFlex>
                <CollLabel>Дата выпуска</CollLabel>
                {item.getIn(['cert', 'dateBegin']) && (
                  <CollDescription>{moment(item.getIn(['cert', 'dateBegin'])).format('DD.MM.YYYY')}</CollDescription>
                )}
              </RowFlex>
              <RowFlex>
                <CollLabel>Дата окончания</CollLabel>
                {item.getIn(['cert', 'dateEnd']) && (
                  <CollDescription>{moment(item.getIn(['cert', 'dateEnd'])).format('DD.MM.YYYY')}</CollDescription>
                )}
              </RowFlex>
              <RowFlex>
                <CollLabel>Серийный номер</CollLabel>
                <CollDescription>{item.getIn(['cert', 'serialNumber']).toUpperCase()}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Издатель</CollLabel>
                <CollDescription>{item.getIn(['cert', 'issuer'])}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Средство подписи</CollLabel>
                <CollDescription>{item.getIn(['cert', 'cryptoProfileName'])}</CollDescription>
              </RowFlex>
            </CertDetail>
          )}

          {(isShowCertDetail && isShowRequestDetail) &&
            <DetailRequestToggler onClick={this.toggleRequestDetail}>
              {isShowDetail ? 'Скрыть' : 'Показать' } запрос
            </DetailRequestToggler>
          }

          {(!isShowCertDetail || isShowDetail) && (
            <RequestDetail>
              <RowFlex>
                <CollLabel>Номер запроса</CollLabel>
                <CollDescription>{item.get('requestNumber')}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Дата запроса</CollLabel>
                <CollDescription>{moment(item.get('requestDate')).format('DD.MM.YYYY')}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Уполномоченное лицо</CollLabel>
                <CollDescription>{item.getIn(['ulk', 'name'])}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Средство подписи</CollLabel>
                <CollDescription>{item.getIn(['cryptoProfile', 'name'])}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Email</CollLabel>
                <CollDescription>{item.getIn(['ulk', 'email'])}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Город</CollLabel>
                <CollDescription>{item.getIn(['ulk', 'cityName'])}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Страна</CollLabel>
                <CollDescription>{item.getIn(['ulk', 'country'])}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Организация</CollLabel>
                <CollDescription>{item.getIn(['ulk', 'orgName'])}</CollDescription>
              </RowFlex>
              <RowFlex>
                <CollLabel>Подразделение</CollLabel>
                <CollDescription>{item.getIn(['ulk', 'branchName'])}</CollDescription>
              </RowFlex>
            </RequestDetail>
          )}
        </BodyDetail>
        <FooterDetail>
          <OperationsButton
            operations={item.get('operations')}
            onDeleteCert={onDeleteCert}
            onSendToBankCert={onSendToBankCert}
            onSignOldCertAction={onSignOldCertAction}
            onPrintCertAction={onPrintCertAction}
            onDownloadCertAction={onDownloadCertAction}
            onCreateNewReissueCert={onCreateNewReissueCert}
          />
        </FooterDetail>
      </ModalExtend>
    );
  }
}

export default CertItemDetail;

