import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { is } from 'immutable';
import moment from 'moment-timezone';
import Loader from '@rbo/components/lib/loader/Loader';
import {
  STATUSES_LABEL,
  ADDITIONAL_STATUS_EXPIRING,
  ADDITIONAL_STATUS_NEW_REG_REGEN,
  ADDITIONAL_STATUS_REISSUE
} from '../constants';

import OperationsIcon from './operation-icon';
import CertItemDetail from './item-detail';

import {
  Item,
  ItemInner,
  CollItemLeft,
  CollItemRight,
  StatusLabel,
  ItemBorder,
  DateItem,
  Name,
  NameTitle,
  CompanyLabel,
  CompanyName,
  IconUser,
  IconAdditionalStatus,
} from './style.js';

class CertItem extends Component {
  static propTypes = {
    item: ImmutablePropTypes.contains({
      ulk: ImmutablePropTypes.contains({
        branchName: PropTypes.string,
        cityName: PropTypes.string,
        country: PropTypes.string,
        email: PropTypes.string,
        id: PropTypes.string,
        name: PropTypes.string,
        orgINN: PropTypes.string,
        orgName: PropTypes.string,
      }),
      requestId: PropTypes.string,
      cert: ImmutablePropTypes.contains({
        cryptoProfile: ImmutablePropTypes.contains({
          id: PropTypes.string,
          name: PropTypes.string
        }),
        dateBegin: PropTypes.string,
        dateEnd: PropTypes.string,
        id: PropTypes.string,
        issuer: PropTypes.string,
        serialNumber: PropTypes.string,
        statusChangeDate: PropTypes.string,
        statusChangeReason: PropTypes.string
      }),
      requestNumber: PropTypes.string,
      requestDate: PropTypes.string,
      mainStatus: PropTypes.string,
      additionalStatus: PropTypes.string,
      operDelete: PropTypes.string,
      operSign: PropTypes.string,
      operSend: PropTypes.string,
      pending: PropTypes.bool,
    }).isRequired,
    deleteCertAction: PropTypes.func.isRequired,
    sendToBankCertAction: PropTypes.func.isRequired,
    signCertAction: PropTypes.func.isRequired,
    printNewCertAction: PropTypes.func.isRequired,
    onDownloadCertAction: PropTypes.func.isRequired,
    changeUrlForReissueCertAction: PropTypes.func.isRequired,
  }

  state = {
    isMouseEnter: false,
    showDetail: false,
  }

  shouldComponentUpdate(nextProps, nextState) {
    const item = !is(nextProps.item, this.props.item);
    const isMouseEnter = nextState.isMouseEnter !== this.state.isMouseEnter;
    const showDetail = nextState.showDetail !== this.state.showDetail;
    return item || isMouseEnter || showDetail;
  }

  onMouseEnter = () => {
    this.setState({ isMouseEnter: true });
  }


  onMouseLeave = () => {
    this.setState({ isMouseEnter: false });
  }

  onDeleteCert = () => {
    const { deleteCertAction, item } = this.props;
    deleteCertAction(item.get('requestId'), item.get('requestType'));
  }

  onDeleteCertFromDetail = () => {
    const { deleteCertAction, item } = this.props;
    deleteCertAction(item.get('requestId'), item.get('requestType'));
    this.onCloseDetail();
  }

  onCreateNewReissueCert = () => {
    const { changeUrlForReissueCertAction, item } = this.props;
    changeUrlForReissueCertAction(item);
  }

  onSendToBankCert = () => {
    const { sendToBankCertAction, item } = this.props;
    sendToBankCertAction(item.get('requestId'), item.get('requestType'));
  }

  onSignOldCertAction = () => {
    const { signCertAction, item } = this.props;
    signCertAction(item, item.get('requestId'));
  }

  onShowDetail = () => {
    this.setState({ showDetail: true });
  }

  onCloseDetail = () => {
    this.setState({ showDetail: false });
  }

  onPrintCertAction = () => {
    const { printNewCertAction, item } = this.props;
    printNewCertAction(item.get('requestId'));
  }

  onDownloadCertAction = () => {
    const { onDownloadCertAction, item } = this.props;
    onDownloadCertAction(item.getIn(['cert', 'id']));
  }
  
  render() {
    const { item } = this.props;
    const { isMouseEnter, showDetail } = this.state;

    return (
      <Item onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} data-loc={`ITEM-${item.get('mainStatus')}`}>
        {item.get('pending') && <Loader centered />}
        <ItemInner isMouseEnter={isMouseEnter} onClick={this.onShowDetail} pending={item.get('pending')}>
          <ItemBorder item={item} status={item.get('mainStatus')} additionalStatus={item.get('additionalStatus')} />
          <CollItemLeft>
            <Name>
              <IconUser type="user" size="20" />
              <NameTitle>
                {item.getIn(['ulk', 'name'])}
              </NameTitle>
            </Name>
            <CompanyName>
              <CompanyLabel>Компания:</CompanyLabel> {item.getIn(['ulk', 'orgName'])}
            </CompanyName>
          </CollItemLeft>
          <CollItemRight>
            <StatusLabel
              status={item.get('mainStatus')}
              data-loc={`ITEM-STATUS-${item.get('mainStatus')}`}
              additionalStatus={item.get('additionalStatus')}
              entityType={item.get('entityType')}
            >
              {STATUSES_LABEL[item.get('mainStatus')]}
              {item.get('additionalStatus') === ADDITIONAL_STATUS_EXPIRING && (
                <IconAdditionalStatus type="clock" size="16" alt="истекает" title="истекает" />
              )}
              {item.get('additionalStatus') === ADDITIONAL_STATUS_NEW_REG_REGEN && (
                <IconAdditionalStatus
                  size="16"
                  type="context-round"
                  alt="создан запрос на перевыпуск"
                  title="создан запрос на перевыпуск"
                />
              )}
              {item.get('additionalStatus') === ADDITIONAL_STATUS_REISSUE && (
                <IconAdditionalStatus size="16" type="repeat" alt="перевыпуск" title="перевыпуск" />
              )}
            </StatusLabel>
            {item.getIn(['cert', 'id']) && (
              <div>
                <DateItem>Дата выпуска: {moment(item.getIn(['cert', 'dateBegin'])).format('DD.MM.YYYY')}</DateItem>
                <DateItem>Дата окончания: {moment(item.getIn(['cert', 'dateEnd'])).format('DD.MM.YYYY')}</DateItem>
              </div>
            )}
            {!item.getIn(['cert', 'id']) && (
              <DateItem>Дата запроса: {moment(item.get('requestDate')).format('DD.MM.YYYY')}</DateItem>
            )}
          </CollItemRight>
        </ItemInner>
        <OperationsIcon
          pending={item.get('pending')}
          operations={item.get('operations')}
          isMouseEnter={isMouseEnter}
          onDeleteCert={this.onDeleteCert}
          onCreateNewReissueCert={this.onCreateNewReissueCert}
          onSendToBankCert={this.onSendToBankCert}
          onSignOldCertAction={this.onSignOldCertAction}
          onPrintCertAction={this.onPrintCertAction}
          onDownloadCertAction={this.onDownloadCertAction}
        />

        {showDetail && (
          <CertItemDetail
            item={item}
            onDeleteCert={this.onDeleteCertFromDetail}
            onCreateNewReissueCert={this.onCreateNewReissueCert}
            onSendToBankCert={this.onSendToBankCert}
            onSignOldCertAction={this.onSignOldCertAction}
            closeModalItem={this.onCloseDetail}
            onPrintCertAction={this.onPrintCertAction}
            onDownloadCertAction={this.onDownloadCertAction}
          />
        )}
      </Item>
    );
  }
}

export default CertItem;

