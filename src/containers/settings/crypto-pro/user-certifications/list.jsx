import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List, Map, is } from 'immutable';
import Loader from '@rbo/components/lib/loader/Loader';
import * as LocalAction from '../actions';
import { mapStateToProps } from '../selectors';
import CertItem from './item';
import {
  ListBlock
} from './style.js';

class CertList extends Component {
  
  static propTypes = {
    certListFiltered: PropTypes.instanceOf(List),
    findReissueItem: PropTypes.instanceOf(Map),
    loadCertListLoad: PropTypes.bool,
    getCertListAction: PropTypes.func,
    deleteCertAction: PropTypes.func,
    repeatCertAction: PropTypes.func,
    sendToBankCertAction: PropTypes.func,
    signCertAction: PropTypes.func,
    reissueId: PropTypes.string,
    printNewCertAction: PropTypes.func,
    onDownloadCertAction: PropTypes.func,
    reissueCertStartAction: PropTypes.func,
    changeUrlForReissueCertAction: PropTypes.func,
  }

  componentDidMount() {
    this.props.getCertListAction();
  }


  componentWillReceiveProps(nextProps) {
    const { findReissueItem, reissueCertStartAction } = nextProps;
    if (findReissueItem) {
      reissueCertStartAction(findReissueItem);
    }
  }

  shouldComponentUpdate(nextProps) {
    const certListFiltered = !is(nextProps.certListFiltered, this.props.certListFiltered);
    const loadCertListLoad = nextProps.loadCertListLoad !== this.props.loadCertListLoad;
    const reissueId = nextProps.reissueId !== this.props.reissueId;

    return certListFiltered || loadCertListLoad || reissueId;
  }
  
  render() {
    const {
      certListFiltered,
      loadCertListLoad
    } = this.props;

    return (
      <ListBlock>
        {loadCertListLoad && <Loader centered />}
        {certListFiltered.map((item, i) =>
          <CertItem
            item={item}
            key={i}
            signCertAction={this.props.signCertAction}
            deleteCertAction={this.props.deleteCertAction}
            repeatCertAction={this.props.repeatCertAction}
            printNewCertAction={this.props.printNewCertAction}
            onDownloadCertAction={this.props.onDownloadCertAction}
            sendToBankCertAction={this.props.sendToBankCertAction}
            reissueCertStartAction={this.props.reissueCertStartAction}
            changeUrlForReissueCertAction={this.props.changeUrlForReissueCertAction}
          />
        )}
      </ListBlock>
    );
  }
}


export default connect(
  mapStateToProps,
  {
    ...LocalAction
  }
)(CertList);

