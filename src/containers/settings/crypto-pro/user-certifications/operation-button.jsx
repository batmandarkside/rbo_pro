import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { DATA_LOC } from '../constants';
import {
  ButtonOperation,
  OperationsButtonContainer
} from './style.js';

const OperationsButton = ({
  operations,
  onDeleteCert,
  onSendToBankCert,
  onPrintCertAction,
  onDownloadCertAction,
  onSignOldCertAction,
  onCreateNewReissueCert,
}) => (
  <OperationsButtonContainer>
    {operations.get('sign') && (
      <ButtonOperation
        primary
        size="large"
        onClick={onSignOldCertAction}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_SIGN_OLD_CERT}
      >Подписать
      </ButtonOperation>
    )}

    {operations.get('repeat') && (
      <ButtonOperation
        primary
        size="large"
        onClick={onCreateNewReissueCert}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_REPEAT}
      >Повторить
      </ButtonOperation>
    )}

    {operations.get('send') && (
      <ButtonOperation
        primary
        size="large"
        onClick={onSendToBankCert}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_SEND}
      >Отправить в банк
      </ButtonOperation>
    )}

    {operations.get('remove') && (
      <ButtonOperation
        size="large"
        onClick={onDeleteCert}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_REMOVE}
      >Удалить
      </ButtonOperation>
    )}
    {operations.get('print') && (
      <ButtonOperation
        primary
        size="large"
        onClick={onPrintCertAction}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_PRINT}
      >Печатать
      </ButtonOperation>
    )}

    {operations.get('download') && (
      <ButtonOperation
        size="large"
        onClick={onDownloadCertAction}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_DOWNLOAD}
      >Скачать
      </ButtonOperation>
    )}
  </OperationsButtonContainer>
);

OperationsButton.propTypes = {
  operations: ImmutablePropTypes.contains({
    repeat: PropTypes.bool,
    remove: PropTypes.bool,
    send: PropTypes.bool,
    sign: PropTypes.bool,
    print: PropTypes.bool,
    download: PropTypes.bool,
  }),
  onDeleteCert: PropTypes.func.isRequired,
  onSendToBankCert: PropTypes.func.isRequired,
  onPrintCertAction: PropTypes.func.isRequired,
  onDownloadCertAction: PropTypes.func.isRequired,
  onSignOldCertAction: PropTypes.func.isRequired,
  onCreateNewReissueCert: PropTypes.func.isRequired
};

export default OperationsButton;

