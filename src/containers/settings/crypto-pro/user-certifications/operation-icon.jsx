import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { DATA_LOC } from '../constants';
import {
  IconOperations,
  Operations
} from './style.js';

const OperationsIcon = ({
  isMouseEnter,
  operations,
  onDeleteCert,
  onSendToBankCert,
  onPrintCertAction,
  onDownloadCertAction,
  onSignOldCertAction,
  onCreateNewReissueCert,
}) => (
  <Operations isMouseEnter={isMouseEnter}>
    {operations.get('repeat') && (
      <IconOperations
        type="repeat"
        size="20"
        title="повторить"
        onClick={onCreateNewReissueCert}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_REPEAT}
      />
    )}

    {operations.get('remove') && (
      <IconOperations
        type="remove"
        size="20"
        title="удалить"
        onClick={onDeleteCert}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_REMOVE}
      />
    )}

    {operations.get('send') && (
      <IconOperations
        type="send"
        size="20"
        title="отправить в банк"
        onClick={onSendToBankCert}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_SEND}
      />
    )}

    {operations.get('print') && (
      <IconOperations
        type="print"
        size="20"
        title="распечатать"
        onClick={onPrintCertAction}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_PRINT}
      />
    )}

    {operations.get('sign') && (
      <IconOperations
        type="sign"
        size="20"
        title="подписать"
        data-loc={DATA_LOC.DATA_LOC_OPERATION_SIGN_OLD_CERT}
        onClick={onSignOldCertAction}
      />
    )}

    {operations.get('download') && (
      <IconOperations
        type="save"
        size="20"
        title="скачать"
        onClick={onDownloadCertAction}
        data-loc={DATA_LOC.DATA_LOC_OPERATION_DOWNLOAD}
      />
    )}
  </Operations>
);

OperationsIcon.propTypes = {
  isMouseEnter: PropTypes.bool,
  operations: ImmutablePropTypes.contains({
    repeat: PropTypes.bool,
    remove: PropTypes.bool,
    send: PropTypes.bool,
    sign: PropTypes.bool,
    print: PropTypes.bool,
    download: PropTypes.bool,
  }),
  onDeleteCert: PropTypes.func.isRequired,
  onSendToBankCert: PropTypes.func.isRequired,
  onPrintCertAction: PropTypes.func.isRequired,
  onDownloadCertAction: PropTypes.func.isRequired,
  onSignOldCertAction: PropTypes.func.isRequired,
  onCreateNewReissueCert: PropTypes.func.isRequired,
};

export default OperationsIcon;

