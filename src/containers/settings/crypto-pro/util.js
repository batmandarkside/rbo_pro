import { Map } from 'immutable';

/**
 *
 * @param item
 * @param valueKey
 * @param titleKey
 */
export const prepareForDropBox = (item, valueKey, titleKey) => Map({
  value: item.getIn(valueKey),
  title: item.getIn(titleKey),
  ...item.toJS()
});


export const prepareUlkValue = ulkList => prepareForDropBox(ulkList, ['ulk', 'id'], ['orgName']);
export const prepareSignAuthorities = ulkList => prepareForDropBox(ulkList, ['id'], ['name']);
export const prepareCryptoProfilesValue = cryptoProfilesList => prepareForDropBox(cryptoProfilesList, ['id'], ['name']);
