import React from 'react';
import PropTypes from 'prop-types';

export const FieldTextInline = ({ input }) => <span data-loc={input.name}>{input.value}</span>;

FieldTextInline.propTypes = {
  input: PropTypes.object
};

