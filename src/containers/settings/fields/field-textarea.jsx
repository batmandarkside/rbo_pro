import React from 'react';
import PropTypes from 'prop-types';
import {
  Label,
  FlexRowAlignStart,
  ErrorsWarning
} from './style';

import { TextArea } from '../style';

/**
 *
 * @param input
 * @param type
 * @param placeholder
 * @param meta
 * @param label
 * @returns {XML}
 * @constructor
 */
export const FieldTextArea = ({ input, type, placeholder, meta, label, autoFocus, disabled, maxLength = 255 }) => {
  const { touched, warning, invalid, error } = meta;
  return (
    <FlexRowAlignStart data-loc={input.name}>
      {label && (
        <Label>{label}</Label>
      )}
      <TextArea
        {...input}
        disabled={disabled}
        autoFocus={autoFocus}
        maxLength={maxLength}
        type={type}
        placeholder={placeholder}
        error={invalid && error}
      />

      {touched && (
        (warning && <ErrorsWarning>{warning}</ErrorsWarning>)
      )}
    </FlexRowAlignStart>
  );
};

FieldTextArea.propTypes = {
  autoFocus: PropTypes.bool,
  disabled: PropTypes.bool,
  maxLength: PropTypes.number,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string
    ]),
    warning: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string
    ])
  }),
  placeholder: PropTypes.string,
  label: PropTypes.string,
  input: PropTypes.object,
  type: PropTypes.string
};

