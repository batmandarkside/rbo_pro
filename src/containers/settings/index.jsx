import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import * as NotificationAction  from 'dal/notification/actions';
import * as ProfileAction       from 'dal/profile/actions';
import { mapStateToProps }      from './selectors';
import sagaCryptoPro from './crypto-pro/sagas';
import sagaPassword from './password/sagas';
import reducer from './combine-reducers';
import SettingsLayout from './container';

const withConnect = connect(
  mapStateToProps,
  {
    ...NotificationAction,
    ...ProfileAction
  }
);
const withReducer = injectReducer({ key: 'SettingsCombineReducer', reducer });
const withSagaPassword = injectSaga({ key: 'settingsPasswordContainerSagas', saga: sagaPassword });
const withSagaCryptoPro = injectSaga({ key: 'settingsCryptoProContainerSagas', saga: sagaCryptoPro });

export default compose(
  withReducer,
  withSagaPassword,
  withSagaCryptoPro,
  withConnect,
)(SettingsLayout);

