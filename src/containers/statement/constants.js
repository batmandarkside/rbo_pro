import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  STATEMENT_CONTAINER_GET_STATEMENT_REQUEST: null,
  STATEMENT_CONTAINER_GET_STATEMENT_SUCCESS: null,
  STATEMENT_CONTAINER_GET_STATEMENT_FAIL: null,
  STATEMENT_CONTAINER_GET_SETTINGS_REQUEST: null,
  STATEMENT_CONTAINER_GET_SETTINGS_SUCCESS: null,
  STATEMENT_CONTAINER_GET_QUERY_PARAMS_REQUEST: null,
  STATEMENT_CONTAINER_GET_QUERY_PARAMS_SUCCESS: null,
  STATEMENT_CONTAINER_CHANGE_PAGE: null,
  STATEMENT_CONTAINER_RELOAD_PAGES: null,
  STATEMENT_CONTAINER_CHANGE_FILTERS: null,
  STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST: null,
  STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS: null,
  STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL: null,
  STATEMENT_CONTAINER_SAVE_SETTINGS_REQUEST: null,
  STATEMENT_CONTAINER_SAVE_SETTINGS_SUCCESS: null,
  STATEMENT_CONTAINER_SAVE_SETTINGS_FAIL: null,
  STATEMENT_CONTAINER_GET_LIST_REQUEST: null,
  STATEMENT_CONTAINER_GET_LIST_SUCCESS: null,
  STATEMENT_CONTAINER_GET_LIST_FAIL: null,
  STATEMENT_CONTAINER_EXPORT_LIST_REQUEST: null,
  STATEMENT_CONTAINER_EXPORT_LIST_SUCCESS: null,
  STATEMENT_CONTAINER_EXPORT_LIST_FAIL: null,
  STATEMENT_CONTAINER_UNMOUNT_REQUEST: null,
  STATEMENT_CONTAINER_RESIZE_COLUMNS: null,
  STATEMENT_CONTAINER_CHANGE_COLUMN: null,
  STATEMENT_CONTAINER_CHANGE_GROUPING: null,
  STATEMENT_CONTAINER_CHANGE_SORTING: null,
  STATEMENT_CONTAINER_CHANGE_PAGINATION: null,
  STATEMENT_CONTAINER_CHANGE_SELECT: null,

  STATEMENT_CONTAINER_OPERATION: null,
  STATEMENT_CONTAINER_OPERATION_PRINT_REQUEST: null,
  STATEMENT_CONTAINER_OPERATION_PRINT_SUCCESS: null,
  STATEMENT_CONTAINER_OPERATION_PRINT_FAIL: null,

  STATEMENT_CONTAINER_PRINT_STATEMENT_REQUEST: null,
  STATEMENT_CONTAINER_PRINT_STATEMENT_SUCCESS: null,
  STATEMENT_CONTAINER_PRINT_STATEMENT_FAIL: null
});

export const LOCAL_REDUCER = 'StatementContainerReducer';

export const LOCAL_ROUTER_ALIAS = '/statements';

export const SETTINGS_ALIAS = 'StatementContainer';

export const VALIDATE_SETTINGS = settings => !!settings &&
  !!settings.visible &&
  !!settings.sorting &&
  !!settings.pagination &&
  !!settings.width &&
  !!settings.width.operDate &&
  !!settings.width.valDate &&
  !!settings.width.operType &&
  !!settings.width.operNumber &&
  !!settings.width.debit &&
  !!settings.width.credit &&
  !!settings.width.corrName &&
  !!settings.width.inn &&
  !!settings.width.corrAccount &&
  !!settings.width.corrBankName &&
  !!settings.width['debit-credit'] &&
  !!settings.width.paymentPurpose;

export const LOCATORS = {
  CONTAINER: 'statementContainer',
  SPREADSHEET: 'statementContainerSpreadsheet',

  TOOLBAR: 'statementContainerSpreadsheetToolbar',
  TOOLBAR_BUTTONS: 'statementContainerSpreadsheetToolbarButtons',
  TOOLBAR_BUTTON_REFRESH_LIST: 'statementContainerSpreadsheetToolbarButtonRefreshList',
  TOOLBAR_BUTTON_PRINT_STATEMENT: 'statementContainerSpreadsheetToolbarButtonPrintStatement',

  FILTERS: 'statementContainerSpreadsheetFilters',
  FILTERS_SELECT: 'statementContainerSpreadsheetFiltersSelect',
  FILTERS_ADD: 'statementContainerSpreadsheetFiltersAdd',
  FILTERS_REMOVE: 'statementContainerSpreadsheetFiltersRemove',
  FILTERS_OPERATIONS: 'statementContainerSpreadsheetFiltersOperations',
  FILTERS_OPERATION_SAVE: 'statementContainerSpreadsheetFiltersOperationSave',
  FILTERS_OPERATION_SAVE_AS: 'statementContainerSpreadsheetFiltersOperationSaveAs',
  FILTERS_OPERATION_SAVE_AS_POPUP: 'statementContainerSpreadsheetFiltersOperationSaveAsPopup',
  FILTERS_OPERATION_CLEAR: 'statementContainerSpreadsheetFiltersOperationClear',
  FILTERS_CONDITIONS: 'statementContainerSpreadsheetFiltersConditions',
  FILTERS_CONDITION: 'statementContainerSpreadsheetFiltersCondition',
  FILTERS_CONDITION_POPUP: 'statementContainerSpreadsheetFiltersConditionPopup',
  FILTERS_CONDITION_REMOVE: 'statementContainerSpreadsheetFiltersConditionRemove',
  FILTERS_ADD_CONDITION: 'statementContainerSpreadsheetFiltersAddCondition',
  FILTERS_ADD_CONDITION_POPUP: 'statementContainerSpreadsheetFiltersAddConditionPopup',
  FILTERS_CLOSE: 'statementContainerSpreadsheetFiltersClose',

  OPERATIONS: 'statementContainerSpreadsheetOperations',

  TABLE: 'statementContainerSpreadsheetTable',
  TABLE_HEAD: 'statementContainerSpreadsheetTableHead',
  TABLE_BODY: 'statementContainerSpreadsheetTableBody',
  TABLE_SETTINGS: 'statementContainerSpreadsheetTableSettings',
  TABLE_SETTINGS_TOGGLE: 'statementContainerSpreadsheetTableSettingsToggle',
  TABLE_SETTINGS_TOGGLE_BUTTON: 'statementContainerSpreadsheetTableSettingsToggleButton',
  TABLE_SETTINGS_POPUP: 'statementContainerSpreadsheetTableSettingsPopup',

  SELECTION: 'statementContainerSpreadsheetSelection',
  EXPORT: 'statementContainerSpreadsheetExport',
  PAGINATION: 'statementContainerSpreadsheetPagination'
};
