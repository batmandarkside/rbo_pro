import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import { Link }  from 'react-router-dom';
import Loader from '@rbo/components/lib/loader/Loader';
import Icon from 'components/ui-components/icon';
import Spreadsheet, {
  Toolbar,
  Filters,
  Operations,
  Table,
  Selection,
  Export,
  Pagination
} from 'components/ui-components/spreadsheet';
import { RboFormFieldPopupOptionInner } from 'components/ui-components/rbo-form-compact';
import { LOCATORS, LOCAL_ROUTER_ALIAS } from './constants';
import './style.css';

class StatementContainer extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired
    }),

    isStatementLoading: PropTypes.bool.isRequired,
    isStatementPrinting: PropTypes.bool.isRequired,
    isExportListLoading: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,

    statementType: PropTypes.bool.isRequired,
    statementTitle: PropTypes.string.isRequired,
    statementInfo: PropTypes.instanceOf(Map).isRequired,
    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    operations: PropTypes.instanceOf(List).isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,
    panelOperations: PropTypes.instanceOf(List).isRequired,
    selectedItems: PropTypes.instanceOf(List).isRequired,
    selectedItemsSumAmount: PropTypes.string.isRequired,

    getStatementAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getDictionaryForFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    exportListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeGroupingAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
    changeSelectAction: PropTypes.func.isRequired,
    operationAction: PropTypes.func.isRequired,
    printStatementAction: PropTypes.func.isRequired
  };

  componentWillMount() {
    const { match: { params: { id } } } = this.props;
    this.props.getStatementAction(id);
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  createToolbarButtons = () => {
    const { statementType, isStatementPrinting, isListLoading, getListAction, printStatementAction } = this.props;

    const printButton = statementType ?
    {
      id: 'printStatement',
      isDisabled: isStatementPrinting,
      type: 'multiple',
      title: 'Распечатать',
      onClick: printStatementAction,
      items: [
        {
          isDefault: true,
          id: 'withoutDetails',
          type: 'simple',
          title: 'Выписка'
        },
        {
          id: 'withDetails',
          type: 'simple',
          title: 'Выписка с приложениями'
        }
      ]
    }
    :
    {
      id: 'printStatement',
      isDisabled: isStatementPrinting,
      type: 'simple',
      title: 'Распечатать',
      onClick: printStatementAction
    };

    return [
      {
        id: 'refreshList',
        isProgress: isListLoading,
        type: 'refresh',
        title: 'Обновить',
        onClick: getListAction
      },
      printButton
    ];
  };

  filterDictionaryFormFieldOptionRenderer = ({ conditionId, option, highlight }) => {
    switch (conditionId) {
      case 'account':
        return (
          <RboFormFieldPopupOptionInner
            title={option && option.account}
            describe={option && option.orgName}
            extra={option && option.availableBalance}
            highlight={{
              value: highlight,
              inTitle: option && option.account
            }}
          />
        );
      case 'currCodeIso':
        return (
          <RboFormFieldPopupOptionInner
            label={option && option.isoCode}
            describe={option && option.name}
            highlight={{
              value: highlight,
              inLabel: option && option.isoCode,
              inDescribe: option && option.name
            }}
          />
        );
      default:
        return (
          <RboFormFieldPopupOptionInner isInline label={option && option.title} />
        );
    }
  };

  renderItemAlignRightElement = (value) => {
    const style = { textAlign: 'right' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemAlignCenterElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>{value}</div>
    );
  };

  renderItemBoolElement = (value) => {
    const style = { textAlign: 'center' };
    return (
      <div style={style}>
        <Icon
          type={value ? 'bool-true' : 'bool-false'}
          size="16"
        />
      </div>
    );
  };

  renderTitleElement = () => (
    <h2>
      <Link to={LOCAL_ROUTER_ALIAS}>
        Выписки
      </Link>
      {' '}
      <Icon type="arrow-right" size="24" />
      {' '}
      <span>{this.props.statementTitle}</span>
    </h2>
  );

  render() {
    const {
      isStatementLoading,
      isExportListLoading,
      isListLoading,
      statementInfo,
      columns,
      filtersConditions,
      filters,
      list,
      operations,
      pages,
      paginationOptions,
      panelOperations,
      selectedItems,
      selectedItemsSumAmount,
      exportListAction,
      changeColumnsAction,
      changeGroupingAction,
      changeFiltersAction,
      getDictionaryForFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSelectAction,
      changeSortingAction,
      operationAction,
      resizeColumnsAction
    } = this.props;

    return (
      <div className="b-statement-container" data-loc={LOCATORS.CONTAINER}>
        {isStatementLoading && <Loader centered />}
        {!isStatementLoading &&
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                  printStatement: LOCATORS.TOOLBAR_BUTTON_PRINT_STATEMENT
                }
              }}
              renderTitle={this.renderTitleElement}
              buttons={this.createToolbarButtons()}
            />
            <div className="b-statement-container__info">
              <div className="b-statement-container__info-block">
                <div className="b-statement-container__info-label">Входящий остаток:</div>
                <div className="b-statement-container__info-value">{statementInfo.get('opBalance')}</div>
              </div>
              <div className="b-statement-container__info-block">
                <div className="b-statement-container__info-label">Итого по дебету:</div>
                <div className="b-statement-container__info-value">{statementInfo.get('turnoverD')}</div>
              </div>
              <div className="b-statement-container__info-block">
                <div className="b-statement-container__info-label">Итого по кредиту:</div>
                <div className="b-statement-container__info-value">{statementInfo.get('turnoverC')}</div>
              </div>
              <div className="b-statement-container__info-block">
                <div className="b-statement-container__info-label">Исходящий остаток:</div>
                <div className="b-statement-container__info-value">{statementInfo.get('closingBalance')}</div>
              </div>
            </div>
            {selectedItems.size ?
              <Operations
                dataLoc={LOCATORS.OPERATIONS}
                operations={panelOperations}
                noAllowedOperationMessage="Нет доступных действий над выделенными операциями"
                operationAction={operationAction}
                selectedItems={selectedItems}
              />
              :
              <Filters
                dataLocs={{
                  main: LOCATORS.FILTERS,
                  select: LOCATORS.FILTERS_SELECT,
                  add: LOCATORS.FILTERS_ADD,
                  remove: LOCATORS.FILTERS_REMOVE,
                  operations: LOCATORS.FILTERS_OPERATIONS,
                  operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                  operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                  operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                  operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                  conditions: LOCATORS.FILTERS_CONDITIONS,
                  condition: LOCATORS.FILTERS_CONDITION,
                  conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                  conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                  addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                  addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                  close: LOCATORS.FILTERS_CLOSE
                }}
                conditions={filtersConditions}
                filters={filters}
                changeFiltersAction={changeFiltersAction}
                dictionaryFormFieldOptionRenderer={this.filterDictionaryFormFieldOptionRenderer}
                onConditionDictionaryFormInputChange={getDictionaryForFiltersAction}
              />
            }
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                final: this.renderItemBoolElement,
                stmtDate: this.renderItemAlignRightElement,
                operDate: this.renderItemAlignRightElement,
                valDate: this.renderItemAlignRightElement,
                operType: this.renderItemAlignRightElement,
                debit: this.renderItemAlignRightElement,
                credit: this.renderItemAlignRightElement
              }}
              renderHeadCells={{
                final: this.renderItemAlignCenterElement,
                stmtDate: this.renderItemAlignRightElement,
                operDate: this.renderItemAlignRightElement,
                valDate: this.renderItemAlignRightElement,
                operType: this.renderItemAlignRightElement,
                debit: this.renderItemAlignRightElement,
                credit: this.renderItemAlignRightElement
              }}
              settings={{
                iconTitle: 'Настройка таблицы операций',
                changeColumnsAction,
                showGrouping: true,
                changeGroupingAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              sorting={{
                changeSortingAction
              }}
              select={{
                selectItemTitle: 'Выделить операцию',
                selectGroupTitle: 'Выделить группу операций',
                selectAllTitle: 'Выделить все операции',
                changeSelectAction
              }}
              resize={{
                resizeColumnsAction
              }}
              operations={operations}
              operationAction={operationAction}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {!isListLoading && !!selectedItems.size &&
            <Selection
              dataLoc={LOCATORS.SELECTION}
              main={{
                label: 'Выбрано:',
                value: selectedItems.size
              }}
              extra={{
                label: 'Итого',
                value: selectedItemsSumAmount
              }}
            />
            }
            {!isListLoading && !!list.size && !selectedItems.size &&
            <Export
              dataLoc={LOCATORS.EXPORT}
              label="Выгрузить список в Excel"
              onClick={exportListAction}
              isLoading={isExportListLoading}
            />
            }
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
        }
      </div>
    );
  }
}

export default StatementContainer;
