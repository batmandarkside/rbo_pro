import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const initialSections = [
  {
    id: 'statement',
    title: 'Выписка',
    route: '/statements/:id',
    selected: true
  }
];

export const initialFiltersConditions = [
  {
    id: 'operNumber',
    title: 'Номер операции',
    type: 'string',
    maxLength: 64
  },
  {
    id: 'corrName',
    title: 'Контрагент',
    type: 'string',
    maxLength: 160
  },
  {
    id: 'inn',
    title: 'ИНН контрагента',
    type: 'string',
    maxLength: 12
  },
  {
    id: 'corrAccount',
    title: 'Счет контрагента',
    type: 'string',
    inputType: 'Account'
  },
  {
    id: 'operType',
    title: 'Дебет/Кредит',
    type: 'select',
    values: [
      'Дебет',
      'Кредит'
    ]
  },
  {
    id: 'purpose',
    title: 'Назначение платежа',
    type: 'string',
    maxLength: 210
  },
  {
    id: 'sum',
    title: 'Сумма операции',
    type: 'amountsRange'
  }
];

export const initialFilters = [
  {
    id: 'statementContainerPresetCredit',
    title: 'Поступления',
    isPreset: true,
    savedConditions: [
      {
        id: 'operType',
        params: 'Кредит'
      }
    ]
  },
  {
    id: 'statementContainerPresetDebit',
    title: 'Списания',
    isPreset: true,
    savedConditions: [
      {
        id: 'operType',
        params: 'Дебет'
      }
    ]
  }
];

export const initialColumns = [
  {
    id: 'operDate',
    title: 'Дата операции',
    canGrouped: true
  },
  {
    id: 'valDate',
    title: 'Дата валютирования',
    canGrouped: false
  },
  {
    id: 'operType',
    title: 'Вид операции',
    canGrouped: true
  },
  {
    id: 'operNumber',
    title: 'Номер операции',
    canGrouped: false
  },
  {
    id: 'debit',
    title: 'Дебет',
    canGrouped: false
  },
  {
    id: 'credit',
    title: 'Кредит',
    canGrouped: false
  },
  {
    id: 'corrName',
    title: 'Контрагент',
    canGrouped: true
  },
  {
    id: 'inn',
    title: 'ИНН контрагента',
    canGrouped: false
  },
  {
    id: 'corrAccount',
    title: 'Счет контрагента',
    canGrouped: true
  },
  {
    id: 'corrBankName',
    title: 'Наименование банка контрагента',
    canGrouped: false
  },
  {
    id: 'debit-credit',
    title: 'Дебет/Кредит',
    canGrouped: true
  },
  {
    id: 'paymentPurpose',
    title: 'Назначение платежа',
    canGrouped: false
  }
];

export const initialOperations = [
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false,
    grouped: true
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialSettings = {
  visible: [
    'operDate',
    'debit',
    'credit',
    'corrName',
    'paymentPurpose'
  ],
  sorting: { id: 'operDate', direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    operDate: 136,
    valDate: 136,
    operType: 136,
    operNumber: 146,
    debit: 280,
    credit: 280,
    corrName: 280,
    inn: 152,
    corrAccount: 196,
    corrBankName: 280,
    'debit-credit': 132,
    paymentPurpose: 320
  }
};

export const initialState = fromJS({
  isStatementLoading: true,
  isStatementPrinting: false,
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: initialFiltersConditions,
  filters: initialFilters,
  list: [],
  operations: initialOperations,
  selectedItems: [],
  statement: {}
});

export const getStatementSuccess = (state, statement) => state.merge({
  statement: fromJS(statement),
  isStatementLoading: false
});

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters;
  if (filter) {
    if (state.get('filters').find(item => item.get('id') === filter.id)) {
      filters = state.get('filters').map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = state.get('filters').push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  } else {
    filters = state.get('filters').map(item => item.set('selected', false));
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false,
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], true)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  selectedItems: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list);
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([]),
  statement: fromJS({}),
  isStatementLoading: true
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

export const operationFinish = (state, operationId, isListUpdateRequired) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

export const printStatementRequest = state => state.merge({
  isStatementPrinting: true
});

export const printStatementFinish = state => state.merge({
  isStatementPrinting: false
});

function StatementContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.STATEMENT_CONTAINER_GET_STATEMENT_SUCCESS:
      return getStatementSuccess(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.STATEMENT_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.STATEMENT_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.STATEMENT_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.STATEMENT_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.STATEMENT_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.STATEMENT_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, action.payload);

    case ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationRequest(state, 'print');

    case ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_FAIL:
      return operationFinish(state, 'print', false);

    case ACTIONS.STATEMENT_CONTAINER_PRINT_STATEMENT_REQUEST:
      return printStatementRequest(state);

    case ACTIONS.STATEMENT_CONTAINER_PRINT_STATEMENT_SUCCESS:
    case ACTIONS.STATEMENT_CONTAINER_PRINT_STATEMENT_FAIL:
      return printStatementFinish(state);

    default:
      return state;
  }
}

export default StatementContainerReducer;
