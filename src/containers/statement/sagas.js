import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';
import { normalizeAllMaxLength } from 'utils';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction
} from 'dal/notification/actions';
import {
  getStatements as dalGetStatement,
  printStatements as dalPrintStatement
} from 'dal/statements/sagas';
import {
  getAccounts as dalGetAccounts,
  getMovements as dalGetList,
  exportMovementsListToExcel as dalExportListToExcel,
  printMovements as dalOperationPrint
} from 'dal/accounts/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { getCurrencyCodes as dalGetCurrCodes } from 'dal/dictionaries/sagas';
import {
  ACTIONS,
  LOCAL_REDUCER,
  SETTINGS_ALIAS,
  VALIDATE_SETTINGS
} from './constants';

const getParamsForDalOperation = ({ settings, pages, filters, statement, type }) => {
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const visibleColumns = settings.get('visible').toJS();
  const selectedFilter = filters.find(filter => filter.get('selected'));

  const account = (statement && statement.get('account')) || null;
  const stmtDateFrom = (statement && statement.get('stateDate')) || null;
  const stmtDateTo = (statement && statement.get('stateDate')) || null;
  const isFinal = (statement && statement.get('stateType'));

  const conditions = selectedFilter && selectedFilter.get('conditions');
  const conditionOperNumber = conditions && conditions.find(c => c.get('id') === 'operNumber');
  const operNumber = (conditionOperNumber && conditionOperNumber.get('params')) || null;
  const conditionCurrCodeIso = conditions && conditions.find(c => c.get('id') === 'currCodeIso');
  const currCodeIso = (conditionCurrCodeIso && conditionCurrCodeIso.get('isoCode')) || null;
  const conditionCorrName = conditions && conditions.find(c => c.get('id') === 'corrName');
  const corrName = (conditionCorrName && conditionCorrName.get('params')) || null;
  const conditionInn = conditions && conditions.find(c => c.get('id') === 'inn');
  const inn = (conditionInn && conditionInn.get('params')) || null;
  const conditionCorrAccount = conditions && conditions.find(c => c.get('id') === 'corrAccount');
  const corrAccount = (conditionCorrAccount && conditionCorrAccount.get('params')) || null;
  const conditionOperType = conditions && conditions.find(c => c.get('id') === 'operType');
  const operTypeValue = (conditionOperType && conditionOperType.get('params')) || null;
  const operType = () => {
    switch (operTypeValue) {
      case 'Дебет': return 0;
      case 'Кредит': return 1;
      default: return null;
    }
  };
  const conditionPurpose = conditions && conditions.find(c => c.get('id') === 'purpose');
  const purpose = (conditionPurpose && conditionPurpose.get('params')) || null;
  const conditionSum = conditions && conditions.find(c => c.get('id') === 'sum');
  const sumMin = (conditionSum && conditionSum.getIn(['params', 'amountFrom'])) || null;
  const sumMax = (conditionSum && conditionSum.getIn(['params', 'amountTo'])) || null;

  const params = {
    sort,
    desc,

    account,
    isFinal,
    stmtDateFrom,
    stmtDateTo,

    operNumber,
    currCodeIso,
    corrName,
    inn,
    corrAccount,
    operType: operType(),
    purpose,
    sumMin,
    sumMax
  };

  switch (type) {
    case 'list':
      params.offset = pages.get('selected') * settings.get('pagination');
      params.offsetStep = settings.get('pagination') + 1;
      break;
    case 'export':
      params.visibleColumns = visibleColumns;
      break;
    default:
      break;
  }

  return params;
};

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);
export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
export const localStatementSelector = state => state.getIn([LOCAL_REDUCER, 'statement']);

export function* getStatement(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const params = {
    account: query.account || null,
    from: query.date || null,
    to: query.date || null,
    stmtType: parseInt(query.final, 10),
  };
  try {
    const result = yield call(dalGetStatement, { payload: params });
    const statement = result.find(item => item.recordID === payload);
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_GET_STATEMENT_SUCCESS, payload: statement });
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_GET_SETTINGS_REQUEST });
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_GET_QUERY_PARAMS_REQUEST });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_GET_STATEMENT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.STATEMENT_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.STATEMENT_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload: { pageIndex, filter } });
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;

  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* getDictionaryForFilters(action) {
  try {
    const { payload } = action;
    let result;
    switch (payload.conditionId) {
      case 'account':
        result = yield call(dalGetAccounts, { payload: { accNum: payload.value } });
        break;
      case 'currCodeIso':
        result = yield call(dalGetCurrCodes, { payload: { every: normalizeAllMaxLength(payload.value, 3) } });
        break;
      default:
        result = null;
        break;
    }
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
      payload: { conditionId: payload.conditionId, items: result } });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getList() {
  try {
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const statement = yield select(localStatementSelector);
    const params = getParamsForDalOperation({ settings, pages, filters, statement, type: 'list' });
    const result = yield call(dalGetList, { payload: params });
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_GET_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* exportList() {
  try {
    const settings = yield select(localSettingsSelector);
    const filters = yield select(localFiltersSelector);
    const statement = yield select(localStatementSelector);
    const params = getParamsForDalOperation({ settings, pages: null, filters, statement, type: 'export' });
    const result = yield call(dalExportListToExcel, { payload: params });
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_EXPORT_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_EXPORT_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* operation(action) {
  const { payload: { operationId, items } } = action;
  switch (operationId) {
    case 'print':
      yield put({ type: ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_REQUEST, payload: items });
      break;
    default:
      break;
  }
}

export function* operationPrint(action) {
  const { payload } = action;
  try {
    const result = yield call(dalOperationPrint, { payload: { id: payload } });
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* printStatement(action) {
  const { payload: { type } } = action;
  const statement = yield select(localStatementSelector);
  const id = statement.get('recordID');
  try {
    let result;
    switch (type) {
      case 'withDetails':
        result = yield call(dalPrintStatement, { payload: { id, detail: true } });
        break;
      default:
        result = yield call(dalPrintStatement, { payload: { id, detail: false } });
    }
    yield put({ type: ACTIONS.STATEMENT_CONTAINER_PRINT_STATEMENT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENT_CONTAINER_PRINT_STATEMENT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}

export default function* saga() {
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_GET_STATEMENT_REQUEST, getStatement);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST, getDictionaryForFilters);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_EXPORT_LIST_REQUEST, exportList);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_EXPORT_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_CHANGE_GROUPING, changeSettingsColumns);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);

  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_OPERATION, operation);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_REQUEST, operationPrint);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_OPERATION_PRINT_FAIL, errorNotification);

  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_PRINT_STATEMENT_REQUEST, printStatement);
  yield takeLatest(ACTIONS.STATEMENT_CONTAINER_PRINT_STATEMENT_FAIL, errorNotification);
}
