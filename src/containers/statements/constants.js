import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  STATEMENTS_CONTAINER_GET_SECTION_INDEX_REQUEST: null,
  STATEMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS: null,
  STATEMENTS_CONTAINER_GET_SETTINGS_REQUEST: null,
  STATEMENTS_CONTAINER_GET_SETTINGS_SUCCESS: null,
  STATEMENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST: null,
  STATEMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS: null,
  STATEMENTS_CONTAINER_CHANGE_SECTION: null,
  STATEMENTS_CONTAINER_CHANGE_PAGE: null,
  STATEMENTS_CONTAINER_RELOAD_PAGES: null,
  STATEMENTS_CONTAINER_CHANGE_FILTERS: null,
  STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST: null,
  STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS: null,
  STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL: null,
  STATEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST: null,
  STATEMENTS_CONTAINER_SAVE_SETTINGS_SUCCESS: null,
  STATEMENTS_CONTAINER_SAVE_SETTINGS_FAIL: null,
  STATEMENTS_CONTAINER_GET_LIST_REQUEST: null,
  STATEMENTS_CONTAINER_GET_LIST_SUCCESS: null,
  STATEMENTS_CONTAINER_GET_LIST_FAIL: null,
  STATEMENTS_CONTAINER_EXPORT_LIST_REQUEST: null,
  STATEMENTS_CONTAINER_EXPORT_LIST_SUCCESS: null,
  STATEMENTS_CONTAINER_EXPORT_LIST_FAIL: null,
  STATEMENTS_CONTAINER_UNMOUNT_REQUEST: null,
  STATEMENTS_CONTAINER_RESIZE_COLUMNS: null,
  STATEMENTS_CONTAINER_CHANGE_COLUMN: null,
  STATEMENTS_CONTAINER_CHANGE_GROUPING: null,
  STATEMENTS_CONTAINER_CHANGE_SORTING: null,
  STATEMENTS_CONTAINER_CHANGE_PAGINATION: null,
  STATEMENTS_CONTAINER_CHANGE_SELECT: null,
  STATEMENTS_CONTAINER_ROUTE_TO_ITEM: null,

  STATEMENTS_CONTAINER_OPERATION: null,
  STATEMENTS_CONTAINER_OPERATION_PRINT_REQUEST: null,
  STATEMENTS_CONTAINER_OPERATION_PRINT_SUCCESS: null,
  STATEMENTS_CONTAINER_OPERATION_PRINT_FAIL: null,
  STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_REQUEST: null,
  STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_SUCCESS: null,
  STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_FAIL: null,
  STATEMENTS_CONTAINER_OPERATION_EXPORT_REQUEST: null,
  STATEMENTS_CONTAINER_OPERATION_EXPORT_SUCCESS: null,
  STATEMENTS_CONTAINER_OPERATION_EXPORT_FAIL: null
});

export const LOCAL_REDUCER = 'StatementsContainerReducer';

export const LOCAL_ROUTER_ALIAS = '/statements';

export const SETTINGS_ALIAS = 'StatementsContainer';

export const VALIDATE_SETTINGS = settings => !!settings &&
  !!settings.visible &&
  !!settings.sorting &&
  !!settings.pagination &&
  !!settings.width &&
  !!settings.width.account &&
  !!settings.width.accountName &&
  !!settings.width.accountCurrency &&
  !!settings.width.branchName &&
  !!settings.width.stateDate &&
  !!settings.width.stateType &&
  !!settings.width.docNumber &&
  !!settings.width.receiptDate &&
  !!settings.width.prevStateDate &&
  !!settings.width.cbRate &&
  !!settings.width.opBalance &&
  !!settings.width.opBalanceNatCurrency &&
  !!settings.width.closingBalance &&
  !!settings.width.closingBalanceNatCurrency &&
  !!settings.width.turnoverD &&
  !!settings.width.turnoverDNatCurrency &&
  !!settings.width.turnoverC &&
  !!settings.width.turnoverCNatCurrency &&
  !!settings.width.documentDCount &&
  !!settings.width.documentCCount &&
  !!settings.width.availBalance &&
  !!settings.width.availBalanceNatCurrency &&
  !!settings.width.org;

export const LOCATORS = {
  CONTAINER: 'statementsContainer',
  SPREADSHEET: 'statementsContainerSpreadsheet',

  TOOLBAR: 'statementsContainerSpreadsheetToolbar',
  TOOLBAR_BUTTONS: 'statementsContainerSpreadsheetToolbarButtons',
  TOOLBAR_BUTTON_REFRESH_LIST: 'statementsContainerSpreadsheetToolbarButtonRefreshList',
  TOOLBAR_TABS: 'statementsContainerSpreadsheetToolbarTabs',

  FILTERS: 'statementsContainerSpreadsheetFilters',
  FILTERS_SELECT: 'statementsContainerSpreadsheetFiltersSelect',
  FILTERS_ADD: 'statementsContainerSpreadsheetFiltersAdd',
  FILTERS_REMOVE: 'statementsContainerSpreadsheetFiltersRemove',
  FILTERS_OPERATIONS: 'statementsContainerSpreadsheetFiltersOperations',
  FILTERS_OPERATION_SAVE: 'statementsContainerSpreadsheetFiltersOperationSave',
  FILTERS_OPERATION_SAVE_AS: 'statementsContainerSpreadsheetFiltersOperationSaveAs',
  FILTERS_OPERATION_SAVE_AS_POPUP: 'statementsContainerSpreadsheetFiltersOperationSaveAsPopup',
  FILTERS_OPERATION_CLEAR: 'statementsContainerSpreadsheetFiltersOperationClear',
  FILTERS_CONDITIONS: 'statementsContainerSpreadsheetFiltersConditions',
  FILTERS_CONDITION: 'statementsContainerSpreadsheetFiltersCondition',
  FILTERS_CONDITION_POPUP: 'statementsContainerSpreadsheetFiltersConditionPopup',
  FILTERS_CONDITION_REMOVE: 'statementsContainerSpreadsheetFiltersConditionRemove',
  FILTERS_ADD_CONDITION: 'statementsContainerSpreadsheetFiltersAddCondition',
  FILTERS_ADD_CONDITION_POPUP: 'statementsContainerSpreadsheetFiltersAddConditionPopup',
  FILTERS_CLOSE: 'statementsContainerSpreadsheetFiltersClose',

  OPERATIONS: 'statementsContainerSpreadsheetOperations',

  TABLE: 'statementsContainerSpreadsheetTable',
  TABLE_HEAD: 'statementsContainerSpreadsheetTableHead',
  TABLE_BODY: 'statementsContainerSpreadsheetTableBody',
  TABLE_SETTINGS: 'statementsContainerSpreadsheetTableSettings',
  TABLE_SETTINGS_TOGGLE: 'statementsContainerSpreadsheetTableSettingsToggle',
  TABLE_SETTINGS_TOGGLE_BUTTON: 'statementsContainerSpreadsheetTableSettingsToggleButton',
  TABLE_SETTINGS_POPUP: 'statementsContainerSpreadsheetTableSettingsPopup',

  SELECTION: 'statementsContainerSpreadsheetSelection',
  EXPORT: 'statementsContainerSpreadsheetExport',
  PAGINATION: 'statementsContainerSpreadsheetPagination'
};
