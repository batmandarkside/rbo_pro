import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import mapStateToProps from './selectors';
import * as Actions from './actions';
import saga from './sagas';
import reducer from './reducer';
import StatementsContainer from './container';

const withConnect = connect(mapStateToProps, Actions);
const withReducer = injectReducer({ key: 'StatementsContainerReducer', reducer });
const withSaga = injectSaga({ key: 'statementsContainerSagas', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(StatementsContainer);
