import { fromJS } from 'immutable';
import moment from 'moment-timezone';
import { ACTIONS } from './constants';

export const initialSections = [
  {
    id: 'statements',
    title: 'Выписки',
    route: '/statements',
    api: '',
    selected: true
  },
  {
    id: 'movements',
    title: 'Операции',
    route: '/statements/movements',
  }
];

export const initialFiltersConditions = [
  {
    id: 'account',
    title: 'Собственный счет',
    type: 'search',
    inputType: 'Account',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['statements']
  },
  {
    id: 'currCodeIso',
    title: 'Валюта счета',
    type: 'search',
    inputType: 'Text',
    values: {
      items: [],
      searchValue: '',
      isSearching: false
    },
    sections: ['statements']
  },
  {
    id: 'stateDate',
    title: 'Дата выписки',
    type: 'datesRange',
    sections: ['statements']
  },
  {
    id: 'stmtType',
    title: 'Итоговая',
    type: 'select',
    values: [
      'Итоговая',
      'Внутридневная'
    ],
    sections: ['statements']
  },
  {
    id: 'corrAccount',
    title: 'Счет контрагента',
    type: 'string',
    inputType: 'Account',
    sections: ['statements']
  },
  {
    id: 'corrName',
    title: 'Наименование контрагента',
    type: 'string',
    maxLength: 100,
    sections: ['statements']
  },
  {
    id: 'zero',
    title: 'Нулевые обороты',
    type: 'select',
    values: [
      'Не показывать',
      'Показывать'
    ],
    sections: ['statements']
  }
];

export const initialFilters = [
  {
    id: 'statementsContainerPresetLastMonth',
    title: 'За прошлый месяц',
    isPreset: true,
    savedConditions: [
      {
        id: 'stateDate',
        params: {
          dateFrom: moment({ hour: 0, minute: 0, seconds: 0 }).subtract(1, 'months').startOf('month').format(),
          dateTo: moment({ hour: 0, minute: 0, seconds: 0 }).subtract(1, 'months').endOf('month').format()
        }
      }
    ],
    sections: ['statements']
  },
  {
    id: 'statementsContainerPresetCurrentMonth',
    title: 'За текущий месяц',
    isPreset: true,
    savedConditions: [
      {
        id: 'stateDate',
        params: {
          dateFrom: moment({ hour: 0, minute: 0, seconds: 0 }).startOf('month').format(),
          dateTo: moment({ hour: 0, minute: 0, seconds: 0 }).format()
        }
      }
    ],
    sections: ['statements']
  },
  {
    id: 'statementsContainerPresetToday',
    title: 'За сегодня',
    isPreset: true,
    savedConditions: [
      {
        id: 'stateDate',
        params: {
          dateFrom: moment({ hour: 0, minute: 0, seconds: 0 }).format(),
          dateTo: moment({ hour: 0, minute: 0, seconds: 0 }).format()
        }
      }
    ],
    sections: ['statements']
  }
];

export const initialColumns = [
  {
    id: 'account',
    title: 'Собственный счет',
    canGrouped: true
  },
  {
    id: 'accountName',
    title: 'Наименование счета',
    canGrouped: false
  },
  {
    id: 'accountCurrency',
    title: 'Валюта',
    canGrouped: false
  },
  {
    id: 'branchName',
    title: 'Подразделение',
    canGrouped: false
  },
  {
    id: 'stateDate',
    title: 'Дата выписки',
    canGrouped: true
  },
  {
    id: 'stateType',
    title: 'Итоговая',
    canGrouped: true
  },
  {
    id: 'docNumber',
    title: 'Номер выписки',
    canGrouped: false
  },
  {
    id: 'receiptDate',
    title: 'Время получения',
    canGrouped: false
  },
  {
    id: 'prevStateDate',
    title: 'Дата пред. выписки',
    canGrouped: false,
    canNotSorting: true
  },
  {
    id: 'cbRate',
    title: 'Курс ЦБ',
    canGrouped: false
  },
  {
    id: 'opBalance',
    title: 'Вх. остаток',
    canGrouped: false
  },
  {
    id: 'opBalanceNatCurrency',
    title: 'Вх. остаток в нац. валюте',
    canGrouped: false
  },
  {
    id: 'closingBalance',
    title: 'Исх. остаток',
    canGrouped: false
  },
  {
    id: 'closingBalanceNatCurrency',
    title: 'Исх. остаток в нац. валюте',
    canGrouped: false
  },
  {
    id: 'turnoverD',
    title: 'Сумма об. по дебету',
    canGrouped: false,
    canNotSorting: true
  },
  {
    id: 'turnoverDNatCurrency',
    title: 'Сумма об. по дебету в нац. валюте',
    canGrouped: false,
    canNotSorting: true
  },
  {
    id: 'turnoverC',
    title: 'Сумма об. по кредиту',
    canGrouped: false,
    canNotSorting: true
  },
  {
    id: 'turnoverCNatCurrency',
    title: 'Сумма об. по кредиту в нац. валюте',
    canGrouped: false,
    canNotSorting: true
  },
  {
    id: 'documentDCount',
    title: 'Кол-во об. по дебету',
    canGrouped: false,
    canNotSorting: true
  },
  {
    id: 'documentCCount',
    title: 'Кол-во об. по кредиту',
    canGrouped: false,
    canNotSorting: true
  },
  {
    id: 'availBalance',
    title: 'Доступный остаток',
    canGrouped: false
  },
  {
    id: 'availBalanceNatCurrency',
    title: 'Доступный остаток в нац. валюте',
    canGrouped: false
  },
  {
    id: 'org',
    title: 'Организация',
    canGrouped: true
  }
];

export const initialOperations = [
  {
    id: 'print',
    title: 'Распечатать',
    icon: 'print',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'printWithDetails',
    title: 'Выписка с приложениями',
    icon: 'documents',
    disabled: false,
    progress: false,
    grouped: true
  },
  {
    id: 'export',
    title: 'Экспортировать в 1С',
    icon: 'download',
    disabled: false,
    progress: false,
    grouped: true
  }
];

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialSettings = {
  visible: [
    'account',
    'accountCurrency',
    'stateDate',
    'stateType',
    'opBalance',
    'closingBalance',
    'turnoverD',
    'turnoverC',
    'documentDCount',
    'documentCCount',
    'availBalance',
    'org'
  ],
  sorting: { id: 'receiptDate', direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    account: 196,
    accountName: 240,
    accountCurrency: 96,
    branchName: 280,
    stateDate: 136,
    stateType: 104,
    docNumber: 148,
    receiptDate: 160,
    prevStateDate: 172,
    cbRate: 110,
    opBalance: 280,
    opBalanceNatCurrency: 280,
    closingBalance: 280,
    closingBalanceNatCurrency: 280,
    turnoverD: 280,
    turnoverDNatCurrency: 280,
    turnoverC: 280,
    turnoverCNatCurrency: 280,
    documentDCount: 184,
    documentCCount: 184,
    availBalance: 280,
    availBalanceNatCurrency: 280,
    org: 280
  }
};

export const initialState = fromJS({
  isExportListLoading: false,
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: [],
  filters: [],
  list: [],
  operations: initialOperations,
  selectedItems: []
});

export const getSectionIndexSuccess = (state, sectionIndex) => {
  const selectedSectionId = state.get('sections').find((section, key) => key === sectionIndex).get('id');
  return state.merge({
    sections: state.get('sections').map((section, key) => section.set('selected', key === sectionIndex)),
    filtersConditions: fromJS(initialFiltersConditions)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections')),
    filters: fromJS(initialFilters)
      .filter(item => item.get('sections').find(section => section === selectedSectionId))
      .map(item => item.remove('sections'))
  });
};

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters;
  if (filter) {
    if (state.get('filters').find(item => item.get('id') === filter.id)) {
      filters = state.get('filters').map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = state.get('filters').push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  } else {
    filters = state.get('filters').map(item => item.set('selected', false));
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const changeSection = (state, sectionIndex) => (
  state.getIn(['sections', sectionIndex, 'selected']) ?
    state :
    state.merge({
      columns: fromJS(initialColumns),
      settings: fromJS(initialSettings),
      pages: fromJS(initialPages),
      paginationOptions: fromJS(initialPaginationOptions)
    })
);

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters, isChanged }) => state.merge({
  filters,
  filtersConditions: isChanged
    ? state.get('filtersConditions')
      .map(condition => (condition.get('type') === 'search' || condition.get('type') === 'suggest'
        ? condition.setIn(['values', 'searchValue'], '')
        : condition))
    : state.get('filtersConditions')
});

export const getDictionaryForFilterRequest = (state, { conditionId, value }) => {
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], true)
    .setIn(['values', 'searchValue'], value);
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterSuccess = (state, { conditionId, items }) => {
  if (!items) return state;
  const newCondition = state.get('filtersConditions')
    .find(condition => condition.get('id') === conditionId)
    .setIn(['values', 'isSearching'], false)
    .setIn(['values', 'items'], fromJS(items));
  return state.merge({
    filtersConditions: state.get('filtersConditions').map(
      condition => (condition.get('id') === conditionId ? newCondition : condition)
    )
  });
};

export const getDictionaryForFilterFail = state => state.merge({
  filtersConditions: state.get('filtersConditions')
    .map(condition => (
      condition.get('type') === 'dictionary' ? condition.setIn(['values', 'isSearching'], false) : condition
    ))
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  selectedItems: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list);
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const exportListRequest = state => state.merge({
  isExportListLoading: true
});

export const exportListFinish = state => state.merge({
  isExportListLoading: false
});

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  selectedItems: fromJS([])
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeGrouping = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

export const changeSelect = (state, selectedItems) => state.merge({
  selectedItems
});

export const operationRequest = (state, operationId) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', true) : operation
  ))
});

export const operationFinish = (state, operationId, isListUpdateRequired) => state.merge({
  operations: state.get('operations').map(operation => (
    operation.get('id') === operationId ? operation.set('progress', false) : operation
  )),
  isListUpdateRequired
});

function StatementsContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.STATEMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS:
      return getSectionIndexSuccess(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_CHANGE_SECTION:
      return changeSection(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.STATEMENTS_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST:
      return getDictionaryForFilterRequest(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS:
      return getDictionaryForFilterSuccess(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL:
      return getDictionaryForFilterFail(state);

    case ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.STATEMENTS_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_EXPORT_LIST_REQUEST:
      return exportListRequest(state);

    case ACTIONS.STATEMENTS_CONTAINER_EXPORT_LIST_SUCCESS:
    case ACTIONS.STATEMENTS_CONTAINER_EXPORT_LIST_FAIL:
      return exportListFinish(state);

    case ACTIONS.STATEMENTS_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.STATEMENTS_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_CHANGE_GROUPING:
      return changeGrouping(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_CHANGE_SELECT:
      return changeSelect(state, action.payload);

    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_REQUEST:
      return operationRequest(state, 'print');

    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_SUCCESS:
    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_FAIL:
      return operationFinish(state, 'print', false);

    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_REQUEST:
      return operationRequest(state, 'export');

    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_SUCCESS:
    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_FAIL:
      return operationFinish(state, 'export', false);

    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_REQUEST:
      return operationRequest(state, 'printWithDetails');

    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_SUCCESS:
    case ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_FAIL:
      return operationFinish(state, 'printWithDetails', false);

    default:
      return state;
  }
}

export default StatementsContainerReducer;
