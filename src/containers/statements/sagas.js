import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';
import { normalizeAllMaxLength } from 'utils';
import {
  addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction
} from 'dal/notification/actions';
import {
  getStatements as dalGetList,
  exportStatementsListToExcel as dalExportListToExcel,
  printStatements as dalOperationPrint,
  exportStatementsTo1C as dalOperationExport
} from 'dal/statements/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { getAccounts as dalGetAccounts } from 'dal/accounts/sagas';
import { getCurrencyCodes as dalGetCurrCodes } from 'dal/dictionaries/sagas';
import {
  ACTIONS,
  LOCAL_ROUTER_ALIAS,
  LOCAL_REDUCER,
  SETTINGS_ALIAS,
  VALIDATE_SETTINGS
} from './constants';

const getParamsForDalOperation = ({ settings, pages, filters, type }) => {
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const visibleColumns = settings.get('visible').toJS();
  const selectedFilter = filters.find(filter => filter.get('selected'));
  const conditions = selectedFilter && selectedFilter.get('conditions');
  const conditionAccount = conditions && conditions.find(c => c.get('id') === 'account');
  const account = (conditionAccount && conditionAccount.get('params')) || null;
  const conditionStateDate = conditions && conditions.find(c => c.get('id') === 'stateDate');
  const from = (conditionStateDate && conditionStateDate.getIn(['params', 'dateFrom'])) || null;
  const to = (conditionStateDate && conditionStateDate.getIn(['params', 'dateTo'])) || null;
  const conditionStmtType = conditions && conditions.find(c => c.get('id') === 'stmtType');
  const stmtTypeValue = (conditionStmtType && conditionStmtType.get('params')) || null;
  const stmtType = () => {
    switch (stmtTypeValue) {
      case 'Внутридневная': return 1;
      case 'Итоговая': return 0;
      default: return null;
    }
  };
  const conditionZero = conditions && conditions.find(c => c.get('id') === 'zero');
  const zeroValue = (conditionZero && conditionZero.get('params')) || null;
  const zero = () => {
    switch (zeroValue) {
      case 'Не показывать': return 1;
      case 'Показывать': return 0;
      default: return null;
    }
  };
  const conditionCorrName = conditions && conditions.find(c => c.get('id') === 'corrName');
  const corrName = (conditionCorrName && conditionCorrName.get('params')) || null;
  const conditionCorrAccount = conditions && conditions.find(c => c.get('id') === 'corrAccount');
  const corrAccount = (conditionCorrAccount && conditionCorrAccount.get('params')) || null;
  const conditionCurrCodeIso = conditions && conditions.find(c => c.get('id') === 'currCodeIso');
  const currCodeIso = (conditionCurrCodeIso && conditionCurrCodeIso.get('params')) || null;

  const params = {
    sort,
    desc,
    account,
    from,
    to,
    stmtType: stmtType(),
    zero: zero(),
    corrName,
    corrAccount: corrAccount ? corrAccount.replace(/\D/g, '') : null,
    currCodeIso
  };

  switch (type) {
    case 'list':
      params.offset = pages.get('selected') * settings.get('pagination');
      params.offsetStep = settings.get('pagination') + 1;
      break;
    case 'export':
      params.visibleColumns = visibleColumns;
      break;
    default:
      break;
  }

  return params;
};

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);
export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
export const localListSelector = state => state.getIn([LOCAL_REDUCER, 'list']);

export function* getSectionIndex() {
  const location = yield select(routingLocationSelector);
  const sections = yield select(localSectionsSelector);
  const pathname = location.get('pathname');
  let sectionIndex = 0;
  sections.forEach((section, key) => {
    if (pathname.match(new RegExp(section.get('route'), 'gi'))) {
      sectionIndex = key;
    }
  });
  yield put({ type: ACTIONS.STATEMENTS_CONTAINER_GET_SECTION_INDEX_SUCCESS, payload: sectionIndex });
  yield put({ type: ACTIONS.STATEMENTS_CONTAINER_GET_SETTINGS_REQUEST });
}

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.STATEMENTS_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.STATEMENTS_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload: { pageIndex, filter } });
}

export function* changeSection(action) {
  const { payload } = action;
  const sections = yield select(localSectionsSelector);
  const activeSection = sections.find(section => section.get('id') === payload);
  yield put(push({ pathname: activeSection.get('route'), search: null }));
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;

  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* getDictionaryForFilters(action) {
  try {
    const { payload } = action;
    let result;
    switch (payload.conditionId) {
      case 'account':
        result = yield call(dalGetAccounts, { payload: { accNum: payload.value } });
        break;
      case 'currCodeIso':
        result = yield call(dalGetCurrCodes, { payload: { every: normalizeAllMaxLength(payload.value, 3) } });
        break;
      default:
        result = null;
        break;
    }
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_SUCCESS,
      payload: { conditionId: payload.conditionId, items: result } });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getList() {
  try {
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ settings, pages, filters, type: 'list' });
    const result = yield call(dalGetList, { payload: params });
    yield put({ type: ACTIONS.STATEMENTS_CONTAINER_GET_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* exportList() {
  try {
    const settings = yield select(localSettingsSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ settings, pages: null, filters, type: 'export' });
    const result = yield call(dalExportListToExcel, { payload: params });
    yield put({ type: ACTIONS.STATEMENTS_CONTAINER_EXPORT_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_EXPORT_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* routeToItem(action) {
  const { payload } = action;

  const list = yield select(localListSelector);
  const selectedItem = list.find(item => item.get('recordID') === payload);
  const final = () => {
    switch (selectedItem.get('stateType')) {
      case false: return 1;
      case true: return 0;
      default: return null;
    }
  };
  const params = {
    account: selectedItem.get('account'),
    date: selectedItem.get('stateDate'),
    final: final()
  };
  yield put(push({
    pathname: `${LOCAL_ROUTER_ALIAS}/${payload}`,
    search: `?${qs.stringify(params)}`
  }));
}

export function* operation(action) {
  const { payload: { operationId, items } } = action;
  switch (operationId) {
    case 'print':
      yield put({ type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_REQUEST, payload: items });
      break;
    case 'export':
      yield put({ type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_REQUEST, payload: items });
      break;
    case 'printWithDetails':
      yield put({ type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_REQUEST, payload: items });
      break;
    default:
      break;
  }
}

export function* operationPrint(action) {
  const { payload } = action;
  try {
    const result = yield call(dalOperationPrint, { payload: { id: payload } });
    yield put({ type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationExport(action) {
  const { payload } = action;
  try {
    const result = yield call(dalOperationExport, { payload: { id: payload } });
    yield put({ type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* operationPrintWithDetails(action) {
  const { payload } = action;
  try {
    const result = yield call(dalOperationPrint, { payload: { id: payload, detail: true } });
    yield put({ type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}

export default function* saga() {
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_GET_SECTION_INDEX_REQUEST, getSectionIndex);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_CHANGE_SECTION, changeSection);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_GET_DICTIONARY_FOR_FILTERS_REQUEST, getDictionaryForFilters);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_EXPORT_LIST_REQUEST, exportList);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_EXPORT_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_CHANGE_GROUPING, changeSettingsColumns);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_ROUTE_TO_ITEM, routeToItem);

  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_OPERATION, operation);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_REQUEST, operationPrint);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_REQUEST, operationExport);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_OPERATION_EXPORT_FAIL, errorNotification);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_REQUEST, operationPrintWithDetails);
  yield takeLatest(ACTIONS.STATEMENTS_CONTAINER_OPERATION_PRINT_WITH_DETAILS_FAIL, errorNotification);
}
