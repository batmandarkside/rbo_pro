import { createSelector } from 'reselect';
import { List, Map, OrderedMap } from 'immutable';
import numeral from 'numeral';
import moment from 'moment-timezone';
import { getFormattedAccNum } from 'utils';
import { LOCAL_REDUCER } from './constants';

const isExportListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isExportListLoading']);
const isListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isListLoading']);
const isListUpdateRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isListUpdateRequired']);
const isReloadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isReloadRequired']);

const dalOrganizationsSelector = state => state.getIn(['organizations', 'orgs']);
const columnsSelector = state => state.getIn([LOCAL_REDUCER, 'columns']);
const filtersConditionsSelector = state => state.getIn([LOCAL_REDUCER, 'filtersConditions']);
const filtersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
const listSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
const sectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
const settingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
const pagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
const paginationOptionsSelector = state => state.getIn([LOCAL_REDUCER, 'paginationOptions']);
const operationsSelector = state => state.getIn([LOCAL_REDUCER, 'operations']);
const selectedItemsSelector = state => state.getIn([LOCAL_REDUCER, 'selectedItems']);

const mapListItemOperations = (operations, stateType) => (
  operations.reduce(
    (result, operation) => result.set(operation.get('id'), operation.reduce(() => {
      const operationId = operation.get('id');
      switch (operationId) {
        case 'printWithDetails':
          return stateType;
        default:
          return true;
      }
    })), OrderedMap()
  ).toJS()
);

const mapListItem = (item, selected, operations, dalOrganizations) => {
  const org = dalOrganizations.find(organization => organization.get('id') === item.get('orgId'));
  return Map({
    id: item.get('recordID'),
    account: getFormattedAccNum(item.get('account')),
    accountName: item.get('accountName'),
    accountCurrency: item.get('accountCurrency'),
    branchName: item.get('branchName'),
    stateDate: item.get('stateDate') ?
      moment(item.get('stateDate')).format('DD.MM.YYYY') :
      null,
    stateType: item.get('stateType'),
    docNumber: item.get('docNumber'),
    receiptDate: item.get('receiptDate') ?
      moment(item.get('receiptDate')).format('DD.MM.YYYY HH:mm:ss') :
      null,
    prevStateDate: item.get('prevStateDate') ?
      moment(item.get('prevStateDate')).format('DD.MM.YYYY') :
      null,
    cbRate: !isNaN(item.get('cbRate')) ?
      numeral(item.get('cbRate')).format('0,0.00') :
      null,
    opBalance: !isNaN(item.get('opBalance')) ?
      numeral(item.get('opBalance')).format('0,0.00') :
      null,
    opBalanceNatCurrency: !isNaN(item.get('opBalanceNatCurrency')) ?
      numeral(item.get('opBalanceNatCurrency')).format('0,0.00') :
      null,
    closingBalance: !isNaN(item.get('closingBalance')) ?
      numeral(item.get('closingBalance')).format('0,0.00') :
      null,
    closingBalanceNatCurrency: !isNaN(item.get('closingBalanceNatCurrency')) ?
      numeral(item.get('closingBalanceNatCurrency')).format('0,0.00') :
      null,
    turnoverD: !isNaN(item.get('turnoverD')) ?
      numeral(item.get('turnoverD')).format('0,0.00') :
      null,
    turnoverDNatCurrency: !isNaN(item.get('turnoverDNatCurrency')) ?
      numeral(item.get('turnoverDNatCurrency')).format('0,0.00') :
      null,
    turnoverC: !isNaN(item.get('turnoverC')) ?
      numeral(item.get('turnoverC')).format('0,0.00') :
      null,
    turnoverCNatCurrency: !isNaN(item.get('turnoverCNatCurrency')) ?
      numeral(item.get('turnoverCNatCurrency')).format('0,0.00') :
      null,
    documentDCount: item.get('documentDCount'),
    documentCCount: item.get('documentCCount'),
    availBalance: !isNaN(item.get('availBalance')) ?
      numeral(item.get('availBalance')).format('0,0.00') :
      null,
    availBalanceNatCurrency: !isNaN(item.get('availBalanceNatCurrency')) ?
      numeral(item.get('availBalanceNatCurrency')).format('0,0.00') :
      null,
    org: org ? org.get('shortName') : null,
    selected,
    operations: mapListItemOperations(operations, item.get('stateType'))
  });
};

const mapFilterConditionDictionaryItems = condition => condition.setIn(
  ['values', 'items'],
  condition.getIn(['values', 'items'], List()).map((item) => {
    switch (condition.get('id')) {
      case 'account':
        return Map({
          accNum: item.get('accNum'),
          availableBalance: !isNaN(item.get('availableBalance')) ?
            numeral(item.get('availableBalance')).format('0,0.00')
            :
            null,
          orgName: item.get('orgName'),
          account: getFormattedAccNum(item.get('accNum')),
          title: item.get('accNum'),
          value: item.get('id')
        });
      case 'currCodeIso':
        return Map({
          isoCode: item.get('isoCode'),
          name: item.get('name'),
          label: `${item.get('isoCode')} ${item.get('name')}`,
          title: `${item.get('isoCode')}`,
          value: item.get('id')
        });
      default:
        return null;
    }
  })
);

const mapFilterConditionParamsToValue = (params, condition) => {
  if (!params) return '...';
  switch (condition.get('type')) {
    case 'datesRange':
      return `${params.get('dateFrom') ?
        `с ${moment(params.get('dateFrom')).format('DD.MM.YYYY')}` : ''} ${params.get('dateTo') ?
        `по ${moment(params.get('dateTo')).format('DD.MM.YYYY')}` : ''}`;
    case 'amountsRange':
      return `${params.get('amountFrom') ? 
        `от ${numeral(params.get('amountFrom')).format('0,0.00')}` : ''} ${params.get('amountTo') ?
        `до ${numeral(params.get('amountTo')).format('0,0.00')}` : ''}`;
    case 'bool':
      return '';
    case 'multipleSelect':
      return params.join(', ');
    case 'search':
    case 'string':
      switch (condition.get('inputType')) {
        case 'Account':
          return getFormattedAccNum(params);
        default:
          return params;
      }
    default:
      return params;
  }
};

const mapFilterConditions = (conditions, savedConditions, filtersConditions) => (
  (conditions && conditions.size ? conditions : savedConditions).map(condition =>
    condition.merge({
      params: condition.get('params'),
      value: mapFilterConditionParamsToValue(
        condition.get('params'),
        filtersConditions.find(filtersCondition => filtersCondition.get('id') === condition.get('id'))
      )
    })
  )
);

export const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) => columns.map(item => item.merge({
    visible: !!settings.get('visible').find(visible => visible === item.get('id')),
    sorting: settings.getIn(['sorting', 'id']) === item.get('id') ? settings.getIn(['sorting', 'direction']) : 0,
    grouped: settings.get('grouped') === item.get('id'),
    width: settings.getIn(['width', item.get('id')])
  }))
);

export const filtersConditionsCreatedSelector = createSelector(
  filtersConditionsSelector,
  filtersConditions => filtersConditions.map(
    condition => (condition.get('type') === 'suggest' || condition.get('type') === 'search'
      ? mapFilterConditionDictionaryItems(condition)
      : condition)
  )
);

export const filtersCreatedSelector = createSelector(
  filtersSelector,
  filtersConditionsSelector,
  (filters, filtersConditions) => filters
    .map(filter => filter.merge({
      conditions: mapFilterConditions(filter.get('conditions'), filter.get('savedConditions'), filtersConditions)
    }))
    .sort((a, b) => {
      if (a.get('isPreset') && b.get('isPreset')) return 0;
      if (a.get('isPreset') && !b.get('isPreset')) return -1;
      if (!a.get('isPreset') && b.get('isPreset')) return 1;
      return a.get('title').localeCompare(b.get('title'));
    })
);

const getGroupTitle = (group, grouped) => {
  switch (grouped) {
    case 'stateType':
      return group ? 'Итоговая' : 'Внутридневная';
    default:
      return group;
  }
};

export const listCreatedSelector = createSelector(
  listSelector,
  selectedItemsSelector,
  settingsSelector,
  operationsSelector,
  dalOrganizationsSelector,
  (list, selectedItems, settings, operations,  dalOrganizations) => {
    if (!list.size) return list;
    const mappedList = list.map(item =>
      mapListItem(
        item,
        !!selectedItems.filter(selectedItem => selectedItem === item.get('recordID')).size,
        operations,
        dalOrganizations
      ));
    const grouped = settings.get('grouped');
    if (!grouped) {
      return List([Map({
        id: 'notGrouped',
        selected: mappedList.filter(item => item.get('selected')).size === mappedList.size,
        items: mappedList
      })]);
    }
    return mappedList.map(item => item.get(grouped)).toOrderedSet().toList().map((group, i) => {
      const items = mappedList.filter(item => item.get(grouped) === group);
      return Map({
        id: `${grouped}_${i}`,
        title: getGroupTitle(group, grouped),
        selected: items.filter(item => item.get('selected')).size === items.size,
        items
      });
    });
  }
);

export const paginationOptionsCreatedSelector = createSelector(
  paginationOptionsSelector,
  settingsSelector,
  (paginationOptions, settings) => paginationOptions.map(item => ({
    value: item,
    title: item,
    selected: settings.get('pagination') === item
  }))
);

export const panelOperationsCreatedSelector = createSelector(
  operationsSelector,
  selectedItemsSelector,
  listSelector,
  (operations, selectedItems, list) => operations.filter((operation) => {
    const itemsWithOperations = selectedItems.map(item => Map({
      id: item,
      operations: mapListItemOperations(
        operations,
        list.find(listItem => listItem.get('recordID') === item).get('stateType')
      )
    }));
    return (
      itemsWithOperations.size ===
      itemsWithOperations.filter(item => !!item.get('operations')[operation.get('id')]).size
      ) &&
      (selectedItems.size === 1 || operation.get('grouped'));
  })
);

export const tabCreateSelector = createSelector(
  sectionsSelector,
  sections => sections.map(section => section.merge({
    isActive: section.get('selected')
  }))
);

const mapStateToProps = state => ({
  isListLoading: isListLoadingSelector(state),
  isExportListLoading: isExportListLoadingSelector(state),
  isListUpdateRequired: isListUpdateRequiredSelector(state),
  isReloadRequired: isReloadRequiredSelector(state),
  columns: columnsCreatedSelector(state),
  filtersConditions: filtersConditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  list: listCreatedSelector(state),
  operations: operationsSelector(state),
  pages: pagesSelector(state),
  paginationOptions: paginationOptionsCreatedSelector(state),
  panelOperations: panelOperationsCreatedSelector(state),
  sections: sectionsSelector(state),
  selectedItems: selectedItemsSelector(state),
  settings: settingsSelector(state),
  tabs: tabCreateSelector(state)
});

export default mapStateToProps;
