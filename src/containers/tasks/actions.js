import { ACTIONS } from './constants';

/**
 *    action получение настроек контейнера
 */
export const getSettingsAction = () => ({
  type: ACTIONS.TASKS_CONTAINER_GET_SETTINGS_REQUEST
});

/**
 *    action получение query-параметров (номер страницы, фильтр)
 */
export const getQueryParamsAction = () => ({
  type: ACTIONS.TASKS_CONTAINER_GET_QUERY_PARAMS_REQUEST
});

/**
 *    action перехода на страницу (получает index выбранной страницы )
 *    @param {number} pageIndex
 */
export const changePageAction = pageIndex => ({
  type: ACTIONS.TASKS_CONTAINER_CHANGE_PAGE,
  payload: pageIndex
});

/**
 *    action перезагрузки страниц (сброс на первую страницу)
 */
export const reloadPagesAction = () => ({
  type: ACTIONS.TASKS_CONTAINER_RELOAD_PAGES
});

/**
 *    action изменения фильтра (получает изменненную коллекцию filters, флаг обновления листа и флаг
 *    необходимости сохранения фильтра)
 *    @param {List} filters - коллекцию filter
 *    @param {bool} isChanged  - флаг обновления фильтра
 *    @param {bool} isSaveSettingRequired - флаг необходимости сохранения фильтров в настройках
 */
export const changeFiltersAction = (filters, isChanged, isSaveSettingRequired) => ({
  type: ACTIONS.TASKS_CONTAINER_CHANGE_FILTERS,
  payload: { filters, isChanged, isSaveSettingRequired }
});

/**
 *    action получение списка (получает параметры для запроса списка)
 *    @param {object} params
 */
export const getListAction = params => ({
  type: ACTIONS.TASKS_CONTAINER_GET_LIST_REQUEST,
  payload: params
});

/**
 *    action выхода из контейнера
 */
export const unmountAction = () => ({
  type: ACTIONS.TASKS_CONTAINER_UNMOUNT_REQUEST
});

/**
 *    action изменения размера колонки (получает изменненную коллекцию columns)
 *    @param {List} columns
 */
export const resizeColumnsAction = columns => ({
  type: ACTIONS.TASKS_CONTAINER_RESIZE_COLUMNS,
  payload: columns
});

/**
 *    action изменения отображаемых столбцов (получает изменненную коллекцию columns)
 *    @param {List} columns
 */
export const changeColumnsAction = columns => ({
  type: ACTIONS.TASKS_CONTAINER_CHANGE_COLUMN,
  payload: columns
});

/**
 *    action изменения сортировки столбцов (получает изменненную коллекцию columns)
 *    @param {List} columns
 */
export const changeSortingAction = columns => ({
  type: ACTIONS.TASKS_CONTAINER_CHANGE_SORTING,
  payload: columns
});

/**
 *    action изменения кол-ва отображаемых строк (получает кол-во отображаемых строк)
 *    @param {number} value
 */
export const changePaginationAction = value => ({
  type: ACTIONS.TASKS_CONTAINER_CHANGE_PAGINATION,
  payload: value
});
