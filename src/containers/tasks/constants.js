import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  TASKS_CONTAINER_GET_SETTINGS_REQUEST: null,
  TASKS_CONTAINER_GET_SETTINGS_SUCCESS: null,
  TASKS_CONTAINER_GET_QUERY_PARAMS_REQUEST: null,
  TASKS_CONTAINER_GET_QUERY_PARAMS_SUCCESS: null,
  TASKS_CONTAINER_CHANGE_PAGE: null,
  TASKS_CONTAINER_RELOAD_PAGES: null,
  TASKS_CONTAINER_CHANGE_FILTERS: null,
  TASKS_CONTAINER_SAVE_SETTINGS_REQUEST: null,
  TASKS_CONTAINER_SAVE_SETTINGS_SUCCESS: null,
  TASKS_CONTAINER_SAVE_SETTINGS_FAIL: null,
  TASKS_CONTAINER_GET_LIST_REQUEST: null,
  TASKS_CONTAINER_GET_LIST_SUCCESS: null,
  TASKS_CONTAINER_GET_LIST_FAIL: null,
  TASKS_CONTAINER_UNMOUNT_REQUEST: null,
  TASKS_CONTAINER_RESIZE_COLUMNS: null,
  TASKS_CONTAINER_CHANGE_COLUMN: null,
  TASKS_CONTAINER_CHANGE_SORTING: null,
  TASKS_CONTAINER_CHANGE_PAGINATION: null
});

export const LOCAL_REDUCER = 'TasksContainerReducer';

export const LOCAL_ROUTER_ALIAS = '/tasks';

export const SETTINGS_ALIAS = 'TaskContainer';

export const VALIDATE_SETTINGS = settings => !!settings &&
  !!settings.visible &&
  !!settings.sorting &&
  !!settings.pagination &&
  !!settings.width &&
  !!settings.width.taskType &&
  !!settings.width.importType &&
  !!settings.width.status &&
  !!settings.width.started &&
  !!settings.width.finished &&
  !!settings.width.fileName &&
  !!settings.width.totalDocs;

export const LOCATORS = {
  CONTAINER: 'tasksContainer',
  SPREADSHEET: 'tasksContainerSpreadsheet',

  TOOLBAR: 'tasksContainerSpreadsheetToolbar',
  TOOLBAR_BUTTONS: 'tasksContainerSpreadsheetToolbarButtons',
  TOOLBAR_BUTTON_REFRESH_LIST: 'tasksContainerSpreadsheetToolbarButtonRefreshList',

  FILTERS: 'tasksContainerSpreadsheetFilters',
  FILTERS_SELECT: 'tasksContainerSpreadsheetFiltersSelect',
  FILTERS_ADD: 'tasksContainerSpreadsheetFiltersAdd',
  FILTERS_REMOVE: 'tasksContainerSpreadsheetFiltersRemove',
  FILTERS_OPERATIONS: 'tasksContainerSpreadsheetFiltersOperations',
  FILTERS_OPERATION_SAVE: 'tasksContainerSpreadsheetFiltersOperationSave',
  FILTERS_OPERATION_SAVE_AS: 'tasksContainerSpreadsheetFiltersOperationSaveAs',
  FILTERS_OPERATION_SAVE_AS_POPUP: 'tasksContainerSpreadsheetFiltersOperationSaveAsPopup',
  FILTERS_OPERATION_CLEAR: 'tasksContainerSpreadsheetFiltersOperationClear',
  FILTERS_CONDITIONS: 'tasksContainerSpreadsheetFiltersConditions',
  FILTERS_CONDITION: 'tasksContainerSpreadsheetFiltersCondition',
  FILTERS_CONDITION_POPUP: 'tasksContainerSpreadsheetFiltersConditionPopup',
  FILTERS_CONDITION_REMOVE: 'tasksContainerSpreadsheetFiltersConditionRemove',
  FILTERS_ADD_CONDITION: 'tasksContainerSpreadsheetFiltersAddCondition',
  FILTERS_ADD_CONDITION_POPUP: 'tasksContainerSpreadsheetFiltersAddConditionPopup',
  FILTERS_CLOSE: 'tasksContainerSpreadsheetFiltersClose',

  TABLE: 'tasksContainerSpreadsheetTable',
  TABLE_HEAD: 'tasksContainerSpreadsheetTableHead',
  TABLE_BODY: 'tasksContainerSpreadsheetTableBody',
  TABLE_SETTINGS: 'tasksContainerSpreadsheetTableSettings',
  TABLE_SETTINGS_TOGGLE: 'tasksContainerSpreadsheetTableSettingsToggle',
  TABLE_SETTINGS_TOGGLE_BUTTON: 'tasksContainerSpreadsheetTableSettingsToggleButton',
  TABLE_SETTINGS_POPUP: 'tasksContainerSpreadsheetTableSettingsPopup',

  PAGINATION: 'tasksContainerSpreadsheetPagination'
};
