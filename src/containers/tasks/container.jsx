import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import Spreadsheet, { Toolbar, Filters, Table, Pagination } from 'components/ui-components/spreadsheet';
import { LOCATORS } from './constants';
import './style.css';

class TasksContainer extends Component {

  static propTypes = {
    isListLoading: PropTypes.bool.isRequired,
    isListUpdateRequired: PropTypes.bool.isRequired,
    isReloadRequired: PropTypes.bool.isRequired,

    columns: PropTypes.instanceOf(List).isRequired,
    filtersConditions: PropTypes.instanceOf(List).isRequired,
    filters: PropTypes.instanceOf(List).isRequired,
    list: PropTypes.instanceOf(List).isRequired,
    location: PropTypes.object.isRequired,
    pages: PropTypes.instanceOf(Map).isRequired,
    paginationOptions: PropTypes.instanceOf(List).isRequired,

    getSettingsAction: PropTypes.func.isRequired,
    getQueryParamsAction: PropTypes.func.isRequired,
    changePageAction: PropTypes.func.isRequired,
    reloadPagesAction: PropTypes.func.isRequired,
    changeFiltersAction: PropTypes.func.isRequired,
    getListAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    resizeColumnsAction: PropTypes.func.isRequired,
    changeColumnsAction: PropTypes.func.isRequired,
    changeSortingAction: PropTypes.func.isRequired,
    changePaginationAction: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.getSettingsAction();
    this.props.getQueryParamsAction();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getQueryParamsAction();
    } else if (nextProps.location.search !== this.props.location.search) {
      this.props.getQueryParamsAction();
    }
  }

  componentDidUpdate() {
    if (this.props.isReloadRequired) {
      this.props.reloadPagesAction();
    }

    if (this.props.isListUpdateRequired) {
      this.props.getListAction();
    }
  }

  componentWillUnmount() {
    this.props.unmountAction();
  }

  renderItemAlignRightElement = (value) => {
    const style = { textAlign: 'right' };
    return (
      <div style={style}>{value}</div>
    );
  };

  render() {
    const {
      isListLoading,
      columns,
      filtersConditions,
      filters,
      list,
      pages,
      paginationOptions,
      getListAction,
      changeColumnsAction,
      changeFiltersAction,
      changePaginationAction,
      changePageAction,
      changeSortingAction,
      resizeColumnsAction,
    } = this.props;

    return (
      <div className="b-tasks-container" data-loc={LOCATORS.CONTAINER}>
        <Spreadsheet dataLoc={LOCATORS.SPREADSHEET}>
          <header>
            <Toolbar
              dataLocs={{
                main: LOCATORS.TOOLBAR,
                buttons: {
                  main: LOCATORS.TOOLBAR_BUTTONS,
                  refreshList: LOCATORS.TOOLBAR_BUTTON_REFRESH_LIST,
                },
                tabs: LOCATORS.TOOLBAR_TABS
              }}
              title="Журнал заданий"
              buttons={[
                {
                  id: 'refreshList',
                  isProgress: isListLoading,
                  type: 'refresh',
                  title: 'Обновить',
                  onClick: getListAction
                }
              ]}
            />
            <Filters
              dataLocs={{
                main: LOCATORS.FILTERS,
                select: LOCATORS.FILTERS_SELECT,
                add: LOCATORS.FILTERS_ADD,
                remove: LOCATORS.FILTERS_REMOVE,
                operations: LOCATORS.FILTERS_OPERATIONS,
                operationSave: LOCATORS.FILTERS_OPERATION_SAVE,
                operationSaveAs: LOCATORS.FILTERS_OPERATION_SAVE_AS,
                operationSaveAsPopup: LOCATORS.FILTERS_OPERATION_SAVE_AS_POPUP,
                operationClear: LOCATORS.FILTERS_OPERATION_CLEAR,
                conditions: LOCATORS.FILTERS_CONDITIONS,
                condition: LOCATORS.FILTERS_CONDITION,
                conditionPopup: LOCATORS.FILTERS_CONDITION_POPUP,
                conditionRemove: LOCATORS.FILTERS_CONDITION_REMOVE,
                addCondition: LOCATORS.FILTERS_ADD_CONDITION,
                addConditionPopup: LOCATORS.FILTERS_ADD_CONDITION_POPUP,
                close: LOCATORS.FILTERS_CLOSE
              }}
              conditions={filtersConditions}
              filters={filters}
              changeFiltersAction={changeFiltersAction}
            />
          </header>
          <main>
            <Table
              dataLocs={{
                main: LOCATORS.TABLE,
                head: LOCATORS.TABLE_HEAD,
                body: LOCATORS.TABLE_BODY,
                settings: {
                  main: LOCATORS.TABLE_SETTINGS,
                  toggle: LOCATORS.TABLE_SETTINGS_TOGGLE,
                  toggleButton: LOCATORS.TABLE_SETTINGS_TOGGLE_BUTTON,
                  popup: LOCATORS.TABLE_SETTINGS_POPUP
                }
              }}
              columns={columns}
              list={list}
              renderBodyCells={{
                started: this.renderItemAlignRightElement,
                finished: this.renderItemAlignRightElement,
                totalDocs: this.renderItemAlignRightElement
              }}
              renderHeadCells={{
                started: this.renderItemAlignRightElement,
                finished: this.renderItemAlignRightElement,
                totalDocs: this.renderItemAlignRightElement
              }}
              settings={{
                iconTitle: 'Настройка таблицы журнала заданий',
                changeColumnsAction,
                showPagination: true,
                paginationOptions: paginationOptions.toJS(),
                changePaginationAction
              }}
              sorting={{
                changeSortingAction
              }}
              resize={{
                resizeColumnsAction
              }}
              isListLoading={isListLoading}
              emptyListMessage="Данных по заданному фильтру не найдено. Попробуйте изменить условие."
            />
          </main>
          <footer>
            {!isListLoading && pages.get('size') > 1 &&
            <Pagination
              dataLoc={LOCATORS.PAGINATION}
              pages={pages}
              onClick={changePageAction}
            />
            }
          </footer>
        </Spreadsheet>
      </div>
    );
  }
}

export default TasksContainer;
