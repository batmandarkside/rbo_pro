import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import mapStateToProps from './selectors';
import * as Actions from './actions';
import saga from './sagas';
import reducer from './reducer';
import TasksContainer from './container';


const withConnect = connect(mapStateToProps, Actions);
const withReducer = injectReducer({ key: 'TasksContainerReducer', reducer });
const withSaga = injectSaga({ key: 'tasksContainerSagas', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(TasksContainer);
