import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const initialSections = [
  {
    id: 'tasks',
    title: 'Журнал заданий',
    route: '/tasks',
    selected: true
  }
];

export const initialFiltersConditions = [
  {
    id: 'status',
    title: 'Статус',
    type: 'multipleSelect',
    values: [
      'Разбор',
      'Импорт платежных поручений',
      'Обработано',
      'Отменяется',
      'Отменено пользователем',
      'Прерван Банком'
    ],
    sections: ['working', 'archive']
  },
  {
    id: 'started',
    title: 'Время начала',
    type: 'datesRange'
  }
];

export const initialColumns = [
  {
    id: 'taskType',
    title: 'Тип',
    canGrouped: false
  },
  {
    id: 'importType',
    title: 'Формат',
    canGrouped: false
  },
  {
    id: 'status',
    title: 'Статус',
    canGrouped: false
  },
  {
    id: 'started',
    title: 'Время начала',
    canGrouped: false
  },
  {
    id: 'finished',
    title: 'Время завершения',
    canGrouped: false
  },
  {
    id: 'fileName',
    title: 'Имя файла',
    canGrouped: false
  },
  {
    id: 'totalDocs',
    title: 'Количество документов',
    canGrouped: false
  }
];

export const initialSettings = {
  visible: ['importType', 'status', 'started', 'finished', 'fileName', 'totalDocs'],
  sorting: { id: 'started', direction: -1 },
  grouped: null,
  pagination: 30,
  width: {
    taskType: 80,
    importType: 94,
    status: 224,
    started: 168,
    finished: 168,
    fileName: 320,
    totalDocs: 200
  }
};

export const initialPages = { size: 1, selected: 0, visible: 1 };

export const initialPaginationOptions = [30, 60, 120, 240];

export const initialState = fromJS({
  isListLoading: false,
  isListUpdateRequired: false,
  isReloadRequired: false,
  sections: initialSections,
  settings: initialSettings,
  columns: initialColumns,
  paginationOptions: initialPaginationOptions,
  pages: initialPages,
  filtersConditions: initialFiltersConditions,
  filters: [],
  list: []
});

export const getSettingsSuccess = (state, settings) => state.merge({
  filters: settings && settings.filters ? state.get('filters').concat(fromJS(settings.filters)) : state.get('filters'),
  settings: settings ? fromJS(settings).delete('filters') : state.get('settings')
});

export const getQueryParams = (state, { pageIndex, filter }) => {
  const normalizePageIndex = pageIndex > 0 ? pageIndex : 0;
  const pages = state.get('pages').merge({
    size: normalizePageIndex + 1,
    selected: normalizePageIndex
  });

  let filters;
  if (filter) {
    if (state.get('filters').find(item => item.get('id') === filter.id)) {
      filters = state.get('filters').map((item) => {
        if (item.get('id') === filter.id) {
          return item.merge({
            conditions: fromJS(filter.conditions),
            isChanged: filter.isChanged === 'true',
            isNew: filter.isNew === 'true',
            isPreset: filter.isPreset === 'true',
            selected: true
          });
        }
        return item;
      });
    } else {
      filters = state.get('filters').push(fromJS({
        savedConditions: [],
        conditions: filter.conditions,
        isChanged: true,
        isNew: true,
        isPreset: true,
        selected: true
      }));
    }
  } else {
    filters = state.get('filters').map(item => item.set('selected', false));
  }

  return state.merge({
    pages,
    filters: filters || state.get('filters'),
    isListUpdateRequired: true
  });
};

export const reloadPagesRequest = state => state.merge({
  isReloadRequired: false
});

export const changeFilters = (state, { filters }) => state.merge({
  filters
});

export const saveSettings = (state, settings) => state.merge({
  settings: fromJS(settings)
});

export const getListRequest = state => state.merge({
  list: fromJS([]),
  isListLoading: true,
  isListUpdateRequired: false
});

export const getListSuccess = (state, list) => {
  const immutableList = fromJS(list);
  const pages = state.get('pages');
  const selectedPageIndex = pages.get('selected');
  if (!immutableList.size && selectedPageIndex) {
    return state.merge({
      pages: fromJS(initialPages),
      isReloadRequired: true
    });
  }
  if (immutableList.size > state.getIn(['settings', 'pagination'])) {
    return state.merge({
      pages: selectedPageIndex === pages.get('size') - 1 ? pages.set('size', pages.get('size') + 1) : pages,
      list: immutableList.setSize(state.getIn(['settings', 'pagination'])),
      isListLoading: false
    });
  }
  return state.merge({
    list: immutableList,
    isListLoading: false
  });
};

export const unmountRequest = state => state.merge({
  list: fromJS([]),
  pages: fromJS(initialPages),
  filters: []
});

export const resizeColumns = (state, columns) => state.merge({
  columns
});

export const changeColumn = (state, columns) => state.merge({
  columns
});

export const changeSorting = (state, columns) => state.merge({
  columns,
  isListUpdateRequired: true
});

export const changePagination = state => state.merge({
  pages: fromJS(initialPages),
  isReloadRequired: true,
  isListUpdateRequired: true
});

function TasksContainerReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.TASKS_CONTAINER_GET_SETTINGS_SUCCESS:
      return getSettingsSuccess(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_GET_QUERY_PARAMS_SUCCESS:
      return getQueryParams(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_RELOAD_PAGES:
      return reloadPagesRequest(state);

    case ACTIONS.TASKS_CONTAINER_CHANGE_FILTERS:
      return changeFilters(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_REQUEST:
      return saveSettings(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.TASKS_CONTAINER_GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_UNMOUNT_REQUEST:
      return unmountRequest(state);

    case ACTIONS.TASKS_CONTAINER_RESIZE_COLUMNS:
      return resizeColumns(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_CHANGE_COLUMN:
      return changeColumn(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_CHANGE_SORTING:
      return changeSorting(state, action.payload);

    case ACTIONS.TASKS_CONTAINER_CHANGE_PAGINATION:
      return changePagination(state, action.payload);

    default:
      return state;
  }
}

export default TasksContainerReducer;
