import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import qs from 'qs';
import { addOrdinaryNotificationAction as dalAddOrdinaryNotificationAction } from 'dal/notification/actions';
import { getImports as dalGetList, } from 'dal/r-payments/sagas';
import { updateSettings as dalSaveSettings } from 'dal/profile/sagas';
import { ACTIONS, LOCAL_REDUCER, SETTINGS_ALIAS, VALIDATE_SETTINGS } from './constants';

const getParamsForDalOperation = ({ settings, pages, filters }) => {
  const offset = pages.get('selected') * settings.get('pagination');
  const offsetStep = settings.get('pagination') + 1;
  const sorting = settings.get('sorting');
  const sort = sorting ? sorting.get('id') : null;
  const desc = sorting ? sorting.get('direction') === -1 : null;
  const selectedFilter = filters.find(filter => filter.get('selected'));
  const conditions = selectedFilter && selectedFilter.get('conditions');
  const conditionStarted = conditions && conditions.find(c => c.get('id') === 'started');
  const dateFrom = (conditionStarted && conditionStarted.getIn(['params', 'dateFrom'])) || null;
  const dateTo = (conditionStarted && conditionStarted.getIn(['params', 'dateTo'])) || null;
  const conditionStatus = conditions && conditions.find(c => c.get('id') === 'status');
  const status = (conditionStatus && conditionStatus.get('params')) ? conditionStatus.get('params').toJS() : null;

  return { offset, offsetStep, sort, desc, dateFrom, dateTo, status };
};

export const routingLocationSelector = state => state.getIn(['routing', 'locationBeforeTransitions']);
export const profileSettingsSelector = state => state.getIn(['profile', 'settings']);
export const localSectionsSelector = state => state.getIn([LOCAL_REDUCER, 'sections']);
export const localSettingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
export const localFiltersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
export const localPagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);

export function* getSettings() {
  const sections = yield select(localSectionsSelector);
  const storedProfileSettings = yield select(profileSettingsSelector);
  const selectedSectionId = sections.find(section => section.get('selected')).get('id');
  const params = (storedProfileSettings && storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId])) ?
    storedProfileSettings.getIn([SETTINGS_ALIAS, selectedSectionId]).toJS() : null;
  const settingsAreValid = VALIDATE_SETTINGS(params);
  yield put({ type: ACTIONS.TASKS_CONTAINER_GET_SETTINGS_SUCCESS, payload: settingsAreValid ? params : null });
}

export function* getQueryParams() {
  const location = yield select(routingLocationSelector);
  const query = qs.parse(location.get('query').toJS());
  const pageIndex = parseInt(query.page, 10) - 1;
  const filter = query.filter;
  yield put({ type: ACTIONS.TASKS_CONTAINER_GET_QUERY_PARAMS_SUCCESS, payload: { pageIndex, filter } });
}

export function* changePage(action) {
  const { payload } = action;
  const location = yield select(routingLocationSelector);
  const currentPage = location.getIn(['query', 'page'], 0);
  if (currentPage !== payload) {
    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(location.get('query').set('page', payload + 1).toJS())}`
    }));
  }
}

export function* reloadPages() {
  const location = yield select(routingLocationSelector);
  yield put(push({
    pathname: location.get('currentLocation'),
    search: `?${qs.stringify(location.get('query').set('page', 1).toJS())}`
  }));
}

export function* changeFilters(action) {
  const { payload } = action;

  if (payload.isChanged) {
    const selectedFilter = payload.filters && payload.filters.find(filter => filter.get('selected'));
    const location = yield select(routingLocationSelector);
    const query = qs.parse(location.get('query').toJS());

    if (selectedFilter) {
      query.filter = {
        id: selectedFilter.get('id'),
        isPreset: selectedFilter.get('isPreset') || false,
        isChanged: selectedFilter.get('isChanged') || false,
        isNew: selectedFilter.get('isNew') || false,
        conditions: selectedFilter.get('conditions').map(item => item.delete('value').delete('isChanging')).toJS(),
        selected: true
      };
    } else {
      delete query.filter;
    }
    delete query.page;

    yield put(push({
      pathname: location.get('currentLocation'),
      search: `?${qs.stringify(query)}`
    }));
  }

  if (payload.isSaveSettingRequired) {
    const storedSettings = yield select(localSettingsSelector);
    const params = storedSettings.merge({
      filters: payload.filters.filter(item => !item.get('isPreset')).map(item => ({
        id: item.get('id'),
        title: item.get('title'),
        isPreset: item.get('isPreset'),
        savedConditions: item.get('savedConditions').toJS()
      })).toJS()
    });
    yield put({ type: ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
  }
}

export function* saveSettings(action) {
  const { payload } = action;
  try {
    const sections = yield select(localSectionsSelector);
    const storedProfileSettings = yield select(profileSettingsSelector);
    const selectedSectionId = sections.find(section => section.get('selected')).get('id');
    const params = { settings: storedProfileSettings.setIn([SETTINGS_ALIAS, selectedSectionId], payload).toJS() };
    yield call(dalSaveSettings, { payload: params });
    yield put({ type: ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_SUCCESS, payload });
  } catch (error) {
    yield put({
      type: ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* getList() {
  try {
    const settings = yield select(localSettingsSelector);
    const pages = yield select(localPagesSelector);
    const filters = yield select(localFiltersSelector);
    const params = getParamsForDalOperation({ settings, pages, filters });
    const result = yield call(dalGetList, { payload: params });
    yield put({ type: ACTIONS.TASKS_CONTAINER_GET_LIST_SUCCESS, payload: result });
  } catch (error) {
    yield put({
      type: ACTIONS.TASKS_CONTAINER_GET_LIST_FAIL,
      error: 'Произошла ошибка на сервере'
    });
  }
}

export function* changeSettingsWidth(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const width = storedSettings.get('width').map(
    (value, key) => payload.find(item => item.get('id') === key).get('width')
  );
  const params = storedSettings.merge({
    width
  });
  yield put({ type: ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsColumns(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const visibleItems = payload.filter(item => item.get('visible'));
  const sortingItem = payload.find(item => !!item.get('sorting'));
  const groupedItem = payload.find(item => item.get('grouped'));
  const params = storedSettings.merge({
    visible: visibleItems.map(item => item.get('id')),
    sorting: { id: sortingItem.get('id'), direction: sortingItem.get('sorting') },
    grouped: groupedItem ? groupedItem.get('id') : null
  });
  yield put({ type: ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* changeSettingsPagination(action) {
  const { payload } = action;
  const storedSettings = yield select(localSettingsSelector);
  const params = storedSettings.merge({
    pagination: payload
  });
  yield put({ type: ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_REQUEST, payload: params });
}

export function* errorNotification(action) {
  const { error } = action;
  yield put(dalAddOrdinaryNotificationAction({ type: 'error', title: error }));
}

export default function* saga() {
  yield takeLatest(ACTIONS.TASKS_CONTAINER_GET_SETTINGS_REQUEST, getSettings);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_GET_QUERY_PARAMS_REQUEST, getQueryParams);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_CHANGE_PAGE, changePage);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_RELOAD_PAGES, reloadPages);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_CHANGE_FILTERS, changeFilters);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_REQUEST, saveSettings);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_SAVE_SETTINGS_FAIL, errorNotification);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_GET_LIST_REQUEST, getList);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_GET_LIST_FAIL, errorNotification);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_RESIZE_COLUMNS, changeSettingsWidth);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_CHANGE_COLUMN, changeSettingsColumns);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_CHANGE_SORTING, changeSettingsColumns);
  yield takeLatest(ACTIONS.TASKS_CONTAINER_CHANGE_PAGINATION, changeSettingsPagination);
}
