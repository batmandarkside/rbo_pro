import { createSelector } from 'reselect';
import { List, Map } from 'immutable';
import moment from 'moment-timezone';
import { LOCAL_REDUCER } from './constants';

const isListLoadingSelector = state => state.getIn([LOCAL_REDUCER, 'isListLoading']);
const isListUpdateRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isListUpdateRequired']);
const isReloadRequiredSelector = state => state.getIn([LOCAL_REDUCER, 'isReloadRequired']);

const columnsSelector = state => state.getIn([LOCAL_REDUCER, 'columns']);
const filtersConditionsSelector = state => state.getIn([LOCAL_REDUCER, 'filtersConditions']);
const filtersSelector = state => state.getIn([LOCAL_REDUCER, 'filters']);
const listSelector = state => state.getIn([LOCAL_REDUCER, 'list']);
const settingsSelector = state => state.getIn([LOCAL_REDUCER, 'settings']);
const pagesSelector = state => state.getIn([LOCAL_REDUCER, 'pages']);
const paginationOptionsSelector = state => state.getIn([LOCAL_REDUCER, 'paginationOptions']);

const mapListItem = item => Map({
  id: item.get('id'),
  importType: item.get('importType'),
  taskType: item.get('taskType'),
  status: item.get('status'),
  started: item.get('started') ? moment(item.get('started')).format('DD.MM.YYYY HH:mm:ss') : null,
  finished: item.get('finished') ? moment(item.get('finished')).format('DD.MM.YYYY HH:mm:ss') : null,
  fileName: item.get('fileName'),
  totalDocs: item.get('totalDocs')
});

const mapFilterConditionParamsToValue = (params, condition) => {
  if (!params) return '...';
  switch (condition.get('type')) {
    case 'datesRange':
      return `${params.get('dateFrom') ?
        `с ${moment(params.get('dateFrom')).format('DD.MM.YYYY')}` : ''} ${params.get('dateTo') ?
        `по ${moment(params.get('dateTo')).format('DD.MM.YYYY')}` : ''}`;
    case 'multipleSelect':
      return params.join(', ');
    default:
      return params;
  }
};

const mapFilterConditions = (conditions, savedConditions, filtersConditions) => (
  (conditions && conditions.size ? conditions : savedConditions).map(condition =>
    condition.merge({
      params: condition.get('params'),
      value: mapFilterConditionParamsToValue(
        condition.get('params'),
        filtersConditions.find(filtersCondition => filtersCondition.get('id') === condition.get('id'))
      )
    })
  )
);

export const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) => columns.map(item => item.merge({
    visible: !!settings.get('visible').find(visible => visible === item.get('id')),
    sorting: settings.getIn(['sorting', 'id']) === item.get('id') ? settings.getIn(['sorting', 'direction']) : 0,
    grouped: settings.get('grouped') === item.get('id'),
    width: settings.getIn(['width', item.get('id')])
  }))
);

export const filtersCreatedSelector = createSelector(
  filtersSelector,
  filtersConditionsSelector,
  (filters, filtersConditions) => filters
    .map(filter => filter.merge({
      conditions: mapFilterConditions(filter.get('conditions'), filter.get('savedConditions'), filtersConditions)
    }))
    .sort((a, b) => {
      if (a.get('isPreset') && b.get('isPreset')) return 0;
      if (a.get('isPreset') && !b.get('isPreset')) return -1;
      if (!a.get('isPreset') && b.get('isPreset')) return 1;
      return a.get('title').localeCompare(b.get('title'));
    })
);

export const listCreatedSelector = createSelector(
  listSelector,
  list => (
    list.size ? List([Map({ id: 'notGrouped', items: list.map(item => mapListItem(item)) })]) : list
  )
);

export const paginationOptionsCreatedSelector = createSelector(
  paginationOptionsSelector,
  settingsSelector,
  (paginationOptions, settings) => paginationOptions.map(item => ({
    value: item,
    title: item,
    selected: settings.get('pagination') === item
  }))
);

const mapStateToProps = state => ({
  isListLoading: isListLoadingSelector(state),
  isListUpdateRequired: isListUpdateRequiredSelector(state),
  isReloadRequired: isReloadRequiredSelector(state),
  columns: columnsCreatedSelector(state),
  filtersConditions: filtersConditionsSelector(state),
  filters: filtersCreatedSelector(state),
  list: listCreatedSelector(state),
  pages: pagesSelector(state),
  paginationOptions: paginationOptionsCreatedSelector(state),
  settings: settingsSelector(state)
});

export default mapStateToProps;
