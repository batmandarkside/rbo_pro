import { put, call, select } from 'redux-saga/effects';
import { saveAs } from 'file-saver';
import moment from 'moment-timezone';
import API from 'app/api';
import { selectedOrganizationsSelector } from '../utils';
import { ACTIONS } from './constants';

export function* getAccounts(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_ACCOUNTS_GET_LIST_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result = yield call(API.accounts.findAllAccountsByQuery, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_ACCOUNTS_GET_LIST_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_ACCOUNTS_GET_LIST_FAIL, payload: [], error });
    throw error;
  }
}

export function* getMovements(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_ACCOUNTS_GET_MOVEMENTS_LIST_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result =  yield call(API.accounts.findAllMovementsByQuery, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_ACCOUNTS_GET_MOVEMENTS_LIST_SUCCESS, resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_ACCOUNTS_GET_MOVEMENTS_LIST_FAIL, error });
    throw error;
  }
}

export function* exportMovementsListToExcel(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_ACCOUNTS_EXPORT_MOVEMENTS_LIST_TO_EXCEL_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result = yield call(API.exports.createAccountOperations, data);
    saveAs(
      result.data,
      `operations_${moment().format('DD-MM-YYYY_HH-mm-ss')}.xlsx`
    );
    yield put({ type: ACTIONS.DAL_ACCOUNTS_EXPORT_MOVEMENTS_LIST_TO_EXCEL_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_ACCOUNTS_EXPORT_MOVEMENTS_LIST_TO_EXCEL_FAIL, error });
    throw error;
  }
}

export function* printMovements(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_ACCOUNTS_PRINT_MOVEMENTS_REQUEST, payload });
  const { id } = payload;
  const ids = Array.isArray(id) ? id : [id];
  try {
    const result = yield call(API.exports.createOperationById, { docId: ids, format: 'pdf' });
    saveAs(result.data, `operations_${moment().format('DD-MM-YYYY_HH-mm-ss')}.pdf`);
    yield put({ type: ACTIONS.DAL_ACCOUNTS_PRINT_MOVEMENTS_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_ACCOUNTS_PRINT_MOVEMENTS_FAIL, error });
    throw error;
  }
}
