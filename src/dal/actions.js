/**
 * Created by drune on 03/03/2017.
 */
import { START_APP } from './constants';

export function startApp() {
  return { type: START_APP };
}
