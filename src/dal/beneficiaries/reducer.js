import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const defaultState = fromJS({
  list: []
});

export default function benefeciarsReducer(state = defaultState, { type, payload }) {
  switch (type) {
    case ACTIONS.LOAD_BENEFICIARIES_SUCCESS:
      return state.set('list', fromJS(payload));
    default:
      return state;
  }
}
