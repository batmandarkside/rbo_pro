import { call, put, takeLatest, select } from 'redux-saga/effects';
import Api from 'app/api';
import { ACTIONS } from './constants';
import { RBOControlsError } from '../utils';

export const selectedOrgsSelector = state => state.getIn(['profile', 'settings', 'selected_orgs']);

export function* getBeneficiars({ payload }) {
  const selectedOrgs = yield select(selectedOrgsSelector);
  try {
    const params = payload || {};
    const result = yield call(Api.beneficiaries.findAllByQuery, {
      ...params,
      orgId: selectedOrgs.toJS()
    });
    const data = (result && result.data) || [];
    yield put({ type: ACTIONS.LOAD_BENEFICIARIES_SUCCESS, payload: data });

    return data;
  } catch (error) {
    yield put({ type: ACTIONS.LOAD_BENEFICIARIES_FAIL, error });
    throw error;
  }
}

export function* fetchBeneficiars({ payload }) {
  const selectedOrgs = yield select(selectedOrgsSelector);
  const params = payload || {};
  const result = yield call(Api.beneficiaries.findAllByQuery, {
    ...params,
    orgId: selectedOrgs.toJS()
  });
  return (result && result.data) || [];
}

export function* getBeneficiaryById({ payload }) {
  try {
    yield put({ type: ACTIONS.LOAD_BENEFICIARY_BY_ID_REQUEST });
    const result = yield call(Api.beneficiaries.getById, payload);
    const data = (result && result.data) || {};
    yield put({ type: ACTIONS.LOAD_BENEFICIARY_BY_ID_SUCCESS });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.LOAD_BENEFICIARY_BY_ID_FAIL });
    throw error;
  }
}

export function* saveBeneficiary({ payload }) {
  try {
    yield put({ type: ACTIONS.SAVE_BENEFICIARY_REQUEST });
    const selectedOrgs = yield select(selectedOrgsSelector);
    const firstSelectedOrg = selectedOrgs && selectedOrgs.first();
    const result = yield call(Api.beneficiaries.save, { orgId: firstSelectedOrg, ...payload });
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.SAVE_BENEFICIARY_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения документа', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.SAVE_BENEFICIARY_FAIL });
    throw error;
  }
}

export function* removeBeneficiary({ payload }) {
  try {
    yield put({ type: ACTIONS.REMOVE_BENEFICIARY_REQUEST });
    const result = yield call(Api.beneficiaries.remove, payload);
    const data = (result && result.data) || {};
    yield put({ type: ACTIONS.REMOVE_BENEFICIARY_SUCCESS });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.REMOVE_BENEFICIARY_FAIL });
    throw error;
  }
}

export function* validateBeneficiary({ payload }) {
  try {
    yield put({ type: ACTIONS.VALIDATE_BENEFICIARY_REQUEST });
    const selectedOrgs = yield select(selectedOrgsSelector);
    const firstSelectedOrg = selectedOrgs && selectedOrgs.first();
    const result = yield call(Api.beneficiaries.validate, { orgId: firstSelectedOrg, ...payload });
    const data = (result && result.data) || {};
    yield put({ type: ACTIONS.VALIDATE_BENEFICIARY_SUCCESS });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.VALIDATE_BENEFICIARY_FAIL });
    throw error;
  }
}

export default function* beneficiariesSaga() {
  yield takeLatest(ACTIONS.LOAD_BENEFICIARIES_REQUEST, getBeneficiars);
}
