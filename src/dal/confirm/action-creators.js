import { ACTION_TYPES } from './constants';

export const confirmRequestAction = options => ({
  type: ACTION_TYPES.CONFIRM_REQUEST,
  payload: { options }
});

export const confirmSuccessAction = () => ({
  type: ACTION_TYPES.CONFIRM_SUCCESS
});

export const confirmCancelAction = () => ({
  type: ACTION_TYPES.CONFIRM_CANCEL
});
