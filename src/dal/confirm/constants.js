import keyMirror from 'keymirror';

export const ACTION_TYPES = keyMirror({
  'CONFIRM_REQUEST': null,
  'CONFIRM_SUCCESS': null,
  'CONFIRM_CANCEL': null
});
