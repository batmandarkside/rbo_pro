import { fromJS } from 'immutable';
import { ACTION_TYPES } from './constants';

const emptyState = fromJS({
  current: null
});

const initialState = emptyState;

function confirm(state, { options }) {
  return state.set('current', fromJS(options));
}

export default function confirmReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ACTION_TYPES.CONFIRM_REQUEST:
      return confirm(state, payload);

    case ACTION_TYPES.CONFIRM_SUCCESS:
    case ACTION_TYPES.CONFIRM_CANCEL:
      return emptyState;

    default:
      return state;
  }
}
