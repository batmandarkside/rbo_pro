import { takeLatest, take, put } from 'redux-saga/effects';
import { ACTION_TYPES } from './constants';

function* confirmRequestSaga({ payload }) {
  const {
    options: {
      success: { action: successAction }
    }
  } = payload;

  yield take(ACTION_TYPES.CONFIRM_SUCCESS);
  yield put(successAction);
}

export default function* confirmSagas() {
  yield takeLatest(ACTION_TYPES.CONFIRM_REQUEST, confirmRequestSaga);
}
