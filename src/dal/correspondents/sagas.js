import { put, call, select } from 'redux-saga/effects';
import API from 'app/api';
import { selectedOrganizationsSelector, RBOControlsError } from '../utils';
import { ACTIONS } from './constants';

export function* getCorrespondents(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_LIST_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result = yield call(API.correspondent.findAllByQuery, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_LIST_SUCCESS, payload: resultData  });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_LIST_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCorrespondentData(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_CORRESPONDENT_DATA_REQUEST, payload });
  const { id } = payload;
  try {
    const result = yield call(API.correspondent.findOneById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_CORRESPONDENT_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_CORRESPONDENT_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getCorrespondentErrors(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_CORRESPONDENT_ERRORS_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    customerBankRecordID: firstSelectedOrganization,
    ...payload
  };
  try {
    const result =  yield call(API.correspondent.validate, data);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_CORRESPONDENT_ERRORS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_GET_CORRESPONDENT_ERRORS_FAIL, payload: {}, error });
    throw error;
  }
}

export function* saveCorrespondentData(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_CORRESPONDENTS_SAVE_CORRESPONDENT_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    orgId: firstSelectedOrganization,
    ...payload
  };
  try {
    const result = yield call(API.correspondent.update, data);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.DAL_CORRESPONDENTS_SAVE_CORRESPONDENT_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения корреспондента', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_SAVE_CORRESPONDENT_FAIL, payload: {}, error });
    throw error;
  }
}

export function* removeCorrespondent(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_CORRESPONDENTS_REMOVE_CORRESPONDENT_REQUEST, payload });
  const { id } = payload;
  const ids = Array.isArray(id) ? id : [id];
  try {
    const result = yield call(API.correspondent.deleteByIds, ids);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_REMOVE_CORRESPONDENT_REQUEST, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CORRESPONDENTS_REMOVE_CORRESPONDENT_REQUEST, payload: {}, error });
    throw error;
  }
}
