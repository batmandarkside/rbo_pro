import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const defaultState = fromJS({
  list: []
});

export default function (state = defaultState, { type, payload }) {
  switch (type) {
    case ACTIONS.DAL_GET_CURTRANSFERS_SUCCESS:
      return state.set('list', fromJS(payload));
    default:
      return state;
  }
}
