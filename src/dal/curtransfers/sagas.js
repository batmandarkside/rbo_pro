import { call, select, put } from 'redux-saga/effects';
import { saveAs } from 'file-saver';
import Api from 'app/api';
import moment from 'moment-timezone';
import { ACTIONS } from './constants';
import { RBOControlsError } from '../utils';

export const selectedOrgsSelector = state => state.getIn(['profile', 'settings', 'selected_orgs']);

export function* fetchCurtransfers(action) {
  const { payload } = action;
  const selectedOrgs = yield select(selectedOrgsSelector);
  try {
    yield put({ type: ACTIONS.DAL_GET_CURTRANSFERS_REQUEST });
    const result =  yield call(Api.curtransfers.findAllByQuery, {
      ...payload,
      orgId: selectedOrgs.toJS()
    });
    const data = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_GET_CURTRANSFERS_SUCCESS, payload: data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_GET_CURTRANSFERS_FAIL });
    throw error;
  }
}

export function* fetchDefaultCurPayment() {
  const selectedOrgs = yield select(selectedOrgsSelector);
  const orgId = selectedOrgs && selectedOrgs.first();

  const result = yield call(Api.curtransfers.findDefaultCurTransferValue, { orgId });
  return result.data;
}

export function* fetchCurPayment(id) {
  const result = yield call(Api.curtransfers.findOneById, id);
  return result.data;
}

/**
 * Печать платежного поручения в формате pdf
 * @param {*} action
 */
export function* printPayment(action) {
  yield put({ type: ACTIONS.DAL_CURTRANSFER_PRINT_PAYMENT_REQUEST });
  try {
    const { payload } = action;
    if (payload.length === 1) {
      const { docId, docNumber, docDate } = payload[0];
      const result = yield call(Api.exports.createDocumentById, { docId, format: 'pdf' });
      yield put({ type: ACTIONS.DAL_CURTRANSFER_PRINT_PAYMENT_SUCCESS });
      saveAs(result.data, `PaymentOrder${docNumber && `_${docNumber}`}${docDate && `_${moment(docDate).format('DD-MM-YYYY')}`}.pdf`);
    } else {
      const result = yield call(Api.exports.createDocumentById, { docId: payload, format: 'pdf' });
      yield put({ type: ACTIONS.DAL_CURTRANSFER_PRINT_PAYMENT_SUCCESS });
      saveAs(result.data, 'PaymentOrders.pdf');
    }
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CURTRANSFER_PRINT_PAYMENT_FAIL, error });
    throw error;
  }
}


/**
 * Сохранение шаблона РПП
 * @param action
 *
 */
export function* savePaymentAsTemplate(action) {
  yield put({ type: ACTIONS.DAL_CURTRANSFER_SAVE_DOCUMENT_AS_TEMPLATE_REQUEST });
  const { payload } = action;
  try {
    const result = yield call(Api.curtransfers.saveAsTemplate, payload);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.DAL_CURTRANSFER_SAVE_DOCUMENT_AS_TEMPLATE_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения шаблона документа', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CURTRANSFER_SAVE_DOCUMENT_AS_TEMPLATE_FAIL, error });
    throw error;
  }
}

export function* getPaymentData(action) {
  yield put({ type: ACTIONS.DAL_GET_CURTRANSFERS_DATA_REQUEST });
  const { payload: { id } } = action;
  try {
    const result = yield call(Api.curtransfers.findOneById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_GET_CURTRANSFERS_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_GET_CURTRANSFERS_DATA_FAIL, error });
    throw error;
  }
}

export function* saveCurtransfers({ payload }) {
  try {
    yield put({ type: ACTIONS.DAL_CURTRANSFERS_SAVE_REQUEST });
    const result = yield call(Api.cPayment.save, payload);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.DAL_CURTRANSFERS_SAVE_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения валютного перевода', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CURTRANSFERS_SAVE_FAIL });
    throw error;
  }
}

export function* validate(data) {
  try {
    yield put({ type: ACTIONS.DAL_CURTRANSFERS_VALIDATE_REQUEST });
    const result = yield call(Api.cPayment.validate, data);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_CURTRANSFERS_VALIDATE_SUCCESS, payload: resultData });
    return resultData.errors || [];
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CURTRANSFERS_VALIDATE_FAIL });
    throw error;
  }
}

export function* fetchConstraints() {
  yield put({ type: ACTIONS.DAL_CURTRANSFERS_CONSTRAINTS_REQUEST });
  try {
    const result = yield call(Api.cPayment.fetchCPaymentConstraints);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_CURTRANSFERS_CONSTRAINTS_SUCCESS });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CURTRANSFERS_CONSTRAINTS_FAIL });
    throw error;
  }
}
