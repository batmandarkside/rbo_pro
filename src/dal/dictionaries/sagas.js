import { put, call, select } from 'redux-saga/effects';
import API from 'app/api';
import { selectedOrganizationsSelector } from '../utils';
import { ACTIONS } from './constants';

export function* getBIC(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_BIC_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findBIC, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_BIC_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_BIC_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCBC(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CBC_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findCBC, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CBC_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CBC_FAIL, payload: [], error });
    throw error;
  }
}

export function* getChargeTypes() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CHARGE_TYPES_REQUEST });
  try {
    const result = yield call(API.dictionary.findChargeTypes);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CHARGE_TYPES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CHARGE_TYPES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getClearingCodes(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CLEARING_CODES_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findClearingCodes, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CLEARING_CODES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CLEARING_CODES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCommissionTypes() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_COMMISSION_TYPES_REQUEST });
  try {
    const result = yield call(API.dictionary.findComissionTypes);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_COMMISSION_TYPES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_COMMISSION_TYPES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getContractTypes() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CONTRACT_TYPES_REQUEST });
  try {
    const result = yield call(API.dictionary.findContractTypes);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CONTRACT_TYPES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CONTRACT_TYPES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCountries(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_COUNTRIES_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findCountries, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_COUNTRIES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_COUNTRIES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCurrencyCodes(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_CODES_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findCurrCodes, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_CODES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_CODES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCurrencyOperationCodes(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_OPERATION_CODES_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findCurrencyOperationCodes, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_OPERATION_CODES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_OPERATION_CODES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCurrencyOperationTypes(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_OPERATION_TYPES_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findCurrencyOperationTypes, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_OPERATION_TYPES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CURRENCY_OPERATION_TYPES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getCustomerKpps() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CUSTOMER_KPPS_REQUEST });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  try {
    const result = yield call(API.dictionary.findCustomerKpps, selectedOrganizations && selectedOrganizations.toJS());
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CUSTOMER_KPPS_SUCCESS, resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_CUSTOMER_KPPS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getDealPassNums(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_DEAL_PASS_NUMS_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    orgId: firstSelectedOrganization,
    ...payload
  };
  try {
    const result = yield call(API.dictionary.findDealPassports, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_DEAL_PASS_NUMS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_DEAL_PASS_NUMS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getDrawerStatuses() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_DRAWER_STATUSES_REQUEST });
  try {
    const result = yield call(API.dictionary.findDrawerStatuses);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_DRAWER_STATUSES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_DRAWER_STATUSES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getForeignBanks(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_FOREIGN_BANKS_REQUEST, payload });
  try {
    const result = yield call(API.dictionary.findForeignBanks, payload);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_FOREIGN_BANKS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_FOREIGN_BANKS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getOfficials(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_OFFICIALS_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    orgId: firstSelectedOrganization,
    ...payload
  };
  try {
    const result = yield call(API.dictionary.findOfficials, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_OFFICIALS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_OFFICIALS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getPaymentKinds() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_KINDS_REQUEST });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  try {
    const result = yield call(API.dictionary.findPaymentKinds, selectedOrganizations && selectedOrganizations.toJS());
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_KINDS_SUCCESS, resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_KINDS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getPaymentPriorities() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_PRIORITIES_REQUEST });
  try {
    const result = yield call(API.dictionary.findPaymentPriorities);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_PRIORITIES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_PRIORITIES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getPaymentPurposes(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_PURPOSES_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    orgId: firstSelectedOrganization,
    ...payload
  };
  try {
    const result = yield call(API.dictionary.findPaymentPurposes, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_PURPOSES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAYMENT_PURPOSES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getPayReasons() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAY_REASONS_REQUEST });
  try {
    const result = yield call(API.dictionary.findPayReasons);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAY_REASONS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_PAY_REASONS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getReceiverTopics() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_RECEIVER_TOPICS_REQUEST });
  try {
    const result = yield call(API.dictionary.findReceiverTopics);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_RECEIVER_TOPICS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_RECEIVER_TOPICS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getResidentAttributes() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_RESIDENT_ATTRIBUTES_REQUEST });
  try {
    const result = yield call(API.dictionary.findResidentAttributes);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_RESIDENT_ATTRIBUTES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_RESIDENT_ATTRIBUTES_FAIL, payload: [], error });
    throw error;
  }
}

export function* getTaxPeriods() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_TAX_PERIODS_REQUEST });
  try {
    const result = yield call(API.dictionary.findTaxPeriods);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_TAX_PERIODS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_TAX_PERIODS_FAIL, payload: [], error });
    throw error;
  }
}

export function* getVatCalculationRules() {
  yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_VAT_CALCULATION_RULES_REQUEST });
  try {
    const result = yield call(API.dictionary.findVatCalculationRules);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_VAT_CALCULATION_RULES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DICTIONARIES_GET_VAT_CALCULATION_RULES_FAIL, payload: [], error });
    throw error;
  }
}

