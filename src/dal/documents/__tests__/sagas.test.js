import { call, put } from 'redux-saga/effects';
import { addOrdinaryNotificationAction }  from 'dal/notification/actions';
import API from 'app/api';

import {
  documentSendToBankSaga
} from '../sagas';

import {
  sendDocumentToBankAction
} from './../actions';

import { ACTIONS } from '../constants';

describe('Test Documents Sagas', () => {
  describe('documentSendToBankSaga', () => {
    it('success', () => {
      const generator = documentSendToBankSaga(sendDocumentToBankAction('123'));
      expect(
        generator.next({ docId: '123' }).value
      ).toEqual(
        call(API.document.sendDocumentToBank, {
          docId: '123'
        })
      );

      expect(
        generator.next({}).value
      ).toEqual(
        put({ type: ACTIONS.DOCUMENT_SEND_SIGN_SUCCESS, payload: {} })
      );


      expect(
        generator.next().value
      ).toEqual(
        put(addOrdinaryNotificationAction({
          type   : 'success',
          title  : '',
          message: 'Документ успешно отправлен'
        }))
      );
    });

    it('failure', () => {
      const generator = documentSendToBankSaga(sendDocumentToBankAction('123'));
      generator.next();
      expect(
        generator.throw('Test error message').value
      ).toEqual(
        put({
          type: ACTIONS.DOCUMENT_SEND_SIGN_FAIL,
          error: 'Test error message'
        })
      );
    });
  });
});
