import { ACTIONS } from './constants';

/**
 *
 * @param {string} docId
 */
export const sendDocumentToBankAction = docId => ({
  type: ACTIONS.DOCUMENT_SEND_SIGN_REQUEST,
  payload: {
    docId
  }
});
