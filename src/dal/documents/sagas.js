import { put, call, takeLatest } from 'redux-saga/effects';
import { saveAs } from 'file-saver';
import moment from 'moment-timezone';
import API from 'app/api';
import { addOrdinaryNotificationAction } from 'dal/notification/actions';
import { prepareConfForNotification } from 'utils/errors-util';
import { RBOError } from '../utils';
import { ACTIONS } from './constants';

export function* print(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DOCUMENTS_PRINT_REQUEST, payload });
  const { docTitle, docId, docNumber, docDate } = payload;
  const docIds = Array.isArray(docId) ? docId : [docId];
  try {
    const result = yield call(API.exports.createDocumentById, { docId: docIds, format: 'pdf' });
    yield put({ type: ACTIONS.DAL_DOCUMENTS_PRINT_SUCCESS });
    saveAs(
      result.data,
      `${docTitle}${docNumber ? `_${docNumber}` : ''}${docDate ? `_${moment(docDate).format('DD-MM-YYYY')}` : ''}.pdf`
    );
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DOCUMENTS_PRINT_FAIL, error });
    throw error;
  }
}

export function* remove(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DOCUMENTS_REMOVE_REQUEST, payload });
  const { docId } = payload;
  const docIds = Array.isArray(docId) ? docId : [docId];
  try {
    yield call(API.document.deleteById, docIds);
    yield put({ type: ACTIONS.DAL_DOCUMENTS_REMOVE_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DOCUMENTS_REMOVE_FAIL });
    throw error;
  }
}

export function* archive(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DOCUMENTS_ARCHIVE_REQUEST, payload });
  const { docId } = payload;
  const docIds = Array.isArray(docId) ? docId : [docId];
  try {
    yield call(API.document.toArchiveByIds, docIds);
    yield put({ type: ACTIONS.DAL_DOCUMENTS_ARCHIVE_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DOCUMENTS_ARCHIVE_FAIL, error });
    throw error;
  }
}

export function* unarchive(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DOCUMENTS_UNARCHIVE_REQUEST, payload });
  const { docId } = payload;
  const docIds = Array.isArray(docId) ? docId : [docId];
  try {
    yield call(API.document.deleteArchiveByIds, docIds);
    yield put({ type: ACTIONS.DAL_DOCUMENTS_UNARCHIVE_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_DOCUMENTS_UNARCHIVE_FAIL, error });
    throw error;
  }
}

export function* sendToBank(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_DOCUMENTS_SEND_TO_BANK_REQUEST, payload });
  const { docId } = payload;
  const docIds = Array.isArray(docId) ? docId : [docId];
  try {
    const result = yield call(API.document.sendDocumentToBank, { docId: docIds });
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_DOCUMENTS_SEND_TO_BANK_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    const rboError = new RBOError('Ошибка.', error);
    yield put({ type: ACTIONS.DAL_DOCUMENTS_SEND_TO_BANK_FAIL, payload: [], error: rboError });
    throw rboError;
  }
}

export function* stateHistoryById(payload) {
  yield put({
    type: ACTIONS.DAL_DOCUMENTS_GET_STATE_HISTORY_REQUEST
  });

  try {
    const result = yield call(API.document.stateHistoryById, payload);

    yield put({
      type: ACTIONS.DAL_DOCUMENTS_GET_STATE_HISTORY_SUCCESS
    });

    return result.data;
  } catch (error) {
    yield put({
      type: ACTIONS.DAL_DOCUMENTS_GET_STATE_HISTORY_FAIL
    });

    throw error;
  }
}

export function* signsCheckById(payload) {
  yield put({
    type: ACTIONS.DAL_DOCUMENTS_SIGN_CHECK_REQUEST
  });

  try {
    const result = yield call(API.document.signsCheckById, payload);

    yield put({
      type: ACTIONS.DAL_DOCUMENTS_SIGN_CHECK_SUCCESS
    });

    return result.data[0].signs;
  } catch (error) {
    yield put({
      type: ACTIONS.DAL_DOCUMENTS_SIGN_CHECK_FAIL
    });

    throw error;
  }
}

export function* documentSendToBankSaga(action) {
  const { payload: { docId } } = action;

  try {
    const result = yield call(API.document.sendDocumentToBank, { docId });
    const resultData = (result && result.data) || {};

    yield put({
      type   : ACTIONS.DOCUMENT_SEND_SIGN_SUCCESS,
      payload: resultData
    });

    yield put(addOrdinaryNotificationAction({
      type   : 'success',
      title  : '',
      message: 'Документ успешно отправлен'
    }));

    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DOCUMENT_SEND_SIGN_FAIL, error });
    yield put(addOrdinaryNotificationAction(prepareConfForNotification(error)));
    throw error;
  }
}

export default function* documentsSaga() {
  yield takeLatest(ACTIONS.DOCUMENT_SEND_SIGN_REQUEST, documentSendToBankSaga);
}
