import qs from 'qs';
import { fromJS } from 'immutable';
import { combineReducers }  from 'redux-immutable';
import { reducer as form } from 'redux-form/immutable';
import { history } from 'app/app-history';
import { LOCATION_CHANGE, routerReducer } from 'react-router-redux';
import rootReducer   from './root/reducer';
import confirm from './confirm/reducer';
import notifications from './notification/reducer';
import profile from './profile/reducer';
import organizations from './organizations/reducer';
import curtransfers from './curtransfers/reducer';
import beneficiaries from './beneficiaries/reducer';
import messages from './messages/reducer';
import sign from './sign/reducer';
import template from './template/reducer';

const initialState = fromJS({
  locationBeforeTransitions: null
});

/**
 * This reducer will update the state with the most recent location history
 * has transitioned to. This may not be in sync with the router, particularly
 * if you have asynchronously-loaded routes, so reading from and relying on
 * this state is discouraged.
 */
export function routing(state = initialState, { type, payload } = {}) {
  if (type === LOCATION_CHANGE) {
    const locationBeforeTransitions = state.get('locationBeforeTransitions');
    let prevLocation;
    if (history.action !== 'REPLACE') {
      prevLocation = locationBeforeTransitions
        ? {
          pathname: locationBeforeTransitions.get('pathname'),
          query: qs.parse(locationBeforeTransitions.get('search')),
          search: locationBeforeTransitions.get('search')
        }
        : null;
    } else {
      prevLocation = locationBeforeTransitions
        ? locationBeforeTransitions.get('prevLocation')
        : null;
    }

    return routerReducer(
      state.merge({
        locationBeforeTransitions: {
          ...payload,
          action: history.action,
          currentLocation: payload.pathname,
          prevLocation
        }
      })
    );
  }
  return routerReducer(state);
}

/**
 *
 * @param asyncReducers
 * @returns {Function}
 */
export default function createReducer(injectedReducers) {
  return combineReducers({
    form,
    rootReducer,
    routing,
    confirm,
    notifications,
    profile,
    organizations,
    messages,
    sign,
    template,
    beneficiaries,
    curtransfers,
    ...injectedReducers,
  });
}
