/* eslint-disable no-unused-vars */
import chai, { expect }           from 'chai';
import chaiImmutable              from 'chai-immutable';
import messages                   from '../reducer';
import { ACTIONS }                from '../constants';

import {
  getMessagesAction,
  getSingleMessageAction,
  getImportantMessagesAction,
  attachmentsAction,
  messagesFromBankAttachmentsInfoAction,
  exportMessagesFromBankAction
} from '../actions';

chai.use(chaiImmutable);

describe('Messages tests', () => {
  const defaultState = messages(undefined, { type: null });

  test('actions test ', () => {
    expect(getMessagesAction).to.be.function;
    expect(attachmentsAction).to.be.function;
    expect(getSingleMessageAction).to.be.function;
    expect(getImportantMessagesAction).to.be.function;
    expect(messagesFromBankAttachmentsInfoAction).to.be.function;
    expect(exportMessagesFromBankAction).to.be.function;

    const getMessages = getMessagesAction({ test: 123 });
    const getSingleMessage = getSingleMessageAction({ test: 123 });
    const getImportantMessages = getImportantMessagesAction({ test: 123 });
    const attachments = attachmentsAction({ id:123 });
    const messagesFromBank = messagesFromBankAttachmentsInfoAction('123');
    const exportMessages = exportMessagesFromBankAction({ accNum: 1, currIsoCode: 2 });

    expect(getMessages)
      .to.have.deep.property('type')
      .to.be.equal(ACTIONS.MESSAGES_LOAD_REQUEST);
    expect(getMessages)
      .to.have.deep.property('payload')
      .to.be.equal({ test: 123 });

    expect(getSingleMessage)
      .to.have.deep.property('type')
      .to.be.equal(ACTIONS.MESSAGES_SINGLE_INCOMING_LOAD_REQUEST);
    expect(getSingleMessage)
      .to.have.deep.property('payload')
      .to.be.equal({ test: 123 });

    expect(getImportantMessages)
      .to.have.deep.property('type')
      .to.be.equal(ACTIONS.MESSAGES_IMPORTANT_LOAD_REQUEST);
    expect(getImportantMessages)
      .to.have.deep.property('payload')
      .to.be.equal({ test: 123 });

    expect(attachments)
      .to.have.deep.property('type')
      .to.be.equal(ACTIONS.MESSAGES_ATTACHMENTS_REQUEST);
    expect(attachments)
      .to.have.deep.property('payload')
      .to.be.equal({ id: 123 });

    expect(messagesFromBank)
      .to.have.deep.property('type')
      .to.be.equal(ACTIONS.MESSAGES_ATTACHMENTS_FROM_BANK_REQUEST);
    expect(messagesFromBank)
      .to.have.deep.property('payload')
      .to.be.equal({ docId: '123' });

    expect(exportMessages)
      .to.have.deep.property('type')
      .to.be.equal(ACTIONS.MESSAGES_EXPORT_FROM_BANK_REQUEST);
    expect(exportMessages)
      .to.have.deep.property('payload')
      .to.be.equal({ accNum: 1, currIsoCode: 2 });
  });
});
