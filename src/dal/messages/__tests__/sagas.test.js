import { call, put, select } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import API from 'app/api';
import fileSaver from 'file-saver';
import {
  addOrdinaryNotificationAction,
  addImportantMessagesAction }                 from 'dal/notification/actions';
import { ACTIONS as ACTIONS_NOTIFICATIONS  }   from 'dal/notification/constants';

import {
  organizationsSelector,
  messagesFiltersSelector,
  selectedOrganizationsSelector,
  fetchMessages,
  fetchSingleMessage,
  fetchImportantMessages,
  fetchMessagesFromBankAttachmentsInfo,
  fetchAttachments,
  exportIncomingMessagesListToExcel,
  getOutgoingMessages
} from './../sagas';

import {
  getMessagesAction,
  getSingleMessageAction,
  attachmentsAction,
  messagesFromBankAttachmentsInfoAction
} from './../actions';

import { ACTIONS } from './../constants';

describe('Test Messages Sagas', () => {
  const state = fromJS({
    profile: {
      settings: {
        selected_orgs: ['22810'],
        messagesFilters: [
          {
            id      : 0,
            name    : 'filters',
            label   : 'Все',
            selected: true,
            sort    : 'read',
            desc    : false,
            value   : 'all'
          },
          {
            id      : 1,
            name    : 'filters',
            label   : 'Непрочитанные',
            isRead  : 0,
            sort    : 'date',
            desc    : false,
            selected: false,
            value   : 'unread'
          }
        ]
      }
    },
    organizations: {
      orgs: [
        {
          id: 112
        }
      ]
    }
  });

  const messagesFilters = messagesFiltersSelector(state);

  // const organizations = organizationsSelector(state);

  describe('fetchMessages', () => {
    it('success', () => {
      const generator = fetchMessages(getMessagesAction({ id: 112 }));
      generator.next();

      expect(
        generator.next(messagesFilters).value
      ).toEqual(
        call(API.incomingMessage.findAllByQuery, {
          desc: false,
          sort: 'read',
          isRead: undefined,
          offsetStep: 10,
          id: 112
        })
      );

      const result = {
        data : [
          {
            mustRead: '1',
            read: '0'
          }
        ]
      };

      expect(
        generator.next(result).value
      ).toEqual(
        put({ type: ACTIONS.MESSAGES_LOAD_SUCCESS, payload: result.data })
      );
    });

    it('failure', () => {
      const generator = fetchMessages(getMessagesAction({ id: 112 }));
      generator.next();

      expect(
        generator.next(messagesFilters).value
      ).toEqual(
        call(API.incomingMessage.findAllByQuery, {
          desc: false,
          sort: 'read',
          isRead: undefined,
          offsetStep: 10,
          id: 112
        })
      );

      expect(
        generator.throw('Test error message').value
      ).toEqual(
        put({
          type: ACTIONS.MESSAGES_LOAD_FAIL,
          error: 'Test error message'
        })
      );

      expect(
        generator.next(addOrdinaryNotificationAction({
          type    : 'error',
          title   : 'Ошибка!',
          message: 'Ошибка сервера',
        })).value
      ).toEqual(
        put({
          type: ACTIONS_NOTIFICATIONS.NOTIFICATION_ADD_ORDINARY_NOTIFICATION,
          notification: {
            type    : 'error',
            title   : 'Ошибка!',
            message: 'Ошибка сервера',
          }
        })
      );
    });
  });

  describe('fetchImportantMessages', () => {
    it.skip('success', () => {
      const generator = fetchImportantMessages();
      generator.next(organizationsSelector(state));
      const organizationsIds = ''; // organizations.map(org => org.id);

      expect(
        generator.next(organizationsIds).value
      ).toEqual(
        call(API.incomingMessage.findAllImportant, {
          orgId: organizationsIds.toJS()
        })
      );

      const result = {
        data : [
          {
            mustRead: '1',
            read: '0'
          }
        ]
      };

      expect(
        generator.next(result).value
      ).toEqual(
        put({ type: ACTIONS.MESSAGES_IMPORTANT_LOAD_SUCCESS, payload: result.data })
      );

      expect(
        generator.next(addImportantMessagesAction({
          type    : 'important_message',
          messages: result.data,
        })).value
      ).toEqual(
        put({
          type: ACTIONS_NOTIFICATIONS.NOTIFICATION_ADD_IMPORTANT_MESSAGES,
          notification: {
            type    : 'important_message',
            messages: result.data,
          }
        })
      );
    });
    it.skip('failure', () => {
      const generator = fetchImportantMessages();
      generator.next();
      const organizationsIds = ''; // organizations.map(org => org.get('id'));

      expect(
        generator.next(organizationsIds).value
      ).toEqual(
        call(API.incomingMessage.findAllImportant, {
          orgId: organizationsIds.toJS()
        })
      );

      expect(
        generator.throw('Test error message').value
      ).toEqual(
        put({
          type: ACTIONS.MESSAGES_IMPORTANT_LOAD_FAIL,
          error: 'Test error message'
        })
      );

      expect(
        generator.next(addOrdinaryNotificationAction({
          type    : 'error',
          title   : 'Ошибка!',
          message: 'Ошибка сервера',
        })).value
      ).toEqual(
        put({
          type: ACTIONS_NOTIFICATIONS.NOTIFICATION_ADD_ORDINARY_NOTIFICATION,
          notification: {
            type    : 'error',
            title   : 'Ошибка!',
            message: 'Ошибка сервера',
          }
        })
      );
    });
  });

  describe('fetchSingleMessage', () => {
    it('success', () => {
      const generator = fetchSingleMessage(getSingleMessageAction({ docId: 112 }));

      expect(
        generator.next().value
      ).toEqual(
        call(API.incomingMessage.findById, 112)
      );

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.MESSAGES_SINGLE_INCOMING_LOAD_SUCCESS })
      );
    });

    it('failure', () => {
      const generator = fetchSingleMessage(getSingleMessageAction({ docId: 112 }));

      expect(
        generator.next().value
      ).toEqual(
        call(API.incomingMessage.findById, 112)
      );
      expect(
        generator.throw('Test error message').value
      ).toEqual(
        put({
          type: ACTIONS.MESSAGES_SINGLE_INCOMING_LOAD_FAIL,
          error: 'Test error message'
        })
      );
    });
  });


  describe('fetchMessagesFromBankAttachmentsInfo', () => {
    it('success', () => {
      const generator = fetchMessagesFromBankAttachmentsInfo(messagesFromBankAttachmentsInfoAction(123));
      expect(
        generator.next().value
      ).toEqual(
        call(API.incomingMessage.findAttachmentsInfoById, 123)
      );

      const result = {
        data : [{
          id: 'test_id'
        }]
      };
      expect(
        generator.next(result).value
      ).toEqual(
        put({ type: ACTIONS.MESSAGES_ATTACHMENTS_REQUEST, payload: { id: 'test_id' } })
      );

      expect(
        generator.next().value
      ).toEqual(
        put({ type: ACTIONS.MESSAGES_ATTACHMENTS_FROM_BANK_SUCCESS, payload: result.data[0] })
      );
    });

    it('failure', () => {
      const generator = fetchMessagesFromBankAttachmentsInfo(messagesFromBankAttachmentsInfoAction(123));
      expect(
        generator.next().value
      ).toEqual(
        call(API.incomingMessage.findAttachmentsInfoById, 123)
      );
      expect(
        generator.throw('Test error message').value
      ).toEqual(
        put({
          type: ACTIONS.MESSAGES_ATTACHMENTS_FROM_BANK_FAIL,
          error: 'Test error message'
        })
      );

      expect(
        generator.next(addOrdinaryNotificationAction({
          type    : 'error',
          title   : 'Ошибка!',
          message: 'Ошибка сервера',
        })).value
      ).toEqual(
        put({
          type: ACTIONS_NOTIFICATIONS.NOTIFICATION_ADD_ORDINARY_NOTIFICATION,
          notification: {
            type    : 'error',
            title   : 'Ошибка!',
            message: 'Ошибка сервера',
          }
        })
      );
    });
  });

  describe('fetchAttachments', () => {
    it('success', () => {
      const generator = fetchAttachments(attachmentsAction({
        id: 123,
        name: 'test.txt',
        mimeType: 'plain/text'
      }));

      expect(
        generator.next().value
      ).toEqual(
        call(API.incomingMessage.findAttachmentsById, 123)
      );

      expect(
        generator.next({ data: 'test' }).value
      ).toEqual(
        put({ type: ACTIONS.MESSAGES_ATTACHMENTS_SUCCESS, payload: true })
      );
    });

    it('failure', () => {
      const generator = fetchAttachments(attachmentsAction({
        id: 123,
        name: 'test.txt',
        mimeType: 'plain/text'
      }));

      expect(
        generator.next().value
      ).toEqual(
        call(API.incomingMessage.findAttachmentsById, 123)
      );
      expect(
        generator.throw('Test error message').value
      ).toEqual(
        put({
          type: ACTIONS.MESSAGES_ATTACHMENTS_FAIL,
          error: 'Test error message'
        })
      );
    });
  });

  describe('exportIncomingMessagesListToExcel', () => {
    it('success', () => {
      const generator = exportIncomingMessagesListToExcel({
        payload: { sort: 'date', desc: true }
      });
      fileSaver.saveAs = jest.fn();

      expect(
        generator.next().value
      ).toEqual(
        select(selectedOrganizationsSelector)
      );

      expect(
        generator.next(selectedOrganizationsSelector(state)).value
      ).toEqual(
        call(API.exports.createExportIncomingMessagesList, {
          orgId : ['22810'],
          sort: 'date',
          desc: true
        })
      );
    });
  });

  describe('getOutgoingMessages', () => {
    it('success', () => {
      const generator = getOutgoingMessages({
        payload: { offset: 0, offsetStep: 31 }
      });

      expect(
        generator.next().value
      ).toEqual(
        select(selectedOrganizationsSelector)
      );

      expect(
        generator.next(selectedOrganizationsSelector(state)).value
      ).toEqual(
        call(API.outgoingMessage.findAllByQuery, {
          orgId : ['22810'],
          offset: 0,
          offsetStep: 31
        })
      );
    });
  });
});
