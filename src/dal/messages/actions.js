import { ACTIONS }                  from './constants';

export const getMessagesAction = params => ({
  type: ACTIONS.MESSAGES_LOAD_REQUEST,
  payload: params
});

export const getSingleMessageAction = params => ({
  type: ACTIONS.MESSAGES_SINGLE_INCOMING_LOAD_REQUEST,
  payload: params
});

export const getImportantMessagesAction = params => ({
  type: ACTIONS.MESSAGES_IMPORTANT_LOAD_REQUEST,
  payload: params
});

/**
 *
 * @param {Object} data
 * @param {string} data.id - индентификатор вложения
 * @param {string} data.name - имя вложения с расширением
 * @param {string} data.mimeType - mime type вложения
 */
export const attachmentsAction = data => ({
  type: ACTIONS.MESSAGES_ATTACHMENTS_REQUEST,
  payload: { ...data }
});

/**
 *
 * @param {string} docId
 */
export const messagesFromBankAttachmentsInfoAction = docId => ({
  type: ACTIONS.MESSAGES_ATTACHMENTS_FROM_BANK_REQUEST,
  payload: { docId }
});


export const exportMessagesFromBankAction = params => ({
  type: ACTIONS.MESSAGES_EXPORT_FROM_BANK_REQUEST,
  payload: params
});

export const getMessageDefaultValue = payload => ({
  type: ACTIONS.MESSAGES_MESSAGE_DEFAULT_VALUE_REQUEST,
  payload
});
