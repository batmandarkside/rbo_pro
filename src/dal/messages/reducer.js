import { fromJS } from 'immutable';
import { ACTIONS } from './constants';


export const defaultState = fromJS({
  list: [],
  messageDefaultValue: {}
});

const messages = (state = defaultState, action) => {
  switch (action.type) {
    case ACTIONS.MESSAGES_LOAD_SUCCESS:
      return state.set('list', fromJS(action.payload));
    default:
      return state;
  }
};

export default messages;
