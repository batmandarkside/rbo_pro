import {
  put,
  call,
  takeLatest,
  takeEvery,
  select,
  all
} from 'redux-saga/effects';
import moment from 'moment-timezone';
import { blobToBase64 } from 'utils';
import {
  addOrdinaryNotificationAction,
  addImportantMessagesAction
}    from 'dal/notification/actions';
import Api                        from 'app/api';
import { saveAs }                 from 'file-saver';
import { RBOControlsError } from '../utils';
import { attachmentsAction }      from './actions';
import { ACTIONS }  from './constants';

export const messagesFiltersSelector = state => state.getIn(['profile', 'settings', 'messagesFilters']);
export const organizationsSelector = state => state.getIn(['organizations', 'orgs']);
export const selectedOrganizationsSelector = state => state.getIn(['profile', 'settings', 'selected_orgs']);

export const getFilterData = messagesFilters => (
  messagesFilters.find(f => f.get('selected')).toJS()
);

/**
 *
 * @param messages
 */
export const filterUnreadMessages = messages => (
  messages.filter(message =>
    message.read === '0'
  )
);

export function* fetchMessages(action) {
  const { payload } = action;
  const messagesFilters = yield select(messagesFiltersSelector);
  const { desc, sort, isRead } = getFilterData(messagesFilters);

  try {
    let result = yield call(Api.incomingMessage.findAllByQuery, {
      desc,
      sort,
      isRead,
      offsetStep: 10,
      ...payload
    });
    result = result && result.data;
    yield put({ type: ACTIONS.MESSAGES_LOAD_SUCCESS, payload: result });
  } catch (e) {
    yield put({ type: ACTIONS.MESSAGES_LOAD_FAIL, error: e });
    yield put(addOrdinaryNotificationAction({
      type: 'error',
      title: 'Ошибка!',
      message: e && e.error ? e.error : 'Ошибка сервера'
    }));
  }
}

export function* fetchSingleMessage(action) {
  const { payload } = action;

  try {
    let result = yield call(Api.incomingMessage.findById, payload.docId);
    result = result && result.data;

    yield put({ type: ACTIONS.MESSAGES_SINGLE_INCOMING_LOAD_SUCCESS, payload: result });
    return result;
  } catch (error) {
    yield put({
      type: ACTIONS.MESSAGES_SINGLE_INCOMING_LOAD_FAIL,
      error
    });

    throw error;
  }
}

/**
 * @todo если понадобится нужно предусмотреть выход из цикла
 * опрашиваем сервер на новые сообщения каждые 5 мин
 */
export function* fetchImportantMessages() {
  const organizations = yield select(organizationsSelector);
  const organizationsIds = organizations.map(org => org.get('id'));

  if (!organizationsIds) {
    throw new Error('no organizationsIds');
  }

  try {
    let result = yield call(Api.incomingMessage.findAllImportant, {
      orgId: organizationsIds.toJS()
    });
    result = result && result.data;

    yield put({ type: ACTIONS.MESSAGES_IMPORTANT_LOAD_SUCCESS, payload: result });

    const unreadImportantMessages = filterUnreadMessages(result);
    if (unreadImportantMessages && unreadImportantMessages.length) {
      let messages = unreadImportantMessages.map((message) => {
        const manyReceivers = message.receivers.length > 1;
        const targetOrg = organizations.find(org => org.get('id') === message.receivers[0]);
        const targetOrgName = targetOrg.get('shortName', '');

        return {
          ...message,
          receiverName: manyReceivers
            ? 'Несколько получателей'
            : targetOrgName.toUpperCase()
        };
      });

      // собираем информацию о вложениях
      const [...messagesInfo] = yield all(messages.map((message) => {
        const attachExists = parseInt(message.attachExists, 10);
        const docId = message.docId;

        return attachExists ? fetchMessagesAttachmentsInfo({ payload: { docId } }) : [];
      }));

      messages = messages.map((message, index) => ({
        ...message,
        attachInfo: messagesInfo[index]
      }));

      yield put(addImportantMessagesAction({
        type: 'important_message',
        messages
      }));
    }
  } catch (e) {
    yield put({ type: ACTIONS.MESSAGES_IMPORTANT_LOAD_FAIL, error: e });
    yield put(addOrdinaryNotificationAction({
      type: 'error',
      title: 'Ошибка!',
      message: e && e.error ? e.error : 'Ошибка сервера'
    }));
  }
}

export function* fetchMessagesAttachmentsInfo(action) {
  const { payload: { docId } } = action;
  try {
    const result = yield call(Api.incomingMessage.findAttachmentsInfoById, docId);
    return (result && result.data) || [];
  } catch (error) {
    throw error;
  }
}

export function* fetchMessagesFromBankAttachmentsInfo(action) {
  const { payload: { docId } } = action;
  try {
    const result = yield call(Api.incomingMessage.findAttachmentsInfoById, docId);

    if (result && result.data && result.data[0] && result.data[0].id) {
      yield put(attachmentsAction(result.data[0]));
      yield put({ type: ACTIONS.MESSAGES_ATTACHMENTS_FROM_BANK_SUCCESS, payload: result.data[0] });
    }

    return result;
  } catch (e) {
    yield put({ type: ACTIONS.MESSAGES_ATTACHMENTS_FROM_BANK_FAIL, error: e });
    yield put(addOrdinaryNotificationAction({
      type: 'error',
      title: 'Ошибка!',
      message: e && e.error ? e.error : 'Ошибка сервера'
    }));
    throw e;
  }
}

export function* fetchAttachments(action) {
  const { payload: { id, name } } = action;
  try {
    const result = yield call(Api.incomingMessage.findAttachmentsById, id);
    yield put({
      type: ACTIONS.MESSAGES_ATTACHMENTS_SUCCESS,
      payload: !!result
    });
    saveAs(result.data, name);
  } catch (e) {
    yield put({ type: ACTIONS.MESSAGES_ATTACHMENTS_FAIL, error: e });
  }
}

export function* exportIncomingMessagesListToExcel(action) {
  const { payload } = action;
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  try {
    const result = yield call(Api.exports.createExportIncomingMessagesList, {
      ...payload,
      orgId: selectedOrganizations.toJS()
    });
    saveAs(
      result.data,
      `MessageFromBank_${moment().format('DD-MM-YYYY_HH-mm-ss')}.xlsx`
    );
    return true;
  } catch (error) {
    throw error;
  }
}

export function* getOutgoingMessages(action) {
  const { payload } = action;
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  try {
    const result = yield call(Api.outgoingMessage.findAllByQuery, {
      ...payload,
      orgId: selectedOrganizations.toJS()
    });
    return (result && result.data) || [];
  } catch (error) {
    throw error;
  }
}

export function* exportOutgoingMessagesListToExcel(action) {
  const { payload } = action;
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  try {
    const result = yield call(Api.exports.createExportOutgoingMessagesList, {
      ...payload,
      orgId: selectedOrganizations.toJS()
    });
    saveAs(
      result.data,
      `MessageToBank_${payload.section ? payload.section : 'working'}_${moment().format('DD-MM-YYYY_HH-mm-ss')}.xlsx`
    );
    return true;
  } catch (error) {
    throw error;
  }
}

export function* archiveMessages(payload) {
  try {
    return yield call(Api.document.toArchiveByIds, payload);
  } catch (error) {
    throw error;
  }
}

export function* unarchiveMessages(payload) {
  try {
    return yield call(Api.document.deleteArchiveByIds, payload);
  } catch (error) {
    throw error;
  }
}

export function* removeMessages(payload) {
  try {
    return yield call(Api.document.deleteById, payload);
  } catch (error) {
    throw error;
  }
}

export function* fetchMessageDefaultValue(orgId) {
  try {
    yield put({
      type: ACTIONS.MESSAGES_MESSAGE_DEFAULT_VALUE_REQUEST
    });

    const result = yield call(Api.outgoingMessage.findDefaultValuesByOrganisationId, orgId);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_DEFAULT_VALUE_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_DEFAULT_VALUE_FAIL });
    throw error;
  }
}

export function* fetchMessageById(id) {
  try {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_BY_ID_REQUEST });

    const result = yield call(Api.outgoingMessage.findOneById, id);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_BY_ID_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_BY_ID_FAIL });
    throw error;
  }
}

export function* fetchMessageRepeat(id) {
  try {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_REPEAT_REQUEST });

    const result = yield call(Api.outgoingMessage.createCopyByMessageId, id);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_REPEAT_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_REPEAT_FAIL });
    throw error;
  }
}

export function* archivedMessage(id) {
  try {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_TO_ARCHIVE_REQUEST });

    const result = yield call(Api.document.toArchiveByIds, [id]);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_TO_ARCHIVE_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_TO_ARCHIVE_FAIL });
    throw error;
  }
}

export function* fromArchiveMessage(id) {
  try {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_FROM_ARCHIVE_REQUEST });

    const result = yield call(Api.document.deleteArchiveByIds, [id]);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_FROM_ARCHIVE_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_FROM_ARCHIVE_FAIL });
    throw error;
  }
}

export function* deleteMessage(id) {
  try {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_DELETE_REQUEST });

    const result = yield call(Api.document.deleteById, [id]);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_DELETE_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_DELETE_FAIL });
    throw error;
  }
}

export function* saveMessage({ payload }) {
  try {
    const result = yield call(Api.outgoingMessage.update, payload);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_SAVE_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_SAVE_FAIL });
    throw error;
  }
}

export function* fetchAttachmetnsForMessageInBank(docId) {
  try {
    yield put({ type: ACTIONS.MESSAGES_IN_BANK_ATTACHMENTS_REQUEST });

    const result = yield call(Api.attachmentInfo.findByAttachmentId, docId);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_IN_BANK_ATTACHMENTS_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_IN_BANK_ATTACHMENTS_FAIL });
    
    return [];
  }
}

export function* sendMessageInBank(docId) {
  try {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_SEND_REQUEST });

    const result = yield call(Api.outgoingMessage.sendMessageWithId, docId);
    const data = (result && result.data) || {};

    yield put({ type: ACTIONS.MESSAGES_MESSAGE_SEND_SUCCESS, data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_MESSAGE_SEND_FAIL });

    throw error;
  }
}

export function* printMessageInBank(docId) {
  yield put({
    type: ACTIONS.MESSAGES_MESSAGE_PRINT_REQUEST
  });

  try {
    const result = yield call(
      Api.exports.createDocumentById,
      { docId, format: 'pdf' }
    );

    saveAs(
      result.data,
      `MessageFromBank_${moment().format('DD-MM-YYYY_HH-mm-ss')}.xlsx`
    );

    yield put({
      type: ACTIONS.MESSAGES_MESSAGE_PRINT_SUCCESS
    });
  } catch (error) {
    yield put({
      type: ACTIONS.MESSAGES_MESSAGE_PRINT_SUCCESS
    });

    throw error;
  }
}


export function* getIncomingMessages(action) {
  yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGES_REQUEST });
  const { payload } = action;
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  try {
    const result = yield call(Api.incomingMessage.findAllByQuery, {
      ...payload,
      orgId: selectedOrganizations && selectedOrganizations.toJS()
    });
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGES_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGES_FAIL, error });
    throw error;
  }
}

export function* getIncomingMessageData(action) {
  yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGE_DATA_REQUEST });
  const { payload: { id } } = action;
  try {
    const result = yield call(Api.incomingMessage.findById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGE_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGE_DATA_FAIL, error });
    throw error;
  }
}

export function* getIncomingMessageAttachments(action) {
  yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGE_ATTACHMENTS_REQUEST });
  const { payload: { id } } = action;
  try {
    const result = yield call(Api.incomingMessage.findAttachmentsInfoById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGE_ATTACHMENTS_SUCCESS, resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_INCOMING_MESSAGE_ATTACHMENTS_FAIL, error });
    return error;
  }
}

export function* downloadIncomingMessageAttachment(action) {
  yield put({ type: ACTIONS.MESSAGES_DOWNLOAD_INCOMING_MESSAGE_ATTACHMENT_REQUEST });
  const { payload: { fileId, fileName } } = action;
  try {
    const result = yield call(Api.incomingMessage.findAttachmentsById, fileId);
    yield put({ type: ACTIONS.MESSAGES_DOWNLOAD_INCOMING_MESSAGE_ATTACHMENT_SUCCESS });
    saveAs(result.data, fileName);
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_DOWNLOAD_INCOMING_MESSAGE_ATTACHMENT_FAIL, error });
    throw error;
  }
}

export function* getOutgoingMessageData(action) {
  yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_DATA_REQUEST });
  const { payload: { id } } = action;
  try {
    const result = yield call(Api.outgoingMessage.findOneById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_DATA_FAIL, error });
    throw error;
  }
}

export function* getOutgoingMessageDefaultData() {
  yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_DEFAULT_DATA_REQUEST });
  const selectedOrgs = yield select(selectedOrganizationsSelector);
  const firstSelectedOrg = selectedOrgs && selectedOrgs.first();
  try {
    const result = yield call(Api.outgoingMessage.findDefaultValuesByOrganisationId, firstSelectedOrg);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_DEFAULT_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_DEFAULT_DATA_FAIL, error });
    throw error;
  }
}

export function* getOutgoingMessageCopyData(action) {
  yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_COPY_DATA_REQUEST });
  const { payload: { id } } = action;
  try {
    const result =  yield call(Api.outgoingMessage.createCopyByMessageId, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_COPY_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_COPY_DATA_FAIL, error });
    throw error;
  }
}

export function* getOutgoingMessageAttachments(action) {
  yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_ATTACHMENTS_REQUEST });
  const { payload: { id } } = action;
  try {
    const result = yield call(Api.outgoingMessage.findByAttachmentId, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_ATTACHMENTS_SUCCESS, resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_ATTACHMENTS_FAIL, error });
    return error;
  }
}

export function* getOutgoingMessageAttachmentData(action) {
  yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_ATTACHMENT_DATA_REQUEST });
  const { payload: { fileId } } = action;
  try {
    const result = yield call(Api.outgoingMessage.getAttachmentById, fileId);
    const resultData = result && result.data;
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_ATTACHMENT_DATA_SUCCESS });
    return resultData ? yield new Promise((resolve) => {
      blobToBase64(resultData, r => resolve(r));
    }) : '';
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_GET_OUTGOING_MESSAGE_ATTACHMENT_DATA_FAIL, error });
    throw error;
  }
}

export function* downloadOutgoingMessageAttachment(action) {
  yield put({ type: ACTIONS.MESSAGES_DOWNLOAD_OUTGOING_MESSAGE_ATTACHMENT_REQUEST });
  const { payload: { fileId, fileName } } = action;
  try {
    const result = yield call(Api.outgoingMessage.downloadAttachmentById, fileId);
    yield put({ type: ACTIONS.MESSAGES_DOWNLOAD_OUTGOING_MESSAGE_ATTACHMENT_SUCCESS });
    saveAs(result.data, fileName);
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_DOWNLOAD_OUTGOING_MESSAGE_ATTACHMENT_FAIL, error });
    throw error;
  }
}

export function* saveOutgoingMessageData(action) {
  yield put({ type: ACTIONS.MESSAGES_SAVE_OUTGOING_MESSAGE_REQUEST });
  const { payload } = action;
  try {
    let attaches = [];
    if (payload.attaches) {
      const attachesContents = yield all(
        payload.attaches.map((attach) => {
          if (attach.file) {
            return new Promise((resolve) => { blobToBase64(attach.file, r => resolve(r)); });
          }
          return attach.content;
        })
      );
      attaches = payload.attaches.map((attach, index) => ({
        ...attach,
        content: attachesContents[index]
      }));
    }
    const params = {
      ...payload,
      attaches
    };

    const result = yield call(Api.outgoingMessage.update, params);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.MESSAGES_SAVE_OUTGOING_MESSAGE_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения документа', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.MESSAGES_SAVE_OUTGOING_MESSAGE_FAIL, error });
    throw error;
  }
}


export default function* messagesSaga() {
  yield takeLatest(ACTIONS.MESSAGES_LOAD_REQUEST, fetchMessages);
  yield takeLatest(ACTIONS.MESSAGES_SINGLE_INCOMING_LOAD_REQUEST, fetchSingleMessage);

  yield takeEvery(ACTIONS.MESSAGES_IMPORTANT_LOAD_REQUEST, fetchImportantMessages);
  yield takeLatest(ACTIONS.MESSAGES_ATTACHMENTS_REQUEST, fetchAttachments);
  yield takeLatest(ACTIONS.MESSAGES_ATTACHMENTS_FROM_BANK_REQUEST, fetchMessagesFromBankAttachmentsInfo);
}
