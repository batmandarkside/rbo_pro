import chai, { expect }               from 'chai';
import chaiImmutable                  from 'chai-immutable';
import { fromJS, List, Map }          from 'immutable';
import { defaultState }               from '../reducer';
import { ACTIONS }                    from '../constants';

import {
  addOrdinaryNotificationAction,
  addImportantMessagesAction,
  closeImportantMessageAction,
  addConfirmNotification,
  removeConfirmNotification
}                                     from '../actions';

chai.use(chaiImmutable);

describe('Notification reducer tests', () => {
  const ordinaryNotification = fromJS({
    type: 'error',
    title: 'Ошибка!',
    message: 'Проверьте ссылку или добавьте видео с помощью кода вставки.'
  });

  const importantMessages = fromJS({
    type: 'important_message',
    messages : []
  });

  const id = '1234';

  it('defaultState test ', () => {
    expect(defaultState.get('lastOrdinaryNotification')).to.be.an.instanceof(Map);

    expect(defaultState.get('importantMessages'))
      .to.be.an.instanceof(Map);
    expect(defaultState.get('importantMessages'))
      .to.have.deep.property('uid')
      .to.equal('');
    expect(defaultState.get('importantMessages'))
      .to.have.deep.property('type')
      .to.equal('');
    expect(defaultState.get('importantMessages'))
      .to.have.deep.property('messages')
      .to.be.an.instanceof(List);
    expect(defaultState.get('confirms'))
      .to.be.an.instanceof(List);
  });

  it('test actions', () => {
    expect(ACTIONS.NOTIFICATION_ADD_ORDINARY_NOTIFICATION).to.exist;
    expect(ACTIONS.NOTIFICATION_ADD_IMPORTANT_MESSAGES).to.exist;
    expect(ACTIONS.NOTIFICATION_CLOSE_IMPORTANT_MESSAGE).to.exist;

    expect(addOrdinaryNotificationAction).to.be.a('function');
    expect(addImportantMessagesAction).to.be.a('function');
    expect(closeImportantMessageAction).to.be.a('function');
    expect(addConfirmNotification).to.be.a('function');
    expect(removeConfirmNotification).to.be.a('function');

    expect(addOrdinaryNotificationAction(ordinaryNotification))
        .to.have.deep.property('type')
        .to.equal(ACTIONS.NOTIFICATION_ADD_ORDINARY_NOTIFICATION);

    expect(addOrdinaryNotificationAction(ordinaryNotification))
        .to.have.deep.property('notification')
        .to.equal(ordinaryNotification);

    expect(addImportantMessagesAction(importantMessages))
        .to.have.deep.property('type')
        .to.equal(ACTIONS.NOTIFICATION_ADD_IMPORTANT_MESSAGES);
    expect(addImportantMessagesAction(importantMessages))
        .to.have.deep.property('notification')
        .to.equal(importantMessages);

    expect(closeImportantMessageAction(id))
      .to.have.deep.property('type')
      .to.equal(ACTIONS.NOTIFICATION_CLOSE_IMPORTANT_MESSAGE);
    expect(closeImportantMessageAction(id))
      .to.have.deep.property('id')
      .to.equal(id);

    expect(addConfirmNotification({}))
      .to.have.deep.property('type')
      .to.equal(ACTIONS.NOTIFICATION_ADD_CONFIRM_NOTIFICATION);

    expect(removeConfirmNotification({}))
      .to.have.deep.property('type')
      .to.equal(ACTIONS.NOTIFICATION_REMOVE_CONFIRM_NOTIFICATION);
  });
});
