import chai, { expect } from 'chai';
import chaiImmutable from 'chai-immutable';
import { fromJS } from 'immutable';
import notifications, {
  addConfirm,
  removeConfirm,
  mergeUniqMessages,
  addImportantMessages
} from '../reducer';

chai.use(chaiImmutable);

describe('Notification reducer tests', () => {
  const defaultState = notifications(undefined, { type: null });

  const messages = fromJS([
    {
      attachExists: '1',
      messageType: 'Договор',
      receivers: [
        '32219'
      ],
      message: '1111',
      account: '',
      date: '2016-10-27T00:00:00+0300',
      topic: 'Проверка',
      docId: 'c05a8e0e-a13a-4850-9ff0-bb5e5a03dcf6',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '0',
      messageType: 'Запрос об удержанных комиссиях третьих банков',
      receivers: [
        '32219'
      ],
      message: 'обязательное',
      account: '',
      date: '2016-10-20T00:00:00+0300',
      topic: 'обязательное',
      docId: '90b07a39-7bbd-4062-9d1e-76de9d129d16',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Досыл уточненных реквизитов',
      receivers: [
        '32219'
      ],
      message: 'ывывывыв',
      account: '',
      date: '2016-09-13T00:00:00+0300',
      topic: 'проба',
      docId: 'c9a980ff-fe2b-40a5-95b6-73d33499f652',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Запрос об удержанных комиссиях третьих банков',
      receivers: [
        '32219'
      ],
      message: '1234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234567899123456789912345678991234...',
      account: '',
      date: '2016-09-02T00:00:00+0300',
      topic: 'wewewee',
      docId: '3319cb87-61cc-4d5a-aeb0-44764835b53a',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Досыл уточненных реквизитов',
      receivers: [
        '32219'
      ],
      message: 'цйцйцйц',
      account: '',
      date: '2016-09-02T00:00:00+0300',
      topic: 'цйцйцй',
      docId: '57bfecc6-6aff-40d5-bfde-fdc0f4ebfcaf',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Договор',
      receivers: [
        '32219'
      ],
      message: 'сссс',
      account: '40702810200000488968',
      date: '2016-08-10T00:00:00+0300',
      topic: 'ссс',
      docId: '4787be99-16d8-4c0c-9422-4373665f556c',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Досыл уточненных реквизитов',
      receivers: [
        '32219'
      ],
      message: 'фыфыфыфы',
      account: '40702810500001488968',
      date: '2016-08-10T00:00:00+0300',
      topic: 'фыфыфыфы',
      docId: '72b2df12-eb0c-4a34-be8d-761492fe625e',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Договор',
      receivers: [
        '32219'
      ],
      message: 'dddd',
      account: '',
      date: '2016-08-10T00:00:00+0300',
      topic: 'dddd',
      docId: '927b7df1-4f1b-458d-92b3-c7cd69902066',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Запрос о возврате средств/об отмене платежного поручения',
      receivers: [
        '32219'
      ],
      message: 'ааа',
      account: '',
      date: '2016-08-10T00:00:00+0300',
      topic: 'ааа',
      docId: 'bb755f2a-09ae-478e-8cbf-b88f80806e18',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '1',
      messageType: 'Договор',
      receivers: [
        '32219'
      ],
      message: '333333',
      account: '',
      date: '2016-07-29T00:00:00+0300',
      topic: '44444444',
      docId: '221a4671-62d1-452e-89eb-e8907d5e4be8',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    }
  ]);

  const newMessages = fromJS([
    {
      attachExists: '60',
      messageType: 'Договор 123',
      receivers: [
        '32219'
      ],
      message: '1111',
      account: '',
      date: '2016-10-27T00:00:00+0300',
      topic: 'Проверка test',
      docId: 'c05a8e0e-a13a-4850-9ff0-bb5e5a03dcf6',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр',
      read: '1',
      mustRead: '1'
    },
    {
      attachExists: '80',
      messageType: 'Запрос об удержанных комиссиях третьих банков 23874628734',
      receivers: [
        '32219'
      ],
      message: 'обязательное',
      account: '',
      date: '2016-10-20T00:00:00+0300',
      topic: 'обязательное',
      docId: '90b07a39-7bbd-4062-9d1e-76de9d129d16',
      sender: 'АО "РАЙФФАЙЗЕНБАНК" сокр test',
      read: '1',
      mustRead: '1'
    }
  ]);

  const actionAdd = {
    payload: {
      uid: '1234'
    }
  };

  const actionRemove = {
    payload: '1234'
  };

  it('reducer test', () => {
    expect(addConfirm(defaultState, actionAdd).get('confirms').size).equals(1);
    expect(removeConfirm(defaultState, actionRemove).get('confirms').size).equals(0);
  });

  it('expect mergeUniqMessages', () => {
    expect(messages).to.have.size(10);
    let result = mergeUniqMessages(messages, newMessages);
    expect(result).to.have.size(12);

    result = mergeUniqMessages(messages, messages);
    expect(result).to.have.size(10);
  });

  it('expect addImportantMessages', () => {
    expect(defaultState.getIn(['importantMessages', 'messages'])).to.have.size(0);

    let newState = addImportantMessages(defaultState, {
      messages: messages.toJS()
    });
    expect(newState.getIn(['importantMessages', 'messages'])).to.have.size(10);
    expect(newState.get('importantMessagesRaw')).to.have.size(10);

    newState = addImportantMessages(newState, {
      messages: messages.toJS()
    });
    expect(newState.getIn(['importantMessages', 'messages'])).to.have.size(10);

    newState = addImportantMessages(newState, {
      messages: newMessages.toJS()
    });
    expect(newState.getIn(['importantMessages', 'messages'])).to.have.size(12);
  });
});
