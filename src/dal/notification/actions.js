import { ACTIONS } from './constants';

/**
 * notification:
 *  type: 'error',
 *  title: 'Ошибка!',
 *  message: 'Проверьте ссылку',
 *  showOnce: показывать один раз
 * @param notification
 */
export const addOrdinaryNotificationAction = notification => ({
  type: ACTIONS.NOTIFICATION_ADD_ORDINARY_NOTIFICATION,
  notification
});

export const removeOrdinaryNotificationAction = () => ({
  type: ACTIONS.NOTIFICATION_REMOVE_ORDINARY_NOTIFICATION
});


/**
 * important message notification
 * {
 *  type: 'important_message',
 *  messages: [{}, {}...]
 * }
 * @param notification
 */
export const addImportantMessagesAction = notification => ({
  type: ACTIONS.NOTIFICATION_ADD_IMPORTANT_MESSAGES,
  notification
});

export const closeImportantMessageAction = id => ({
  type: ACTIONS.NOTIFICATION_CLOSE_IMPORTANT_MESSAGE,
  id
});

export const addConfirmNotification = payload => ({
  type: ACTIONS.NOTIFICATION_ADD_CONFIRM_NOTIFICATION,
  payload
});

export const removeConfirmNotification = payload => ({
  type: ACTIONS.NOTIFICATION_REMOVE_CONFIRM_NOTIFICATION,
  payload
});


export const checkUserCertsAction = () => ({
  type: ACTIONS.NOTIFICATION_CHECK_CERTS_REQUEST
});

export const certsCreateAction = () => ({
  type: ACTIONS.NOTIFICATION_CERTS_CREATE
});
