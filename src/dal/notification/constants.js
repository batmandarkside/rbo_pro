import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  NOTIFICATION_ADD_ORDINARY_NOTIFICATION: null,
  NOTIFICATION_ADD_IMPORTANT_MESSAGES: null,
  NOTIFICATION_CLOSE_IMPORTANT_MESSAGE: null,
  NOTIFICATION_ADD_CONFIRM_NOTIFICATION: null,
  NOTIFICATION_REMOVE_CONFIRM_NOTIFICATION: null,
  NOTIFICATION_REMOVE_ORDINARY_NOTIFICATION: null,

  NOTIFICATION_CHECK_CERTS_REQUEST: null,
  NOTIFICATION_CHECK_CERTS_SUCCESS: null,
  NOTIFICATION_CHECK_CERTS_FAIL: null,

  NOTIFICATION_CERTS_CREATE: null,
  NOTIFICATION_CERTS_NOT_NOTIFY: null,

  NOTIFICATION_CERTS_REISSUE: null,
  NOTIFICATION_CERTS_NOT_NOTIFY_REISSUE: null,
  NOTIFICATION_EMPTY: null,
});
