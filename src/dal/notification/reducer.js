import { fromJS, is, Set }     from 'immutable';
import { ACTIONS }             from './constants';

/**
 * мержим массивы сообщений
 * @param prevMessages
 * @param nextMessages
 * @returns {List.<V>}
 */
export function mergeUniqMessages(prevMessages, nextMessages) {
  let prevMessagesSet = prevMessages.toSet();
  prevMessagesSet = prevMessagesSet.union(Set(nextMessages));
  return prevMessagesSet.toList();
}

/**
 *
 * @param state
 * @param action
 */
export const addConfirm = (state, action) =>
  state.set('confirms', state.get('confirms').push(action.payload));

/**
 *
 * @param state
 * @param action
 */
export const removeConfirm = (state, action) =>
  state.set('confirms', state.get('confirms').filter(confirm => confirm.uid !== action.payload));


/**
 *
 * @param state
 * @param notification
 */
export const addOrdinaryNotification = (state, notification) => (
  state.set('lastOrdinaryNotification', fromJS({
    ...notification,
    uid: (Date.now() + Math.random()).toString(16)
  }))
);


export const cleanNotification = state => (
  state.set('lastOrdinaryNotification', fromJS({}))
);

/**
 * запрос каждые 5 мин за сообщениями
 * @param state
 * @param notification
 */
export const addImportantMessages = (state, notification) => {
  const prevMessages = state.get('importantMessagesRaw');
  const nextMessages = fromJS(notification.messages);

  // если сообщения не изменились
  if (is(prevMessages, nextMessages)) {
    return state;
  }

  return state.mergeDeep({
    importantMessagesRaw: nextMessages,
    importantMessages: {
      uid: (Date.now() + Math.random()).toString(16),
      type: notification.type,
      messages: mergeUniqMessages(prevMessages, nextMessages)
    }
  });
};

/**
 *
 * @param state
 * @param id
 */
export const closeImportantMessages = (state, id) => (
  state.setIn(
    ['importantMessages', 'messages'],
    state.getIn(['importantMessages', 'messages']).filter(item => (item.get('docId') !== id))
  )
);


/**
 * notification:
 *  type: 'error',
 *  title: 'Ошибка!',
 *  message: 'Проверьте ссылку',
 *
 * Параметры при добавлении нотификации
 * См. https://github.com/igorprado/react-notification-system
 */

export const defaultState = fromJS({
  // поле для хранения исходных сообщений
  // нужно для сравнения immutable.is
  importantMessagesRaw: [],
  lastOrdinaryNotification: {},

  // компонент важных сообщений
  importantMessages: {
    uid: '',
    messages: [],
    type: ''
  },
  confirms: [],
  checkCerts: []
});

const notifications = (state = defaultState, action) => {
  switch (action.type) {
    case ACTIONS.NOTIFICATION_ADD_ORDINARY_NOTIFICATION:
      return addOrdinaryNotification(state, action.notification);
    case ACTIONS.NOTIFICATION_REMOVE_ORDINARY_NOTIFICATION:
      return cleanNotification(state);

    case ACTIONS.NOTIFICATION_ADD_IMPORTANT_MESSAGES:
      return addImportantMessages(state, action.notification);
    case ACTIONS.NOTIFICATION_CLOSE_IMPORTANT_MESSAGE:
      return closeImportantMessages(state, action.id);

    case ACTIONS.NOTIFICATION_ADD_CONFIRM_NOTIFICATION:
      return addConfirm(state, action);
    case ACTIONS.NOTIFICATION_REMOVE_CONFIRM_NOTIFICATION:
      return removeConfirm(state, action);
    case ACTIONS.NOTIFICATION_CHECK_CERTS_SUCCESS:
      return state.set('checkCerts', fromJS(action.payload));
    default:
      return state;
  }
};

export default notifications;

