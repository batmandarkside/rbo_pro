import { put, call, takeEvery, select, all } from 'redux-saga/effects';
import API from 'app/api';
import { List } from 'immutable';
import { push } from 'react-router-redux';
import moment from 'moment';
import { filterPlural }  from 'utils';
import { updateSettings }  from 'dal/profile/sagas';
import { ACTIONS } from './constants';
import { addOrdinaryNotificationAction } from './actions';

export const settingsProfileNotificationsCertSelector = state => state.getIn(['profile', 'settings', 'notificationsCert']);
export const selectedOrgsSelector = state => state.getIn(['profile', 'settings', 'selected_orgs']);
export const settingsSelector = state => state.getIn(['profile', 'settings']);

export function* certsCreateSaga() {
  yield put(push({
    pathname: '/settings/crypto-pro',
    query: { create: true }
  }));
}


// @link https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=105958376

/**
 *
 * @param {string} reissue
 */
export function* certsReissueSaga({ payload: { reissue } }) {
  yield put(push({
    pathname: '/settings/crypto-pro',
    query: { reissue }
  }));
}

export function* certsNotNotifySaga() {
  const settings = yield select(settingsSelector);
  const raw = settings.toJS();
  try {
    yield call(updateSettings, {
      payload: {
        settings: {
          ...raw,
          notificationsCert: {
            ...raw.notificationsCert,
            hideNotificationDate: moment()
          }
        }
      }
    });
    yield put({ type: ACTIONS.NOTIFICATION_EMPTY });
  } catch (error) {
    yield put({ type: ACTIONS.NOTIFICATION_EMPTY });
  }
}

export function* certsNotNotifyReissueSaga() {
  const settings = yield select(settingsSelector);
  const raw = settings.toJS();
  try {
    yield call(updateSettings, {
      payload: {
        settings: {
          ...raw,
          notificationsCert: {
            ...raw.notificationsCert,
            hideNotifyReissueDate: moment()
          }
        }
      }
    });
    yield put({ type: ACTIONS.NOTIFICATION_EMPTY });
  } catch (error) {
    yield put({ type: ACTIONS.NOTIFICATION_EMPTY });
  }
}

/**
 * уведомление об истекающих сертификатах
 * @param payload
 */
export function* сertExpiringSaga({ payload }) {
  const settingsProfileNotificationsCert = yield select(settingsProfileNotificationsCertSelector);
  const notifyReissueDate = settingsProfileNotificationsCert.get('hideNotifyReissueDate');
  const { findCertExpiring } = payload;
  try {
    // Есть активный сертификат (Срок окончания - текущая дата < 31) И нет отправленного в банк запроса на перегенерацию
    if (findCertExpiring && findCertExpiring.length) {
      const diff = moment().diff(moment(notifyReissueDate), 'days');

      if (diff === 0) {
        return;
      }

      const confNotify = {
        type: 'confirm',
        level: 'warning',
        cancel: {
          label: 'Больше не напоминать',
          action: {
            type: ACTIONS.NOTIFICATION_CERTS_NOT_NOTIFY_REISSUE
          }
        }
      };
      const textCert = filterPlural(['сертификат:', 'сертификата:', 'сертификатов:'], findCertExpiring.length);
      if (findCertExpiring.length > 1) {
        confNotify.message = `Через 30 дней истекают ${findCertExpiring.length} ${textCert}`;
        confNotify.success = {
          label: 'Перевыпустить',
          action: {
            type: ACTIONS.NOTIFICATION_CERTS_REISSUE,
            payload: {
              reissue: 'all'
            }
          }
        };

        // Нет активных сертификатов и отправленных в банк запросов
      } else if (findCertExpiring.length === 1) {
        confNotify.message = 'Через 30 дней истекает сертификат:';
        confNotify.success = {
          label: 'Перевыпустить',
          action: {
            type: ACTIONS.NOTIFICATION_CERTS_REISSUE,
            payload: {
              reissue: findCertExpiring[0].cert.id
            }
          }
        };
      }
      yield put(addOrdinaryNotificationAction(confNotify));
    }
  } catch (error) {
    yield put({ type: ACTIONS.NOTIFICATION_EMPTY });
  }
}


/**
 * уведомление об активированных сертификатах
 * @param payload
 */
export function* сertAsActivateSaga({ payload }) {
  const { activatedCert } = payload;
  const settings = yield select(settingsSelector);
  const settingsProfileNotificationsCert = yield select(settingsProfileNotificationsCertSelector);
  let hideCertActiveIds = settingsProfileNotificationsCert.get('hideCertActiveIds') || List();
  const raw = settings.toJS();
  const notificationsCert = settingsProfileNotificationsCert.toJS();

  // уведомление что сертификат успешно активирован
  // прячем навсегда, записываем его id в settings
  try {
    if (activatedCert && (!hideCertActiveIds || !hideCertActiveIds.find(h => h === activatedCert.cert.id))) {
      yield put(addOrdinaryNotificationAction({
        type: 'success',
        autoDismiss: 20,
        message: 'Сертификат успешно активирован.'
      }));

      hideCertActiveIds = hideCertActiveIds.push(activatedCert.cert.id);


      yield call(updateSettings, {
        payload: {
          settings: {
            ...raw,
            notificationsCert: {
              ...notificationsCert,
              hideCertActiveIds: hideCertActiveIds.toJS()
            }
          }
        }
      });
    }
  } catch (error) {
    yield put({ type: ACTIONS.NOTIFICATION_EMPTY });
  }
}

/**
 * нет ни одного активного сертификата
 */
export function* сertNotActiveSaga() {
  const settingsProfileNotificationsCert = yield select(settingsProfileNotificationsCertSelector);
  const hideNotificationDate = settingsProfileNotificationsCert.get('hideNotificationDate');
  try {
    const diff = moment().diff(moment(hideNotificationDate), 'days');
    // const diff = moment.duration(moment().diff(moment(hideDate)));
    // const minutes = parseInt(diff.asMinutes(), 10);

    if (diff === 0) {
      return;
    }

    yield put(addOrdinaryNotificationAction({
      type: 'confirm',
      level: 'warning',
      message: 'У вас нет ни одного активного сертификата',
      success: {
        label: 'Создать',
        action: {
          type: ACTIONS.NOTIFICATION_CERTS_CREATE
        }
      },
      cancel: {
        label: 'Больше не напоминать',
        action: {
          type: ACTIONS.NOTIFICATION_CERTS_NOT_NOTIFY
        }
      }
    }));
  } catch (error) {
    yield put({ type: ACTIONS.NOTIFICATION_EMPTY });
  }
}

/**
 * проверяем наличие у пользователя сертификатов для подписи документов
 * или наличие истекающих сертификатов
 */
export function* checkUserCertsSaga() {
  const selectedOrgs = yield select(selectedOrgsSelector);

  try {
    const checkCertsResult = yield call(API.signature.checkCerts, {
      orgId: selectedOrgs.get(0)
    });
    const checkCertData = (checkCertsResult && checkCertsResult.data) || [];
    yield put({
      type: ACTIONS.NOTIFICATION_CHECK_CERTS_SUCCESS,
      payload: checkCertData
    });

    // отсутствие активных ертификатов
    const findCertMissing = checkCertData.find(c => c.mainStatus === 'certMissing');              
    // активированные сертификаты
    const findIsCertActive = checkCertData.filter(c => c.mainStatus === 'certActive');
    // истекающие сертификаты
    const findCertExpiring = checkCertData.filter(c => c.mainStatus === 'certExpiring');

    yield call(сertExpiringSaga, { payload: { findCertExpiring } });

    if (findIsCertActive && findIsCertActive.length) {
      yield all(
        findIsCertActive.map(activatedCert =>
          call(сertAsActivateSaga, { payload: { activatedCert } })
        )
      );
    }

    if (findCertMissing) {
      yield call(сertNotActiveSaga);
    }
  } catch (e) {
    yield put({ type: ACTIONS.NOTIFICATION_CHECK_CERTS_FAIL, payload: e.response });
  }
}


export default function* notificationSaga() {
  yield takeEvery(ACTIONS.NOTIFICATION_CHECK_CERTS_REQUEST, checkUserCertsSaga);
  yield takeEvery(ACTIONS.NOTIFICATION_CERTS_CREATE, certsCreateSaga);
  yield takeEvery(ACTIONS.NOTIFICATION_CERTS_REISSUE, certsReissueSaga);
  yield takeEvery(ACTIONS.NOTIFICATION_CERTS_NOT_NOTIFY, certsNotNotifySaga);
  yield takeEvery(ACTIONS.NOTIFICATION_CERTS_NOT_NOTIFY_REISSUE, certsNotNotifyReissueSaga);
}
