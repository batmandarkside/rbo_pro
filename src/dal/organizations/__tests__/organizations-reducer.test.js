import chai, { expect }   from 'chai';
import { fromJS }         from 'immutable';
import { CALL_API }       from 'redux-api-middleware';
import chaiImmutable      from 'chai-immutable';
import {
  BRANCH_ITEM,
  LINKS_ITEM,
  ORG_ITEM,
  normalize,
  defaultState
}  from '../reducer';

import { getOrganizationsAction } from '../actions';
import ACTIONS  from '../constants';

chai.use(chaiImmutable);

describe('Organizations reducer tests', () => {
  const payload = {
    branches: [BRANCH_ITEM],
    links: [LINKS_ITEM],
    orgs: [ORG_ITEM]
  };
  
  it('defaultState test ', () => {
    expect(defaultState).to.have.size(3);
    expect(defaultState).to.have.deep.property('branches').to.have.size(0);
    expect(defaultState).to.have.deep.property('links').to.have.size(0);
    expect(defaultState).to.have.deep.property('orgs').to.have.size(0);
  });
  
  it('test actions', () => {
    expect(ACTIONS).to.have.deep.property('ORGANIZATIONS_LOAD_REQUEST');
    expect(ACTIONS).to.have.deep.property('ORGANIZATIONS_LOAD_SUCCESS');
    expect(ACTIONS).to.have.deep.property('ORGANIZATIONS_LOAD_FAIL');
    
    expect(getOrganizationsAction).to.be.a('function');
    
    const result = getOrganizationsAction()[CALL_API];
    
    expect(result).to.have.deep.property('url')
        .to.equal('user/organizations');
    
    expect(result).to.have.deep.property('method')
        .to.equal('GET');
    
    expect(result).to.not.have.deep.property('params');
    
    expect(result).to.have.deep.property('types')
        .to.be.array;
    
    expect(result.types[0]).to.equal(ACTIONS.ORGANIZATIONS_LOAD_REQUEST);
    expect(result.types[1]).to.equal(ACTIONS.ORGANIZATIONS_LOAD_SUCCESS);
    expect(result.types[2]).to.equal(ACTIONS.ORGANIZATIONS_LOAD_FAIL);
  });
  
  
  it('test normalize organizations', () => {
    expect(LINKS_ITEM)
        .to.have.deep.property('branchId')
        .to.equal('f4027a46-7997-4adf-9478-92d05610221c');
    expect(LINKS_ITEM)
        .to.have.deep.property('orgId')
        .to.equal('3002');
    
    
    const state = normalize(defaultState, fromJS(payload));
    const links = state.get('links');
    expect(links)
        .to.have.deep.property([0, 'branchId'])
        .to.equal('3002');
    expect(links)
        .to.have.deep.property([0, 'orgId'])
        .to.equal('f4027a46-7997-4adf-9478-92d05610221c');
  });
});
