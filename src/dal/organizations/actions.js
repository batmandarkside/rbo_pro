import { CALL_API }     from 'redux-api-middleware';
import ACTIONS  from './constants';


export const getOrganizationsAction = () => ({
  [CALL_API]: {
    url: 'user/organizations',
    method: 'GET',
    types: [
      ACTIONS.ORGANIZATIONS_LOAD_REQUEST,
      ACTIONS.ORGANIZATIONS_LOAD_SUCCESS,
      ACTIONS.ORGANIZATIONS_LOAD_FAIL
    ]
  }
});

export const changeOrganizations = () => ({
  type: ACTIONS.ORGANIZATIONS_CHANGE
});
