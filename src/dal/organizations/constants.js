import mirrorCreator from 'mirror-creator';

const prefix = 'rbo-front-app/dal/organizations/';

export default mirrorCreator([
  'ORGANIZATIONS_LOAD_REQUEST',
  'ORGANIZATIONS_LOAD_SUCCESS',
  'ORGANIZATIONS_LOAD_FAIL',
  'ORGANIZATIONS_CHANGE',
], { prefix });
