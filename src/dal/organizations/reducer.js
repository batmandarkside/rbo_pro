import { fromJS }   from 'immutable';
import ACTIONS  from './constants';

// @todo for tests
export const BRANCH_ITEM = {
  id: '11754',
  shortName: 'Дополнительный офис "Отделение Романов двор 2"',
  intName: 'Дополнительный офис "Отделение Романов двор 2"',
  bic: '',
  payName: '',
  intPayName: '',
  corAccount: '',
  placeType: '',
  place: '',
  intPlace: '',
  swift: '',
  countryCode: '',
  countryIsoCode: '',
  address: '',
  intAddress: '',
  placeShortName: '',
  timeZone: '10800'
};

// @todo for tests
export const LINKS_ITEM = {
  branchId: 'f4027a46-7997-4adf-9478-92d05610221c',
  orgId: '3002'
};

// @todo for tests
export const ORG_ITEM = {
  area: 'Москва г',
  city: '',
  country: 'Российская Федерация',
  id: 'f4027a46-7997-4adf-9478-92d05610221c',
  inn : '7729580257',
  intAddress: 'AKADEMIKA ANOKHINA STR 38 BLD 1',
  intName: 'Art Bazaar',
  kpp: '772901001',
  lawType: 'ООО',
  name : 'Общество с ограниченной ответственностью АРТ БАЗАР',
  ogrn: '1077758447073',
  shortName: 'ООО АРТ БАЗАР (сокр)'
};


/**
 * В links поменять местами  orgId и branchId
 *
 * @param state
 * @param payload
 */
export function normalize(state, payload) {
  const newState = state;
  let newPayload = payload;
  let links = newPayload.get('links');
  
  links = links.map((link) => {
    const orgId = link.get('orgId');
    const branchId = link.get('branchId');
    return link.merge({
      orgId: branchId,
      branchId: orgId,
    });
  });
  newPayload = newPayload.set('links', links);
  return newState.merge(newPayload);
}

export const defaultState = fromJS({
  branches: [],
  links: [],
  orgs: []
});

const organizations = (state = defaultState, action) => {
  switch (action.type) {
    case ACTIONS.ORGANIZATIONS_LOAD_SUCCESS:
      return normalize(state, fromJS(action.payload));
    default:
      return state;
  }
};

export default organizations;
