import chai, { expect }   from 'chai';
import { CALL_API }       from 'redux-api-middleware';
import { List, fromJS }   from 'immutable';
import chaiImmutable      from 'chai-immutable';
import profile, {
  setSettings,
  addAllAccounts,
  selectedAllOrganizations,
  removeUserSettingsByType,
  addOrRemoveOrganizationSettings
}  from '../reducer';

import { ACTIONS }       from '../constants';

import {
  getProfileDataAction,  
  checkedStatusesAction,
  checkedAllStatusesAction
} from '../actions';

chai.use(chaiImmutable);

describe('Profile reducer tests', () => {
  const defaultState = profile(undefined, { type: null });
  const account = fromJS({
    lastBalanceCourse     : '0',
    restDate              : '2016-08-02T00:00:00+03:00',
    currentBalance        : '7658',
    accNum                : '40703810300000000038',
    currCodeIso           : 'RUR',
    lastBalance           : '0',
    balanceDate           : '2016-08-02T00:00:00+03:00',
    name                  : 'Рублевый расчетный',
    branchName            : 'АО "РАЙФФАЙЗЕНБАНК"',
    orgName               : 'Коллегия адвокатов Московская городская коллегия адвокатов ФОРТ1',
    branchCorrAcc         : '30101810200000000700',
    accStatus             : '1',
    availableBalance      : '7658',
    rest                  : '7658',
    intName               : 'Рублевый расчетный',
    branchBic             : '044525700',
    id                    : 'ae1e8c97-73e6-4b49-b32f-b770e214941e',
    accType               : '1',
    branchId              : '3002',
    lastBalanceByAccCourse: '0',
    orgId                 : 'a82572db-4561-4187-b196-f079a15833a7',
    currCode              : '810'
  });

  it('defaultState test ', () => {
    expect(defaultState).to.have.deep.property('showProfile').to.be.false;
    expect(defaultState).to.have.deep.property('settings');
    expect(defaultState).to.have.deep.property('accountMovements').to.have.size(0);
    expect(defaultState.get('settings'))
      .to.have.deep.property('favorite_accounts')
      .to.have.size(0);
    expect(defaultState.get('settings'))
      .to.have.deep.property('selected_orgs')
      .to.have.size(0);
  });

  it('test actions', () => {
    expect(ACTIONS).to.have.deep.property('PROFILE_LOAD_REQUEST');
    expect(ACTIONS).to.have.deep.property('PROFILE_LOAD_SUCCESS');
    expect(ACTIONS).to.have.deep.property('PROFILE_LOAD_FAIL');

    expect(ACTIONS).to.have.deep.property('PROFILE_LOGOUT_REQUEST');
    expect(ACTIONS).to.have.deep.property('PROFILE_LOGOUT_SUCCESS');
    expect(ACTIONS).to.have.deep.property('PROFILE_LOGOUT_FAIL');

    expect(ACTIONS).to.have.deep.property('PROFILE_SETTINGS_REQUEST');
    expect(ACTIONS).to.have.deep.property('PROFILE_SETTINGS_SUCCESS');
    expect(ACTIONS).to.have.deep.property('PROFILE_SETTINGS_FAIL');

    expect(ACTIONS).to.have.deep.property('PROFILE_SETTINGS_ADD_ACTIVE_ORG');
    expect(ACTIONS).to.have.deep.property('PROFILE_SETTINGS_ADD_ALL_ORG');
    expect(ACTIONS).to.have.deep.property('PROFILE_SETTINGS_REMOVE_ALL_ORG');

    expect(ACTIONS).to.have.deep.property('PROFILE_UPDATE_SETTINGS_REQUEST');
    expect(ACTIONS).to.have.deep.property('PROFILE_UPDATE_SETTINGS_SUCCESS');
    expect(ACTIONS).to.have.deep.property('PROFILE_UPDATE_SETTINGS_FAIL');

    expect(ACTIONS).to.have.deep.property('PROFILE_LOAD_MOVEMENTS_REQUEST');
    expect(ACTIONS).to.have.deep.property('PROFILE_LOAD_MOVEMENTS_SUCCESS');
    expect(ACTIONS).to.have.deep.property('PROFILE_LOAD_MOVEMENTS_FAIL');
  });

  it('test getProfileDataAction', () => {
    expect(getProfileDataAction).to.be.a('function');
    const result = getProfileDataAction()[CALL_API];

    expect(result).to.have.deep.property('url')
      .to.equal('user');

    expect(result).to.have.deep.property('method')
      .to.equal('GET');

    expect(result).to.have.deep.property('params')
      .to.equal({ current: true });

    expect(result).to.have.deep.property('types')
      .to.be.array;

    expect(result.types[0]).to.equal(ACTIONS.PROFILE_LOAD_REQUEST);
    expect(result.types[1]).to.equal(ACTIONS.PROFILE_LOAD_SUCCESS);
    expect(result.types[2]).to.equal(ACTIONS.PROFILE_LOAD_FAIL);
  });

  /**
   * Выбираем все организации
   */
  it('test selectedAllOrganizations', () => {
    let newState = defaultState.setIn(['settings', 'selected_orgs'], List([
      'sldkfhksjdhfksdhfjksdf',
      'sdkfjhskjdfhgjhsdgfjhsdf'
    ]));

    const newOrg = fromJS([
      {
        id: 'sldkfhksjdhfksdhfjksdf'
      }, {
        id: 'sdkfjhskjdfhgjhsdgfjhsdf'
      }, {
        id: '222222'
      }, {
        id: '333333'
      }
    ]);
    expect(newState.getIn(['settings', 'selected_orgs'])).to.have.size(2);
    newState = selectedAllOrganizations(newState, { organizations: newOrg });
    expect(newState.getIn(['settings', 'selected_orgs'])).to.have.size(4);
  });

  /**
   * Сбрасываем выбор всех организаций
   */
  it('test removeAllOrganizations::removeUserSettingsByType', () => {
    const newState = defaultState.setIn(['settings', 'selected_orgs'], List([
      'sldkfhksjdhfksdhfjksdf',
      'sdkfjhskjdfhgjhsdgfjhsdf'
    ]));

    let selectedOrgs = newState.getIn(['settings', 'selected_orgs']);
    expect(selectedOrgs).to.have.size(2);

    const state = removeUserSettingsByType(newState, 'selected_orgs');
    selectedOrgs = state.getIn(['settings', 'selected_orgs']);
    expect(selectedOrgs).to.have.size(0);
  });

  /**
   * Сбрасываем выбор всех аккаунтов
   */
  it('test removeAllFavoriteAccounts::removeUserSettingsByType', () => {
    const newState = defaultState.setIn(['settings', 'favorite_accounts'], List([
      'sldkfhksjdhfksdhfjksdf',
      'sdkfjhskjdfhgjhsdgfjhsdf'
    ]));

    let selectedOrgs = newState.getIn(['settings', 'favorite_accounts']);
    expect(selectedOrgs).to.have.size(2);

    const state = removeUserSettingsByType(newState, 'favorite_accounts');
    selectedOrgs = state.getIn(['settings', 'favorite_accounts']);
    expect(selectedOrgs).to.have.size(0);
  });

  it('test addOrRemoveOrganizationSettings', () => {
    const newOrg = fromJS([
      {
        id: 'sldkfhksjdhfksdhfjksdf'
      }, {
        id: 'sdkfjhskjdfhgjhsdgfjhsdf'
      }, {
        id: '222222'
      }, {
        id: '333333'
      }
    ]);

    // сделать выбор первой организации из списка
    expect(defaultState.getIn(['settings', 'selected_orgs'])).to.have.size(0);
    let state = addOrRemoveOrganizationSettings(defaultState, { organization: newOrg.get(0) });
    expect(state.getIn(['settings', 'selected_orgs']))
      .to.have.size(1)
      .to.equal(List([newOrg.getIn([0, 'id'])]));

    // сделать выбор посленей организации из списка
    state = addOrRemoveOrganizationSettings(state, { organization: newOrg.get(3) });
    expect(state.getIn(['settings', 'selected_orgs']))
      .to.have.size(2)
      .to.equal(List([newOrg.getIn([0, 'id']), newOrg.getIn([3, 'id'])]));


    // снять выбор первой организации из списка
    state = addOrRemoveOrganizationSettings(state, { organization: newOrg.get(0) });
    expect(state.getIn(['settings', 'selected_orgs']))
      .to.have.size(1)
      .to.equal(List([newOrg.getIn([3, 'id'])]));
  });

  it('test setSettings when settings NULL', () => {
    const settings = { settings: null };
    const state = setSettings(defaultState, settings);
    const newSettings = state.get('settings');
    expect(newSettings)
      .to.have.deep.property('selected_orgs')
      .to.have.size(0);
  });

  it('test addAllAccounts', () => {
    expect(defaultState.getIn(['settings', 'favorite_accounts'])).to.have.size(0);

    const state = addAllAccounts(defaultState, new List([account, account, account]));
    const favoriteAccounts = state.getIn(['settings', 'favorite_accounts']);
    expect(favoriteAccounts).to.have.size(3);
  });

  it('test setSettings when settings not NULL', () => {
    const settings = {
      settings: {
        selected_orgs: [
          'sldkfhksjdhfksdhfjksdf',
          'sdkfjhskjdfhgjhsdgfjhsdf'
        ]
      }
    };
    const state = setSettings(defaultState, settings);
    const newSettings = state.get('settings');
    expect(newSettings)
      .to.have.deep.property('selected_orgs')
      .to.have.size(2);
  });

  test('checkedStatusesAction test ', () => {
    expect(checkedStatusesAction).to.be.a('function');
    const result = checkedStatusesAction(2, 3);

    expect(result).to.have.deep.property('type')
      .to.equal(ACTIONS.RPAY_CHECK_STATUS);

    expect(result)
      .to.have.deep.property('payload')
      .to.equal({ statuses: 2, status  : 3 });
  });

  test('checkedAllStatusesAction test ', () => {
    expect(checkedAllStatusesAction).to.be.a('function');
    const result = checkedAllStatusesAction(true);

    expect(result).to.have.deep.property('type')
      .to.equal(ACTIONS.RPAY_CHECK_ALL_STATUSES);

    expect(result).to.have.deep.property('payload')
      .to.be.true;
  });
});
