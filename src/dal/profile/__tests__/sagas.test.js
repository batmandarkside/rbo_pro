import { call, put }  from 'redux-saga/effects';
import API            from 'app/api';
import { keycloak }   from 'config/application/keycloak';

import {
  checkAuthentication,
  logout
} from './../sagas';

import { profileLogoutAction } from './../actions';

import { ACTIONS } from './../constants';

keycloak.instance = { clearToken: jest.fn() };

describe('Test Profile Sagas', () => {
  describe('checkAuthentication', () => {
    const actionStatus = {
      payload: {
        status: 401
      },
      type: {
        endsWith() {
          return true;
        }
      }
    };

    it('success', () => {
      const generator = checkAuthentication(actionStatus);
      expect(
        generator.next(profileLogoutAction()).value
      ).toEqual(
        put({ type: ACTIONS.PROFILE_LOGOUT_REQUEST })
      );
    });
  });

  describe('logout', () => {
    history.pushState = jest.fn();
    window.sessionStorage = {};
    window.sessionStorage.removeItem = jest.fn();

    it('success', () => {
      const generator = logout();

      expect(
        generator.next().value
      ).toEqual(
        call(API.authentication.logout)
      );
      expect(
        generator.next([]).value
      ).toEqual(
        put({ type: ACTIONS.PROFILE_LOGOUT_SUCCESS, payload: [] })
      );

      expect(generator.return());
    });

    it('fail', () => {
      const generator = logout();
      expect(
        generator.next().value
      ).toEqual(
        call(API.authentication.logout)
      );

      const e = {
        response: 'Test error message'
      };

      expect(
        generator.throw(e).value
      ).toEqual(
        put({
          type : ACTIONS.PROFILE_LOGOUT_FAIL,
          payload: e.response
        })
      );
      expect(history.pushState).toBeCalledWith(null, '', '/');
      expect(window.sessionStorage.removeItem).toBeCalledWith('token');
      expect(generator.return());
    });
  });
});
