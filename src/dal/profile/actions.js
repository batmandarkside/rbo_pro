import { CALL_API } from 'redux-api-middleware';
import { ACTIONS }  from './constants';

export const getProfileDataAction = () => ({
  [CALL_API]: {
    url: 'user',
    params: {
      current: true
    },
    method: 'GET',
    types: [
      ACTIONS.PROFILE_LOAD_REQUEST,
      ACTIONS.PROFILE_LOAD_SUCCESS,
      ACTIONS.PROFILE_LOAD_FAIL
    ]
  }
});

/**
 * Оповещает API о выходе пользователя из системы
 * @return {[type]} [description]
 */
export const profileLogoutAction = () => ({
  type: ACTIONS.PROFILE_LOGOUT_REQUEST
});

/**
 * получаем настройки пользователя 
 */
export const getUserSettingsAction = () => ({
  type: ACTIONS.PROFILE_SETTINGS_REQUEST
});

/**
* обновляем настроки пользователя
*/
export const updateUserSettingsAction = () => ({
  type: ACTIONS.PROFILE_UPDATE_SETTINGS_REQUEST
});

export const accountMovementsAction = params => ({
  [CALL_API]: {
    url: 'accounts/movements',
    method: 'GET',
    params,
    types: [
      ACTIONS.PROFILE_LOAD_MOVEMENTS_REQUEST,
      ACTIONS.PROFILE_LOAD_MOVEMENTS_SUCCESS,
      ACTIONS.PROFILE_LOAD_MOVEMENTS_FAIL
    ]
  }
});

export const exportOperationsAction = params => ({
  [CALL_API]: {
    url: 'accounts/exportScrollerOperations',
    method: 'GET',
    params,
    types: [
      ACTIONS.PROFILE_EXPORT_OPERATIONS_REQUEST,
      ACTIONS.PROFILE_EXPORT_OPERATIONS_SUCCESS,
      ACTIONS.PROFILE_EXPORT_OPERATIONS_FAIL
    ]
  }
});

/**
 * Добавление удаление организации в settings
 * @param organization
 * @param isRemove
 */
export const addUserSettingsOrganizationAction = organization => ({
  type: ACTIONS.PROFILE_SETTINGS_ADD_ACTIVE_ORG,
  payload: {
    organization
  }
});

export const selectedAllOrganizationsAction = organizations => ({
  type: ACTIONS.PROFILE_SETTINGS_ADD_ALL_ORG,
  payload: {
    organizations
  }
});

export const removeAllOrganizationsAction = () => ({
  type: ACTIONS.PROFILE_SETTINGS_REMOVE_ALL_ORG
});


/**
 * Добавление удаление favorite_accounts в settings
 * @param {array} favoriteAccounts
 * @param {boolean} isRemove
 */
export const addUserSettingsFavoriteAccountAction = favoriteAccount => ({
  type: ACTIONS.PROFILE_SETTINGS_ADD_FAVORITE_ACCOUNTS,
  payload: {
    favoriteAccount
  }
});

export const saveSettingsSelectedStatusesAction = statuses => ({
  type: ACTIONS.PROFILE_SETTINGS_SAVE_SELECTED_STATUSES,
  payload: {
    statuses
  }
});

/**
 *
 * @param id - account который переместили
 * @param order - индекс сортировки
 * @param accounts - все favorite аккаунты
 */
export const changeOrderFavoriteAccountsAction = (id, order, accounts) => ({
  type: ACTIONS.PROFILE_SETTINGS_CHANGE_ORDER_ACCOUNTS,
  payload: { id, order, accounts }
});


/**
 * @todo выбираем все accounts
 *  saga слушает это событие
 */
export const selectedAllFavoriteAccountsAction = () => ({
  type: ACTIONS.PROFILE_SETTINGS_ADD_ALL_ACCOUNTS
});

export const removeAllFavoriteAccountsAction = () => ({
  type: ACTIONS.PROFILE_SETTINGS_REMOVE_ALL_ACCOUNTS
});


/**
 *
 * @param {Object} status
 */
export const checkedStatusesAction = (statuses, status) => ({
  type   : ACTIONS.RPAY_CHECK_STATUS,
  payload: { statuses, status }
});

/**
 *
 * @param {boolean} isCheck
 */
export const checkedAllStatusesAction = isCheck => ({
  type   : ACTIONS.RPAY_CHECK_ALL_STATUSES,
  payload: isCheck
});

/**
 *
 * @param {Number} id
 */
export const changeFiltersAction = id => ({
  type   : ACTIONS.MESSAGES_CHANGE_FILTERS,
  payload: { id }
});
