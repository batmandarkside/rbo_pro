import { fromJS, List, Map }        from 'immutable';
import { ACTIONS }                  from './constants';

/**
 * Выбрать все организации
 *
 * @param state
 * @param payload
 */
export function selectedAllOrganizations(state, payload) {
  const { organizations }  = payload;
  const selectedOrgs = state.getIn(['settings', 'selected_orgs']);

  if (selectedOrgs.size !== organizations.size) {
    return state.setIn(['settings', 'selected_orgs'], List(organizations.map(org => org.get('id'))));
  }

  return state;
}

/**
 * @param state
 * @param {string} fieldType
 * @returns {*}
 */
export function removeUserSettingsByType(state, fieldType = 'selected_orgs') {
  if (state.getIn(['settings', fieldType]).size) {
    return state.setIn(['settings', fieldType], new List([]));
  }
  return state;
}

/**
 * выбрать все accounts
 * @param state
 * @param accounts
 */
export function addAllAccounts(state, accounts) {
  return state.setIn(
    ['settings', 'favorite_accounts'],
    accounts.map((acc, order) => new Map({
      id: acc.get('id'),
      order
    })));
}

/**
 *
 * @param {Array} statuses
 * @return {Array<Map<id, label>>}
 */
const normalizeStatuses = (statuses, selected) => (
  statuses.map((status, index) => new Map({
    id   : index,
    label: status,
    selected
  }))
);

const normalizeSettingsSeletedStatuses = (statuses) => {
  let selectedStatuses = statuses;
  if (selectedStatuses && !Map.isMap(selectedStatuses.get(0))) {
    selectedStatuses = normalizeStatuses(selectedStatuses, true);
  }
  return selectedStatuses;
};

/**
 * @todo нормализация settings
 *  с сервера приходит свойство main_rpays.selected_statuses с неправильным форматом статусов
 *  делаем новое свойство rpays_selected_statuses и создаем новый формат статусов
 *
 * @param state
 * @param payload
 * @returns {*}
 */
export function setSettings(state, payload) {
  let settings = state.get('settings');
  let nrmSettings = payload && payload.settings ?
    fromJS(payload) :
    fromJS({ settings: {} });


  if (nrmSettings.has('settings') && nrmSettings.get('settings').size) {
    let selectedStatuses = nrmSettings.getIn(['settings', 'main_rpays.selected_statuses']);
    const rpayWorking = nrmSettings.getIn(['settings', 'rpay.working']);
    selectedStatuses = normalizeSettingsSeletedStatuses(selectedStatuses);

    nrmSettings = nrmSettings
      .deleteIn(['settings', 'main_rpays.selected_statuses'])
      .deleteIn(['settings', 'rpay.working']);

    if (selectedStatuses) {
      nrmSettings = nrmSettings
        .setIn(['settings', 'rpays_selected_statuses'], selectedStatuses || new List([]));
    }

    nrmSettings = nrmSettings
      .setIn(['settings', 'rpay_working'], rpayWorking || new Map({}));

    settings = settings.merge(nrmSettings.get('settings'));
  }

  return state.set('settings', settings);
}

/**
 * { organization } объект организации который выбрали или отменили выбор
 * @param state
 * @param payload
 */
export function addOrRemoveOrganizationSettings(state, payload) {
  const { organization }  = payload;
  let settings = state.getIn(['settings', 'selected_orgs']);
  const orgId = organization.get('id');

  settings = settings.includes(orgId) ?
    settings.filter(id => id !== orgId) :
    settings.push(orgId);

  return state.setIn(['settings', 'selected_orgs'], settings);
}

/**
 *
 * @param state
 * @param payload
 */
export function changeOrderFavoriteAccountsSettings(state, payload) {
  const { accounts }  = payload;
  const sortedFavorites = accounts.map((acc, i) => new Map({
    id   : acc.get('id'),
    order: i
  }));
  return state.setIn(['settings', 'favorite_accounts'], sortedFavorites);
}

/**
 * Добавление удаление favorite_accounts
 * @param state
 * @param payload
 */
export function addToggleFavoriteAccountsSettings(state, payload) {
  const { favoriteAccount }  = payload;
  let favorites = state.getIn(['settings', 'favorite_accounts']);
  const fId = favoriteAccount.get('id');

  if (favorites.find(f => f.get('id') === fId)) {
    favorites = favorites.filter(f => f.get('id') !== fId);
  } else {
    let orders = favorites.map(f => +f.get('order'));
    orders = Math.max(...orders.toJS());
    favorites = favorites.push(fromJS({
      id   : favoriteAccount.get('id'),
      order: favorites.size ? orders + 1 : favorites.size
    }));
  }
  return state.setIn(['settings', 'favorite_accounts'], favorites);
}

/**
 * сохраняем в настройки список активных статусов main_rpays
 * @param state
 * @param payload
 */
export function saveSelectedStatus(state, payload) {
  const { status }  = payload;
  return state.updateIn(['settings', 'rpays_selected_statuses'], statuses => statuses.push(status));
}

/**
 *
 * @param state
 */
export function saveAllSelectedStatuses(state) {
  const statuses = state.get('statuses');
  const filteredStatuses = statuses.filter(status => status.get('selected'));
  return state.setIn(['settings', 'rpays_selected_statuses'], filteredStatuses);
}


/**
 *
 * @param statusId
 * @returns {function(*)}
 */
function toggleStatus(statusId) {
  return (someStatus) => {
    if (someStatus.get('id') === statusId) {
      return someStatus.set('selected', !someStatus.get('selected'));
    }
    return someStatus;
  };
}


/**
 *
 * @param status
 * @returns {*}
 */
export const toggleSelectionOfStatus = (state, { statuses, status }) => {
  const updatedStatuses = statuses.update(s => s.map(toggleStatus(status.get('id'))));
  const statusesState = state.update('statuses', () => updatedStatuses);
  return saveAllSelectedStatuses(statusesState);
};

/**
 *
 * @param {boolean} isSelected
 * @returns {*}
 */
export const setSelectionOfAllStatuses = (state, isSelected) => {
  const statusesState = state.update('statuses', statuses =>
    statuses.map(status => status.set('selected', isSelected))
  );

  return saveAllSelectedStatuses(statusesState);
};

/**
 *
 * @param state
 * @param {Number} id
 */
export const changeFilters = (state, id) => {
  const settings = state.get('settings');
  const selectedFilters = settings.update('messagesFilters', filters => filters.map(item => item.set('selected', item.get('id') === id)));
  return state.set('settings', selectedFilters);
};

export const messagesFilters = fromJS([
  {
    id      : 0,
    name    : 'filters',
    label   : 'Все',
    selected: true,
    sort    : 'read',
    desc    : false,
    value   : 'all'
  },
  {
    id      : 1,
    name    : 'filters',
    label   : 'Непрочитанные',
    isRead  : 0,
    sort    : 'date',
    desc    : false,
    selected: false,
    value   : 'unread'
  }
]);

const import1C = {
  correctionRules: '',
  payerNameFromFile: false,
  paymentKindFromFile: true,
  paymentPurposeIgnoreNewLines: false,
  shouldCorrectSymbols: false
};

const correspondentsContainer = {};

export const defaultState = fromJS({
  showProfile     : false,
  statuses        : [],
  fullName        : '',
  settings        : {
    firstLoadAccounts      : true,
    firstLoadFilters       : true,
    favorite_accounts      : [],
    selected_orgs          : [],
    rpays_selected_statuses: [],
    main_page_view_type    : 'compact',
    working                : {},
    notificationsCert: {      
      hideNotificationDate: null,
      hideNotifyReissueDate: null,
      hideCertActiveIds: [],
    },
    messagesFilters,
    import1C,
    correspondentsContainer,
    CPaymentsContainer: {}
  },
  accountMovements: []
});

export default function profile(state = defaultState, action) {
  switch (action.type) {
    case ACTIONS.PROFILE_LOAD_SUCCESS:
      return state.merge(fromJS(action.payload));
    case ACTIONS.PROFILE_SETTINGS_SUCCESS:
    case ACTIONS.PROFILE_UPDATE_SETTINGS_SUCCESS:
      return setSettings(state, action.payload);
    case ACTIONS.PROFILE_SETTINGS_ADD_ACTIVE_ORG:
      return addOrRemoveOrganizationSettings(state, action.payload);
    case ACTIONS.PROFILE_SETTINGS_ADD_ALL_ORG:
      return selectedAllOrganizations(state, action.payload);
    case ACTIONS.PROFILE_SETTINGS_REMOVE_ALL_ORG:
      return removeUserSettingsByType(state, 'selected_orgs');
    case ACTIONS.PROFILE_LOAD_MOVEMENTS_SUCCESS:
      return state.set('accountMovements', fromJS(action.payload));
    case ACTIONS.PROFILE_SETTINGS_CHOOSE_ALL_ACCOUNTS:
      return addAllAccounts(state, action.payload);
    case ACTIONS.PROFILE_SETTINGS_REMOVE_ALL_ACCOUNTS:
      return removeUserSettingsByType(state, 'favorite_accounts');
    case ACTIONS.PROFILE_SETTINGS_ADD_FAVORITE_ACCOUNTS:
      return addToggleFavoriteAccountsSettings(state, action.payload);
    case ACTIONS.PROFILE_SETTINGS_CHANGE_ORDER_ACCOUNTS:
      return changeOrderFavoriteAccountsSettings(state, action.payload);
    case ACTIONS.PROFILE_SETTINGS_SAVE_SELECTED_STATUSES:
      return saveSelectedStatus(state, action.payload);
    case ACTIONS.RPAY_CHECK_STATUS:
      return toggleSelectionOfStatus(state, action.payload);
    case ACTIONS.RPAY_CHECK_ALL_STATUSES:
      return setSelectionOfAllStatuses(state, action.payload);

    case ACTIONS.MESSAGES_CHANGE_FILTERS:
      return changeFilters(state, action.payload.id);

    case ACTIONS.FIRST_LOAD_FILTERS_SUCCESS:
      return state.setIn(['settings', 'firstLoadFilters'], false);
    case ACTIONS.FIRST_LOAD_ACCOUNTS_SUCCESS:
      return state.setIn(['settings', 'firstLoadAccounts'], false);

    default:
      return state;
  }
}
