import { put, call, takeEvery, select } from 'redux-saga/effects';
import { keycloak } from 'config/application/keycloak';
import API from 'app/api';
import { ACTIONS } from './constants';
import { profileLogoutAction } from './actions';

export const accountsListSelector = state => state.getIn(['accounts', 'list']);
export const orgsSelector = state => state.getIn(['organizations', 'orgs']);
export const selectedOrgsSelector = state => state.getIn(['profile', 'settings', 'selected_orgs']);
export const settingsSelector = state => state.getIn(['profile', 'settings']);

export function* checkAuthentication(action) {
  if (action.type.endsWith('LOAD_FAIL') && action.payload && action.payload.status && action.payload.status === 401) {
    yield put(profileLogoutAction());
  }
}

export function* getUserSettingsSaga() {
  const settings = yield select(settingsSelector);

  try {
    // user/settings
    let result = yield call(API.settings.findOneForCurrentSession);
    let data = (result && result.data);

    // если settings null ( такое бывает когда пользователь новый ), то записываем default state settings
    if (!data || !data.settings) {
      result = yield call(updateSettings, {
        payload: {
          settings: {
            ...settings.toJS()
          }
        }
      });
      data = (result && result.data);
    }
    yield put({ type: ACTIONS.PROFILE_SETTINGS_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: ACTIONS.PROFILE_SETTINGS_FAIL, payload: error });
  }
}

export function* updateSettings(action) {
  const { payload } = action;
  try {
    const result = yield call(API.settings.updateForCurrentSession, payload);
    const data = (result && result.data) || {};
    yield put({ type: ACTIONS.PROFILE_UPDATE_SETTINGS_SUCCESS, payload: data });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.PROFILE_UPDATE_SETTINGS_FAIL, payload: error });
    throw error;
  }
}

/**
 * чтобы уйти от строго - updateUserSettingsAction
 */
export function* updateSettingsSaga() {
  const settings = yield select(settingsSelector);
  try {
    const result = yield call(API.settings.updateForCurrentSession, {
      settings: { ...settings.toJS() }
    });
    const data = (result && result.data) || {};
    yield put({ type: ACTIONS.PROFILE_UPDATE_SETTINGS_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: ACTIONS.PROFILE_UPDATE_SETTINGS_FAIL, payload: error });
  }
}

export function* logout() {
  try {
    const logoutSuccess = yield call(API.authentication.logout);
    yield put({ type: ACTIONS.PROFILE_LOGOUT_SUCCESS, payload: logoutSuccess });
  } catch (e) {
    yield put({ type: ACTIONS.PROFILE_LOGOUT_FAIL, payload: e.response });
  } finally {
    if (keycloak && keycloak.instance) {
      history.pushState(null, '', '/');
      keycloak.instance.clearToken();
    }
    if (window.sessionStorage) {
      window.sessionStorage.removeItem('token');
    }
  }
}

export function* profileLoadSuccess() {
  const organizations = yield select(orgsSelector);
  const selectedOrgs = yield select(selectedOrgsSelector);
  if (organizations.size === 1 && selectedOrgs.size === 0) {
    yield put({ type: ACTIONS.PROFILE_SETTINGS_ADD_ALL_ORG, payload: { organizations } });
  }
}

export function* selectedAllAccounts() {
  const accounts = yield select(accountsListSelector);
  if (accounts.size) {
    yield put({ type: ACTIONS.PROFILE_SETTINGS_CHOOSE_ALL_ACCOUNTS, payload: accounts });
  }
}

export default function* profileSaga() {
  yield takeEvery('*', checkAuthentication);
  yield takeEvery(ACTIONS.PROFILE_LOAD_SUCCESS, profileLoadSuccess);
  yield takeEvery(ACTIONS.PROFILE_LOGOUT_REQUEST, logout);
  yield takeEvery(ACTIONS.PROFILE_SETTINGS_REQUEST, getUserSettingsSaga);
  yield takeEvery(ACTIONS.PROFILE_UPDATE_SETTINGS_REQUEST, updateSettingsSaga);
  yield takeEvery(ACTIONS.PROFILE_SETTINGS_ADD_ALL_ACCOUNTS, selectedAllAccounts);
}
