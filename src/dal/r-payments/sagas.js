import { put, call, select } from 'redux-saga/effects';
import { saveAs } from 'file-saver';
import moment from 'moment-timezone';
import API from 'app/api';
import { selectedOrganizationsSelector, RBOControlsError } from '../utils';
import { ACTIONS } from './constants';

export function* getCount(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_COUNT_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgIds: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result = yield call(API.rpay.countByQuery, data);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_COUNT_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_COUNT_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getPayments(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_LIST_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result =  yield call(API.rpay.findAllByQuery, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_LIST_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_LIST_FAIL, payload: [], error });
    throw error;
  }
}

export function* getTemplates(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATES_LIST_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result =  yield call(API.rpay.findTemplatesByQuery, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATES_LIST_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATES_LIST_FAIL, payload: [], error });
    throw error;
  }
}

export function* getImports(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_IMPORTS_LIST_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result =  yield call(API.imports.findAllByQuery, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_IMPORTS_LIST_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_IMPORTS_LIST_FAIL, payload: [], error });
    throw error;
  }
}

export function* exportPaymentsListToExcel(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_EXPORT_LIST_TO_EXCEL_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result = yield call(API.exports.createExportRpaysList, data);
    saveAs(
      result.data,
      `payment_orders_${payload.section ? payload.section : 'working'}_${moment().format('DD-MM-YYYY_HH-mm-ss')}.xlsx`
    );
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_EXPORT_LIST_TO_EXCEL_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_EXPORT_LIST_TO_EXCEL_FAIL, error });
    throw error;
  }
}

export function* getPaymentData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_DATA_REQUEST, payload });
  const { id } = payload;
  try {
    const result = yield call(API.rpay.findOneById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getPaymentDefaultData() {
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_DEFAULT_DATA_REQUEST });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  try {
    const result = yield call(API.rpay.findDefaultValuesByOrganisationId, firstSelectedOrganization);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_DEFAULT_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_DEFAULT_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getPaymentCopyData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_COPY_DATA_REQUEST, payload });
  const { id } = payload;
  try {
    const result =  yield call(API.rpay.createCopyDocumentById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_COPY_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_COPY_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getPaymentFromTemplateData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_FROM_TEMPLATE_DATA_REQUEST, payload });
  const { templateId } = payload;
  try {
    const result =  yield call(API.rpay.createDocumentFromTemplate, templateId);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_FROM_TEMPLATE_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_FROM_TEMPLATE_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getPaymentErrors(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_ERRORS_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    customerBankRecordID: firstSelectedOrganization,
    ...payload
  };
  try {
    const result =  yield call(API.rpay.validateDocument, data);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_ERRORS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_PAYMENT_ERRORS_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getTemplateData(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATE_DATA_REQUEST, payload });
  const { id } = payload;
  try {
    const result =  yield call(API.rpay.getTemplateById, id);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATE_DATA_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATE_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* getTemplateErrors(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATE_ERRORS_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    customerBankRecordID: firstSelectedOrganization,
    ...payload
  };
  try {
    const result =  yield call(API.rpay.validateTemplate, data);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATE_ERRORS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_GET_TEMPLATE_ERRORS_FAIL, payload: {}, error });
    throw error;
  }
}

export function* savePaymentData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_PAYMENT_DATA_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    customerBankRecordID: firstSelectedOrganization,
    ...payload
  };
  try {
    const result = yield call(API.rpay.createByRpay, data);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_PAYMENT_DATA_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения документа', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_PAYMENT_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* saveTemplateData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_TEMPLATE_DATA_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  const data = {
    customerBankRecordID: firstSelectedOrganization,
    ...payload
  };
  try {
    const result = yield call(API.rpay.saveTemplate, data);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_TEMPLATE_DATA_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения шаблона документа', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_TEMPLATE_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

export function* savePaymentAsTemplate(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_PAYMENT_AS_TEMPLATE_REQUEST, payload });
  try {
    const result = yield call(API.rpay.saveAsTemplate, payload);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_PAYMENT_AS_TEMPLATE_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения шаблона документа', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_SAVE_PAYMENT_AS_TEMPLATE_FAIL, error });
    throw error;
  }
}

export function* removeTemplate(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_R_PAYMENTS_REMOVE_TEMPLATE_REQUEST, payload });
  try {
    const result = yield call(API.rpay.removeTemplate, payload);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_REMOVE_TEMPLATE_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_R_PAYMENTS_REMOVE_TEMPLATE_FAIL, error });
    throw error;
  }
}
