import { call, put, select } from 'redux-saga/effects';
import API from 'app/api';
import { selectedOrganizationsSelector, RBOControlsError } from '../utils';
import { ACTIONS } from './constants';

const getRecallParentDocDataAPIMethod = (parentDocType) => {
  switch (parentDocType) {
    case 'cur-transfers':
      return API.curtransfers.findOneById;
    case 'r-payments':
      return API.rpay.findOneById;
    default:
      return new Error('Неизвестный тип документа (Unknown parentDocType)');
  }
};

const getRecallParentDocName = (parentDocType) => {
  switch (parentDocType) {
    case 'cur-transfers':
      return 'Валютный перевод';
    case 'r-payments':
      return 'Рублевое платежное поручение';
    default:
      return '';
  }
};

/**
 * Получение данных по отзыву
 * @action: {
 *   payload: {
 *     parentDocType   - тип родительского документа
 *     parentDocId     - id родительского документа
 *     docId           - id отзыва
 *   }
 * }
 *
 */
export function* getDocData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_DATA_REQUEST, payload });
  const { parentDocType, parentDocId, docId } = payload;
  try {
    const getParentDocDataAPIMethod = getRecallParentDocDataAPIMethod(parentDocType);
    const result = yield call(getParentDocDataAPIMethod, parentDocId);
    const parentDocData = (result && result.data) || null;
    const resultData = (parentDocData && parentDocData.linkedDocs
      && parentDocData.linkedDocs.find(item => item.recordID === docId)) || null;
    if (!parentDocData || !resultData) throw new Error('Ошибка получения данных документа');
    const resultFormattedData = {
      ...resultData,
      recallDocID: parentDocData.recordID,
      recallDocType: parentDocType,
      parentDoc: {
        docName: getRecallParentDocName(parentDocType),
        docNumber: parentDocData.docNumber,
        docDate: parentDocData.docDate,
        docAmount: parentDocData.amount,
        orgId: parentDocData.customerBankRecordID,
        payerAccount: parentDocData.payerAccount,
        receiverAccount: parentDocData.receiverAccount
      }
    };
    yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_DATA_SUCCESS, payload: resultFormattedData });
    return resultFormattedData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

/**
 * Получение данных по умолчанию для создания нового отзыва
 * @action: {
 *   payload: {
 *     parentDocType   - тип родительского документа
 *     parentDocId     - id родительского документа
 *   }
 * }
 *
 */
export function* getDocDefaultData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_DEFAULT_DATA_REQUEST, payload });
  const { parentDocType, parentDocId } = payload;
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const firstSelectedOrganization = selectedOrganizations && selectedOrganizations.first();
  try {
    const getParentDocDataAPIMethod = getRecallParentDocDataAPIMethod(parentDocType);
    const parentDoc = yield call(getParentDocDataAPIMethod, parentDocId);
    const parentDocData = (parentDoc && parentDoc.data) || null;
    const result = yield call(API.recall.getDocDefaultData, firstSelectedOrganization);
    const resultData = (result && result.data) || null;
    if (!parentDocData || !resultData) throw new Error('Ошибка получения данных документа');
    const resultFormattedData = {
      ...resultData,
      recallDocID: parentDocData.recordID,
      recallDocType: parentDocType,
      parentDoc: {
        docName: getRecallParentDocName(parentDocType),
        docNumber: parentDocData.docNumber,
        docDate: parentDocData.docDate,
        docAmount: parentDocData.amount,
        orgId: parentDocData.customerBankRecordID,
        payerAccount: parentDocData.payerAccount,
        receiverAccount: parentDocData.receiverAccount
      }
    };
    yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_DEFAULT_DATA_SUCCESS, payload: resultFormattedData });
    return resultFormattedData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_DEFAULT_DATA_FAIL, payload: {}, error });
    throw error;
  }
}

/**
 * Проверка данных отзыва и получение ошибок
 * @action: {
 *   payload: {...}    - данные отзыва
 * }
 *
 */
export function* getDocErrors(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_ERRORS_REQUEST, payload });
  try {
    const result =  yield call(API.recall.getDocErrors, payload);
    const resultData = (result && result.data) || {};
    yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_ERRORS_SUCCESS, payload: resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_RECALLS_GET_DOC_ERRORS_FAIL, payload: {}, error });
    throw error;
  }
}

/**
 * Сохранение отзыва
 * @action: {
 *   payload: {...}    - данные отзыва
 * }
 *
 */
export function* saveDocData(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_RECALLS_SAVE_DOC_DATA_REQUEST, payload });
  try {
    const result = yield call(API.recall.saveDoc, payload);
    const resultData = (result && result.data) || {};
    if (!resultData.errors || !resultData.errors.filter(error => error.level === '3').length) {
      yield put({ type: ACTIONS.DAL_RECALLS_SAVE_DOC_DATA_SUCCESS, payload: resultData });
    } else {
      throw new RBOControlsError('Ошибки сохранения документа', resultData.errors);
    }
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_RECALLS_SAVE_DOC_DATA_FAIL, payload: {}, error });
    throw error;
  }
}
