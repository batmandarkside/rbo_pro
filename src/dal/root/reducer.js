import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const initialState = fromJS({
  globalLoaderShow: false
});

export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.ROOT_CONTAINER_TOGGLE_LOADER:
      return state.set('globalLoaderShow', action.payload.show);
    default:
      return state;
  }
}
