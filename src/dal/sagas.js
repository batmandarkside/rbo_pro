import { all } from 'redux-saga/effects';
import profileSaga          from './profile/sagas';
import confirmSaga          from './confirm/sagas';
import messagesSaga         from './messages/sagas';
import signSaga             from './sign/sagas';
import documentsSaga        from './documents/sagas';
import beneficiariesSaga      from './beneficiaries/sagas';
import notificationSaga     from './notification/sagas';

export default function* rootSagas() {
  yield all([
    profileSaga(),
    confirmSaga(),
    messagesSaga(),
    signSaga(),
    documentsSaga(),
    beneficiariesSaga(),
    notificationSaga()
  ]);
}
