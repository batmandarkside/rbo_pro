import { call, put, all }      from 'redux-saga/effects';
import { delay }          from 'redux-saga';
import Api                from 'app/api';
import { fromJS }         from 'immutable';
import { toggleGlobalLoaderAction }   from 'dal/root/actions';
import { addOrdinaryNotificationAction }  from 'dal/notification/actions';
import { prepareConfForNotification }  from 'utils/errors-util';

import { hideSignModal } from '../sagas/show-modal-saga';
import { getCryptoProfilesSaga } from '../sagas/get-crypto-profiles-saga';
import { addDocForSignSaga } from '../sagas/operations-saga';
import { saveDocIdForFutureSignSelector } from '../sagas/create-sign-saga';

import {
  getCryptoProfilesAction,  
  signDocAddAndTrySigningAction,
  signDocAdd,
  clearSignDataAction
} from '../actions';

import { ACTIONS } from '../constants';

describe('Test Sign Sagas', () => {
  const state = fromJS({
    sign: {
      saveDocIdForFutureSign: ['3465876348756sjkdfgsjhdgfsjgdf'],
      saveDocVisa: {
        docIds: ['3465876348756sjkdfgsjhdgfsjgdf'],
        visa: false
      },
      channel: {
        success: '',
        cancel: '',
        fail: ''
      },
      createSignResult      : {}
    }
  });


  describe('addDocForSignSaga', () => {
    it('success', () => {
      const id = 'sdjfhsjhrdtsghdfg345345';
      const generator = addDocForSignSaga(signDocAddAndTrySigningAction({
        docIds: [id],
        visa: false
      }));

      expect(
        generator.next().value
      ).toEqual(
        put(signDocAdd([id], false))
      );

      expect(
        generator.next().value
      ).toEqual(
        call(delay, 0)
      );

      expect(
        generator.next().value
      ).toEqual(
        put(getCryptoProfilesAction(false))
      );
    });
  });

  describe('getCryptoProfilesSaga', () => {
    it('success', () => {
      const generator = getCryptoProfilesSaga(getCryptoProfilesAction());

      expect(
        generator.next().value
      ).toEqual(
        put(toggleGlobalLoaderAction(true))
      );

      generator.next();

      expect(
        generator.next(saveDocIdForFutureSignSelector(state)).value
      ).toEqual(
        call(Api.signature.getCryptoProfiles, {
          docId  : state.getIn(['sign', 'saveDocIdForFutureSign']).toJS(),
          forVisa: false,
        })
      );

      expect(
        generator.next({ data: [{ cryptoProfiles: [{ test: 123  }] }] }).value
      ).toEqual(
        all([
          put({
            type   : ACTIONS.SIGN_CRYPTO_PROFILES_LOAD_SUCCESS,
            payload: [{ cryptoProfiles: [{ test: 123  }] }]
          }),
          put(toggleGlobalLoaderAction(false))
        ])
      );
    });

    it('fail', () => {
      const generator = getCryptoProfilesSaga(getCryptoProfilesAction());

      expect(
        generator.next().value
      ).toEqual(
        put(toggleGlobalLoaderAction(true))
      );

      generator.next();

      expect(
        generator.next(saveDocIdForFutureSignSelector(state)).value
      ).toEqual(
        call(Api.signature.getCryptoProfiles, {
          docId  : state.getIn(['sign', 'saveDocIdForFutureSign']).toJS(),
          forVisa: false,
        })
      );

      // todo сделать правельное тестирование all([])
      it.skip('fail', () => {
        expect(
          generator.throw('sign.wrong_doc_status').value
        ).toEqual(
          all([
            put({
              type: ACTIONS.SIGN_CRYPTO_PROFILES_LOAD_FAIL,
              error: 'sign.wrong_doc_status'
            }),
            put({ type: ACTIONS.SIGN_DOCUMENT_SEND_FAIL }),
            put(addOrdinaryNotificationAction(prepareConfForNotification('sign.wrong_doc_status'))),
            put(toggleGlobalLoaderAction(false))
          ])
        );
      });
    });
  });

  describe('hideSignModal', () => {
    it('success', () => {
      const generator = hideSignModal();

      expect(
        generator.next().value
      ).toEqual(
        all([
          put({ type: ACTIONS.SIGN_MODAL_VISIBILITY, payload: { show: false, isNotPutCancelActionChannel: true } }),
          put(clearSignDataAction())
        ])
      );
    });
  });
});
