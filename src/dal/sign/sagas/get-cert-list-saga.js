import { put, call } from 'redux-saga/effects';
import API from 'app/api';
import { ACTIONS } from '../constants';
import { normalizeCertList } from '../normalizr';

export function* getCertListSaga() {  
  try {
    const result = yield call(API.signature.getCertList);
    const data = normalizeCertList(result.data || []);
    yield put({
      type: ACTIONS.DAL_CRYPTOPRO_GET_CERT_LIST_SUCCESS,
      payload: {
        list: data
      }
    });  
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CRYPTOPRO_GET_CERT_LIST_FAIL });
    throw error;
  }
}
