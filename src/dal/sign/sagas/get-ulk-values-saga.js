import { put, call } from 'redux-saga/effects';
import API from 'app/api';
import { ACTIONS } from '../constants';

export function* getUlkValuesSaga(action) {
  const { payload } = action;
  try {
    const result = yield call(API.signature.getDefaultValuesCertReq, payload);
    const data = result.data || {};

    yield put({
      type: ACTIONS.DAL_CRYPTOPRO_GET_ULK_SUCCESS,
      payload: data
    });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CRYPTOPRO_GET_ULK_FAIL });
    throw error;
  }
}

/**
 * 
 * @param {*} param0 
 */
export function* getUlkValuesForRegenerateCertSaga({ cert }) {
  try {
    const result = yield call(API.signature.getDefaultValuesCertReq, {
      certId: cert.getIn(['cert', 'id'], ''),
      ulkId: cert.getIn(['ulk', 'id'], '')
    });
    const data = result.data || {};

    yield put({
      type: ACTIONS.DAL_CRYPTOPRO_GET_ULK_FOR_REGENERATE_SUCCESS,
      payload: data
    });

    return data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CRYPTOPRO_GET_ULK_FOR_REGENERATE_FAIL });
    throw error;
  }
}
