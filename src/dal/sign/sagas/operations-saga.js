import { put, call } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import Api from 'app/api';
import { saveAs } from 'file-saver';
import moment from 'moment-timezone';
import { List } from 'immutable';
import { ACTIONS } from '../constants';
import {
  signDocAdd,  
  getCryptoProfilesAction  
} from '../actions';

export const saveDocIdForFutureSignSelector = state => ({
  docIdsCollection: state.getIn(['sign', 'saveDocIdForFutureSign'], List([])).toJS(),
  forVisa: state.getIn(['sign', 'saveDocVisa', 'visa'], false)
});
export const createSignResultSelector = state => state.getIn(['sign', 'createSignResult']);
export const smsCodeSelector = state => state.getIn(['sign', 'smsCode']);
export const channelSelector = state => state.getIn(['sign', 'channel']);

/**
 *
 * @param action
 */
export function* downloadSign(action) {
  const {
    payload: {
      params
    }
  } = action;

  const { docId, signId, format } = params;

  try {
    const resultSign = yield call(Api.signature.signToFormat, {
      docId,
      signId,
      format
    });
    yield put({
      type: ACTIONS.SIGN_DOWNLOAD_SUCCESS,
      payload: resultSign
    });

    return resultSign.data;
  } catch (error) {
    yield put({ type: ACTIONS.SIGN_DOWNLOAD_FAIL, error });

    return error;
  }
}

/**
 * добавляем в reducer id документа на подпись
 * так же сохраняем уникальный id канала в котором работает подпись - actionChannelName
 * и вызываем компонент подписи
 * @param action
 */
export function* addDocForSignSaga(action) {
  const { payload: { docIds, visa, channel } } = action;
  // сограняем в reducer
  yield put(signDocAdd(docIds, visa));

  // задержка для теста
  yield call(delay, 0);

  if (channel) {
    yield put({
      type: ACTIONS.SIGN_SAVE_CHANNEL,
      payload: {
        channel
      }
    });
  }

  // получение КриптоПрофайлов для выбора способа подписи
  yield put(getCryptoProfilesAction(false));
}

/**
 *
 * @param action
 */
export function* printSignature(action) {
  yield put({ type: ACTIONS.SIGN_PRINT_SIGNATURE_REQUEST });
  const { payload : { docId, signId, signDate } } = action;
  try {
    const result = yield call(Api.signature.signToFormat, { docId, signId, format: 'pdf' });
    yield put({ type: ACTIONS.SIGN_PRINT_SIGNATURE_SUCCESS });
    saveAs(result.data, `Signature${signDate && `_${moment(signDate).format('DD-MM-YYYY_HH-mm')}`}.pdf`);
  } catch (error) {
    yield put({ type: ACTIONS.SIGN_PRINT_SIGNATURE_FAIL, error });
    throw error;
  }
}

/**
 * 
 * @param {docId} param
 */
export function* printCertSaga({ docId }) {  
  try {
    const result = yield call(Api.signature.certReqFormatPrint, {
      docId
    });
    yield put({
      type: ACTIONS.DAL_CRYPTO_OPERATION_PRINT_SUCCESS,
      payload: docId
    });

    // скачивание файла
    saveAs(result.data, 'cert.pdf');

    yield delay(1000);
    // печать в новом окне
    /* if (URL && URL.createObjectURL) {
      const fileURL = URL.createObjectURL(result.data);
      window.open(fileURL);
    } */
    return result.data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CRYPTO_OPERATION_PRINT_FAIL, error: error.message });
    throw error;
  }
}

/**
 *
 * @param action
 */
export function* downloadSignature(action) {
  yield put({ type: ACTIONS.SIGN_DOWNLOAD_SIGNATURE_REQUEST });
  const { payload : { docId, signId, signDate } } = action;
  try {
    const result = yield call(Api.signature.signToFormat, { docId, signId, format: 'zip' });
    yield put({ type: ACTIONS.SIGN_DOWNLOAD_SIGNATURE_SUCCESS });
    saveAs(result.data, `Signature${signDate && `_${moment(signDate).format('DD-MM-YYYY_HH-mm')}`}.zip`);
  } catch (error) {
    yield put({ type: ACTIONS.SIGN_DOWNLOAD_SIGNATURE_FAIL, error });
    throw error;
  }
}

export function* downloadActiveCertSaga({ certId }) {
  try {
    const result = yield call(Api.signature.downloadActiveCert, {
      certId
    });
    const data = result.data;
    yield put({
      type: ACTIONS.DAL_CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_SUCCESS,
      payload: certId
    });

    // скачивание файла
    const file = new File([data], 'certificate.cert', { type: 'text/plain;charset=utf-8' });
    saveAs(file);
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CRYPTO_OPERATION_DOWNLOAD_ACTIVE_CERT_FAIL, error: error.message });
    throw error;
  }
}

/**
 *
 * @param payload
 */
export function* deleteCertSaga({ payload }) {
  const { id, requestType } = payload;
  try {
    const result = yield call(Api.signature.deleteCert, {
      docId: id,
      reqType: requestType === 'new' ? 'CryptoProReqNew' : 'CryptoProReqRegen'
    });
    const data = result && result.data;
    if (data) {
      yield put({
        type: ACTIONS.DAL_CRYPTO_OPERATION_DELETE_SUCCESS,
        payload: id
      });
    } else {
      throw new Error('Ошибка сервера');
    }
    return data;    
  } catch (error) {    
    yield put({ type: ACTIONS.DAL_CRYPTO_OPERATION_DELETE_FAIL, error: error.message });
    throw error;
  }
}
