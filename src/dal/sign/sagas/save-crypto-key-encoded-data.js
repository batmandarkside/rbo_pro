import { put, call } from 'redux-saga/effects';
import API from 'app/api';
import { ACTIONS } from '../constants';

export function* saveCryptoKeyAndEncodedData({ payload }) {  
  try {
    // отправляем открытый ключ на сервер для валидации
    // получаем recordID для отправки в банк
    const result = yield call(API.signature.createNewCert, payload);    
    const data = result.data || {};
    yield put({
      type: ACTIONS.DAL_CRYPTOPRO_CREATE_NEW_CERT_SUCCESS,
      payload: {
        list: data
      }
    });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CRYPTOPRO_CREATE_NEW_CERT_FAIL });
    throw error;
  }
}
