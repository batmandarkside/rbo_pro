import { call, put } from 'redux-saga/effects';
import API from 'app/api';
import { ACTIONS } from '../constants';

/**
 *
 * @param payload
 * @returns {*|{}}
 */
export function* sendToBankCert({ payload }) {
  try {
    yield put({ type: ACTIONS.DAL_CRYPTO_SEND_TO_BANK_NEW_CERT_REQUEST });
    const result = yield call(API.signature.sendCertToBank, payload);
    const data = (result && result.data) || {};
    yield put({
      type: ACTIONS.DAL_CRYPTO_SEND_TO_BANK_NEW_CERT_SUCCESS,
      payload: {
        data
      }
    });
    return data;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_CRYPTO_SEND_TO_BANK_NEW_CERT_FAIL });
    throw error;
  }
}
