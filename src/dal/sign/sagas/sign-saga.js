import { call } from 'redux-saga/effects';
import API from 'app/api';
import CryptoProService from 'services/cryptopro/cryptopro';

// Перевыпуск и подпись сертификата
// Для подписи документов есть сага - createSignSaga

/**
 * @param doc
 * @param signer
 */
export const signDoc = (doc, signer) => (
  CryptoProService.createDetachedSign(doc, signer)
);

export function* signCertSaga(action) {
  const { payload: { docId, signAuthId } } = action;
  try {
    const digestResult = yield call(API.signature.initSignCert, {
      docId,
      signAuthId
    });

    const digestData = digestResult && digestResult.data;
    if (!digestData ||  !digestData.digests.length) {
      throw new Error('Ошибка при получении digests документа');
    }
    const { digests, cert } = digestData;
    // информация по плагину
    yield CryptoProService.pluginInfo();
    // поиск сертификата
    const foundCert = yield CryptoProService.findCert(cert.content);
    yield CryptoProService.isOnToken(foundCert);
    const signer = yield CryptoProService.signer(foundCert);
    const signDocResult = yield call(signDoc, digests[0], signer);

    const result = yield call(API.signature.sendSignToBank, {
      docId,
      sign: signDocResult.sign,
      onToken: false
    });

    return result && result.data;
  } catch (error) {
    throw error;
  }
}

