import { put, call, select } from 'redux-saga/effects';
import { saveAs } from 'file-saver';
import moment from 'moment-timezone';
import API from 'app/api';
import { selectedOrganizationsSelector } from '../utils';
import { ACTIONS } from './constants';

export function* getStatements(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_STATEMENTS_GET_LIST_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result =  yield call(API.statement.findAllByQuery, data);
    const resultData = (result && result.data) || [];
    yield put({ type: ACTIONS.DAL_STATEMENTS_GET_LIST_SUCCESS, resultData });
    return resultData;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_STATEMENTS_GET_LIST_FAIL, payload: [], error });
    throw error;
  }
}

export function* exportStatementsListToExcel(action = {}) {
  const { payload = {} } = action;
  yield put({ type: ACTIONS.DAL_STATEMENTS_EXPORT_LIST_TO_EXCEL_REQUEST, payload });
  const selectedOrganizations = yield select(selectedOrganizationsSelector);
  const data = {
    ...payload,
    orgId: selectedOrganizations && selectedOrganizations.toJS()
  };
  try {
    const result = yield call(API.exports.createStatementsByQuery, data);
    saveAs(result.data, `statements_${moment().format('DD-MM-YYYY_HH-mm-ss')}.xlsx`);
    yield put({ type: ACTIONS.DAL_STATEMENTS_EXPORT_LIST_TO_EXCEL_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_STATEMENTS_EXPORT_LIST_TO_EXCEL_FAIL, error });
    throw error;
  }
}

export function* printStatements(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_STATEMENTS_PRINT_REQUEST, payload });
  const { id, detail } = payload;
  const ids = Array.isArray(id) ? id : [id];
  try {
    const result = yield call(API.exports.createStatementByQuery, { docId: ids, format: 'pdf', detail });
    saveAs(result.data, `statements_${moment().format('DD-MM-YYYY_HH-mm-ss')}.pdf`);
    yield put({ type: ACTIONS.DAL_STATEMENTS_PRINT_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_STATEMENTS_PRINT_FAIL, error });
    throw error;
  }
}

export function* exportStatementsTo1C(action = {}) {
  const { payload } = action;
  yield put({ type: ACTIONS.DAL_STATEMENTS_EXPORT_TO_1C_REQUEST, payload });
  const { id } = payload;
  const ids = Array.isArray(id) ? id : [id];
  try {
    const result = yield call(API.exports.exportStatement1C, { docId: ids });
    saveAs(result.data, `statements_to_1C_${moment().format('DD-MM-YYYY_HH-mm-ss')}.zip`);
    yield put({ type: ACTIONS.DAL_STATEMENTS_EXPORT_TO_1C_SUCCESS });
    return true;
  } catch (error) {
    yield put({ type: ACTIONS.DAL_STATEMENTS_EXPORT_TO_1C_FAIL, error });
    throw error;
  }
}
