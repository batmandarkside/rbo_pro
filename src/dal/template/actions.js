import { ACTIONS } from './constants';

export const showTemplateModalAction = (isShow, docId) => ({
  type: ACTIONS.TEMPLATE_MODAL_VISIBILITY,
  payload: {
    show: isShow,
    docId
  }
});

export const saveAsTemplateAction = name => ({
  type: ACTIONS.TEMPLATE_SAVE_AS_TEMPLATE_REQUEST,
  payload: name
});

export const saveAsTemplateFailAction = error => ({
  type: ACTIONS.TEMPLATE_SAVE_AS_TEMPLATE_FAIL,
  error
});
