import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  TEMPLATE_SAVE_AS_TEMPLATE_REQUEST: null,
  TEMPLATE_SAVE_AS_TEMPLATE_SUCCESS: null,
  TEMPLATE_SAVE_AS_TEMPLATE_FAIL   : null,

  TEMPLATE_MODAL_VISIBILITY: null,
});
