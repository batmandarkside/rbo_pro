import { fromJS } from 'immutable';
import { ACTIONS } from './constants';

export const defaultState = fromJS({
  showTemplateModal: false,
  docIdToTemplate: '',
  name: '',
  errors: [],
});


const template = (state = defaultState, action) => {
  switch (action.type) {
    case ACTIONS.TEMPLATE_MODAL_VISIBILITY:
      return state
        .set('showTemplateModal', action.payload.show)
        .set('docIdToTemplate', action.payload.docId);

    case ACTIONS.TEMPLATE_SAVE_AS_TEMPLATE_REQUEST:
      return state.set('name', action.payload);
    case ACTIONS.TEMPLATE_SAVE_AS_TEMPLATE_SUCCESS:
      return state;
    case ACTIONS.TEMPLATE_SAVE_AS_TEMPLATE_FAIL:
      return state.set('errors', fromJS(action.error));

    default:
      return state;
  }
};

export default template;
