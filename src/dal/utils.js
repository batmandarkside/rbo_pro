export const selectedOrganizationsSelector = state => state.getIn(['profile', 'settings', 'selected_orgs']);

export function RBOError(title, error) {
  this.name = 'RBO Error';
  this.title = title || '';
  this.message = (error.response && error.response.data && error.response.data.error) || '';
}

export function RBOControlsError(title, stack) {
  this.name = 'RBO Controls Error';
  this.title = title;
  this.stack = stack;
}
