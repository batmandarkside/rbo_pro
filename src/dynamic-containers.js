import asyncComponent from './async-bundle';

function errorLoading(error) {
  console.warn(`Dynamic page loading failed: ${error}`); // eslint-disable-line no-console
}

const loadRoute = mod => mod.default;

export const PageHome = asyncComponent(() => (
  import('./containers/home').then(component => loadRoute(component))
));

export const PageRPayments = asyncComponent(() => (
  import('./containers/r-payments')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageRPayment = asyncComponent(() => (
  import('./containers/r-payment')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageRPaymentTemplate = asyncComponent(() => (
  import('./containers/r-payment-template')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageCurTransfers = asyncComponent(() => (
  import('./containers/c-payments')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageCurTransfer = asyncComponent(() => (
  import('./containers/c-payment')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageStatements = asyncComponent(() => (
  import('./containers/statements')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageStatement = asyncComponent(() => (
  import('./containers/statement')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageMovements = asyncComponent(() => (
  import('./containers/movements')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageIncomingMessages = asyncComponent(() => (
  import('./containers/incoming-messages')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageIncomingMessage = asyncComponent(() => (
  import('./containers/incoming-message')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageOutgoingMessages = asyncComponent(() => (
  import('./containers/outgoing-messages')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageOutgoingMessage = asyncComponent(() => (
  import('./containers/outgoing-message/index')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageCorrespondents = asyncComponent(() => (
  import('./containers/correspondents')
    .then(component => loadRoute(component))
    .catch(errorLoading)
), { title: 'Контрагенты' });

export const PageCorrespondent = asyncComponent(() => (
  import('./containers/correspondent')
    .then(component => loadRoute(component))
    .catch(errorLoading)
), { title: 'Контрагент' });

export const PageRecall = asyncComponent(() => (
  import('./containers/recall')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageTasks = asyncComponent(() => (
  import('./containers/tasks')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const PageSettings = asyncComponent(() => (
  import('./containers/settings')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const Beneficiaries = asyncComponent(() => (
  import('./containers/beneficiaries')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));

export const Beneficiary = asyncComponent(() => (
  import('./containers/beneficiary')
    .then(component => loadRoute(component))
    .catch(errorLoading)
));
