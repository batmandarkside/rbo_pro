import { axiosConfig } from 'config/application/axios';
import store from '../store';
import { profileLogoutAction } from '../dal/profile/actions';

export default function httpResponseMiddleware() {
  axiosConfig.getAxiosInstance().interceptors.response.use(response => response, (error) => {
    if (error.response.status === 401) {
      store.dispatch(profileLogoutAction());
    }
    return Promise.reject(error);
  });
}
