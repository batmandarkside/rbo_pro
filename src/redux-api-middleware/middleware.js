import CALL_API from './CALL_API';
import { isRSAA } from './validation';
import { normalizeTypeDescriptors, actionWith } from './utils';

function apiMiddleware({ axiosInstance }) {
  return ({ getState }) => next => async (action) => {
    if (!isRSAA(action)) return next(action);
    const bodyAction = action[CALL_API];

    const { types, ...requestParams } = bodyAction;
    const [requestType, successType, failureType] = normalizeTypeDescriptors(types);
    let response = null;

    next(await actionWith(
      requestType,
      [action, getState()]
    ));

    try {
      response = await axiosInstance(requestParams);
    } catch (e) {
      return next(await actionWith(
        {
          ...failureType,
          payload: e.response,
          error  : true
        },
        [action, getState()]
      ));
    }
    if (response.status === 200) {
      return next(await actionWith(
        {
          ...successType
        },
        [action, getState(), response]
      ));
    }

    return next(await actionWith(
      {
        ...failureType,
        error: true
      },
      [action, getState(), response]
    ));
  };
}

export default apiMiddleware;
