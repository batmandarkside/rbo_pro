import { InternalError, ApiError } from './errors';

/**
 * Extract JSON body from a server response
 *
 * @function getJSON
 * @access public
 * @param {object} res - A raw response object
 * @returns {promise}
 */
export const getJSON = async (res) => {
  const contentType = res.headers['content-type'];
  const emptyCodes = [204, 205];
  if (
    !emptyCodes.includes(res.status) &&
    contentType &&
    contentType.includes('json')
  ) {
    return await res.data;
  }

  return await Promise.resolve();
};

/**
 * Blow up string or symbol types into full-fledged type descriptors,
 *   and add defaults
 *
 * @function normalizeTypeDescriptors
 * @access private
 * @param {array} types - The [CALL_API].types from a validated RSAA
 * @returns {array}
 */
export const normalizeTypeDescriptors = (types) => {
  let [requestType, successType, failureType] = types;

  if (typeof requestType === 'string' || typeof requestType === 'symbol') {
    requestType = { type: requestType };
  }

  if (typeof successType === 'string' || typeof successType === 'symbol') {
    successType = { type: successType };
  }
  successType = {
    payload: (action, state, res) => getJSON(res),
    ...successType
  };

  if (typeof failureType === 'string' || typeof failureType === 'symbol') {
    failureType = { type: failureType };
  }
  failureType = {
    payload: (action, state, res) =>
      getJSON(res).then(
        json => new ApiError(res.status, res.statusText, json)
      ),
    ...failureType
  };


  return [requestType, successType, failureType];
};

/**
 * Evaluate a type descriptor to an FSA
 *
 * @function actionWith
 * @access private
 * @param {object} descriptor - A type descriptor
 * @param {array} args - The array of arguments for `payload` and `meta` function properties
 * @returns {object}
 */
export const actionWith = async (descriptor, args) => {
  const desc = descriptor;
  try {
    desc.payload = await (
      typeof desc.payload === 'function' ?
      desc.payload(...args) :
      desc.payload
    );
  } catch (e) {
    desc.payload = new InternalError(e.message);
    desc.error = true;
  }

  try {
    desc.meta = await (
      typeof desc.meta === 'function' ?
      desc.meta(...args) :
      desc.meta
    );
  } catch (e) {
    delete desc.meta;
    desc.payload = new InternalError(e.message
    );
    desc.error = true;
  }

  return desc;
};
