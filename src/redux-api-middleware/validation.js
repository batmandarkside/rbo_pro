import isPlainObject from 'lodash/isPlainObject';
import CALL_API from './CALL_API';


/**
 * Is the given action a plain JavaScript object with a [CALL_API] property?
 *
 * @function isRSAA
 * @access public
 * @param {object} action - The action to check
 * @returns {boolean}
 */
export const isRSAA = action =>
  isPlainObject(action) && Object.prototype.hasOwnProperty.call(action, CALL_API);


/**
 * Is the given object a valid type descriptor?
 *
 * @function isValidTypeDescriptor
 * @access private
 * @param {object} obj - The object to check agains the type descriptor definition
 * @returns {boolean}
 */
export const isValidTypeDescriptor = (obj) => {
  const validKeys = [
    'type',
    'payload',
    'meta'
  ];

  if (!isPlainObject(obj)) {
    return false;
  }
  for (let key in obj) { // eslint-disable-line
    if (!validKeys.includes(key) && Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  if (!('type' in obj)) {
    return false;
  } else if (typeof obj.type !== 'string' && typeof obj.type !== 'symbol') {
    return false;
  }

  return true;
};

/**
 * Checks an action against the RSAA definition, returning a (possibly empty)
 * array of validation errors.
 *
 * @function validateRSAA
 * @access public
 * @param {object} action - The action to check against the RSAA definition
 * @returns {array}
 */
export const validateRSAA = (action) => {
  const validationErrors = [];
  const validCallAPIKeys = [
    'url',
    'method',
    'data',
    'params',
    'headers',
    'types'
  ];
  const validMethods = [
    'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'
  ];

  if (!isRSAA(action)) {
    validationErrors.push('RSAAs must be plain JavaScript objects with a [CALL_API] property');
    return validationErrors;
  }

  Object.keys(action).forEach((key) => {
    if (key !== [CALL_API]) {
      validationErrors.push(`Invalid root key: ${key}`);
    }
  });

  const callAPI = action[CALL_API];

  if (!isPlainObject(callAPI)) {
    validationErrors.push('[CALL_API] property must be a plain JavaScript object');
  }

  Object.keys(callAPI).forEach((key) => {
    if (!~validCallAPIKeys.indexOf(key)) { // eslint-disable-line no-bitwise
      validationErrors.push(`Invalid [CALL_API] key: ${key}`);
    }
  });

  const { url, method, headers, types } = callAPI;

  /**
   * Validate url
   */
  if (typeof url === 'undefined') {
    validationErrors.push('[CALL_API] must have an url property');
  } else if (typeof url !== 'string' && typeof url !== 'function') {
    validationErrors.push('[CALL_API].url property must be a string or a function');
  }

  /**
   * Validate Method
   */
  if (typeof method === 'undefined') {
    validationErrors.push('[CALL_API] must have a method property');
  } else if (typeof method !== 'string') {
    validationErrors.push('[CALL_API].method property must be a string');
  } else if (!~validMethods.indexOf(method.toUpperCase())) { // eslint-disable-line no-bitwise
    validationErrors.push(`Invalid [CALL_API].method: ${method.toUpperCase()}`);
  }

  /**
   * Validate Headers
   */
  if (typeof headers !== 'undefined' && !isPlainObject(headers) && typeof headers !== 'function') {
    validationErrors.push('[CALL_API].headers property must be undefined, a plain JavaScript object, or a function');
  }

  /**
   * Validate types
   */
  if (typeof types === 'undefined') {
    validationErrors.push('[CALL_API] must have a types property');
  } else if (!Array.isArray(types) || types.length !== 3) {
    validationErrors.push('[CALL_API].types property must be an array of length 3');
  } else {
    const [requestType, successType, failureType] = types;

    if (typeof requestType !== 'string' && typeof requestType !== 'symbol' && !isValidTypeDescriptor(requestType)) {
      validationErrors.push('Invalid request type');
    }

    if (typeof successType !== 'string' && typeof successType !== 'symbol' && !isValidTypeDescriptor(successType)) {
      validationErrors.push('Invalid success type');
    }

    if (typeof failureType !== 'string' && typeof failureType !== 'symbol' && !isValidTypeDescriptor(failureType)) {
      validationErrors.push('Invalid failure type');
    }
  }

  return validationErrors;
};

/**
 * Is the given action a valid RSAA?
 *
 * @function isValidRSAA
 * @access public
 * @param {object} action - The action to check against the RSAA definition
 * @returns {boolean}
 */
export const isValidRSAA = action => !validateRSAA(action).length;
