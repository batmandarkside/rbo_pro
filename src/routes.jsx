import NotFoundRouter from './containers/no-router';
import * as Pages from './dynamic-containers';

export const routes = [
  {
    exact: true,
    path: '/beneficiaries',
    title: 'Отзыв',
    component: Pages.Beneficiaries
  },
  {
    exact: true,
    path: '/beneficiaries/:id',
    title: 'Отзыв',
    component: Pages.Beneficiary
  },
  {
    exact: true,
    path: '/:parentDocType/:parentDocId/recalls/:id',
    title: 'Отзыв',
    component: Pages.PageRecall
  },
  {
    exact: true,
    path: '/correspondents',
    title: 'Контрагенты',
    component: Pages.PageCorrespondents
  },
  {
    exact: true,
    path: '/correspondents/:id',
    title: 'Контрагент',
    component: Pages.PageCorrespondent
  },
  {
    exact: true,
    path: '/r-payments/archive',
    title: 'Рублевые платежи / Архив',
    component: Pages.PageRPayments

  },
  {
    exact: true,
    path: '/r-payments/trash',
    title: 'Рублевые платежи / Удаленные',
    component: Pages.PageRPayments

  },
  {
    exact: true,
    path: '/r-payments/:id',
    title: 'Платежное поручение',
    component: Pages.PageRPayment
  },
  {
    exact: true,
    path: '/r-payments/templates/:id',
    title: 'Шаблон платежного поручения',
    component: Pages.PageRPaymentTemplate
  },
  {
    exact: true,
    path: '/r-payments',
    title: 'Рублевые платежи / Рабочие документы',
    component: Pages.PageRPayments
  },
  {
    exact: true,
    path: '/cur-transfers/archive',
    title: 'Валютные переводы / Архив',
    component: Pages.PageCurTransfers
  },
  {
    exact: true,
    path: '/cur-transfers/trash',
    title: 'Валютные переводы / Удаленные',
    component: Pages.PageCurTransfers
  },
  {
    exact: true,
    path: '/cur-transfers',
    title: 'Валютные переводы / Рабочие документы',
    component: Pages.PageCurTransfers
  },
  {
    exact: true,
    path: '/cur-transfers/:id',
    title: 'Валютный перевод',
    component: Pages.PageCurTransfer
  },
  {
    exact: true,
    path: '/statements/movements',
    title: 'Выписки / Операции',
    component: Pages.PageMovements
  },
  {
    exact: true,
    path: '/statements/:id',
    title: 'Выписка',
    component: Pages.PageStatement
  },
  {
    exact: true,
    path: '/statements',
    title: 'Выписки',
    component: Pages.PageStatements
  },
  {
    exact: true,
    path: '/messages/outgoing/archive',
    title: 'Исходящие письма / Архив',
    component: Pages.PageOutgoingMessages
  },
  {
    exact: true,
    path: '/messages/outgoing/trash',
    title: 'Исходящие письма / Удаленные',
    component: Pages.PageOutgoingMessages

  },
  {
    exact: true,
    path: '/messages/outgoing/:id',
    title: 'Письмо в банк',
    component: Pages.PageOutgoingMessage
  },
  {
    exact: true,
    path: '/messages/outgoing',
    title: 'Исходящие письма',
    component: Pages.PageOutgoingMessages
  },
  {
    exact: true,
    path: '/messages/:id',
    title: 'Письмо из банка',
    component: Pages.PageIncomingMessage
  },
  {
    exact: true,
    path: '/messages',
    title: 'Входящие письма',
    component: Pages.PageIncomingMessages
  },
  {
    exact: true,
    path: '/tasks',
    title: 'Журнал заданий',
    component: Pages.PageTasks
  },
  {
    exact: true,
    path: '/settings/:settingsTypeUrl',
    title: 'Настройки',
    component: Pages.PageSettings
  },
  {
    exact: true,
    path: '/settings',
    title: 'Настройки',
    component: Pages.PageSettings
  },
  {
    exact: true,
    path: '/',
    component: Pages.PageHome
  },
  {
    path: '*',
    component: NotFoundRouter
  }
];
