## Настройка сбора логов
(https://www.cryptopro.ru/forum2/default.aspx?g=posts&t=4577)[cryptopro форум]

## программ debugview
(https://docs.microsoft.com/en-us/sysinternals/downloads/debugview)[debugview]


## CLI 
* /opt/cprocsp/sbin/cpconfig --help 
* /opt/cprocsp/sbin/cpconfig -license -view
* sudo /opt/cprocsp/sbin/cpconfig -license -set <серийный_номер>

## Список закрытых ключей /opt/cprocsp/bin/
    ./csptest -keyset -enum_cont -verifycontext -fqcn

 ## Перечисление контейнеров компьютера: /opt/cprocsp/bin/
    ./csptest -keyset -enum_cont -verifycontext -fqcn -machinekeys
