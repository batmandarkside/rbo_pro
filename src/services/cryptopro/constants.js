export const CERT_KEY_ENCIPHERMENT_KEY_USAGE  = 0x10;
export const CERT_DATA_ENCIPHERMENT_KEY_USAGE = 0x20;
export const CERT_DIGITAL_SIGNATURE_KEY_USAGE = 0x80;
export const CERT_NON_REPUDIATION_KEY_USAGE = 0x40;
export const XCN_CERT_NAME_STR_NONE = 0;
export const PROVIDER_NAME = 'Crypto-Pro GOST R 34.10-2001 Cryptographic Service Provider';
// export const REGISTRATION_AUTHORITY_USER = '1.2.643.2.2.34.6';
export const CX509CertificateRequestPkcs10 = 'X509Enrollment.CX509CertificateRequestPkcs10';
export const CX509ExtensionEnhancedKeyUsage = 'X509Enrollment.CX509ExtensionEnhancedKeyUsage';
export const CX509ExtensionKeyUsage = 'X509Enrollment.CX509ExtensionKeyUsage';
export const CX500DistinguishedName = 'X509Enrollment.CX500DistinguishedName';
export const CX509PrivateKey = 'X509Enrollment.CX509PrivateKey';
export const CX509Enrollment = 'X509Enrollment.CX509Enrollment';
export const CObjectIds = 'X509Enrollment.CObjectIds';
export const CObjectId = 'X509Enrollment.CObjectId';

// -----BEGIN NEW CERTIFICATE REQUEST---—
export const CREATE_REQUEST = 0x3;
