/**
 *
 * @param {Number} number
 * @returns {string}
 */
export function decimalToHexString(number) {
  let copyNumber = number;
  if (copyNumber < 0) {
    copyNumber = 0xFFFFFFFF + copyNumber + 1;
  }

  return copyNumber.toString(16).toUpperCase();
}


/**
 *
 * @param e
 * @constructor
 */
export function GetMessageFromException(e) {
  let err = e.message;
  if (!err) {
    err = e;
  } else if (e.number) {
    err += `(0x${decimalToHexString(e.number)})`;
  }
  return err;
}


export function isIOS() {
  const retVal = (navigator.userAgent.match(/ipod/i) ||
  navigator.userAgent.match(/ipad/i) ||
  navigator.userAgent.match(/iphone/i));
  return retVal;
}

export function isIE() {
  const retVal = ((navigator.appName === 'Microsoft Internet Explorer') || // IE < 11
  navigator.userAgent.match(/Trident\/./i)); // IE 11
  return retVal;
}

export const isOperaCheck = () => navigator.userAgent.match(/opr/i);
export const isYaBrowserCheck = () => navigator.userAgent.match(/YaBrowser/i);

export function isChromiumBased() {
  const retValChrome = navigator.userAgent.match(/chrome/i);
  if (retValChrome === null) {
    return false;
  }

  if (retValChrome.length > 0 || isOperaCheck() != null) {
    return true;
  }

  return false;
}

