/* eslint-disable react/jsx-filename-extension */
import { expect }       from 'chai';
import {
  getErrorByKey,
  getErrorData,
  getErrors,
  splitErrorText,
  prepareConfForNotification
} from '../errors-util';

describe('Error-Utils tests', () => {
  test('getErrorByKey', () => {
    expect(getErrorByKey).to.be.function;
    expect(getErrorByKey('sign.wrong_rights'))
      .to.eql('Операция недоступна. У вас недостаточно прав для подписи документа.');
    expect(getErrorByKey('')).to.be.empty;
  });

  test('getErrorData', () => {
    const error = {
      response: {
        data: {
          error: '123error'
        }
      }
    };
    const error2 = {
      response: {
        data: {}
      }
    };
    expect(getErrorData(error)).to.equal('123error');
    expect(getErrorData(error2)).to.empty;
  });

  test('getErrors', () => {
    expect(getErrors('sign.wrong_rights'))
      .to.equal('Операция недоступна. У вас недостаточно прав для подписи документа.');
    expect(getErrors('sign'))
      .to.have.property('error')
      .to.equal('Ошибка подписи');
  });

  test('splitErrorText', () => {
    expect(splitErrorText('sign.wrong_rights'))
      .to.equal('sign');

    expect(getErrors('signwrong_rights'))
      .to.equal('signwrong_rights');

    expect(getErrors())
      .to.be.undefined;
  });

  test('prepareConfForNotification', () => {
    const error = {
      response: {
        data: {
          error: 'sign.wrong_rights'
        }
      }
    };
    expect(prepareConfForNotification(error)).to.have.property('type')
      .to.equal('error');
    expect(prepareConfForNotification(error)).to.have.property('title')
      .to.equal('Ошибка подписи');
    expect(prepareConfForNotification(error)).to.have.property('message')
      .to.equal('Операция недоступна. У вас недостаточно прав для подписи документа.');
  });


  test('prepareConfForNotification not error title', () => {
    const error = {
      response: {
        data: {
          error: 'server.legacy_backend_unreachable'
        }
      }
    };
    expect(prepareConfForNotification(error)).to.have.property('type')
      .to.equal('error');
    expect(prepareConfForNotification(error)).to.have.property('title')
      .to.equal('Ошибка.');
    expect(prepareConfForNotification(error)).to.have.property('message')
      .to.equal('Сервер ELBRUS не отвечает.');
  });
});
