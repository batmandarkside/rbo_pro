import { expect }       from 'chai';
import {
  insertThousandsDelimiters,
  unformat,
  normalize,
  customizableFormatter
}   from '../number-formatter';

describe('Number formatter tests', () => {
  test('insertThousandsDelimiters', () => {
    const formattedNumber = insertThousandsDelimiters('111111', '.');
    expect(formattedNumber).to.equal('111.111');
  });

  test('unformat', () => {
    let unformatNumber = unformat('f111.g111');
    expect(unformatNumber).to.equal('111.111');
    unformatNumber = unformat('f111,g111', ',');
    expect(unformatNumber).to.equal('111,111');
  });

  test('normalize', () => {
    let normalized = normalize();
    expect(normalized).to.equal('0.00');
    normalized = normalize(25);
    expect(normalized).to.equal('25.00');
  });

  test('customizableFormatter', () => {
    const formatter = customizableFormatter('a', 'b', 3);
    const formattedNumber = formatter('1000');
    expect(formattedNumber).to.equal('1a000b000');
  });
});
