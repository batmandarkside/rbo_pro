/* eslint-disable react/jsx-filename-extension */
import { expect } from 'chai';
import {
  filterPlural,

  getLastOperationDay
} from '../';

describe('Utils tests', () => {
  test('filterPlural ', () => {
    const plural = filterPlural.bind(null, ['стол', 'стола', 'столов']);
    expect(plural(11)).to.equal('столов');
    expect(plural(4)).to.equal('стола');
    expect(plural(1)).to.equal('стол');
    expect(plural(26)).to.equal('столов');
    expect(plural(0)).to.equal('столов');
  });

  test('getLastOperationDay', () => {
    expect(getLastOperationDay).to.be.function;
    expect(getLastOperationDay()).to.be.string;
  });
});
