/* eslint-disable react/jsx-filename-extension */
import { expect }       from 'chai';
import {
  isEmptyField,
  email,
  required,
  minLength,
  maxLength,
  integer,
  oneOf,
  match,
  isBiggerThen
}       from '../validation';

describe('Date utils tests', () => {
  it('isEmptyField ', () => {
    const result = isEmptyField('');
    const result1 = isEmptyField([]);
    const result2 = isEmptyField({});
    const result3 = isEmptyField(null);
    const result4 = isEmptyField(undefined);
    const result5 = isEmptyField(0);
    
    expect(result).to.be.true;
    expect(result1).to.be.true;
    expect(result2).to.be.true;
    expect(result3).to.be.true;
    expect(result4).to.be.true;
    expect(result5).to.be.false;
  });
  
  
  // @todo уточнить шаблон валидации email
  it('email', () => {
    let result = email('test@test.com');
    expect(result).to.be.null;
    
    result = email('test@.com');
    expect(result).to.equal('Невалидный email адрес');
    
    result = email('test@test');
    expect(result).to.equal('Невалидный email адрес');
    
    result = email('__test__@test.com');
    expect(result).to.be.null;
    
    result = email('__test.new.dev@test.com');
    expect(result).to.be.null;
  });
  
  it('required', () => {
    let result = required('');
    expect(result).to.equal('Обязательно для заполенения');
  
    result = required();
    expect(result).to.equal('Обязательно для заполенения');
    
    result = required('test@.com');
    expect(result).to.be.null;
  });
  
  it('minLength', () => {
    let result = minLength('минимальная длинна 10', 10);
    expect(result).to.be.null;
    
    result = minLength('минима', 10);
    expect(result).to.equal('Мин. 10 символов');
    
    result = minLength('ми', 4);
    expect(result).to.equal('Мин. 4 символа');
  });
  
  it('maxLength', () => {
    let result = maxLength('минималь', 10);
    expect(result).to.be.null;
    
    result = maxLength('минима', 5);
    expect(result).to.equal('Макс. 5 символов');
    
    result = maxLength('ми', 1);
    expect(result).to.equal('Макс. 1 символ');
  });
  
  it('integer', () => {
    let result = integer(10);
    expect(result).to.be.null;
    
    result = integer('число');
    expect(result).to.equal('Только число');
  });
  
  it('oneOf', () => {
    let result = oneOf(['test', 'test2', 'tes3'], 'test');
    expect(result).to.be.null;
    
    result = oneOf(['test', 'test2', 'tes3'], 'test4');
    expect(result).to.equal('Должен быть один из: test, test2, tes3');
  });
  
  it('match', () => {
    let result = match('test', 'test');
    expect(result).to.be.null;
    
    result = match('минималь', 'не минимальный');
    expect(result).to.equal('Значения не совпадают');
  });
  
  it('isBiggerThen', () => {
    let result = isBiggerThen(10, 12);
    expect(result).to.be.null;
    
    result = isBiggerThen(10, 8);
    expect(result).to.equal('Значение 10 больше 8');
  });
});
