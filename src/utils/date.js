import moment from 'moment-timezone';

/**
 * Отдает дату последнего операционного дня
 * @return {string} ISO time format
 */
export function getLastOperationDay() {
  const SUNDAY = 0;
  const lastOperationDay = moment({ hour: 0, minute: 0, seconds: 0 }).subtract(1, 'days');
  if (lastOperationDay.day() === SUNDAY) {
    lastOperationDay.subtract(1, 'days');
  }
  return lastOperationDay;
}

export function isSupportWorkingHours() {
  const MOSCOW_UTC_OFFSET = '+03:00';
  const TIME_FORMAT = 'HH:mm:ss';
  const SUPPORT_WORK_FROM = '03:00:00';
  const SUPPORT_WORK_TO = '19:00:00';

  const currentMoscowDayTime =  moment().utcOffset(MOSCOW_UTC_OFFSET);

  const isWorkingTime = currentMoscowDayTime.isBetween(
    moment(SUPPORT_WORK_FROM, TIME_FORMAT),
    moment(SUPPORT_WORK_TO, TIME_FORMAT)
  );
  const isWorkingDay = currentMoscowDayTime.isoWeekday() < 6;

  return isWorkingTime && isWorkingDay;
}
