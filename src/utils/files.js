export const getMimeTypeTitleByMimeType = (mimeType) => {
  const types = {
    'application/zip'             : 'Compressed Archive ZIP',
    'application/x-zip'           : 'Compressed Archive ZIP',
    'application/x-zip-compressed': 'Compressed Archive ZIP',
    'application/x-compress'      : 'Compressed Archive ZIP',
    'application/x-compressed'    : 'Compressed Archive ZIP',
    'multipart/x-zip'             : 'Compressed Archive ZIP',

    'application/x-rar-compressed': 'Compressed Archive RAR',
    'application/x-rar'           : 'Compressed Archive RAR',
    'application/rar'             : 'Compressed Archive RAR',

    'application/x-rtf'    : 'Rich Text Format File',
    'text/rtf'             : 'Rich Text Format File',
    'application/rtf'      : 'Rich Text Format File',
    'text/richtext'        : 'Rich Text Format File',
    'application/x-soffice': 'Rich Text Format File',

    'application/dbf'               : 'Database',
    'application/x-dbf'             : 'Database',
    'zz-application/zz-winassoc-dbf': 'Database',

    'text/plain'        : 'Plain text',
    'plain/text'        : 'Plain text',
    'application/txt'   : 'Plain text',
    'browser/internal'  : 'Plain text',
    'text/anytext'      : 'Plain text',
    'widetext/plain'    : 'Plain text',
    'widetext/paragraph': 'Plain text',

    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'Microsoft Excel',
    'application/vnd.ms-excel'                                         : 'Microsoft Excel',
    'application/msexcel'                                              : 'Microsoft Excel',
    'application/x-msexcel'                                            : 'Microsoft Excel',
    'application/x-ms-excel'                                           : 'Microsoft Excel',
    'application/x-excel'                                              : 'Microsoft Excel',
    'application/x-dos_ms_excel'                                       : 'Microsoft Excel',
    'application/xls'                                                  : 'Microsoft Excel',

    'image/tif'         : 'Tagged Image Format File',
    'image/x-tif'       : 'Tagged Image Format File',
    'image/tiff'        : 'Tagged Image Format File',
    'image/x-tiff'      : 'Tagged Image Format File',
    'application/tif'   : 'Tagged Image Format File',
    'application/x-tif' : 'Tagged Image Format File',
    'application/tiff'  : 'Tagged Image Format File',
    'application/x-tiff': 'Tagged Image Format File',

    'image/jpeg'              : 'JPEG/JIFF Image',
    'image/jpg'               : 'JPEG/JIFF Image',
    'image/jpe_'              : 'JPEG/JIFF Image',
    'image/pjpeg'             : 'JPEG/JIFF Image',
    'image/vnd.swiftview-jpeg': 'JPEG/JIFF Image',

    'image/gif'      : 'Graphic Interchange Format',
    'image/x-xbitmap': 'Graphic Interchange Format',
    'image/gi_'      : 'Graphic Interchange Format',

    'application/msword'                                                     : 'Microsoft Word',
    'application/doc'                                                        : 'Microsoft Word',
    'appl/text'                                                              : 'Microsoft Word',
    'application/vnd.msword'                                                 : 'Microsoft Word',
    'application/vnd.ms-word'                                                : 'Microsoft Word',
    'application/winword'                                                    : 'Microsoft Word',
    'application/word'                                                       : 'Microsoft Word',
    'application/x-msw6'                                                     : 'Microsoft Word',
    'application/x-msword'                                                   : 'Microsoft Word',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'Microsoft Word',

    'image/bmp'                : 'Windows OS/2 Bitmap Graphics',
    'image/x-bmp'              : 'Windows OS/2 Bitmap Graphics',
    'image/x-bitmap'           : 'Windows OS/2 Bitmap Graphics',
    'image/x-win-bitmap'       : 'Windows OS/2 Bitmap Graphics',
    'image/x-windows-bmp'      : 'Windows OS/2 Bitmap Graphics',
    'image/ms-bmp'             : 'Windows OS/2 Bitmap Graphics',
    'image/x-ms-bmp'           : 'Windows OS/2 Bitmap Graphics',
    'application/bmp'          : 'Windows OS/2 Bitmap Graphics',
    'application/x-bmp'        : 'Windows OS/2 Bitmap Graphics',
    'application/x-win-bitmap ': 'Windows OS/2 Bitmap Graphics',

    'image/png'        : 'Portable (Public) Network Graphic',
    'application/png'  : 'Portable (Public) Network Graphic',
    'application/x-png': 'Portable (Public) Network Graphic',

    'application/pdf'     : 'Portable Document Format',
    'application/x-pdf'   : 'Portable Document Format',
    'application/acrobat' : 'Portable Document Format',
    'applications/vnd.pdf': 'Portable Document Format',
    'text/pdf'            : 'Portable Document Format',
    'text/x-pdf'          : 'Portable Document Format',

    'text/xml'         : 'Extensible Markup Language(XML)',
    'application/xml'  : 'Extensible Markup Language(XML)',
    'application/x-xml': 'Extensible Markup Language(XML)',

    'video/x-mng': 'Animation',
    'video/mng'  : 'Animation',

    'application/arj'      : 'Compressed Archive ARJ',
    'application/x-arj'    : 'Compressed Archive ARJ',
    'application/x-tar'    : 'Compressed Archive TAR',
    'application/x-gzip'   : 'Compressed Archive GZIP',
    'application/x-stuffit': 'Compressed Archive ARJ',
    'zz-application'       : 'Compressed Archive ARJ',

    'application/vnd.ms-powerpoint': 'Microsoft PowerPoint',
    'application/mspowerpoint'     : 'Microsoft PowerPoint',
    'application/ms-powerpoint'    : 'Microsoft PowerPoint',
    'application/mspowerpnt'       : 'Microsoft PowerPoint',
    'application/vnd-mspowerpoint' : 'Microsoft PowerPoint',
    'application/powerpoint'       : 'Microsoft PowerPoint',
    'application/x-powerpoint'     : 'Microsoft PowerPoint',
    'application/x-m'              : 'Microsoft PowerPoint'
  };

  return types[mimeType] || 'Неизвестный тип';
};

export const getFormattedFileSize = (size) => {
  const bytes = parseInt(size, 10);
  if (bytes >= 1073741824) {
    return `${(bytes / 1073741824).toFixed(2)} Гб`;
  } else if (bytes >= 1048576) {
    return `${(bytes / 1048576).toFixed(2)} Мб`;
  } else if (bytes >= 1024) {
    return `${(bytes / 1024).toFixed(2)} Кб`;
  }
  return `${bytes} байт`;
};

export const getFormattedFilesTotalSize = (filesSizes) => {
  let totalSize = 0;
  filesSizes.forEach((item) => { totalSize += item; });
  return totalSize ? getFormattedFileSize(totalSize) : '';
};

export const blobToBase64 = (blob, callback) => {
  const reader = new FileReader();
  reader.readAsDataURL(blob);
  reader.onloadend = () => { callback(reader.result.split(',')[1]); };
};
