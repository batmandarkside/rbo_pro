import { fromJS } from 'immutable';
import { isNil } from 'lodash';
import { DOC_STATUSES, SIGN_TYPES, SIGN_VALIDATION_STATUSES } from 'constants';

export const regExpEscape = text => text && text.replace(/[-[\]{}()*+?.,\\^$|#]/gi, '\\$&');

/**
 * Проверка на наличие slash в конце строки
 * @param {string} str
 */
export function isSlashTerminatedString(str) {
  return (typeof str === 'string' && str.charAt(str.length - 1) === '/');
}

/**
 * filterPlural(['стол', 'стола', 'столов'], 11)
 * curry
 *  this.curry = filterPlural.bind(null, ['стол', 'стола', 'столов'])
 *  this.curry(1)
 *  this.curry(5)
 *
 * @param {Array<string>} titles
 * @param {number} number
 * @returns {string}
 */
export const filterPlural = (titles, number) => {
  const cases = [2, 0, 1, 1, 1, 2];
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
};

/**
 * Получение css-класса стиля в зависимости от статуса документа
 * @param {string} docStatus - статус документа
 * @returns {string} - 'm-status-success', 'm-status-error' или ''
 */
export const getDocStatusClassNameMode = (docStatus) => {
  switch (docStatus) {
    case DOC_STATUSES.ACCEPTED:
    case DOC_STATUSES.PROCESSED:
    case DOC_STATUSES.EXECUTED:
      return 'm-status-success';
    case DOC_STATUSES.INVALID:
    case DOC_STATUSES.INVALID_PROPS:
    case DOC_STATUSES.INVALID_SIGN:
    case DOC_STATUSES.DECLINED:
    case DOC_STATUSES.DECLINED_BY_ABS:
    case DOC_STATUSES.RECALLED:
      return 'm-status-error';
    default:
      return '';
  }
};

/**
 * Получение css-класса стиля в зависимости от результата проверки подписи
 * @param {string} signValidationStatus - результат проверки подписи
 * @returns {string} - 'm-status-error' или ''
 */
export const getSignValidationStatusClassNameMode = (signValidationStatus) => {
  switch (signValidationStatus) {
    case SIGN_VALIDATION_STATUSES.INVALID:
      return 'm-status-error';
    default:
      return '';
  }
};

/**
 * Форматирование номера счета (добавление разделителей, по умолчанию - ".")
 * @param {string} accNum - номер счета (40703810300000000038)
 * @param {string} delimiter - разделитель (.)
 * @returns {string} - отформатированный номер счета (40703.810.3.00000000038)
 */
export const getFormattedAccNum = (accNum, delimiter = '.') => {
  const value = accNum ? accNum.toString().replace(/\D/g, '').substring(0, 20) : '';

  const result = [];
  value.split('').forEach((digit, i) => {
    result.push(digit);
    if (i !== value.length - 1 && (i === 4 || i === 7 || i === 8)) result.push(delimiter);
  });
  return result.join('');
};

/**
 * НОрмализация максимальной длины любого текста
 * @param {string} value
 * @param {number} max
 * @returns {*}
 */
export const normalizeAllMaxLength = (value, max) => {
  if (isNil(value) || value === '') {
    return value;
  }
  return value.substring(0, max);
};

/**
 * Получение флага доступности редактирования документа в зависимости от статуса и флага архивности документа
 * @param {string} docStatus - статус документа
 * @param {boolean} isDocArchived - флаг архивности документа
 * @returns {boolean}
 */
export const getIsDocEditable = (docStatus, isDocArchived) => {
  if (isDocArchived) return false;
  return !![
    DOC_STATUSES.NEW,
    DOC_STATUSES.CREATED,
    DOC_STATUSES.IMPORTED,
    DOC_STATUSES.INVALID
  ].find(v => docStatus === v);
};

/**
 * Получение флага необходимости получения ошибок для документа в зависимости от статуса
 * @param {string} docStatus - статус документа
 * @returns {boolean}
 */
export const getIsDocNeedGetErrors = docStatus => !![
  DOC_STATUSES.CREATED,
  DOC_STATUSES.IMPORTED,
  DOC_STATUSES.INVALID
].find(v => docStatus === v);

/**
 * Получение ошибок для формы в зависимотси от уровня и маппинга ошибок к полям
 * @param {List} docErrors - ошибки документа
 * @param {Object} docErrorsFieldsMap - схема маппинга ошибок к полям формы
 * @param {Array} levels - список уровней ошибок (1, 2, 3)
 * @returns {List}
 */
export const getDocFormErrors = (docErrors, docErrorsFieldsMap, levels) => {
  const result = {};
  docErrors
    .filter(error => !!levels.find(v => error.get('level') === v))
    .forEach((error) => {
      const fieldsIds = docErrorsFieldsMap[error.get('id')];
      if (fieldsIds) {
        fieldsIds.forEach((fieldId) => {
          result[fieldId] = true;
        });
      }
    });
  return fromJS(result);
};

/**
 * Маппинг секций и ошибок документа
 * @param {object} docSections - секции документа
 * @param {object} docSectionsFieldsMap - схема маппинга полей в секциях документа
 * @param {List} docErrors - ошибки документа
 * @param {List} activeDocField - активное поле документа (для того, чтобы сделать активной секцию)
 * @returns {Map}
 */
export const getMappingDocSectionsAndErrors = (docSections, docSectionsFieldsMap, docErrors, activeDocField) => {
  const result = [];
  docSections.forEach((section, key) => {
    result.push(section.merge({
      id: key,
      firstFieldId: docSectionsFieldsMap[key][0],
      isActive: !!docSectionsFieldsMap[key].find(field => field === activeDocField),
      errors: docErrors
        .filter(error => !!docSectionsFieldsMap[key].find(field => error.get('fieldsIds').find(e => field === e)))
        .sort((a, b) => (a.get('level') < b.get('level') ? 1 : -1))
    }));
  });
  return fromJS(result.sort((a, b) => (a.get('order') < b.get('order') ? -1 : 1)));
};

/**
 * Получение значения для скроллинга по data-id элемента и селектору элемента контента
 * @param {string} elementDataId - data-id элемента
 * @param {string} contentSelector - селектор элемента контента
 */
export const getScrollTopByDataId = (elementDataId, contentSelector) => {
  const contentOffsetTop = document.querySelector(contentSelector).getBoundingClientRect().top;
  const elementOffsetTop = document.querySelector(`[data-id="${elementDataId}"]`).getBoundingClientRect().top;
  return elementOffsetTop - contentOffsetTop - 16;
};

/**
 * Получение типа подписи
 * @param {number} signTypeIndex - index типа подписи (приходит с сервера)
 * @returns {string} - строковое значение типа подписи
 */
export const getSignType = signTypeIndex =>
[null,
  SIGN_TYPES.FIRST,
  SIGN_TYPES.SECOND,
  SIGN_TYPES.VISA,
  SIGN_TYPES.SINGLE,
  SIGN_TYPES.VK,
  SIGN_TYPES.CONTROLLER
][signTypeIndex] || '';

/**
 * Плавная прокрутка контента
 * @param {number} scrollTo - значение, куда промотать window
 * @param {function()} callBackFunc - коллбэк-функция
 */
export const smoothScrollTo = (scrollTo, callBackFunc) => {
  const maxScroll = Math.max(
      document.body.scrollHeight, document.documentElement.scrollHeight,
      document.body.offsetHeight, document.documentElement.offsetHeight,
      document.body.clientHeight, document.documentElement.clientHeight
    ) - window.innerHeight;
  const realScrollTo = scrollTo < maxScroll ? scrollTo : maxScroll;
  const scrollFrom = window.pageYOffset;
  const scrollDistance = realScrollTo - scrollFrom;
  const scrollDirection = scrollDistance > 0;
  if (scrollDistance < -20 || scrollDistance > 20) {
    let scrollStep = scrollDistance / 30;
    if (scrollStep > 0 && scrollStep < 20) scrollStep = 20;
    if (scrollStep < 0 && scrollStep > -20) scrollStep = -20;
    let scrolled = scrollFrom;
    const scrollInterval = setInterval(() => {
      scrolled += scrollStep;
      window.scrollTo(window.pageXOffset, scrolled);
      if ((scrollDirection && scrolled > realScrollTo) || (!scrollDirection && scrolled < realScrollTo)) {
        clearInterval(scrollInterval);
        callBackFunc && callBackFunc();
      }
    }, 10);
  } else callBackFunc && callBackFunc();
};

export {
  getMimeTypeTitleByMimeType,
  getFormattedFileSize,
  getFormattedFilesTotalSize,
  blobToBase64
} from './files';

export {
  getLastOperationDay,
  isSupportWorkingHours
} from './date';
