import {
  padEnd,
  toString,
  trimStart
} from 'lodash';

export const THOUSANDS_DELIMITER = ' ';
export const DECIMAL_DELIMITER = '.';
export const PRECISION = 2;

export function insertThousandsDelimiters(value, thousandsDelimiter = THOUSANDS_DELIMITER) {
  return String(value).replace(/\B(?=(\d{3})+(?!\d))/g, thousandsDelimiter);
}

/**
 * Strips all characters but digits and decimal delimiter
 * @param value
 * @param decimalDelimiter
 * @returns {string|XML}
 */
export function unformat(value, decimalDelimiter = DECIMAL_DELIMITER) {
  return toString(value).replace(/[\.,](?=.*[\.,])/g, '') // eslint-disable-line no-useless-escape
    .replace(/[\.,]/, decimalDelimiter) // eslint-disable-line no-useless-escape
    .replace(new RegExp(`[^\\d\\${decimalDelimiter}]`, 'g'), ''); // eslint-disable-line no-useless-escape
}

/**
 * Unformat value, trim/add leading zeroes and trim to fixed precision
 *
 * @param value
 * @param decimalDelimiter
 * @param precision
 * @returns {string}
 */
export function normalize(value = '0', decimalDelimiter = DECIMAL_DELIMITER, precision = PRECISION) {
  let [integer, fractional] = unformat(value).split(decimalDelimiter);
  const negative = value.includes && value.includes('-')
    ? '-'
    : '';

  integer = trimStart(integer, '0');
  integer = padEnd(integer, 1, '0');

  fractional = padEnd(fractional, precision, '0');
  fractional = fractional.slice(0, precision);

  return `${negative}${integer}${decimalDelimiter}${fractional}`;
}

/**
 * Use to build concrete formatter
 *
 * @param thousandsDelimiter
 * @param decimalDelimiter
 * @param precision
 * @returns {format} Concrete formatter
 */
export function customizableFormatter(thousandsDelimiter = THOUSANDS_DELIMITER, decimalDelimiter = DECIMAL_DELIMITER, precision = PRECISION) {
  if (thousandsDelimiter === decimalDelimiter) {
    throw new Error('Thousands delimiter should not be equal to decimal delimiter');
  }

  return function format(value) {
    let [integer, fractional] = normalize(value, decimalDelimiter, precision).split(decimalDelimiter);
    integer = insertThousandsDelimiters(integer, thousandsDelimiter);

    if (precision === 0) {
      return integer;
    }

    return `${integer}${decimalDelimiter}${fractional}`;
  };
}

export default customizableFormatter(THOUSANDS_DELIMITER, DECIMAL_DELIMITER);
